//
//  InterruptAllViewController.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"

@interface InterruptAllViewController : UIViewController
{
    IBOutlet UIButton *interruptButton;
    IBOutlet MarqueeLabel *interruptLabel;
     IBOutlet UILabel *lbl_Green;
}

@end
