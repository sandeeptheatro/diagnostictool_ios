//
//  InterruptAllViewController.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "InterruptAllViewController.h"
#import "Constant.h"
#import "StateMachineHandler.h"
#import "ViewController.h"
#import "UIViewController.h"
#import "HttpHandler.h"
#import "DiagnosticTool.h"

@interface InterruptAllViewController ()
{
    NSString *storeNameString,*staticerrorcode;
    UILabel *connectionLabel;
    NSDictionary *dictLang;
}


@end

@implementation InterruptAllViewController


-(void)coveragejson:(NSNotification *)notification
{
    connectionLabel.hidden=YES;
    interruptButton.userInteractionEnabled=YES;
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)coverageRequestToServertask:(NSNotification *)notification
{
    // Network messages getting from native c2 and updating to UI
    [self connectionRequest];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"texttime"])
    {
        if ([notification.object isEqualToString:@"server connection lost..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"server connection lost..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"could not start a connection..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"could not start a connection..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not logon to store..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not logon to store..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not connect to Proxy server..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not connect to Proxy server..."],staticerrorcode];
        }
        else
        {
            //Second time login ->please wait
            connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
        }
        
    }
    else
    {
        //First time login ,connecting to server->fetching emp list
        connectionLabel.text=notification.object;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"texttime"];
    }
    
    
}

-(void)tgsAlarmerror:(NSString *)tgsLoginerror
{
    staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:@"InterruptAllVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"TgsLogin"],[[DiagnosticTool sharedManager]generateErrorCode:tgsLoginerror]];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        [HttpHandler alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"TgsLogin"] severity:@"1" alarmdesc:staticerrorcode];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
    }];
}

-(void)connectionRequest
{
    connectionLabel.hidden=NO;
    interruptButton.userInteractionEnabled=NO;
    
}

#pragma mark - view methods
-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coverageRequestToServertask" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        [[StateMachineHandler sharedManager]sendPlaybufferenable:NO];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
        }];
    }];
    [super viewWillDisappear: animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    NSLog(@"InterruptAll viewWillAppear");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coverageRequestToServertask:)
                                                 name:@"coverageRequestToServertask"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:@"localnotification" object:nil];
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    interruptLabel.marqueeType = MLContinuous;
    interruptLabel.scrollDuration = 8.0;
    interruptLabel.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    interruptLabel.fadeLength = 0.0f;
    interruptLabel.leadingBuffer = 0.0f;
    interruptLabel.trailingBuffer = 10.0f;
    
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    interruptLabel.text=[dictLang objectForKey:@"Interrupt All"];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    connectionLabel=[[UILabel alloc]init];
    connectionLabel.textColor=[UIColor whiteColor];
        connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
    connectionLabel.frame= CGRectMake(0,self.view.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height);
    connectionLabel.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    connectionLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:connectionLabel];
    connectionLabel.hidden=YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coveragejson:)
                                                 name:@"coveragejson"
                                               object:nil];
    interruptButton.userInteractionEnabled=YES;
    storeNameString=[[NSUserDefaults standardUserDefaults]objectForKey:@"storeNameString"];
    self.navigationItem.title=storeNameString;
    [interruptButton addTarget:self action:@selector(methodTouchDown:) forControlEvents: UIControlEventTouchDown];
    [interruptButton addTarget:self action:@selector(recordPauseTapped:) forControlEvents: UIControlEventTouchUpInside];
    [interruptButton addTarget:self action:@selector(buttonDragOutside:) forControlEvents: UIControlEventTouchDragOutside];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: kNavBackgroundColor};
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

#pragma mark - logonStealing method
-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"InterruptAllViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [ self.navigationController popToRootViewControllerAnimated:YES];
    }
}


#pragma mark - reOpenClosedSockets method
-(void)reOpenClosedSocketsNotifiacation
{
    NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush isEqualToString:@"YES"])
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"StoreListViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        else
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"VPViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        
    }
    
    else
    {
        connectionLabel.hidden=NO;
        [[StateMachineHandler sharedManager] reOpenClosedSockets];
        [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
    }
}


#pragma mark - button down method
- (void)methodTouchDown:(UIButton *)sender
{
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    [[StateMachineHandler sharedManager]sendPlaybufferenable:YES];
    interruptLabel.text=[dictLang objectForKey:@"Interrupting all…"];
    [[StateMachineHandler sharedManager] mediabuttonAction:@"interruptall" nameStr:@"null" Button:YES];
    [interruptButton setImage:[UIImage imageNamed:@"mike_blue_white.png"] forState:UIControlStateNormal];
}


#pragma mark - button up method
- (void)recordPauseTapped:(UIButton *)sender
{
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [[StateMachineHandler sharedManager]sendPlaybufferenable:NO];
    interruptLabel.text=[dictLang objectForKey:@"Interrupt All"];
    [[StateMachineHandler sharedManager] mediabuttonAction:@"interruptall" nameStr:@"null" Button:NO];
    [interruptButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
}


-(void)buttonDragOutside:(UIButton *)sender
{
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [[StateMachineHandler sharedManager]sendPlaybufferenable:NO];
    interruptLabel.text=[dictLang objectForKey:@"Interrupt All"];
    [[StateMachineHandler sharedManager] mediabuttonAction:@"interruptall" nameStr:@"null" Button:NO];
    [interruptButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
}



-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coveragejson" object:nil];
}

@end
