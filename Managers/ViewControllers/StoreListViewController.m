//
//  PlacesViewController.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 12/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "StoreListViewController.h"
#import "PlacesTableViewCell.h"
#import "ContactsViewController.h"
#import "Constant.h"
#import "HttpHandler.h"
#import "StoreList.h"
#import "ViewController.h"
#import "Reachability.h"
#import "Constant.h"
#import "AlertView.h"
#import "UIViewController.h"
#import "LeftMenuViewController.h"
#import <sys/utsname.h>


@interface StoreListViewController ()
{
    NSMutableArray *storesArray,*searchResultsAll,*storesListArray;
    NSDictionary *loginDetails;
    StoreList *storeList;
    UIView *backendView,*alphaView;
    NSArray *mappFeatures;
}

@end

@implementation StoreListViewController
@synthesize loggedoff,logonFailed;

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
     [[NSNotificationCenter defaultCenter] postNotificationName:@"lefttablereload" object:nil];
    return YES;
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
    alphaView.hidden=YES;
     [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
    //Code to handle the gesture
}

-(void)leftMenuSelected:(NSNotification *)notification
{
    if ([notification.object isEqualToString:@"open"])
    {
        alphaView.hidden=NO;
    }
    else
    {
        alphaView.hidden=YES;
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:kInternetConnectiontitle message:kInternetConnectionmessage];
        
    }
    else
    {
    [NSThread detachNewThreadSelector:@selector(salesupdate) toTarget:self withObject:nil];
    }
}

-(void)salesupdate
{
    mappFeatures=[[HttpHandler salesEnabled:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:@"Tx874874" twMoment:@"46746.6877"]objectForKey:@"mappFeatures"];
    [self performSelectorOnMainThread:@selector(salesupdateAction) withObject:nil waitUntilDone:NO];
}

-(void)salesupdateAction
{
    if (![mappFeatures isKindOfClass:[NSNull class]])
    {
        [[NSUserDefaults standardUserDefaults]setObject:mappFeatures forKey:@"salesenable"];
    }
}


- (void)viewDidLoad {
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [storesTableview setContentOffset:CGPointMake(0, searchBar.frame.size.height)];
    alphaView=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    alphaView.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.0];
    [self.view addSubview:alphaView];
    self.navigationController.navigationBar.hidden=NO;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    tapGestureRecognizer.delegate = self;
    [alphaView addGestureRecognizer:tapGestureRecognizer];
    alphaView.hidden=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(leftMenuSelected:) name:@"leftMenuSelectedNotification" object:nil];
    [[UIBarButtonItem appearance] setTintColor:kNavBackgroundColor];
    [[NSUserDefaults standardUserDefaults]setObject:@"storeview" forKey:@"storeview"];
     NSLog(@"StorelistView viewdidload");
    storesArray=[[NSMutableArray alloc]init];
    storesListArray=[[NSMutableArray alloc]init];
    // Create your image
    UIImage *image = [UIImage imageNamed: @"logo.png"];
    UIImageView *imageview = [[UIImageView alloc] initWithImage: image];
    
    // set the text view to the image view
    self.navigationItem.titleView = imageview;
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:kInternetConnectiontitle message:kInternetConnectionmessage];
        
    }
    else
    {
        backendView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        backendView.alpha=0.5;
        backendView.backgroundColor=[UIColor darkGrayColor];
        UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake((self.view.frame.size.width-30)/2, self.view.frame.size.height/2, 60, 60)];
        activityView.backgroundColor=[UIColor darkGrayColor];
        activityView.activityIndicatorViewStyle=UIActivityIndicatorViewStyleWhiteLarge;
        [activityView startAnimating];
        [backendView addSubview:activityView];
        [self.view addSubview:backendView];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
       loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
       
       NSDictionary *twResponseHeader= [HttpHandler storeList:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"]];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
           
            if (![[[twResponseHeader objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
            {
                NSArray *storeListArray=[twResponseHeader objectForKey:@"twStoreList"];
            for (int i=0;i<[storeListArray count];i++)
            {
                storeList=[[StoreList alloc]init];
                
                if ([[[storeListArray objectAtIndex:i] objectForKey:@"name"] length]>0)
                {
                    storeList.name=[[storeListArray objectAtIndex:i] objectForKey:@"name"];
                    storeList.firstName=[[storeList.name componentsSeparatedByString:@" "][0]lowercaseString];
                    storeList.storeId=[[[storeListArray objectAtIndex:i] objectForKey:@"storeId"] intValue];
                    if ([storeList.name componentsSeparatedByString:@" "].count>1)
                    {
                        storeList.lastName=[[storeList.name componentsSeparatedByString:@" "][1]lowercaseString];
                         
                    }
                     [ storesListArray addObject:storeList];
                }
                else
                {
                    [AlertView alert:@"Error" message:@"The request timed out."];
                }
               
                
                [storesArray addObject:storeList];
                NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                [storesArray sortUsingDescriptors:[NSArray arrayWithObject:sort]];
            }
            [backendView removeFromSuperview];
           
            [storesTableview reloadData];
            }
            else
            {
                NSString *twerrorMessage=[twResponseHeader objectForKey:@"twerrorMessage"];
                [AlertView alert:@"Error" message:twerrorMessage];
            }
            
        }];
    }];
    }
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:viewController animated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [searchResultsAll count];
        
    } else {
        return [storesArray count];
        
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"cell";
    storeList=[storesArray objectAtIndex:indexPath.row];
    PlacesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[PlacesTableViewCell alloc]
                 initWithStyle:UITableViewCellStyleDefault
                 reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.placeButton.layer.cornerRadius=8;
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        storeList = [searchResultsAll objectAtIndex:indexPath.row];
        if ([storeList.name length]>0)
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%@",
                                   storeList.name];
            cell.textLabel.textColor=kNavBackgroundColor;
        }
  
        
    } else {
        [cell.placeButton setTitle:storeList.name forState:UIControlStateNormal];
        cell.placeButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [cell.placeButton addTarget:self action:@selector(placeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.placeButton.backgroundColor=kNavBackgroundColor;
        NSInteger placeButtonTag=indexPath.row;
        cell.placeButton.tag=placeButtonTag;
    }
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactsViewController *contactsController;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        
            contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactsViewController"];
        
            storeList=[searchResultsAll objectAtIndex:indexPath.row];
        
                [[NSUserDefaults standardUserDefaults] setObject:storeList.name forKey:@"storeNameString"];
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:storeList.storeId] forKey:@"storeid"];
        [self.navigationController pushViewController:contactsController animated:YES];
    }
}

-(void)placeButtonAction:(UIButton *)indexrow
{
    storeList=[storesArray objectAtIndex:indexrow.tag];
    ContactsViewController *contactsController;
   
        contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactsViewController"];
    [[NSUserDefaults standardUserDefaults] setObject:storeList.name forKey:@"storeNameString"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:storeList.storeId] forKey:@"storeid"];
    [self.navigationController pushViewController:contactsController animated:YES];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    
    NSMutableArray *firstNameArray=[[NSMutableArray alloc]init];
    NSMutableArray *lastNameArray=[[NSMutableArray alloc]init];
    NSMutableArray *totalNameArray=[[NSMutableArray alloc]init];
    searchResultsAll=[[NSMutableArray alloc]init];
    if (searchText.length>0) {
        NSPredicate *firstNamepredicate,*lastNamepredicate,*tagOutNamepredicate;
        
        firstNamepredicate = [NSPredicate predicateWithFormat:
                              @"firstName BEGINSWITH[cd] %@", searchText];
        
        NSPredicate *notFirstNamepredicate = [NSCompoundPredicate notPredicateWithSubpredicate:firstNamepredicate];
        
        lastNamepredicate = [NSPredicate predicateWithFormat:
                             @"lastName BEGINSWITH[cd] %@", searchText];
        NSPredicate *finalLastNamepredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[lastNamepredicate, notFirstNamepredicate]];
        if (firstNamepredicate!=nil)
        {
            [firstNameArray addObjectsFromArray:[storesListArray filteredArrayUsingPredicate:firstNamepredicate]];
            
            
        }
        if (lastNamepredicate!=nil)
        {
            
            [ lastNameArray addObjectsFromArray:[storesListArray filteredArrayUsingPredicate:finalLastNamepredicate] ];
            
        }
        if ([[storesListArray filteredArrayUsingPredicate:firstNamepredicate]count]==0 && [[storesListArray filteredArrayUsingPredicate:lastNamepredicate] count]==0)
        {
            
            tagOutNamepredicate = [NSPredicate predicateWithFormat:
                                   @"name BEGINSWITH[cd] %@", searchText];
        }
        if (tagOutNamepredicate!=nil)
        {
            [totalNameArray addObjectsFromArray: [storesListArray filteredArrayUsingPredicate:tagOutNamepredicate]];
            
        }
        if (firstNameArray.count>0)
        {
            [searchResultsAll addObjectsFromArray:firstNameArray];
        }
        if (lastNameArray.count>0)
        {
            [searchResultsAll addObjectsFromArray:lastNameArray];
        }
        if (totalNameArray.count>0)
        {
            [searchResultsAll addObjectsFromArray:totalNameArray];
        }
        
    }
}



#pragma mark - UISearchDisplayController delegate methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end
