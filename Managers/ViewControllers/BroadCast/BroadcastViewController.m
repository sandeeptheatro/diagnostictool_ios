//
//  BroadCostViewController.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "BroadcastViewController.h"
#import "StateMachineHandler.h"
#import "ViewController.h"
#import "UIViewController.h"
#import "Constant.h"
#import "AudioManager.h"
#import "HttpHandler.h"
#import "DiagnosticTool.h"

@interface BroadcastViewController ()
{
    UILabel *connectionLabel;
    UIBarButtonItem *speakerButton;
    NSDictionary *dictLang;
    NSString *staticerrorcode;
}


@end

@implementation BroadcastViewController


-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)coveragejson:(NSNotification *)notification
{
    connectionLabel.hidden=YES;
    broadCastButton.userInteractionEnabled=YES;
}

-(void)coverageRequestToServertask:(NSNotification *)notification
{
    // Network messages getting from native c2 and updating to UI
    [self connectionRequest];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"texttime"])
    {
        if ([notification.object isEqualToString:@"server connection lost..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"server connection lost..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"could not start a connection..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"could not start a connection..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not logon to store..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not logon to store..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not connect to Proxy server..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not connect to Proxy server..."],staticerrorcode];
        }
        else
        {
            //Second time login ->please wait
            connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
        }
        
    }
    else
    {
        //First time login ,connecting to server->fetching emp list
        connectionLabel.text=notification.object;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"texttime"];
    }
    
    
}


-(void)tgsAlarmerror:(NSString *)tgsLoginerror
{
    staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:@"BroadcastVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"TgsLogin"],[[DiagnosticTool sharedManager]generateErrorCode:tgsLoginerror]];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        [HttpHandler alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"TgsLogin"] severity:@"1" alarmdesc:staticerrorcode];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
    }];
}

-(void)connectionRequest
{
    connectionLabel.hidden=NO;
    broadCastButton.userInteractionEnabled=NO;
    
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coverageRequestToServertask" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        
        // Navigation button was pressed. Do some stuff
        [[StateMachineHandler sharedManager]sendPlaybufferenable:NO];
        [[StateMachineHandler sharedManager] enableBroadcastScreen:NO];
        [[StateMachineHandler sharedManager]sendCancelPlayoutToNative:YES];
    }
    
    
    [super viewWillDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"Broadcastview viewDidAppear");
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coverageRequestToServertask:)
                                                 name:@"coverageRequestToServertask"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:@"localnotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterLockNotification) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    // Navigation button was pressed. Do some stuff
    [[StateMachineHandler sharedManager]sendPlaybufferenable:YES];
    [[StateMachineHandler sharedManager] enableBroadcastScreen:YES];
    [[StateMachineHandler sharedManager]sendCancelPlayoutToNative:NO];
    
    [super viewDidAppear:animated];
}

-(void)enterLockNotification
{
    [[StateMachineHandler sharedManager] doQueryButtonAction:NO];
    [[StateMachineHandler sharedManager]enterLockScreen];
}

- (void)viewDidLoad {
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    broadCastLabel.text=[dictLang objectForKey:@"Broadcast"];
    broadCastLive.text=[dictLang objectForKey:@"Live"];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coveragejson:)
                                                 name:@"coveragejson"
                                               object:nil];
    
    connectionLabel=[[UILabel alloc]init];
    connectionLabel.textColor=[UIColor whiteColor];
    connectionLabel.text=@"Please wait...";
    connectionLabel.frame= CGRectMake(0,self.view.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height);
    connectionLabel.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    connectionLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:connectionLabel];
    connectionLabel.hidden=YES;
    self.navigationItem.title=[[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
    [broadCastButton addTarget:self action:@selector(buttonDownPressed:) forControlEvents: UIControlEventTouchDown];
    [broadCastButton addTarget:self action:@selector(buttonUpPressed:) forControlEvents: UIControlEventTouchUpInside];
    [broadCastButton addTarget:self action:@selector(buttonDragOutPressed:) forControlEvents: UIControlEventTouchDragOutside];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    speakerButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"loud"] style:UIBarButtonItemStyleDone target:self action:@selector(speakerButtonAction:)];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"loud"] isEqualToString:@"mic"])
    {
        //[speakerButton setTintColor:[UIColor grayColor]];
        [speakerButton setImage:[UIImage imageNamed:@"headphones"]];
        speakerButton.tag=2;
        [[NSUserDefaults standardUserDefaults]setObject:@"mic" forKey:@"loud"];
        
    }
    else
    {
        speakerButton.tag=1;
        //[speakerButton setTintColor:kNavBackgroundColor];
        [speakerButton setImage:[UIImage imageNamed:@"loud"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"loud" forKey:@"loud"];
        
    }
    
    self.navigationItem.rightBarButtonItem=speakerButton;
    [[AudioManager getsharedInstance] switchAudioOutput];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)speakerButtonAction:(UIBarButtonItem *)button
{
    if (button.tag==1)
    {
       // [speakerButton setTintColor:[UIColor grayColor]];
        [speakerButton setImage:[UIImage imageNamed:@"headphones"]];
        speakerButton.tag=2;
        [[NSUserDefaults standardUserDefaults]setObject:@"mic" forKey:@"loud"];
    }
    else
    {
        speakerButton.tag=1;
        //[speakerButton setTintColor:kNavBackgroundColor];
        [speakerButton setImage:[UIImage imageNamed:@"loud"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"loud" forKey:@"loud"];
    }
    [[AudioManager getsharedInstance] switchAudioOutput];
}

-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"BroadcastViewController"])
    {
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            
            [[StateMachineHandler sharedManager]sendPlaybufferenable:NO];
            [[StateMachineHandler sharedManager] enableBroadcastScreen:NO];
            [[StateMachineHandler sharedManager]sendCancelPlayoutToNative:YES];
            [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [ self.navigationController popToRootViewControllerAnimated:YES];
            }];
        }];
        
    }
}

-(void)reOpenClosedSocketsNotifiacation
{
    NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush isEqualToString:@"YES"])
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"StoreListViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        else
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"VPViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        
    }
    
    else
    {
        connectionLabel.hidden=NO;
        [[StateMachineHandler sharedManager]sendPlaybufferenable:YES];
        [[StateMachineHandler sharedManager] enableBroadcastScreen:YES];
        [[StateMachineHandler sharedManager]sendCancelPlayoutToNative:NO];
        
        [[StateMachineHandler sharedManager]reOpenClosedSockets];
        [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
    }
}

- (void)buttonDownPressed:(UIButton *)sender
{
    broadCastLabel.text=[dictLang objectForKey:@"Broadcasting..."];
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    [[StateMachineHandler sharedManager] doQueryButtonAction:YES];
}

- (void)buttonDragOutPressed:(UIButton *)sender
{
    broadCastLabel.text=[dictLang objectForKey:@"Broadcasting..."];
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [[StateMachineHandler sharedManager] doQueryButtonAction:NO];
}



- (void)buttonUpPressed:(UIButton *)sender
{
    broadCastLabel.text=[dictLang objectForKey:@"Broadcast"];
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [[StateMachineHandler sharedManager] doQueryButtonAction:NO];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coveragejson" object:nil];
}

@end
