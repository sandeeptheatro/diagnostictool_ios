//
//  BroadCostViewController.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BroadcastViewController : UIViewController
{
    IBOutlet UIButton *broadCastButton;
     IBOutlet UILabel *lbl_Green;
    IBOutlet UILabel *broadCastLabel;
    IBOutlet UILabel *broadCastLive;
}

@end
