//
//  ViewController.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 12/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface ViewController : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate>
{
    UIButton *loginButton,*forgotpwdButton,*chooseLangudage;;
    IBOutlet UILabel *nameLabel,*passwordLabel,*chooseLang;
    IBOutlet UITextField *nameTextField,*passwordTextField;
}
@property(nonatomic,strong)NSString *logonFailed,*loggedoff;
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
-(IBAction)loginDetail:(id)sender;
-(IBAction)forgotpwdAction:(id)sender;
-(IBAction)chooseLanguage:(id)sender;

@end
