//
//  ViewController.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 12/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "ViewController.h"
#import "Constant.h"
#import "HttpHandler.h"
#import "StoreListViewController.h"
#import "LoginDetailViewController.h"
#import "ForgotPasswordViewController.h"
#import "Reachability.h"
#import "AlertView.h"
#import "NSLogger.h"
#import "AudioManager.h"
#import "EarboxDataStore.h"
#import "VPViewController.h"
#import "ActivityIndicatorView.h"
#import "InstoreData.h"
#import "AppDelegate.h"
#import "DiagnosticTool.h"

@interface ViewController ()
{
    NSDictionary *twResponseHeaderDic;
    UIView *settingsView;
    UITextField *settingsTextField;
    UIButton *cancelButton,*settingButton;
        NSDictionary *dict;
}

@end

@implementation ViewController
@synthesize loggedoff,logonFailed;

#pragma mark - keyboard disappear method
/*
 This method will call when user clicked on emailid/password field and keyboard will disappear
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

/*- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)replacementString {
 if (textField.text) {
 textField.text = [textField.text stringByReplacingCharactersInRange:range withString:replacementString];
 } else {
 textField.text = replacementString;
 }
 
 return NO;
 }*/


#pragma mark - forgot password method
/*
 This method will call when user clicked on forgotpassward button and it will move to ForgotPasswordViewController class
 */
-(IBAction)forgotpwdAction:(id)sender
{
    ForgotPasswordViewController *loginDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dict objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController pushViewController:loginDetailVC animated:YES];
}

/*
 This method will call when logon stealing happend and it will clear all Database and userdefaults vales to be empty
 */
-(void)cleardatabase
{
    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"messagetotalcount"];
    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"announcementtotalcount"];
    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"messagecount"];
    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"announcementcount"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"earbox"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"firstTimeStore"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"logedin"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"earboxv2"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"inboxpreviousTab"];
    
    //Database delete
    [[EarboxDataStore sharedStore]deleteAllEntities:@"EarboxInbox"];
    [[EarboxDataStore sharedStore]deleteAllEntities:@"EarboxSaved"];
    [[EarboxDataStore sharedStore]deleteAllEntities:@"EarboxSent"];
    [[EarboxDataStore sharedStore]deleteAllEntities:@"AnnouncementInbox"];
    [[EarboxDataStore sharedStore]deleteAllEntities:@"AnnouncementSaved"];
    [[EarboxDataStore sharedStore]deleteAllEntities:@"AnnouncementSent"];
    [[EarboxDataStore sharedStore]deleteAllEntities:@"Favorites"];
    
    //Audio files deleted
    [[AudioManager getsharedInstance]deleteallEarboxAudioPath];
    
    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
    [[StoreListViewController getsharedInstance].deletetimer invalidate];
    
    [StoreListViewController getsharedInstance].deletetimer=nil;
    [[VPViewController getsharedInstance].deletetimer invalidate];
    
    [VPViewController getsharedInstance].deletetimer=nil;
}


#pragma mark - View methods
/*
 This method will call when logon stealing happend and it show proper dialog in UI
 */
-(void)viewWillAppear:(BOOL)animated
{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    nameTextField.placeholder=[dict objectForKey:@"Email address or User name"];
    passwordTextField.placeholder=[dict objectForKey:@"Password"];
    
    NSLog(@"Login view viewWillAppear");
    self.navigationController.navigationBar.hidden=YES;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"loggedoff"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dict objectForKey:@"Logged off"]
                                                        message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"you have been logged-off by another C2"],[[DiagnosticTool sharedManager] generateScreenCode:@"LoginVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Login"],[[DiagnosticTool sharedManager]generateErrorCode:@"you have been logged-off by another C2"]]
                                                       delegate:nil cancelButtonTitle:[dict objectForKey:@"OK"] otherButtonTitles:nil];
        [alert show];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"loggedoff"];
        [self cleardatabase];
        
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"loggedoffsystem"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dict objectForKey:@"Logged off by system"]
                                                        message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Your session is logged off by system"],[[DiagnosticTool sharedManager] generateScreenCode:@"LoginVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Login"],[[DiagnosticTool sharedManager]generateErrorCode:@"Your session is logged off by system"]]
                                                       delegate:nil cancelButtonTitle:[dict objectForKey:@"OK"] otherButtonTitles:nil];
        [alert show];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"loggedoffsystem"];
        [self cleardatabase];
        
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"LogonFailed"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dict objectForKey:@"LogonFailed"]
                                                        message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"This name is used in C2"],[[DiagnosticTool sharedManager] generateScreenCode:@"LoginVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Login"],[[DiagnosticTool sharedManager]generateErrorCode:@"This name is used in C2"]]
                              
                                                       delegate:nil cancelButtonTitle:[dict objectForKey:@"OK"] otherButtonTitles:nil];
        [alert show];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LogonFailed"];
        [self cleardatabase];
    }
    NSDictionary  *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    
    nameTextField.text=[loginDetails objectForKey:@"twUserName"];
    passwordTextField.text=[loginDetails objectForKey:@"twPassword"];
    
    [super viewWillAppear:animated];
}

-(IBAction)chooseLanguage:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[dict objectForKey:@"Language"]
                                                                   message:[dict objectForKey:@"Choose Language"]
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:[dict objectForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }]];// 1
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"English"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                  {
                                      NSLog(@"You pressed english button");
                                      
                                      NSString *path = [[NSBundle mainBundle] pathForResource:@"Localizable"
                                                                                       ofType:@"strings"
                                                                                  inDirectory:nil
                                                                              forLocalization:@"en"];
                                      
                                      // compiled .strings file becomes a "binary property list"
                                      dict = [NSDictionary dictionaryWithContentsOfFile:path];
                                      
                                     // NSString *jaTranslation = [dict objectForKey:@"Forgot Password"];
                                      
                                      [loginButton setTitle:[dict objectForKey:@"Log in"] forState:UIControlStateNormal];
                                      //forgotpwdButton
                                      [forgotpwdButton setTitle:[dict objectForKey:@"Forgot Password"] forState:UIControlStateNormal];
                                      chooseLang.text=[dict objectForKey:@"Language"];
                                      nameTextField.placeholder=[dict objectForKey:@"Email address or User name"];
                                      passwordTextField.placeholder=[dict objectForKey:@"Password"];
                                      [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"Language"];
                                       [[NSUserDefaults standardUserDefaults] setObject:@"english" forKey:@"Languageselected"];
                                      [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"ChoosedLang"];
                                      
                                  }]; // 2
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Français"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                   {
                                       NSLog(@"You pressed français button");
                                       
                                       NSString *path = [[NSBundle mainBundle] pathForResource:@"Localizable"
                                                                                        ofType:@"strings"
                                                                                   inDirectory:nil
                                                                               forLocalization:@"fr"];
                                       
                                       // compiled .strings file becomes a "binary property list"
                                       dict = [NSDictionary dictionaryWithContentsOfFile:path];
                                       
                                      // NSString *jaTranslation = [dict objectForKey:@"Forgot Password"];
                                       
                                       [loginButton setTitle:[dict objectForKey:@"Log in"] forState:UIControlStateNormal];
                                       //forgotpwdButton
                                       [forgotpwdButton setTitle:[dict objectForKey:@"Forgot Password"] forState:UIControlStateNormal];
                                       chooseLang.text=[dict objectForKey:@"Language"];
                                       nameTextField.placeholder=[dict objectForKey:@"Email address or User name"];
                                       passwordTextField.placeholder=[dict objectForKey:@"Password"];
                                       [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"Language"];
                                       [[NSUserDefaults standardUserDefaults] setObject:@"french" forKey:@"Languageselected"];
                                       [[NSUserDefaults standardUserDefaults] setObject:@"FR" forKey:@"ChoosedLang"];
                                   }]; // 3
    
    [alert addAction:firstAction]; // 4
    [alert addAction:secondAction]; // 5
    
    [self presentViewController:alert animated:YES completion:nil]; // 6
}


- (void)viewDidLoad
{
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"NotificationFilter"];
    
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"SwitchtoC2"];
    
        //[[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"UMA"];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"secondtime"])
    {
        
    }
    else
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Localizable"
                                                         ofType:@"strings"
                                                    inDirectory:nil
                                                forLocalization:@"en"];
        
        // compiled .strings file becomes a "binary property list"
        dict = [NSDictionary dictionaryWithContentsOfFile:path];
        
        //NSString *jaTranslation = [dict objectForKey:@"Forgot Password"];
        
        [loginButton setTitle:[dict objectForKey:@"Log in"] forState:UIControlStateNormal];
        //forgotpwdButton
        [forgotpwdButton setTitle:[dict objectForKey:@"Forgot Password"] forState:UIControlStateNormal];
        chooseLang.text=[dict objectForKey:@"Language"];
        
        // nameTextField,*passwordTextField
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dict objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
        [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"ChoosedLang"];
        [[NSUserDefaults standardUserDefaults] setObject:@"english" forKey:@"Languageselected"];
        [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"Language"];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"secondtime"];
    }
    
    
   /* */
    dict=[[NSUserDefaults standardUserDefaults]objectForKey:@"Language"];
    [loginButton setTitle:[dict objectForKey:@"Log in"] forState:UIControlStateNormal];
    //forgotpwdButton
    [forgotpwdButton setTitle:[dict objectForKey:@"Forgot Password"] forState:UIControlStateNormal];
    chooseLang.text=[dict objectForKey:@"Language"];
    [[NSUserDefaults standardUserDefaults]setObject:@"loginview" forKey:@"storeview"];
    settingButton=[UIButton buttonWithType:UIButtonTypeCustom];
    settingButton.frame=CGRectMake(self.view.frame.size.width-140, self.view.frame.size.height-60, 120, 40);
    [ settingButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [settingButton setImage:[UIImage imageNamed:@"initial-dots"] forState:UIControlStateNormal];
    settingButton.layer.cornerRadius=7;
    [settingButton addTarget:self action:@selector(settingsButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:settingButton];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
       [AlertView alert:[dict objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"LoginVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Login"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
        
    }
    
    passwordTextField.delegate=self;
    nameLabel.backgroundColor=kLineBackgroundColor;
    passwordLabel.backgroundColor=kLineBackgroundColor;
    loginButton.layer.cornerRadius=7;
    nameTextField.clearsOnBeginEditing=YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


#pragma mark - setting clicked method
/*
 This method will call when user clicked on setting button in login UI and it will create setting UI
 */
-(void)settingsButton
{
    [settingButton setEnabled:NO];
    [forgotpwdButton setEnabled:NO];
    cancelButton=[UIButton buttonWithType:UIButtonTypeCustom];
    
    settingsView=[[UIView alloc] initWithFrame:CGRectMake(30, (self.view.frame.size.height-200)/2, self.view.frame.size.width-60, 200)];
    float  cancelyposition=(self.view.frame.size.height-240)/2;
    cancelButton.frame=CGRectMake(self.view.frame.size.width-35,cancelyposition, 25, 25);
    
    settingsView.layer.cornerRadius=7;
    settingsView.backgroundColor=kSettingsColor;
    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(logFilesend)];
    doubleTapRecognizer.numberOfTapsRequired = 5;
    doubleTapRecognizer.numberOfTouchesRequired = 1;
    doubleTapRecognizer.delegate = self;
    [settingsView addGestureRecognizer:doubleTapRecognizer];
    UILabel *pathLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 50, 100, 40)];
    pathLabel.textColor=[UIColor blackColor];
    pathLabel.text=@"Server Path:";
    UILabel *serverLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 30)];
    serverLabel.textColor=kNavBackgroundColor;
    serverLabel.text=[dict objectForKey:@"Web Services"];
    [settingsView addSubview:serverLabel];
    UILabel *versionLabel=[[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-137, 120, 90, 40)];
    versionLabel.textColor=kGreyColor;
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
    versionLabel.text=[NSString stringWithFormat:@"V:%@",version];;
    [settingsView addSubview:versionLabel];
    settingsTextField=[[UITextField alloc] initWithFrame:CGRectMake(20, 50, self.view.frame.size.width-100, 40)];
    settingsTextField.borderStyle=UITextBorderStyleRoundedRect;
    settingsTextField.textColor=[UIColor blackColor];
    
    NSString *str=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpathGlobal"];
    
    if (str.length>0)
    {
        settingsTextField.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpathGlobal"];
    }
    else
    {
        settingsTextField.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
        [[NSUserDefaults standardUserDefaults]setObject:settingsTextField.text forKey:@"serverpathGlobal"];
    }
    
    if (settingsTextField.text.length==0)
    {
        settingsTextField.text=@"https://central.theatro.com/";
        [[NSUserDefaults standardUserDefaults]setObject:settingsTextField.text forKey:@"serverpath"];
    }
    settingsTextField.delegate=self;
    settingsTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    settingsTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [settingsView addSubview:settingsTextField];
    UIButton *okButton=[UIButton buttonWithType:UIButtonTypeCustom];
    okButton.frame=CGRectMake((settingsView.frame.size.width-100)/2, 120, 100, 40);
    okButton.backgroundColor=[UIColor lightGrayColor];
    [okButton setTitle:[dict objectForKey:@"OK"] forState:UIControlStateNormal];
    okButton.backgroundColor=kNavBackgroundColor;
    okButton.layer.cornerRadius=7;
    [okButton addTarget:self action:@selector(okButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [settingsView addSubview:okButton];
    
    cancelButton.layer.cornerRadius=7;
    cancelButton.backgroundColor=kSettingsColor;
    [cancelButton setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelButton];
    [self.view addSubview:settingsView];
}

/*
 This method will call when user clicked on OK button in setting UI and will store server url in userdefaults
 */
-(void)okButtonAction
{
    [settingButton setEnabled:YES];
    [forgotpwdButton setEnabled:YES];
    if ([settingsTextField.text length]!=0)
    {
        [[NSUserDefaults standardUserDefaults]setObject:settingsTextField.text forKey:@"serverpathGlobal"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"https://central.theatro.com/" forKey:@"serverpathGlobal"];
        settingsTextField.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpathGlobal"];
        
    }
    [settingsView removeFromSuperview];
    [cancelButton removeFromSuperview];
}

/*
 This method will call when user clicked on Cancel button in setting UI and will move to login UI
 */
-(void)cancelButtonAction
{
    [settingButton setEnabled:YES];
    [forgotpwdButton setEnabled:YES];
    [settingsView removeFromSuperview];
    [cancelButton removeFromSuperview];
}


#pragma mark - user login method
/*
 This method will call when user clicked on Signin button
 */
-(IBAction)loginDetail:(id)sender
{
    nameTextField.text=[nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //passwordTextField.text=[passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([nameTextField.text length]!=0 && [passwordTextField.text length]!=0)
    {
        if ([nameTextField.text length]<=255 && [passwordTextField.text length]<=32 )
        {
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus internetStatus = [reachability currentReachabilityStatus];
            if (internetStatus == NotReachable)
            {
                [AlertView alert:[dict objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"LoginVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Login"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
            }
            else
            {
                [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
                NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
                
                [myQueue addOperationWithBlock:^{
                    
                    twResponseHeaderDic=[HttpHandler login:nameTextField.text twPassword:passwordTextField.text twTxId:@"Tx874874" twMoment:@"46746" userScreen:@"LoginVC"];
                    
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        if (![[[twResponseHeaderDic objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
                        {
                            if ([twResponseHeaderDic objectForKey:@"Error"] || [twResponseHeaderDic objectForKey:@"Server Error"])
                            {
                                if([twResponseHeaderDic objectForKey:@"Server Error"])
                                {
                                    [AlertView alert:[NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"Log in"],[dict objectForKey:@"Error"]] message:[NSString stringWithFormat:@"%@ (%@i)",[twResponseHeaderDic objectForKey:@"Server Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                                }
                                else
                                {
                                    [AlertView alert:[NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"Log in"],[dict objectForKey:@"Error"]] message:[NSString stringWithFormat:@"%@ (%@i)",[twResponseHeaderDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                                }
                            }
                            else
                            {
                                
                                int firstTimeLogin=[[[twResponseHeaderDic objectForKey:@"twmAppLogin"] objectForKey:@"firstTimeLogin"] intValue];
                                if (firstTimeLogin)
                                {
                                    NSMutableDictionary *dic=[NSMutableDictionary new];
                                    [dic setObject:nameTextField.text forKey:@"twUserName"];
                                    [dic setObject:passwordTextField.text forKey:@"twPassword"];
                                    [dic setObject:@"Tx874874" forKey:@"twTxId"];
                                    [dic setObject:@"46746.6877" forKey:@"twMoment"];
                                    if ([[[twResponseHeaderDic objectForKey:@"twmAppLogin"] objectForKey:@"userId"]isKindOfClass:[NSNull class]])
                                    {
                                        [dic setObject:@"create" forKey:@"userId"];
                                        
                                    }
                                    else
                                    {
                                        [dic setObject:[[twResponseHeaderDic objectForKey:@"twmAppLogin"] objectForKey:@"userId"] forKey:@"userId"];
                                        
                                    }
                                    LoginDetailViewController *loginDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginDetailViewController"];
                                    loginDetailVC.twResponseHeaderDic=dic;
                                    [self.navigationController pushViewController:loginDetailVC animated:YES];
                                }
                                else
                                {
                                    if ([[twResponseHeaderDic objectForKey:@"twmAppLogin"] isKindOfClass:[NSNull class]])
                                    {
                                        
                                    }
                                    else
                                    {
                                        [[NSUserDefaults standardUserDefaults] setObject:[twResponseHeaderDic objectForKey:@"twmAppLogin"] forKey:@"twmAppLogin"];
                                        [[NSUserDefaults standardUserDefaults] setObject:[[twResponseHeaderDic objectForKey:@"twmAppLogin"] objectForKey:@"companyId"] forKey:@"companyId"];
                                        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"loginstatus"];
                                        [[NSUserDefaults standardUserDefaults]synchronize];
                                    }
                                    
                                    //NSString *logInStr = [NSString stringWithFormat:@"%@",[[twResponseHeaderDic objectForKey:@"twmAppLogin"] objectForKey:@"roleId"]];
                                    
                                    [[NSUserDefaults standardUserDefaults] setObject:[[twResponseHeaderDic objectForKey:@"twmAppLogin"] objectForKey:@"roleId"] forKey:@"roleId"];
                                    [self featureToggle];
                                    
                                }
                                
                                
                            }
                            //[ActivityIndicatorView activityIndicatorViewRemove:self.view];
                        }
                        else
                        {
                            [AlertView alert:[NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"Log in"],[dict objectForKey:@"Error"]] message:[NSString stringWithFormat:@"%@ (%@i)",[[twResponseHeaderDic objectForKey:@"twResponseHeader"] objectForKey:@"twerrorMessage"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                            
                        }
                    }];
                }];
            }
        }
        else
        {
            if ([nameTextField.text length]>255)
            {
                [AlertView alert:[NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"Log in"],[dict objectForKey:@"Error"]] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"User id should not exceed 255 character"],[[DiagnosticTool sharedManager] generateScreenCode:@"LoginVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Login"],[[DiagnosticTool sharedManager]generateErrorCode:@"User id should not exceed 255 character"]]];
                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
            }
            else if ([passwordTextField.text length]>32)
            {
                [AlertView alert:[NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"Log in"],[dict objectForKey:@"Error"]] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:[dict objectForKey:@"Password length should not exceed 32 character"]],[[DiagnosticTool sharedManager] generateScreenCode:@"LoginVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Login"],[[DiagnosticTool sharedManager]generateErrorCode:@"Password length should not exceed 32 character"]]];
                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
            }
        }
    }
    else
    {
        [AlertView alert:[NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"Log in"],[dict objectForKey:@"Error"]] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Username or Password is empty"],[[DiagnosticTool sharedManager] generateScreenCode:@"LoginVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Login"],[[DiagnosticTool sharedManager]generateErrorCode:@"Username or Password is empty"]]];
        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
    }
}

-(void)featureToggle
{
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    
    NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
    [myQueueSales addOperationWithBlock:^{
        NSArray *featureToggleArray=[HttpHandler featureToggle:[loginDetails objectForKey:@"twUserName"] password:[loginDetails objectForKey:@"twPassword"] userScreen:@"LoginVC"];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            /*[
             {
             "state": 2,
             "name": "SwitchToCommunicator"
             },
             {
             "state": 0,
             "name": "CommunicationMode"
             },
             {
             "state": 1,
             "name": "PNSFilter"
             },
             {
             "state": 3,
             "name": "BluetoothSupport"
             }
             ]
             */
            if ([[featureToggleArray objectAtIndex:0]objectForKey:@"Error"])
            {
                
            }
            else
            {
                if (featureToggleArray.count>0)
                {
                    
                    
                    for (int i=0; i<[featureToggleArray count]; i++)
                    {
                        int stateint;
                        NSString *nameStr=[[featureToggleArray objectAtIndex:i] objectForKey:@"name"];
                        if ([nameStr isEqualToString:@"SwitchToCommunicator"])
                        {
                            stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                            
                            if (stateint==1)
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"SwitchtoC2"];
                            }
                            else
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"SwitchtoC2"];
                            }
                        }
                        else if ([nameStr isEqualToString:@"CommunicationMode"])
                        {
                            stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                            if (stateint==3)
                            {
                                //instore
                                
                                 AppDelegate *AppD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                 //AppDelegate *delegate=[[AppDelegate alloc]init];
                                 [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"UMA"];
                                 [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"Toggled"];
                                 [AppD checkNetworkStatus:nil];
                                
                            }
                            else if (stateint==0)
                            {
                                //HMA
                                
                                 AppDelegate *AppD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                 //AppDelegate *delegate=[[AppDelegate alloc]init];
                                 [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"UMA"];
                                 [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"Toggled"];
                                 [AppD checkNetworkStatus:nil];;
                                 
                            }
                        }
                        else if ([nameStr isEqualToString:@"PNSFilter"])
                        {
                            stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                            if (stateint==1)
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"NotificationFilter"];
                            }
                            else
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"NotificationFilter"];
                            }
                        }
                        else if ([nameStr isEqualToString:@"BluetoothSupport"])
                        {
                            stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                        }
                    }
                    
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"NotificationFilter"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"SwitchtoC2"];
                }
            }
            int roll_ID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"roleId"] intValue];
            if (roll_ID>10)
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"NONVP" forKey:@"VP"];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"logedin"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self performSegueWithIdentifier:@"VP" sender:self];
                
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"VP" forKey:@"VP"];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"logedin"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                StoreListViewController *storeListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StoreListViewController"];
                
                [self.navigationController pushViewController:storeListVC animated:YES];
                
            }
            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
        }];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"VP"])
    {
        [segue destinationViewController]; //---->VPViewController
    }
}

#pragma mark - app logfile send method
-(void)logFilesend
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        picker.mailComposeDelegate = self;
        NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
        NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
        [picker setSubject:[NSString stringWithFormat:@"iOS MA log Version %@",version]];
        NSString *mimeType = @"Text/zip";
        
        
        NSString *str=[NSString stringWithFormat:@"%@",[[NSLogger sharedInstance] getdata]];
        NSData *file=[NSData dataWithContentsOfFile:str];
        NSData *fileData =[str dataUsingEncoding:NSUTF8StringEncoding];
        float cacheSize =([fileData length]/1000);
        
        cacheSize = (cacheSize/1024);
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if(internetStatus == NotReachable)
        {
            //No internet
        }
        else if (internetStatus == ReachableViaWiFi)
        {
            //WiFi
            
            if (cacheSize>5)
            {
                
            }
        }
        else if (internetStatus == ReachableViaWWAN)
        {
            //3G
        }
        if (file.length!=0)
        {
            [picker addAttachmentData:file mimeType:mimeType fileName:@"iOS MA log"];
            NSString *emailBody = @"Please find the attached iOS MA log";
            [picker setMessageBody:emailBody isHTML:NO];
            [self presentViewController:picker animated:YES completion:nil];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:[dict objectForKey:@"Log file is empty"]
                                                           delegate:nil cancelButtonTitle:[dict objectForKey:@"OK"] otherButtonTitles:nil];
            [alert show];
        }
        
        
    }
}


#pragma mark - mail composer method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail Result: canceled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail Result: saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail Result: sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail Result: failed");
            break;
        default:
            NSLog(@"Mail Result: not sent");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
