//
//  LoginDetailViewController.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 16/01/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#import "LoginDetailViewController.h"
#import "HttpHandler.h"
#import "StoreListViewController.h"
#import "Reachability.h"
#import "Constant.h"
#import "AlertView.h"
#import "AudioManager.h"
#import "VPViewController.h"
#import "AppDelegate.h"
#import "DiagnosticTool.h"

@interface LoginDetailViewController ()
{
        NSDictionary *dict;
}

@end

@implementation LoginDetailViewController
@synthesize twResponseHeaderDic;

#pragma mark - keyboard disappear method
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
}
-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - textfield editing method
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField==passWord || textField==conformPassWord)
    {
        BOOL lowerCaseLetter = false,upperCaseLetter = false,digit = false,specialCharacter = 0;
        if([textField.text length] >= 8 && (textField==passWord || textField==conformPassWord))
        {
            for (int i = 0; i < [textField.text length]; i++)
            {
                unichar c = [textField.text characterAtIndex:i];
                if(!lowerCaseLetter)
                {
                    lowerCaseLetter = [[NSCharacterSet lowercaseLetterCharacterSet] characterIsMember:c];
                }
                if(!upperCaseLetter)
                {
                    upperCaseLetter = [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:c];
                }
                if(!digit)
                {
                    digit = [[NSCharacterSet decimalDigitCharacterSet] characterIsMember:c];
                }
                if(!specialCharacter)
                {
                    NSCharacterSet  *set= [NSCharacterSet symbolCharacterSet];
                    if ([textField.text rangeOfCharacterFromSet:[set invertedSet]].location != NSNotFound)
                    {
                        specialCharacter = YES;
                    }
                    
                }
            }
            
            if(specialCharacter && digit && lowerCaseLetter && upperCaseLetter)
            {
                //do what u want
            }
            else
            {
                [AlertView alert:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Please Ensure that you have at least one lower case letter, one upper case letter, one digit and one special character"],[[DiagnosticTool sharedManager] generateScreenCode:@"FirstTimeLoginVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"CreatePassWord"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please Ensure that you have at least one lower case letter, one upper case letter, one digit and one special character"]]];
            }
            
        }
        else
        {
            [AlertView alert:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Please Enter at least 8 characters password"],[[DiagnosticTool sharedManager] generateScreenCode:@"FirstTimeLoginVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"CreatePassWord"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please Enter at least 8 characters password"]]];
        }
    }
}

#pragma mark - createPassWord method
-(IBAction)createPassWordAction:(id)sender
{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dict objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"FirstTimeLoginVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"CreatePassWord"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        userName.text=[userName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        passWord.text=[passWord.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        conformPassWord.text=[conformPassWord.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if ([userName.text length]!=0 && [passWord.text length]!=0 && [conformPassWord.text length]!=0)
        {
            [self textFieldDidEndEditing:passWord];
            if ([passWord.text isEqualToString:conformPassWord.text])
            {
                NSDictionary *dic= [HttpHandler createPassWord:[twResponseHeaderDic objectForKey:@"twUserName"] twPassword:passWord.text twTxId:[twResponseHeaderDic objectForKey:@"twTxId"] twMoment:[twResponseHeaderDic objectForKey:@"twMoment"] twUserID:userName.text userScreen:@"FirstTimeLoginVC"];
                if (![[[dic objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
                {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PasswordChanged"];
                    [[NSUserDefaults standardUserDefaults]setObject:[dic objectForKey:@"twmAppLogin"] forKey:@"twmAppLogin"];
                    [[NSUserDefaults standardUserDefaults] setObject:[[dic objectForKey:@"twmAppLogin"] objectForKey:@"companyId"] forKey:@"companyId"];
                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"loginstatus"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    NSString *logInStr = [NSString stringWithFormat:@"%@",[[dic objectForKey:@"twmAppLogin"] objectForKey:@"roleId"]];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:logInStr forKey:@"roleId"];
                    
                    [self featureToggle];
                    
                }
                else
                {
                    [AlertView alert:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[[dic objectForKey:@"twResponseHeader"] objectForKey:@"twerrorMessage"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                    
                }
            }
            else
            {
                [AlertView alert:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"password and confirmpassword must be same"],[[DiagnosticTool sharedManager] generateScreenCode:@"FirstTimeLoginVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"CreatePassWord"],[[DiagnosticTool sharedManager]generateErrorCode:@"password and confirmpassword must be same"]]];
            }
        }
        else
        {
            [AlertView alert:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"name password and confirm password fields should not empty"],[[DiagnosticTool sharedManager] generateScreenCode:@"FirstTimeLoginVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"CreatePassWord"],[[DiagnosticTool sharedManager]generateErrorCode:@"name password and confirm password fields should not empty"]]];
        }
    }
}


-(void)featureToggle
{
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    
    NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
    [myQueueSales addOperationWithBlock:^{
        NSArray *featureToggleArray=[HttpHandler featureToggle:[loginDetails objectForKey:@"twUserName"] password:[loginDetails objectForKey:@"twPassword"] userScreen:@"FirstTimeLoginVC"];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            /*[
             {
             "state": 2,
             "name": "SwitchToCommunicator"
             },
             {
             "state": 0,
             "name": "CommunicationMode"
             },
             {
             "state": 1,
             "name": "PNSFilter"
             },
             {
             "state": 3,
             "name": "BluetoothSupport"
             }
             ]
             */
            if ([[featureToggleArray objectAtIndex:0]objectForKey:@"Error"])
            {
                
            }
            else
            {
                if (featureToggleArray.count>0)
                {
                    
                    for (int i=0; i<[featureToggleArray count]; i++)
                    {
                        int stateint;
                        NSString *nameStr=[[featureToggleArray objectAtIndex:i] objectForKey:@"name"];
                        if ([nameStr isEqualToString:@"SwitchToCommunicator"])
                        {
                            stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                            
                            if (stateint==1)
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"SwitchtoC2"];
                            }
                            else
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"SwitchtoC2"];
                            }
                        }
                        else if ([nameStr isEqualToString:@"CommunicationMode"])
                        {
                            stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                            if (stateint==3)
                            {
                                //instore
                                
                                 AppDelegate *AppD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                 //AppDelegate *delegate=[[AppDelegate alloc]init];
                                 [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"UMA"];
                                 [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"Toggled"];
                                 [AppD checkNetworkStatus:nil];
                                
                            }
                            else if (stateint==0)
                            {
                                //HMA
                                
                                 AppDelegate *AppD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                 //AppDelegate *delegate=[[AppDelegate alloc]init];
                                 [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"UMA"];
                                 [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"Toggled"];
                                 [AppD checkNetworkStatus:nil];;
                                 
                            }
                        }
                        else if ([nameStr isEqualToString:@"PNSFilter"])
                        {
                            stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                            if (stateint==1)
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"NotificationFilter"];
                            }
                            else
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"NotificationFilter"];
                            }
                        }
                        else if ([nameStr isEqualToString:@"BluetoothSupport"])
                        {
                            stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                        }
                    }
                    
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"NotificationFilter"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"SwitchtoC2"];
                }
            }
            int roll_ID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"roleId"] intValue];
            if (roll_ID>10)
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"NONVP" forKey:@"VP"];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"logedin"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self performSegueWithIdentifier:@"VP" sender:self];
                
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"VP" forKey:@"VP"];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"logedin"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                StoreListViewController *storeListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StoreListViewController"];
                
                [self.navigationController pushViewController:storeListVC animated:YES];
                
            }
        }];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"VP"])
    {
        [segue destinationViewController]; //---->VPViewController
    }
}

#pragma mark - view method
-(void)viewDidLoad
{
    dict =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    
    userName.placeholder=[dict objectForKey:@"Enter user name"];
    passWord.placeholder=[dict objectForKey:@"Enter new password"];
    conformPassWord.placeholder=[dict objectForKey:@"Confirm Password"];
    
    submitButton.layer.cornerRadius=7;
    [submitButton setTitle:[dict objectForKey:@"Submit"] forState:UIControlStateNormal];
    submitButton.backgroundColor=kNavBackgroundColor;
    NSLog(@"LogindetailView viewDidLoad");
    
    if ([[twResponseHeaderDic objectForKey:@"userId"] isEqualToString:@"create"])
    {
        
    }
    else
    {
        userName.text=[twResponseHeaderDic objectForKey:@"userId"];
        userName.userInteractionEnabled=NO;
    }
    [super viewDidLoad];
}


@end
