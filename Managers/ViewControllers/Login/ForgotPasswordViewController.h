//
//  ForgotPasswordViewController.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 16/01/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>
{
    IBOutlet UITextField *userNameTextField;
    IBOutlet UIButton *submitButton;
}
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
-(IBAction)submitAction:(id)sender;

@end
