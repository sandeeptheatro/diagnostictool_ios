//
//  ForgotPasswordViewController.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 16/01/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "HttpHandler.h"
#import "ViewController.h"
#import "Reachability.h"
#import "AlertView.h"
#import "Constant.h"
#import "ActivityIndicatorView.h"
#import "DiagnosticTool.h"

@interface ForgotPasswordViewController ()
{
        NSDictionary *dict;
}


@end

@implementation ForgotPasswordViewController

#pragma mark - keyboard disappear method
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - view method

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
    self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
    self.navigationController.navigationBar.hidden=NO;
    [super viewWillAppear:animated];
}
-(void)viewDidLoad
{
    NSLog(@"Forgotpassword view viewDidLoad");
    dict =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    userNameTextField.placeholder=[dict objectForKey:@"Email address"];
    [submitButton setTitle:[dict objectForKey:@"Submit"] forState:UIControlStateNormal];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dict objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    
    submitButton.layer.cornerRadius=7;
    submitButton.backgroundColor=kNavBackgroundColor;
    [super viewDidLoad];
    
}

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - password submit method
-(IBAction)submitAction:(id)sender
{
    
    userNameTextField.text=[userNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([userNameTextField.text length]!=0)
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
           [AlertView alert:[dict objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"ForgotPassWordVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"ForgotPassWord"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        }
        else
        {
            [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
            NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
            [myQueue addOperationWithBlock:^{
                NSDictionary *twResponseHeader=[HttpHandler forgotPassWord:userNameTextField.text twTxId:@"Tx874874" twMoment:@"46746.6877" userScreen:@"ForgotPasswordVC"];
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    if (![[[twResponseHeader objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]] && [[[twResponseHeader objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"] isEqualToString:@"Success"])
                    {
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PasswordChanged"];
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict objectForKey:@"Sucess"] message:[dict objectForKey:@"Please check your email and follow the instructions to reset your password."] delegate:self cancelButtonTitle:[dict objectForKey:@"OK"] otherButtonTitles:nil];
                        [alert show];
                        
                    }
                    else
                    {
                        [AlertView alert:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@)",[twResponseHeader objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                    }
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                }];
            }];
        }
    }
    else
    {
        [AlertView alert:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Email is empty"],[[DiagnosticTool sharedManager] generateScreenCode:@"ForgotPasswordVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"ForgotPassWord"],[[DiagnosticTool sharedManager]generateErrorCode:@"Email is empty"]]];
    }
}


#pragma mark - alert method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ViewController *loginDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:loginDetailVC animated:YES];
}


@end
