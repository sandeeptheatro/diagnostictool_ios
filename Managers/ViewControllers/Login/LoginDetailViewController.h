//
//  LoginDetailViewController.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 16/01/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginDetailViewController : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField *userName,*passWord,*conformPassWord;
    IBOutlet UIButton *submitButton;
}

@property(nonatomic,strong)NSDictionary *twResponseHeaderDic;
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
-(IBAction)createPassWordAction:(id)sender;

@end
