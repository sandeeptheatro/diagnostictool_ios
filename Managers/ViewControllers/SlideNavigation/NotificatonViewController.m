//
//  NotificatonViewController.m
//  ManagersMockup
//
//  Created by Ravi on 18/05/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import "NotificatonViewController.h"
#import "Constant.h"
#import "HttpHandler.h"

@interface NotificatonViewController ()
{
    NSMutableArray *stores,*filterStatus,*fetchMArray;
    NSDictionary *dict;
}

@end

@implementation NotificatonViewController

- (void)viewDidLoad
{
    dict=[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    
    lbl_Alert.marqueeType = MLContinuous;
    lbl_Alert.scrollDuration = 15.0;
    lbl_Alert.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    lbl_Alert.fadeLength = 0.0f;
    lbl_Alert.leadingBuffer = 0.0f;
    lbl_Alert.trailingBuffer = 10.0f;
    
    [btn_Reset setTitle:[dict objectForKey:@"Reset Password"] forState:UIControlStateNormal];
    lbl_Alert.text=[dict objectForKey:@"Alert Filtering"];
    
    filterStatus=[[NSMutableArray alloc] init];
    
    filterStatus=[NSMutableArray arrayWithObjects:@"0",@"1",@"1",@"0", nil];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dict objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    
    stores=[[NSMutableArray alloc] init];
    
    stores =[NSMutableArray arrayWithObjects:[dict objectForKey:@"Announcements"],[dict objectForKey:@"Group Messages"],[dict objectForKey:@"Everyone Messages"],[dict objectForKey:@"Private Messages"],nil];
    self.navigationItem.title=[dict objectForKey:@"Settings"];
    // Do any additional setup after loading the view.
}

-(void)forceLogOut:(NSNotification *)notification
{
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    [super viewWillAppear:animated];
   // fetchMArray=[[NSMutableArray alloc] init];
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    
    NSString *notif=[[NSUserDefaults standardUserDefaults] objectForKey:@"NotificationFilter"];
    if ([notif isEqualToString:@"YES"])
    {
        notificationTableview.hidden=NO;
        lbl_Alert.hidden=NO;
    fetchMArray=[[HttpHandler notificationFetch:[loginDetails objectForKey:@"twUserName"] password:[loginDetails objectForKey:@"twPassword"] userScreen:@"NotificationsVC"]  mutableCopy];
    if ([[fetchMArray objectAtIndex:0] objectForKey:@"Error"] || [fetchMArray count]==0)
    {
        fetchMArray=nil;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[dict objectForKey:@"Responce is empty"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:nil cancelButtonTitle:[dict objectForKey:@"OK"] otherButtonTitles:nil, nil];
        
        [alert show];
    }
    else
    {
        
    }
    }
    else
    {
        notificationTableview.hidden=YES;
        lbl_Alert.hidden=YES;
    }
}

-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ( [fromPush1 isEqualToString:@"YES"])
    {
        NSArray *viewcontrollers=[self.navigationController viewControllers];
        for (int i=0;i<[viewcontrollers count];i++)
        {
            UIViewController *vc=[viewcontrollers objectAtIndex:i];
            NSString *currentView=NSStringFromClass([vc class]);
            if ([currentView isEqualToString:@"VPViewController"])
            {
                [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
            }
        }
        
    }
    
    
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(IBAction)changePassWord:(id)sender
{
   // [self performSegueWithIdentifier:@"change" sender:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [fetchMArray count];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UILabel *messageTypeLbl=[[UILabel alloc] init];
    UILabel *switchOff=[[UILabel alloc] init];
    UILabel *switchOn=[[UILabel alloc] init];
    UISwitch *switchview = [[UISwitch alloc] initWithFrame:CGRectZero];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //add a switch
        
        //cell.accessoryView = switchview;
        
        [switchview setOnTintColor:kNavBackgroundColor];
        messageTypeLbl.frame=CGRectMake(10, 10, 180, 30);
        switchOff.frame=CGRectMake(self.view.frame.size.width-115, 10, 30, 30);
        switchview.frame=CGRectMake(self.view.frame.size.width-85, 10, 30, 30);
        switchOn.frame=CGRectMake(self.view.frame.size.width-30, 10, 30, 30);
        //messageTypeLbl.backgroundColor=[UIColor redColor];
        [cell.contentView addSubview:messageTypeLbl];
        [cell.contentView addSubview:switchOn];
        [cell.contentView addSubview:switchOff];
        [cell.contentView addSubview:switchview];
    }
    
    NSString *status =[NSString stringWithFormat:@"%@",[[fetchMArray objectAtIndex:indexPath.row] objectForKey:@"enable"]] ;
    
    if ([status isEqualToString:@"1"])
    {
        [switchview setOn:YES animated:YES];
    }
    else
    {
        [switchview setOn:NO animated:NO];
    }
//    switchOff.text = [dict objectForKey:@"Off"];
//    switchOn.text = [dict objectForKey:@"On"];
    
    switchOff.text =@"";
    switchOn.text = @"";
    switchview.tag=indexPath.row;
    [switchview addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    NSString *messageType=[[fetchMArray objectAtIndex:indexPath.row] objectForKey:@"messageType"];
    
    if ([messageType isEqualToString:@"announcements"])
    {
        messageTypeLbl.text = [dict objectForKey:@"Announcements"];
    }
    else if([messageType isEqualToString:@"groupMessages"])
    {
        messageTypeLbl.text = [dict objectForKey:@"Group Messages"];
    }
    else if ([messageType isEqualToString:@"everyOneMessages"])
    {
        messageTypeLbl.text = [dict objectForKey:@"Everyone Messages"];
    }
    else if ([messageType isEqualToString:@"privateMessages"])
    {
        messageTypeLbl.text = [dict objectForKey:@"Private Messages"];
    }
    
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(void)switchChanged:(UISwitch *)buttonIndex
{
    NSInteger i =0;
    i=buttonIndex.tag;
    
    /*
     [{
     "messageType": "announcements",
     "enable": 1, //Enabled
     "storeIds": []
     },
     {
     "messageType": "groupMessages",
     "enable": 0, //Disabled
     "storeIds": [1, 2, 3]
     },
     {
     "messageType": "everyOneMessages",
     "enable": 0,
     "storeIds": [1, 2, 3]
     },
     {
     "messageType": "privateMessages",
     "enable": 0,
     "storeIds": [1, 2, 3]
     }
     ]
     */
    
    if(fetchMArray.count>0)
    {
    if ([[[fetchMArray objectAtIndex:i] objectForKey:@"enable"] intValue]==1)
    {
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[fetchMArray objectAtIndex:i];
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:[NSNumber numberWithInt:0] forKey:@"enable"];
        [fetchMArray replaceObjectAtIndex:i withObject:newDict];

    }
    else
    {
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[fetchMArray objectAtIndex:i];
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:[NSNumber numberWithInt:1] forKey:@"enable"];
        [fetchMArray replaceObjectAtIndex:i withObject:newDict];
    }
    
    NSDictionary *loginDetails1 =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    
    
    
    //[self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
    NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
    [myQueueSales addOperationWithBlock:^{
        NSDictionary *responsDic=[HttpHandler NotificationSave:[loginDetails1 objectForKey:@"twUserName"] password:[loginDetails1 objectForKey:@"twPassword"] jSonStr:fetchMArray userScreen:@"NotificationsVC"];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if ([responsDic objectForKey:@"Error"])
            {
                if ([[[fetchMArray objectAtIndex:i] objectForKey:@"enable"] intValue]==1)
                {
                    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                    NSDictionary *oldDict = (NSDictionary *)[fetchMArray objectAtIndex:i];
                    [newDict addEntriesFromDictionary:oldDict];
                    [newDict setObject:[NSNumber numberWithInt:0] forKey:@"enable"];
                    [fetchMArray replaceObjectAtIndex:i withObject:newDict];
                    
                }
                else
                {
                    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                    NSDictionary *oldDict = (NSDictionary *)[fetchMArray objectAtIndex:i];
                    [newDict addEntriesFromDictionary:oldDict];
                    [newDict setObject:[NSNumber numberWithInt:1] forKey:@"enable"];
                    [fetchMArray replaceObjectAtIndex:i withObject:newDict];
                }
                NSString *status =[NSString stringWithFormat:@"%@",[[fetchMArray objectAtIndex:i] objectForKey:@"enable"]] ;
                
                if ([status isEqualToString:@"1"])
                {
                    [buttonIndex setOn:YES animated:YES];
                }
                else
                {
                    [buttonIndex setOn:NO animated:NO];
                }
                //[notificationTableview reloadData];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:nil cancelButtonTitle:[dict objectForKey:@"OK"] otherButtonTitles:nil, nil];
                
                [alert show];
            }
        }];
    }];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - Navigation

//In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    [segue destinationViewController];
}


@end
