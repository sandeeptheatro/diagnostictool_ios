//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "ViewController.h"
#import "Constant.h"
#import "HttpHandler.h"
#import "AudioManager.h"
#import "EarboxDataStore.h"
#import "UAirship.h"
#import "UAConfig.h"
#import "UAPush.h"
#import "StoreListViewController.h"
#import "Reachability.h"
#import "AlertView.h"
#import "NSLogger.h"
#import "NotificatonViewController.h"
#import "ChangePasswordViewController.h"
#import "VPViewController.h"
#import "C2ScanningViewController.h"
#import "DiagnosticTool.h"

@implementation LeftMenuViewController
{
        NSDictionary *dict;
}

#pragma mark - UIViewController Methods -


static  LeftMenuViewController *sharedMgr = nil;


+(LeftMenuViewController *)getsharedInstance {
    if (sharedMgr == nil)
    {
        sharedMgr = [[LeftMenuViewController alloc] init];
    }
    return sharedMgr;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = YES;
    
    return [super initWithCoder:aDecoder];
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
}

- (void)viewDidLoad
{
    
    
    dict =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dict objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    
    NSLog(@"Slidemenu View viewDidLoad");
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lefttablereload) name:@"lefttablereload" object:nil];
    logoutbutton.backgroundColor=kNavBackgroundColor;
    self.tableView.scrollEnabled=NO;
}


//Camera code

-(void)profileimageTapped
{
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:@"Profile"
                                 message:@"Choose image from"
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:[dict objectForKey:@"OK"]
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             //[view dismissViewControllerAnimated:YES completion:nil];
                             [self useCamera];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 //[view dismissViewControllerAnimated:YES completion:nil];
                                 [self useCameraRoll];
                                 
                             }];
    
    
    [view addAction:ok];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            [self useCamera];
            break;
        case 1:
            [self useCameraRoll];
            break;
            
    }
}

- (void) useCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
        
        newMedia = YES;
    }
}


- (void) useCameraRoll
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = NO;
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    image = [info
             objectForKey:UIImagePickerControllerOriginalImage];
    [profileimage setImage:image forState:UIControlStateNormal];
    if (newMedia)
        UIImageWriteToSavedPhotosAlbum(image,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
    
}
-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"\
                              delegate: nil
                              cancelButtonTitle:[dict objectForKey:@"OK"]
                              otherButtonTitles:nil];
        [alert show];
    }
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(void)lefttablereload
{
    [self.tableView reloadData];
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count;
    NSString *switchToc2=[[NSUserDefaults standardUserDefaults] objectForKey:@"SwitchtoC2"];
    
    if ([switchToc2 isEqualToString:@"YES"])
    {
        count=5;
    }
    else
    {
        count=4;
    }
    return count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 130)];
    view.backgroundColor = kNavBackgroundColor;
    profileimage=[UIButton buttonWithType:UIButtonTypeCustom];
    profileimage.frame=CGRectMake((self.tableView.frame.size.width-155)/2, 25,81, 80);
    profileimage.layer.cornerRadius = profileimage.frame.size.width / 2;
    profileimage.clipsToBounds = YES;
    //[profileimage addTarget:self action:@selector(profileimageTapped) forControlEvents:UIControlEventTouchUpInside];
    [profileimage setImage:[UIImage imageNamed:@"user"] forState:UIControlStateNormal];
    [view addSubview:profileimage];
    UILabel *profilename=[[UILabel alloc]initWithFrame:CGRectMake((self.tableView.frame.size.width-260)/2, 110,190, 20)];
    profilename.textAlignment=NSTextAlignmentCenter;
    profilename.text=[[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"]capitalizedString];
    profilename.textColor=[UIColor whiteColor];
    [view addSubview:profilename];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 150;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    dict =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    
    static NSString *cellIdentifier=@"cell";
    
    UITableViewCell *cell;
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:cellIdentifier];
        logoutbutton=[UIButton buttonWithType:UIButtonTypeCustom];
        logoutbutton.frame=CGRectMake((self.tableView.frame.size.width-200)/2, 10,140, 40);
        logoutbutton.layer.cornerRadius=8;
        logoutbutton.backgroundColor=kNavBackgroundColor;
        [logoutbutton addTarget:self action:@selector(logOutbuttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [logoutbutton addTarget:self action:@selector(logOutbuttoncolor:) forControlEvents:UIControlEventTouchDown];
        [logoutbutton setTitle:[dict objectForKey:@"Log out"] forState:UIControlStateNormal];
        
    }
    cell.backgroundColor=[UIColor lightGrayColor];
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
    NSString *switchToc2=[[NSUserDefaults standardUserDefaults] objectForKey:@"SwitchtoC2"];
    
    if ([switchToc2 isEqualToString:@"YES"])
    {
    switch (indexPath.row)
    {
            
        case 0:
            cell.textLabel.text =[dict objectForKey:@"Home"] ;
            break;
        case 1:
            cell.textLabel.text = [dict objectForKey:@"Switch to C2"];
            break;
        case 2:
            cell.textLabel.text =[dict objectForKey:@"Settings"] ;
            break;
        case 3:
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"Version"],version];
            break;
            
        case 4:
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell.contentView addSubview:logoutbutton];
            break;
    }
    }
    else
    {
        switch (indexPath.row)
        {
                
            case 0:
                cell.textLabel.text =[dict objectForKey:@"Home"];
                break;
            
            case 1:
                cell.textLabel.text =[dict objectForKey:@"Settings"];
                break;
            case 2:
                
                cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"Version"],version];
                break;
            case 3:
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                [cell.contentView addSubview:logoutbutton];
                break;
        }
    }
    
    return cell;
}

-(void)logOutbuttoncolor:(UIButton *)sender
{
    sender.backgroundColor=kLightcolor;
}


-(void)forceLogOut:(NSNotification *)notification
{
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)logOutbuttonAction:(UIButton *)sender
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dict objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"SettingsVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"UserLogout"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        NSString *str=[NSString stringWithFormat:@"%@",[[NSLogger sharedInstance] getdata]];
        NSData *file=[NSData dataWithContentsOfFile:str];
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            
            NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
            
            NSDictionary *twResponseHeader= [HttpHandler userlogout:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] userScreen:@"SettingsVC"];
            if (file.length>0)
            {
            [HttpHandler uploadlogfiles:file userScreen:@"SettingsVC"];
            }
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                if (![[[twResponseHeader objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
                {
                    [[NSUserDefaults standardUserDefaults]setObject:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] forKey:@"logoutname"];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"earbox"];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"firstTimeStore"];
                     [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"logedin"];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"earboxv2"];
                    //[[NSUserDefaults standardUserDefaults]removeObjectForKey:@"twmAppLogin"];
                    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"inboxpreviousTab"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"badgecount"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"notificationtype"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"storeNameString"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"chainName"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"privateMessageCountdef"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"storeAnnouncementCountdef"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"groupMessageCountdef"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"everyoneMessageCountdef"];
                    //Database delete
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"EarboxInbox"];
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"EarboxSaved"];
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"EarboxSent"];
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"AnnouncementInbox"];
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"AnnouncementSaved"];
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"AnnouncementSent"];
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"Favorites"];
                    
                   // [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
                    
                    //Audio files deleted
                    [[AudioManager getsharedInstance]deleteallEarboxAudioPath];
                    
                    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
                    [[UIApplication sharedApplication]cancelAllLocalNotifications];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"messagecount"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"announcementcount"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"messagetotalcount"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"announcementtotalcount"];
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                             bundle: nil];
                    ViewController *loginVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ViewController"];
                    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:loginVC
                                                                             withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                                     andCompletion:nil];
                    sender.backgroundColor=kNavBackgroundColor;
                    [[StoreListViewController getsharedInstance].deletetimer invalidate];
                    
                    [StoreListViewController getsharedInstance].deletetimer=nil;
                    
                    [[VPViewController getsharedInstance].deletetimer invalidate];
                    
                    [VPViewController getsharedInstance].deletetimer=nil;
                    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginstatus"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                }
                else
                {
                    
                }
                
            }];
        }];
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    logoutbutton.backgroundColor=kNavBackgroundColor;
    
    NSString *switchToc2=[[NSUserDefaults standardUserDefaults] objectForKey:@"SwitchtoC2"];
    
    if ([switchToc2 isEqualToString:@"YES"])
    {
    switch (indexPath.row)
    {
        case 0:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"leftMenuSelectedNotification" object:@"closed"];
            [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
            
            break;
        case 1:
        {
            /* [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
             NotificatonViewController *contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificatonViewController"];
             // contactsController.storeNameString=[searchResults objectAtIndex:indexPath.row];
             [[SlideNavigationController sharedInstance] pushViewController:contactsController animated:YES];*/
            [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
            C2ScanningViewController *contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"C2ScanningViewController"];
            [[SlideNavigationController sharedInstance] pushViewController:contactsController animated:YES];
            
        }
            break;
        case 2:
        {
            /*[[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
            ChangePasswordViewController *contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
            // contactsController.storeNameString=[searchResults objectAtIndex:indexPath.row];
            [[SlideNavigationController sharedInstance] pushViewController:contactsController animated:YES];*/
            
            [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
            NotificatonViewController *contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificatonViewController"];
            // contactsController.storeNameString=[searchResults objectAtIndex:indexPath.row];
            [[SlideNavigationController sharedInstance] pushViewController:contactsController animated:YES];

        }
    }
    }
    else
    {
        switch (indexPath.row)
        {
            case 0:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"leftMenuSelectedNotification" object:@"closed"];
                [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
                
                break;
            case 1:
            {
                /*[[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
                 ChangePasswordViewController *contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
                 // contactsController.storeNameString=[searchResults objectAtIndex:indexPath.row];
                 [[SlideNavigationController sharedInstance] pushViewController:contactsController animated:YES];*/
                
                [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
                NotificatonViewController *contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificatonViewController"];
                // contactsController.storeNameString=[searchResults objectAtIndex:indexPath.row];
                [[SlideNavigationController sharedInstance] pushViewController:contactsController animated:YES];
                
            }
        }
    }
    
}





@end
