//
//  ChangePasswordViewController.h
//  ManagersMockup
//
//  Created by Ravi on 18/05/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"


@interface ChangePasswordViewController : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField *txt_OldPassword,*txt_NewPassword,*txt_ConfirmPassword;
    IBOutlet UIButton *btn_ChangePassword;
     IBOutlet UILabel *lbl_Green,*lbl_Old,*lbl_New;
    IBOutlet MarqueeLabel *lbl_Confirm;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
-(IBAction)changePassword:(id)sender;
@end
