//
//  NotificatonViewController.h
//  ManagersMockup
//
//  Created by Ravi on 18/05/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"

@interface NotificatonViewController : UIViewController
{
    IBOutlet UILabel *lbl_Green;
    IBOutlet MarqueeLabel *lbl_Alert;
    IBOutlet UIButton *btn_Reset;
    IBOutlet UITableView *notificationTableview;
}
-(IBAction)changePassWord:(id)sender;
@end
