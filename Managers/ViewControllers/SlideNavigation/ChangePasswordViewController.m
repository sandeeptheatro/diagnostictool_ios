//
//  ChangePasswordViewController.m
//  ManagersMockup
//
//  Created by Ravi on 18/05/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "Reachability.h"
#import "AlertView.h"
#import "Constant.h"
#import "HttpHandler.h"
#import "DiagnosticTool.h"

@interface ChangePasswordViewController ()
{
    NSDictionary *dict;
}

@end

@implementation ChangePasswordViewController


-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    
    dict=[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    
    lbl_Confirm.marqueeType = MLContinuous;
    lbl_Confirm.scrollDuration = 15.0;
    lbl_Confirm.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    lbl_Confirm.fadeLength = 0.0f;
    lbl_Confirm.leadingBuffer = 0.0f;
    lbl_Confirm.trailingBuffer = 10.0f;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dict objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    
    lbl_New.text=[dict objectForKey:@"New Password"];
    txt_NewPassword.placeholder=[dict objectForKey:@"Please enter new password"];
    
    lbl_Old.text=[dict objectForKey:@"Old Password"];
    txt_OldPassword.placeholder=[dict objectForKey:@"Please enter old password"];
    
    lbl_Confirm.text=[dict objectForKey:@"Confirm Password"];
    txt_ConfirmPassword.placeholder=[dict objectForKey:@"Please confirm the passowrd"];
    
    [btn_ChangePassword setTitle:[dict objectForKey:@"Change Password"] forState:UIControlStateNormal];
    
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    
    [super viewDidLoad];
    btn_ChangePassword.layer.masksToBounds=YES;
    btn_ChangePassword.layer.cornerRadius=8;
    self.navigationItem.title=[dict objectForKey:@"Change Password"];
    // Do any additional setup after loading the view.
}

-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ( [fromPush1 isEqualToString:@"YES"])
    {
        
        NSArray *viewcontrollers=[self.navigationController viewControllers];
        for (int i=0;i<[viewcontrollers count];i++)
        {
            UIViewController *vc=[viewcontrollers objectAtIndex:i];
            NSString *currentView=NSStringFromClass([vc class]);
            if ([currentView isEqualToString:@"VPViewController"])
            {
                [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
            }
        }
        
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    [super viewWillAppear:animated];
}
-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}
#pragma mark - changePassword method
-(IBAction)changePassword:(id)sender
{
    
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dict objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"ChangePasswordVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"ChangePassword"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        txt_OldPassword.text=[txt_OldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        txt_NewPassword.text=[txt_NewPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        txt_ConfirmPassword.text=[txt_ConfirmPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if ([txt_OldPassword.text length]!=0 && [txt_NewPassword.text length]!=0 && [txt_ConfirmPassword.text length]!=0)
        {
            //[self textFieldDidEndEditing:txt_NewPassword];
            if ([txt_NewPassword.text isEqualToString:txt_ConfirmPassword.text])
            {
                NSDictionary *twResponseHeaderDic=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
                NSDictionary *dic= [HttpHandler changePassword:[twResponseHeaderDic objectForKey:@"twUserName"] twPassword: [twResponseHeaderDic objectForKey:@"twPassword"] twTxId:[twResponseHeaderDic objectForKey:@"twTxId"] twMoment:[twResponseHeaderDic objectForKey:@"twMoment"] newPassword:txt_NewPassword.text currentPassword:txt_OldPassword.text userScreen:@"ChangePasswordVC"] ;
                if ([[[dic objectForKey:@"twResponseHeader"] objectForKey:@"twInternetStatus"] intValue]==200)
                {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PasswordChanged"];
                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"loginstatus"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict objectForKey:@"Password Changed"] message:[dict objectForKey:@"Password has been successfully changed and Please Log-In"] delegate:self cancelButtonTitle:nil otherButtonTitles:[dict objectForKey:@"OK"], nil];
                    alert.tag=100;
                    [alert show];
                    
                }
                else
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[[dic objectForKey:@"twResponseHeader"] objectForKey:@"twerrorMessage"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dict objectForKey:@"OK"], nil];
                    [alert show];
                }
            }
            else
            {
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"new password and confirm password must be same"],[[DiagnosticTool sharedManager] generateScreenCode:@"ChangePasswordVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"ChangePassword"],[[DiagnosticTool sharedManager]generateErrorCode:@"new password and confirm password must be same"]] delegate:nil cancelButtonTitle:nil otherButtonTitles:[dict objectForKey:@"OK"], nil];
                [alert show];
            }
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"old password and new password and confirm password fields should not empty"],[[DiagnosticTool sharedManager] generateScreenCode:@"ChangePasswordVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"ChangePassword"],[[DiagnosticTool sharedManager]generateErrorCode:@"old password and new password and confirm password fields should not empty"]] delegate:nil cancelButtonTitle:nil otherButtonTitles:[dict objectForKey:@"OK"], nil];
            [alert show];
        }
    }
}


#pragma mark - keyboard disappear method
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==100)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
    [txt_NewPassword becomeFirstResponder];
    txt_ConfirmPassword.text=@"";
    txt_NewPassword.text=@"";
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
