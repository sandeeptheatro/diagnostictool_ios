//
//  PlacesViewController.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 12/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "StoreListViewController.h"
#import "PlacesTableViewCell.h"
#import "ContactsViewController.h"
#import "Constant.h"
#import "HttpHandler.h"
#import "StoreList.h"
#import "ViewController.h"
#import "Reachability.h"
#import "Constant.h"
#import "AlertView.h"
#import "UIViewController.h"
#import "LeftMenuViewController.h"
#import <sys/utsname.h>
#import "EarboxDataStore.h"
#import "NSDateClass.h"
#import "EarboxInbox.h"
#import "AnnouncementInbox.h"
#import "StateMachineHandler.h"
#import "AudioManager.h"
#import "NSLogger.h"
#import "ActivityIndicatorView.h"
#import "InstoreData.h"
#import "AppDelegate.h"
#import "DiagnosticTool.h"

@interface StoreListViewController ()
{
    NSMutableArray *storesArray,*searchResultsAll;
    NSDictionary *loginDetails;
    StoreList *storeList;
    UIView *alphaView;
    NSArray *storeListArray;
    NSTimer *storeCountUpdatetimer;
    int totalbadgecount;
        NSDictionary *dictLang;
}

@end

@implementation StoreListViewController
@synthesize loggedoff,logonFailed,deletetimer;


static StoreListViewController  *sharedMgr = nil;

+(StoreListViewController *)getsharedInstance {
    if (sharedMgr == nil)
    {
        sharedMgr = [[StoreListViewController alloc] init];
    }
    return sharedMgr;
}

#pragma  mark - menu item methods
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    NSString *str =[[NSUserDefaults standardUserDefaults] objectForKey:@"roleId"];
    int rollID=0;
    rollID = [str intValue];
    if (rollID>10)
    {
        return NO;
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"lefttablereload" object:nil];
        return YES;
    }
    return 0;
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
    alphaView.hidden=YES;
    [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
    //Code to handle the gesture
}

-(void)leftMenuSelected:(NSNotification *)notification
{
    if ([notification.object isEqualToString:@"open"])
    {
        alphaView.hidden=NO;
    }
    else
    {
        alphaView.hidden=YES;
    }
    
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)pushToContactsList
{
    
    NSString *storeName = [[NSUserDefaults standardUserDefaults] objectForKey:@"StoreNameForPush"];
    NSInteger storeId = 0;
    
    ContactsViewController *contactsController;
    
    contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactsViewController"];
    if (storesArray.count>0)
    {
        for (int i=0; i<[storesArray count]; i++)
        {
            storeList = [storesArray objectAtIndex:i];
            if ([storeName isEqualToString:storeList.name])
            {
                storeId = storeList.storeId;
                [[NSUserDefaults standardUserDefaults] setObject:storeName forKey:@"storeNameString"];
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:storeId] forKey:@"storeid"];
                [self.navigationController pushViewController:contactsController animated:YES];
                break;
            }
        }
    }
    else
    {
        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
        NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
        if ([currentView isEqualToString:@"StoreListViewController"])
        {
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus internetStatus = [reachability currentReachabilityStatus];
            if (internetStatus == NotReachable)
            {
                [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"StoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"StoreList"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
                
            }
            else
            {
                [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
                NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
                [myQueue addOperationWithBlock:^{
                    
                    loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
                    
                    NSDictionary *twResponseHeader= [HttpHandler storeList:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] userScreen:@"StoresVC"];
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        
                        if (![[[twResponseHeader objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
                        {
                            if ([twResponseHeader objectForKey:@"Error"])
                            {
                                [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[twResponseHeader objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                            }
                            else
                            {
                                totalbadgecount=0;
                                storesArray=[[NSMutableArray alloc]init];
                                storeListArray=[twResponseHeader objectForKey:@"storeList"];
                                for (int i=0;i<[storeListArray count];i++)
                                {
                                    storeList=[[StoreList alloc]init];
                                    
                                    if ([[[storeListArray objectAtIndex:i] objectForKey:@"name"] length]>0)
                                    {
                                        storeList.name=[[storeListArray objectAtIndex:i] objectForKey:@"name"];
                                        storeList.storeId=[[[storeListArray objectAtIndex:i] objectForKey:@"storeId"] intValue];
                                        storeList.messageCount=[[[storeListArray objectAtIndex:i] objectForKey:@"messageCount"] intValue];
                                        storeList.announcementCount=[[[storeListArray objectAtIndex:i] objectForKey:@"announcementCount"] intValue];
                                        storeList.totalCount=[[[storeListArray objectAtIndex:i] objectForKey:@"totalCount"] intValue];
                                        totalbadgecount=totalbadgecount+storeList.totalCount;
                                    }
                                    
                                    
                                    [storesArray addObject:storeList];
                                    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                                    [storesArray sortUsingDescriptors:[NSArray arrayWithObject:sort]];
                                }
                                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:totalbadgecount];
                                [storesTableview reloadData];
                                [self pushToContactsList];
                            }
                        }
                        else
                        {
                            NSString *twerrorMessage=[twResponseHeader objectForKey:@"twerrorMessage"];
                            [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",twerrorMessage,[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                        }
                        
                    }];
                }];
            }
        }
        
    }
    
    
}

-(void)storecountnotificationupdate:(NSNotification *)sender
{
    NSString *storeName = [[NSUserDefaults standardUserDefaults] objectForKey:@"multistoreNameString"];
    if (storesArray.count>0)
    {
        if ([sender.object isEqualToString:@"msgincrement"] || [sender.object isEqualToString:@"anncincrement"])
        {
            for (int i=0; i<[storesArray count]; i++)
            {
                storeList=[[StoreList alloc]init];
                storeList = [storesArray objectAtIndex:i];
                if ([storeName isEqualToString:storeList.name])
                {
                    storeList.name=storeList.name;
                    storeList.storeId=storeList.storeId;
                    if ([sender.object isEqualToString:@"msgincrement"])
                    {
                        storeList.messageCount=storeList.messageCount+1;
                        storeList.announcementCount=storeList.announcementCount;
                    }
                    else
                    {
                        storeList.announcementCount=storeList.announcementCount+1;
                        storeList.messageCount=storeList.messageCount;
                    }
                    
                    
                    storeList.totalCount=storeList.totalCount+1;
                    [storesArray replaceObjectAtIndex:i withObject:storeList];
                    [storesTableview reloadData];
                }
            }
            
        }
    }
    
}



#pragma mark - view methods
-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version =[infoDict objectForKey:@"CFBundleVersion"];
    if ([version isEqualToString:@"4.6"]&&[[[NSUserDefaults standardUserDefaults]objectForKey:@"appversioncheck"] isEqualToString:@"NO"])
    {
   
       // [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"appversioncheck"];
    }
    else
    {
        //Database delete
        [[EarboxDataStore sharedStore]deleteAllEntities:@"EarboxInbox"];
        [[EarboxDataStore sharedStore]deleteAllEntities:@"EarboxSaved"];
        [[EarboxDataStore sharedStore]deleteAllEntities:@"EarboxSent"];
        [[EarboxDataStore sharedStore]deleteAllEntities:@"AnnouncementInbox"];
        [[EarboxDataStore sharedStore]deleteAllEntities:@"AnnouncementSaved"];
        [[EarboxDataStore sharedStore]deleteAllEntities:@"AnnouncementSent"];
        [[EarboxDataStore sharedStore]deleteAllEntities:@"Favorites"];
        
        //Audio files deleted
        [[AudioManager getsharedInstance]deleteallEarboxAudioPath];
        [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"appversioncheck"];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
    NSLog(@"StorelistView viewwillappear");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(storecountnotificationupdate:) name:@"pushnotificationstorecount" object:nil];
    totalbadgecount=0;
    storeCountUpdatetimer= [NSTimer scheduledTimerWithTimeInterval: 60.0 target: self selector: @selector(storeCountUpdate) userInfo: nil repeats: YES];
    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
    
    NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush isEqualToString:@"YES"])
        {
            [self pushToContactsList];
        }
        else
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"VPViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        
    }
    
    else
    {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(leftMenuSelected:) name:@"leftMenuSelectedNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loaddata) name:UIApplicationDidBecomeActiveNotification object:nil];
        [self loaddata];
    }
    [super viewWillAppear:animated];
}

-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush isEqualToString:@"YES"])
        {
            [self pushToContactsList];
        }
        else
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"VPViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [storeCountUpdatetimer invalidate];
    storeCountUpdatetimer = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"leftMenuSelectedNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushnotificationstorecount" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [super viewWillDisappear:animated];
}

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)storeCountUpdate
{
    [self performSelectorOnMainThread:@selector(loaddata) withObject:nil waitUntilDone:NO];
}

#pragma mark - TTL time expire method
-(void)updateExpiryTime
{
    
    [self performSelectorOnMainThread:@selector(updatetimer) withObject:nil waitUntilDone:NO];
}

-(void)updatetimer
{
    if ([storesArray count]>0)
    {
        for (int i=0; i<[storesArray count]; i++)
        {
            storeList=[storesArray objectAtIndex:i];
            if (storeList.storeId !=0)
            {
                
                NSArray *messageArray= [[EarboxDataStore sharedStore] artists:@"EarboxInbox" storeid:storeList.storeId calltype:4 ];
                for (int i=0; i<[messageArray count]; i++)
                {
                    EarboxInbox *messageinboxdata=[messageArray objectAtIndex:i];
                    if ([[NSDateClass ttlformate:messageinboxdata.ttl] isEqualToString:@"0"]) //&& [messageinboxdata.status isEqualToString:@"HEARD"])
                    {
                        [[StateMachineHandler sharedManager]sendMsgActionToNative:2 txnid:[NSString stringWithFormat:@"%@", messageinboxdata.callId]];
                        [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@", messageinboxdata.callId]];
                        
                        if ([messageinboxdata.status isEqualToString:@"new"])
                        {
                            int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
                            if (messagecount==0)
                            {
                                messagecount=0;
                            }
                            else
                            {
                                messagecount--;
                            }
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagecount"];
                            
                        }
                        [[EarboxDataStore sharedStore] deleteEarboxInbox:messageinboxdata];
                        
                        
                        
                    }
                }
                NSArray *groupmessageArray= [[EarboxDataStore sharedStore] artists:@"EarboxInbox" storeid:storeList.storeId calltype:17];
                for (int i=0; i<[groupmessageArray count]; i++)
                {
                    EarboxInbox *messageinboxdata=[groupmessageArray objectAtIndex:i];
                    if ([[NSDateClass ttlformate:messageinboxdata.ttl] isEqualToString:@"0"]) //&& [messageinboxdata.status isEqualToString:@"HEARD"])
                    {
                        [[StateMachineHandler sharedManager]sendMsgActionToNative:2 txnid:[NSString stringWithFormat:@"%@", messageinboxdata.callId]];
                        [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@", messageinboxdata.callId]];
                        
                        if ([messageinboxdata.status isEqualToString:@"new"])
                        {
                            int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
                            if (messagecount==0)
                            {
                                messagecount=0;
                            }
                            else
                            {
                                messagecount--;
                            }
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagecount"];
                            
                        }
                        [[EarboxDataStore sharedStore] deleteEarboxInbox:messageinboxdata];
                        
                        
                        
                    }
                }
                
                NSArray *announcementArray= [[EarboxDataStore sharedStore] artists:@"AnnouncementInbox" storeid:storeList.storeId calltype:0];
                for (int i=0; i<[announcementArray count]; i++)
                {
                    AnnouncementInbox *announcementinboxdata=[announcementArray objectAtIndex:i];
                    
                    if ([[NSDateClass ttlformate:announcementinboxdata.ttl] isEqualToString:@"0"]) //&& [announcementinboxdata.status isEqualToString:@"HEARD"])
                    {
                        [[StateMachineHandler sharedManager]sendMsgActionToNative:2 txnid:[NSString stringWithFormat:@"%@",announcementinboxdata.callId]];
                        [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@",announcementinboxdata.callId]];
                        
                        if ([announcementinboxdata.status isEqualToString:@"new"])
                        {
                            int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementcount"] intValue];
                            if (announcementcount==0)
                            {
                                announcementcount=0;
                            }
                            else
                            {
                                announcementcount--;
                            }
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:announcementcount]
                                                                     forKey:@"announcementcount"];
                            
                        }
                        
                        [[EarboxDataStore sharedStore] deleteAnnouncementInbox:announcementinboxdata];
                        
                    }
                }
            }
        }
        //[[NSNotificationCenter defaultCenter]postNotificationName:@"pushnotificationcount" object:@"messagecount"];
        
        
        NSString *str=[NSString stringWithFormat:@"%@",[[NSLogger sharedInstance] getdata]];
        NSData *file=[NSData dataWithContentsOfFile:str];
        //NSData *fileData =[str dataUsingEncoding:NSUTF8StringEncoding];
        long cacheSize =([file length]/1000);
        //cacheSize = (cacheSize/1024);
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if(internetStatus == NotReachable)
        {
            //No internet
        }
        else if (internetStatus == ReachableViaWiFi)
        {
            //WiFi
        }
        else if (internetStatus == ReachableViaWWAN)
        {
            //3G
        }
        if (file.length!=0)
        {
            int logUpload=[[[[NSUserDefaults standardUserDefaults]objectForKey:@"appConfig"] objectForKey:@"logUpload"] intValue];
            if (![[[[NSUserDefaults standardUserDefaults]objectForKey:@"appConfig"] objectForKey:@"logUpload"] isKindOfClass:[NSNull class]] && logUpload)
            {
                if (![[[[NSUserDefaults standardUserDefaults]objectForKey:@"appConfig"] objectForKey:@"logSize"] isKindOfClass:[NSNull class]])
                {
                    if (cacheSize>9216)
                    {
                        NSLog(@"StoreListViewController-Log file reached more than 10MB");
                        [[NSLogger sharedInstance]removefilePath];
                        [[NSLogger sharedInstance]writeNSLogToFile];
                    }
                    
                    else if (cacheSize>[[[[NSUserDefaults standardUserDefaults]objectForKey:@"appConfig"] objectForKey:@"logSize"] intValue])
                    {
                        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
                        [myQueue addOperationWithBlock:^{
                            
                            [HttpHandler uploadlogfiles:file userScreen:@"StoresVC"];
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                
                                
                            }];
                        }];
                        
                    }
                }
            }
            else
            {
                if (cacheSize>9216)
                {
                    NSLog(@"StoreListViewController-Log file file reached more than 10MB");
                    [[NSLogger sharedInstance]removefilePath];
                    [[NSLogger sharedInstance]writeNSLogToFile];
                }
            }
        }
    }
    
}


- (void)viewDidLoad
{
    //[self featureToggle];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Language"]==nil)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Localizable"
                                                         ofType:@"strings"
                                                    inDirectory:nil
                                                forLocalization:@"en"];
        
        // compiled .strings file becomes a "binary property list"
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
        
        [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"Language"];
        [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"ChoosedLang"];
        [[NSUserDefaults standardUserDefaults] setObject:@"english" forKey:@"Languageselected"];
        
        NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
        //[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpathGlobal"]
        [[NSUserDefaults standardUserDefaults] setObject:serverPath forKey:@"serverpathGlobal"];
        
    }
    else
    {
        
    }
    
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    self.searchDisplayController.searchBar.placeholder=[dictLang objectForKey:@"Search"];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:0] forKey:@"storeid"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PasswordChanged"];
    NSString *str =[[NSUserDefaults standardUserDefaults] objectForKey:@"roleId"];
    int rollID=0;
    rollID = [str intValue];
    if (rollID>10)
    {
        self.navigationItem.title=[[NSUserDefaults standardUserDefaults] objectForKey:@"storename_Tcm"];
    }
    else
    {
        deletetimer= [NSTimer scheduledTimerWithTimeInterval: 60.0 target: self selector: @selector(updateExpiryTime) userInfo: nil repeats: YES];
        UIImage *image = [UIImage imageNamed: @"logo"];
        UIImageView *imageview = [[UIImageView alloc] initWithImage: image];
        
        // set the text view to the image view
        self.navigationItem.titleView = imageview;
    }
    
    //self.navigationItem.title=[[NSUserDefaults standardUserDefaults] objectForKey:@"new_Tcm"]
    [[NSUserDefaults standardUserDefaults]setObject:@"storeview" forKey:@"storeview"];
    
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [storesTableview setContentOffset:CGPointMake(0, searchBar.frame.size.height)];
    alphaView=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    alphaView.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.0];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Store" forKey:@"CurrentScreen"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.navigationController.navigationBar.hidden=NO;
    NSString *screen;
    screen = [[NSUserDefaults standardUserDefaults] objectForKey:@"VP"];
    if ([screen isEqualToString:@"NONVP"])
    {
        
    }
    else
    {
        [self.view addSubview:alphaView];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
        tapGestureRecognizer.delegate = self;
        [alphaView addGestureRecognizer:tapGestureRecognizer];
        alphaView.hidden=YES;
        [[UIBarButtonItem appearance] setTintColor:kNavBackgroundColor];
    }
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)featureToggle
{
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    
    NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
    [myQueueSales addOperationWithBlock:^{
        NSArray *featureToggleArray=[HttpHandler featureToggle:[loginDetails objectForKey:@"twUserName"] password:[loginDetails objectForKey:@"twPassword"] userScreen:@"StoresVC"];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if ([[featureToggleArray objectAtIndex:0]objectForKey:@"Error"])
            {
                
            }
            else
            {
                for (int i=0; i<[featureToggleArray count]; i++)
                {
                    int stateint;
                    NSString *nameStr=[[featureToggleArray objectAtIndex:i] objectForKey:@"name"];
                    if ([nameStr isEqualToString:@"SwitchToCommunicator"])
                    {
                        stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                        
                        if (stateint==1)
                        {
                            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"SwitchtoC2"];
                        }
                        else
                        {
                            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"SwitchtoC2"];
                        }
                    }
                    else if ([nameStr isEqualToString:@"CommunicationMode"])
                    {
                        stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                        if (stateint==3)
                        {
                            //instore
                            AppDelegate *AppD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                            //AppDelegate *delegate=[[AppDelegate alloc]init];
                            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"UMA"];
                            [AppD checkNetworkStatus:nil];
                        }
                        else if (stateint==0)
                        {
                            //HMA
                            AppDelegate *AppD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                            //AppDelegate *delegate=[[AppDelegate alloc]init];
                            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"UMA"];
                            [AppD checkNetworkStatus:nil];;
                        }
                    }
                    else if ([nameStr isEqualToString:@"PNSFilter"])
                    {
                        stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                        if (stateint==1)
                        {
                            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"NotificationFilter"];
                        }
                        else
                        {
                            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"NotificationFilter"];
                        }
                    }
                    else if ([nameStr isEqualToString:@"BluetoothSupport"])
                    {
                        stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                    }
                }
                
            }
        }];
    }];
}

-(void)loaddata
{
    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"StoreListViewController"])
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"StoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"StoreList"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
            
        }
        else
        {
            [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
            NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
            [myQueue addOperationWithBlock:^{
                
                loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
                
                NSDictionary *twResponseHeader= [HttpHandler storeList:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] userScreen:@"StoresVC"];
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                    if (![[[twResponseHeader objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
                    {
                        if ([twResponseHeader objectForKey:@"Error"])
                        {
                            [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[twResponseHeader objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                        }
                        else
                        {
                            totalbadgecount=0;
                            storesArray=[[NSMutableArray alloc]init];
                            storeListArray=[twResponseHeader objectForKey:@"storeList"];
                            for (int i=0;i<[storeListArray count];i++)
                            {
                                storeList=[[StoreList alloc]init];
                                
                                if ([[[storeListArray objectAtIndex:i] objectForKey:@"name"] length]>0)
                                {
                                    storeList.name=[[storeListArray objectAtIndex:i] objectForKey:@"name"];
                                    storeList.storeId=[[[storeListArray objectAtIndex:i] objectForKey:@"storeId"] intValue];
                                    storeList.messageCount=[[[storeListArray objectAtIndex:i] objectForKey:@"messageCount"] intValue];
                                    storeList.announcementCount=[[[storeListArray objectAtIndex:i] objectForKey:@"announcementCount"] intValue];
                                    storeList.totalCount=[[[storeListArray objectAtIndex:i] objectForKey:@"totalCount"] intValue];
                                    totalbadgecount=totalbadgecount+storeList.totalCount;
                                }
                                
                                [storesArray addObject:storeList];
                                NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                                [storesArray sortUsingDescriptors:[NSArray arrayWithObject:sort]];
                            }
                            
                            [[UIApplication sharedApplication]setApplicationIconBadgeNumber:totalbadgecount];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:totalbadgecount] forKey:@"badgecount"];
                            
                            [storesTableview reloadData];
                            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                        }
                    }
                    else
                    {
                        NSString *twerrorMessage=[twResponseHeader objectForKey:@"twerrorMessage"];
                        [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)", twerrorMessage,[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    
                }];
            }];
        }
    }
    
    loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
       [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"StoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SalesEnabled"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
    }
    else
    {
        NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
        [myQueueSales addOperationWithBlock:^{
            NSDictionary *featureslist=[HttpHandler salesEnabled:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:@"Tx874874" twMoment:@"46746.6877" userScreen:@"StoresVC"];
            NSArray *mappFeatures=[featureslist objectForKey:@"mappFeatures"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                if (![mappFeatures isKindOfClass:[NSNull class]])
                {
                    [[NSUserDefaults standardUserDefaults]setObject:mappFeatures forKey:@"salesenable"];
                }
                
                NSData *announcementEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:[featureslist objectForKey:@"uiElements"]];
                [[NSUserDefaults standardUserDefaults]setObject:announcementEncodedObject forKey:@"uiElements"];
                if (![[featureslist objectForKey:@"appConfig"] isKindOfClass:[NSNull class]])
                {
                    if (![[[featureslist objectForKey:@"appConfig"] objectForKey:@"logSize"] isKindOfClass:[NSNull class]] && ![[[featureslist objectForKey:@"appConfig"] objectForKey:@"logUpload"] isKindOfClass:[NSNull class]])
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:[featureslist objectForKey:@"appConfig"] forKey:@"appConfig"];
                    }
                    else
                    {
                        NSLog(@"StoreListViewController-feature list appconfig logsize/logupload is null");
                    }
                    
                }
                else
                {
                    
                }
              //  [ActivityIndicatorView activityIndicatorViewRemove:self.view];
            }];
        }];
    }
    
}

#pragma mark - alert method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:viewController animated:YES completion:nil];
}

#pragma mark - Tableview methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView != storesTableview) {
        return [searchResultsAll count];
        
    } else {
        return [storesArray count];
        
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"cell";
    storeList=[storesArray objectAtIndex:indexPath.row];
    PlacesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[PlacesTableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.placeButton.layer.cornerRadius=8;
    if (tableView != storesTableview)
    {
        storeList = [searchResultsAll objectAtIndex:indexPath.row];
        if ([storeList.name length]>0)
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%@",
                                   storeList.name];
            cell.textLabel.textColor=kNavBackgroundColor;
            if(storeList.totalCount>0)
            {
                //cell.countLbl.hidden=NO;
                cell.countLbl.text=[NSString stringWithFormat:@"%d",
                                    storeList.totalCount];
            }
            else
            {
                cell.countLbl.hidden=YES;
            }
        }
        
        
    } else {
        [cell.placeButton setTitle:storeList.name forState:UIControlStateNormal];
        cell.placeButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [cell.placeButton addTarget:self action:@selector(placeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.placeButton.backgroundColor=kNavBackgroundColor;
        NSInteger placeButtonTag=indexPath.row;
        cell.placeButton.tag=placeButtonTag;
        if (storeList.totalCount>0)
        {
            //cell.countLbl.hidden=NO;
            cell.countLbl.text=[NSString stringWithFormat:@"%d",
                                storeList.totalCount];
        }
        else
        {
            cell.countLbl.hidden=YES;
        }
        
        
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"StoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Stores"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        ContactsViewController *contactsController;
        if (tableView != storesTableview) {
            if (searchResultsAll.count>0)
            {
                if ([[InstoreData sharedManager] isInStoreTGS])
                {
                    NSString *storeNameTgs=[[[InstoreData sharedManager] getInStoreTgsStoreName] uppercaseString];
                    
                    NSString *storeName2=[storeList.name uppercaseString];
                    if (![storeName2 isEqualToString:storeNameTgs])
                    {
                        BOOL status = [HttpHandler checkCentralServerAvailability];
                        if (status==YES)
                        {
                            [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"InstoreWebSocket"];
                        }
                        else
                        {
                           [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"StoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Stores"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
                            return;
                        }
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"InstoreWebSocket"];
                    }
                   
                }

                
                contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactsViewController"];
                
                storeList=[searchResultsAll objectAtIndex:indexPath.row];
                [[NSUserDefaults standardUserDefaults] setObject:storeList.name forKey:@"storeNameString"];
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:storeList.storeId] forKey:@"storeid"];
            UIBarButtonItem *newBackButton=[[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                 style:UIBarButtonItemStyleDone
                                                target:nil
                                                action:nil];
                [[self navigationItem] setBackBarButtonItem:newBackButton];
                [self.navigationController pushViewController:contactsController animated:YES];
            }
        }
    }
}

-(void)placeButtonAction:(UIButton *)indexrow
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"StoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Stores"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        if (storesArray.count>0)
        {
            
            if ([[InstoreData sharedManager] isInStoreTGS])
            {
                NSString *storeNameTgs=[[[InstoreData sharedManager] getInStoreTgsStoreName] uppercaseString];
                
                NSString *storeName2=[storeList.name uppercaseString];
                if (![storeName2 isEqualToString:storeNameTgs])
                {
                    BOOL status = [HttpHandler checkCentralServerAvailability];
                    if (status==YES)
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"InstoreWebSocket"];
                    }
                    else
                    {
                        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"StoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"StoresVC"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
                        return;
                    }
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"InstoreWebSocket"];
                }
                
            }

            
            storeList=[storesArray objectAtIndex:indexrow.tag];
            ContactsViewController *contactsController;
            contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactsViewController"];
            [[NSUserDefaults standardUserDefaults] setObject:storeList.name forKey:@"storeNameString"];
            [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:storeList.storeId] forKey:@"storeid"];
            UIBarButtonItem *newBackButton=[[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                            style:UIBarButtonItemStyleDone
                                                                           target:nil
                                                                           action:nil];
            [[self navigationItem] setBackBarButtonItem:newBackButton];
            [self.navigationController pushViewController:contactsController animated:YES];
        }
    }
}


#pragma mark - UISearchDisplayController delegate methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    searchResultsAll=[[NSMutableArray alloc]init];
    
    for (int i =0;i<[storesArray count];i++)
        
    {
        storeList = [storesArray objectAtIndex:i];
        NSRange range=[storeList.name rangeOfString:searchText options:NSCaseInsensitiveSearch];
        if (range.length >0)
        {
            if (![searchResultsAll containsObject:storeList.name]) {
                [searchResultsAll addObject:storeList];
            }
        }
        
    }
}


-(BOOL)searchDisplayController:(UISearchController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[controller.searchBar scopeButtonTitles]
                                      objectAtIndex:[controller.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end
