//
//  GroupsDetailViewController.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface GroupsDetailViewController : UIViewController
<AVAudioRecorderDelegate, AVAudioPlayerDelegate>
{
    IBOutlet UILabel *messageLabel;
    IBOutlet UILabel *lbl_Green;
}

@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;


- (IBAction)deleteFileTapped:(id)sender;


@end
