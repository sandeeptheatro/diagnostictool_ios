//
//  GroupsViewController.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *groupTableview;
    IBOutlet UISearchBar *searchBar;
    IBOutlet UILabel *lbl_Green;
}

@end
