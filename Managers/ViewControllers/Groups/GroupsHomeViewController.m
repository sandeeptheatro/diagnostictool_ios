//
//  GroupsHomeViewController.m
//  Managers
//
//  Created by sandeepchalla on 10/20/15.
//  Copyright © 2015 theatro. All rights reserved.
//

#import "GroupsHomeViewController.h"
#import "Constant.h"
#import "UIViewController.h"
#import "Reachability.h"
#import "AlertView.h"
#import "HttpHandler.h"
#import "ContactsList.h"
#import "StateMachineHandler.h"
#import "ActivityIndicatorView.h"
#import "DiagnosticTool.h"

@interface GroupsHomeViewController ()
{
    int storeId,avaliablecount;
    ContactsList *contactList;
    NSMutableArray *groupdetailListArray,*newContactsListArray,*statusArray,*groupnamesArray,*avaliableArray;
    NSDictionary *loginDetails;
    NSString *modString,*staticerrorcode;
    BOOL avaliablecountcheck;
    NSDictionary *dictLang;
}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *dynamictableHeight;

@end

@implementation GroupsHomeViewController
@synthesize groupid;



-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush isEqualToString:@"YES"])
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"StoreListViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        else
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"VPViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        
    }
    
    else
    {
        connectionLabel.hidden=NO;
        [[StateMachineHandler sharedManager]reOpenClosedSockets];
        [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
    }
    
}

-(void)coveragejson:(NSNotification *)notification
{
    connectionLabel.hidden=YES;
    groupstabBarObj.userInteractionEnabled=YES;
    groupdetailTableview.userInteractionEnabled=YES;
    //Coverage jsonstring getting from ntive c2.
    
    NSArray *coverageArray=[notification.object objectForKey:@"coverage"];
    
    for (int i=0; i<[coverageArray count]; i++)
    {
        int covarageStatus=[[[[coverageArray objectAtIndex:i]objectForKey:@"status"] componentsSeparatedByString:@"_"][0] intValue];
        
        NSArray *groupnames=[[NSUserDefaults standardUserDefaults]objectForKey:@"groupnames"];
        for (int j=0; j<[groupnames count]; j++)
        {
            NSString *covarageTagOutName=[[[coverageArray objectAtIndex:i]objectForKey:@"name"] componentsSeparatedByString:@"_"][1];
            if ([[covarageTagOutName componentsSeparatedByString:@"|"] count]>1)
            {
                covarageTagOutName=[covarageTagOutName componentsSeparatedByString:@"|"][0];
                
            }
            if ([[groupnames objectAtIndex:j] isEqualToString:covarageTagOutName] )
            {
                if (covarageStatus==0)
                {
                    if ([avaliableArray containsObject:covarageTagOutName])
                    {
                        
                    }
                    else
                    {
                        if( [[[coverageArray objectAtIndex:i]objectForKey:@"ct"] intValue]!=2)
                        {
                            [avaliableArray addObject:covarageTagOutName];
                            avaliablecount++;
                        }
                        
                    }
                    
                }
                else
                {
                    if ([avaliableArray count]>0)
                    {
                        
                        if ([avaliableArray containsObject:covarageTagOutName] || [[[coverageArray objectAtIndex:i]objectForKey:@"ct"] intValue]==2)
                        {
                            [avaliableArray removeObject:covarageTagOutName];
                            avaliablecount--;
                            
                        }
                    }
                }
                break;
            }
        }
    }
    
    if (avaliablecount>0)
    {
        [[[groupstabBarObj items]objectAtIndex:1]setEnabled:YES];
    }
    else
    {
        [[[groupstabBarObj items]objectAtIndex:1]setEnabled:NO];
    }
    if ([avaliableArray count]>0)
    {
        [[NSUserDefaults standardUserDefaults]setObject:avaliableArray forKey:@"avaliablelist"];
    }
    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:avaliablecount] forKey:@"avaliablecount"];
    
    
    
    NSMutableArray *fullCoverageArray=[[NSMutableArray alloc]init];
    for (int i=0; i<[coverageArray count]; i++)
    {
        // getting all covarageTagOutNames from jsonstring
        
        NSString *covarageTagOutName=[[[coverageArray objectAtIndex:i]objectForKey:@"name"] componentsSeparatedByString:@"_"][1];
        [fullCoverageArray addObject:covarageTagOutName];
        
    }
    
    for (int i=0; i<[groupdetailListArray count]; i++)
    {
        
        for (int j=0; j<[fullCoverageArray count]; j++)
        {
            contactList=[groupdetailListArray objectAtIndex:i];
            
            // Checking tagoutname in coverage jsonstring
            
            id tagOutName=[NSString stringWithFormat:@"%@",contactList.tagOutName];
            id coverageName=[NSString stringWithFormat:@"%@",[fullCoverageArray objectAtIndex:j]];
            
            if ([[coverageName componentsSeparatedByString:@"|"] count]>1)
            {
                modString=[coverageName componentsSeparatedByString:@"|"][1];
                coverageName=[coverageName componentsSeparatedByString:@"|"][0];
                
            }
            if([tagOutName isEqualToString:coverageName])
            {
                if([[fullCoverageArray objectAtIndex:j] isEqualToString:[loginDetails objectForKey:@"twUserId"]])
                {
                    
                }
                else
                {
                    
                    int status=[[[[coverageArray objectAtIndex:j]objectForKey:@"status"] componentsSeparatedByString:@"_"][0] intValue];
                    [statusArray replaceObjectAtIndex:i withObject:[NSString stringWithFormat:@"%d",status]];
                    
                    NSString *location=[[[coverageArray objectAtIndex:j]objectForKey:@"status"] componentsSeparatedByString:@"_"][1];
                    
                    if (status==6)
                    {
                        contactList.location=@"";
                    }
                    else
                    {
                        contactList.location=location;
                    }
                    
                    [groupdetailListArray replaceObjectAtIndex:i withObject:contactList];
                    contactList.manageriCon=[[coverageArray objectAtIndex:j]objectForKey:@"ct"];
                    [groupdetailListArray replaceObjectAtIndex:i withObject:contactList];
                }
            }
            else
            {
                //tagoutname not avaliable in coverage jsonstring
                
            }
            
        }
        
    }
    newContactsListArray = [[NSMutableArray alloc]init];
    NSMutableArray *names = [NSMutableArray array];
    NSMutableArray *location = [NSMutableArray array];
    NSMutableArray *manageriCon = [NSMutableArray array];
    NSMutableArray *empid = [NSMutableArray array];
    for (int z=0; z<[groupdetailListArray count]; z++)
    {
        contactList=[groupdetailListArray objectAtIndex:z];
        [names addObject:contactList.tagOutName];
        [location addObject:contactList.location];
        [manageriCon addObject:contactList.manageriCon];
        [empid addObject:[NSNumber numberWithInt:contactList.employeeId]];
    }
    
    // Forming all names,location and status as single dictionary with key value pair and adding to Array
    
    for (int idx = 0;idx<[names count];idx++) {
        NSDictionary *dict = @{@"Name": names[idx],@"location":location[idx],@"manageriCon":manageriCon[idx ], @"Status":statusArray[idx],@"empid":empid[idx ]};
        
        [newContactsListArray addObject:dict];
        
        if ([[manageriCon objectAtIndex:idx] intValue]==3)
        {
            if ([[dict objectForKey:@"manageriCon" ] intValue]==3 && ([[dict objectForKey:@"Status" ] intValue]==6))
            {
                
            }
            else
            {
                [newContactsListArray removeObjectAtIndex:idx];
                [newContactsListArray insertObject:dict atIndex:0];
            }
            
        }
    }
    
    // Sorting names based upon Alphabat order
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"Name" ascending:YES];
    [newContactsListArray sortUsingDescriptors:@[descriptor]];
    
    // Sorting Statusicon based upon status
    
    NSSortDescriptor *descriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"Status" ascending:YES];
    [newContactsListArray sortUsingDescriptors:@[descriptor1]];
    
    
    for (int idx = 0;idx<[names count];idx++) {
        NSDictionary *dict = [newContactsListArray objectAtIndex:idx];
        
        if ([[dict objectForKey:@"manageriCon" ] intValue]==3)
        {
            if ([[dict objectForKey:@"manageriCon" ] intValue]==3 && ([[dict objectForKey:@"Status" ] intValue]==6))
            {
                
            }
            else
            {
                [newContactsListArray removeObjectAtIndex:idx];
                [newContactsListArray insertObject:dict atIndex:0];
            }
        }
        
    }
    // Reloading view
    
    [groupdetailTableview reloadData];
    
    
}

-(void)coverageRequestToServertask:(NSNotification *)notification
{
    // Network messages getting from native c2 and updating to UI
    [self connectionRequest];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"texttime"])
    {
        if ([notification.object isEqualToString:@"server connection lost..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"server connection lost..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"could not start a connection..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"could not start a connection..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not logon to store..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not logon to store..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not connect to Proxy server..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not connect to Proxy server..."],staticerrorcode];
        }
        else
        {
            //Second time login ->please wait
            connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
        }
        
    }
    else
    {
        //First time login ,connecting to server->fetching emp list
        connectionLabel.text=notification.object;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"texttime"];
    }
    
    
}

-(void)tgsAlarmerror:(NSString *)tgsLoginerror
{
    staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:@"GroupVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"TgsLogin"],[[DiagnosticTool sharedManager]generateErrorCode:tgsLoginerror]];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        [HttpHandler alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"TgsLogin"] severity:@"1" alarmdesc:staticerrorcode];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
    }];
}

-(void)connectionRequest
{
    connectionLabel.hidden=NO;
    groupstabBarObj.userInteractionEnabled=NO;
    groupdetailTableview.userInteractionEnabled=NO;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coverageRequestToServertask" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [super viewWillDisappear:animated];
}

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    NSLog(@"Group home viewWillAppear");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coverageRequestToServertask:)
                                                 name:@"coverageRequestToServertask"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [super viewWillAppear:animated];
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [groupstabBarObj invalidateIntrinsicContentSize];
    [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    [[[groupstabBarObj items]objectAtIndex:0]setTitle:[dictLang objectForKey:@"Group Message"]];
    [[[groupstabBarObj items]objectAtIndex:1]setTitle:[dictLang objectForKey:@"Group Chat"]];
    [[[groupstabBarObj items]objectAtIndex:2]setTitle:[dictLang objectForKey:@"Group Announcement"]];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    connectionLabel=[[UILabel alloc]init];
    connectionLabel.textColor=[UIColor whiteColor];
    connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
    connectionLabel.frame= CGRectMake(0,self.view.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height);
    connectionLabel.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    connectionLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:connectionLabel];
    connectionLabel.hidden=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coveragejson:)
                                                 name:@"coveragejson"
                                               object:nil];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"GE"])
    {
        [[[grouphometabbar items]objectAtIndex:2]setEnabled:NO];
    }
    else
    {
        [[[grouphometabbar items]objectAtIndex:2]setEnabled:YES];
    }
    
    groupdetailTableview.layer.cornerRadius=7;
    groupdetailTableview.layer.borderWidth=1;
    groupdetailTableview.layer.borderColor=kNavBackgroundColor.CGColor;
    loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    storeId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"]intValue];
    self.navigationItem.title= [[[NSUserDefaults standardUserDefaults] objectForKey:@"groupstring"]capitalizedString];
    connectionLabel=[[UILabel alloc]init];
    connectionLabel.textColor=[UIColor whiteColor];
    connectionLabel.frame= CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height/2);
    connectionLabel.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    connectionLabel.textAlignment=NSTextAlignmentCenter;
    connectionLabel.hidden=YES;
    groupdetailListArray=[[NSMutableArray alloc]init];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"GroupVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"GroupdetailList"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        groupnamesArray=[[NSMutableArray alloc]init];
        avaliableArray=[[NSMutableArray alloc]init];
        statusArray=[NSMutableArray array];
        [myQueue addOperationWithBlock:^{
            
            NSArray *twGroupListArray=[[HttpHandler groupdetailList:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] groupId:groupid userScreen:@"GroupVC"] objectForKey:@"twEmployeeList"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                NSArray *twEmployeeListArray=[[NSUserDefaults standardUserDefaults]objectForKey:@"contactlist"];
                
                for (int i=0;i<[twGroupListArray count];i++)
                {
                    contactList=[[ContactsList alloc]init];
                    
                    [statusArray addObject:[NSString stringWithFormat:@"%d",6]];
                    if ([[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] isEqualToString:[[twGroupListArray objectAtIndex:i] objectForKey:@"tagOutName"]])
                        
                    {
                        
                    }
                    else
                    {
                        contactList.status=[NSString stringWithFormat:@"%d",6];
                        [groupnamesArray addObject:[[twGroupListArray objectAtIndex:i] objectForKey:@"tagOutName"]];
                        contactList.tagOutName=[[twGroupListArray objectAtIndex:i] objectForKey:@"tagOutName"];
                        contactList.location=@"";
                        contactList.manageriCon=@"1";
                        [groupdetailListArray addObject:contactList];
                    }
                    
                }
                [[NSUserDefaults standardUserDefaults]setObject:groupnamesArray forKey:@"groupnames"];
                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"tagOutName" ascending:YES];
                [groupdetailListArray sortUsingDescriptors:[NSArray arrayWithObject:sort]];
                
                
                for (int i=0; i<[groupdetailListArray count]; i++)
                {
                    
                    for (int j=0; j<[twEmployeeListArray count]; j++)
                    {
                        contactList=[groupdetailListArray objectAtIndex:i];
                        
                        // Checking tagoutname in coverage jsonstring
                        
                        id tagOutName=[NSString stringWithFormat:@"%@",contactList.tagOutName];
                        id coverageName=[NSString stringWithFormat:@"%@",[[twEmployeeListArray objectAtIndex:j] objectForKey:@"Name"]];
                        
                        if([tagOutName isEqualToString:coverageName])
                        {
                            
                            
                            int status=[[[twEmployeeListArray objectAtIndex:j] objectForKey:@"Status"]intValue];
                            [statusArray replaceObjectAtIndex:i withObject:[NSString stringWithFormat:@"%d",status]];
                            
                            NSString *location=[[twEmployeeListArray objectAtIndex:j] objectForKey:@"location"];
                            if (status==6)
                            {
                                contactList.location=@"";
                            }
                            else
                            {
                                contactList.location=location;
                            }
                            
                            [groupdetailListArray replaceObjectAtIndex:i withObject:contactList];
                            contactList.manageriCon=[[twEmployeeListArray objectAtIndex:j] objectForKey:@"manageriCon"];
                            [groupdetailListArray replaceObjectAtIndex:i withObject:contactList];
                            
                        }
                        else
                        {
                            //tagoutname not avaliable in coverage jsonstring
                            
                        }
                        
                    }
                    
                }
                newContactsListArray = [[NSMutableArray alloc]init];
                NSMutableArray *names = [NSMutableArray array];
                NSMutableArray *location = [NSMutableArray array];
                NSMutableArray *manageriCon = [NSMutableArray array];
                NSMutableArray *empid = [NSMutableArray array];
                for (int z=0; z<[groupdetailListArray count]; z++)
                {
                    contactList=[groupdetailListArray objectAtIndex:z];
                    [names addObject:contactList.tagOutName];
                    [location addObject:contactList.location];
                    [manageriCon addObject:contactList.manageriCon];
                    [empid addObject:[NSNumber numberWithInt:contactList.employeeId]];
                }
                
                // Forming all names,location and status as single dictionary with key value pair and adding to Array
                
                for (int idx = 0;idx<[names count];idx++) {
                    NSDictionary *dict = @{@"Name": names[idx],@"location":location[idx],@"manageriCon":manageriCon[idx ], @"Status":statusArray[idx],@"empid":empid[idx ]};
                    
                    [newContactsListArray addObject:dict];
                    
                    if ([[manageriCon objectAtIndex:idx] intValue]==3)
                    {
                        if ([[dict objectForKey:@"manageriCon" ] intValue]==3 && ([[dict objectForKey:@"Status" ] intValue]==6))
                        {
                            
                        }
                        else
                        {
                            [newContactsListArray removeObjectAtIndex:idx];
                            [newContactsListArray insertObject:dict atIndex:0];
                        }
                        
                    }
                }
                
                // Sorting names based upon Alphabat order
                
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"Name" ascending:YES];
                [newContactsListArray sortUsingDescriptors:@[descriptor]];
                
                // Sorting Statusicon based upon status
                
                NSSortDescriptor *descriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"Status" ascending:YES];
                [newContactsListArray sortUsingDescriptors:@[descriptor1]];
                
                
                for (int idx = 0;idx<[names count];idx++) {
                    NSDictionary *dict = [newContactsListArray objectAtIndex:idx];
                    
                    if ([[dict objectForKey:@"manageriCon" ] intValue]==3)
                    {
                        if ([[dict objectForKey:@"manageriCon" ] intValue]==3 && ([[dict objectForKey:@"Status" ] intValue]==6))
                        {
                            
                        }
                        else
                        {
                            [newContactsListArray removeObjectAtIndex:idx];
                            [newContactsListArray insertObject:dict atIndex:0];
                        }
                    }
                    
                }
                
                
                avaliablecount=0;
                for (int i=0;i<[twEmployeeListArray count];i++)
                {
                    if ([[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] isEqualToString:[[twEmployeeListArray objectAtIndex:i] objectForKey:@"Name"]])
                        
                    {
                        
                    }
                    else
                    {
                        for (int j=0; j<[groupnamesArray count]; j++)
                        {
                            if ([[groupnamesArray objectAtIndex:j] isEqualToString:[[twEmployeeListArray objectAtIndex:i] objectForKey:@"Name"]])
                            {
                                int status =[[[twEmployeeListArray objectAtIndex:i] objectForKey:@"Status"]intValue];
                                
                                switch (status) {
                                    case 0:
                                        if ([[[twEmployeeListArray objectAtIndex:i]objectForKey:@"manageriCon"] intValue]==2) {
                                            
                                        }
                                        else
                                        {
                                            avaliablecount++;
                                            [avaliableArray addObject:[[twEmployeeListArray objectAtIndex:i] objectForKey:@"Name"]];
                                        }
                                        break;
                                        
                                }
                                break;
                            }
                            
                        }
                    }
                }
                
                
                if (avaliablecount>0)
                {
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:avaliablecount] forKey:@"avaliablecount"];
                    [[[groupstabBarObj items]objectAtIndex:1]setEnabled:YES];
                }
                else
                {
                    [[[groupstabBarObj items]objectAtIndex:1]setEnabled:NO];
                }
                [groupdetailTableview reloadData];
                
            }];
        }];
    }
    self.dynamictableHeight.constant =  45*[newContactsListArray count];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"GroupsHomeViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [ self.navigationController popToRootViewControllerAnimated:YES];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [newContactsListArray count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier=@"cell";
    UITableViewCell *cell;
    UIImageView *managersView;
    UIButton *modView;
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle
                reuseIdentifier:cellIdentifier];
        managersView=[[UIImageView alloc]initWithFrame:CGRectMake(240, 10, 30, 30)];
        modView=[UIButton buttonWithType:UIButtonTypeCustom];
        modView.titleLabel.font = [UIFont fontWithName:kFontName size:13];
        [modView setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        modView .frame=CGRectMake(230, 10, 50, 30);
        
        [cell.contentView addSubview:managersView];
        [cell.contentView addSubview:modView];;
    }
    cell.selectionStyle=UITableViewCellStyleDefault;
    cell.textLabel.font=[UIFont fontWithName:kFontName size:kFontSize];
    cell.detailTextLabel.textColor=kNavBackgroundColor;
    cell.textLabel.textColor=kNavBackgroundColor;
   
    cell.textLabel.text=[[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"Name"]capitalizedString];
    cell.detailTextLabel.text=[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"location"];
    int status =[[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"Status"]intValue];
    NSString *image;
    switch (status) {
        case 0:
            image=@"green_circle";
            
            break;
            
        case 1:
            image=@"red_circle";
             break;
            
        case 2:
            image=@"red_circle";
             break;
            
        case 3:
            image=@"red_circle";
            
            break;
        case 6:
            image=@"grey_circle";
            
            break;
    }
    
    if ([[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"]intValue] == 2)
        
    {
        managersView.image=[UIImage imageNamed:@"manager_icon"];
        
        
    }
    if ([[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"]intValue] == 3)
    {
        
        modView.layer.cornerRadius=7;
        modView.layer.borderWidth=3;
        modView.layer.borderColor=kNavBackgroundColor.CGColor;
        [modView setTitle:@"MOD" forState:UIControlStateNormal];
        
    }
    if(status==6)
    {
        modView.hidden=YES;
    }
    cell.imageView.image=[UIImage imageNamed:image];
    
    return cell;
    
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    UITabBarController *tabController=[self.storyboard instantiateViewControllerWithIdentifier:@"GroupTabbbar"];
    
    switch (item.tag) {
        case 0:
            [tabController setSelectedIndex:0];
            break;
        case 1:
            [tabController setSelectedIndex:1];
            break;
        case 2:
            [tabController setSelectedIndex:2];
            break;
    }
    [self.navigationController pushViewController:tabController animated:YES];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coveragejson" object:nil];
}

@end
