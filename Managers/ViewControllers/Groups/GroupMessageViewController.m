//
//  GroupMessageViewController.m
//  Managers
//
//  Created by sandeepchalla on 10/20/15.
//  Copyright © 2015 theatro. All rights reserved.
//

#import "GroupMessageViewController.h"
#import "GroupsDetailViewController.h"
#import "AudioManager.h"
#import "Constant.h"
#import "Reachability.h"
#import "AlertView.h"
#import "HttpHandler.h"
#import "StateMachineHandler.h"
#import "UIViewController.h"
#import "ViewController.h"
#import "UIViewController.h"
#import "Earboxinboxmodel.h"
#import "DiagnosticTool.h"

@interface GroupMessageViewController ()
{
    NSURL *messagegroupaudiourl;
    NSString *storeNameString,*groupNameString;
    NSMutableDictionary *pns;
    BOOL audioSendButton;
    BOOL audioInterruption;
     NSDictionary *dictLang;
}

@end

@implementation GroupMessageViewController
@synthesize playButton,deleteButton,sendButton,recordPauseButton;

static GroupMessageViewController   *sharedMgr = nil;

+(GroupMessageViewController *)getsharedInstance {
    if (sharedMgr == nil)
    {
        sharedMgr = [[GroupMessageViewController alloc] init];
    }
    return sharedMgr;
}
-(void)audioInterruptionEnd
{
    audioInterruption=YES;
    
}

-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush isEqualToString:@"YES"])
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"StoreListViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        else
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"VPViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        
    }
    
    else
    {
        if (audioInterruption==YES)
        {
            [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
            recordPauseButton.userInteractionEnabled=YES;
            audioInterruption=NO;
        }
        [[StateMachineHandler sharedManager]reOpenClosedSockets];
        [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
    }
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAnnouncementsNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"audioNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recievedAnnouncementsNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAnnouncementsSingleTapNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        [[AudioManager getsharedInstance]audioplayerstop];
        [[AudioManager getsharedInstance]recordstop];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
        
    }];
    
    [super viewWillDisappear:animated];
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}


-(void)viewWillAppear:(BOOL)animated
{
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    
    NSLog(@"Group message viewWillAppear");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playAnnouncement:)
                                                 name:@"playAnnouncementsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendAnnouncement:)
                                                 name:@"audioNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedAnnouncement:)
                                                 name:@"recievedAnnouncementsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(buttonSingleTap)
                                                 name:@"playAnnouncementsSingleTapNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    
    self.parentViewController.navigationItem.title=[[[NSUserDefaults standardUserDefaults]objectForKey:@"groupstring"]capitalizedString];
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:NO];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:NO];
    messageLabel.text=@"";
    
    
    [super viewWillAppear:animated];
}

-(void)viewDidLoad
{
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setTitle:[dictLang objectForKey:@"Group Message"]];
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setTitle:[dictLang objectForKey:@"Group Chat"]];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setTitle:[dictLang objectForKey:@"Group Announcement"]];
    [deleteButton setTitle:[dictLang objectForKey:@"Delete"] forState:UIControlStateNormal];
    [sendButton setTitle:[dictLang objectForKey:@"Send"] forState:UIControlStateNormal];
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];

   self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    playButton.layer.cornerRadius=8;
    deleteButton.layer.cornerRadius=8;
    sendButton.layer.cornerRadius=8;
    playButton.backgroundColor=kGreyColor;
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    [self buttonsDisable];
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    [super viewDidLoad];
    [recordPauseButton addTarget:self action:@selector(buttondDownPressed:) forControlEvents: UIControlEventTouchDown];
    [recordPauseButton addTarget:self action:@selector(buttonUpPressed:) forControlEvents: UIControlEventTouchUpInside];
    [recordPauseButton addTarget:self action:@selector(buttonDragOutside:) forControlEvents: UIControlEventTouchDragOutside];
    [[AudioManager getsharedInstance] path];
    
    
}

-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"GroupMessageViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [ self.navigationController popToRootViewControllerAnimated:YES];
    }
}



-(void)buttonSingleTap
{
    playButton.backgroundColor=kGreyColor;
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    
    [self buttonsDisable];
    messageLabel.text=@"";
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    
}


-(void)buttonsDisable
{
    if (playButton.isEnabled)
    {
        [playButton setEnabled:NO];
    }
    if (deleteButton.isEnabled)
    {
        [deleteButton setEnabled:NO];
    }
    if (sendButton.isEnabled)
    {
        [sendButton setEnabled:NO];
    }
}

-(void)playAnnouncement:(NSNotification *)sender
{
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    messageLabel.text=@"";
    deleteButton.backgroundColor=kNavBackgroundColor;
    [deleteButton setEnabled:YES];
    
    if (audioSendButton==YES)
    {
        sendButton.backgroundColor=kGreyColor;
        [sendButton setEnabled:NO];
        
    }
    else
    {
        messageLabel.text=[dictLang objectForKey:@"Ready to send"];
        sendButton.backgroundColor=kNavBackgroundColor;
        [sendButton setEnabled:YES];
        
    }
    
}

-(void)sendAnnouncement:(NSNotification *)sender
{
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    self.navigationController.navigationBar.tintColor = kGreyColor;
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"GroupMessageViewController"])
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"GroupMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
            
        }
        else
        {
            messagegroupaudiourl=sender.object;
            if ([[AudioManager getsharedInstance]getAudioduration:messagegroupaudiourl]<1.0)
            {
                [AlertView alert:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Audio length is too short to send"],[[DiagnosticTool sharedManager] generateScreenCode:@"GroupMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"Audio length is too short to send"]]];
                messageLabel.text=@"";
                recordPauseButton.userInteractionEnabled=YES;
                [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
                self.navigationController.navigationBar.userInteractionEnabled = YES;
                self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
            }
            else
            {
            NSString *millisec=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ];
            NSArray *tempArray = [millisec componentsSeparatedByString:@"."];
            NSString *macId;
            if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
                macId=[[NSString stringWithFormat:@"%@",
                        [[[[UIDevice currentDevice] identifierForVendor] UUIDString]substringFromIndex:24]]lowercaseString];
            }
            NSDictionary  *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
            
            NSData *file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
            
            
            groupNameString=[[NSUserDefaults standardUserDefaults] objectForKey:@"groupstring"];
            
            
            pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",groupNameString,@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0] doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:messagegroupaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithDouble:17],@"callType",[NSNumber numberWithDouble:300],@"ttl",nil];
            
            storeNameString=[[NSUserDefaults standardUserDefaults]objectForKey:@"storeNameString"];
            NSDictionary *whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[@""],@"users",[NSArray arrayWithObject:storeNameString],@"store", [NSArray arrayWithObject:[loginDetails objectForKey:@"chainName"]],@"chain",@"managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",@[groupNameString],@"groups",nil];
            [HttpHandler audioFileSend:whoDic audioFile:file1Data pns:pns userScreen:@"GroupMessageVC"];
            
        }
        }
    }
    
    
}

-(void)forceLogOut:(NSNotification *)notification
{
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)recievedAnnouncement:(NSNotification *)sender
{
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"GroupMessageViewController"])
    {
        recordPauseButton.userInteractionEnabled=YES;
        [playButton setEnabled:YES];
        [deleteButton setEnabled:YES];
        playButton.backgroundColor=kNavBackgroundColor;
        deleteButton.backgroundColor=kNavBackgroundColor;
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
        NSString *millisec=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ];
        NSArray *tempArray = [millisec componentsSeparatedByString:@"."];
        pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",groupNameString,@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0] doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:messagegroupaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithDouble:17],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
        
        if ([[sender.object objectAtIndex:0] isEqualToString:@"error"])
        {
            messageLabel.text=[dictLang objectForKey:@"Group Message not Sent"];
            sendButton.backgroundColor=kNavBackgroundColor;
            [sendButton setEnabled:YES];
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
            
        }
        else if ([[sender.object objectAtIndex:0] isEqualToString:@"message"])
        {
            messageLabel.text=[dictLang objectForKey:@"Group Message Sent"];
            audioSendButton=YES;
            sendButton.backgroundColor=kGreyColor;
            
            [sendButton setEnabled:NO];
            [pns setObject:groupNameString  forKey:@"groupName"];
            [pns setObject:storeNameString forKey:@"storeName"];
            [pns setObject:@"HEARD" forKey:@"status"];
            [pns setObject:[NSNumber numberWithInt:1] forKey:@"maCallType"];
            [pns setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] forKey:@"storeId"];
            //[pns setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"] forKey:@"pushTxId"];
            if ([sender.object count]==2)
            {
                [pns setObject:[sender.object objectAtIndex:1] forKey:@"pushTxId"];
            }
            if ([pns count]>0)
            {
                [[Earboxinboxmodel sharedModel]messagesentSave:[NSArray arrayWithObject:pns]];
            }
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
        }
    }
    
}

- (void)buttondDownPressed:(UIButton *)sender
{
    [self buttonsDisable];
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    playButton.backgroundColor=kGreyColor;
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    messageLabel.text=[dictLang objectForKey:@"Recording..."];
    [[AudioManager getsharedInstance] recordPauseTapped];
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue_white"] forState:UIControlStateNormal];
}



- (void)buttonUpPressed:(UIButton *)sender {
        messageLabel.text=[dictLang objectForKey:@"Ready to send"];
    audioSendButton=NO;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
    [playButton setEnabled:YES];
    [deleteButton setEnabled:YES];
    [sendButton setEnabled:YES];
    deleteButton.backgroundColor=kNavBackgroundColor;
    sendButton.backgroundColor=kNavBackgroundColor;
    playButton.backgroundColor=kNavBackgroundColor;
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    [[AudioManager getsharedInstance] stopTapped];
}

-(void)buttonDragOutside:(UIButton *)sender
{
        messageLabel.text=[dictLang objectForKey:@"Ready to send"];
    audioSendButton=NO;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
    [playButton setEnabled:YES];
    [deleteButton setEnabled:YES];
    [sendButton setEnabled:YES];
    deleteButton.backgroundColor=kNavBackgroundColor;
    sendButton.backgroundColor=kNavBackgroundColor;
    playButton.backgroundColor=kNavBackgroundColor;
    [[AudioManager getsharedInstance] stopTapped];
}

- (IBAction)playTapped:(UIButton *)sender {
    if ([playButton.titleLabel.text isEqualToString:[dictLang objectForKey:@"Stop"]])
    {
        if (audioSendButton==YES)
        {
            [deleteButton setEnabled:YES];
            [sendButton setEnabled:NO];
            deleteButton.backgroundColor=kNavBackgroundColor;
            sendButton.backgroundColor=kGreyColor;
            messageLabel.text=@"";
            [[AudioManager getsharedInstance]audioplayerstop];
            [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
        }
        else
        {
            [deleteButton setEnabled:YES];
            [sendButton setEnabled:YES];
            deleteButton.backgroundColor=kNavBackgroundColor;
            sendButton.backgroundColor=kNavBackgroundColor;
            messageLabel.text=@"";
            [[AudioManager getsharedInstance]audioplayerstop];
            [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
        }
    }
    else
    {
        [playButton setTitle:[dictLang objectForKey:@"Stop"] forState:UIControlStateNormal];
        [deleteButton setEnabled:NO];
        [sendButton setEnabled:NO];
        deleteButton.backgroundColor=kGreyColor;
        sendButton.backgroundColor=kGreyColor;
        messageLabel.text=[dictLang objectForKey:@"Replaying..."];
        [[AudioManager getsharedInstance] playTapped];
    }
}


- (IBAction)sendFileTapped:(id)sender
{
    [deleteButton setEnabled:NO];
    [sendButton setEnabled:NO];
    [playButton setEnabled:NO];
    playButton.backgroundColor=[UIColor grayColor];
    deleteButton.backgroundColor=[UIColor grayColor];
    sendButton.backgroundColor=[UIColor grayColor];
    recordPauseButton.userInteractionEnabled=NO;
    messageLabel.text=[dictLang objectForKey:@"Sending..."];
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_gray.png"] forState:UIControlStateNormal];
    [[AudioManager getsharedInstance] sendTapped];
}

- (IBAction)deleteFileTapped:(id)sender
{
    messageLabel.text=[dictLang objectForKey:@"Group Message deleted"];
    deleteButton.backgroundColor=[UIColor grayColor];
    sendButton.backgroundColor=[UIColor grayColor];
    playButton.backgroundColor=[UIColor grayColor];
    [sendButton setEnabled:NO];
    [playButton setEnabled:NO];
    [deleteButton setEnabled:NO];
    [[AudioManager getsharedInstance] deleteTapped];
}

@end
