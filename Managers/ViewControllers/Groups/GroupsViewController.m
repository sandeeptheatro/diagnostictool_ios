//
//  GroupsViewController.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "GroupsViewController.h"
#import "PlacesTableViewCell.h"
#import "GroupsDetailViewController.h"
#import "Constant.h"
#import "HttpHandler.h"
#import "GroupList.h"
#import "Reachability.h"
#import "AlertView.h"
#import "Constant.h"
#import "GroupsHomeViewController.h"
#import "UIViewController.h"
#import "StateMachineHandler.h"
#import "ActivityIndicatorView.h"
#import "DiagnosticTool.h"

@interface GroupsViewController ()
{
    NSMutableArray *groupListArray,*groupsearchResultsAll;
    GroupList *groupList;
    UILabel *connectionLabel;
     NSDictionary *dictLang;
}

@end

@implementation GroupsViewController

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [super viewWillDisappear:animated];
}


-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    
    NSLog(@"Groups list viewWillAppear");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:@"localnotification" object:nil];
    [super viewWillAppear:animated];
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}


- (void)viewDidLoad
{
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    self.searchDisplayController.searchBar.placeholder=[dictLang objectForKey:@"Search"];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
   // [groupTableview setContentOffset:CGPointMake(0, searchBar.frame.size.height)];
    int storeId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"]intValue];
    self.navigationItem.title=[dictLang objectForKey:@"Groups"];

    groupListArray=[[NSMutableArray alloc]init];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: kNavBackgroundColor};
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"GroupsVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"GroupList"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            
            NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
            NSDictionary *twResponseHeaderDic=[HttpHandler groupList:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"]storeId:storeId userScreen:@"GroupsVC"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if ([twResponseHeaderDic objectForKey:@"Error"])
                {
                    [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[twResponseHeaderDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                }
                else
                {
                    NSArray *twGroupListArray=[twResponseHeaderDic objectForKey:@"twGroupList"];
                    for (int i=0;i<[twGroupListArray count];i++)
                    {
                        groupList=[[GroupList alloc]init];
                        groupList.name=[[twGroupListArray objectAtIndex:i] objectForKey:@"name"];
                        groupList.groupId=[[[twGroupListArray objectAtIndex:i] objectForKey:@"groupId"] intValue];
                        groupList.groupType=[[[twGroupListArray objectAtIndex:i] objectForKey:@"groupType"] intValue];
                        [groupListArray addObject:groupList];
                    }
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                    [groupListArray sortUsingDescriptors:[NSArray arrayWithObject:sort]];
                    [groupTableview reloadData];
                }
                
            }];
        }];
    }
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"GroupsViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [ self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 70;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView != groupTableview) {
        return [groupsearchResultsAll count];
        
    }
    else
    {
        return [groupListArray count];
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier=@"cell";
    PlacesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[PlacesTableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:cellIdentifier];
        
        
    }
    
    if (tableView != groupTableview)
    {
        cell.textLabel.text =[groupsearchResultsAll objectAtIndex:indexPath.row];
        cell.textLabel.textAlignment=NSTextAlignmentCenter;
        cell.textLabel.textColor=kNavBackgroundColor;
        
    }
    else
    {
        groupList=[groupListArray objectAtIndex:indexPath.row];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.placeButton.backgroundColor=kNavBackgroundColor;
        cell.placeButton.layer.cornerRadius=8;
        [cell.placeButton setTitle:groupList.name forState:UIControlStateNormal];
        cell.placeButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [cell.placeButton addTarget:self action:@selector(placeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.placeButton.tag=indexPath.row;
    }
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView != groupTableview)
    {
        
        GroupsHomeViewController *groupDetailController = [self.storyboard instantiateViewControllerWithIdentifier:@"GroupsHomeViewController"];
        for (int i=0; i<[groupListArray count]; i++)
        {
            groupList=[groupListArray objectAtIndex:i];
            if ([[groupsearchResultsAll objectAtIndex:indexPath.row] isEqualToString:groupList.name])
            {
                
                [[NSUserDefaults standardUserDefaults] setObject:[[groupsearchResultsAll objectAtIndex:indexPath.row] lowercaseString] forKey:@"groupstring"];
                groupDetailController.groupid=groupList.groupId;
            }
        }
        [self.navigationController pushViewController:groupDetailController animated:YES];
    }
}

-(void)reOpenClosedSocketsNotifiacation
{
    NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush isEqualToString:@"YES"])
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"StoreListViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        else
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"VPViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        
    }
    
}

-(void)placeButtonAction:(UIButton *)indexrow
{
    groupList=[groupListArray objectAtIndex:indexrow.tag];
    GroupsHomeViewController *groupDetailController = [self.storyboard instantiateViewControllerWithIdentifier:@"GroupsHomeViewController"];
    groupDetailController.groupid=groupList.groupId;
    [[NSUserDefaults standardUserDefaults]setObject:groupList.name forKey:@"groupstring"];
    [self.navigationController pushViewController:groupDetailController animated:YES];
}


#pragma mark - UISearchDisplayController delegate methods

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    groupsearchResultsAll=[[NSMutableArray alloc]init];
    
    for (int i =0;i<[groupListArray count];i++)
        
    {
        groupList = [groupListArray objectAtIndex:i];
        NSRange range=[groupList.name rangeOfString:searchText options:NSCaseInsensitiveSearch];
        if (range.length >0)
        {
            if (![groupsearchResultsAll containsObject:groupList.name]) {
                [groupsearchResultsAll addObject:groupList.name];
            }
        }
        
    }
}

-(BOOL)searchDisplayController:(UISearchController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[controller.searchBar scopeButtonTitles]
                                      objectAtIndex:[controller.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end
