//
//  GroupsHomeViewController.h
//  Managers
//
//  Created by sandeepchalla on 10/20/15.
//  Copyright © 2015 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupsHomeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITabBarDelegate>
{
    IBOutlet UITabBar *grouphometabbar;
    IBOutlet UITableView *groupdetailTableview;
    IBOutlet UITabBar *groupstabBarObj;
    IBOutlet UILabel *connectionLabel;
    IBOutlet UILabel *lbl_Green;
}

@property(nonatomic,assign)int groupid;

@end
