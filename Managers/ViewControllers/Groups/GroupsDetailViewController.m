//
//  GroupsDetailViewController.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "GroupsDetailViewController.h"
#import "AudioManager.h"
#import "Constant.h"
#import "HttpHandler.h"
#import "StateMachineHandler.h"
#import "Reachability.h"
#import "UIViewController.h"
#import "ViewController.h"
#import "UIViewController.h"
#import "Constant.h"
#import "DiagnosticTool.h"

@interface GroupsDetailViewController ()
{
    UILabel *contactPlaceLabel;
    UIImageView *contactStatusImage;
    int covarageStatus,avaliablecount;
    BOOL onetone,doubleTapTabitem;
    NSString *storeNameString,*onetooneInterruptString,*contactString,*staticerrorcode;
    BOOL audioSendButton,onGoinggroupcall;
    NSMutableArray *avaliablelistArray;
    UILabel *connectionLabel;
    UIBarButtonItem *speakerButton;
    NSDictionary *dictLang;
}

@end

@implementation GroupsDetailViewController
@synthesize deleteButton,recordPauseButton;


-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    NSLog(@"Group OnetoOne viewWillAppear");
    connectionLabel=[[UILabel alloc]init];
    connectionLabel.textColor=[UIColor whiteColor];
    connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
    connectionLabel.frame= CGRectMake(0,self.view.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height);
    connectionLabel.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    connectionLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:connectionLabel];
    connectionLabel.hidden=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterLockNotification) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coverageRequestToServertask:)
                                                 name:@"coverageRequestToServertask"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:NO];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:NO];
    onetooneInterruptString=@"groupone2one";
    recordPauseButton.userInteractionEnabled=YES;
    deleteButton.backgroundColor=[UIColor grayColor];
    [deleteButton setTitle:[dictLang objectForKey:@"End"] forState:UIControlStateNormal];
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coverageRequestToServertask" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
    [[StateMachineHandler sharedManager]sendCallEndToNative];
    [[StateMachineHandler sharedManager]sendPlaybufferenable:NO];
    [[StateMachineHandler sharedManager]sendCancelPlayoutToNative:YES];
    [super viewWillDisappear:animated];
}


-(void)coverageRequestToServertask:(NSNotification *)notification
{
    // Network messages getting from native c2 and updating to UI
    [self connectionRequest];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"texttime"])
    {
        if ([notification.object isEqualToString:@"server connection lost..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"server connection lost..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"could not start a connection..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"could not start a connection..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not logon to store..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not logon to store..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not connect to Proxy server..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not connect to Proxy server..."],staticerrorcode];
        }
        else
        {
            //Second time login ->please wait
            connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
        }
        
    }
    else
    {
        //First time login ,connecting to server->fetching emp list
        connectionLabel.text=notification.object;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"texttime"];
    }
    
}


-(void)tgsAlarmerror:(NSString *)tgsLoginerror
{
    staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:@"GroupdetailListVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"TgsLogin"],[[DiagnosticTool sharedManager]generateErrorCode:tgsLoginerror]];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        [HttpHandler alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"TgsLogin"] severity:@"1" alarmdesc:staticerrorcode];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
    }];
}

-(void)connectionRequest
{
    connectionLabel.hidden=NO;
    recordPauseButton.userInteractionEnabled=NO;
    
}

-(void)coveragejson:(NSNotification *)notification
{
    connectionLabel.hidden=YES;
    recordPauseButton.userInteractionEnabled=YES;
    NSArray *coverageArray=[notification.object objectForKey:@"coverage"];
    for (int i=0; i<[coverageArray count]; i++)
    {
        // getting all covarageTagOutNames from jsonstring
        covarageStatus=[[[[coverageArray objectAtIndex:i]objectForKey:@"status"] componentsSeparatedByString:@"_"][0] intValue];
        
        
        NSArray *groupnames=[[NSUserDefaults standardUserDefaults]objectForKey:@"groupnames"];
        
        
        for (int j=0; j<[groupnames count]; j++)
        {
            NSString *covarageTagOutName=[[[coverageArray objectAtIndex:i]objectForKey:@"name"] componentsSeparatedByString:@"_"][1];
            if ([[groupnames objectAtIndex:j] isEqualToString:covarageTagOutName])
            {
                if (covarageStatus==0)
                {
                    if ([avaliablelistArray containsObject:covarageTagOutName])
                    {
                        
                    }
                    else
                    {
                        [avaliablelistArray addObject:covarageTagOutName];
                        avaliablecount++;
                    }
                    
                }
                else
                {
                    if ([avaliablelistArray containsObject:covarageTagOutName])
                    {
                        if (covarageStatus==2 )
                        {
                            if (onGoinggroupcall)
                            {
                                contactStatusImage.image=[UIImage imageNamed:@"red_circle"];
                            }
                            else
                            {
                                [avaliablelistArray removeObject:covarageTagOutName];
                                avaliablecount--;
                            }
                        }
                        
                        else
                        {
                            [avaliablelistArray removeObject:covarageTagOutName];
                            avaliablecount--;
                        }
                        
                    }
                }
                break;
            }
        }
        
        if(covarageStatus==5)
        {
            onGoinggroupcall=NO;
            deleteButton.backgroundColor=[UIColor grayColor];
            [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
            recordPauseButton.userInteractionEnabled=YES;
            
        }
    }
    [self avaliablecount];
}

-(void)avaliablecount
{
    if (avaliablecount>0)
    {
        
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
        recordPauseButton.userInteractionEnabled=YES;
        contactStatusImage.image=[UIImage imageNamed:@"green_circle"];
    }
    else
    {
        
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_gray"] forState:UIControlStateNormal];
        recordPauseButton.userInteractionEnabled=NO;
        contactStatusImage.image=[UIImage imageNamed:@"red_circle"];
    }
    
}





-(void)enterLockNotification
{
    [[StateMachineHandler sharedManager]mediabuttonAction:onetooneInterruptString nameStr:contactString Button:NO];
    [[StateMachineHandler sharedManager]enterLockScreen];
}


-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"GroupsDetailViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [ self.navigationController popToRootViewControllerAnimated:YES];
    }
}
- (void)viewDidLoad
{
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setTitle:[dictLang objectForKey:@"Group Message"]];
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setTitle:[dictLang objectForKey:@"Group Chat"]];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setTitle:[dictLang objectForKey:@"Group Announcement"]];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coveragejson:)
                                                 name:@"coveragejson"
                                               object:nil];
    storeNameString=[[NSUserDefaults standardUserDefaults]objectForKey:@"groupstring"];
    
    [self intialView];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = kNavBackgroundColor;
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
    titleLabel.text = [[[NSUserDefaults standardUserDefaults]objectForKey:@"groupstring"] capitalizedString];
    [titleLabel sizeToFit];
    
    contactString=[[NSUserDefaults standardUserDefaults]objectForKey:@"groupstring"];
    
    contactPlaceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 18, self.view.frame.size.width-150, 20)];
    contactPlaceLabel.backgroundColor = [UIColor clearColor];
    contactPlaceLabel.textColor = kNavBackgroundColor;
    contactPlaceLabel.font = [UIFont systemFontOfSize:12];
    
    contactPlaceLabel.textAlignment=NSTextAlignmentCenter;
    
    UIView *navigationTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, 30)];
    [navigationTitleView addSubview:titleLabel];
    [navigationTitleView addSubview:contactPlaceLabel];
    contactStatusImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    float widthDiff = contactPlaceLabel.frame.size.width - titleLabel.frame.size.width;
    CGRect titleframe = titleLabel.frame;
    if (widthDiff > 0) {
        
        titleframe.origin.x = (widthDiff) / 2;
        titleLabel.frame = CGRectIntegral(titleframe);
        
    }else{
        titleframe.size.width=self.view.frame.size.width-100;
        titleLabel.frame = CGRectIntegral(titleframe);
    }
    contactStatusImage.frame=CGRectMake(titleframe.origin.x-20, 05, 12, 12);
    self.parentViewController.navigationItem.titleView=navigationTitleView;
    deleteButton.layer.cornerRadius=8;
    
    [super viewDidLoad];
    [recordPauseButton addTarget:self action:@selector(methodTouchDown:) forControlEvents: UIControlEventTouchDown];
    [recordPauseButton addTarget:self action:@selector(recordPauseTapped:) forControlEvents: UIControlEventTouchUpInside];
    [recordPauseButton addTarget:self action:@selector(buttonDragOutside:) forControlEvents: UIControlEventTouchDragOutside];
    
    // Disable Stop/Play button when application launches
    [[AudioManager getsharedInstance] path];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:@"localnotification" object:nil];
    avaliablecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"avaliablecount"] intValue];
    avaliablelistArray=[[[NSUserDefaults standardUserDefaults]objectForKey:@"avaliablelist"]mutableCopy];
    [self avaliablecount];
    speakerButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"loud"] style:UIBarButtonItemStyleDone target:self action:@selector(speakerButtonAction:)];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"loud"] isEqualToString:@"mic"])
    {
        [speakerButton setImage:[UIImage imageNamed:@"headphones"]];
        speakerButton.tag=2;
        [[NSUserDefaults standardUserDefaults]setObject:@"mic" forKey:@"loud"];
        
    }
    else
    {
        speakerButton.tag=1;
        [speakerButton setImage:[UIImage imageNamed:@"loud"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"loud" forKey:@"loud"];
        
    }
    
    self.parentViewController.navigationItem.rightBarButtonItem=speakerButton;
    [[AudioManager getsharedInstance] switchAudioOutput];
}


-(void)speakerButtonAction:(UIBarButtonItem *)button
{
    if (button.tag==1)
    {
        [speakerButton setImage:[UIImage imageNamed:@"headphones"]];
        speakerButton.tag=2;
        [[NSUserDefaults standardUserDefaults]setObject:@"mic" forKey:@"loud"];
    }
    else
    {
        speakerButton.tag=1;
        [speakerButton setImage:[UIImage imageNamed:@"loud"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"loud" forKey:@"loud"];
    }
    [[AudioManager getsharedInstance] switchAudioOutput];
}


-(void)reOpenClosedSocketsNotifiacation
{
    NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush isEqualToString:@"YES"])
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"StoreListViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        else
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"VPViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        
    }
    
    else
    {
        connectionLabel.hidden=NO;
        [[StateMachineHandler sharedManager]reOpenClosedSockets];
        [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
    }
}


-(void)intialView
{
    recordPauseButton.userInteractionEnabled=NO;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_gray"] forState:UIControlStateNormal];
    deleteButton.backgroundColor=kGreyColor;
    
    [[StateMachineHandler sharedManager]sendCallEndToNative];
    [[StateMachineHandler sharedManager]sendPlaybufferenable:NO];
}




- (void)methodTouchDown:(UIButton *)sender
{
    onGoinggroupcall=YES;
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    
    [[StateMachineHandler sharedManager]mediabuttonAction:onetooneInterruptString nameStr:contactString Button:YES];
    
    [[StateMachineHandler sharedManager]sendPlaybufferenable:YES];
    
}

-(void)buttonDragOutside:(UIButton *)sender
{
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    audioSendButton=NO;
    deleteButton.userInteractionEnabled=YES;
    deleteButton.backgroundColor=[UIColor redColor];
    [[StateMachineHandler sharedManager]sendPlaybufferenable:YES];
    [[StateMachineHandler sharedManager] mediabuttonAction:onetooneInterruptString nameStr:contactString Button:NO];
    
    
}

- (void)recordPauseTapped:(UIButton *)sender
{
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    audioSendButton=NO;
    deleteButton.userInteractionEnabled=YES;
    deleteButton.backgroundColor=[UIColor redColor];
    [[StateMachineHandler sharedManager]sendPlaybufferenable:YES];
    [[StateMachineHandler sharedManager] mediabuttonAction:onetooneInterruptString nameStr:contactString Button:NO];
    
    
    
}

- (IBAction)deleteFileTapped:(id)sender
{
    onGoinggroupcall=NO;
    [[StateMachineHandler sharedManager]sendPlaybufferenable:NO];
    [[StateMachineHandler sharedManager]sendCallEndToNative];
    [[StateMachineHandler sharedManager]sendCancelPlayoutToNative:YES];
    deleteButton.backgroundColor=[UIColor grayColor];
}


-(void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coveragejson" object:nil];
}

@end
