//
//  GroupWeekViewController.h
//  Managers
//
//  Created by sandeepchalla on 3/7/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupWeekViewController : UIViewController<UIAlertViewDelegate>
{
    IBOutlet UILabel *messageLabel;
    IBOutlet UILabel *lbl_Green;
}
@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property(nonatomic,strong)NSString *contactString,*contactStatusString;
-(void)audioInterruptionEnd;
+(GroupWeekViewController *)getsharedInstance;

- (IBAction)sendFileTapped:(id)sender;
- (IBAction)deleteFileTapped:(id)sender;
- (IBAction)playTapped:(id)sender;

@end
