//
//  NewMessageViewController.h
//  ManagersMockup
//
//  Created by Ravi on 01/09/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Earboxinboxmodel.h"
#import "Favorites+CoreDataProperties.h"
#import "HttpHandler.h"
#import "EarboxDataStore.h"

@interface NewMessageViewController : UIViewController

@end
