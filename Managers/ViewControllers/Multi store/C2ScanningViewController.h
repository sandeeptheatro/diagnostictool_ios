//
//  C2ScanningViewController.h
//  Managers
//
//  Created by sandeepchalla on 9/5/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "MarqueeLabel.h"

@interface C2ScanningViewController : UIViewController<AVCaptureMetadataOutputObjectsDelegate,UITextFieldDelegate>
{
    IBOutlet MarqueeLabel *boardIdLabel;
    IBOutlet UITextField *boardIdTextfield;
    IBOutlet UILabel *lbl_Green;
}

@end
