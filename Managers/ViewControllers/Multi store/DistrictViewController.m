//
//  DistrictViewController.m
//  ManagersMockup
//
//  Created by Ravi on 31/08/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "DistrictViewController.h"
#import "ListCell.h"
#import "ContactHomeViewController.h"
#import "Constant.h"
#import "DiagnosticTool.h"

@interface DistrictViewController ()
{
    NSMutableArray *disTrictArray,*listImages,*messageArray,*everyOneArray,*individualArray,*groupArray;
    NSInteger selectedIndex;
    NSDictionary *dictLang;
}

@end

@implementation DistrictViewController

- (void)viewDidLoad
{
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
   // self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kNavBackgroundColor,
       NSFontAttributeName:[UIFont fontWithName:kFontName size:13]}];
    disTrictArray=[[NSMutableArray alloc]init];
    individualArray = [[NSMutableArray alloc] init];
    groupArray = [[NSMutableArray alloc] init];
    
    NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
    
    
    UIBarButtonItem *favoritsButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"favorite"] style:UIBarButtonItemStyleDone target:self action:@selector(favoritsButtonAction)];
    // self.navigationItem.rightBarButtonItem=favoritsButton;
    if ([fav isEqualToString:@"fav"])
    {
        [favoritsButton setEnabled:NO];
    }
    
    
    everyOneArray=[NSMutableArray arrayWithObjects:[dictLang objectForKey:@"One Store"],[dictLang objectForKey:@"Multiple Stores"],[dictLang objectForKey:@"All Stores"],nil];
    messageArray = [[NSMutableArray alloc] init];
    messageArray =[NSMutableArray arrayWithObjects:@"14",@"3",@"10",@"0",@"6",@"4",@"1",@"0",nil];
    
    
    listImages =[[NSMutableArray alloc]init];
    
    
    
    
    [listImages  addObject:[UIImage imageNamed:@"users-group"]];
    districtTable.allowsMultipleSelection = YES;
    
    NSString *fromStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"MessageFrom"];
    if ([fromStr isEqualToString:@"Message"])
    {
        self.navigationItem.title =[dictLang objectForKey:@"New Message To"];
        //disTrictArray=[NSMutableArray arrayWithObjects:@"AS",@"MS",@"SL",nil];
        disTrictArray = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"config_message"]];
        
        //[str caseInsensitiveCompare:@"Store"]
        //[str caseInsensitiveCompare:@"HQ individual"]
        for (int i=0; i<[disTrictArray count]; i++)
        {
            if ([[[disTrictArray objectAtIndex:i] objectForKey:@"name"] isEqualToString:@"Store"] ||[[[disTrictArray objectAtIndex:i] objectForKey:@"name"] isEqualToString:@"HQ individual"])
            {
                [individualArray addObject:[disTrictArray objectAtIndex:i]];
            }
            else
            {
                [groupArray addObject:[disTrictArray objectAtIndex:i]];
            }
        }
        
        // disTrictArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"config_message"];
        //[disTrictArray addObject:@"AS"];
    }
    else
    {
        self.navigationItem.title = @"Announcement To:";
        //disTrictArray=[NSMutableArray arrayWithObjects:@"All Stores",@"AS",@"MS",@"SL",@"Select Stores",nil];
        //disTrictArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"config_announcement"];
        disTrictArray = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"config_announcement"]];
    }
    
    //        UIBarButtonItem *chkmanuaaly = [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextview)];
    //        self.navigationItem.rightBarButtonItem=chkmanuaaly;
    
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}
-(void)favoritsButtonAction
{
     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Please enter the favorite name"],[[DiagnosticTool sharedManager] generateScreenCode:@"NewMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please enter the favorite name"]] message:nil delegate:self cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
    alert.alertViewStyle=UIAlertViewStylePlainTextInput;
    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeDefault];
    
    //[[alert textFieldAtIndex:1] becomeFirstResponder];
    [alert show];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==108)
    {
        [self favoritsButtonAction];
    }
    else
    {
        if (buttonIndex==1)
        {
            UITextField *group = [actionSheet textFieldAtIndex:0];
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            
            
            group.text= [group.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (group.text.length>0 )
            {
                //group.text= [group.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                NSMutableArray *newArray; //= [[NSMutableArray alloc] init];
                NSMutableArray *nameArray = [[NSMutableArray alloc]init];
                newArray= [[EarboxDataStore sharedStore] favoritesArtists];
                
                for (int i=0; i<[newArray count]; i++)
                {
                    Favorites *fav;
                    fav=[newArray objectAtIndex:i];
                    
                    [nameArray addObject:fav.favouriteName];
                }
                
                if ([nameArray containsObject:group.text])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@ %@ (%@%@%@%@i)",[dictLang objectForKey:@"This"],[[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"]capitalizedString],[dictLang objectForKey:@"name already exists"],[[DiagnosticTool sharedManager] generateScreenCode:@"NewMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"favorite name already exists"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    alert.tag=108;
                    [alert show];
                }
                else
                {
                    NSString *fromStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"MessageFrom"];
                    
                    NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
                    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
                    [postData setObject:@"" forKey:@"PlayAudioType"];
                    [postData setObject:fromStr forKey:@"FinalView"];
                    [postData setObject:@[] forKey:@"Groups"];
                    [postData setObject:@[] forKey:@"Stores"];
                    //[postData setObject:@"" forKey:@"ChannelSected"];
                    [postData setObject:group.text forKey:@"FavouriteName"];
                    [postData setObject:sl forKey:@"MessageType"];
                    //[postData setObject:@"" forKey:@"Sender"];//featureType
                    [postData setObject:@"" forKey:@"FeatureType"];
                    [postData setObject:@"MS-MA" forKey:@"Originator"];//PlatForm
                    [postData setObject:@"iOS" forKey:@"PlatForm"];//FavouriteID
                    [postData setObject:@"NewMessage" forKey:@"FavouriteType"];
                    [postData setObject:@"" forKey:@"NodeId"];
                    [postData setObject:@"" forKey:@"ParentId"];
                    [postData setObject:@"" forKey:@"LevelId"];
                    [postData setObject:@"false" forKey:@"StoreScreen"];
                    [postData setObject:@[] forKey:@"StoreNames"];
                    
                    [postData setObject:@"" forKey:@"MultiStore"];
                    [postData setObject:@"" forKey:@"HQTagOutName"];
                    [postData setObject:@[] forKey:@"HQManagerAppID"];
                    [postData setObject:@[] forKey:@"Nodes"];
                    [postData setObject:@[] forKey:@"MultiStoreNodeId"];
                    [postData setObject:@[] forKey:@"MultiStoreNodesNames"];
                    [postData setObject:@"" forKey:@"levelName"];
                    [postData setObject:@"" forKey:@"storeselect"];
                    [postData setObject:@"" forKey:@"title"];
                 
                    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
                    
                    
                    
                    
                    NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
                    [myQueueSales addOperationWithBlock:^{
                        NSDictionary *responsDic=[HttpHandler favSave:[loginDetails objectForKey:@"twUserName"] password:[loginDetails objectForKey:@"twPassword"] jSonStr:postData userScreen:@"NewMessageVC"];
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            if ([responsDic objectForKey:@"Error"])
                            {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                                
                                [alert show];
                            }
                            else if ([responsDic objectForKey:@"FavouriteID"])
                            {
                                [postData setObject:[responsDic objectForKey:@"FavouriteID"] forKey:@"FavouriteID"];
                                [array addObject:postData];
                                //[array addObject:responsDic];
                                [[Earboxinboxmodel sharedModel]favoritesInsert:array];
                            }
                            
                        }];
                    }];
                }
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@ (%@%@%@%@i)",[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"],[dictLang objectForKey:@"name cannot be empty"],[[DiagnosticTool sharedManager] generateScreenCode:@"NewMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"favorite name cannot be empty"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                alert.tag=108;
                [alert show];
            }
            
        }
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"storeselect"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MultiStore"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSUserDefaults standardUserDefaults]setObject:@[@""] forKey:@"selectedstoresname"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Select"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedGroup"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedstores"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"featureType"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //storeTagOutName
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"storeTagOutName"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ASGroup"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"levelname"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedManagerAppID"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedDistServer"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedDist"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedDistsname"];
    
    
    
    
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [super viewWillDisappear:animated];
}

-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ( [fromPush1 isEqualToString:@"YES"])
    {
        
        NSArray *viewcontrollers=[self.navigationController viewControllers];
        for (int i=0;i<[viewcontrollers count];i++)
        {
            UIViewController *vc=[viewcontrollers objectAtIndex:i];
            NSString *currentView=NSStringFromClass([vc class]);
            if ([currentView isEqualToString:@"VPViewController"])
            {
                [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
            }
        }
        
    }
    
}

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)nextview
{
    
    NSArray *selectedCells = [districtTable indexPathsForSelectedRows];
    bool selected= false;
    bool other = false;
    
    if (selectedCells.count>0)
    {
        
        for (int i =0; i<[selectedCells count]; i++)
        {
            NSIndexPath *firstIndexPath = [selectedCells objectAtIndex:i];
            NSUInteger row = firstIndexPath.row;
            
            
            if (row==4)
            {
                selected=YES;
            }
            else if (row==0)
            {
                other=YES;
            }
        }
        
        if (selected ==YES)
        {
            [self performSegueWithIdentifier:@"store" sender:self];
        }
        else
        { NSString *fromStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"MessageFrom"];
            if ([fromStr isEqualToString:@"Message"])
            {
                if (selectedCells.count==1 && other)
                {
                    ContactHomeViewController *contactsDetailVC=[self.storyboard instantiateViewControllerWithIdentifier:@"ContactHomeViewController"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"AS" forKey:@"contactname"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"dallas" forKey:@"contactlocation"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"grey" forKey:@"contactstatus"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"contactmanagericon"];
                    [self.navigationController pushViewController:contactsDetailVC animated:YES];
                    /*
                     GroupsDetailViewController *groupDetailController = [self.storyboard instantiateViewControllerWithIdentifier:@"GroupsDetailViewController"];
                     // groupDetailController.contactString=@"Message";
                     [self.navigationController pushViewController:groupDetailController animated:YES];
                     */
                }
                else
                {
                    ContactHomeViewController *contactsDetailVC=[self.storyboard instantiateViewControllerWithIdentifier:@"ContactHomeViewController"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"AS" forKey:@"contactname"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"dallas" forKey:@"contactlocation"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"grey" forKey:@"contactstatus"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"contactmanagericon"];
                    [self.navigationController pushViewController:contactsDetailVC animated:YES];
                    /*
                     GroupsViewController *groupsVC=[self.storyboard instantiateViewControllerWithIdentifier:@"GroupsViewController"];
                     [self.navigationController pushViewController:groupsVC animated:YES];
                     */
                }
            }
            else
            {
                ContactHomeViewController *contactsDetailVC=[self.storyboard instantiateViewControllerWithIdentifier:@"ContactHomeViewController"];
                [[NSUserDefaults standardUserDefaults]setObject:@"AS" forKey:@"contactname"];
                [[NSUserDefaults standardUserDefaults]setObject:@"dallas" forKey:@"contactlocation"];
                [[NSUserDefaults standardUserDefaults]setObject:@"grey" forKey:@"contactstatus"];
                [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"contactmanagericon"];
                [self.navigationController pushViewController:contactsDetailVC animated:YES];
                /*
                 AnnouncementViewController *announcementVC=[self.storyboard instantiateViewControllerWithIdentifier:@"AnnouncementViewController"];
                 //announcementVC.storeNameString=@"Announcement";
                 [self.navigationController pushViewController:announcementVC animated:YES];
                 */
            }
            
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Please select the audience from the list"],[[DiagnosticTool sharedManager] generateScreenCode:@"NewMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"NewMessage"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please select the audience from the list"]]
                                                       delegate: nil
                                              cancelButtonTitle:[dictLang objectForKey:@"OK"]
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSInteger count;
    
    if (section==0)
    {
        count=[everyOneArray count];
    }
    else if (section==1)
    {
        count=[individualArray count];
    }
    else
    {
        count=[groupArray count];
    }
    
    return count;
    
    
    //return [disTrictArray count];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSInteger height;
    
    height=30;
    
    return height;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *tempView=[[UIView alloc]initWithFrame:CGRectMake(0,0,300,30)];
    tempView.backgroundColor=[UIColor whiteColor];
    
    UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(5,0,300,30)];
    tempLabel.backgroundColor=[UIColor clearColor];
    //tempLabel.shadowColor = [UIColor blackColor];
    tempLabel.shadowOffset = CGSizeMake(0,2);
    tempLabel.textColor = kSkybluecolor; //here you can change the text color of header.
    tempLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
    tempLabel.font = [UIFont boldSystemFontOfSize:17];
    //tempLabel.text=@"Header Text";
    
    if (section==0)
    {
        tempLabel.text=[dictLang objectForKey:@"EVERYONE in:"];
    }
    else if (section==1)
    {
        tempLabel.text=[dictLang objectForKey:@"An INDIVIDUAL in:"];
    }
    else
    {
        tempLabel.text=[dictLang objectForKey:@"A GROUP in:"];
    }    [tempView addSubview:tempLabel];
    
    return tempView;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
     cell.hotelName.marqueeType = MLContinuous;
     cell.hotelName.scrollDuration = 15.0;
     cell.hotelName.animationCurve = UIViewAnimationOptionCurveEaseInOut;
     cell.hotelName.fadeLength = 0.0f;
     cell.hotelName.leadingBuffer = 0.0f;
     cell.hotelName.trailingBuffer = 10.0f;
     */
    static NSString *cellIdentifier=@"ListCell";
    ListCell*cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListCell" owner:self options:nil];
        cell = [nib objectAtIndex:2];
        
        
    }
    cell.btn_Info.hidden=YES;
    NSString *fromStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"Controller"];
    if ([fromStr isEqualToString:@"FromNewMessage"])
    {
        /*
         if([[tableView indexPathsForSelectedRows] containsObject:indexPath])
         {
         cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkboxChecked"]];
         
         }
         else
         {
         cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox"]];
         }
         */
    }
    else
    {
        NSString *messageCount =[messageArray objectAtIndex:indexPath.row];
        
        if ([messageCount isEqualToString:@"0"])
        {
            // messageCount =@"";
            cell.messageCount.hidden=YES;
        }
        else
        {
            cell.messageCount.hidden=NO;
            cell.messageCount.text = [messageArray objectAtIndex:indexPath.row];
            cell.messageCount.layer.masksToBounds = YES;
            
            cell.messageCount.layer.cornerRadius=11;
        }
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.list.layer.masksToBounds = YES;
    cell.list.layer.cornerRadius = 5.0;
    
    cell.list_text.marqueeType = MLContinuous;
    cell.list_text.scrollDuration = 8.0;
    cell.list_text.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    cell.list_text.fadeLength = 0.0f;
    cell.list_text.leadingBuffer = 0.0f;
    cell.list_text.trailingBuffer = 10.0f;
    
    if (indexPath.section==0)
    {
        cell.list_text.text =[everyOneArray objectAtIndex:indexPath.row];
        cell.list_Name.text=[dictLang objectForKey:@"Everyone"];
        cell.image_State.image=[UIImage imageNamed:@"group"];
    }
    else if (indexPath.section==1)
    {
        //cell.list_text.text =[individualArray objectAtIndex:indexPath.row];
        cell.list_Name.text=[dictLang objectForKey:@"Individual"];
        cell.image_State.image=[UIImage imageNamed:@"user"];
        
        
        if([[[[individualArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"custom_text"] isKindOfClass:[NSNull class]])
        {
            if([[[[individualArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"theatro_text"] isKindOfClass:[NSNull class]])
            {
                cell.list_text.text=@"No TEXT";
            }
            else
            {
                cell.list_text.text =[[[individualArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"theatro_text"];
                //str=[[[disTrictArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"theatro_text"];
            }
        }
        else
        {
            cell.list_text.text =[[[individualArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"custom_text"];
            //str =[[[disTrictArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"custom_text"];
        }
        
        NSString *grant =[[individualArray objectAtIndex:indexPath.row] objectForKey:@"grants"];
        
        int per = [grant intValue];
        if(per==0)
        {
            cell.list.backgroundColor = [UIColor lightGrayColor];
        }
        
    }
    else
    {
        //cell.list_text.text =[groupArray objectAtIndex:indexPath.row];
        cell.list_Name.text=[dictLang objectForKey:@"Group"];
        cell.image_State.image=[UIImage imageNamed:@"groups"];
        if([[[[groupArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"custom_text"] isKindOfClass:[NSNull class]])
        {
            if([[[[groupArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"theatro_text"] isKindOfClass:[NSNull class]])
            {
                cell.list_text.text=@"No TEXT";
            }
            else
            {
                cell.list_text.text =[[[groupArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"theatro_text"];
                //str=[[[disTrictArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"theatro_text"];
            }
        }
        else
        {
            cell.list_text.text =[[[groupArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"custom_text"];
            //str =[[[disTrictArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"custom_text"];
        }
        
        NSString *grant =[[groupArray objectAtIndex:indexPath.row] objectForKey:@"grants"];
        
        int per = [grant intValue];
        if(per==0)
        {
            cell.list.backgroundColor = [UIColor lightGrayColor];
        }
        
    }
    //[cell.placeButton addTarget:self action:@selector(placeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    //cell.placeButton.titleLabel.font=[UIFont fontWithName:kFontName size:kFontSize];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
    
    NSString *fromStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"MessageFrom"];
    
    
    if ([fromStr isEqualToString:@"Message"])
    {
        
        if (indexPath.section==0)
        {
            NSString *str = [NSString stringWithFormat:@"%@",[everyOneArray objectAtIndex:indexPath.row]];
            if ([str isEqualToString:@"One Store"] ||[str isEqualToString:@"Un magasin"])
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"store" forKey:@"Select"];
                [[NSUserDefaults standardUserDefaults] setObject:@"storeEveryOne" forKey:@"storeselect"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self performSegueWithIdentifier:@"Store" sender:self];
            }
            else if ([str isEqualToString:@"Multiple Stores"] ||[str isEqualToString:@"Plusieurs magasins"])
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"MS" forKey:@"MultiStore"];
                
                [[NSUserDefaults standardUserDefaults] setObject:@"ms" forKey:@"featureType"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] setObject:@"SelectStoresGroup" forKey:@"Select"];
                [self performSegueWithIdentifier:@"selectstore" sender:self];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"all" forKey:@"featureType"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] setObject:@[@"All"] forKey:@"selectedstores"];
                [[NSUserDefaults standardUserDefaults] setObject:@"AllStores" forKey:@"Select"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self performSegueWithIdentifier:@"sendmessage" sender:self];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:@"Everyone" forKey:@"title"];
        }
        else if (indexPath.section==1)
        {
            
            NSString *str = [NSString stringWithFormat:@"%@",[[individualArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
            
            
            NSString *grant =[[individualArray objectAtIndex:indexPath.row] objectForKey:@"grants"];
            int per = [grant intValue];
            
            if ([str isEqualToString:@"Store"])
            {
                if(per==0)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"You don't have permission to this feature."],[[DiagnosticTool sharedManager] generateScreenCode:@"NewMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"NewMessage"],[[DiagnosticTool sharedManager]generateErrorCode:@"You don't have permission to this feature"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    [alert show];
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"store" forKey:@"Select"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"storeselect" forKey:@"storeselect"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [self performSegueWithIdentifier:@"Store" sender:self];
                    /*
                     [[NSUserDefaults standardUserDefaults] setObject:@"store" forKey:@"Select"];
                     [[NSUserDefaults standardUserDefaults] setObject:@"storeselect" forKey:@"storeselect"];
                     [self performSegueWithIdentifier:@"Store" sender:self];
                     */
                }
                
            }
            else if ([str isEqualToString:@"HQ individual"])
            {
                if(per==0)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"You don't have permission to this feature."],[[DiagnosticTool sharedManager] generateScreenCode:@"NewMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"NewMessage"],[[DiagnosticTool sharedManager]generateErrorCode:@"You don't have permission to this feature"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    [alert show];
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"AS Member" forKey:@"featureType"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [[NSUserDefaults standardUserDefaults] setObject:@"HQ" forKey:@"Select"];
                    [self performSegueWithIdentifier:@"selectstore" sender:self];
                }
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:@"Individual" forKey:@"title"];
        }
        else if (indexPath.section==2)
        {
            
            NSString *str = [NSString stringWithFormat:@"%@",[[groupArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
            
            
            NSString *grant =[[groupArray objectAtIndex:indexPath.row] objectForKey:@"grants"];
            int per = [grant intValue];
            
            if([str isEqualToString:@"AS"])
            {
                if(per==0)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"You don't have permission to this feature."],[[DiagnosticTool sharedManager] generateScreenCode:@"NewMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"NewMessage"],[[DiagnosticTool sharedManager]generateErrorCode:@"You don't have permission to this feature"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    [alert show];
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"AS" forKey:@"featureType"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@"ASGroup" forKey:@"ASGroup"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [self performSegueWithIdentifier:@"group" sender:self];
                }
            }
            else if ([str isEqualToString:@"MS"])
            {
                if(per==0)
                {
                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"You don't have permission to this feature."],[[DiagnosticTool sharedManager] generateScreenCode:@"NewMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"NewMessage"],[[DiagnosticTool sharedManager]generateErrorCode:@"You don't have permission to this feature"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    [alert show];
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"MS" forKey:@"MultiStore"];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@"ms" forKey:@"featureType"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [self performSegueWithIdentifier:@"group" sender:self];
                }
                
                
            }
            else if([str isEqualToString:@"SL"])
            {
                if(per==0)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"You don't have permission to this feature."],[[DiagnosticTool sharedManager] generateScreenCode:@"NewMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"NewMessage"],[[DiagnosticTool sharedManager]generateErrorCode:@"You don't have permission to this feature"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    [alert show];
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"OneStore" forKey:@"Select"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"OneStore" forKey:@"storeselect"];
                    [self performSegueWithIdentifier:@"group" sender:self];
                }
            }
            [[NSUserDefaults standardUserDefaults] setObject:@"Group" forKey:@"title"];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"storelist"])
    {
        [segue destinationViewController]; //------>StoreListViewController
    }
    if ([[segue identifier] isEqualToString:@"sendmessage"])
    {
        [segue destinationViewController]; //------>MutilMessageViewController
    }
    if ([[segue identifier] isEqualToString:@"group"])
    {
        [segue destinationViewController]; //------>SelectGroupsViewController
    }
    if ([[segue identifier] isEqualToString:@"selectstore"])
    {
        [segue destinationViewController]; //------>SelectStoresViewController
    }
}


@end
