//
//  NavCollectionViewCell.h
//  MultiStoreNavigation
//
//  Created by sandeepchalla on 11/3/16.
//  Copyright © 2016 sandeepchalla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UIButton *titleButton;

@end
