//
//  SelectStoresViewController.h
//  Managers
//
//  Created by Ravi on 05/10/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Earboxinboxmodel.h"
#import "EarboxDataStore.h"

@interface SelectStoresViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate>
{
    IBOutlet UITableView *storeTable;
    IBOutlet UIButton *allButton,*check_btn;
    IBOutlet UILabel *all_Lbl;
    BOOL buttonSelected;
    IBOutlet UISearchBar *searchBar;
    NSString *levelName;
    NSString *str_Ms;
    IBOutlet UILabel *lbl_Green;
}
@property(weak,nonatomic)IBOutlet NSLayoutConstraint *tableviewYpostion;

-(IBAction)allSelected:(id)sender;
@end
