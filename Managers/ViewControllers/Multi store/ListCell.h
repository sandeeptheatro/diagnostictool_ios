//
//  ListCell.h
//  RoomsToNite
//
//  Created by RoomsToniteMac3 on 25/06/15.
//  Copyright (c) 2015 RoomsToniteMac3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"



@interface ListCell : UITableViewCell


@property (nonatomic, retain)IBOutlet UIImageView *store_Image,*checkBox,*image_State;
@property (nonatomic, retain)IBOutlet UILabel *count_lbl,*anncount_lbl;

@property (nonatomic,weak) IBOutlet UILabel *store_Name,*btnlower;
@property (nonatomic,weak) MarqueeLabel *favorite_Name;
@property (nonatomic,weak) IBOutlet UILabel *list_Name,*list;
@property (nonatomic,weak) UILabel *messgaLbl;
@property (nonatomic,weak) UILabel *messageCount;
@property (nonatomic,weak) UILabel *announcementLbl;
@property (nonatomic,weak) UILabel *announcementCount;
@property (nonatomic,weak) IBOutlet UIButton *btn_Info,*btn_Select,*btn_AsInfo,*btn_AsCheck;
@property (nonatomic,weak) IBOutlet UILabel *lbl_asMember,*lbl_MemberPlace,*lbl_Status;
@property(weak,nonatomic)IBOutlet NSLayoutConstraint *count_lblWidth,*count_lblHeight,*anncount_lblWidth,*anncount_lblHeight;

@property (weak,nonatomic)  IBOutlet MarqueeLabel *list_text,*lbl_AsMemberName;



@end
