//
//  SelectStoresViewController.m
//  Managers
//
//  Created by Ravi on 05/10/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "SelectStoresViewController.h"
#import "ListCell.h"
#import "HttpHandler.h"
#import "Reachability.h"
#import "AlertView.h"
#import "Constant.h"
#import "ActivityIndicatorView.h"
#import "DiagnosticTool.h"

@interface SelectStoresViewController ()
{
    NSMutableArray *storesArray;
    NSMutableArray *selectedStores,*searchResultsAll,*selectedstoresname,*groupSelectArray,*arrayForBool,*finalArray,*groupDistSelectArray;
    NSMutableDictionary *loginDetails;
    NSString *aString;
    NSString *bStr;
    NSMutableArray *checkedArray;
    UIBarButtonItem *chkmanuaaly ;
    ListCell *cell;
    UIBarButtonItem *favoritsButton;
    NSMutableArray *sectionTitleArray;
    NSInteger countAll;
    NSMutableArray *storeChecked;
    NSMutableArray *districtChecked;
    NSMutableArray *district;
    NSMutableArray *stores;
    NSDictionary *dictLang;
}

@end

@implementation SelectStoresViewController

- (void)viewDidLoad
{
    
    MarqueeLabel *_lectureName = [[MarqueeLabel alloc] initWithFrame:CGRectMake(100,20,60, 20) duration:8.0 andFadeLength:10.0f];
    [_lectureName setTextAlignment:NSTextAlignmentCenter];
    [_lectureName setBackgroundColor:[UIColor clearColor]];
    
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    all_Lbl.text=[dictLang objectForKey:@"all"];
   searchBar.placeholder=[dictLang objectForKey:@"Search"];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kNavBackgroundColor,
       NSFontAttributeName:[UIFont fontWithName:kFontName size:13]}];
    countAll=0;
    
    arrayForBool = [[NSMutableArray alloc] init];
    finalArray= [[NSMutableArray alloc] init];
    
    sectionTitleArray = [[NSMutableArray alloc] init];
    
    buttonSelected=NO;
    selectedStores = [[NSMutableArray alloc] init];
    selectedstoresname=[[NSMutableArray alloc]init];
    checkedArray = [[NSMutableArray alloc] init];
    storeChecked = [[NSMutableArray alloc] init];
    districtChecked = [[NSMutableArray alloc] init];
    district=[[NSMutableArray alloc] init];
    stores = [[NSMutableArray alloc] init];
    
    
    //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Ba" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    storesArray=[[NSMutableArray alloc] init];
    NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
    NSString *fromStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"MessageFrom"];
    if ([fromStr isEqualToString:@"Message"])
    {
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"title"] isEqualToString:@"Group"])
        {
            //self.navigationItem.title = [dictLang objectForKey:@"Group Message"];
            
            [_lectureName setText:[dictLang objectForKey:@"Group Message"]];
        }
        else
        {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"title"] isEqualToString:@"Individual"])
            {
                //self.navigationItem.title =[dictLang objectForKey:@"Message to an Individual"];
                
                [_lectureName setText:[dictLang objectForKey:@"Message to an Individual"]];
            }
            else
            {
               // self.navigationItem.title = [NSString stringWithFormat:@"Message to %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"title"]];
                
                [_lectureName setText:[dictLang objectForKey:@"Message to Everyone"]];
            }
            
        }
        
    }
    else
    {
        //self.navigationItem.title = @"Announcement To:";
        [_lectureName setText:@"Announcement To:"];
    }
    
    str_Ms =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
    
    if ([sl isEqualToString:@"SL"] || [sl isEqualToString:@"HQ"])
    {
        allButton.hidden=YES;
        check_btn.hidden=YES;
        all_Lbl.hidden=YES;
        favoritsButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"favorite.png"] style:UIBarButtonItemStyleDone target:self action:@selector(favoritsButtonAction)];
        self.navigationItem.rightBarButtonItem=favoritsButton;
        NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
        
        if ([fav isEqualToString:@"fav"])
        {
            [favoritsButton setEnabled:NO];
        }
        
        
    }
    else
    {
        //        allButton.hidden=NO;
        //        check_btn.hidden=NO;
        //        all_Lbl.hidden=NO;
        storeTable.allowsMultipleSelection = YES;
        NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
        
        
        chkmanuaaly = [[UIBarButtonItem alloc]initWithTitle:[dictLang objectForKey:@"Next"] style:UIBarButtonItemStylePlain target:self action:@selector(nextview)];
        [chkmanuaaly setEnabled:NO];
        favoritsButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"favorite.png"] style:UIBarButtonItemStyleDone target:self action:@selector(favoritsButtonAction)];
        self.navigationItem.rightBarButtonItems=@[chkmanuaaly,favoritsButton];
        
        
        if ([fav isEqualToString:@"fav"])
        {
            [favoritsButton setEnabled:NO];
            all_Lbl.hidden=YES;
        }
        //self.navigationItem.rightBarButtonItem=chkmanuaaly;
        
        if ([str_Ms isEqualToString:@"MS"])
        {
            self.tableviewYpostion.constant=-78;
            allButton.hidden=YES;
            check_btn.hidden=YES;
            all_Lbl.hidden=YES;
            searchBar.hidden=YES;
            //self.tableviewYpostion.constant=-60;
            //self.tableviewYpostion.constant=160;
            //self.navigationItem.title = @"Message To:";
            
            //storesArray=[NSMutableArray arrayWithObjects:@"Dist1",@"Dist2",@"Dist3",@"HDC_A",@"India_02",nil];
        }
        
        
    }
    
    if ([sl isEqualToString:@"HQ"])
    {
        self.tableviewYpostion.constant=-33;
        [self fetchHQMembers];
    }
    else
    {
        if ([str_Ms isEqualToString:@"MS"])
        {
            [self fetchHQMembers];
        }
        else
        {
            [self configAPICall];
        }
        
    }
    
    
    
    //[_lectureName setText:@"I am a moving Label in iphone Application"];
    _lectureName.adjustsFontSizeToFitWidth=NO;
    [_lectureName setAnimationCurve:UIViewAnimationOptionCurveEaseIn];
        _lectureName.marqueeType = MLContinuous;
    [self.navigationItem setTitleView:_lectureName];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)favoritsButtonAction
{
    if (district.count>0 || storesArray.count>0)
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Please enter the favorite name"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please enter the favorite name"]] message:nil delegate:self cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
        alert.alertViewStyle=UIAlertViewStylePlainTextInput;
        [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeASCIICapable];
        
        //[[alert textFieldAtIndex:1] becomeFirstResponder];
        [alert show];
    }
    else
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Empty List, cannot add favorite."],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"Empty List, cannot add favorite"]] message:nil delegate:self cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
        [alert show];
    }
    
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==108)
    {
        [self favoritsButtonAction];
    }
    else
    {
        if (buttonIndex==1)
        {
            UITextField *group = [actionSheet textFieldAtIndex:0];
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            
                group.text= [group.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                if (group.text.length>0 )
                {
                    //group.text= [group.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    NSMutableArray *newArray; //= [[NSMutableArray alloc] init];
                    NSMutableArray *nameArray = [[NSMutableArray alloc]init];
                    newArray= [[EarboxDataStore sharedStore] favoritesArtists];
                    
                    for (int i=0; i<[newArray count]; i++)
                    {
                        Favorites *fav;
                        fav=[newArray objectAtIndex:i];
                        
                        [nameArray addObject:fav.favouriteName];
                    }
                    if ([nameArray containsObject:group.text])
                    {
                       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@ %@ (%@%@%@%@i)",[dictLang objectForKey:@"This"],[[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"]capitalizedString],[dictLang objectForKey:@"name already exists"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"favorite name already exists"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                        alert.tag=108;
                        [alert show];
                    }
                    else
                    {
                    NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
                        NSString *Multistore =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
                    NSString *str =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"MessageFrom"]];
                    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
                    [postData setObject:str forKey:@"PlayAudioType"];
                    [postData setObject:@"Stores" forKey:@"FinalView"];
                    [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"] forKey:@"Groups"];
                    
                    // [postData setObject:@"" forKey:@"ChannelSected"];
                    [postData setObject:group.text forKey:@"FavouriteName"];
                    [postData setObject:sl forKey:@"MessageType"];
                    //[postData setObject:@"" forKey:@"Sender"];//featureType
                    [postData setObject:@"" forKey:@"FeatureType"];
                    [postData setObject:@"MS-MA" forKey:@"Originator"];
                    [postData setObject:@"iOS" forKey:@"PlatForm"];
                    //int rangeLow = 100;
                   // int rangeHigh = 100060;
                    //int randomNumber = arc4random() % (rangeHigh-rangeLow+1) + rangeLow;
                    [postData setObject:@"" forKey:@"FavouriteID"];
                    [postData setObject:@"NewMessage" forKey:@"FavouriteType"];
                    [postData setObject:@"" forKey:@"NodeId"];
                    [postData setObject:@"" forKey:@"ParentId"];
                    [postData setObject:@"" forKey:@"LevelId"];
                    [postData setObject:@"false" forKey:@"StoreScreen"];
                    [postData setObject:@"" forKey:@"storeselect"];
                        [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"title"] forKey:@"title"];
                        
                        [postData setObject:Multistore forKey:@"MultiStore"];
                        if ([Multistore isEqualToString:@"MS"])
                        {
                            NSMutableArray *dist; //= [[NSMutableArray alloc] init];
                            
                            NSMutableArray *storeSel;// =[[NSMutableArray alloc] init];
                            
                            NSMutableArray *distUpload =[[NSMutableArray alloc] init];
                            
                            NSMutableArray *distNamesUpload = [[NSMutableArray alloc] init];
                            
                            NSMutableArray *storesUpload =[[NSMutableArray alloc] init];
                            NSMutableArray *distServer = [[NSMutableArray alloc] init];
                            
                            dist=[checkedArray objectAtIndex:0];
                            
                            storeSel=[checkedArray objectAtIndex:1];
                            
                            
                                if ([dist containsObject:@"1"] || [storeSel containsObject:@"1"] || buttonSelected )
                                {
                                    
                                    if (buttonSelected)
                                        
                                    {
                                        
                                        for (int i=0; i<[district count]; i++)
                                            
                                        {
                                            
                                            [distUpload addObject:[[district objectAtIndex:i] objectForKey:@"nodeId"]];
                                            [distServer addObject:[district objectAtIndex:i]];
                                            [distNamesUpload addObject:[[district objectAtIndex:i] objectForKey:@"nodeName"]];
                                            
                                        }
                                        
                                        
                                        
                                        for (int i=0; i<[storesArray count]; i++)
                                            
                                        {
                                            
                                            
                                            
                                            [storesUpload addObject:[[storesArray objectAtIndex:i] objectForKey:@"store_id"]];
                                            
                                            [selectedstoresname addObject:[[storesArray objectAtIndex:i] objectForKey:@"store_name"]];
                                            
                                        }
                                        
                                    }
                                    
                                    else
                                        
                                    {
                                        
                                        if ([dist containsObject:@"1"])
                                            
                                        {
                                            
                                            for (int i=0; i<[dist count]; i++)
                                                
                                            {
                                                
                                                if ([[dist objectAtIndex:i] isEqualToString:@"1"])
                                                    
                                                {
                                                    
                                                    [distUpload addObject:[[district objectAtIndex:i] objectForKey:@"nodeId"]];
                                                    [distServer addObject:[district objectAtIndex:i]];
                                                    [distNamesUpload addObject:[[district objectAtIndex:i] objectForKey:@"nodeName"]];
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                        if([storeSel containsObject:@"1"])
                                            
                                        {
                                            
                                            for (int i=0; i<[storeSel count]; i++)
                                                
                                            {
                                                
                                                if ([[storeSel objectAtIndex:i] isEqualToString:@"1"])
                                                    
                                                {
                                                    
                                                    [storesUpload addObject:[[storesArray objectAtIndex:i] objectForKey:@"store_id"]];
                                                    
                                                    [selectedstoresname addObject:[[storesArray objectAtIndex:i] objectForKey:@"store_name"]];
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    [postData setObject:storesUpload forKey:@"Stores"];
                                    [postData setObject:selectedstoresname forKey:@"StoreNames"];
                                    [postData setObject:@"" forKey:@"HQTagOutName"];
                                    [postData setObject:@[] forKey:@"HQManagerAppID"];
                                    [postData setObject:@[] forKey:@"Nodes"];
                                    [postData setObject:distUpload forKey:@"MultiStoreNodeId"];
                                    [postData setObject:distNamesUpload forKey:@"MultiStoreNodesNames"];
                                    [postData setObject:@"" forKey:@"levelName"];
                                    
                                }
                                else
                                {
                                    [postData setObject:selectedStores forKey:@"Stores"];
                                    [postData setObject:selectedstoresname forKey:@"StoreNames"];
                                    [postData setObject:@"" forKey:@"HQTagOutName"];
                                    [postData setObject:@[] forKey:@"HQManagerAppID"];
                                    [postData setObject:@[] forKey:@"Nodes"];
                                    [postData setObject:@[] forKey:@"MultiStoreNodeId"];
                                    [postData setObject:@[] forKey:@"MultiStoreNodesNames"];
                                    [postData setObject:@"" forKey:@"levelName"];
                                }

                            
                            
                            
                        }
                        else
                        {
                        [postData setObject:selectedStores forKey:@"Stores"];
                        [postData setObject:selectedstoresname forKey:@"StoreNames"];
                        [postData setObject:@"" forKey:@"HQTagOutName"];
                        [postData setObject:@[] forKey:@"HQManagerAppID"];
                        [postData setObject:@[] forKey:@"Nodes"];
                        [postData setObject:@[] forKey:@"MultiStoreNodeId"];
                        [postData setObject:@[] forKey:@"MultiStoreNodesNames"];
                        [postData setObject:@"" forKey:@"levelName"];
                        }
                    NSDictionary *loginDetails1 =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
                    
                    
                    //[self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
                    NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
                    [myQueueSales addOperationWithBlock:^{
                        NSDictionary *responsDic=[HttpHandler favSave:[loginDetails1 objectForKey:@"twUserName"] password:[loginDetails1 objectForKey:@"twPassword"] jSonStr:postData userScreen:@"SelectStoresVC"];
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            if ([responsDic objectForKey:@"Error"])
                            {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                                [alert show];
                            }
                            else if ([responsDic objectForKey:@"FavouriteID"])
                            {
                            [postData setObject:[responsDic objectForKey:@"FavouriteID"] forKey:@"FavouriteID"];
                            [array addObject:postData];
                            [[Earboxinboxmodel sharedModel]favoritesInsert:array];
                            }
                            
                        }];
                    }];
                    }
                }
                else
                {
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@ (%@%@%@%@i)",[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"],[dictLang objectForKey:@"name cannot be empty"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"favorite name cannot be empty"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    alert.tag=108;
                    [alert show];
                }
        }
    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    
    //self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
    //selectedManagerAppID
    //levelname
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"HeirarchyDistSelected"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"HeirarchyStoresSelected"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"levelname"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedManagerAppID"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"storeTagOutName"];
    //storeTagOutName
    groupSelectArray=[[NSMutableArray alloc] init];
    [groupSelectArray addObjectsFromArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedstores"]];
    groupDistSelectArray = [[NSMutableArray alloc] init];
    [groupDistSelectArray addObjectsFromArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedDist"]];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedDist"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedstores"];
    [[NSUserDefaults standardUserDefaults]setObject:@[@""] forKey:@"selectedstoresname"];
    [[NSUserDefaults standardUserDefaults]setObject:@[] forKey:@"selectedDistsname"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedDistServer"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    [super viewWillAppear:animated];
}

-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ( [fromPush1 isEqualToString:@"YES"])
    {
        
        NSArray *viewcontrollers=[self.navigationController viewControllers];
        for (int i=0;i<[viewcontrollers count];i++)
        {
            UIViewController *vc=[viewcontrollers objectAtIndex:i];
            NSString *currentView=NSStringFromClass([vc class]);
            if ([currentView isEqualToString:@"VPViewController"])
            {
                [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
            }
        }
        
    }
}

-(void)nextview
{
    if (checkedArray.count>0)
    {
        //NSArray *selectedCells = [storeTable indexPathsForSelectedRows];
        if ([str_Ms isEqualToString:@"MS"])
        {
            NSMutableArray *dist = [[NSMutableArray alloc] init];
            
            NSMutableArray *storeSel =[[NSMutableArray alloc] init];
            
            NSMutableArray *distUpload =[[NSMutableArray alloc] init];
            NSMutableArray *storesUpload =[[NSMutableArray alloc] init];
            [[NSUserDefaults standardUserDefaults] setObject:district forKey:@"HeirarchydistunSelected"];
            //levelname
            [[NSUserDefaults standardUserDefaults] setObject:levelName forKey:@"levelname"];
            dist=[checkedArray objectAtIndex:0];
            
            storeSel=[checkedArray objectAtIndex:1];
            
            if ([arrayForBool containsObject:@(1)])
            {
                if ([dist containsObject:@"1"] || [storeSel containsObject:@"1"])
                {
                    
                    if([storeSel containsObject:@"1"])
                        
                    {
                        
                        for (int i=0; i<[storeSel count]; i++)
                            
                        {
                            
                            if ([[storeSel objectAtIndex:i] isEqualToString:@"1"])
                                
                            {
                                
                                [storesUpload addObject:[storesArray objectAtIndex:i]];
                                
                                //[selectedstoresname addObject:[[storesArray objectAtIndex:i] objectForKey:@"store_name"]];
                                
                            }
                            
                        }
                        
                    }
                    if ([dist containsObject:@"1"])
                        
                    {
                        
                        for (int i=0; i<[dist count]; i++)
                            
                        {
                            
                            if ([[dist objectAtIndex:i] isEqualToString:@"1"])
                                
                            {
                                
                                [distUpload addObject:[district objectAtIndex:i]];
                                
                                //[selectedstoresname addObject:[[storesArray objectAtIndex:i] objectForKey:@"store_name"]];
                                
                            }
                            
                        }
                        
                    }
                    
                    
                    NSMutableArray *distchilds=[[NSMutableArray alloc]init];
                    for (int k=0; k<[district count]; k++)
                    {
                        int count=0;
                        for (int g=0; g<[storesUpload count]; g++)
                        {
                            if ([[[[district objectAtIndex:k] objectForKey:@"childs"]valueForKey:@"store_name"] containsObject:[[storesUpload objectAtIndex:g] objectForKey:@"store_name"]])
                            {
                                count++;
                            }
                        }
                        // NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:count],[[district objectAtIndex:k] objectForKey:@"nodeName"], nil];
                        [distchilds addObject:[NSNumber numberWithInt:count]];
                    }
                    for (int i=0; i<[district count]; i++)
                    {
                        
                        NSInteger s=[[[[district objectAtIndex:i] objectForKey:@"childs"]valueForKey:@"store_name"]count];
                        if (s==[[distchilds objectAtIndex:i] intValue])
                        {
                            for (int j=0; j<[distUpload count]; j++)
                            {
                                if ([[[distUpload objectAtIndex:j] objectForKey:@"nodeName"] isEqualToString:[[district objectAtIndex:i] objectForKey:@"nodeName"]] || [distUpload count]==[district count])
                                {
                                    break;
                                }
                                else
                                {
                                    [distUpload addObject:[district objectAtIndex:i]];
                                }
                            }
                            if ([distUpload count]==0)
                            {
                                [distUpload addObject:[district objectAtIndex:i]];
                            }
                        }
                    }
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
                    [[NSUserDefaults standardUserDefaults] setObject:district forKey:@"totalDist"];
                    [[NSUserDefaults standardUserDefaults] setObject:distUpload forKey:@"HeirarchyDistSelected"];
                    [[NSUserDefaults standardUserDefaults] setObject:storesUpload forKey:@"HeirarchyStoresSelected"];
                    [self performSegueWithIdentifier:@"DistHi" sender:self];
                }
                else
                {
                    NSString *strAlert = [NSString stringWithFormat:@"%@ %@ %@",[dictLang objectForKey:@"Please select"],levelName,[dictLang objectForKey:@"or stores from the list"]];
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                          
                                                                    message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",strAlert,[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectStores"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please select stores from the list"]]
                                          
                                                                   delegate: nil
                                          
                                                          cancelButtonTitle:[dictLang objectForKey:@"OK"]
                                          
                                                          otherButtonTitles:nil];
                    
                    [alert show];
                }
                
                
                
                
            }
            else
            {
                NSString *strAlert = [NSString stringWithFormat:@"%@ %@ %@",[dictLang objectForKey:@"Please select"],levelName,[dictLang objectForKey:@"or stores from the list and any one of the section should be expanded"]];
                //
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                      
                                                                message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",strAlert,[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectStores"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please select stores from the list and any one of the section should be expanded"]]
                                      
                                                               delegate: nil
                                      
                                                      cancelButtonTitle:[dictLang objectForKey:@"OK"]
                                      
                                                      otherButtonTitles:nil];
                
                [alert show];
            }
        }
        else
        {
            if ([checkedArray containsObject:@"1"])
            {
                
                if ( buttonSelected==YES || selectedStores.count>0)
                {
                    if (buttonSelected)
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:selectedStores forKey:@"selectedstores"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [[NSUserDefaults standardUserDefaults] setObject:selectedstoresname forKey:@"selectedstoresname"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:selectedStores forKey:@"selectedstores"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [[NSUserDefaults standardUserDefaults] setObject:selectedstoresname forKey:@"selectedstoresname"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                    }
                    //StoreGroup
                    NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
                    if ([sl isEqualToString:@"SL"])
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
                        [self performSegueWithIdentifier:@"StoreGroup" sender:self];
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
                        [self performSegueWithIdentifier:@"sendmessage" sender:self];
                    }
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                    message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",@"Please select stores from the list",[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectStores"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please select stores from the list"]]
                                                                   delegate: nil
                                                          cancelButtonTitle:[dictLang objectForKey:@"OK"]
                                                          otherButtonTitles:nil];
                    [alert show];
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",@"Please select stores from the list",[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectStores"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please select stores from the list"]]
                                                               delegate: nil
                                                      cancelButtonTitle:[dictLang objectForKey:@"OK"]
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Responce is empty"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectStores"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please select stores from the list"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
        
        [alert show];
    }
}

-(IBAction)allSelected:(id)sender
{
    [favoritsButton setEnabled:YES];
    str_Ms =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
    if ([str_Ms isEqualToString:@"MS"])
    {
        
        NSMutableArray *newA = [[NSMutableArray alloc] init];
        NSMutableArray *newB = [[NSMutableArray alloc] init];
        
        if (buttonSelected==NO)
        {
            buttonSelected=YES;
            [checkedArray removeAllObjects];
            [selectedStores removeAllObjects];
            [allButton setImage:[UIImage imageNamed:@"checkboxChecked"] forState:UIControlStateNormal];
            
            for (int i=0; i<[storesArray count]; i++)
            {
                [newB addObject:@"1"];
            }
            
            for (int i=0; i<[district count]; i++)
            {
                [newA addObject:@"1"];
            }
            [checkedArray addObject:newA];
            [checkedArray addObject:newB];
            
            [storeTable reloadData];
        }
        else
        {
            buttonSelected=NO;
            [checkedArray removeAllObjects];
            [selectedStores removeAllObjects];
            for (int i=0; i<[storesArray count]; i++)
            {
                [newB addObject:@"0"];
            }
            
            for (int i=0; i<[district count]; i++)
            {
                [newA addObject:@"0"];
            }
            [checkedArray addObject:newA];
            [checkedArray addObject:newB];
            
            [allButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
            [storeTable reloadData];
            
        }
        [arrayForBool removeAllObjects];
        for (int i=0; i<[sectionTitleArray count]; i++)
        {
            [arrayForBool addObject:[NSNumber numberWithBool:YES]];
        }
        [storeTable reloadData];
    }
else
{
    if (buttonSelected==NO)
    {
        buttonSelected=YES;
        [checkedArray removeAllObjects];
        [selectedStores removeAllObjects];
        [selectedstoresname removeAllObjects];
        [allButton setImage:[UIImage imageNamed:@"checkboxChecked"] forState:UIControlStateNormal];
        
        for (int i=0; i<[storesArray count]; i++)
        {
            id storeID = [[storesArray objectAtIndex:i] objectForKey:@"store_id"];
            [selectedStores addObject:storeID];
            [selectedstoresname addObject:[[storesArray objectAtIndex:i] objectForKey:@"store_name"]];
            [checkedArray addObject:@"1"];
        }
        
        [storeTable reloadData];
    }
    else
    {
        buttonSelected=NO;
        [checkedArray removeAllObjects];
        [selectedStores removeAllObjects];
        [selectedstoresname removeAllObjects];
        for (int i=0; i<[storesArray count]; i++)
        {
            [checkedArray addObject:@"0"];
        }
        
        [allButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        [storeTable reloadData];
        
    }
}
}

-(void)fetchHQMembers
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
       [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectStores"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
        [myQueueSales addOperationWithBlock:^{
            NSDictionary *responsDic=[HttpHandler asMembersAndDistricts:@"SelectStoresVC"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
                if ([sl isEqualToString:@"MS"])
                {
                    NSString *message =[responsDic objectForKey:@"message"];
                    if ([message isEqualToString:@"Success"])
                    {
                        NSArray *sortedArray=[responsDic objectForKey:@"aboveStoreNodes"];
                        levelName =[[responsDic objectForKey:@"levelName"] lowercaseString];
                        
                        if ([[responsDic objectForKey:@"aboveStoreNodes"]  isKindOfClass:[NSNull class]])
                        {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"No HQ members found!"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectStores"],[[DiagnosticTool sharedManager]generateErrorCode:@"No HQ members found"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                            
                            [alert show];
                            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                        }
                        else
                        {
                            NSArray *distFliter = [[NSArray alloc] init];
                            [chkmanuaaly setEnabled:YES];
                            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"nodeName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                            distFliter=[sortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
                            // NSMutableArray *storesHierarchy = [[NSMutableArray alloc] init];
                            for (int i=0; i<[distFliter count]; i++)
                            {
                                if ([[[distFliter objectAtIndex:i] objectForKey:@"childs"] count]>0)
                                {
                                    [district addObject:[distFliter objectAtIndex:i]];
                                }
                            }
                            for (int i=0; i<[district count]; i++)
                            {
                                if ([[[district objectAtIndex:i] objectForKey:@"childs"] count]>0)
                                {
                                    for (int j=0; j<[[[district objectAtIndex:i] objectForKey:@"childs"] count]; j++)
                                    {
                                        [storesArray addObject:[[[district objectAtIndex:i] objectForKey:@"childs"] objectAtIndex:j]];
                                    }
                                    
                                }
                            }
                            NSSortDescriptor *storesort = [NSSortDescriptor sortDescriptorWithKey:@"store_name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                            [storesArray sortUsingDescriptors:[NSArray arrayWithObject:storesort]];
                            NSSortDescriptor *distsort = [NSSortDescriptor sortDescriptorWithKey:@"nodeName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                            [district sortUsingDescriptors:[NSArray arrayWithObject:distsort]];
                            [finalArray addObject:district];
                            [finalArray addObject:storesArray];
                            
                            storeChecked=[[NSMutableArray alloc] init];
                            districtChecked = [[NSMutableArray alloc] init];
                            
                            NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
                            
                            if ([fav isEqualToString:@"fav"])
                            {
                                for (int i=0; i<[district count]; i++)
                                {
                                    BOOL found = NO;
                                    for (int j=0; j<[groupDistSelectArray count]; j++)
                                    {
                                        if ([[[district objectAtIndex:i] objectForKey:@"nodeId"] isEqualToString:[groupDistSelectArray objectAtIndex:j]] )
                                        {
                                            found=YES;
                                            break;
                                        }
                                        
                                    }
                                    if (found)
                                    {
                                        [districtChecked addObject:@"1"];
                                        //id storeID = [[storesArray objectAtIndex:i] objectForKey:@"store_id"];
                                        //[selectedStores addObject:storeID];
                                        //[selectedstoresname addObject:[[storesArray objectAtIndex:i] objectForKey:@"store_name"]];
                                    }
                                    else
                                    {
                                        [districtChecked addObject:@"0"];
                                    }
                                    
                                }
                                
                                
                                for (int i=0; i<[storesArray count]; i++)
                                {
                                    BOOL found = NO;
                                    for (int j=0; j<[groupSelectArray count]; j++)
                                    {
                                        if ([[[storesArray objectAtIndex:i] objectForKey:@"store_id"] intValue] ==[[groupSelectArray objectAtIndex:j] intValue] )
                                        {
                                            found=YES;
                                            break;
                                        }
                                        
                                    }
                                    if (found)
                                    {
                                        [storeChecked addObject:@"1"];
                                        //id storeID = [[storesArray objectAtIndex:i] objectForKey:@"store_id"];
                                        //[selectedStores addObject:storeID];
                                        //[selectedstoresname addObject:[[storesArray objectAtIndex:i] objectForKey:@"store_name"]];
                                    }
                                    else
                                    {
                                        [storeChecked addObject:@"0"];
                                    }
                                }
                                
                                [checkedArray addObject:districtChecked];
                                [checkedArray addObject:storeChecked];
                                
                                NSString * lblStr = [NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Hide All"],levelName];
                                sectionTitleArray=[NSMutableArray arrayWithObjects:lblStr,[dictLang objectForKey:@"Hide All Stores"], nil];
                                for (int i=0; i<[sectionTitleArray count]; i++)
                                {
                                    if (i==0)
                                    {
                                        if (groupDistSelectArray.count>0)
                                        {
                                            //self.tableviewYpostion.constant=0;
                                            allButton.hidden=NO;
                                            check_btn.hidden=NO;
                                            //all_Lbl.hidden=NO;
                                            [arrayForBool addObject:[NSNumber numberWithBool:YES]];
                                        }
                                        else
                                        {
                                            [arrayForBool addObject:[NSNumber numberWithBool:NO]];
                                        }
                                    }
                                    else if (i==1)
                                    {
                                        if (groupSelectArray.count>0)
                                        {
                                            //self.tableviewYpostion.constant=0;
                                            allButton.hidden=NO;
                                            check_btn.hidden=NO;
                                            //all_Lbl.hidden=NO;
                                            [arrayForBool addObject:[NSNumber numberWithBool:YES]];
                                        }
                                        else
                                        {
                                            [arrayForBool addObject:[NSNumber numberWithBool:YES]];
                                        }
                                    }
                                    
                                }
                                
                                NSMutableArray *storesCount=[[NSMutableArray alloc] init];
                                NSMutableArray *distCount= [[NSMutableArray alloc] init];
                                
                                for (int i=0; i<[districtChecked count]; i++)
                                {
                                    if ([[districtChecked objectAtIndex:i] isEqualToString:@"1"])
                                    {
                                        [distCount addObject:@"1"];
                                    }
                                }
                                
                                for (int i=0; i<[storeChecked count]; i++)
                                {
                                    if ([[storeChecked objectAtIndex:i] isEqualToString:@"1"])
                                    {
                                        [storesCount addObject:@"1"];
                                    }
                                }
                                
                                if (storesCount.count==storesArray.count && distCount.count==district.count)
                                {
                                    [allButton setImage:[UIImage imageNamed:@"checkboxChecked.png"] forState:UIControlStateNormal];
                                    buttonSelected=YES;
                                }
                                [storeTable reloadData];
                                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                                
                            }
                            else
                            {
                                for (int i=0; i<[storesArray count]; i++)
                                {
                                    [storeChecked addObject:@"0"];
                                }
                                
                                for (int i=0; i<[district count]; i++)
                                {
                                    [districtChecked addObject:@"0"];
                                }
                                [checkedArray addObject:districtChecked];
                                [checkedArray addObject:storeChecked];
                                
                                NSString * lblStr = [NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Show All"],levelName];
                                sectionTitleArray=[NSMutableArray arrayWithObjects:lblStr,[dictLang objectForKey:@"Hide All Stores"], nil];
                                for (int i=0; i<[sectionTitleArray count]; i++)
                                {
                                    if (i==0)
                                    {
                                        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
                                    }
                                    else
                                    {
                                        [arrayForBool addObject:[NSNumber numberWithBool:YES]];
                                    }
                                    
                                }
                                
                                [storeTable reloadData];
                                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                            }
                        }
                    }
                    else
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@ (%@i)", message,[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                        
                        [alert show];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                        
                    }
                }
                else
                {
                    NSArray *sortedArray=[responsDic objectForKey:@"asMembers"];
                    NSString *message =[responsDic objectForKey:@"message"];
                    if ([message isEqualToString:@"Success"])
                    {
                        
                        
                        if ([[responsDic objectForKey:@"asMembers"]  isKindOfClass:[NSNull class]])
                        {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"No HQ members found!"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectStores"],[[DiagnosticTool sharedManager]generateErrorCode:@"No HQ members found"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                            
                            [alert show];
                            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                        }
                        else
                        {
                            
                            [chkmanuaaly setEnabled:YES];
                            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"tagOutName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                            storesArray=[[sortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
                            
                            
                            NSSortDescriptor *descriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                            [storesArray sortUsingDescriptors:@[descriptor1]];
                            
                            [storeTable reloadData];
                            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                        }
                    }
                    else
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@ (%@i)", message,[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                        
                        [alert show];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                }
            }];
        }];
    }
    
}

-(void)configAPICall
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectStores"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
        [myQueueSales addOperationWithBlock:^{
            NSDictionary *responsDic=[HttpHandler selectStoresV2:@"SelectStoresVC"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                NSArray *sortedArray=[responsDic objectForKey:@"stores"];
                
                if ([[responsDic objectForKey:@"stores"]  isKindOfClass:[NSNull class]])
                {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"No Stores found!"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectStoresVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectStores"],[[DiagnosticTool sharedManager]generateErrorCode:@"No Stores found"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    [alert show];
                }
                else
                {
                    
                    [chkmanuaaly setEnabled:YES];
                    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"store_name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                    storesArray=[[sortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
                    
                    NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
                    
                    if ([fav isEqualToString:@"fav"])
                    {
                        NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
                        if ([sl isEqualToString:@"MS"])
                        {
                            if ([sl isEqualToString:@"MS"])
                            {
                                [self fetchHQMembers];
                            }
                        }
                        else
                        {
                            for (int i=0; i<[storesArray count]; i++)
                            {
                                BOOL found = NO;
                                for (int j=0; j<[groupSelectArray count]; j++)
                                {
                                    if ([[[storesArray objectAtIndex:i] objectForKey:@"store_id"] intValue] ==[[groupSelectArray objectAtIndex:j] intValue] )
                                    {
                                        found=YES;
                                        break;
                                    }
                                    
                                }
                                if (found)
                                {
                                    [checkedArray addObject:@"1"];
                                    id storeID = [[storesArray objectAtIndex:i] objectForKey:@"store_id"];
                                    [selectedStores addObject:storeID];
                                    [selectedstoresname addObject:[[storesArray objectAtIndex:i] objectForKey:@"store_name"]];
                                }
                                else
                                {
                                    [checkedArray addObject:@"0"];
                                }
                            }
                            
                            if (selectedStores.count==sortedArray.count)
                            {
                                [allButton setImage:[UIImage imageNamed:@"checkboxChecked.png"] forState:UIControlStateNormal];
                                buttonSelected=YES;
                            }
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        
                    }
                    
                    NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
                    if ([sl isEqualToString:@"MS"])
                    {
                        [self fetchHQMembers];
                    }
                    else
                    {
                        for (int i=0; i<[storesArray count]; i++)
                        {
                            
                            [checkedArray addObject:@"0"];
                        }
                        
                        [storeTable reloadData];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    
                }
            }];
        }];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    str_Ms =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
    if ([str_Ms isEqualToString:@"MS"] && tableView==storeTable)
    {
        return [sectionTitleArray count];
    }
    
    return 1;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSInteger height;
    
    str_Ms =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
    if ([str_Ms isEqualToString:@"MS"] && tableView==storeTable)
    {
        height=40; ;
    }
    else
    {
        height=0;
    }
    return height;
}

#pragma mark - Creating View for TableView Section

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionView;
    NSString *str_Ms1 =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
    if ([str_Ms1 isEqualToString:@"MS"])
    {
        sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,40)];
        sectionView.tag=section;
        UILabel *cover=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
        cover.backgroundColor=[UIColor whiteColor];
        [sectionView addSubview:cover];
        
        UILabel *viewLabel = [[UILabel alloc] init];
        //UILabel *viewLabel=[[UILabel alloc]initWithFrame:];
        
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        
        
        if (screenSize.width ==414)
            
        {
            viewLabel.frame =CGRectMake(0, 10,self.view.frame.size.width, 30);
        }
        else if (screenSize.width ==375)
        {
            viewLabel.frame =CGRectMake(0, 10,self.view.frame.size.width, 30);
        }
        else
        {
            viewLabel.frame =CGRectMake(0, 10,self.view.frame.size.width, 30);
        }
        
        viewLabel.backgroundColor=[UIColor clearColor];
        sectionView.layer.cornerRadius=5.0;
        sectionView.layer.masksToBounds = YES;
        viewLabel.layer.masksToBounds=YES;
        viewLabel.layer.cornerRadius=5.0;
        viewLabel.textAlignment=NSTextAlignmentCenter;
        viewLabel.textColor=kNavBackgroundColor;
        viewLabel.font=[UIFont systemFontOfSize:19];
        viewLabel.text=[NSString stringWithFormat:@"%@",[sectionTitleArray objectAtIndex:section]];
        [sectionView addSubview:viewLabel];
        /********** Add a custom Separator with Section view *******************/
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(15,70, storeTable.frame.size.width-15, 1)];
        separatorLineView.backgroundColor = [UIColor blackColor];
        // [sectionView addSubview:separatorLineView];
        
        /********** Add UITapGestureRecognizer to SectionView   **************/
        
        UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
        [sectionView addGestureRecognizer:headerTapped];
        
        
    }
    return sectionView;
    
    
}

#pragma mark - Table header gesture tapped
/*
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[sectionTitleArray count]; i++) {
            if (indexPath.section==i) {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
            
        }
        [storeTable reloadData];
        // [storeTable reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationLeft];
        if (indexPath.section==1 && [[arrayForBool objectAtIndex:0] boolValue] && [[arrayForBool objectAtIndex:1] boolValue]) {
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[arrayForBool count]-1 inSection:1];
            [storeTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
        
    }
    
    
}
*/

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[sectionTitleArray count]; i++) {
            if (indexPath.section==i)
            {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
            
        }
        if ([[arrayForBool objectAtIndex:0] boolValue] && [[arrayForBool objectAtIndex:1] boolValue])
        {
            //[sectionTitleArray removeAllObjects];
            NSString *lblStr = [NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Hide All"],levelName];
            sectionTitleArray= [NSMutableArray arrayWithObjects:lblStr,[dictLang objectForKey:@"Hide All Stores"], nil];
        }
        else if(![[arrayForBool objectAtIndex:0] boolValue] && ![[arrayForBool objectAtIndex:1] boolValue])
        {
            //[sectionTitleArray removeAllObjects];
            NSString *lblStr = [NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Show All"],levelName];
            sectionTitleArray= [NSMutableArray arrayWithObjects:lblStr,[dictLang objectForKey:@"Show All Stores"], nil];
        }
        else if([[arrayForBool objectAtIndex:0] boolValue] && ![[arrayForBool objectAtIndex:1] boolValue])
        {
            //[sectionTitleArray removeAllObjects];
            NSString *lblStr = [NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Hide All"],levelName];
            sectionTitleArray= [NSMutableArray arrayWithObjects:lblStr,[dictLang objectForKey:@"Show All Stores"], nil];
        }
        else if(![[arrayForBool objectAtIndex:0] boolValue] && [[arrayForBool objectAtIndex:1] boolValue])
        {
            //[sectionTitleArray removeAllObjects];
            NSString *lblStr = [NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Show All"],levelName];
            sectionTitleArray= [NSMutableArray arrayWithObjects:lblStr,[dictLang objectForKey:@"Hide All Stores"], nil];
        }
        [storeTable reloadData];
        // [storeTable reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationLeft];
        if (indexPath.section==1 && [[arrayForBool objectAtIndex:0] boolValue] && [[arrayForBool objectAtIndex:1] boolValue]) {
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
            [storeTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
        
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSInteger count;
    count=0;
    str_Ms =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
    if ([str_Ms isEqualToString:@"MS"] && tableView==storeTable)
    {
        if ([[arrayForBool objectAtIndex:section] boolValue])
        {
            count=[[finalArray objectAtIndex:section] count];
        }
    }
    else if (tableView != storeTable)
    {
        count= [searchResultsAll count];
    }
    else
    {
        count= [storesArray count];
    }
    return count;
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger height=0;
    NSString *str_as = [[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
    if ([str_as isEqualToString:@"HQ"])
    {
        height=60;
    }
    else
    {
        height=87;
    }
    return height;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"ListCell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListCell" owner:self options:nil];
        NSString *str_as = [[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
        if ([str_as isEqualToString:@"HQ"])
        {
            cell = [nib objectAtIndex:3];
        }
        else
        {
            cell = [nib objectAtIndex:1];
        }
        
        
        
        
    }
    //cell.btn_Info.hidden=YES;
    NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
    if ([sl isEqualToString:@"SL"] ||[sl isEqualToString:@"HQ"])
    {
        cell.btn_Info.hidden=YES;
        cell.btn_Select.hidden=YES;
    }
    else
    {
        cell.btn_Info.layer.masksToBounds = YES;
        cell.btn_Info.layer.cornerRadius = 5.0;
        str_Ms =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
        if ([str_Ms isEqualToString:@"MS"] && tableView==storeTable)
        {
            
            BOOL manyCells  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
            
            /********** If the section supposed to be closed *******************/
            if(!manyCells)
            {
                cell.backgroundColor=[UIColor clearColor];
                
                cell.textLabel.text=@"";
            }
            /********** If the section supposed to be Opened *******************/
            else
            {
                
                cell.btn_Info.hidden=NO;
                cell.btn_Select.hidden=NO;
                [cell.btn_Select addTarget:self action:@selector(selectGroup:) forControlEvents:UIControlEventTouchUpInside];
                //cell.btn_select.tag=indexPath.row;
                NSInteger sectionVar1=0;
                NSString *sectionVar=[NSString stringWithFormat:@"%ld%ld",(long)indexPath.section+1,(long)indexPath.row];
                sectionVar1 = [sectionVar intValue];
                cell.btn_Select.tag=sectionVar1;
                if (buttonSelected)
                {
                    [cell.btn_Info setImage:[UIImage imageNamed:@"checkboxChecked"]
                                   forState:UIControlStateNormal];
                    
                    
                }
                else
                {
                    if([[[checkedArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:@"0"])
                    {
                        [cell.btn_Info setImage:[UIImage imageNamed:@"checkbox"]
                                       forState:UIControlStateNormal];
                    }
                    else
                    {
                        [cell.btn_Info setImage:[UIImage imageNamed:@"checkboxChecked"]
                                       forState:UIControlStateNormal];
                        
                    }
                    
                }
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.favorite_Name.layer.masksToBounds = YES;
                cell.favorite_Name.layer.cornerRadius = 5.0;
            }
        }
        else if(tableView==storeTable)
        {
            cell.btn_Info.hidden=NO;
            cell.btn_Select.hidden=NO;
            [cell.btn_Select addTarget:self action:@selector(selectGroup:) forControlEvents:UIControlEventTouchUpInside];
            cell.btn_Select.tag=indexPath.row;
            
            if (buttonSelected)
            {
                [cell.btn_Info setImage:[UIImage imageNamed:@"checkboxChecked"]
                               forState:UIControlStateNormal];
                
                
            }
            else
            {
                if([[checkedArray objectAtIndex:indexPath.row] isEqualToString:@"0"])
                {
                    [cell.btn_Info setImage:[UIImage imageNamed:@"checkbox"]
                                   forState:UIControlStateNormal];
                    
                    
                }
                else
                {
                    [cell.btn_Info setImage:[UIImage imageNamed:@"checkboxChecked"]
                                   forState:UIControlStateNormal];
                    
                    
                }
                
            }
        }

    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.favorite_Name.layer.masksToBounds = YES;
    cell.favorite_Name.layer.cornerRadius = 5.0;
    
    if (tableView != storeTable)
    {
        NSString *str_M =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
       
        if([str_M isEqualToString:@"HQ"])
        {
            cell.lbl_Status.hidden=YES;
          cell.textLabel.text = [[searchResultsAll objectAtIndex:indexPath.row]objectForKey:@"tagOutName" ];
        }
        else
        {
          cell.textLabel.text = [[searchResultsAll objectAtIndex:indexPath.row]objectForKey:@"store_name" ];
        }
        cell.btn_Info.hidden=YES;
        
        cell.favorite_Name.hidden=YES;
        cell.accessoryView.hidden=YES;
    }
    else
    {
        cell.favorite_Name.hidden=NO;
        cell.accessoryView.hidden=NO;
        str_Ms =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
        if ([str_Ms isEqualToString:@"MS"] )
        {
            if (indexPath.section==0)
            {
                cell.favorite_Name.text =[[[finalArray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row] objectForKey:@"nodeName"];
            }
            else
            {
                cell.favorite_Name.text =[[[finalArray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row] objectForKey:@"store_name"];
            }
            
        }
        else
        {
            if ([sl isEqualToString:@"HQ"])
            {
                storeTable.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
                //groupTable.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
                cell.lbl_Status.layer.masksToBounds=YES;
                cell.lbl_Status.layer.cornerRadius=6.5;
                cell.lbl_asMember.text=[[storesArray objectAtIndex:indexPath.row] objectForKey:@"tagOutName"];
                //cell.favorite_Name.text =[[storesArray objectAtIndex:indexPath.row] objectForKey:@"tagOutName"];
                bool status = [[[storesArray objectAtIndex:indexPath.row] objectForKey:@"status"] boolValue];
                
                if (!status)
                {
                    cell.lbl_Status.backgroundColor=[UIColor lightGrayColor];
                }
                else if (status==YES)
                {
                    cell.lbl_Status.backgroundColor=kGreencolor;
                }
                if ([[[storesArray objectAtIndex:indexPath.row] objectForKey:@"role"] isKindOfClass:[NSNull class]])
                {
                    cell.lbl_MemberPlace.text = @"<NULL>";
                }
                else
                {
                cell.lbl_MemberPlace.text=[[storesArray objectAtIndex:indexPath.row] objectForKey:@"role"];
                }
            }
            else
            {
                cell.favorite_Name.text =[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_name"];
            }
        }
        
        //cell.favorite_Name.text =;
    }
    return cell;
    
}

-(void)selectGroup:(UIButton *)button
{
    [favoritsButton setEnabled:YES];
    str_Ms =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
    if ([str_Ms isEqualToString:@"MS"])
    {
       // NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
        
       
        int buttongIndex=0;
        int buttonSection=0;
        
        NSString *totalStr = [NSString stringWithFormat:@"%ld",(long)button.tag];
        NSString *firstLetter = [totalStr substringToIndex:1];
        
        NSString *remainingStr = [totalStr substringFromIndex:1];
        
        buttongIndex=[remainingStr intValue];
        
        if ([firstLetter isEqualToString:@"1"])
        {
            buttonSection=0;
        }
        else if ([firstLetter isEqualToString:@"2"])
        {
            buttonSection=1;
        }
        
        
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:buttongIndex inSection:buttonSection] ;
        cell = [storeTable cellForRowAtIndexPath:myIP];
        
        if ([[[checkedArray objectAtIndex:buttonSection] objectAtIndex:buttongIndex] isEqualToString:@"1"])
        {
            //[checkedArray replaceObjectAtIndex:myIP.row  withObject:@"0"];
            [[checkedArray objectAtIndex:buttonSection] replaceObjectAtIndex:buttongIndex withObject:@"0"];
            //[selectedStores removeObject:[finalArray objectAtIndex:myIP.row]];
            [cell.btn_Info setImage:[UIImage imageNamed:@"checkbox"]
                           forState:UIControlStateNormal];
        }
        else
        {
            [[checkedArray objectAtIndex:buttonSection] replaceObjectAtIndex:buttongIndex withObject:@"1"];
            //[checkedArray replaceObjectAtIndex:myIP.row withObject:@"1"];
            //[selectedStores addObject:[finalArray objectAtIndex:myIP.row]];
            [cell.btn_Info setImage:[UIImage imageNamed:@"checkboxChecked"]
                           forState:UIControlStateNormal];
        }
        if (checkedArray.count>2)
        {
            if (checkedArray.count==3)
            {
                [checkedArray removeObjectAtIndex:2];
            }
            else if (checkedArray.count==4)
            {
                [checkedArray removeObjectAtIndex:2];
                [checkedArray removeObjectAtIndex:2];
            }
        }
        
        countAll=0;
        for (int i=0; i<[checkedArray count]; i++)
        {
            for (int j=0; j<[[checkedArray objectAtIndex:i] count]; j++)
            {
                if ([[[checkedArray objectAtIndex:i] objectAtIndex:j] isEqualToString:@"1"])
                {
                    countAll++;
                }
            }
        }
        
        if (countAll==storeChecked.count+districtChecked.count)
        {
            buttonSelected=YES;
            [allButton setImage:[UIImage imageNamed:@"checkboxChecked"] forState:UIControlStateNormal];
        }
        else
        {
            buttonSelected=NO;
            [allButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
        
        
    }
    else
    {
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:button.tag inSection:0];
        cell = [storeTable cellForRowAtIndexPath:myIP];
        
        if ([[checkedArray objectAtIndex:myIP.row] isEqualToString:@"1"])
        {
            [checkedArray replaceObjectAtIndex:myIP.row withObject:@"0"];
            id storeID = [[storesArray objectAtIndex:button.tag] objectForKey:@"store_id"];
            [selectedStores removeObject:storeID];
            [selectedstoresname removeObject:[[storesArray objectAtIndex:button.tag] objectForKey:@"store_name"]];
            [cell.btn_Info setImage:[UIImage imageNamed:@"checkbox"]
                           forState:UIControlStateNormal];
        }
        else
        {
            [checkedArray replaceObjectAtIndex:myIP.row withObject:@"1"];
            id storeID = [[storesArray objectAtIndex:button.tag] objectForKey:@"store_id"];
            [selectedStores addObject:storeID];
            [selectedstoresname addObject:[[storesArray objectAtIndex:button.tag] objectForKey:@"store_name"]];
            [cell.btn_Info setImage:[UIImage imageNamed:@"checkboxChecked"]
                           forState:UIControlStateNormal];
        }
        
        if (selectedStores.count==storesArray.count)
        {
            buttonSelected=YES;
            [allButton setImage:[UIImage imageNamed:@"checkboxChecked"] forState:UIControlStateNormal];
        }
        else
        {
            buttonSelected=NO;
            [allButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    str_Ms =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
    if ([str_Ms isEqualToString:@"MS"] && tableView==storeTable)
    {
       // [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:NO]];
    }
    else
    {
        if (tableView != storeTable)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
            
            [selectedStores removeAllObjects];
            [selectedstoresname removeAllObjects];
            NSString *strAS = [[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
            if ([strAS isEqualToString:@"HQ"])
            {
                [selectedStores addObject:[NSString stringWithFormat:@"%@",[[searchResultsAll objectAtIndex:indexPath.row] objectForKey:@"managerAppId"]]];
                [selectedstoresname addObject:[[searchResultsAll objectAtIndex:indexPath.row] objectForKey:@"tagOutName"]];
                
                [[NSUserDefaults standardUserDefaults] setObject:selectedStores forKey:@"selectedManagerAppID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] setObject:[[searchResultsAll objectAtIndex:indexPath.row] objectForKey:@"tagOutName"]  forKey:@"storeTagOutName"];
                [[NSUserDefaults standardUserDefaults] setObject:selectedstoresname  forKey:@"selectedstoresname"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            else
            {
                [selectedStores addObject:[NSString stringWithFormat:@"%@",[[searchResultsAll objectAtIndex:indexPath.row] objectForKey:@"store_id"]]];
                [selectedstoresname addObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_name"]];
            }
            
            NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
            if ([sl isEqualToString:@"SL"])
            {
                [[NSUserDefaults standardUserDefaults] setObject:selectedStores forKey:@"selectedstores"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] setObject:selectedstoresname  forKey:@"selectedstoresname"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self performSegueWithIdentifier:@"StoreGroup" sender:self];
            }
            else
            {
                [self performSegueWithIdentifier:@"sendmessage" sender:self];
            }
            
        }
        else
        {
            NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
            if ([sl isEqualToString:@"SL"])
            {
                [selectedStores removeAllObjects];
                [selectedstoresname removeAllObjects];
                [selectedStores addObject:[NSString stringWithFormat:@"%@",[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_id"]]];
                [selectedstoresname addObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_name"]];
                [[NSUserDefaults standardUserDefaults] setObject:selectedStores forKey:@"selectedstores"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] setObject:selectedstoresname  forKey:@"selectedstoresname"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] setObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_id"] forKey:@"storeID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self performSegueWithIdentifier:@"StoreGroup" sender:self];
            }
            else if([sl isEqualToString:@"HQ"])
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
                [selectedStores removeAllObjects];
                [selectedstoresname removeAllObjects];

                [selectedStores addObject:[NSString stringWithFormat:@"%@",[[storesArray objectAtIndex:indexPath.row] objectForKey:@"managerAppId"]]];
                [selectedstoresname addObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"tagOutName"]];
                
                [[NSUserDefaults standardUserDefaults] setObject:selectedStores forKey:@"selectedManagerAppID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] setObject:selectedstoresname  forKey:@"selectedstoresname"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] setObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"tagOutName"]  forKey:@"storeTagOutName"];
                [self performSegueWithIdentifier:@"sendmessage" sender:self];
            }
        }
    }
    /*else
    {
        //
        NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
        if ([sl isEqualToString:@"SL"])
        {
            [selectedStores removeAllObjects];
            [selectedstoresname removeAllObjects];
            [selectedStores addObject:[NSString stringWithFormat:@"%@",[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_id"]]];
            [selectedstoresname addObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_name"]];
            [[NSUserDefaults standardUserDefaults] setObject:selectedStores forKey:@"selectedstores"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSUserDefaults standardUserDefaults] setObject:selectedstoresname  forKey:@"selectedstoresname"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_id"] forKey:@"storeID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self performSegueWithIdentifier:@"StoreGroup" sender:self];
        }
        else
        {
            if (buttonSelected==NO)
            {
                if (!([selectedStores count]==[storesArray count]))
                {
                    if (![selectedStores containsObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_id"]])
                    {
                        [selectedStores addObject:[NSString stringWithFormat:@"%@",[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_id"]]];
                        [selectedstoresname addObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_name"]];
                        [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
                        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkboxChecked"]];
                    }
                    else
                    {
                        [selectedStores removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_id"]];
                        [selectedstoresname removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_name"]];
                        [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
                        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox"]];
                    }
                }
                else
                {
                    [selectedStores removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_id"]];
                    [selectedstoresname removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_name"]];
                    [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
                    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox"]];
                    
                }
                
            }
            else
            {
                buttonSelected=NO;
                [selectedStores removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_id"]];
                [selectedstoresname removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_name"]];
                [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
                UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox"]];
                
            }
            
            
            
            if (selectedStores.count==storesArray.count)
            {
                buttonSelected=YES;
                [allButton setImage:[UIImage imageNamed:@"checkboxChecked"] forState:UIControlStateNormal];
            }
            else
            {
                [allButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
            }
            
        }
    }*/
    
}


-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
    if ([sl isEqualToString:@"SL"])
    {
        //[self performSegueWithIdentifier:@"StoreGroup" sender:self];
    }
    /*else
    {
        if (buttonSelected==NO)
        {
            if (!([selectedStores count]==[storesArray count]))
            {
                [selectedStores removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_id"]];
                [selectedstoresname removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_name"]];
                [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
                UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox"]];
            }
            else
            {
                [selectedStores addObject:[NSString stringWithFormat:@"%@",[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_id"]]];
                [selectedstoresname addObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_name"]];
                [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
                UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkboxChecked"]];
            }
            
        }
        else
        {
            if (selectedStores.count==storesArray.count)
            {
                [selectedStores removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_id"]];
                [selectedstoresname removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_name"]];
                [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
                UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox"]];
                
            }
            else
            {
                [selectedStores addObject:[NSString stringWithFormat:@"%@",[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_id"]]];
                [selectedstoresname addObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"store_name"]];
                [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
                UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkboxChecked"]];
                
            }
        }
        
        
        if (selectedStores.count==storesArray.count)
        {
            buttonSelected=YES;
            [allButton setImage:[UIImage imageNamed:@"checkboxChecked"] forState:UIControlStateNormal];
        }
        else
        {
            buttonSelected=NO;
            [allButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
    }*/
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"sendmessage"])
    {
        [segue destinationViewController]; //---->MutilMessageViewController
    }
    if ([[segue identifier] isEqualToString:@"StoreGroup"])
    {
        [segue destinationViewController];  //---->SelectGroupsViewController
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


#pragma mark - UISearchDisplayController delegate methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    
    NSMutableArray *totalNameArray=[[NSMutableArray alloc]init];
    searchResultsAll=[[NSMutableArray alloc]init];
    
    /*
     NSPredicate *tagOutNamepredicate = [NSPredicate predicateWithFormat:
     @"store_name BEGINSWITH[cd] %@", searchText];
     
     // NSPredicate *tagOutNamepredicate = [NSPredicate predicateWithFormat:@"self.youProperty contains[c] %@", searchText];
     
     if (tagOutNamepredicate!=nil)
     {
     [totalNameArray addObjectsFromArray: [storesArray filteredArrayUsingPredicate:tagOutNamepredicate]];
     
     }
     
     */
    
    
   str_Ms =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
    if ([str_Ms isEqualToString:@"MS"])
    {
        [storesArray removeAllObjects];
        
        if([[arrayForBool objectAtIndex:0] boolValue])
        {
            for (int i=0; i<[district count]; i++)
            {
                [storesArray addObject:[district objectAtIndex:i]];
            }
        }
        if ([[arrayForBool objectAtIndex:1] boolValue])
        {
            for (int i=0; i<[stores count]; i++)
            {
                [storesArray addObject:[stores objectAtIndex:i]];
            }
        }

    }
    for (int i =0;i<[storesArray count];i++)
        
    {
        NSString *city;
        // NSString* city1 = [city stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *str= [[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
        if ([str isEqualToString:@"HQ"])
        {
            city = [[storesArray objectAtIndex:i] objectForKey:@"tagOutName"];
        }
        else
        {
            city = [[storesArray objectAtIndex:i] objectForKey:@"store_name"];
        }
        
        if ([str_Ms isEqualToString:@"MS"])
        {
            
        }
        
        NSRange range=[city rangeOfString:searchText options:NSCaseInsensitiveSearch];
        if (range.length >0)
        {
            if (![searchResultsAll containsObject:city]) {
                [searchResultsAll addObject:[storesArray objectAtIndex:i]];
            }
        }
        
    }
    
    NSLog(@"SelectStoresViewController-%@",searchResultsAll);
    if (totalNameArray.count>0)
    {
        [searchResultsAll addObjectsFromArray:totalNameArray];
    }
    
}


-(BOOL)searchDisplayController:(UISearchController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[controller.searchBar scopeButtonTitles]
                                      objectAtIndex:[controller.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}


@end
