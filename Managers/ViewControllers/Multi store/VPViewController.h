//
//  VPViewController.h
//  Managers
//
//  Created by Ravi on 30/09/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VPViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>
{
    IBOutlet UITableView *storesTableview;
    IBOutlet UILabel *lbl_Green;
    BOOL nointernet;
  
}
@property (nonatomic, retain)NSTimer *deletetimer;
+(VPViewController *)getsharedInstance;
@end
