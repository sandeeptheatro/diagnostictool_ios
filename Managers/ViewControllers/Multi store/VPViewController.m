//
//  VPViewController.m
//  Managers
//
//  Created by Ravi on 30/09/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "VPViewController.h"
#import "ListCell.h"
#import "Constant.h"
#import "SlideNavigationController.h"
#import "HttpHandler.h"
#import "UAPush.h"
#import "UAirship.h"
#import "Reachability.h"
#import "AlertView.h"
#import "NSLogger.h"
#import "ActivityIndicatorView.h"
#import "EarboxDataStore.h"
#import "NSDateClass.h"
#import "EarboxInbox.h"
#import "AnnouncementInbox.h"
#import "AudioManager.h"
#import "InboxViewController.h"
#import "C2ScanningViewController.h"
#import "AppDelegate.h"
#import "DiagnosticTool.h"

@interface VPViewController ()
{
    NSArray *storesListArray;
    NSArray *searchResults;
    NSDictionary *loginDetails;
    NSMutableArray * listImages;
    UIView *alphaView;
    int inboxcount,messagecount,announcementcount;
    NSDictionary *dictLang;
}

@end

@implementation VPViewController
@synthesize deletetimer;
static VPViewController  *sharedMgr = nil;

+(VPViewController *)getsharedInstance {
    if (sharedMgr == nil)
    {
        sharedMgr = [[VPViewController alloc] init];
    }
    return sharedMgr;
}

-(void)updateExpiryTime
{
    [self performSelectorOnMainThread:@selector(updatetimer) withObject:nil waitUntilDone:NO];
}

-(void)updatetimer
{
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        if ([[[UAirship push]channelID] length]!=0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:[[UAirship push]channelID] forKey:@"deviceTokenString"];
            [HttpHandler configsV2:@"VPVC"];
        }
        
    }
    
    NSArray *messageArray= [[EarboxDataStore sharedStore] inboxartists:@"EarboxInbox" calltype:4];
    for (int i=0; i<[messageArray count]; i++)
    {
        EarboxInbox *messageinboxdata=[messageArray objectAtIndex:i];
        if ([[NSDateClass ttlformate:messageinboxdata.ttl] isEqualToString:@"0"]) //&& [messageinboxdata.status isEqualToString:@"HEARD"])
        {
            [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@%d%d", messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid]];
            
            if ([messageinboxdata.status isEqualToString:@"new"])
            {
                 messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
                if (messagecount==0)
                {
                    messagecount=0;
                }
                else
                {
                    messagecount--;
                }
                int privateMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"privateMessageCountdef"] intValue];
               
                if (privateMessageCountdef!=0)
                {
                     privateMessageCountdef--;
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:privateMessageCountdef] forKey:@"privateMessageCountdef"];
                }

                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagecount"];
                int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            [[EarboxDataStore sharedStore] deleteEarboxInbox:messageinboxdata];
             [storesTableview reloadData];
            
        }
    }
    
    NSArray *announcementArray= [[EarboxDataStore sharedStore] artists:@"AnnouncementInbox" storeid:0 calltype:0];
    for (int i=0; i<[announcementArray count]; i++)
    {
        AnnouncementInbox *announcementinboxdata=[announcementArray objectAtIndex:i];
        
        if ([[NSDateClass ttlformate:announcementinboxdata.ttl] isEqualToString:@"0"]) //&& [announcementinboxdata.status isEqualToString:@"HEARD"])
        {
            [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@%d%d",announcementinboxdata.callId,announcementinboxdata.callType,announcementinboxdata.storeid]];
            
            if ([announcementinboxdata.status isEqualToString:@"new"])
            {
                 announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementcount"] intValue];
                if (announcementcount==0)
                {
                    announcementcount=0;
                }
                else
                {
                    announcementcount--;
                }
                int storeAnnouncementCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeAnnouncementCountdef"] intValue];
                
                if (storeAnnouncementCountdef!=0)
                {
                    storeAnnouncementCountdef--;
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:storeAnnouncementCountdef]
                                                             forKey:@"storeAnnouncementCountdef"];
                }
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:announcementcount]
                                                         forKey:@"announcementcount"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            [[EarboxDataStore sharedStore] deleteAnnouncementInbox:announcementinboxdata];
             [storesTableview reloadData];
            
        }
    }
    
    NSArray *groupmessageArray= [[EarboxDataStore sharedStore] inboxartists:@"EarboxInbox" calltype:17];
    for (int i=0; i<[groupmessageArray count]; i++)
    {
        EarboxInbox *messageinboxdata=[groupmessageArray objectAtIndex:i];
        if ([[NSDateClass ttlformate:messageinboxdata.ttl] isEqualToString:@"0"]) //&& [messageinboxdata.status isEqualToString:@"HEARD"])
        {
            [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@%d%d", messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid]];
            
            if ([messageinboxdata.status isEqualToString:@"new"])
            {
                 messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
                if (messagecount==0)
                {
                    messagecount=0;
                }
                else
                {
                    messagecount--;
                }
                int groupMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"groupMessageCountdef"] intValue];
                
                if (groupMessageCountdef!=0)
                {
                    groupMessageCountdef--;
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:groupMessageCountdef] forKey:@"groupMessageCountdef"];
                }
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagecount"];
                int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            [[EarboxDataStore sharedStore] deleteEarboxInbox:messageinboxdata];
             [storesTableview reloadData];
        }
    }
    
    NSArray *everyonemessageArray= [[EarboxDataStore sharedStore] inboxartists:@"EarboxInbox" calltype:33];
    for (int i=0; i<[everyonemessageArray count]; i++)
    {
        EarboxInbox *messageinboxdata=[everyonemessageArray objectAtIndex:i];
        if ([[NSDateClass ttlformate:messageinboxdata.ttl] isEqualToString:@"0"]) //&& [messageinboxdata.status isEqualToString:@"HEARD"])
        {
            [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@%d%d", messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid]];
            
            if ([messageinboxdata.status isEqualToString:@"new"])
            {
                messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
                if (messagecount==0)
                {
                    messagecount=0;
                }
                else
                {
                    messagecount--;
                }
                int everyoneMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"everyoneMessageCountdef"] intValue];
                
                if (everyoneMessageCountdef!=0)
                {
                    everyoneMessageCountdef--;
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:everyoneMessageCountdef] forKey:@"everyoneMessageCountdef"];
                }

                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagecount"];
                int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            [[EarboxDataStore sharedStore] deleteEarboxInbox:messageinboxdata];
            [storesTableview reloadData];
        }
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"pushnotificationcount" object:@"messagecount"];
   // [self inboxtotalcount];
}

-(void)uploadlogfile
{
    NSString *str=[NSString stringWithFormat:@"%@",[[NSLogger sharedInstance] getdata]];
    NSData *file=[NSData dataWithContentsOfFile:str];
    long cacheSize =([file length]/1000);
    //cacheSize = (cacheSize/1024);
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus == NotReachable)
    {
        //No internet
    }
    else if (internetStatus == ReachableViaWiFi)
    {
        //WiFi
    }
    else if (internetStatus == ReachableViaWWAN)
    {
        //3G
    }
    if (file.length!=0)
    {
        int logUpload=[[[[NSUserDefaults standardUserDefaults]objectForKey:@"appConfig"] objectForKey:@"logUpload"] intValue];
        if (![[[[NSUserDefaults standardUserDefaults]objectForKey:@"appConfig"] objectForKey:@"logUpload"] isKindOfClass:[NSNull class]] && logUpload)
        {
            if (![[[[NSUserDefaults standardUserDefaults]objectForKey:@"appConfig"] objectForKey:@"logSize"] isKindOfClass:[NSNull class]])
            {
                if (cacheSize>9216)
                {
                    NSLog(@"VPViewController-Log file reached more than 10MB");
                    [[NSLogger sharedInstance]removefilePath];
                   [[NSLogger sharedInstance]writeNSLogToFile];
                }
                
                else if (cacheSize>[[[[NSUserDefaults standardUserDefaults]objectForKey:@"appConfig"] objectForKey:@"logSize"] intValue])
                {
                    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
                    [myQueue addOperationWithBlock:^{
                        
                        [HttpHandler uploadlogfiles:file userScreen:@"VPVC"];
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            
                            
                        }];
                    }];
                    
                }
            }
        }
        else
        {
            if (cacheSize>9216)
            {
                NSLog(@"VPViewController-Log file reached more than 10MB");
                [[NSLogger sharedInstance]removefilePath];
                [[NSLogger sharedInstance]writeNSLogToFile];
            }
        }
    }
}

-(void)inboxtotalcount
{
    loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        if (nointernet==NO)
        {
            nointernet=YES;
            [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"VPVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"UnheardV2"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        }
        
        
    }
    else
    {
        nointernet=NO;
        NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
        [myQueueSales addOperationWithBlock:^{
            NSDictionary *responsDic=[HttpHandler unheardV2:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:[[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"] intValue] userScreen:@"VPVC"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if([responsDic objectForKey:@"earBoxUnheard"] && ![[responsDic objectForKey:@"earBoxUnheard"] isKindOfClass:[NSNull class]])
                {
                    inboxcount=[[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"announcement"] intValue]+[[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"groupMessage"] intValue]+[[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"message"] intValue]+[[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"everyone"] intValue];
                    messagecount=[[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"groupMessage"] intValue]+[[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"message"] intValue]+[[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"everyone"] intValue];
                    announcementcount=[[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"announcement"] intValue];
                    
                    [storesTableview reloadData];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:inboxcount] forKey:@"badgecount"];
                    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:inboxcount];
                     NSData *announcementEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:responsDic];
                     [[NSUserDefaults standardUserDefaults] setObject:announcementEncodedObject forKey:@"inboxcount"];
                }
            }];
        }];
    }
    
}


-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    //NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    //NSString* version = [infoDict objectForKey:@"NSCameraUsageDescription"];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Language"]==nil)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Localizable"
                                                         ofType:@"strings"
                                                    inDirectory:nil
                                                forLocalization:@"en"];
        
        // compiled .strings file becomes a "binary property list"
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
        
        [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"Language"];
        [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"ChoosedLang"];
        NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
        [[NSUserDefaults standardUserDefaults] setObject:@"english" forKey:@"Languageselected"];
        //[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpathGlobal"]
        [[NSUserDefaults standardUserDefaults] setObject:serverPath forKey:@"serverpathGlobal"];
    }
    else
    {
        
    }
    
 dictLang=[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    //[self featureToggle];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }


    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:0] forKey:@"storeid"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kNavBackgroundColor,
       NSFontAttributeName:[UIFont fontWithName:kFontName size:13]}];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PasswordChanged"];
    deletetimer= [NSTimer scheduledTimerWithTimeInterval: 60.0 target: self selector: @selector(updateExpiryTime) userInfo: nil repeats: YES];
    [[NSUserDefaults standardUserDefaults]setObject:@"storeview" forKey:@"storeview"];
    storesListArray=[[NSMutableArray alloc]init];
    
    [self configAPICall];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"VP" forKey:@"CurrentScreen"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    alphaView=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    alphaView.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.0];
    [self.view addSubview:alphaView];
    self.navigationController.navigationBar.hidden=NO;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    tapGestureRecognizer.delegate = self;
    [alphaView addGestureRecognizer:tapGestureRecognizer];
    alphaView.hidden=YES;
    
    [[UIBarButtonItem appearance] setTintColor:kNavBackgroundColor];
    
    // Create your image
    UIImage *image = [UIImage imageNamed: @"logo"];
    UIImageView *imageview = [[UIImageView alloc] initWithImage: image];
    
    // set the text view to the image view
    self.navigationItem.titleView = imageview;
    [super viewDidLoad];
    
    listImages =[[NSMutableArray alloc]init];
    
    [listImages  addObject:[UIImage imageNamed:@"shopping"]];
    
    [listImages  addObject:[UIImage imageNamed:@"inbox"]];
    
    [listImages  addObject:[UIImage imageNamed:@"star"]];
    
    [listImages  addObject:[UIImage imageNamed:@"newmessage"]];
    // Do any additional setup after loading the view.
}

-(void)featureToggle
{
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];

    NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
    [myQueueSales addOperationWithBlock:^{
        NSArray *featureToggleArray=[HttpHandler featureToggle:[loginDetails objectForKey:@"twUserName"] password:[loginDetails objectForKey:@"twPassword"] userScreen:@"VPVC"];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            /*[
             {
             "state": 2,
             "name": "SwitchToCommunicator"
             },
             {
             "state": 0,
             "name": "CommunicationMode"
             },
             {
             "state": 1,
             "name": "PNSFilter"
             },
             {
             "state": 3,
             "name": "BluetoothSupport"
             }
             ]
             */
            if ([[featureToggleArray objectAtIndex:0]objectForKey:@"Error"])
            {
                
            }
            else
            {
            for (int i=0; i<[featureToggleArray count]; i++)
            {
                int stateint;
                NSString *nameStr=[[featureToggleArray objectAtIndex:i] objectForKey:@"name"];
                if ([nameStr isEqualToString:@"SwitchToCommunicator"])
                {
                    stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                    
                    if (stateint==1)
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"SwitchtoC2"];
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"SwitchtoC2"];
                    }
                }
                else if ([nameStr isEqualToString:@"CommunicationMode"])
                {
                    stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                    if (stateint==3)
                    {
                        //instore
                        AppDelegate *AppD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        //AppDelegate *delegate=[[AppDelegate alloc]init];
                        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"UMA"];
                        [AppD checkNetworkStatus:nil];
                    }
                    else if (stateint==0)
                    {
                        //HMA
                        AppDelegate *AppD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        //AppDelegate *delegate=[[AppDelegate alloc]init];
                        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"UMA"];
                        [AppD checkNetworkStatus:nil];;
                    }
                }
                else if ([nameStr isEqualToString:@"PNSFilter"])
                {
                    stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                    if (stateint==1)
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"NotificationFilter"];
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"NotificationFilter"];
                    }
                }
                else if ([nameStr isEqualToString:@"BluetoothSupport"])
                {
                    stateint=[[[featureToggleArray objectAtIndex:i] objectForKey:@"state"] intValue];
                }
            }
            
            }
        }];
    }];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)configAPICall
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"VPVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"ConfigsV2"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
        [myQueueSales addOperationWithBlock:^{
            NSDictionary *responsDic=[HttpHandler configsV2:@"VPVC"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if ([[responsDic objectForKey:@"announcement"]  isKindOfClass:[NSNull class]] ||[[responsDic objectForKey:@"configs"]  isKindOfClass:[NSNull class]]||[[responsDic objectForKey:@"message"]  isKindOfClass:[NSNull class]] || [responsDic objectForKey:@"Error"])
                {
                    if ([responsDic objectForKey:@"Error"])
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                        [alert show];
                    }
                    else
                    {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"MS user not configured"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    [alert show];
                    }
                    
                }
                else
                {
                    NSMutableArray *sortArray;//=[[NSMutableArray alloc] init];
                    
                    sortArray= [responsDic objectForKey:@"configs"];
                    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                    storesListArray=[sortArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
                    
                    
                    //storesListArray =[responsDic objectForKey:@"configs"];
                    if (storesListArray.count>0)
                    {
                        NSData *announcementEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:[responsDic objectForKey:@"announcement"]];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:announcementEncodedObject forKey:@"config_announcement"];
                        NSData *messageEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:[responsDic objectForKey:@"message"]];
                        [[NSUserDefaults standardUserDefaults] setObject:messageEncodedObject forKey:@"config_message"];
                        [storesTableview reloadData];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    else
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"MS user not configured"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                        [alert show];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    //announcement
                    
                }
            }];
        }];
    }
}

-(void)getFeatureList
{
    loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        
    }
    else
    {
        NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
        [myQueueSales addOperationWithBlock:^{
            NSDictionary *featureslist=[HttpHandler salesEnabled:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:@"Tx874874" twMoment:@"46746.6877" userScreen:@"VPVC"];
            NSArray *mappFeatures=[featureslist objectForKey:@"mappFeatures"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                if (![mappFeatures isKindOfClass:[NSNull class]])
                {
                    [[NSUserDefaults standardUserDefaults]setObject:mappFeatures forKey:@"salesenable"];
                }
                
                NSData *announcementEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:[featureslist objectForKey:@"uiElements"]];
                [[NSUserDefaults standardUserDefaults]setObject:announcementEncodedObject forKey:@"uiElements"];
                if (![[featureslist objectForKey:@"appConfig"] isKindOfClass:[NSNull class]])
                {
                    if (![[[featureslist objectForKey:@"appConfig"] objectForKey:@"logSize"] isKindOfClass:[NSNull class]] && ![[[featureslist objectForKey:@"appConfig"] objectForKey:@"logUpload"] isKindOfClass:[NSNull class]])
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:[featureslist objectForKey:@"appConfig"] forKey:@"appConfig"];
                        [self uploadlogfile];
                    }
                    else
                    {
                        NSLog(@"VPViewController-feature list appconfig logsize/logupload is null");
                    }
                }
                else
                {
                    
                }
            }];
        }];
    }
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushnotificationcount" object:nil];
    [super viewWillDisappear:animated];
}

-(void)pushnotificationcount:(NSNotification *)sender
{
    [self inboxtotalcount];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(configAPICall) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
 // [[NSUserDefaults standardUserDefaults]objectForKey:@"title"]
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"title"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"storeselect"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MultiStore"];
    
    [[NSUserDefaults standardUserDefaults]setObject:@[@""] forKey:@"selectedstoresname"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Select"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedGroup"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedstores"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"featureType"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //storeTagOutName
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"storeTagOutName"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ASGroup"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"levelname"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedManagerAppID"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedDistServer"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedDist"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedDistsname"];
    
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"killcommunicator"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"FromFav"];
    
    [storesTableview reloadData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushnotificationcount:) name:@"pushnotificationcount" object:nil];
    [self inboxtotalcount];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification1) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification1) name:@"localnotification" object:nil];
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
   // NSString *fromPush2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForC2"];
    /*if ([fromPush1 isEqualToString:@"YES"] && [fromPush2 isEqualToString:@"YES"])
    {
         [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"pushForC2"];
        [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
        C2ScanningViewController *contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"C2ScanningViewController"];
        [[SlideNavigationController sharedInstance] pushViewController:contactsController animated:YES];
    }
   else*/ if ([fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush1 isEqualToString:@"YES"])
        {
            //[str isEqualToString:@"Inbox"]
            for (int i=0; i<[storesListArray count]; i++)
            {
                NSString *str = [NSString stringWithFormat:@"%@",[[storesListArray objectAtIndex:i] objectForKey:@"name"]];
                if ([str isEqualToString:@"Inbox"])
                {
                    NSString *grant =[[storesListArray objectAtIndex:i] objectForKey:@"grants"];
                    
                    int per = [grant intValue];
                    if(!(per==0))
                    {
                        [self performSegueWithIdentifier:@"inbox" sender:self];
                    }
                    
                }
            }
            
            
            
        }
    }
    
    [self getFeatureList];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(leftMenuSelected:) name:@"leftMenuSelectedNotification" object:nil];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MessageFrom"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [super viewWillAppear:YES];
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];

    }
}

-(void)reOpenClosedSocketsNotification1
{
    [self inboxtotalcount];
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush1 isEqualToString:@"YES"])
        {
            //[str isEqualToString:@"Inbox"]
            for (int i=0; i<[storesListArray count]; i++)
            {
                NSString *str = [NSString stringWithFormat:@"%@",[[storesListArray objectAtIndex:i] objectForKey:@"name"]];
                if ([str isEqualToString:@"Inbox"])
                {
                    NSString *grant =[[storesListArray objectAtIndex:i] objectForKey:@"grants"];
                    
                    int per = [grant intValue];
                    if(!(per==0))
                    {
                        [self performSegueWithIdentifier:@"inbox" sender:self];
                    }
                    
                }
            }
            
            
            
        }
    }
}
-(void)leftMenuSelected:(NSNotification *)notification
{
    if ([notification.object isEqualToString:@"open"])
    {
        alphaView.hidden=NO;
    }
    else
    {
        alphaView.hidden=YES;
    }
    
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"lefttablereload" object:nil];
    return YES;
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
    alphaView.hidden=YES;
    [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
    //Code to handle the gesture
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView != storesTableview) {
        return [searchResults count];
        
    } else {
        return [storesListArray count];
        
    }
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"ListCell";
    ListCell*cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
        
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if (tableView != storesTableview)
    {
        cell.textLabel.text = [searchResults objectAtIndex:indexPath.row];
    }
    else
    {
        cell.store_Name.layer.masksToBounds = YES;
        cell.store_Name.layer.cornerRadius = 5.0;
        
        if([[[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"custom_text"] isKindOfClass:[NSNull class]])
        {
            if([[[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"theatro_text"] isKindOfClass:[NSNull class]])
            {
                cell.store_Name.text =@"NO TEXT";
            }
            else
            {
                cell.store_Name.text =[[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"theatro_text"];
            }
        }
        else
        {
            cell.store_Name.text =[[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"custom_text"];
        }
        cell.count_lblWidth.constant=25.0;
        cell.count_lblHeight.constant=25.0;
        cell.anncount_lblWidth.constant=25.0;
        cell.anncount_lblHeight.constant=25.0;
        cell.store_Name.backgroundColor=kNavBackgroundColor;
        NSString *str = [NSString stringWithFormat:@"%@",[[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"theatro_text"]];
        NSString *grant =[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"grants"];
        
        int per = [grant intValue];
        if(per==0)
        {
            cell.store_Name.backgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            
        }
        if ([str isEqualToString:@"Stores"])
        {
            cell.count_lbl.hidden=YES;
            cell.anncount_lbl.hidden=YES;
            cell.store_Image.image=[listImages objectAtIndex:0];
        }
        else if ([str isEqualToString:@"Inbox"])
        {
            
            if(messagecount>0)
            {
                cell.count_lbl.hidden=NO;
                cell.count_lbl.text=[NSString stringWithFormat:@"%d",messagecount];
                //float widthIs = cell.count_lbl.intrinsicContentSize.width;
               // cell.count_lblWidth.constant=widthIs;
               // cell.count_lblHeight.constant=widthIs/3;
            }
            else
            {
                cell.count_lbl.hidden=YES;
                
            }
            if (announcementcount>0)
            {
                cell.anncount_lbl.backgroundColor=kGreencolor;
                cell.anncount_lbl.hidden=NO;
                cell.anncount_lbl.text=[NSString stringWithFormat:@"%d",
                                     announcementcount];
            }
            else
            {
                cell.anncount_lbl.hidden=YES;
            }
            cell.anncount_lbl.layer.cornerRadius=13;
            cell.anncount_lbl.layer.masksToBounds=YES;
            cell.count_lbl.layer.cornerRadius=8;
            cell.store_Image.image=[listImages objectAtIndex:1];
        }
        else if ([str isEqualToString:@"Favorite"])
        {
            cell.count_lbl.hidden=YES;
            cell.anncount_lbl.hidden=YES;
            cell.store_Image.image=[listImages objectAtIndex:2];
            
            if ([[[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"custom_text"] isKindOfClass:[NSNull class]])
            {
                if([[[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"theatro_text"] isKindOfClass:[NSNull class]])
                {
                   // cell.store_Name.text =@"NO TEXT";
                    [[NSUserDefaults standardUserDefaults] setObject:[dictLang objectForKey:@"Favorites"] forKey:@"fav_Tcm"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                else
                {
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"theatro_text"] forKey:@"fav_Tcm"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
            }
            else
            {
            [[NSUserDefaults standardUserDefaults] setObject:[[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"custom_text"] forKey:@"fav_Tcm"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        else if ([str isEqualToString:@"New Message"])
        {
            cell.count_lbl.hidden=YES;
            cell.anncount_lbl.hidden=YES;
            cell.store_Image.image=[listImages objectAtIndex:3];
        }
        //cell.placeButton.titleLabel.font=[UIFont fontWithName:kFontName size:kFontSize];
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView != storesTableview) {
        
    }
    else
    {
        NSString *featureName;
        if([[[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"custom_text"] isKindOfClass:[NSNull class]])
        {
            if([[[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"theatro_text"] isKindOfClass:[NSNull class]])
            {
                featureName =@"NO TEXT";
            }
            else
            {
                featureName =[[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"theatro_text"];
            }
        }
        else
        {
            featureName =[[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"customization"] objectForKey:@"custom_text"];
        }
        NSString *str = [NSString stringWithFormat:@"%@",[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
        
        NSString *grant =[[storesListArray objectAtIndex:indexPath.row] objectForKey:@"grants"];
        
        int per = [grant intValue];
        
        
        if ([str isEqualToString:@"Stores"])
        {
            if(per==0)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"You don't have permission to this feature."],[[DiagnosticTool sharedManager] generateScreenCode:@"VPVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"VP"],[[DiagnosticTool sharedManager]generateErrorCode:@"You don't have permission to this feature"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:featureName forKey:@"storename_Tcm"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //nav
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self performSegueWithIdentifier:@"Nav" sender:self];
                });
                
            }
        }
        else if ([str isEqualToString:@"Inbox"])
        {
            if(per==0)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"You don't have permission to this feature."],[[DiagnosticTool sharedManager] generateScreenCode:@"VPVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"VP"],[[DiagnosticTool sharedManager]generateErrorCode:@"You don't have permission to this feature"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:featureName forKey:@"inbox_Tcm"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self performSegueWithIdentifier:@"inbox" sender:self];
                });
                
            }
        }
        else if ([str isEqualToString:@"Favorite"])
        {
            if(per==0)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"You don't have permission to this feature."],[[DiagnosticTool sharedManager] generateScreenCode:@"VPVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"VP"],[[DiagnosticTool sharedManager]generateErrorCode:@"You don't have permission to this feature"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                   [self performSegueWithIdentifier:@"Favorite" sender:self];
                
            }
        }
        else if ([str isEqualToString:@"New Message"])
        {
            if(per==0)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"You don't have permission to this feature."],[[DiagnosticTool sharedManager] generateScreenCode:@"VPVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"VP"],[[DiagnosticTool sharedManager]generateErrorCode:@"You don't have permission to this feature"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:featureName forKey:@"new_Tcm"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] setObject:@"Message" forKey:@"MessageFrom"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self performSegueWithIdentifier:@"newmessage" sender:self];
                });
                
            }
        }
        
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
    if ([[segue identifier] isEqualToString:@"District"])
    {
        [segue destinationViewController]; //---->DistrictViewController
        [[NSUserDefaults standardUserDefaults] setObject:@"FromStrore" forKey:@"Controller"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if ([[segue identifier] isEqualToString:@"Favorite"])
    {
        [segue destinationViewController];  //---->FavoriteViewController
    }
    else if ([[segue identifier] isEqualToString:@"newmessage"])
    {
       
        [segue destinationViewController];  //---->NewMessageViewController
    }
    else if ([[segue identifier] isEqualToString:@"inbox"])
    {
        
            InboxViewController *inbox= [segue destinationViewController];
            inbox.inboxpreviousTab=YES;
            [segue destinationViewController];
         //---->InboxViewController
    }
    else if ([[segue identifier] isEqualToString:@"Nav"])
    {
        // NavViewController *vc=[segue destinationViewController];
    }
    
}

@end
