//
//  FavoriteViewController.h
//  ManagersMockup
//
//  Created by Ravi on 01/09/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EarboxDataStore.h"
#import "Favorites+CoreDataProperties.h"
#import "Earboxinboxmodel.h"

@interface FavoriteViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *tbl_Fav;
    IBOutlet UILabel *lbl_NoFav;
     IBOutlet UILabel *lbl_Green;
}

@end
