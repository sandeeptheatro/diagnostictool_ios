//
//  InboxViewController.h
//  Managers
//
//  Created by Ravi on 05/10/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface InboxViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,AVAudioPlayerDelegate>
{
        IBOutlet UIButton *privateMessageButton,*everyoneMessageButton,*groupMessageButton,*storeAnnouncementButton,*inboxButton,*savedButton,*sentButton;
        IBOutlet UILabel *privateMessageCount,*everyoneMessageCount,*groupMessageCount,*storeAnnouncementCount;
    int selectedrow;
    BOOL selected;
    NSString *audioId;
    BOOL isReplying;
    NSString *replyingTo;
    BOOL deselected;
    IBOutlet UILabel *lbl_Green;
    
    
}
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property BOOL inboxpreviousTab;
@property BOOL isPaused;
@property BOOL scrubbing;
@property NSTimer *timer;
-(void)percentageCompleted:(NSArray *)notification;
@end
