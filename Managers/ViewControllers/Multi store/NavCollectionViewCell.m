//
//  NavCollectionViewCell.m
//  MultiStoreNavigation
//
//  Created by sandeepchalla on 11/3/16.
//  Copyright © 2016 sandeepchalla. All rights reserved.
//

#import "NavCollectionViewCell.h"


@implementation NavCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"NavCollectionViewCell" owner:self options:nil];
        self = [arrayOfViews objectAtIndex:0];
        
    }
    
    return self;
    
}


@end
