//
//  C2ScanningViewController.m
//  Managers
//
//  Created by sandeepchalla on 9/5/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import "C2ScanningViewController.h"
#import "Constant.h"
#import "AlertView.h"
#import "Reachability.h"
#import "ActivityIndicatorView.h"
#import "HttpHandler.h"
#import "DiagnosticTool.h"

@interface C2ScanningViewController ()<AVCaptureMetadataOutputObjectsDelegate>
{
    UIButton *c2SwitchButton;
    NSString *boardidString;
    NSDictionary *dict;
    
    
}
@property (strong, nonatomic) UIView *cameraPreviewView;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *captureLayer;

@end

@implementation C2ScanningViewController

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (boardIdTextfield.text.length>4)
    {
        c2SwitchButton.backgroundColor = kNavBackgroundColor;
        c2SwitchButton.userInteractionEnabled=YES;
        boardidString=boardIdTextfield.text;
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Please enter valid Board Id"],[[DiagnosticTool sharedManager] generateScreenCode:@"C2ScanningVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"LogonC2ApiCall"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please enter valid Board Id"]] delegate:self cancelButtonTitle:[dict objectForKey:@"OK"] otherButtonTitles:nil];
        
        [alert show];
    }
    
    [textField resignFirstResponder];
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
    [super viewWillAppear:animated];
}

-(void)forceLogOut:(NSNotification *)notification
{
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
    }
}

- (void)viewDidLoad
{
    boardIdLabel.marqueeType = MLContinuous;
    boardIdLabel.scrollDuration = 8.0;
    boardIdLabel.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    boardIdLabel.fadeLength = 0.0f;
    boardIdLabel.leadingBuffer = 0.0f;
    boardIdLabel.trailingBuffer = 10.0f;
    
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
     dict =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    
    [super viewDidLoad];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kNavBackgroundColor}];
    self.navigationItem.title=[dict objectForKey:@"Switch to C2"];//@"C2 Board Id";
    
    c2SwitchButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [c2SwitchButton setTitle:[dict objectForKey:@"Switch"] forState:UIControlStateNormal];
    c2SwitchButton.frame=CGRectMake((self.view.bounds.size.width/2)-80, 430, 160, 40);
    c2SwitchButton.backgroundColor = kGreyColor;
    c2SwitchButton.layer.cornerRadius=8;
    c2SwitchButton.userInteractionEnabled=NO;
    [c2SwitchButton setClipsToBounds:YES];
    [c2SwitchButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [c2SwitchButton addTarget:self action:@selector(c2SwitchAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:c2SwitchButton];
    
    boardIdLabel.textColor =kNavBackgroundColor;
    
    boardIdTextfield.layer.cornerRadius=4;
    [boardIdTextfield setClipsToBounds:YES];
    boardIdTextfield.textColor =[UIColor blackColor];
    boardIdTextfield.placeholder =[dict objectForKey:@"Scan C2 Board id"];
    boardIdTextfield.text=[boardIdTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    boardIdLabel.text=[dict objectForKey:@"Board id"];
    
    self.cameraPreviewView=[[UIView alloc]init];
    self.cameraPreviewView.frame =CGRectMake(20, 150, self.view.bounds.size.width-40, 250);
    self.cameraPreviewView.layer.borderWidth=2;
    self.cameraPreviewView.layer.borderColor=[UIColor greenColor].CGColor;
    [self.view addSubview:self.cameraPreviewView];
    [self setupScanningSession];
}

// Local method to setup camera scanning session.
- (void)setupScanningSession {
    // Initalising hte Capture session before doing any video capture/scanning.
    self.captureSession = [[AVCaptureSession alloc] init];
    
    NSError *error;
    // Set camera capture device to default and the media type to video.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    // Set video capture input: If there a problem initialising the camera, it will give am error.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        NSLog(@"C2ScanningViewController error Getting Camera Input");
        return;
    }
    // Adding input souce for capture session. i.e., Camera
    [self.captureSession addInput:input];
    
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    // Set output to capture session. Initalising an output object we will use later.
    [self.captureSession addOutput:captureMetadataOutput];
    
    // Create a new queue and set delegate for metadata objects scanned.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("scanQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    // Delegate should implement captureOutput:didOutputMetadataObjects:fromConnection: to get callbacks on detected metadata.
    [captureMetadataOutput setMetadataObjectTypes:[captureMetadataOutput availableMetadataObjectTypes]];
    
    // Layer that will display what the camera is capturing.
    self.captureLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    [self.captureLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.captureLayer setFrame:self.cameraPreviewView.layer.bounds];
    // Adding the camera AVCaptureVideoPreviewLayer to our view's layer.
    [self.cameraPreviewView.layer addSublayer:self.captureLayer];
    [self.captureSession startRunning];
}

-(void)c2SwitchAction
{
    if (boardIdTextfield.text.length>4)
    {
        c2SwitchButton.backgroundColor = kNavBackgroundColor;
        c2SwitchButton.userInteractionEnabled=YES;
        boardIdTextfield.userInteractionEnabled=NO;
        [self logonC2ApiCall];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Please enter valid Board Id"],[[DiagnosticTool sharedManager] generateScreenCode:@"C2ScanningVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"LogonC2ApiCall"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please enter valid Board Id"]] delegate:self cancelButtonTitle:[dict objectForKey:@"OK"] otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==100)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [boardIdTextfield becomeFirstResponder];
    }
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    //CGRect highlightViewRect = CGRectZero;
    // AVMetadataMachineReadableCodeObject *barCodeObject;
    boardidString = nil;
    NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];
    
    for (AVMetadataObject *metadata in metadataObjects) {
        for (NSString *type in barCodeTypes) {
            if ([metadata.type isEqualToString:type])
            {
                //barCodeObject = (AVMetadataMachineReadableCodeObject *)[self.captureLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
                //highlightViewRect = barCodeObject.bounds;
                boardidString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
                break;
            }
        }
        
        if (boardidString != nil)
        {
            dispatch_sync(dispatch_get_main_queue(), ^{
                c2SwitchButton.backgroundColor = kNavBackgroundColor;
                c2SwitchButton.userInteractionEnabled=YES;
                boardIdTextfield.userInteractionEnabled=NO;
                boardIdTextfield.layer.borderWidth=2;
                boardIdTextfield.layer.borderColor=kNavBackgroundColor.CGColor;
                boardIdTextfield.text =boardidString;
                [self.captureSession stopRunning];
                [self logonC2ApiCall];
            });
            
        }
        else
        {
             dispatch_sync(dispatch_get_main_queue(), ^{
            boardIdTextfield.text = [dict objectForKey:@"Unable to recognise C2 BoardId"];
            boardIdTextfield.layer.borderColor=kNavBackgroundColor.CGColor;
                  });
        }
    }
}

-(void)logonC2ApiCall
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dict objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dict objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"C2ScanningVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"LogonC2ApiCall"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            
            NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
            
            NSDictionary *twResponseHeader= [HttpHandler logonC2ApiCall:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] boardid:boardidString userScreen:@"C2ScanningVC"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                if (![[[twResponseHeader objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
                {
                    if ([twResponseHeader objectForKey:@"Error"])
                    {
                        
                        c2SwitchButton.backgroundColor = kGreyColor;
                        c2SwitchButton.userInteractionEnabled=NO;
                        boardIdTextfield.userInteractionEnabled=YES;
                        boardIdTextfield.layer.borderWidth=0;
                        boardIdTextfield.layer.borderColor=(__bridge CGColorRef _Nullable)([UIColor lightGrayColor]);
                        boardIdTextfield.text =@"";
                        [self.captureSession startRunning];
                        
                        [AlertView alert:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[twResponseHeader objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                        
                    }
                    else
                    {
                        if ([twResponseHeader objectForKey:@"message"])
                        {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"%@ (%@i)",[twResponseHeader objectForKey:@"message"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:self cancelButtonTitle:[dict objectForKey:@"OK"] otherButtonTitles:nil];
                            alert.tag=100;
                            [alert show];
                            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                        }
                        
                    }
                }
                else
                {
                    NSString *twerrorMessage=[twResponseHeader objectForKey:@"twerrorMessage"];
                    [AlertView alert:[dict objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)", twerrorMessage,[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                }
                
            }];
        }];
    }
}

@end
