//
//  HierarchyViewController.m
//  ManagersMockup
//
//  Created by Ravi on 14/06/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import "HierarchyViewController.h"
#import "ListCell.h"
#import "Constant.h"
#import "EarboxDataStore.h"
#import "HttpHandler.h"
#import "Earboxinboxmodel.h"
#import "DiagnosticTool.h"

@interface HierarchyViewController ()
{
    NSMutableArray *sectionTitleArray,*arrayForBool,*district,*stores,*finalArray,*storeChecked,*districtChecked,*checkedArray,*unselectedDist,*distAvailable,*selectedstoresname,*storesCheckedArray,*distAvailablechange;
    UIBarButtonItem *chkmanuaaly ;
    ListCell *cell;
    NSArray *totalDist;
    BOOL dist1,dist2;
    NSArray *storeselected;
    NSString *strLevel;
    BOOL clicked;
   NSDictionary *dictLang;
}

@end

@implementation HierarchyViewController

- (void)viewDidLoad
{
    MarqueeLabel *_lectureName = [[MarqueeLabel alloc] initWithFrame:CGRectMake(100,20,100, 20) duration:8.0 andFadeLength:10.0f];
    [_lectureName setTextAlignment:NSTextAlignmentCenter];
    [_lectureName setBackgroundColor:[UIColor clearColor]];
    
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    clicked=NO;
    
    //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kNavBackgroundColor,
       NSFontAttributeName:[UIFont fontWithName:kFontName size:13]}];
    dist1=NO;
    dist2=NO;
    
    sectionTitleArray=[[NSMutableArray alloc] init];
    finalArray=[[NSMutableArray alloc] init];
    checkedArray = [[NSMutableArray alloc] init];
    unselectedDist = [[NSMutableArray alloc] init];
    selectedstoresname = [[NSMutableArray alloc] init];
    storesCheckedArray=[[NSMutableArray alloc]init];
    distAvailablechange=[[NSMutableArray alloc]init];
    levelName=[[NSUserDefaults standardUserDefaults] objectForKey:@"levelname"];
    
    strLevel=[NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Hide All"],levelName];
    sectionTitleArray=[NSMutableArray arrayWithObjects:strLevel,[dictLang objectForKey:@"Hide All Stores"], nil];
    
    arrayForBool = [[NSMutableArray alloc] init];
    for (int i=0; i<[sectionTitleArray count]; i++) {
        if (i==0)
        {
            [arrayForBool addObject:[NSNumber numberWithBool:YES]];
        }
        else
        {
            [arrayForBool addObject:[NSNumber numberWithBool:YES]];
        }
        
    }
    
    
    stores=[[NSMutableArray alloc]init];
    distAvailable= [[NSMutableArray alloc] init];
    NSString *strFav =[[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
    
    
    NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
    
    
    
    district=[[[NSUserDefaults standardUserDefaults] objectForKey:@"HeirarchyDistSelected"] mutableCopy];
    
    if (district.count==0)
    {
        district=[[NSMutableArray alloc]init];
    }
    else
    {
        for (int i=0; i<[district count]; i++)
        {
            //[distAvailable addObject:@"1"];
            [distAvailablechange addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[district objectAtIndex:i]objectForKey:@"nodeName"],@"nodeName",@"1",@"status",nil]];
        }
    }
    totalDist=[[NSUserDefaults standardUserDefaults] objectForKey:@"totalDist"];
    
    if (![fav isEqualToString:@"fav"])
    {
        for (int i=0; i<[district count]; i++)
        {
            if ([[[district objectAtIndex:i] objectForKey:@"childs"] count]>0)
            {
                for (int j=0; j<[[[district objectAtIndex:i] objectForKey:@"childs"] count]; j++)
                {
                    [stores addObject:[[[district objectAtIndex:i] objectForKey:@"childs"] objectAtIndex:j]];
                }
                
            }
        }
    }
    
    storeselected=[[NSUserDefaults standardUserDefaults] objectForKey:@"HeirarchyStoresSelected"];
    if (storeselected.count>0)
    {
        for (int i=0; i<[storeselected count]; i++)
        {
            NSString *storeName =[[storeselected objectAtIndex:i] objectForKey:@"store_name"];
            
            if (![[stores valueForKey:@"store_name"] containsObject:storeName])
            {
                [stores addObject:[storeselected objectAtIndex:i]];
            }
        }
        
        if (![fav isEqualToString:@"fav"])
        {
            int z=0;
            for (int i=0; i<[totalDist count]; i++)
            {
                if ([[[totalDist objectAtIndex:i] objectForKey:@"childs"] count]>0)
                {
                    for (int j=0; j<[[[totalDist objectAtIndex:i] objectForKey:@"childs"] count]; j++)
                    {
                        // [stores addObject:];
                        NSString *storename=[[[totalDist objectAtIndex:i] objectForKey:@"childs"] objectAtIndex:j];
                        if ([stores containsObject:storename] )
                        {
                            
                            if (![district containsObject:[totalDist objectAtIndex:i]])
                            {
                                [district addObject:[totalDist objectAtIndex:i]];
                                [distAvailablechange addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[district objectAtIndex:z]objectForKey:@"nodeName"],@"nodeName",@"0",@"status",nil]];
                                //[distAvailable addObject:@"0"];
                                z++;
                            }
                            
                            
                            
                        }
                    }
                    
                }
            }
        }
        else
        {
            
            distAvailable=[[NSUserDefaults standardUserDefaults] objectForKey:@"HQManagerAppID"];
        }
    }
    
    
    NSSortDescriptor *storesort = [NSSortDescriptor sortDescriptorWithKey:@"store_name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [stores sortUsingDescriptors:[NSArray arrayWithObject:storesort]];
    NSSortDescriptor *distsort = [NSSortDescriptor sortDescriptorWithKey:@"nodeName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [district sortUsingDescriptors:[NSArray arrayWithObject:distsort]];
    
    NSSortDescriptor *distchecksort = [NSSortDescriptor sortDescriptorWithKey:@"nodeName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [distAvailablechange sortUsingDescriptors:[NSArray arrayWithObject:distchecksort]];
    
    [finalArray addObject:district];
    [finalArray addObject:stores];
    
    
    storeChecked=[[NSMutableArray alloc] init];
    districtChecked = [[NSMutableArray alloc] init];
    if ([stores count]!=0)
    {
        if ([strFav isEqualToString:@"fav"])
        {
            NSMutableArray *nodes =nil;
            nodes = [[NSUserDefaults standardUserDefaults] objectForKey:@"Nodes"];
            
            for (int i=0; i<[stores count]; i++)
            {
                BOOL found = NO;
                for (int j=0; j<[nodes count]; j++)
                {
                    if ([[[stores objectAtIndex:i] objectForKey:@"store_id"] integerValue] ==[[nodes objectAtIndex:j] integerValue] )
                    {
                        found=YES;
                        break;
                    }
                    
                }
                if (found)
                {
                    [storeChecked addObject:@"1"];
                    [storesCheckedArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[stores objectAtIndex:i] objectForKey:@"store_name"],@"store_name",@"1",@"status", nil]];
                    //[selectedArray addObject:[[sortedArray objectAtIndex:i] objectForKey:@"id"]];
                }
                else
                {
                    [storeChecked addObject:@"0"];
                    [storesCheckedArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[stores objectAtIndex:i] objectForKey:@"store_name"],@"store_name",@"0",@"status", nil]];
                }
            }
        }
        else
        {
            for (int i=0; i<[stores count]; i++)
            {
                
                [storeChecked addObject:@"1"];
                [storesCheckedArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[stores objectAtIndex:i] objectForKey:@"store_name"],@"store_name",@"1",@"status", nil]];
                
            }
        }
        
        /*
         for (int i=0; i<[storesArray count]; i++)
         {
         BOOL found = NO;
         for (int j=0; j<[groupSelectArray count]; j++)
         {
         if ([[[sortedArray objectAtIndex:i] objectForKey:@"id"] integerValue] ==[[groupSelectArray objectAtIndex:j] integerValue] )
         {
         found=YES;
         break;
         }
         
         }
         if (found)
         {
         [checkedArray addObject:@"1"];
         [selectedArray addObject:[[sortedArray objectAtIndex:i] objectForKey:@"id"]];
         }
         else
         {
         [checkedArray addObject:@"0"];
         }
         }
         */
    }
    if ([district count]!=0)
    {
        for (int i=0; i<[district count]; i++)
        {
            [districtChecked addObject:@"1"];
        }
    }
    
    if (distAvailablechange.count>0)
    {
        if (![strFav isEqualToString:@"fav"])
        {
            for (int i=0; i<[distAvailablechange count]; i++)
            {
                NSString *str=[[distAvailablechange objectAtIndex:i] objectForKey:@"status"];
                [distAvailable addObject:str];
            }
        }
        [checkedArray addObject:distAvailable];
    }
    if (storeChecked.count>0)
    {
        [checkedArray addObject:storeChecked];
    }
    
    /*NSString *str=[self distselectorunselect:[[finalArray objectAtIndex:1]valueForKey:@"store_name"]];
     NSArray *valArray = [[finalArray objectAtIndex:0] valueForKey:@"nodeName"];
     for (int i=0; i<[valArray count]; i++)
     {
     //NSUInteger index = [[valArray objectAtIndex:i] indexOfObject:str];
     if ([str isEqualToString:[valArray objectAtIndex:i]])
     {
     NSInteger selectedstorescount=[[[finalArray objectAtIndex:1] valueForKey:@"store_name"] count];
     NSArray*as =[totalDist valueForKey:@"nodeName"];
     NSUInteger index = [as indexOfObject:str];
     NSInteger count=[[[[totalDist objectAtIndex:index] objectForKey:@"childs"] valueForKey:@"store_name"] count];
     int k=0;
     
     NSArray *fullArray=[[finalArray objectAtIndex:1] valueForKey:@"store_name"];
     for (int j=0; j<[fullArray count]; j++)
     {
     if ([[[[totalDist objectAtIndex:index] objectForKey:@"childs"] valueForKey:@"store_name"] containsObject:[fullArray objectAtIndex:j]])
     {
     k++;
     }
     }
     NSArray *nodeNameArray=[[finalArray objectAtIndex:0] valueForKey:@"nodeName"];
     NSUInteger nodeindex = [nodeNameArray indexOfObject:str];
     //[[checkedArray objectAtIndex:0] replaceObjectAtIndex:index withObject:@"1"];
     if (k==count)
     {
     //                if ([[checkedArray objectAtIndex:buttonSection] containsObject:@"0"])
     //                {
     //                    [[checkedArray objectAtIndex:0] replaceObjectAtIndex:index withObject:@"0"];
     //                }
     //                else
     {
     [[checkedArray objectAtIndex:0] replaceObjectAtIndex:nodeindex withObject:@"1"];
     }
     }
     }
     }*/
    
    // NSSortDescriptor *storechecksort = [NSSortDescriptor sortDescriptorWithKey:@"store_name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    // [storesCheckedArray sortUsingDescriptors:[NSArray arrayWithObject:storechecksort]];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"title"] isEqualToString:@"Group"])
    {
        //self.navigationItem=[dictLang objectForKey:@"Group Message"];
        
        [_lectureName setText:[dictLang objectForKey:@"Group Message"]];
    }
    else
    {
        //self.navigationItem.title = [NSString stringWithFormat:@"Message to %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"title"]];
        [_lectureName setText:[dictLang objectForKey:@"Message to Everyone"]];
    }
    
    chkmanuaaly = [[UIBarButtonItem alloc]initWithTitle:[dictLang objectForKey:@"Next"] style:UIBarButtonItemStylePlain target:self action:@selector(nextview)];
    favoritsButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"favorite"] style:UIBarButtonItemStyleDone target:self action:@selector(favoritsButtonAction)];
    self.navigationItem.rightBarButtonItems=@[chkmanuaaly,favoritsButton];
    
    [tbl_Hierarchy reloadData];
    
    
    
    
    if ([fav isEqualToString:@"fav"])
    {
        [favoritsButton setEnabled:NO];
    }
    
    
    _lectureName.adjustsFontSizeToFitWidth=NO;
    [_lectureName setAnimationCurve:UIViewAnimationOptionCurveEaseIn];
    _lectureName.marqueeType = MLContinuous;
    [self.navigationItem setTitleView:_lectureName];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
    [super viewWillAppear:animated];
}

-(void)nextview
{
    [[NSUserDefaults standardUserDefaults] setObject:levelName forKey:@"levelname"];
    NSMutableArray *dist =nil;
    
    NSMutableArray *storeSel =nil;
    
    NSMutableArray *distUpload =[[NSMutableArray alloc] init];
    
    NSMutableArray *distNamesUpload = [[NSMutableArray alloc] init];
    
    NSMutableArray *storesUpload =[[NSMutableArray alloc] init];
    NSMutableArray *distServer = [[NSMutableArray alloc] init];
    
    dist=[checkedArray objectAtIndex:0];
    
    storeSel=[checkedArray objectAtIndex:1];
    
    
    if ([arrayForBool containsObject:@(1)])
    {
        
        if ([dist containsObject:@"1"] || [storeSel containsObject:@"1"])
        {
            
            
            if ([dist containsObject:@"1"])
                
            {
                
                for (int i=0; i<[dist count]; i++)
                    
                {
                    
                    if ([[dist objectAtIndex:i] isEqualToString:@"1"])
                        
                    {
                        
                        [distUpload addObject:[[district objectAtIndex:i] objectForKey:@"nodeId"]];
                        [distServer addObject:[district objectAtIndex:i]];
                        [distNamesUpload addObject:[[district objectAtIndex:i] objectForKey:@"nodeName"]];
                        
                    }
                    
                }
                
            }
            
            if([storeSel containsObject:@"1"])
                
            {
                
                for (int i=0; i<[storeSel count]; i++)
                    
                {
                    
                    if ([[storeSel objectAtIndex:i] isEqualToString:@"1"])
                        
                    {
                        
                        [storesUpload addObject:[[stores objectAtIndex:i] objectForKey:@"store_id"]];
                        
                        [selectedstoresname addObject:[[stores objectAtIndex:i] objectForKey:@"store_name"]];
                        
                    }
                    
                }
                
            }
            
            
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
            
            [[NSUserDefaults standardUserDefaults] setObject:distServer forKey:@"selectedDistServer"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:distUpload forKey:@"selectedDist"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:storesUpload forKey:@"selectedstores"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:selectedstoresname forKey:@"selectedstoresname"];
            
            [[NSUserDefaults standardUserDefaults] setObject:distNamesUpload forKey:@"selectedDistsname"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self performSegueWithIdentifier:@"sendmessage" sender:self];
        }
        else
        {
            
            NSString *strAlert = [NSString stringWithFormat:@"%@ %@ %@",[dictLang objectForKey:@"Please select"],levelName,[dictLang objectForKey:@"or stores from the list"]];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                  
                                                            message: [NSString stringWithFormat:@"%@ (%@%@%@%@i)",strAlert,[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Hierarchy"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please select stores from the list"]]
                                  
                                                           delegate: nil
                                  
                                                  cancelButtonTitle:[dictLang objectForKey:@"OK"]
                                  
                                                  otherButtonTitles:nil];
            
            [alert show];
            
            
        }
        
    }
    else
    {
        
        NSString *strAlert = [NSString stringWithFormat:@"%@ %@ %@",[dictLang objectForKey:@"Please select"],levelName,[dictLang objectForKey:@"or stores from the list and any one of the section should be expanded"]];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                              
                                                        message: [NSString stringWithFormat:@"%@ (%@%@%@%@i)",strAlert,[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Hierarchy"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please select stores from the list and any one of the section should be expanded"]]
                              
                                                       delegate: nil
                              
                                              cancelButtonTitle:[dictLang objectForKey:@"OK"]
                              
                                              otherButtonTitles:nil];
        
        [alert show];
        
    }
    
}

-(void)favoritsButtonAction
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Please enter the favorite name"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please enter the favorite name"]] message:nil delegate:self cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
    alert.alertViewStyle=UIAlertViewStylePlainTextInput;
    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeASCIICapable];
    [alert show];
    
}

-(void)forceLogOut:(NSNotification *)notification
{
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==108)
    {
        [self favoritsButtonAction];
    }
    else
    {
        if (buttonIndex==1)
        {
            UITextField *group = [actionSheet textFieldAtIndex:0];
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            group.text= [group.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (group.text.length>0 )
            {
                //group.text= [group.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                NSMutableArray *newArray; //= [[NSMutableArray alloc] init];
                NSMutableArray *nameArray = [[NSMutableArray alloc]init];
                newArray= [[EarboxDataStore sharedStore] favoritesArtists];
                
                for (int i=0; i<[newArray count]; i++)
                {
                    Favorites *fav;
                    fav=[newArray objectAtIndex:i];
                    
                    [nameArray addObject:fav.favouriteName];
                }
                
                if ([nameArray containsObject:group.text])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@ %@ (%@%@%@%@i)",[dictLang objectForKey:@"This"],[[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"]capitalizedString],[dictLang objectForKey:@"name already exists"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"favorite name already exists"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    alert.tag=108;
                    [alert show];
                }
                else
                {
                    NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
                    NSString *Multistore =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
                    NSString *str =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"MessageFrom"]];
                    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
                    [postData setObject:str forKey:@"PlayAudioType"];
                    [postData setObject:@"DistHierarchy" forKey:@"FinalView"];
                    [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"] forKey:@"Groups"];
                    
                    // [postData setObject:@"" forKey:@"ChannelSected"];
                    [postData setObject:group.text forKey:@"FavouriteName"];
                    [postData setObject:sl forKey:@"MessageType"];
                    //[postData setObject:@"" forKey:@"Sender"];//featureType
                    [postData setObject:@"" forKey:@"FeatureType"];
                    [postData setObject:@"MS-MA" forKey:@"Originator"];
                    [postData setObject:@"iOS" forKey:@"PlatForm"];
                    //int rangeLow = 100;
                    // int rangeHigh = 100060;
                    //int randomNumber = arc4random() % (rangeHigh-rangeLow+1) + rangeLow;
                    [postData setObject:@"" forKey:@"FavouriteID"];
                    [postData setObject:@"NewMessage" forKey:@"FavouriteType"];
                    [postData setObject:@"" forKey:@"NodeId"];
                    [postData setObject:@"" forKey:@"ParentId"];
                    [postData setObject:@"" forKey:@"LevelId"];
                    [postData setObject:@"false" forKey:@"StoreScreen"];
                    [postData setObject:@"" forKey:@"storeselect"];
                    [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"title"] forKey:@"title"];
                    
                    [postData setObject:Multistore forKey:@"MultiStore"];
                    NSMutableArray *dist; //= [[NSMutableArray alloc] init];
                    
                    NSMutableArray *storeSel;
                    
                    NSMutableArray *distUpload =[[NSMutableArray alloc] init];
                    
                    NSMutableArray *distNamesUpload = [[NSMutableArray alloc] init];
                    
                    NSMutableArray *storesUpload =[[NSMutableArray alloc] init];
                    NSMutableArray *distServer = [[NSMutableArray alloc] init];
                    
                    
                    
                    dist=[checkedArray objectAtIndex:0];
                    
                    storeSel=[checkedArray objectAtIndex:1];
                    
                    
                    if ([arrayForBool containsObject:@(1)])
                    {
                        
                        if ([dist containsObject:@"1"] || [storeSel containsObject:@"1"])
                        {
                            
                            
                            if ([dist containsObject:@"1"])
                                
                            {
                                
                                for (int i=0; i<[dist count]; i++)
                                    
                                {
                                    
                                    if ([[dist objectAtIndex:i] isEqualToString:@"1"])
                                        
                                    {
                                        
                                        [distUpload addObject:[[district objectAtIndex:i] objectForKey:@"nodeId"]];
                                        [distServer addObject:[district objectAtIndex:i]];
                                        [distNamesUpload addObject:[[district objectAtIndex:i] objectForKey:@"nodeName"]];
                                        
                                    }
                                    
                                }
                                
                            }
                            
                            if([storeSel containsObject:@"1"])
                                
                            {
                                
                                for (int i=0; i<[storeSel count]; i++)
                                    
                                {
                                    
                                    if ([[storeSel objectAtIndex:i] isEqualToString:@"1"])
                                        
                                    {
                                        
                                        [storesUpload addObject:[[stores objectAtIndex:i] objectForKey:@"store_id"]];
                                        
                                        [selectedstoresname addObject:[[stores objectAtIndex:i] objectForKey:@"store_name"]];
                                        
                                    }
                                    
                                }
                                
                            }
                        }
                        
                    }
                    if (clicked)
                    {
                        [postData setObject:[checkedArray objectAtIndex:0] forKey:@"HQManagerAppID"];
                        
                    }
                    else
                    {
                        [postData setObject:distAvailable forKey:@"HQManagerAppID"];
                    }
                    
                    [postData setObject:stores forKey:@"Stores"];
                    [postData setObject:@"" forKey:@"StoreNames"];
                    [postData setObject:@"" forKey:@"HQTagOutName"];
                    [postData setObject:storesUpload forKey:@"Nodes"];
                    [postData setObject:district forKey:@"MultiStoreNodeId"];
                    [postData setObject:totalDist forKey:@"MultiStoreNodesNames"];
                    [postData setObject:strLevel forKey:@"levelName"];
                    
                    NSDictionary *loginDetails1 =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
                    
                    //[self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
                    NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
                    [myQueueSales addOperationWithBlock:^{
                        NSDictionary *responsDic=[HttpHandler favSave:[loginDetails1 objectForKey:@"twUserName"] password:[loginDetails1 objectForKey:@"twPassword"] jSonStr:postData userScreen:@"HierarchyVC"];
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            if ([responsDic objectForKey:@"Error"])
                            {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                                
                                [alert show];
                            }
                            else if ([responsDic objectForKey:@"FavouriteID"])
                            {
                                [postData setObject:[responsDic objectForKey:@"FavouriteID"] forKey:@"FavouriteID"];
                                [array addObject:postData];
                                [[Earboxinboxmodel sharedModel]favoritesInsert:array];
                            }
                            
                        }];
                    }];
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@ (%@%@%@%@i)",[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"],[dictLang objectForKey:@"name cannot be empty"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"favorite name cannot be empty"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                alert.tag=108;
                [alert show];
            }
        }
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [sectionTitleArray count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSInteger height;
    
    
    height=40; ;
    
    return height;
}

#pragma mark - Creating View for TableView Section

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionView;
    NSString *str_Ms =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
    if ([str_Ms isEqualToString:@"MS"])
    {
        sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,40)];
        sectionView.tag=section;
        UILabel *cover=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
        cover.backgroundColor=[UIColor whiteColor];
        [sectionView addSubview:cover];
        
        UILabel *viewLabel = [[UILabel alloc] init];
        //UILabel *viewLabel=[[UILabel alloc]initWithFrame:];
        
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        
        
        
        if (screenSize.width ==414)
            
        {
            viewLabel.frame =CGRectMake(0, 10,self.view.frame.size.width, 30);
        }
        else if (screenSize.width ==375)
        {
            viewLabel.frame =CGRectMake(0, 10,self.view.frame.size.width, 30);
        }
        else
        {
            viewLabel.frame =CGRectMake(0, 10,self.view.frame.size.width, 30);
        }
        
        viewLabel.backgroundColor=[UIColor clearColor];
        sectionView.layer.cornerRadius=5.0;
        sectionView.layer.masksToBounds = YES;
        viewLabel.layer.masksToBounds=YES;
        viewLabel.layer.cornerRadius=5.0;
        viewLabel.textAlignment=NSTextAlignmentCenter;
        viewLabel.textColor=kNavBackgroundColor;
        viewLabel.font=[UIFont systemFontOfSize:19];
        viewLabel.text=[NSString stringWithFormat:@"%@",[sectionTitleArray objectAtIndex:section]];
        [sectionView addSubview:viewLabel];
        /********** Add a custom Separator with Section view *******************/
        
        /********** Add UITapGestureRecognizer to SectionView   **************/
        
        UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
        [sectionView addGestureRecognizer:headerTapped];
        
        
    }
    return sectionView;
    
}

#pragma mark - Table header gesture tapped

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer
{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[sectionTitleArray count]; i++) {
            if (indexPath.section==i)
            {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
            
        }
        if ([[arrayForBool objectAtIndex:0] boolValue] && [[arrayForBool objectAtIndex:1] boolValue])
        {
            //[sectionTitleArray removeAllObjects];
            NSString *lblStr = [NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Hide All"],levelName];
            sectionTitleArray= [NSMutableArray arrayWithObjects:lblStr,[dictLang objectForKey:@"Hide All Stores"], nil];
        }
        else if(![[arrayForBool objectAtIndex:0] boolValue] && ![[arrayForBool objectAtIndex:1] boolValue])
        {
            //[sectionTitleArray removeAllObjects];
            NSString *lblStr = [NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Show All"],levelName];
            sectionTitleArray= [NSMutableArray arrayWithObjects:lblStr,[dictLang objectForKey:@"Show All Stores"], nil];
        }
        else if([[arrayForBool objectAtIndex:0] boolValue] && ![[arrayForBool objectAtIndex:1] boolValue])
        {
            //[sectionTitleArray removeAllObjects];
            NSString *lblStr = [NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Hide All"],levelName];
            sectionTitleArray= [NSMutableArray arrayWithObjects:lblStr,[dictLang objectForKey:@"Show All Stores"], nil];
        }
        else if(![[arrayForBool objectAtIndex:0] boolValue] && [[arrayForBool objectAtIndex:1] boolValue])
        {
            //[sectionTitleArray removeAllObjects];
            NSString *lblStr = [NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Show All"],levelName];
            sectionTitleArray= [NSMutableArray arrayWithObjects:lblStr,[dictLang objectForKey:@"Hide All Stores"], nil];
        }
        [tbl_Hierarchy reloadData];
        // [storeTable reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationLeft];
        if (indexPath.section==1 && [[arrayForBool objectAtIndex:0] boolValue] && [[arrayForBool objectAtIndex:1] boolValue])
        {
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
            
            [tbl_Hierarchy scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            
        }
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count;
    count=0;
    
    
    if ([[arrayForBool objectAtIndex:section] boolValue])
    {
        if (section==1)
        {
            if( [[finalArray objectAtIndex:section] count]!=0)
            {
                count=[[finalArray objectAtIndex:section] count];
            }
        }
        else
        {
            if( [[finalArray objectAtIndex:section] count]!=0)
            {
                count=[[finalArray objectAtIndex:section] count];
            }
        }
        
    }
    
    return count;
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 87;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"ListCell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListCell" owner:self options:nil];
        cell = [nib objectAtIndex:1];
        
        
    }
    
    
    
    BOOL manyCells  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
    
    /********** If the section supposed to be closed *******************/
    if(!manyCells)
    {
        cell.backgroundColor=[UIColor clearColor];
        
        cell.textLabel.text=@"";
    }
    /********** If the section supposed to be Opened *******************/
    else
    {
        
        cell.btn_Info.hidden=NO;
        cell.btn_Select.hidden=NO;
        [cell.btn_Select addTarget:self action:@selector(selectGroup:) forControlEvents:UIControlEventTouchUpInside];
        //cell.btn_select.tag=indexPath.row;
        NSInteger sectionVar1=0;
        NSString *sectionVar=[NSString stringWithFormat:@"%ld%ld",(long)indexPath.section+1,(long)indexPath.row];
        sectionVar1 = [sectionVar intValue];
        cell.btn_Select.tag=sectionVar1;
        
        
        
        if([[[checkedArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:@"0"])
        {
            
            NSInteger section=0;
            section=indexPath.section;
            if (section==0 && [[[checkedArray objectAtIndex:0] objectAtIndex:indexPath.row] isEqualToString:@"0"])
            {
                //cell.btn_Info.backgroundColor=[UIColor grayColor];
                [cell.btn_Info setImage:[UIImage imageNamed:@"blackbox"]
                               forState:UIControlStateNormal];
            }
            else
                
            {
                [cell.btn_Info setImage:[UIImage imageNamed:@"checkbox"]
                               forState:UIControlStateNormal];
            }
        }
        else
        {
            NSInteger section=0;
            section=indexPath.section;
            if (section==0 && [[distAvailable objectAtIndex:indexPath.row] isEqualToString:@"0"])
            {
                //cell.btn_Info.backgroundColor=[UIColor grayColor];
                [cell.btn_Info setImage:[UIImage imageNamed:@"blackbox"]
                               forState:UIControlStateNormal];
            }
            else
            {
                
                [cell.btn_Info setImage:[UIImage imageNamed:@"checkboxChecked"]
                               forState:UIControlStateNormal];
            }
            
        }
        
    }
    
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.favorite_Name.layer.masksToBounds = YES;
    cell.favorite_Name.layer.cornerRadius = 5.0;
    
    
    cell.favorite_Name.hidden=NO;
    cell.accessoryView.hidden=NO;
    //[sectionTitleArray objectAtIndex:indexPath.section]
    
    if (indexPath.section==0)
    {
        cell.favorite_Name.text =[[[finalArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"nodeName"];
    }
    else
    {
        cell.favorite_Name.text =[[[finalArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"store_name"];
    }
    
    
    return cell;
    
}

-(NSString *)distselectorunselect:(NSString *)store
{
    for (int i=0; i<[totalDist count]; i++)
    {
        if ([[[totalDist objectAtIndex:i] objectForKey:@"childs"] count]>0)
        {
            for (int j=0; j<[[[totalDist objectAtIndex:i] objectForKey:@"childs"] count]; j++)
            {
                // [stores addObject:];
                NSDictionary *storename=[[[totalDist objectAtIndex:i] objectForKey:@"childs"] objectAtIndex:j];
                if ([store isEqualToString:[storename objectForKey:@"store_name"]] )
                {
                    return [[totalDist objectAtIndex:i] objectForKey:@"nodeName"];
                    
                    
                    
                }
            }
            
        }
    }
    return 0;
}


-(void)selectGroup:(UIButton *)button
{
    
    int buttongIndex=0;
    int buttonSection=0;
    
    NSString *totalStr = [NSString stringWithFormat:@"%ld",(long)button.tag];
    NSString *firstLetter = [totalStr substringToIndex:1];
    
    NSString *remainingStr = [totalStr substringFromIndex:1];
    
    buttongIndex=[remainingStr intValue];
    
    if ([firstLetter isEqualToString:@"1"])
    {
        buttonSection=0;
    }
    else if ([firstLetter isEqualToString:@"2"])
    {
        buttonSection=1;
    }
    
    
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:buttongIndex inSection:buttonSection] ;
    cell = [tbl_Hierarchy cellForRowAtIndexPath:myIP];
    
    if ([[[checkedArray objectAtIndex:buttonSection] objectAtIndex:buttongIndex] isEqualToString:@"1"])
    {
        //[checkedArray replaceObjectAtIndex:myIP.row  withObject:@"0"];
        if (buttonSection==0)
        {
            
        }
        else
        {
            clicked=YES;
            
            
            [favoritsButton setEnabled:YES];
            [[checkedArray objectAtIndex:buttonSection] replaceObjectAtIndex:buttongIndex withObject:@"0"];
            NSDictionary *dic1 = [[finalArray objectAtIndex:buttonSection] objectAtIndex:buttongIndex];
            [storesCheckedArray  replaceObjectAtIndex:buttongIndex withObject:[NSDictionary dictionaryWithObjectsAndKeys:[dic1 objectForKey:@"store_name"],@"store_name",@"0",@"status", nil]];
            //[selectedStores removeObject:[finalArray objectAtIndex:myIP.row]];
            [cell.btn_Info setImage:[UIImage imageNamed:@"checkbox"]
                           forState:UIControlStateNormal];
            NSString *str=[self distselectorunselect:[[[finalArray objectAtIndex:1] objectAtIndex:buttongIndex] objectForKey:@"store_name"]];
            NSArray *valArray = [[finalArray objectAtIndex:0] valueForKey:@"nodeName"];
            NSUInteger index = [valArray indexOfObject:str];
            if (![[[checkedArray objectAtIndex:0]objectAtIndex:index] isEqualToString:@"0"])
            {
                NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:[checkedArray objectAtIndex:0]];
                [mutableArray replaceObjectAtIndex:index withObject:@"0"];
                //array = mutableArray;
                
                [checkedArray removeObjectAtIndex:0];
                [checkedArray insertObject:mutableArray atIndex:0];
                //[[checkedArray objectAtIndex:0] replaceObjectAtIndex:index withObject:@"0"];
            }
            
            
            [tbl_Hierarchy reloadData];
        }
        
        
        if (buttonSection==0)
        {
            if ([[[checkedArray objectAtIndex:buttonSection] objectAtIndex:buttongIndex] isEqualToString:@"0"])
            {
                [cell.btn_Info setImage:[UIImage imageNamed:@"blackcheck-box"]
                               forState:UIControlStateNormal];
            }
            else
            {
                [[checkedArray objectAtIndex:buttonSection] replaceObjectAtIndex:buttongIndex withObject:@"1"];
                //[checkedArray replaceObjectAtIndex:myIP.row withObject:@"1"];
                //[selectedStores addObject:[finalArray objectAtIndex:myIP.row]];
                [cell.btn_Info setImage:[UIImage imageNamed:@"checkboxChecked"]
                               forState:UIControlStateNormal];
            }
        }
        
        
    }
    else
    {
        [favoritsButton setEnabled:YES];
        if (!(buttonSection==0))
        {
            
            
            [[checkedArray objectAtIndex:buttonSection] replaceObjectAtIndex:buttongIndex withObject:@"1"];
            NSDictionary *dic1 = [[finalArray objectAtIndex:buttonSection] objectAtIndex:buttongIndex];
            [storesCheckedArray  replaceObjectAtIndex:buttongIndex withObject:[NSDictionary dictionaryWithObjectsAndKeys:[dic1 objectForKey:@"store_name"],@"store_name",@"1",@"status", nil]];
            //[checkedArray replaceObjectAtIndex:myIP.row withObject:@"1"];
            //[selectedStores addObject:[finalArray objectAtIndex:myIP.row]];
            [cell.btn_Info setImage:[UIImage imageNamed:@"checkboxChecked"]
                           forState:UIControlStateNormal];
            NSString *str=[self distselectorunselect:[[[finalArray objectAtIndex:1] objectAtIndex:buttongIndex] objectForKey:@"store_name"]];
            NSArray *valArray = [[finalArray objectAtIndex:0] valueForKey:@"nodeName"];
            for (int i=0; i<[valArray count]; i++)
            {
                //NSUInteger index = [[valArray objectAtIndex:i] indexOfObject:str];
                if ([str isEqualToString:[valArray objectAtIndex:i]])
                {
                    
                    NSArray*as =[totalDist valueForKey:@"nodeName"];
                    NSUInteger index = [as indexOfObject:str];
                    NSInteger count=[[[[totalDist objectAtIndex:index] objectForKey:@"childs"] valueForKey:@"store_name"] count];
                    int k=0;
                    
                    NSArray *fullArray=[[finalArray objectAtIndex:1] valueForKey:@"store_name"];
                    for (int j=0; j<[fullArray count]; j++)
                    {
                        if ([[[[totalDist objectAtIndex:index] objectForKey:@"childs"] valueForKey:@"store_name"] containsObject:[fullArray objectAtIndex:j]])
                        {
                            NSString *str1 = [[storesCheckedArray objectAtIndex:j] objectForKey:@"status"];
                            if ([str1 isEqualToString:@"0"])
                            {
                                k--;
                            }
                            else
                            {
                                k++;
                            }
                            
                        }
                    }
                    NSArray *nodeNameArray=[[finalArray objectAtIndex:0] valueForKey:@"nodeName"];
                    NSUInteger nodeindex = [nodeNameArray indexOfObject:str];
                    //[[checkedArray objectAtIndex:0] replaceObjectAtIndex:index withObject:@"1"];
                    if (k==count)
                    {
                        // if ([[checkedArray objectAtIndex:buttonSection] containsObject:@"0"])
                        {
                            NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:[checkedArray objectAtIndex:0]];
                            [mutableArray replaceObjectAtIndex:nodeindex withObject:@"1"];
                            //array = mutableArray;
                            
                            [checkedArray removeObjectAtIndex:0];
                            [checkedArray insertObject:mutableArray atIndex:0];
                            //[[checkedArray objectAtIndex:0] replaceObjectAtIndex:index withObject:@"0"];
                        }
                        /* else
                         {
                         NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:[checkedArray objectAtIndex:0]];
                         [mutableArray replaceObjectAtIndex:nodeindex withObject:@"1"];
                         //array = mutableArray;
                         
                         [checkedArray removeObjectAtIndex:0];
                         [checkedArray insertObject:mutableArray atIndex:0];
                         //[[checkedArray objectAtIndex:0] replaceObjectAtIndex:nodeindex withObject:@"1"];
                         }*/
                    }
                }
            }
            
            [tbl_Hierarchy reloadData];
        }
        
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"sendmessage"])
    {
        [segue destinationViewController]; //---->MutilMessageViewController
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
