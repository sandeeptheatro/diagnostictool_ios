//
//  FavoriteViewController.m
//  ManagersMockup
//
//  Created by Ravi on 01/09/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "FavoriteViewController.h"
#import "ListCell.h"
#import "SelectGroupsViewController.h"
#import "SelectStoresViewController.h"
#import "MutilMessageViewController.h"
#import "NavViewController.h"
#import "DistrictViewController.h"
#import "NewMessageViewController.h"
#import "HttpHandler.h"
#import "ActivityIndicatorView.h"
#import "Constant.h"

@interface FavoriteViewController ()
{
    NSArray *favoriteArray;
    UIBarButtonItem *chkmanuaaly;
    BOOL edit;
    NSMutableArray *searchResultsAll;
    NSDictionary *dictLang;
}

@end

@implementation FavoriteViewController

- (void)viewDidLoad
{
    dictLang=[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    self.searchDisplayController.searchBar.placeholder=[dictLang objectForKey:@"Search"];
    //self.navigationController.navigationBar.backItem.title =[dictLang objectForKey:@"Back"];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kNavBackgroundColor,
       NSFontAttributeName:[UIFont fontWithName:kFontName size:13]}];
    [super viewDidLoad];
    edit=NO;
    favoriteArray =[[NSMutableArray alloc] init];
    self.navigationItem.title = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"fav_Tcm"]];//@"Favorites";
        
    // Do any additional setup after loading the view.
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)editTableForDeletingRow
{
    if (edit==NO)
    {
        edit=YES;
        chkmanuaaly.title=[dictLang objectForKey:@"Done"];
        [tbl_Fav setEditing:YES animated:YES];
    }
    else
    {
        edit=NO;
        chkmanuaaly.title=[dictLang objectForKey:@"Edit"];
        [tbl_Fav setEditing:NO animated:YES];
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    
    lbl_NoFav.text=[NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Empty"],[[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"]capitalizedString]];
    //tbl_Fav.contentInset = UIEdgeInsetsMake(110,0,0,0);
    favoriteArray = [[EarboxDataStore sharedStore] favoritesArtists];
   
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Select"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FavInfo"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ASGroup"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MessageFrom"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedGroup"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedstores"];
    [[NSUserDefaults standardUserDefaults] setObject:@[@""] forKey:@"selectedstoresname"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedDist"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedDistServer"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedDistsname"];//levelname
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"levelname"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"storeTagOutName"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"selectedManagerAppID"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MultiStore"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"totalDist"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"HeirarchyDistSelected"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"HeirarchyStoresSelected"];

    
    //selectedstores selectedGroup
    [[NSUserDefaults standardUserDefaults] synchronize];
    //Message
    [super viewWillAppear:animated];
    
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    
    // NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
    //[myQueueSales addOperationWithBlock:^{
    if ([favoriteArray count]==0)
    {
        NSMutableArray *fetchArray = [[NSMutableArray alloc] init];
        
        NSMutableArray *fetchMArray=nil;
        
        fetchMArray=[HttpHandler favFetch:[loginDetails objectForKey:@"twUserName"] password:[loginDetails objectForKey:@"twPassword"] userScreen:@"FavoriteVC"];
        if ([fetchMArray containsObject:@"Error"])
        {
            lbl_NoFav.hidden=NO;
            return;
        }
        if (fetchMArray.count==0)
        {
        }
        else
        {
        if (fetchMArray.count>0)
        {
            if ([fetchMArray valueForKey:@"favouriteJSON"])
            {
           
            for (int i=0; i<[fetchMArray count]; i++)
            {
                NSMutableDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[[[fetchMArray objectAtIndex:i] objectForKey:@"favouriteJSON"] dataUsingEncoding:NSUTF8StringEncoding]
                                                                           options:0 error:NULL];
                
                NSString *str =[jsonObject objectForKey:@"PlatForm"];
                if ([str isEqualToString:@"iOS"])
                {
                    NSString *jsonObject1 = [NSString stringWithFormat:@"%@",[[fetchMArray objectAtIndex:i] objectForKey:@"favouriteId"]];
                    
                    NSLog(@"%@",[jsonObject objectForKey:@"FavouriteName"]);
                    //[jsonObject setObject:@"2" forKey:@"FavouriteID"];
                    
                    NSMutableDictionary *mutableDict = [jsonObject mutableCopy];
                    [mutableDict setObject:jsonObject1 forKey:@"FavouriteID"];
                    jsonObject = [mutableDict mutableCopy];
                    
                    [fetchArray addObject:jsonObject];
                }
                
                //FavouriteID
                
                //[fetchArray setValue:jsonObject1 forKey:@"FavouriteID"];
            }
            
            [[Earboxinboxmodel sharedModel]favoritesInsert:fetchArray];
            favoriteArray = [[EarboxDataStore sharedStore] favoritesArtists];
            
        }
        }
        }
        
    }
    // [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    
    
    
    //}];
    // }];
    
    
    // [favoriteArray addObjectsFromArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"FavDetails"]];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"favouriteName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    favoriteArray=[favoriteArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
    if (favoriteArray.count>0)
    {
        chkmanuaaly = [[UIBarButtonItem alloc]initWithTitle:[dictLang objectForKey:@"Edit"] style:UIBarButtonItemStylePlain target:self action:@selector(editTableForDeletingRow)];
        self.navigationItem.rightBarButtonItem=chkmanuaaly;
        lbl_NoFav.hidden=YES;
        tbl_Fav.hidden=NO;
        [tbl_Fav reloadData];
    }
    else
    {
        lbl_NoFav.hidden=NO;
        tbl_Fav.hidden=YES;
    }
    
}

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView != tbl_Fav)
    {
        return [searchResultsAll count];
    }
    else
    {
    return [favoriteArray count];
    }
    
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}


- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [dictLang objectForKey:@"Delete"];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle==UITableViewCellEditingStyleDelete)
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        if (favoriteArray.count>0)
        {
        Favorites *fav;
        fav=[favoriteArray objectAtIndex:indexPath.row];
        
       // [[EarboxDataStore sharedStore] deleteFavorites:fav];
        
       // favoriteArray = [[EarboxDataStore sharedStore] favoritesArtists];
        
        
        NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        
        NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
        [myQueueSales addOperationWithBlock:^{
            NSDictionary *responsDic=[HttpHandler favDelete:[loginDetails objectForKey:@"twUserName"] password:[loginDetails objectForKey:@"twPassword"] favDeleteID:fav.favouriteID userScreen:@"FavoriteVC"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if ([responsDic objectForKey:@"Error"])
                {
                    if ([[responsDic objectForKey:@"Error"] isEqualToString:@"OK"])
                    {
                        if (favoriteArray.count>0)
                        {
                        Favorites *fav;
                        fav=[favoriteArray objectAtIndex:indexPath.row];
                        
                        [[EarboxDataStore sharedStore] deleteFavorites:fav];
                        
                        favoriteArray = [[EarboxDataStore sharedStore] favoritesArtists];
                        
                        if (favoriteArray.count==0)
                        {
                            chkmanuaaly.title =@"";
                            lbl_NoFav.hidden=NO;
                            tbl_Fav.hidden=YES;
                        }
                            NSLog(@"EarboxViewController-Deleted");
                        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
                        
                        }

                    }
                    else
                    {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:[dictLang objectForKey:@"Error"]],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    
                    [alert show];
                    }
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Server error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    
                    [alert show];
                }
                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                
            }];
            
        }];
        
        
    }
    }
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 87;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    static NSString *cellIdentifier=@"ListCell";
    
    ListCell*cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
        
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListCell" owner:self options:nil];
        
        cell = [nib objectAtIndex:1];
        
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    // cell.placeButton.layer.cornerRadius=8;
    
    
    
    // [cell.store_Name setTitle:[storesListArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
    
    if (tableView != tbl_Fav)
        
    {
        
        Favorites *fav;
        
        fav=[searchResultsAll objectAtIndex:indexPath.row];
        
        cell.textLabel.text = fav.favouriteName;
        
        cell.favorite_Name.hidden=YES;
        
    }
    
    else
        
    {
        
        Favorites *fav;
        
        fav=[favoriteArray objectAtIndex:indexPath.row];
        
        cell.favorite_Name.layer.masksToBounds = YES;
        
        cell.favorite_Name.layer.cornerRadius = 5.0;
        
        cell.favorite_Name.text =fav.favouriteName;
        
    }
    
    
    
    return cell;
    
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if (tableView==tbl_Fav)
        
    {
        
        
        
        Favorites *fav;
        
        fav=[favoriteArray objectAtIndex:indexPath.row];
        
        NSString *str_View=fav.finalView;
        
        [[NSUserDefaults standardUserDefaults] setObject:@"fav" forKey:@"FromFav"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if ([str_View isEqualToString:@"Nav"])
            
        {
            
            
            
            
            NSMutableArray *array=[[NSMutableArray alloc] init];
            
            
            
            
            NSMutableDictionary *postData=[NSMutableDictionary dictionary];
            
            [postData setObject:fav.nodeId forKey:@"NodeId"];
            
            [postData setObject:fav.parentId forKey:@"ParentId"];
            
            [postData setObject:fav.levelId forKey:@"LevelId"];
            
            [postData setObject:fav.storeScreen forKey:@"StoreScreen"];
            
            [postData setObject:fav.messageType forKey:@"MessageType"];
            
            [postData setObject:fav.featureType forKey:@"FeatureType"];
            
            //MessageType
            
            //FeatureType
            
            [array addObject:postData];
            
            [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"FavInfo"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.multiStore forKey:@"Select"];
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.storeselect forKey:@"storeselect"];
            
            [self performSegueWithIdentifier:@"Nav" sender:self];
            
            /*
             
             NavViewController *earboxVC=[self.storyboard instantiateViewControllerWithIdentifier:@"NavViewController"];
             
             earboxVC.favArray =[favoriteArray objectAtIndex:indexPath.row];
             
             [self.navigationController pushViewController:earboxVC animated:YES];
             
             */
            //DistHierarchy
        }
        else if ([str_View isEqualToString:@"DistHierarchy"])
        {
            NSArray *stores,*Dist,*totalDist,*nodes,*nodeName;
            
            nodes=[NSKeyedUnarchiver unarchiveObjectWithData:fav.nodes];
            nodeName=[NSKeyedUnarchiver unarchiveObjectWithData:fav.hQManagerAppID];
            Dist=[NSKeyedUnarchiver unarchiveObjectWithData:fav.multiStoreNodeId];
            stores=[NSKeyedUnarchiver unarchiveObjectWithData:fav.stores];
            totalDist = [NSKeyedUnarchiver unarchiveObjectWithData:fav.multiStoreNodesNames];
            
            [[NSUserDefaults standardUserDefaults] setObject:totalDist forKey:@"totalDist"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.levelName forKey:@"levelname"];
            [[NSUserDefaults standardUserDefaults] setObject:Dist forKey:@"HeirarchyDistSelected"];
            [[NSUserDefaults standardUserDefaults] setObject:stores forKey:@"HeirarchyStoresSelected"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.multiStore forKey:@"MultiStore"];
            //MessageFrom
            [[NSUserDefaults standardUserDefaults] setObject:fav.playAudioType forKey:@"MessageFrom"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.title forKey:@"title"];
             [[NSUserDefaults standardUserDefaults] setObject:fav.messageType forKey:@"Select"];
            [[NSUserDefaults standardUserDefaults] setObject:nodes forKey:@"Nodes"];//HQManagerAppID
            [[NSUserDefaults standardUserDefaults] setObject:nodeName forKey:@"HQManagerAppID"];
            //Nodes
            [self performSegueWithIdentifier:@"distHie" sender:self];
        }
        else if ([str_View isEqualToString:@"Stores"])
            
        {
            
            
            
            NSArray *groupsArray;
            
            NSArray *storesArry,*storeNamesArray,*distID,*distName;
            
            
            
            distID=[NSKeyedUnarchiver unarchiveObjectWithData:fav.multiStoreNodeId];
            distName = [NSKeyedUnarchiver unarchiveObjectWithData:fav.multiStoreNodesNames];
            [[NSUserDefaults standardUserDefaults] setObject:distID forKey:@"selectedDist"];
            [[NSUserDefaults standardUserDefaults] setObject:distName forKey:@"selectedDistsname"];
            
            // [[NSUserDefaults standardUserDefaults] objectForKey:@"Select"]
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.playAudioType forKey:@"MessageFrom"];
            
            
            
            
            
            groupsArray=[NSKeyedUnarchiver unarchiveObjectWithData:fav.groups];
            
            storesArry=[NSKeyedUnarchiver unarchiveObjectWithData:fav.stores];
            
            storeNamesArray=[NSKeyedUnarchiver unarchiveObjectWithData:fav.storeNames];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:storeNamesArray forKey:@"selectedstoresname"];
            
            [[NSUserDefaults standardUserDefaults] setObject:groupsArray forKey:@"selectedGroup"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:storesArry forKey:@"selectedstores"];
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.messageType forKey:@"Select"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.multiStore forKey:@"MultiStore"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.title forKey:@"title"];
            
            [self performSegueWithIdentifier:@"Stores" sender:self];
            
        }
        
        else if ([str_View isEqualToString:@"Groups"])
            
        {
            
            NSArray *groupsArray;
            
            NSArray *storesArry,*storeNamesArray;
            
            
            
            storeNamesArray=[NSKeyedUnarchiver unarchiveObjectWithData:fav.storeNames];
            
            groupsArray=[NSKeyedUnarchiver unarchiveObjectWithData:fav.groups];
            
            storesArry=[NSKeyedUnarchiver unarchiveObjectWithData:fav.stores];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.playAudioType forKey:@"MessageFrom"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:groupsArray forKey:@"selectedGroup"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:storesArry forKey:@"selectedstores"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:storeNamesArray forKey:@"selectedstoresname"];
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.title forKey:@"title"];
            
            NSString *strAs =fav.messageType;
            
            
            
            if ([strAs isEqualToString:@"ASGroup"])
                
            {
                
                [[NSUserDefaults standardUserDefaults] setObject:fav.messageType forKey:@"ASGroup"];
                
            }
            
            else
                
            {
                
                [[NSUserDefaults standardUserDefaults] setObject:fav.messageType forKey:@"Select"];
                
            }
            
            //[[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"]
            
            [self performSegueWithIdentifier:@"Groups" sender:self];
            
        }
        
        else if ([str_View isEqualToString:@"Multi"])
            
        {
            
            //[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"]
            
            NSArray *groupsArray;
            
            NSArray *storesArry,*storeNamesArray,*nodeArray,*nodeIdArray,*nodeNamesArray,*hqManagerAppID;
            
            groupsArray=[NSKeyedUnarchiver unarchiveObjectWithData:fav.groups];
            
            storesArry=[NSKeyedUnarchiver unarchiveObjectWithData:fav.stores];
            
            storeNamesArray=[NSKeyedUnarchiver unarchiveObjectWithData:fav.storeNames];
            
            nodeArray =[NSKeyedUnarchiver unarchiveObjectWithData:fav.nodes];
            
            nodeIdArray =[NSKeyedUnarchiver unarchiveObjectWithData:fav.multiStoreNodeId];
            
            nodeNamesArray = [NSKeyedUnarchiver unarchiveObjectWithData:fav.multiStoreNodesNames];
            
            hqManagerAppID =[NSKeyedUnarchiver unarchiveObjectWithData:fav.hQManagerAppID];
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.storeselect forKey:@"storeselect"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.title forKey:@"title"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.nodeId forKey:@"store"];
            [[NSUserDefaults standardUserDefaults] setObject:nodeArray forKey:@"selectedDistServer"];
            
            [[NSUserDefaults standardUserDefaults] setObject:nodeIdArray forKey:@"selectedDist"];
            
            [[NSUserDefaults standardUserDefaults] setObject:nodeNamesArray forKey:@"selectedDistsname"];
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.featureType forKey:@"featureType"];
            
            //[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"]
            NSString *strAs =fav.messageType;
            
            
            
            if ([strAs isEqualToString:@"ASGroup"])
                
            {
                
                [[NSUserDefaults standardUserDefaults] setObject:fav.messageType forKey:@"ASGroup"];
                
            }
            
            else
                
            {
                
                [[NSUserDefaults standardUserDefaults] setObject:fav.messageType forKey:@"Select"];
                
            }
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.playAudioType forKey:@"MessageFrom"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:groupsArray forKey:@"selectedGroup"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:storesArry forKey:@"selectedstores"];
            
            [[NSUserDefaults standardUserDefaults] setObject:storeNamesArray forKey:@"selectedstoresname"];
            
            //selectedstoresname
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.multiStore forKey:@"MultiStore"];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.hQTagOutName forKey:@"storeTagOutName"];
            [[NSUserDefaults standardUserDefaults] setObject:hqManagerAppID forKey:@"selectedManagerAppID"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.levelName forKey:@"levelname"];
            
            [self performSegueWithIdentifier:@"Multi" sender:self];
            
        }
        
        else if ([str_View isEqualToString:@"Message"] || [str_View isEqualToString:@"Announcement"])
            
        {
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:str_View forKey:@"MessageFrom"];
            
            
            
            [self performSegueWithIdentifier:@"Dist" sender:self];
            
        }
        
        else if ([str_View isEqualToString:@"NewMessage"])
            
        {
            
            
            
            [self performSegueWithIdentifier:@"NewMessage" sender:self];
            
        }
        
    }
    
    else
        
    {
        
        
        
        Favorites *fav;
        
        fav=[searchResultsAll objectAtIndex:indexPath.row];
        
        NSString *str_View=fav.finalView;
        
        [[NSUserDefaults standardUserDefaults] setObject:@"fav" forKey:@"FromFav"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if ([str_View isEqualToString:@"Nav"])
            
        {
            
            
            
            
            
            NSMutableArray *array=[[NSMutableArray alloc] init];
            
            
            
            
            
            NSMutableDictionary *postData=[NSMutableDictionary dictionary];
            
            [postData setObject:fav.nodeId forKey:@"NodeId"];
            
            [postData setObject:fav.parentId forKey:@"ParentId"];
            
            [postData setObject:fav.levelId forKey:@"LevelId"];
            
            [postData setObject:fav.storeScreen forKey:@"StoreScreen"];
            
            [postData setObject:fav.messageType forKey:@"MessageType"];
            
            [postData setObject:fav.featureType forKey:@"FeatureType"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.multiStore forKey:@"Select"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.storeselect forKey:@"storeselect"];
            //MessageType
            
            //FeatureType
            
            [array addObject:postData];
            
            [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"FavInfo"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            
            [self performSegueWithIdentifier:@"Nav" sender:self];
            
            /*
             
             NavViewController *earboxVC=[self.storyboard instantiateViewControllerWithIdentifier:@"NavViewController"];
             
             earboxVC.favArray =[favoriteArray objectAtIndex:indexPath.row];
             
             [self.navigationController pushViewController:earboxVC animated:YES];
             
             */
            
        }
        else if ([str_View isEqualToString:@"DistHierarchy"])
        {
            NSArray *stores,*Dist,*totalDist,*nodes,*nodeName;
            
            nodes=[NSKeyedUnarchiver unarchiveObjectWithData:fav.nodes];
            nodeName=[NSKeyedUnarchiver unarchiveObjectWithData:fav.hQManagerAppID];
            Dist=[NSKeyedUnarchiver unarchiveObjectWithData:fav.multiStoreNodeId];
            stores=[NSKeyedUnarchiver unarchiveObjectWithData:fav.stores];
            totalDist = [NSKeyedUnarchiver unarchiveObjectWithData:fav.multiStoreNodesNames];
            
            [[NSUserDefaults standardUserDefaults] setObject:totalDist forKey:@"totalDist"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.levelName forKey:@"levelname"];
            [[NSUserDefaults standardUserDefaults] setObject:Dist forKey:@"HeirarchyDistSelected"];
            [[NSUserDefaults standardUserDefaults] setObject:stores forKey:@"HeirarchyStoresSelected"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.multiStore forKey:@"MultiStore"];
            //MessageFrom
            [[NSUserDefaults standardUserDefaults] setObject:fav.playAudioType forKey:@"MessageFrom"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.title forKey:@"title"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.messageType forKey:@"Select"];
            [[NSUserDefaults standardUserDefaults] setObject:nodes forKey:@"Nodes"];//HQManagerAppID
            [[NSUserDefaults standardUserDefaults] setObject:nodeName forKey:@"HQManagerAppID"];
            //Nodes
            [self performSegueWithIdentifier:@"distHie" sender:self];
        }
        
        else if ([str_View isEqualToString:@"Stores"])
            
        {
            NSArray *groupsArray;
            
            NSArray *storesArry,*storeNamesArray,*distID,*distName;
            
            // [[NSUserDefaults standardUserDefaults] objectForKey:@"Select"]
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.playAudioType forKey:@"MessageFrom"];
            
            groupsArray=[NSKeyedUnarchiver unarchiveObjectWithData:fav.groups];
            
            storesArry=[NSKeyedUnarchiver unarchiveObjectWithData:fav.stores];
            
            storeNamesArray=[NSKeyedUnarchiver unarchiveObjectWithData:fav.storeNames];
            distID=[NSKeyedUnarchiver unarchiveObjectWithData:fav.multiStoreNodeId];
            distName = [NSKeyedUnarchiver unarchiveObjectWithData:fav.multiStoreNodesNames];
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.title forKey:@"title"];
            
            [[NSUserDefaults standardUserDefaults] setObject:storeNamesArray forKey:@"selectedstoresname"];
            
            [[NSUserDefaults standardUserDefaults] setObject:groupsArray forKey:@"selectedGroup"];
            
            [[NSUserDefaults standardUserDefaults] setObject:distID forKey:@"selectedDist"];
            [[NSUserDefaults standardUserDefaults] setObject:distName forKey:@"selectedDistsname"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:storesArry forKey:@"selectedstores"];
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.messageType forKey:@"Select"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.multiStore forKey:@"MultiStore"];
            
            [self performSegueWithIdentifier:@"Stores" sender:self];
        }
        
        else if ([str_View isEqualToString:@"Groups"])
            
        {
            
            NSArray *groupsArray;
            
            NSArray *storesArry,*storeNamesArray;
            
            
            
            storeNamesArray=[NSKeyedUnarchiver unarchiveObjectWithData:fav.storeNames];
            
            groupsArray=[NSKeyedUnarchiver unarchiveObjectWithData:fav.groups];
            
            storesArry=[NSKeyedUnarchiver unarchiveObjectWithData:fav.stores];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.title forKey:@"title"];
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.playAudioType forKey:@"MessageFrom"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:groupsArray forKey:@"selectedGroup"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:storesArry forKey:@"selectedstores"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:storeNamesArray forKey:@"selectedstoresname"];
            
            
            
            NSString *strAs =fav.messageType;
            
            
            
            if ([strAs isEqualToString:@"ASGroup"])
                
            {
                
                [[NSUserDefaults standardUserDefaults] setObject:fav.messageType forKey:@"ASGroup"];
                
            }
            
            else
                
            {
                
                [[NSUserDefaults standardUserDefaults] setObject:fav.messageType forKey:@"Select"];
                
            }
            
            //[[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"]
            
            [self performSegueWithIdentifier:@"Groups" sender:self];
            
        }
        
        else if ([str_View isEqualToString:@"Multi"])
            
        {
            //[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"]
            
            NSArray *groupsArray;
            
            NSArray *storesArry,*storeNamesArray,*nodeArray,*nodeIdArray,*nodeNamesArray,*hqManagerAppID;
            
            groupsArray=[NSKeyedUnarchiver unarchiveObjectWithData:fav.groups];
            
            storesArry=[NSKeyedUnarchiver unarchiveObjectWithData:fav.stores];
            
            storeNamesArray=[NSKeyedUnarchiver unarchiveObjectWithData:fav.storeNames];
            
            nodeArray =[NSKeyedUnarchiver unarchiveObjectWithData:fav.nodes];
            
            nodeIdArray =[NSKeyedUnarchiver unarchiveObjectWithData:fav.multiStoreNodeId];
            
            nodeNamesArray = [NSKeyedUnarchiver unarchiveObjectWithData:fav.multiStoreNodesNames];
            
            hqManagerAppID =[NSKeyedUnarchiver unarchiveObjectWithData:fav.hQManagerAppID];
            [[NSUserDefaults standardUserDefaults] setObject:fav.featureType forKey:@"featureType"];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:nodeArray forKey:@"selectedDistServer"];
            
            [[NSUserDefaults standardUserDefaults] setObject:nodeIdArray forKey:@"selectedDist"];
            
            [[NSUserDefaults standardUserDefaults] setObject:nodeNamesArray forKey:@"selectedDistsname"];
            
            
            NSString *strAs =fav.messageType;
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.title forKey:@"title"];
            
            if ([strAs isEqualToString:@"ASGroup"])
                
            {
                
                [[NSUserDefaults standardUserDefaults] setObject:fav.messageType forKey:@"ASGroup"];
                
            }
            
            else
                
            {
                
                [[NSUserDefaults standardUserDefaults] setObject:fav.messageType forKey:@"Select"];
                
            }
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.playAudioType forKey:@"MessageFrom"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:groupsArray forKey:@"selectedGroup"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:storesArry forKey:@"selectedstores"];
            
            [[NSUserDefaults standardUserDefaults] setObject:storeNamesArray forKey:@"selectedstoresname"];
            
            //selectedstoresname
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.multiStore forKey:@"MultiStore"];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:fav.hQTagOutName forKey:@"storeTagOutName"];
            [[NSUserDefaults standardUserDefaults] setObject:hqManagerAppID forKey:@"selectedManagerAppID"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.levelName forKey:@"levelname"];
            [[NSUserDefaults standardUserDefaults] setObject:fav.storeselect forKey:@"storeselect"];
            [self performSegueWithIdentifier:@"Multi" sender:self];
            
        }
        
        else if ([str_View isEqualToString:@"Message"] || [str_View isEqualToString:@"Announcement"])
            
        {
            
            
            [[NSUserDefaults standardUserDefaults] setObject:str_View forKey:@"MessageFrom"];
            
            [self performSegueWithIdentifier:@"Dist" sender:self];
            
        }
        
        else if ([str_View isEqualToString:@"NewMessage"])
            
        {
            
            [self performSegueWithIdentifier:@"NewMessage" sender:self];
            
        }
        
    }
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [segue destinationViewController];
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - UISearchDisplayController delegate methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    searchResultsAll=[[NSMutableArray alloc]init];
    
    for (int i =0;i<[favoriteArray count];i++)
        
    {
        Favorites *fav;
        fav=[favoriteArray objectAtIndex:i];
        
        NSString *city = fav.favouriteName;
        NSRange range=[city rangeOfString:searchText options:NSCaseInsensitiveSearch];
        if (range.length >0)
        {
            if (![searchResultsAll containsObject:city]) {
                [searchResultsAll addObject:fav];
            }
        }
        
    }
    
    
}


-(BOOL)searchDisplayController:(UISearchController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[controller.searchBar scopeButtonTitles]
                                      objectAtIndex:[controller.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}


@end
