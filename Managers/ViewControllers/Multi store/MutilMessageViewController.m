//
//  MutilMessageViewController.m
//  Managers
//
//  Created by Ravi on 05/10/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "MutilMessageViewController.h"
#import "AudioManager.h"
#import "Constant.h"
#import "Reachability.h"
#import "AlertView.h"
#import "HttpHandler.h"
#import "UIViewController.h"
#import "Earboxinboxmodel.h"
#import "DiagnosticTool.h"

@interface MutilMessageViewController ()
{
    NSURL *messageaudiourl;
    BOOL audioSendButton,audioInterruption;
    NSMutableDictionary *pns;
    NSMutableArray *tabsArray,*announcementtabwidth;
    NSDictionary *dictLang;
}

@end

@implementation MutilMessageViewController


@synthesize deleteButton, playButton, recordPauseButton,sendButton,contactString,contactStatusString,earbox;

static MutilMessageViewController  *sharedMgr = nil;

+(MutilMessageViewController *)getsharedInstance {
    if (sharedMgr == nil)
    {
        sharedMgr = [[MutilMessageViewController alloc] init];
    }
    return sharedMgr;
}


-(void)audioInterruptionEnd
{
    audioInterruption=YES;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAnnouncementsNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"audioNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recievedAnnouncementsNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAnnouncementsSingleTapNotification" object:nil];
    
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        [[AudioManager getsharedInstance]audioplayerstop];
        [[AudioManager getsharedInstance]recordstop];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
        
    }];
    
    [super viewWillDisappear:animated];
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}
-(void)viewWillAppear:(BOOL)animated
{

    MarqueeLabel *_lectureName = [[MarqueeLabel alloc] initWithFrame:CGRectMake(100,20,60, 20) duration:8.0 andFadeLength:10.0f];
    [_lectureName setTextAlignment:NSTextAlignmentCenter];
    [_lectureName setBackgroundColor:[UIColor clearColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
    announcementtabwidth = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    NSLog(@"Ms Audio Upload viewWillAppear");
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playAnnouncement:)
                                                 name:@"playAnnouncementsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendAnnouncement:)
                                                 name:@"audioNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedAnnouncement:)
                                                 name:@"recievedAnnouncementsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(buttonSingleTap)
                                                 name:@"playAnnouncementsSingleTapNotification"
                                               object:nil];
    messageLabel.text=@"";
    messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    messageLabel.numberOfLines = 0;
    
    
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"MessageFrom"];
    
    
    if (str.length>0)
    { NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
        if ([sl isEqualToString:@"HQ"] || [sl isEqualToString:@"AllStores"])
        {
            if ([sl isEqualToString:@"AllStores"] ||[sl isEqualToString:@"Tous les magasins"])
            {
                [_lectureName setText:[dictLang objectForKey:@"Message to All Stores"]];//[NSString stringWithFormat:@"Message To %@",name];
            }
            else
            {
                NSString *name =[[[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedstoresname"] objectAtIndex:0]capitalizedString];
                //self. navigationItem.title=[NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Message to"],name];
                
                [_lectureName setText:[NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Message to"],name]];
            }
        }
        else
        {
            NSString *strTsr=[[NSUserDefaults standardUserDefaults] objectForKey:@"store"];
            NSString *groupStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"title"];
            
            if (strTsr.length>0 || [groupStr isEqualToString:@"Group"])
            {
                
                
                if ([groupStr isEqualToString:@"Group"])
                {
                   
                    //self. navigationItem.title = [dictLang objectForKey:@"Group Message"];
                    
                    [_lectureName setText:[dictLang objectForKey:@"Group Message"]];
                    //self. navigationItem.title=[NSString stringWithFormat:@"Group %@ to %@",str,[[[NSUserDefaults standardUserDefaults] objectForKey:@"store"]capitalizedString]];
                }
                else
                {
                    //self. navigationItem.title=[NSString stringWithFormat:@"%@ to %@",str,[[[NSUserDefaults standardUserDefaults] objectForKey:@"store"]capitalizedString]];
                    
                    [_lectureName setText:[NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Message to"],[[[NSUserDefaults standardUserDefaults] objectForKey:@"store"]capitalizedString]]];
                }
            }
            else
            {
                groupStr=[dictLang objectForKey:@"New everyone"];
                NSString *strLan=[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"];
                if ([strLan isEqualToString:@"en"])
                {
                     [_lectureName setText:[NSString stringWithFormat:@"%@ to %@",[dictLang objectForKey:@"Message"],[groupStr capitalizedString]]];
                }
                else
                {
                    [_lectureName setText:[NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Message"],[groupStr lowercaseString]]];
                }
                //self. navigationItem.title=[NSString stringWithFormat:@"%@ to %@",str,[[[NSUserDefaults standardUserDefaults] objectForKey:@"title"]capitalizedString]];
               
            }
        }
        //self. navigationItem.title=str;
    }
    
    int tabitemwidth=0;
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    if (screenSize.width ==414)
    {
        announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"20",@"151",@"282", nil];
        tabitemwidth=110;
    }
    else if (screenSize.width ==375)
    {
        announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"15",@"130",@"245", nil];
        tabitemwidth=110;
    }
    else
    {
        announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"30",@"130",@"233", nil];
        tabitemwidth=55;
    }
    
    
    
    
    if ([str isEqualToString:@"Announcement"])
    {
        ann_TabBar.hidden=NO;
        
        nowbtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nowbtn.frame =CGRectMake([[announcementtabwidth objectAtIndex:0]intValue], 7, tabitemwidth, 35);
        [nowbtn setTitle:@"Now" forState:UIControlStateNormal];
        nowbtn.backgroundColor=kTabbarColor;
        nowbtn.layer.cornerRadius=6;
        [nowbtn addTarget:self
                   action:@selector(now)
         forControlEvents:UIControlEventTouchUpInside];
        nowbtn.titleLabel.font=[UIFont fontWithName:kFontName size:12];
        
        [ann_TabBar addSubview:nowbtn];
        
        Hrsbtn = [UIButton buttonWithType:UIButtonTypeCustom];
        Hrsbtn.frame =CGRectMake([[announcementtabwidth objectAtIndex:1]intValue], 7, tabitemwidth, 35);
        [Hrsbtn setTitle:@"24Hrs" forState:UIControlStateNormal];
        Hrsbtn.layer.cornerRadius=6;
        Hrsbtn.titleLabel.font=[UIFont fontWithName:kFontName size:12];
        [Hrsbtn addTarget:self
                   action:@selector(hrs)
         forControlEvents:UIControlEventTouchUpInside];
        Hrsbtn.backgroundColor=kNavBackgroundColor;
        [ann_TabBar addSubview:Hrsbtn];
        
        weekbtn = [UIButton buttonWithType:UIButtonTypeCustom];
        weekbtn.frame =CGRectMake([[announcementtabwidth objectAtIndex:2]intValue], 7, tabitemwidth, 35);
        weekbtn.layer.cornerRadius=6;
        weekbtn.titleLabel.font=[UIFont fontWithName:kFontName size:12];
        [weekbtn addTarget:self
                    action:@selector(week)
          forControlEvents:UIControlEventTouchUpInside];
        [weekbtn setTitle:@"Week" forState:UIControlStateNormal];
        weekbtn.backgroundColor=kNavBackgroundColor;
        [ann_TabBar addSubview:weekbtn];
        
        
        NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
        
        if ([fav isEqualToString:@"fav"])
            
        {
            
            NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"];
            
            NSArray *tempArray = [str1 componentsSeparatedByString:@"-"];
            
            str1 = [tempArray objectAtIndex:0];
            
            
            
            
            
            if ([str1 isEqualToString:@"Now"])
                
            {
                
                [self now];
                
            }
            
            else if ([str1 isEqualToString:@"Today"])
                
            {
                
                [self hrs];
                
            }
            
            else if ([str1 isEqualToString:@"Week"])
                
            {
                
                [self week];
                
            }
            
            
            
        }
        
    }
    else
    {
        ann_TabBar.hidden=YES;
    }
    
    _lectureName.adjustsFontSizeToFitWidth=NO;
    [_lectureName setAnimationCurve:UIViewAnimationOptionCurveEaseIn];
    _lectureName.marqueeType = MLContinuous;
    [self.navigationItem setTitleView:_lectureName];
    
    [super viewWillAppear:animated];
}

-(void)now
{
    callType=21;
    nowbtn.backgroundColor = kTabbarColor;
    Hrsbtn.backgroundColor=kNavBackgroundColor;
    weekbtn.backgroundColor=kNavBackgroundColor;
}

-(void)hrs
{
    callType=23;
    nowbtn.backgroundColor = kNavBackgroundColor;
    Hrsbtn.backgroundColor=kTabbarColor;
    weekbtn.backgroundColor=kNavBackgroundColor;
}

-(void)week
{
    callType=25;
    nowbtn.backgroundColor = kNavBackgroundColor;
    Hrsbtn.backgroundColor=kNavBackgroundColor;
    weekbtn.backgroundColor=kTabbarColor;
}


-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ( [fromPush1 isEqualToString:@"YES"])
    {
        
        NSArray *viewcontrollers=[self.navigationController viewControllers];
        for (int i=0;i<[viewcontrollers count];i++)
        {
            UIViewController *vc=[viewcontrollers objectAtIndex:i];
            NSString *currentView=NSStringFromClass([vc class]);
            if ([currentView isEqualToString:@"VPViewController"])
            {
                [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
            }
        }
        
    }
    
    
}

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewDidLoad
{
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
   
    [deleteButton setTitle:[dictLang objectForKey:@"Delete"] forState:UIControlStateNormal];
    [sendButton setTitle:[dictLang objectForKey:@"Send"] forState:UIControlStateNormal];
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    
   // self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kNavBackgroundColor,
       NSFontAttributeName:[UIFont fontWithName:kFontName size:13]}];
    NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
     callType=21;
    
    UIBarButtonItem *favoritsButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"favorite"] style:UIBarButtonItemStyleDone target:self action:@selector(favoritsButtonAction)];
    self.navigationItem.rightBarButtonItem=favoritsButton;
    if ([fav isEqualToString:@"fav"])
    {
        [favoritsButton setEnabled:NO];
    }

    playButton.layer.cornerRadius=8;
    deleteButton.layer.cornerRadius=8;
    sendButton.layer.cornerRadius=8;
    playButton.backgroundColor=kGreyColor;
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    [self buttonsDisable];
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    [super viewDidLoad];
    [recordPauseButton addTarget:self action:@selector(buttondDownPressed:) forControlEvents: UIControlEventTouchDown];
    [recordPauseButton addTarget:self action:@selector(buttonUpPressed:) forControlEvents: UIControlEventTouchUpInside];
    [recordPauseButton addTarget:self action:@selector(buttonDragOutside:) forControlEvents: UIControlEventTouchDragOutside];
    [[AudioManager getsharedInstance] path];
     
    
}

-(void)favoritsButtonAction
{
     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Please enter the favorite name"],[[DiagnosticTool sharedManager] generateScreenCode:@"MultiMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please enter the favorite name"]] message:nil delegate:self cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
    alert.alertViewStyle=UIAlertViewStylePlainTextInput;
    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeASCIICapable];
    
    //[[alert textFieldAtIndex:1] becomeFirstResponder];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==108)
    {
        [self favoritsButtonAction];
    }
    else
    {
        if (buttonIndex==1)
        {
            UITextField *group = [actionSheet textFieldAtIndex:0];
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            
                
                group.text= [group.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                if (group.text.length>0 )
                {
                   // group.text= [group.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    NSMutableArray *newArray;
                    NSMutableArray *nameArray = [[NSMutableArray alloc]init];
                    newArray= [[EarboxDataStore sharedStore] favoritesArtists];
                    
                    for (int i=0; i<[newArray count]; i++)
                    {
                        Favorites *fav;
                        fav=[newArray objectAtIndex:i];
                        
                        [nameArray addObject:fav.favouriteName];
                    }
                    
                    if ([nameArray containsObject:group.text])
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@ %@ (%@%@%@%@i)",[dictLang objectForKey:@"This"],[[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"]capitalizedString],[dictLang objectForKey:@"name already exists"],[[DiagnosticTool sharedManager] generateScreenCode:@"MultiMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"favorite name already exists"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                        alert.tag=108;
                        [alert show];
                    }

                    else
                    {
                    NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
                    
                    if ([asGroup isEqualToString:@"ASGroup"])
                    {
                        
                    }
                    else
                    {
                        asGroup=[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
                    }
                    
                        NSString *featureType; ;
                        
                        NSString *Multistore =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
                        
                        NSString *str =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"MessageFrom"]];
                        
                        
                        
                        if ([str isEqualToString:@"Announcement"])
                            
                        {
                            
                            if (callType==21)
                                
                            {
                                
                                featureType=[NSString stringWithFormat:@"Now-%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"]];
                                
                            }
                            
                            else if (callType==23)
                                
                            {
                                
                                featureType=[NSString stringWithFormat:@"Today-%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"]];
                                
                            }
                            
                            else if (callType==25)
                                
                            {
                                
                                featureType=[NSString stringWithFormat:@"Week-%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"]];
                                
                            }
                            
                        }
                        
                        else
                            
                        {
                            
                            featureType=[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"];
                            
                            
                        }
                    
                        NSString *title=[[NSUserDefaults standardUserDefaults] objectForKey:@"store"];
                        if (title.length>0)
                        {
                            
                        }
                        else
                        {
                            title=@"";
                        }
                        
                    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
                    [postData setObject:str forKey:@"PlayAudioType"];
                    [postData setObject:@"Multi" forKey:@"FinalView"];
                    [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"] forKey:@"Groups"];
                    [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"] forKey:@"Stores"];
                    //[postData setObject:@"" forKey:@"ChannelSected"];
                    [postData setObject:group.text forKey:@"FavouriteName"];
                    [postData setObject:asGroup forKey:@"MessageType"];
                    //[postData setObject:@"" forKey:@"Sender"];//featureType
                        if (featureType.length!=0)
                        {
                            
                    [postData setObject:featureType forKey:@"FeatureType"];
                        }
                    [postData setObject:@"MS-MA" forKey:@"Originator"];
                    [postData setObject:@"iOS" forKey:@"PlatForm"];
                   // int rangeLow = 100;
                    //int rangeHigh = 100060;
                    //int randomNumber = arc4random() % (rangeHigh-rangeLow+1) + rangeLow;
                    [postData setObject:@"NewMessage" forKey:@"FavouriteType"];
                    [postData setObject:title forKey:@"NodeId"];
                    [postData setObject:@"" forKey:@"ParentId"];
                    [postData setObject:@"" forKey:@"LevelId"];
                    [postData setObject:@"false" forKey:@"StoreScreen"];
                        [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstoresname"] forKey:@"StoreNames"];
                        
                        NSString *tagOut =[[NSUserDefaults standardUserDefaults] objectForKey:@"storeTagOutName"];
                        [postData setObject:Multistore forKey:@"MultiStore"];
                        [postData setObject:tagOut forKey:@"HQTagOutName"];
                        [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedManagerAppID"] forKey:@"HQManagerAppID"];
                        [postData setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedDistServer"] forKey:@"Nodes"];
                        [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedDist"] forKey:@"MultiStoreNodeId"];
                        [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedDistsname"] forKey:@"MultiStoreNodesNames"];
                        [postData setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"levelname"] forKey:@"levelName"];
                        
                        [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] forKey:@"storeselect"];
                        
                        
                            [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"title"] forKey:@"title"];
                       
                    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
                    
                    
                    NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
                    [myQueueSales addOperationWithBlock:^{
                        NSDictionary *responsDic=[HttpHandler favSave:[loginDetails objectForKey:@"twUserName"] password:[loginDetails objectForKey:@"twPassword"] jSonStr:postData userScreen:@"MultiMessageVC"];
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            if ([responsDic objectForKey:@"Error"])
                            {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                                
                                [alert show];
                            }
                            else if ([responsDic objectForKey:@"FavouriteID"])
                            {
                            [postData setObject:[responsDic objectForKey:@"FavouriteID"] forKey:@"FavouriteID"];
                            [array addObject:postData];
                            
                            //[array addObject:responsDic];
                            [[Earboxinboxmodel sharedModel]favoritesInsert:array];
                            }
                        }];
                    }];
                    
                    }
                    
                }
                else
                {
                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@ (%@%@%@%@i)",[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"],[dictLang objectForKey:@"name cannot be empty"],[[DiagnosticTool sharedManager] generateScreenCode:@"MultiMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"favorite name cannot be empty"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    alert.tag=108;
                    [alert show];
                }
            
            
            
        }
    }
}


-(void)buttonSingleTap
{
    playButton.backgroundColor=kGreyColor;
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    
    [self buttonsDisable];
    messageLabel.text=@"";
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    
}


-(void)buttonsDisable
{
    if (playButton.isEnabled)
    {
        [playButton setEnabled:NO];
    }
    if (deleteButton.isEnabled)
    {
        [deleteButton setEnabled:NO];
    }
    if (sendButton.isEnabled)
    {
        [sendButton setEnabled:NO];
    }
}

-(void)playAnnouncement:(NSNotification *)sender
{
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    messageLabel.text=@"";
    deleteButton.backgroundColor=kNavBackgroundColor;
    [deleteButton setEnabled:YES];
    
    if (audioSendButton==YES)
    {
        sendButton.backgroundColor=kGreyColor;
        [sendButton setEnabled:NO];
        
    }
    else
    {
        messageLabel.text=[dictLang objectForKey:@"Ready to send"];
        sendButton.backgroundColor=kNavBackgroundColor;
        [sendButton setEnabled:YES];
        
    }
    
}

-(void)sendAnnouncement:(NSNotification *)sender

{
    
    nowbtn.enabled=NO;
    
    Hrsbtn.enabled=NO;
    
    weekbtn.enabled=NO;
    
    
    
    NSString *msStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
    
    
    
    if ([msStr isEqualToString:@"MS"])
        
    {
        
        [self sendMsAnnMsg:sender];
        
    }
    
    else
        
    {
        
        self.navigationController.navigationBar.userInteractionEnabled = NO;
        
        self.navigationController.navigationBar.tintColor = [UIColor lightGrayColor];
        
        NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
        
        if ([currentView isEqualToString:@"MutilMessageViewController"])
            
        {//MutilMessageViewController
            
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            
            NetworkStatus internetStatus = [reachability currentReachabilityStatus];
            
            if (internetStatus == NotReachable)
                
            {
                
                [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"MultiMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
                
                
                
            }
            
            else
                
            {
                
                messageaudiourl=sender.object;
                
                if ([[AudioManager getsharedInstance]getAudioduration:messageaudiourl]<1.0)
                    
                {
                    
                    [AlertView alert:[dictLang objectForKey:@"Audio length error"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Audio length is too short to send"],[[DiagnosticTool sharedManager] generateScreenCode:@"MultiMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"Audio length is too short to send"]]];
                    
                    messageLabel.text=@"";
                    
                    recordPauseButton.userInteractionEnabled=YES;
                    
                    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
                    
                    self.navigationController.navigationBar.userInteractionEnabled = YES;
                    
                    self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
                    
                }
                
                else
                    
                {
                    
                    NSString *millisec=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ];
                    
                    NSArray *tempArray = [millisec componentsSeparatedByString:@"."];
                    
                    
                    
                    NSData *file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
                    
                    NSString *macId;
                    
                    if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
                        
                        macId=[[NSString stringWithFormat:@"%@",
                                
                                [[[[UIDevice currentDevice] identifierForVendor] UUIDString]substringFromIndex:24]]lowercaseString];
                        
                    }
                    
                    
                    
                    NSDictionary  *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
                    
                    NSString *str =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"MessageFrom"]];
                    
                    NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
                    
                    if ([str isEqualToString:@"Message"] || [str isEqualToString:@"Announcement"])
                        
                    {
                        
                        NSString *featureType;
                        
                        NSString *type;
                        
                        
                        
                        NSMutableArray *userArray = [[NSMutableArray alloc] init];
                        
                        NSString *strASMember = [[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
                        
                        if ([str isEqualToString:@"Message"])
                            
                        {
                            
                            type=@"message";
                            
                            featureType = [NSString stringWithFormat:@"msg_%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"]];
                            
                            
                            
                            if ([asGroup isEqualToString:@"ASGroup"])
                                
                            {
                                
                                pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",@"all",@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:messageaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithInt:108],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
                                
                            }
                            
                            //to:TagOutName Calltype:102
                            
                            else
                                
                            {
                                
                                
                                
                                if ([strASMember isEqualToString:@"HQ"])
                                    
                                {
                                    
                                    pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",[[NSUserDefaults standardUserDefaults] objectForKey:@"storeTagOutName"],@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:messageaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithInt:102],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
                                    
                                }
                                
                                else
                                    
                                {
                                    NSString *titleStr =[[NSUserDefaults standardUserDefaults]objectForKey:@"title"];
                                    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"] || [titleStr isEqualToString:@"Everyone"] )
                                    {
                                         pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",@"all",@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:messageaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithInt:33],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
                                    }
                                    else
                                    {
                                         pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",@"all",@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:messageaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithInt:17],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
                                    }
                                   
                                    
                                }
                                
                                
                                
                            }
                            
                        }
                        
                        else
                            
                        {
                            
                            // type = @"announcement_now";
                            
                            if (callType==21)
                                
                            {
                                
                                type = @"announcement_now";
                                
                            }
                            
                            else if (callType==23)
                                
                            {
                                
                                type = @"announcement_today";
                                
                            }
                            
                            else if (callType==25)
                                
                            {
                                
                                type = @"announcement_week";
                                
                            }
                            
                            
                            
                            featureType = [NSString stringWithFormat:@"announcement_%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"]];
                            
                            
                            
                            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"] isEqualToString:@"all"] ||[[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"] isEqualToString:@"selectstores"])
                                
                            {
                                
                                [userArray addObject:@"all"];
                                
                            }
                            
                            else
                                
                            {
                                
                                [userArray addObject:@""];
                                
                            }
                            
                            pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",@"all",@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:messageaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithInteger:callType],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
                            
                        }
                        
                        NSDictionary *whoDic;
                        
                        if ([asGroup isEqualToString:@"ASGroup"])
                            
                        {
                            
                            whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[loginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",type,@"type",macId,@"macId",millisec,@"request_time",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groups",featureType,@"featureType",nil];
                            
                            //remove groups and add users:managerappid
                            
                        }
                        
                        else
                            
                        {
                            
                            if ([strASMember isEqualToString:@"HQ"])
                                
                            {
                                
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[loginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",type,@"type",macId,@"macId",millisec,@"request_time",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedManagerAppID"],@"users",featureType,@"featureType",nil];
                                
                            }
                            
                            else
                                
                            {
                                
                                
                                if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"])
                                {
                                    [userArray addObject:@"all"];
                                    whoDic=[NSDictionary dictionaryWithObjectsAndKeys:userArray,@"users",[loginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",type,@"type",macId,@"macId",millisec,@"request_time",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groups",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"],@"store",featureType,@"featureType",nil];
                                }
                                else
                                {
                                    //
                                  //  NSString *titleStr =[[NSUserDefaults standardUserDefaults]objectForKey:@"title"];
                                    if ([featureType isEqualToString:@"msg_all"])
                                    {
                                        [userArray addObject:@"all"];
                                        whoDic=[NSDictionary dictionaryWithObjectsAndKeys:userArray,@"users",[loginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",type,@"type",macId,@"macId",millisec,@"request_time",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groups",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"],@"store",featureType,@"featureType",nil];
                                    }
                                    else
                                    {
                                        whoDic=[NSDictionary dictionaryWithObjectsAndKeys:userArray,@"users",[loginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",type,@"type",macId,@"macId",millisec,@"request_time",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groups",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"],@"store",featureType,@"featureType",nil];
                                    }
                                
                                }
                                
                                
                                
                            }
                            
                            
                            
                        }
                        
                        
                        
                        [HttpHandler audioFileSend:whoDic audioFile:file1Data pns:pns userScreen:@"MultiMessageVC"];
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}



-(void)sendMsAnnMsg:(NSNotification *)sender

{
    
    
    
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    
    self.navigationController.navigationBar.tintColor = [UIColor lightGrayColor];
    
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    
    if ([currentView isEqualToString:@"MutilMessageViewController"])
        
    {//MutilMessageViewController
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        
        if (internetStatus == NotReachable)
            
        {
            
            [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"MultiMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
            
        }
        
        else
            
        {
            
            messageaudiourl=sender.object;
            
            if ([[AudioManager getsharedInstance]getAudioduration:messageaudiourl]<1.0)
                
            {
                
                [AlertView alert:[dictLang objectForKey:@"Audio length error"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Audio length is too short to send"],[[DiagnosticTool sharedManager] generateScreenCode:@"MultiMessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"Audio length is too short to send"]]];
                
                messageLabel.text=@"";
                
                recordPauseButton.userInteractionEnabled=YES;
                
                [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
                
                self.navigationController.navigationBar.userInteractionEnabled = YES;
                
                self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
                
            }
            
            else
                
            {
                
                NSString *millisec=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ];
                
                NSArray *tempArray = [millisec componentsSeparatedByString:@"."];
                
                
                
                NSData *file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
                
                NSString *macId;
                
                if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
                    
                    macId=[[NSString stringWithFormat:@"%@",
                            
                            [[[[UIDevice currentDevice] identifierForVendor] UUIDString]substringFromIndex:24]]lowercaseString];
                    
                }
                
                
                
                NSDictionary  *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
                
                NSString *str =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"MessageFrom"]];
                
                
                
                if ([str isEqualToString:@"Message"] || [str isEqualToString:@"Announcement"])
                    
                {
                    
                    NSString *featureType;
                    
                    NSString *type;
                    
                    
                    
                    NSMutableArray *userArray = [[NSMutableArray alloc] init];
                    
                    if ([str isEqualToString:@"Message"])
                        
                    {
                        
                        type=@"message";
                        
                        featureType = [NSString stringWithFormat:@"msg_%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"]];
                        
                        
                        NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"title"];
                        if ([str isEqualToString:@"Everyone"])
                        {
                            pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",@"all",@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:messageaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithInt:33],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
                        }
                        else
                        {
                            pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",@"all",@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:messageaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithInt:17],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
                        }
                        
                    }
                    
                    else
                        
                    {
                        
                        // type = @"announcement_now";
                        
                        if (callType==21)
                            
                        {
                            
                            type = @"announcement_now";
                            
                        }
                        
                        else if (callType==23)
                            
                        {
                            
                            type = @"announcement_today";
                            
                        }
                        
                        else if (callType==25)
                            
                        {
                            
                            type = @"announcement_week";
                            
                        }
                        
                        
                        
                        featureType = [NSString stringWithFormat:@"announcement_%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"]];
                        
                        
                        
                        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"] isEqualToString:@"all"] ||[[[NSUserDefaults standardUserDefaults] objectForKey:@"featureType"] isEqualToString:@"selectstores"])
                            
                        {
                            
                            [userArray addObject:@"all"];
                            
                        }
                        
                        else
                            
                        {
                            
                            [userArray addObject:@""];
                            
                        }
                        
                        pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",@"all",@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:messageaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithInteger:callType],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
                        
                    }
                    
                    NSDictionary *whoDic;
                    
                    
                    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"title"];
                    if ([str isEqualToString:@"Everyone"])
                    {
                        [userArray addObject:@"all"];
                        whoDic=[NSDictionary dictionaryWithObjectsAndKeys:userArray,@"users",[loginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",type,@"type",macId,@"macId",millisec,@"request_time",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groups",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"],@"store",featureType,@"featureType",nil];
                    }
                    else
                    {
                        [userArray addObject:@""];
                         whoDic=[NSDictionary dictionaryWithObjectsAndKeys:userArray,@"users",[loginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",type,@"type",macId,@"macId",millisec,@"request_time",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groups",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"],@"store",featureType,@"featureType",nil];
                       // whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[loginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",type,@"type",macId,@"macId",millisec,@"request_time",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groups",[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedDistServer"],@"nodes",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"],@"stores",featureType,@"featureType",[[NSUserDefaults standardUserDefaults] objectForKey:@"levelname"],@"levelName",nil];
                    }
                    
                    
                    
                    
                    [HttpHandler audioFileSend:whoDic audioFile:file1Data pns:pns userScreen:@"MultiMessageVC"];
                    
                }
                
            }
            
        }
        
    }
    
    
    
}


-(void)recievedAnnouncement:(NSNotification *)sender
{
    
    nowbtn.enabled=YES;
    
    Hrsbtn.enabled=YES;
    
    weekbtn.enabled=YES;
    
    
    
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    
    if ([currentView isEqualToString:@"MutilMessageViewController"])
        
    {
        
        recordPauseButton.userInteractionEnabled=YES;
        
        [playButton setEnabled:YES];
        
        [deleteButton setEnabled:YES];
        
        playButton.backgroundColor=kNavBackgroundColor;
        
        deleteButton.backgroundColor=kNavBackgroundColor;
        
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
        
        
        
        NSString *str1 =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"MessageFrom"]];
        
        if ([[sender.object objectAtIndex:0] isEqualToString:@"error"])
            
        {
            
            if ([str1 isEqualToString:@"Message"])
                
            {
                
                messageLabel.text=[dictLang objectForKey:@"Message not sent"];
                
            }
            
            else
                
            {
                
                messageLabel.text=[dictLang objectForKey:@"Announcement not sent"];
                
            }
            
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            
            self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
            
            sendButton.backgroundColor=kNavBackgroundColor;
            
            [sendButton setEnabled:YES];
            
            
            
        }
        
        else if ([[sender.object objectAtIndex:0] isEqualToString:@"message"])
            
        {
            
            
            
            if ([str1 isEqualToString:@"Message"])
                
            {
                
                [pns setObject:@"HEARD" forKey:@"status"];
                
                NSString *asMember = [[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
                
                NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
                
                
                
                if ([asGroup isEqualToString:@"ASGroup"] || [asMember isEqualToString:@"HQ"])
                    
                {
                    
                    if ([asGroup isEqualToString:@"ASGroup"])
                        
                    {
                        
                        [pns setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroupName"] forKey:@"groupName"];
                        
                        NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groupId" ,nil];
                        
                        // NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstoresname"],@"storeName", nil];
                        
                        [pns setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",nil] forKey:@"audience"];
                        
                    }
                    
                    else if ([asMember isEqualToString:@"HQ"])
                        
                    {
                        
                        [pns setObject:@[] forKey:@"groupName"];
                        NSString *managerAppId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedManagerAppID"] objectAtIndex:0];
                        NSDictionary *hq=[NSDictionary dictionaryWithObjectsAndKeys:managerAppId,@"managerAppId",[[NSUserDefaults standardUserDefaults]objectForKey:@"storeTagOutName"],@"tagOutName", nil];
                        [pns setObject:hq forKey:@"hq"];
                        [pns setObject:managerAppId forKey:@"managerAppId"];
                        //NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:@[],@"groupId" ,nil];
                        
                        //add tagoutname and managerAppid into db
                        
                        [pns setObject:[NSDictionary dictionaryWithObjectsAndKeys:@[],@"groups",nil] forKey:@"audience"];
                        
                    }
                    
                }
                
                else
                    
                {
                    
                    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
                    
                    
                    
                    if ([str isEqualToString:@"MS"])
                        
                    {
                        
                        //ad nodeid(selectedDist)
                        
                        // nodename userdefault key selectedDistsname
                        
                        // NSDictionary *stores,*nodes;
                        
                        // NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groupName" ,nil];
                        
                        //                        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstoresname"] count]==0)
                        //
                        //                        {
                        //
                        //                            stores=[NSDictionary dictionaryWithObjectsAndKeys:@[],@"storeName",@[],@"storeId", nil];
                        //                            nodes=[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedDistsname"],@"nodeName",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedDist"],@"nodeId", nil];
                        //
                        //                        }
                        //
                        //                        else
                        //
                        //                        {
                        NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groupName" ,nil];
                        
                        NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstoresname"],@"storeName",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"],@"storeId", nil];
                        
                        [pns setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                        /*
                         stores=[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstoresname"],@"storeName",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"],@"storeId", nil];
                         nodes=[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedDistsname"],@"nodeName",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedDist"],@"nodeId", nil];
                         */
                        
                        // }
                        // NSDictionary *node=[NSDictionary dictionaryWithObjectsAndKeys:nodes,@"nodes", nil];
                        
                        
                        [pns setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                        
                    }
                    
                    else
                        
                    {
                        
                        NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groupName" ,nil];
                        
                        NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstoresname"],@"storeName",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"],@"storeId", nil];
                        
                        [pns setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                        
                    }
                    
                    
                    
                }
                
                [pns setObject:[NSNumber numberWithInt:2] forKey:@"maCallType"];
                
                //[pns setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"] forKey:@"pushTxId"];
                
                if ([sender.object count]==2)
                    
                {
                    
                    [pns setObject:[sender.object objectAtIndex:1] forKey:@"pushTxId"];
                    
                }
                
                messageLabel.text=[dictLang objectForKey:@"Message Sent"];
                
                if ([pns count]>0)
                    
                {
                    
                    [[Earboxinboxmodel sharedModel]messagesentSave:[NSArray arrayWithObject:pns]];
                    
                }
                
                
                
            }
            
            else
                
            {
                
                [pns setObject:@"HEARD" forKey:@"status"];
                
                NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groupName" ,nil];
                
                NSDictionary *stores;
                
                
                
                NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
                
                
                
                if ([str isEqualToString:@"MS"])
                    
                {
                    
                    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstoresname"] count]==0)
                        
                    {
                        
                        stores=[NSDictionary dictionaryWithObjectsAndKeys:@[],@"storeName",@[],@"storeId", nil];
                        
                    }
                    
                    else
                        
                    {
                        
                        stores=[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstoresname"],@"storeName",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"],@"storeId", nil];
                        
                    }
                    
                }
                
                else
                    
                    
                    
                {
                    
                    if ([[[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstoresname"] objectAtIndex:0] length]==0)
                        
                    {
                        
                        stores=[NSDictionary dictionaryWithObjectsAndKeys:@[@"All"],@"storeName",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"],@"storeId", nil];
                        
                    }
                    
                    else
                        
                    {
                        
                        stores=[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstoresname"],@"storeName",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"],@"storeId", nil];
                        
                    }
                    
                }
                
                [pns setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                
                // [pns setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"] forKey:@"pushTxId"];
                
                if ([sender.object count]==2)
                    
                {
                    
                    [pns setObject:[sender.object objectAtIndex:1] forKey:@"pushTxId"];
                    
                }
                
                [pns setObject:[NSNumber numberWithInt:2] forKey:@"maCallType"];
                
                messageLabel.text=[dictLang objectForKey:@"Announcement Sent"];
                
                if ([pns count]>0)
                    
                {
                    
                    [[Earboxinboxmodel sharedModel]announcementsentSave:[NSArray arrayWithObject:pns]];
                    
                }
                
            }
            
            
            
            audioSendButton=YES;
            
            sendButton.backgroundColor=kGreyColor;
            
            
            
            [sendButton setEnabled:NO];
            
            
            
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            
            self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
            
        }
        
        
        
    }
    
    
}

- (void)buttondDownPressed:(UIButton *)sender
{
    [self buttonsDisable];
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    playButton.backgroundColor=kGreyColor;
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    messageLabel.text=[dictLang objectForKey:@"Recording..."];
    receivedMessageLabel.text=@"";
    [[AudioManager getsharedInstance] recordPauseTapped];
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue_white.png"] forState:UIControlStateNormal];
}



- (void)buttonUpPressed:(UIButton *)sender {
    messageLabel.text=[dictLang objectForKey:@"Ready to send"];
    audioSendButton=NO;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
    [playButton setEnabled:YES];
    [deleteButton setEnabled:YES];
    [sendButton setEnabled:YES];
    deleteButton.backgroundColor=kNavBackgroundColor;
    sendButton.backgroundColor=kNavBackgroundColor;
    playButton.backgroundColor=kNavBackgroundColor;
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    [[AudioManager getsharedInstance] stopTapped];
    
}

-(void)buttonDragOutside:(UIButton *)sender
{
    messageLabel.text=[dictLang objectForKey:@"Ready to send"];
    audioSendButton=NO;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
    [playButton setEnabled:YES];
    [deleteButton setEnabled:YES];
    [sendButton setEnabled:YES];
    deleteButton.backgroundColor=kNavBackgroundColor;
    sendButton.backgroundColor=kNavBackgroundColor;
    playButton.backgroundColor=kNavBackgroundColor;
    [[AudioManager getsharedInstance] stopTapped];
}

- (IBAction)playTapped:(UIButton *)sender {
    if ([playButton.titleLabel.text isEqualToString:[dictLang objectForKey:@"Stop"]])
    {
        if (audioSendButton==YES)
        {
            [deleteButton setEnabled:YES];
            [sendButton setEnabled:NO];
            deleteButton.backgroundColor=kNavBackgroundColor;
            sendButton.backgroundColor=kGreyColor;
            messageLabel.text=@"";
            [[AudioManager getsharedInstance]audioplayerstop];
            [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
        }
        else
        {
            [deleteButton setEnabled:YES];
            [sendButton setEnabled:YES];
            deleteButton.backgroundColor=kNavBackgroundColor;
            sendButton.backgroundColor=kNavBackgroundColor;
            messageLabel.text=@"";
            [[AudioManager getsharedInstance]audioplayerstop];
            [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
        }
    }
    else
    {
        [playButton setTitle:[dictLang objectForKey:@"Stop"] forState:UIControlStateNormal];
        [deleteButton setEnabled:NO];
        [sendButton setEnabled:NO];
        deleteButton.backgroundColor=kGreyColor;
        sendButton.backgroundColor=kGreyColor;
        messageLabel.text=[dictLang objectForKey:@"Replaying..."];
        receivedMessageLabel.text=@"";
        [[AudioManager getsharedInstance] playTapped];
    }
}


- (IBAction)sendFileTapped:(id)sender
{
    [deleteButton setEnabled:NO];
    [sendButton setEnabled:NO];
    [playButton setEnabled:NO];
    playButton.backgroundColor=[UIColor grayColor];
    deleteButton.backgroundColor=[UIColor grayColor];
    sendButton.backgroundColor=[UIColor grayColor];
    recordPauseButton.userInteractionEnabled=NO;
    messageLabel.text=[dictLang objectForKey:@"Sending..."];
    receivedMessageLabel.text=@"";
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_gray.png"] forState:UIControlStateNormal];
    [[AudioManager getsharedInstance] sendTapped];
    
}

- (IBAction)deleteFileTapped:(id)sender
{
    NSString *str1 =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"MessageFrom"]];
    if ([str1 isEqualToString:@"Message"])
    {
        messageLabel.text=[dictLang objectForKey:@"Message deleted"];
    }
    else
    {
        messageLabel.text=[dictLang objectForKey:@"Announcement deleted"];
    }
    receivedMessageLabel.text=@"";
    deleteButton.backgroundColor=[UIColor grayColor];
    sendButton.backgroundColor=[UIColor grayColor];
    playButton.backgroundColor=[UIColor grayColor];
    [sendButton setEnabled:NO];
    [playButton setEnabled:NO];
    [deleteButton setEnabled:NO];
    [[AudioManager getsharedInstance] deleteTapped];
}

@end
