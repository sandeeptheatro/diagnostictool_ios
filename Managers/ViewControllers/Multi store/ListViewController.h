//
//  ListViewController.h
//  ManagersMockup
//
//  Created by Ravi on 31/08/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *storesTableview;
    IBOutlet UIButton *backButton;
}

@end
