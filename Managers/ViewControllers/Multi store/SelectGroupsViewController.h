//
//  SelectGroupsViewController.h
//  Managers
//
//  Created by Ravi on 05/10/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Earboxinboxmodel.h"
#import "EarboxDataStore.h"

@interface SelectGroupsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UIGestureRecognizerDelegate,UISearchDisplayDelegate>
{
    IBOutlet UITableView *groupTable,*asTable;
    IBOutlet UIButton *allButton,*check_btn,*closeBtn;
    IBOutlet UILabel *all_Lbl;
    BOOL buttonSelected;
    IBOutlet UISearchBar *searchBar;
    IBOutlet UIView *as_view;
    IBOutlet UILabel *lbl_Green;
}

-(IBAction)allSelected:(id)sender;
-(IBAction)cancelAction:(id)sender;
@end
