//
//  SelectGroupsViewController.m
//  Managers
//
//  Created by Ravi on 05/10/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "SelectGroupsViewController.h"
#import "ListCell.h"
#import "HttpHandler.h"
#import "Reachability.h"
#import "Constant.h"
#import "AlertView.h"
#import "ActivityIndicatorView.h"
#import "DiagnosticTool.h"

@interface SelectGroupsViewController ()
{
    NSArray *storesArray;
    NSMutableArray *selectedArray,*searchResultsAll,*asNamesArray,*selectedstoresname;
    NSMutableDictionary *loginDetails;
    NSMutableArray *checkedArray;
    UIBarButtonItem *chkmanuaaly;
    ListCell *cell;
    NSMutableArray *groupSelectArray,*cellSelectedArray;
    UIBarButtonItem *favoritsButton;
    NSDictionary *dictLang;
}

@end

@implementation SelectGroupsViewController

- (void)viewDidLoad
{
    MarqueeLabel *_lectureName = [[MarqueeLabel alloc] initWithFrame:CGRectMake(100,20,60, 20) duration:8.0 andFadeLength:10.0f];
    [_lectureName setTextAlignment:NSTextAlignmentCenter];
    [_lectureName setBackgroundColor:[UIColor clearColor]];
    
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    all_Lbl.text=[dictLang objectForKey:@"all"];
    searchBar.placeholder=[dictLang objectForKey:@"Search"];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kNavBackgroundColor,
       NSFontAttributeName:[UIFont fontWithName:kFontName size:13]}];
     groupSelectArray=[[NSMutableArray alloc] init];
    selectedArray =[[NSMutableArray alloc] init];
    checkedArray = [[NSMutableArray alloc] init];
    asNamesArray = [[NSMutableArray alloc] init];
    cellSelectedArray = [[NSMutableArray alloc] init];
    selectedstoresname = [[NSMutableArray alloc] init];
    
    buttonSelected=NO;
    
    closeBtn.layer.masksToBounds = YES;
    closeBtn.layer.cornerRadius = 5.0;
    
    asTable.layer.masksToBounds = YES;
    asTable.layer.cornerRadius = 5.0;
    
    asTable.hidden=YES;
    as_view.hidden=YES;
    NSString *fromStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"MessageFrom"];
    if ([fromStr isEqualToString:@"Message"])
    {
       // self.navigationItem.title = [dictLang objectForKey:@"Group Message"];
        [_lectureName setText:[dictLang objectForKey:@"Group Message"]];
    }
    else
    {
        //self.navigationItem.title = @"Announcement To:";
        
        [_lectureName setText:@"Announcement To:"];
    }
    NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
    if ([asGroup isEqualToString:@"ASGroup"])
    {
        /*
         UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
         initWithTarget:self action:@selector(handleLongPress:)];
         lpgr.minimumPressDuration = 2.0; //seconds
         lpgr.delegate = self;
         [groupTable addGestureRecognizer:lpgr];
         */
        
        asTable.allowsMultipleSelection=NO;
        //all_Lbl.hidden=YES;
        //allButton.hidden=YES;
        //check_btn.hidden=YES;
        [chkmanuaaly setEnabled:YES];
        //groupTable.allowsMultipleSelection = NO;
        NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
        
        favoritsButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"favorite"] style:UIBarButtonItemStyleDone target:self action:@selector(favoritsButtonAction)];
        
        if ([fav isEqualToString:@"fav"])
        {
            [favoritsButton setEnabled:NO];
        }
        
        chkmanuaaly = [[UIBarButtonItem alloc]initWithTitle:[dictLang objectForKey:@"Next"] style:UIBarButtonItemStylePlain target:self action:@selector(nextview)];
        self.navigationItem.rightBarButtonItems=@[chkmanuaaly,favoritsButton];
        
    }
    else
    {
        
        storesArray=[[NSMutableArray alloc] init];
        
        NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
        
        
        favoritsButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"favorite"] style:UIBarButtonItemStyleDone target:self action:@selector(favoritsButtonAction)];
        
        if ([fav isEqualToString:@"fav"])
        {
            [favoritsButton setEnabled:NO];
        }
        
        chkmanuaaly = [[UIBarButtonItem alloc]initWithTitle:[dictLang objectForKey:@"Next"] style:UIBarButtonItemStylePlain target:self action:@selector(nextview)];
        self.navigationItem.rightBarButtonItems=@[chkmanuaaly,favoritsButton];
        [chkmanuaaly setEnabled:NO];
        all_Lbl.hidden=YES;
        allButton.hidden=YES;
        check_btn.hidden=YES;
        //self.navigationItem.rightBarButtonItem=chkmanuaaly;
        
        groupTable.allowsMultipleSelection = YES;
    }
    
    [self configAPICall];
    
    
    _lectureName.adjustsFontSizeToFitWidth=NO;
    [_lectureName setAnimationCurve:UIViewAnimationOptionCurveEaseIn];
    _lectureName.marqueeType = MLContinuous;
    [self.navigationItem setTitleView:_lectureName];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)favoritsButtonAction
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Please enter the favorite name"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectGroupsVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please enter the favorite name"]] message:nil delegate:self cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
    alert.alertViewStyle=UIAlertViewStylePlainTextInput;
    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeASCIICapable];
    
    //[[alert textFieldAtIndex:1] becomeFirstResponder];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==108)
    {
        [self favoritsButtonAction];
    }
    else
    {
        if (buttonIndex==1)
        {
            UITextField *group = [actionSheet textFieldAtIndex:0];
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            group.text= [group.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if (group.text.length>0 )
            {
                //group.text= [group.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                NSMutableArray *newArray = [[NSMutableArray alloc] init];
                NSMutableArray *nameArray = [[NSMutableArray alloc]init];
                newArray= [[EarboxDataStore sharedStore] favoritesArtists];
                
                for (int i=0; i<[newArray count]; i++)
                {
                    Favorites *fav;
                    fav=[newArray objectAtIndex:i];
                    
                    [nameArray addObject:fav.favouriteName];
                }
                
                if ([nameArray containsObject:group.text])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@ %@ (%@%@%@%@i)",[dictLang objectForKey:@"This"],[[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"]capitalizedString],[dictLang objectForKey:@"name already exists"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectGroupsVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"favorite name already exists"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    alert.tag=108;
                    [alert show];
                }
                else
                {
                    NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
                    
                    if ([asGroup isEqualToString:@"ASGroup"])
                    {
                        
                    }
                    else
                    {
                        asGroup=[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
                    }
                    NSString *str =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"MessageFrom"]];
                    //NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
                    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
                    [postData setObject:str forKey:@"PlayAudioType"];
                    [postData setObject:@"Groups" forKey:@"FinalView"];
                    [postData setObject:selectedArray forKey:@"Groups"];
                    [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstores"] forKey:@"Stores"];
                    //[postData setObject:@"" forKey:@"ChannelSected"];
                    [postData setObject:group.text forKey:@"FavouriteName"];
                    [postData setObject:asGroup forKey:@"MessageType"];
                    //[postData setObject:@"" forKey:@"Sender"];//featureType
                    [postData setObject:@"" forKey:@"FeatureType"];
                    [postData setObject:@"MS-MA" forKey:@"Originator"];
                    [postData setObject:@"iOS" forKey:@"PlatForm"];
                    [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedstoresname"] forKey:@"StoreNames"];
                    // int rangeLow = 100;
                    // int rangeHigh = 100060;
                    // int randomNumber = arc4random() % (rangeHigh-rangeLow+1) + rangeLow;
                    [postData setObject:@"NewMessage" forKey:@"FavouriteType"];
                    [postData setObject:@"" forKey:@"NodeId"];
                    [postData setObject:@"" forKey:@"ParentId"];
                    [postData setObject:@"" forKey:@"LevelId"];
                    [postData setObject:@"false" forKey:@"StoreScreen"];
                    
                    [postData setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"] forKey:@"MultiStore"];
                    [postData setObject:@"" forKey:@"HQTagOutName"];
                    [postData setObject:@[] forKey:@"HQManagerAppID"];
                    [postData setObject:@[] forKey:@"Nodes"];
                    [postData setObject:@[] forKey:@"MultiStoreNodeId"];
                    [postData setObject:@[] forKey:@"MultiStoreNodesNames"];
                    [postData setObject:@"" forKey:@"levelName"];
                    [postData setObject:@"" forKey:@"storeselect"];
                    [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"title"] forKey:@"title"];
                    //[array addObject:postData];
                    //[[NSUserDefaults standardUserDefaults] setObject:array forKey:@"FavDetails"];
                    
                    
                    
                    NSError *error;
                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData // Here you can pass array or dictionary
                                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                                         error:&error];
                    NSString *jsonString;
                    if (jsonData)
                    {
                        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                        //This is your JSON String
                        //NSUTF8StringEncoding encodes special characters using an escaping scheme
                    } else {
                        NSLog(@"SelectGroupsViewController-Got an error: %@", error);
                        jsonString = @"";
                    }
                    
                    NSDictionary *loginDetails1 =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
                    
                    
                    
                    //[self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
                    NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
                    [myQueueSales addOperationWithBlock:^{
                        NSDictionary *responsDic=[HttpHandler favSave:[loginDetails1 objectForKey:@"twUserName"] password:[loginDetails1 objectForKey:@"twPassword"] jSonStr:postData userScreen:@"SelectGroupsVC"];
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            if ([responsDic objectForKey:@"Error"])
                            {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];

                                
                                [alert show];
                            }
                            else if ([responsDic objectForKey:@"FavouriteID"])
                            {
                                [postData setObject:[responsDic objectForKey:@"FavouriteID"] forKey:@"FavouriteID"];
                                [array addObject:postData];
                                [[Earboxinboxmodel sharedModel]favoritesInsert:array];
                            }
                        }];
                    }];
                }
                
            }
            
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@ (%@%@%@%@i)",[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"],[dictLang objectForKey:@"name cannot be empty"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectGroupsVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"favorite name cannot be empty"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                alert.tag=108;
                [alert show];
            }
            
            
        }
    }
}


-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:groupTable];
    
    NSIndexPath *indexPath = [groupTable indexPathForRowAtPoint:p];
    if (indexPath == nil) {
        NSLog(@"SelectGroupsViewController-long press on table view but not on a row");
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        NSLog(@"SelectGroupsViewController long press on table view at row %ld", (long)indexPath.row);
    } else {
        NSLog(@"SelectGroupsViewController-gestureRecognizer.state = %ld", (long)gestureRecognizer.state);
    }
}



-(void)configAPICall
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectGroupsVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectGroups"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
        [myQueueSales addOperationWithBlock:^{
            NSDictionary *responsDic;
            NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
            if ([asGroup isEqualToString:@"ASGroup"])
            {
                responsDic=[HttpHandler selectGroupsASV3:@"SelectGroupsVC"];
            }
            else
            {
                responsDic=[HttpHandler selectGroupsV2:@"SelectGroupsVC"];
            }
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                
                NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
                NSArray *sortedArray;
                
                if ([asGroup isEqualToString:@"ASGroup"])
                {
                    sortedArray=[responsDic objectForKey:@"groups"];
                    
                }
                else
                {
                    if([sl isEqualToString:@"SL"])
                    {
                        sortedArray=[responsDic objectForKey:@"groups"];
                        
                    }
                    else
                    {
                        sortedArray=[responsDic objectForKey:@"common_groups"];
                        
                    }
                }
                if ([responsDic objectForKey:@"Error"])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    [alert show];
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                }
                else
                {
                if ([[responsDic objectForKey:@"common_groups"]  isKindOfClass:[NSNull class]]||[[responsDic objectForKey:@"groups"]  isKindOfClass:[NSNull class]])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"No Groups found!"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectGroupsVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectGroups"],[[DiagnosticTool sharedManager]generateErrorCode:@"No Groups found"]]  delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    
                    [alert show];
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                }
                else
                {
                    if ([asGroup isEqualToString:@"ASGroup"])
                    {
                        //all_Lbl.hidden=YES;
                        //allButton.hidden=YES;
                        //check_btn.hidden=YES;
                        //[chkmanuaaly setEnabled:NO];
                    }
                    else
                    {
                        all_Lbl.hidden=NO;
                        allButton.hidden=NO;
                        check_btn.hidden=NO;
                        [chkmanuaaly setEnabled:YES];
                    }
                    
                    
                    
                    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"store_name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                    storesArray=[sortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
                    
                    NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
                    if ([fav isEqualToString:@"fav"])
                    {
                        NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
                        if ([asGroup isEqualToString:@"ASGroup"])
                        {
                            for (int i=0; i<[storesArray count]; i++)
                            {
                                BOOL found = NO;
                                for (int j=0; j<[groupSelectArray count]; j++)
                                {
                                    if ([[[sortedArray objectAtIndex:i] objectForKey:@"id"] integerValue] ==[[groupSelectArray objectAtIndex:j] integerValue] )
                                    {
                                        found=YES;
                                        break;
                                    }
                                    
                                }
                                if (found)
                                {
                                    [checkedArray addObject:@"1"];
                                    [selectedArray addObject:[[sortedArray objectAtIndex:i] objectForKey:@"id"]];
                                }
                                else
                                {
                                    [checkedArray addObject:@"0"];
                                }
                            }
                        }
                        else{
                            for (int i=0; i<[storesArray count]; i++)
                            {
                                BOOL found = NO;
                                for (int j=0; j<[groupSelectArray count]; j++)
                                {
                                    if ([[[sortedArray objectAtIndex:i] objectForKey:@"name"] isEqualToString:[groupSelectArray objectAtIndex:j]] )
                                    {
                                        found=YES;
                                        break;
                                    }
                                    
                                }
                                if (found)
                                {
                                    [checkedArray addObject:@"1"];
                                    [selectedArray addObject:[[sortedArray objectAtIndex:i] objectForKey:@"name"]];
                                }
                                else
                                {
                                    [checkedArray addObject:@"0"];
                                }
                            }
                        }
                        if (selectedArray.count==storesArray.count)
                        {
                            
                            [allButton setImage:[UIImage imageNamed:@"checkboxChecked.png"] forState:UIControlStateNormal];
                            buttonSelected=YES;
                            
                        }
                    }
                    else
                    {
                        for (int i=0; i<[storesArray count]; i++)
                        {
                            
                            [checkedArray addObject:@"0"];
                        }
                    }
                    
                    /*
                     for (int i=0; i<[storesArray count]; i++)
                     {
                     
                     [checkedArray addObject:@"0"];
                     }
                     */
                    [groupTable reloadData];
                    
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                }
                }
            }];
        }];
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    
    [searchBar becomeFirstResponder];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    
   self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
    //selectedGroup
    groupSelectArray=[[NSMutableArray alloc] init];
    [groupSelectArray addObjectsFromArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedGroup"]];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedGroup"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [super viewWillAppear:animated];
}


-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ( [fromPush1 isEqualToString:@"YES"])
    {
        
        NSArray *viewcontrollers=[self.navigationController viewControllers];
        for (int i=0;i<[viewcontrollers count];i++)
        {
            UIViewController *vc=[viewcontrollers objectAtIndex:i];
            NSString *currentView=NSStringFromClass([vc class]);
            if ([currentView isEqualToString:@"VPViewController"])
            {
                [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
            }
        }
        
    }
    
    
}

-(void)nextview
{
    
    // NSArray *selectedCells = [groupTable indexPathsForSelectedRows];
    if ([checkedArray containsObject:@"1"])
    {
        if (selectedArray.count>0 || buttonSelected==YES )
        {
            
            [[NSUserDefaults standardUserDefaults] setObject:selectedArray forKey:@"selectedGroup"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
            
            NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
            if ([asGroup isEqualToString:@"ASGroup"])
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
                
                [[NSUserDefaults standardUserDefaults] setObject:selectedArray forKey:@"selectedGroup"];
                [[NSUserDefaults standardUserDefaults] setObject:selectedstoresname forKey:@"selectedGroupName"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self performSegueWithIdentifier:@"sendmessage" sender:self];
            }
            else
            {
                if ([sl isEqualToString:@"SL"])
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
                    [self performSegueWithIdentifier:@"sendmessage" sender:self];
                }
                else if ([sl isEqualToString:@"OneStore"])
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
                    [self performSegueWithIdentifier:@"Nav" sender:self];
                    //Nav
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"SelectStoresGroup" forKey:@"Select"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
                    [self performSegueWithIdentifier:@"selectstore" sender:self];
                    //[self configAPICall];
                }
            }
        }
        else
        {
            
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Please select groups from the list"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectGroupsVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectGroups"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please select groups from the list"]]  delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Please select groups from the list"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectGroupsVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectGroups"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please select groups from the list"]]  delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(IBAction)allSelected:(id)sender
{
    [favoritsButton setEnabled:YES];
    if (buttonSelected==NO)
    {
        buttonSelected=YES;
        [checkedArray removeAllObjects];
        [selectedArray removeAllObjects];
        [selectedstoresname removeAllObjects];
        
        NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
        if ([asGroup isEqualToString:@"ASGroup"])
        {
            for (int i=0; i<[storesArray count]; i++)
            {
                [selectedArray addObject:[[storesArray objectAtIndex:i] objectForKey:@"id"]];
                [selectedstoresname addObject:[[storesArray objectAtIndex:i] objectForKey:@"name"]];
                [checkedArray addObject:@"1"];
            }
           [allButton setImage:[UIImage imageNamed:@"checkboxChecked"] forState:UIControlStateNormal];
            [groupTable reloadData];
        }
        else
        {
        for (int i=0; i<[storesArray count]; i++)
        {
            [selectedArray addObject:[[storesArray objectAtIndex:i] objectForKey:@"name"]];
            [checkedArray addObject:@"1"];
        }
        [allButton setImage:[UIImage imageNamed:@"checkboxChecked"] forState:UIControlStateNormal];
        [groupTable reloadData];
        }
    }
    else
    {
        [selectedArray removeAllObjects];
        [checkedArray removeAllObjects];
        [selectedstoresname removeAllObjects];
        
        for (int i=0; i<[storesArray count]; i++)
        {
            [checkedArray addObject:@"0"];
        }
        buttonSelected=NO;
        [allButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        [groupTable reloadData];
        
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count;
    if (tableView==asTable)
    {
        count=[asNamesArray count];
    }
    else
    {
        if (tableView != groupTable)
        {
            count= [searchResultsAll count];
        }
        else
        {
            count=[storesArray count];
        }
    }
    return count;
    
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger height;
    if (tableView==asTable)
    {
        height=50;
    }
    else
    {
        height=87;
    }
    return height;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"ListCell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListCell" owner:self options:nil];
        if ([asGroup isEqualToString:@"ASGroup"])
        {
            cell = [nib objectAtIndex:4];
        }
        else
        {
            cell = [nib objectAtIndex:1];
        }
        
    }
    cell.btn_Info.layer.masksToBounds = YES;
    cell.btn_Info.layer.cornerRadius = 5.0;
    if (tableView==asTable)
    {
        cell.favorite_Name.hidden=YES;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.textLabel.text = [asNamesArray objectAtIndex:indexPath.row];
        cell.textLabel.textAlignment=NSTextAlignmentCenter;
        cell.lbl_AsMemberName.hidden=YES;
        cell.btn_AsCheck.hidden=YES;
        cell.btn_AsInfo.hidden=YES;
        /*
         myLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, asTable.frame.size.width-40, 45)];
         //Add a tag to it in order to find it later
         //Align it
         myLabel.layer.masksToBounds = YES;
         myLabel.layer.cornerRadius = 5.0;
         myLabel.backgroundColor = [UIColor grayColor];
         myLabel.textColor = [UIColor whiteColor];
         myLabel.textAlignment= NSTextAlignmentCenter;
         myLabel.text =[asNamesArray objectAtIndex:indexPath.row];
         [cell.contentView addSubview:myLabel];
         */
    }
    else
    {
        
        if ([asGroup isEqualToString:@"ASGroup"])
        {
            
        }
        else
        {
            /*
            if (buttonSelected)
            {
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkboxChecked"]];
            }
            else
            {
                if([[checkedArray objectAtIndex:indexPath.row] isEqualToString:@"0"])
                {
                    
                    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox"]];
                    
                }
                else
                {
                    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkboxChecked"]];
                    
                    
                }
                
            }
             */
        }
        
        if ([asGroup isEqualToString:@"ASGroup"])
        {
            cell.lbl_AsMemberName.layer.masksToBounds = YES;
            cell.lbl_AsMemberName.layer.cornerRadius = 5.0;
            NSMutableArray *asMem;//=[[NSMutableArray alloc] init];
            asMem = [[storesArray objectAtIndex:indexPath.row] objectForKey:@"members"];
            if (asMem.count>0)
            {
                cell.btn_AsInfo.tag=indexPath.row;
                [cell.btn_AsInfo addTarget:self action:@selector(infoAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            else
            {
                cell.btn_Info.hidden=YES;
                cell.btn_Select.hidden=YES;
            }
            
            cell.btn_Select.hidden=NO;
            [cell.btn_AsCheck addTarget:self action:@selector(selectGroup:) forControlEvents:UIControlEventTouchUpInside];
            cell.btn_AsCheck.tag=indexPath.row;
            
            if (buttonSelected)
            {
                [cell.btn_AsCheck setImage:[UIImage imageNamed:@"checkboxChecked"]
                               forState:UIControlStateNormal];
                
                
            }
            else
            {
                if([[checkedArray objectAtIndex:indexPath.row] isEqualToString:@"0"])
                {
                    [cell.btn_AsCheck setImage:[UIImage imageNamed:@"checkbox"]
                                   forState:UIControlStateNormal];
                    
                    
                }
                else
                {
                    [cell.btn_AsCheck setImage:[UIImage imageNamed:@"checkboxChecked"]
                                   forState:UIControlStateNormal];
                    
                    
                }
                
            }

            
        }
        else
        {
            //        cell.btn_Info.hidden=NO;
            //        [cell.btn_Info addTarget:self action:@selector(infoAction:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btn_Info.hidden=NO;
            cell.btn_Select.hidden=NO;
            [cell.btn_Select addTarget:self action:@selector(selectGroup:) forControlEvents:UIControlEventTouchUpInside];
            cell.btn_Select.tag=indexPath.row;
            
            if (buttonSelected)
            {
                [cell.btn_Info setImage:[UIImage imageNamed:@"checkboxChecked"]
                               forState:UIControlStateNormal];
                
                
            }
            else
            {
                if([[checkedArray objectAtIndex:indexPath.row] isEqualToString:@"0"])
                {
                    [cell.btn_Info setImage:[UIImage imageNamed:@"checkbox"]
                                   forState:UIControlStateNormal];
                    
                    
                }
                else
                {
                    [cell.btn_Info setImage:[UIImage imageNamed:@"checkboxChecked"]
                                   forState:UIControlStateNormal];
                    
                    
                }
                
            }
            
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.favorite_Name.layer.masksToBounds = YES;
        cell.favorite_Name.layer.cornerRadius = 5.0;
        
        if (tableView != groupTable)
        {
            
            cell.textLabel.text = [[searchResultsAll objectAtIndex:indexPath.row]objectForKey:@"name" ];
            cell.favorite_Name.hidden=YES;
            cell.accessoryView.hidden=YES;
            cell.btn_Info.hidden=YES;
            cell.lbl_AsMemberName.hidden=YES;
            cell.btn_AsCheck.hidden=YES;
            cell.btn_AsInfo.hidden=YES;
        }
        else
        {
            cell.favorite_Name.hidden=NO;
            cell.accessoryView.hidden=NO;
            if ([asGroup isEqualToString:@"ASGroup"])
            {
                cell.lbl_AsMemberName.marqueeType = MLContinuous;
                cell.lbl_AsMemberName.scrollDuration = 8.0;
                cell.lbl_AsMemberName.animationCurve = UIViewAnimationOptionCurveEaseInOut;
                cell.lbl_AsMemberName.fadeLength = 0.0f;
                cell.lbl_AsMemberName.leadingBuffer = 0.0f;
                cell.lbl_AsMemberName.trailingBuffer = 10.0f;
                cell.lbl_AsMemberName.text =[[storesArray objectAtIndex:indexPath.row] objectForKey:@"name"];
            }else
            {
                
                cell.favorite_Name.marqueeType = MLContinuous;
                cell.favorite_Name.scrollDuration = 8.0;
                cell.favorite_Name.animationCurve = UIViewAnimationOptionCurveEaseInOut;
                cell.favorite_Name.fadeLength = 0.0f;
                cell.favorite_Name.leadingBuffer = 0.0f;
                cell.favorite_Name.trailingBuffer = 10.0f;
              cell.favorite_Name.text =[[storesArray objectAtIndex:indexPath.row] objectForKey:@"name"];
            }
            
        }
    }
    return cell;
    
}

-(void)selectGroup:(UIButton *)button
{
    [favoritsButton setEnabled:YES];
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:button.tag inSection:0] ;
    cell = [groupTable cellForRowAtIndexPath:myIP];
    
     NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
    if ([asGroup isEqualToString:@"ASGroup"])
    {
        if ([[checkedArray objectAtIndex:myIP.row] isEqualToString:@"1"])
        {
            [checkedArray replaceObjectAtIndex:myIP.row withObject:@"0"];
            [selectedArray removeObject:[[storesArray objectAtIndex:myIP.row] objectForKey:@"id"]];
            [selectedstoresname removeObject:[[storesArray objectAtIndex:button.tag] objectForKey:@"name"]];
            [cell.btn_AsCheck setImage:[UIImage imageNamed:@"checkbox"]
                           forState:UIControlStateNormal];
        }
        else
        {
            [checkedArray replaceObjectAtIndex:myIP.row withObject:@"1"];
            [selectedArray addObject:[[storesArray objectAtIndex:myIP.row] objectForKey:@"id"]];
            [selectedstoresname addObject:[[storesArray objectAtIndex:button.tag] objectForKey:@"name"]];
            [cell.btn_AsCheck setImage:[UIImage imageNamed:@"checkboxChecked"]
                           forState:UIControlStateNormal];
        }
        
        if (selectedArray.count==storesArray.count)
        {
            buttonSelected=YES;
            [allButton setImage:[UIImage imageNamed:@"checkboxChecked"] forState:UIControlStateNormal];
        }
        else
        {
            buttonSelected=NO;
            [allButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
    }
    else
    {
    if ([[checkedArray objectAtIndex:myIP.row] isEqualToString:@"1"])
    {
        [checkedArray replaceObjectAtIndex:myIP.row withObject:@"0"];
        [selectedArray removeObject:[[storesArray objectAtIndex:myIP.row] objectForKey:@"name"]];
        [cell.btn_Info setImage:[UIImage imageNamed:@"checkbox"]
                       forState:UIControlStateNormal];
    }
    else
    {
        [checkedArray replaceObjectAtIndex:myIP.row withObject:@"1"];
        [selectedArray addObject:[[storesArray objectAtIndex:myIP.row] objectForKey:@"name"]];
        [cell.btn_Info setImage:[UIImage imageNamed:@"checkboxChecked"]
                       forState:UIControlStateNormal];
    }
    
    if (selectedArray.count==storesArray.count)
    {
        buttonSelected=YES;
        [allButton setImage:[UIImage imageNamed:@"checkboxChecked"] forState:UIControlStateNormal];
    }
    else
    {
        buttonSelected=NO;
        [allButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
    }
    }
    
}

-(IBAction)cancelAction:(id)sender
{
    as_view.hidden=YES;
    asTable.hidden=YES;
}

-(void)infoAction:(UIButton *)sender
{
    asNamesArray=[[storesArray objectAtIndex:sender.tag] objectForKey:@"members"];
    if (asNamesArray.count>0)
    {
        NSInteger height;
        
        height = 40+50*[asNamesArray count];
        
        if (height> self.view.frame.size.height)
        {
            height =self.view.frame.size.height-120;
        }
        asTable.frame= CGRectMake(asTable.frame.origin.x, asTable.frame.origin.y, asTable.frame.size.width, height);
        as_view.hidden=NO;
        asTable.hidden=NO;
        [asTable reloadData];
    }
    else
    {
        [AlertView alert:kInternetConnectiontitle message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Please try again"],[[DiagnosticTool sharedManager] generateScreenCode:@"SelectGroupsVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectGroups"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please try again"]]];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==asTable)
    {
        return 40;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    if (tableView==asTable)
    {
        
        /* Create custom view to display section header... */
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 25)];
        [label setFont:[UIFont boldSystemFontOfSize:18]];
        label.textAlignment=NSTextAlignmentCenter;
        /* Section header is in 0th index... */
        [label setText:[dictLang objectForKey:@"Group Members"]];
        label.textColor=[UIColor whiteColor];
        [view addSubview:label];
        [view setBackgroundColor:kNavBackgroundColor]; //your background color...
        
    }
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==asTable)
    {
        
    }
    else
    {
        if (tableView != groupTable)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
            NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
            if ([asGroup isEqualToString:@"ASGroup"])
            {
                [selectedArray removeAllObjects];
                
                NSString *groupstr=[[searchResultsAll objectAtIndex:indexPath.row] objectForKey:@"id"];
                
                [selectedArray addObject:groupstr];
                
                [[NSUserDefaults standardUserDefaults] setObject:selectedArray forKey:@"selectedGroup"];
                [[NSUserDefaults standardUserDefaults] setObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"name"] forKey:@"selectedGroupName"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self performSegueWithIdentifier:@"sendmessage" sender:self];
                
            }
            else
            {
                [selectedArray removeAllObjects];
                [selectedArray addObject:[[searchResultsAll objectAtIndex:indexPath.row] objectForKey:@"name"]];
                [[NSUserDefaults standardUserDefaults] setObject:selectedArray forKey:@"selectedGroup"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                 NSString *MS =[[NSUserDefaults standardUserDefaults] objectForKey:@"MultiStore"];
                if ([MS isEqualToString:@"MS"])
                {
                    [self performSegueWithIdentifier:@"selectstore" sender:self];
                }
                else
                {
                NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
                if ([sl isEqualToString:@"SL"])
                {
                    [self performSegueWithIdentifier:@"sendmessage" sender:self];
                }
                else
                {
                    
                        [self performSegueWithIdentifier:@"Nav" sender:self];
                        //Nav
                    //[self configAPICall];
                }
                }
            }
            
            
        }
        else
        {
            /*
            NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
            if ([asGroup isEqualToString:@"ASGroup"])
            {
                [selectedArray removeAllObjects];
                
                NSString *groupstr=[[storesArray objectAtIndex:indexPath.row] objectForKey:@"id"];
                [selectedArray addObject:groupstr];
                [[NSUserDefaults standardUserDefaults] setObject:selectedArray forKey:@"selectedGroup"];
                [[NSUserDefaults standardUserDefaults] setObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"name"] forKey:@"selectedGroupName"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self performSegueWithIdentifier:@"sendmessage" sender:self];
            }
            else
            {
                //
                if (buttonSelected==NO)
                {
                    if (!([selectedArray count]==[storesArray count]))
                    {
                        if (![selectedArray containsObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"name"]])
                        {
                            [selectedArray addObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
                            [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
                            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkboxChecked"]];
                        }
                        else
                        {
                            [selectedArray removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
                            [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
                            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox"]];
                        }
                    }
                    else
                    {
                        [selectedArray removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
                        [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
                        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox"]];
                        
                    }
                    
                }
                else
                {
                    buttonSelected=NO;
                    [selectedArray removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
                    [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
                    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox"]];
                    
                }
                if (selectedArray.count==storesArray.count)
                {
                    buttonSelected=YES;
                    [allButton setImage:[UIImage imageNamed:@"checkboxChecked"] forState:UIControlStateNormal];
                }
                else
                {
                    [allButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
                }
                
                
            }*/
            
            
        }
        
    }
    
}


-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==asTable)
    {
        
    }
    else
    {
        NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
        if ([asGroup isEqualToString:@"ASGroup"])
        {
            
        }
        /*else
        {
            if (buttonSelected==NO)
            {
                if (!([selectedArray count]==[storesArray count]))
                {
                    [selectedArray removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
                    [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
                    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox"]];
                }
                else
                {
                    [selectedArray addObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
                    [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
                    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkboxChecked"]];
                }
                
            }
            else
            {
                if (selectedArray.count==storesArray.count)
                {
                    [selectedArray removeObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
                    [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
                    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox"]];
                    
                }
                else
                {
                    [selectedArray addObject:[[storesArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
                    [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
                    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkboxChecked"]];
                    
                }
            }
            
            if (selectedArray.count==storesArray.count)
            {
                buttonSelected=YES;
                [allButton setImage:[UIImage imageNamed:@"checkboxChecked"] forState:UIControlStateNormal];
            }
            else
            {
                buttonSelected=NO;
                [allButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
            }
        }*/
        
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"selectstore"])
    {
        [segue destinationViewController];  //---->SelectStoresViewController
    }
    if ([[segue identifier] isEqualToString:@"sendmessage"]) {
        
        [segue destinationViewController];  //---->MutilMessageViewController
        
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [super viewWillDisappear:animated];
}

#pragma mark - UISearchDisplayController delegate methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    searchResultsAll=[[NSMutableArray alloc]init];
    
    for (int i =0;i<[storesArray count];i++)
        
    {
        
        NSString *city = [[storesArray objectAtIndex:i] objectForKey:@"name"];
        NSRange range=[city rangeOfString:searchText options:NSCaseInsensitiveSearch];
        if (range.length >0)
        {
            if (![searchResultsAll containsObject:city]) {
                [searchResultsAll addObject:[storesArray objectAtIndex:i]];
            }
        }
        
    }
    
    
}

-(BOOL)searchDisplayController:(UISearchController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[controller.searchBar scopeButtonTitles]
                                      objectAtIndex:[controller.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end
