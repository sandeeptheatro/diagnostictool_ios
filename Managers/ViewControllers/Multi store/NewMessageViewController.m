//
//  NewMessageViewController.m
//  ManagersMockup
//
//  Created by Ravi on 01/09/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "NewMessageViewController.h"
#import "ListCell.h"
#import "Constant.h"

@interface NewMessageViewController ()
{
    NSMutableArray *listArray,*listImages;
    NSDictionary *dictLang;
}

@end

@implementation NewMessageViewController

- (void)viewDidLoad
{
    dictLang=[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kNavBackgroundColor,
       NSFontAttributeName:[UIFont fontWithName:kFontName size:13]}];
    self.navigationItem.title = [[NSUserDefaults standardUserDefaults] objectForKey:@"new_Tcm"];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
    
    
    UIBarButtonItem *favoritsButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"favorite"] style:UIBarButtonItemStyleDone target:self action:@selector(favoritsButtonAction)];
    self.navigationItem.rightBarButtonItem=favoritsButton;
    if ([fav isEqualToString:@"fav"])
    {
        [favoritsButton setEnabled:NO];
    }
    
    [super viewDidLoad];
    listArray = [[NSMutableArray alloc] init];
    
    listArray=[NSMutableArray arrayWithObjects:@"Announcement",@"Message",nil];
    
    
    listImages =[[NSMutableArray alloc]init];
    [listImages  addObject:[UIImage imageNamed:@"megaphone"]];
    [listImages  addObject:[UIImage imageNamed:@"speaking"]];
    
    // Do any additional setup after loading the view.
}

-(void)favoritsButtonAction
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Please enter the favorite name"] message:nil delegate:self cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
    alert.alertViewStyle=UIAlertViewStylePlainTextInput;
    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeDefault];
    
    //[[alert textFieldAtIndex:1] becomeFirstResponder];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==108)
    {
        [self favoritsButtonAction];
    }
    else
    {
        if (buttonIndex==1)
        {
            UITextField *group = [actionSheet textFieldAtIndex:0];
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
                        
                group.text= [group.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                if (group.text.length>0 )
                {
                    
                    NSMutableArray *newArray; //= [[NSMutableArray alloc] init];
                    NSMutableArray *nameArray = [[NSMutableArray alloc]init];
                    newArray= [[EarboxDataStore sharedStore] favoritesArtists];
                    
                    for (int i=0; i<[newArray count]; i++)
                    {
                        Favorites *fav;
                        fav=[newArray objectAtIndex:i];
                        
                        [nameArray addObject:fav.favouriteName];
                    }
                    
                    if ([nameArray containsObject:group.text])
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@ %@",[dictLang objectForKey:@"This"],[[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"]capitalizedString],[dictLang objectForKey:@"name already exists"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                        alert.tag=108;
                        [alert show];
                    }
                    else
                    {
                    NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
                    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
                    [postData setObject:@"" forKey:@"PlayAudioType"];
                    [postData setObject:@"NewMessage" forKey:@"FinalView"];
                    [postData setObject:@[] forKey:@"Groups"];
                    [postData setObject:@[] forKey:@"Stores"];
                    //[postData setObject:@"" forKey:@"ChannelSected"];
                    [postData setObject:group.text forKey:@"FavouriteName"];
                    [postData setObject:sl forKey:@"MessageType"];
                    //[postData setObject:@"" forKey:@"Sender"];//featureType
                    [postData setObject:@"" forKey:@"FeatureType"];
                    [postData setObject:@"MS-MA" forKey:@"Originator"];
                    [postData setObject:@"iOS" forKey:@"PlatForm"];
                   // int rangeLow = 100;
                    //int rangeHigh = 100060;
                    //int randomNumber = arc4random() % (rangeHigh-rangeLow+1) + rangeLow;
                    [postData setObject:@"NewMessage" forKey:@"FavouriteType"];
                    [postData setObject:@"" forKey:@"NodeId"];
                    [postData setObject:@"" forKey:@"ParentId"];
                    [postData setObject:@"" forKey:@"LevelId"];
                    [postData setObject:@"false" forKey:@"StoreScreen"];
                    [postData setObject:@[] forKey:@"StoreNames"];
                        
                        
                    [postData setObject:@"" forKey:@"MultiStore"];
                    [postData setObject:@"" forKey:@"HQTagOutName"];
                    [postData setObject:@[] forKey:@"HQManagerAppID"];
                    [postData setObject:@[] forKey:@"Nodes"];
                    [postData setObject:@[] forKey:@"MultiStoreNodeId"];
                    [postData setObject:@[] forKey:@"MultiStoreNodesNames"];
                        [postData setObject:@"" forKey:@"levelName"];
                  
                    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
                    
                    
                    
                    NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
                    [myQueueSales addOperationWithBlock:^{
                        NSDictionary *responsDic=[HttpHandler favSave:[loginDetails objectForKey:@"twUserName"] password:[loginDetails objectForKey:@"twPassword"] jSonStr:postData userScreen:@"NewMessageVC"];
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            if ([responsDic objectForKey:@"Error"])
                            {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[responsDic objectForKey:@"Error"] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                                
                                [alert show];
                            }
                            else if ([responsDic objectForKey:@"FavouriteID"])
                            {
                            [postData setObject:[responsDic objectForKey:@"FavouriteID"] forKey:@"FavouriteID"];
                            [array addObject:postData];
                            [[Earboxinboxmodel sharedModel]favoritesInsert:array];
                            }
                            
                        }];
                    }];
                    }
                    
                }
                else
                {
                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"],[dictLang objectForKey:@"name cannot be empty"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    alert.tag=108;
                    [alert show];
                }
                
                
            
            
            
            
        }
    }
}


-(void)viewWillAppear:(BOOL)animated
{
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"nothing" forKey:@"Select"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedGroup"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedstores"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ASGroup"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [super viewWillAppear:animated];
}

-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ( [fromPush1 isEqualToString:@"YES"])
    {
        
        NSArray *viewcontrollers=[self.navigationController viewControllers];
        for (int i=0;i<[viewcontrollers count];i++)
        {
            UIViewController *vc=[viewcontrollers objectAtIndex:i];
            NSString *currentView=NSStringFromClass([vc class]);
            if ([currentView isEqualToString:@"VPViewController"])
            {
                [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
            }
        }
        
    }
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [super viewWillDisappear:animated];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [listArray count];
    
    
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 87;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"ListCell";
    ListCell*cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListCell" owner:self options:nil];
        cell = [nib objectAtIndex:1];
        
        
    }
    cell.btn_Info.hidden=YES;
    cell.favorite_Name.layer.masksToBounds = YES;
    cell.favorite_Name.layer.cornerRadius = 5.0;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.favorite_Name.text =[listArray objectAtIndex:indexPath.row];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
    
    if (indexPath.row==0)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"Announcement" forKey:@"MessageFrom"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"Message" forKey:@"MessageFrom"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [self performSegueWithIdentifier:@"Dist" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    [segue destinationViewController]; //------>DistrictViewController
}


@end
