//
//  NavViewController.h
//  MultiStoreNavigation
//
//  Created by sandeepchalla on 11/3/16.
//  Copyright © 2016 sandeepchalla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Earboxinboxmodel.h"
#import "Favorites+CoreDataProperties.h"
#import "EarboxDataStore.h"

@interface NavViewController : UIViewController<UISearchBarDelegate>
{
    BOOL clickedOnChildNode;
    NSString *hierarchyLevel;
    NSMutableArray *favArray;
    NSString *storeScreen;
    UIBarButtonItem *favoritsButton;
    IBOutlet UILabel *lbl_Green;
}
@end
