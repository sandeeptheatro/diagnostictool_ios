//
//  HierarchyViewController.h
//  ManagersMockup
//
//  Created by Ravi on 14/06/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HierarchyViewController : UIViewController
{
    IBOutlet UITableView *tbl_Hierarchy;
    NSString *levelName;
    UIBarButtonItem *favoritsButton;
    //IBOutlet UILabel *lbl_Green;
    IBOutlet UILabel *lbl_Green;
}

@end
