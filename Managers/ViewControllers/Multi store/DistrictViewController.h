//
//  DistrictViewController.h
//  ManagersMockup
//
//  Created by Ravi on 31/08/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Earboxinboxmodel.h"
#import "Favorites+CoreDataProperties.h"
#import "HttpHandler.h"
#import "EarboxDataStore.h"
@interface DistrictViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *districtTable;
    IBOutlet UILabel *lbl_Green;
}

@end
