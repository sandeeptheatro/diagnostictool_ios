//
//  InboxViewController.m
//  Managers
//
//  Created by Ravi on 05/10/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "InboxViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIViewController.h"
#import "Constant.h"
#import "AudioManager.h"
#import "TableViewCell.h"
#import "EarboxDataStore.h"
#import "Earboxinboxmodel.h"
#import "HttpHandler.h"
#import "AlertView.h"
#import "Reachability.h"
#import "NSDateClass.h"
#import "ActivityIndicatorView.h"
#import "ReplayView.h"
#import "MarqueeLabel.h"
#import "DiagnosticTool.h"

/*
 
 UI
 
 self.tableView.tag=1 --> privateMessage inbox
 self.tableView.tag=2 --> privateMessage saved
 self.tableView.tag=3 --> privateMessage sent
 self.tableView.tag=4 --> groupMessage inbox
 self.tableView.tag=5 --> groupMessage saved
 self.tableView.tag=6 --> groupMessage sent
 self.tableView.tag=10 --> everyoneMessage inbox
 self.tableView.tag=11 --> everyoneMessage saved
 self.tableView.tag=12 --> everyoneMessage sent
 self.tableView.tag=7 --> storeAnnouncement inbox
 self.tableView.tag=8 --> storeAnnouncement saved
 self.tableView.tag=9 --> storeAnnouncement sent
 
 callTpes:
 
 4    --> message,reply of groupmessage,reply of everyonemessage
 17   --> groupmessage
 10   --> announcement_huddle
 16   --> announcement_sales
 21   --> announcement_now
 22   --> announcement_hrs
 23   --> announcement_today
 25   --> announcement_week
 33   --> everyonemessage
 108  --> ASgroupmessage
 104  --> reply of groupmessage and ASgroupmessage
 102  --> ASgroupmessage one to one
 110  --> reply of ASgroupmessage one to one
 
 Database
 
 EarboxInbox  --> message,groupmessage,everyonemessage
 EarboxSaved  --> message,groupmessage,everyonemessage
 EarboxSent  --> message,groupmessage,everyonemessage
 AnnouncementInbox,AnnouncementSaved,AnnouncementSent
 
 */


@interface InboxViewController ()<UISearchBarDelegate,UIGestureRecognizerDelegate>
{
    NSMutableArray *inboxArray,*savedArray,*sentArray,*freshInboxList,*freshSavedList,*freshReplyList,*searchResultsAll;
    TableViewCell *cell;
    BOOL replyClicked,isRowClosed;
    UIButton *imagebutton,*cancelButton;
    NSString *storeNameString,*contactNameString,*searchString,*inboxSearchText,*inboxsortText;
    NSString *audioString,*audiologString,*audiolength;
    NSIndexPath *indexpathcell,*expandedIndexPath;
    NSInteger deletebuttonindex,rowindex;
    NSDictionary *loginDetails,*apploginDetails,*groupDict;
    UIBarButtonItem *searchbutton,*searchsepcbutton,*speakerButton;
    EarboxInbox *messageinboxdata, *messageFreshList;
    EarboxSaved *messagesaveddata, *messageFreshSavedData;
    EarboxSent *messagesentdata;
    AnnouncementInbox *announcementinboxdata;
    AnnouncementSaved *announcementsaveddata;
    AnnouncementSent *announcementsentdata;
    int storeidString,companyId,searchButtonalertTableHeight;
    EarboxDataStore *store;
    Earboxinboxmodel *earboxinboxmodelStore;
    UITableView *nameButtonalertTable,*searchButtonalertTable;
    UIView *backendView;
    ReplayView *rootView;
    UISearchBar *searchBar;
    NSArray *alertArray;
    BOOL inboxClicked,savedClicked,sentClicked,privateMessageClicked,everyoneMessageClicked,groupMessageClicked,storeAnnouncementClicked,searchTag,searchsepctag,tablecellclose,searchbackendView,replayCancelEnable,pushnotificationRecieved;
    NSDictionary *dictLang;
    NSString *selectedCallId;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSMutableDictionary *pnsdic;

@end

@implementation InboxViewController
@synthesize inboxpreviousTab;

#pragma mark - privateMessageButtonAction method
-(void)privateMessageButtonAction:(UIButton *)sender
{
    expandedIndexPath=nil;
    [self creationUIbuttons_Private_everyone_announcement_group];
    sender.backgroundColor=kmessageselectedtabcolor;
    sender.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
    // Setup the string
    NSMutableAttributedString *privateMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Private"],[dictLang objectForKey:@"Messages"]]];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
    {
        // Set the font to bold from the beginning of the string to the ","
        [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 7)];
        
        // Normal font for the rest of the text
        [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(7, 9)];
    }
    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
    {
        // Set the font to bold from the beginning of the string to the ","
        [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
        
        // Normal font for the rest of the text
        [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(5, 9)];
    }
    
    // Set the attributed string as the buttons' title text
    [sender setAttributedTitle:privateMessageText forState:UIControlStateNormal];
    privateMessageClicked=YES;
    everyoneMessageClicked=NO;
    groupMessageClicked=NO;
    storeAnnouncementClicked=NO;
    if (inboxClicked)
    {
        self.tableView.tag=1;
        inboxButton.backgroundColor=kinboxunselectedtabcolor;
        inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:1 bottom:1 tabletag:1];
    }
    if (savedClicked)
    {
        self.tableView.tag=2;
        savedButton.backgroundColor=kinboxunselectedtabcolor;
        savedButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:1 bottom:2 tabletag:2];
    }
    if (sentClicked)
    {
        self.tableView.tag=3;
        sentButton.backgroundColor=kinboxunselectedtabcolor;
        sentButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:1 bottom:3 tabletag:3];
    }
    [self searchBarCancelButtonClicked:searchBar];
    searchTag=NO;
    [self audioPlayerStop];
    
    [self tableViewUpdate];
    
}

#pragma mark - everyoneMessageButtonAction method
-(void)everyoneMessageButtonAction:(UIButton *)sender
{
    expandedIndexPath=nil;
    [self creationUIbuttons_Private_everyone_announcement_group];
    sender.backgroundColor=kmessageselectedtabcolor;
    sender.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
    // Setup the string
    NSMutableAttributedString *everyoneMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Everyone"],[dictLang objectForKey:@"Messages"]]];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
    {
        // Set the font to bold from the beginning of the string to the ","
        [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 8)];
        // Normal font for the rest of the text
        [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(8, 9)];
        
    }
    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
    {
        // Set the font to bold from the beginning of the string to the ","
        [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 6)];
        // Normal font for the rest of the text
        [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(6, 9)];
    }
    
    // Set the attributed string as the buttons' title text
    [sender setAttributedTitle:everyoneMessageText forState:UIControlStateNormal];
    privateMessageClicked=NO;
    everyoneMessageClicked=YES;
    groupMessageClicked=NO;
    storeAnnouncementClicked=NO;
    if (inboxClicked)
    {
        self.tableView.tag=10;
        inboxButton.backgroundColor=kinboxunselectedtabcolor;
        inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:2 bottom:1 tabletag:10];
    }
    if (savedClicked)
    {
        self.tableView.tag=11;
        savedButton.backgroundColor=kinboxunselectedtabcolor;
        savedButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:2 bottom:2 tabletag:11];
    }
    if (sentClicked)
    {
        self.tableView.tag=12;
        sentButton.backgroundColor=kinboxunselectedtabcolor;
        sentButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:2 bottom:3 tabletag:12];
    }
    [self searchBarCancelButtonClicked:searchBar];
    searchTag=NO;
    [self audioPlayerStop];
    
    [self tableViewUpdate];
}

#pragma mark - groupMessageButtonAction method
-(void)groupMessageButtonAction:(UIButton *)sender
{
    expandedIndexPath=nil;
    [self creationUIbuttons_Private_everyone_announcement_group];
    sender.backgroundColor=kmessageselectedtabcolor;
    sender.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
    // Setup the string
    NSMutableAttributedString *groupMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Group"],[dictLang objectForKey:@"Messages"]]];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
    {
        // Set the font to bold from the beginning of the string to the ","
        [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
        
        // Normal font for the rest of the text
        [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(5, 9)];
    }
    else  if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
    {
        // Set the font to bold from the beginning of the string to the ","
        [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 6)];
        
        // Normal font for the rest of the text
        [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(6, 9)];
    }
    
    // Set the attributed string as the buttons' title text
    [sender setAttributedTitle:groupMessageText forState:UIControlStateNormal];
    privateMessageClicked=NO;
    everyoneMessageClicked=NO;
    groupMessageClicked=YES;
    storeAnnouncementClicked=NO;
    if (inboxClicked)
    {
        self.tableView.tag=4;
        inboxButton.backgroundColor=kinboxunselectedtabcolor;
        inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:3 bottom:1 tabletag:4];
    }
    if (savedClicked)
    {
        self.tableView.tag=5;
        savedButton.backgroundColor=kinboxunselectedtabcolor;
        savedButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:3 bottom:2 tabletag:5];
    }
    if (sentClicked)
    {
        self.tableView.tag=6;
        sentButton.backgroundColor=kinboxunselectedtabcolor;
        sentButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:3 bottom:3 tabletag:6];
    }
    [self searchBarCancelButtonClicked:searchBar];
    searchTag=NO;
    [self audioPlayerStop];
    
    [self tableViewUpdate];
}

#pragma mark - storeAnnouncementButtonAction method
-(void)storeAnnouncementButtonAction:(UIButton *)sender
{
    expandedIndexPath=nil;
    [self creationUIbuttons_Private_everyone_announcement_group];
    sender.backgroundColor=kmessageselectedtabcolor;
    sender.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
    // Setup the string
    NSMutableAttributedString *storeAnnouncementText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@\n%@",[dictLang objectForKey:@"Active"],[dictLang objectForKey:@"Store"],[dictLang objectForKey:@"Announcements"]]];
    
    // Set the font to bold from the beginning of the string to the ","
    NSDictionary *attrDict = @{
                               NSFontAttributeName : [UIFont systemFontOfSize:9],
                               NSForegroundColorAttributeName : [UIColor redColor]
                               };
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
    {
        [storeAnnouncementText addAttributes:attrDict range:NSMakeRange(0, 6)];
        //NSDictionary *attrs = @{ NSForegroundColorAttributeName : [UIColor redColor] };
        [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:9] forKey:NSFontAttributeName] range:NSMakeRange(0, 6)];
        
        [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(6, 6)];
        
        // Normal font for the rest of the text
        [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(12, 14)];
    }
    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
    {
        [storeAnnouncementText addAttributes:attrDict range:NSMakeRange(0, 5)];
        //NSDictionary *attrs = @{ NSForegroundColorAttributeName : [UIColor redColor] };
        [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:9] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
        
        [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(5, 8)];
        
        // Normal font for the rest of the text
        [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(13, 9)];
    }
    
    // Set the attributed string as the buttons' title text
    [sender setAttributedTitle:storeAnnouncementText forState:UIControlStateNormal];
    privateMessageClicked=NO;
    everyoneMessageClicked=NO;
    groupMessageClicked=NO;
    storeAnnouncementClicked=YES;
    if (inboxClicked)
    {
        self.tableView.tag=7;
        inboxButton.backgroundColor=kinboxunselectedtabcolor;
        inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:4 bottom:1 tabletag:7];
    }
    if (savedClicked)
    {
        self.tableView.tag=8;
        savedButton.backgroundColor=kinboxunselectedtabcolor;
        savedButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:4 bottom:2 tabletag:8];
    }
    if (sentClicked)
    {
        self.tableView.tag=9;
        sentButton.backgroundColor=kinboxunselectedtabcolor;
        sentButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:4 bottom:3 tabletag:9];
    }
    [self searchBarCancelButtonClicked:searchBar];
    searchTag=NO;
    [self audioPlayerStop];
    
    [self tableViewUpdate];
}

#pragma mark - inboxButtonAction method
-(void)inboxButtonAction:(UIButton *)sender
{
    expandedIndexPath=nil;
    [self creationUIbuttons_Private_everyone_announcement_group];
    sender.backgroundColor=kinboxselectedtabcolor;
    sender.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
    savedClicked=NO;
    sentClicked=NO;
    inboxClicked=YES;
    if (privateMessageClicked)
    {
        self.tableView.tag=1;
        privateMessageButton.backgroundColor=kmessageselectedtabcolor;
        privateMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:1 bottom:1 tabletag:1];
    }
    if (everyoneMessageClicked)
    {
        self.tableView.tag=10;
        everyoneMessageButton.backgroundColor=kmessageselectedtabcolor;
        everyoneMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:2 bottom:1 tabletag:10];
    }
    if (groupMessageClicked)
    {
        self.tableView.tag=4;
        groupMessageButton.backgroundColor=kmessageselectedtabcolor;
        groupMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:3 bottom:1 tabletag:4];
    }
    if (storeAnnouncementClicked)
    {
        self.tableView.tag=7;
        storeAnnouncementButton.backgroundColor=kmessageselectedtabcolor;
        storeAnnouncementButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:4 bottom:1 tabletag:7];
    }
    [self searchBarCancelButtonClicked:searchBar];
    searchTag=NO;
    [self audioPlayerStop];
    
    [self tableViewUpdate];
}

#pragma mark - savedButtonAction method
-(void)savedButtonAction:(UIButton *)sender
{
    expandedIndexPath=nil;
    [self creationUIbuttons_Private_everyone_announcement_group];
    sender.backgroundColor=kinboxselectedtabcolor;
    sender.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
    savedClicked=YES;
    sentClicked=NO;
    inboxClicked=NO;
    if (privateMessageClicked)
    {
        self.tableView.tag=2;
        privateMessageButton.backgroundColor=kmessageselectedtabcolor;
        privateMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:1 bottom:2 tabletag:2];
    }
    if (everyoneMessageClicked)
    {
        self.tableView.tag=11;
        everyoneMessageButton.backgroundColor=kmessageselectedtabcolor;
        everyoneMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:2 bottom:2 tabletag:11];
    }
    if (groupMessageClicked)
    {
        self.tableView.tag=5;
        groupMessageButton.backgroundColor=kmessageselectedtabcolor;
        groupMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:3 bottom:2 tabletag:5];
    }
    if (storeAnnouncementClicked)
    {
        self.tableView.tag=8;
        storeAnnouncementButton.backgroundColor=kmessageselectedtabcolor;
        storeAnnouncementButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:4 bottom:2 tabletag:8];
    }
    [self searchBarCancelButtonClicked:searchBar];
    searchTag=NO;
    [self audioPlayerStop];
    
    [self tableViewUpdate];
}

#pragma mark - sentButtonAction method
-(void)sentButtonAction:(UIButton *)sender
{
    expandedIndexPath=nil;
    [self creationUIbuttons_Private_everyone_announcement_group];
    sender.backgroundColor=kinboxselectedtabcolor;
    sender.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
    savedClicked=NO;
    sentClicked=YES;
    inboxClicked=NO;
    if (privateMessageClicked)
    {
        self.tableView.tag=3;
        privateMessageButton.backgroundColor=kmessageselectedtabcolor;
        privateMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:1 bottom:3 tabletag:3];
    }
    if (everyoneMessageClicked)
    {
        self.tableView.tag=12;
        everyoneMessageButton.backgroundColor=kmessageselectedtabcolor;
        everyoneMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:2 bottom:3 tabletag:12];
    }
    if (groupMessageClicked)
    {
        self.tableView.tag=6;
        groupMessageButton.backgroundColor=kmessageselectedtabcolor;
        groupMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:3 bottom:3 tabletag:6];
    }
    if (storeAnnouncementClicked)
    {
        self.tableView.tag=9;
        storeAnnouncementButton.backgroundColor=kmessageselectedtabcolor;
        storeAnnouncementButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:4 bottom:3 tabletag:9];
    }
    [self searchBarCancelButtonClicked:searchBar];
    searchTag=NO;
    [self audioPlayerStop];
    
    [self tableViewUpdate];
}

#pragma mark - inboxpreviousTabCreation method
-(void)inboxpreviousTabCreation:(NSInteger)top bottom:(NSInteger)bottom tabletag:(NSInteger)tabletag
{
    NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:top],@"topsegmentindex",[NSNumber numberWithInteger:bottom],@"bottomsegmentindex",[NSNumber numberWithInteger:tabletag],@"tabletag", nil];
    NSData *announcementEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:dic];
    
    [[NSUserDefaults standardUserDefaults] setObject:announcementEncodedObject forKey:@"inboxpreviousTab"];
}

#pragma mark - creationUI buttons Private/everyone/announcement/group method
-(void)creationUIbuttons_Private_everyone_announcement_group
{
    privateMessageButton.backgroundColor=kmessageunselectedtabcolor;
    privateMessageButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    privateMessageButton.layer.cornerRadius=8;
    privateMessageButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    privateMessageButton.layer.borderWidth=4;
    privateMessageButton.tag=100;
    [privateMessageButton addTarget:self action:@selector(privateMessageButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    privateMessageButton.layer.borderColor=kmessageunselectedtabcolor.CGColor;
    // Setup the string
    NSMutableAttributedString *privateMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Private"],[dictLang objectForKey:@"Messages"]]];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
    {
    // Set the font to bold from the beginning of the string to the ","
    [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 7)];
    
    // Normal font for the rest of the text
    [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(7, 9)];
    }
    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
    {
        // Set the font to bold from the beginning of the string to the ","
        [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
        
        // Normal font for the rest of the text
        [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(5, 9)];
    }
    
    // Set the attributed string as the buttons' title text
    [privateMessageButton setAttributedTitle:privateMessageText forState:UIControlStateNormal];
    
    everyoneMessageButton.backgroundColor=kmessageunselectedtabcolor;
    everyoneMessageButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    everyoneMessageButton.layer.cornerRadius=8;
    everyoneMessageButton.layer.borderWidth=4;
    everyoneMessageButton.layer.borderColor=kmessageunselectedtabcolor.CGColor;
    everyoneMessageButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    everyoneMessageButton.tag=200;
    [everyoneMessageButton addTarget:self action:@selector(everyoneMessageButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    // Setup the string
    NSMutableAttributedString *everyoneMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Everyone"],[dictLang objectForKey:@"Messages"]]];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
    {
        // Set the font to bold from the beginning of the string to the ","
        [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 8)];
        // Normal font for the rest of the text
        [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(8, 9)];
        
    }
    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
    {
        // Set the font to bold from the beginning of the string to the ","
        [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 6)];
        // Normal font for the rest of the text
        [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(6, 9)];
    }
    
  
    
    // Set the attributed string as the buttons' title text
    [everyoneMessageButton setAttributedTitle:everyoneMessageText forState:UIControlStateNormal];
    
    groupMessageButton.backgroundColor=kmessageunselectedtabcolor;
    groupMessageButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    groupMessageButton.layer.cornerRadius=8;
    groupMessageButton.layer.borderWidth=4;
    groupMessageButton.layer.borderColor=kmessageunselectedtabcolor.CGColor;
    groupMessageButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    groupMessageButton.tag=300;
    [groupMessageButton addTarget:self action:@selector(groupMessageButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    // Setup the string
    NSMutableAttributedString *groupMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Group"],[dictLang objectForKey:@"Messages"]]];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
    {
    // Set the font to bold from the beginning of the string to the ","
    [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
    
    // Normal font for the rest of the text
    [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(5, 9)];
    }
    else  if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
    {
        // Set the font to bold from the beginning of the string to the ","
        [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 6)];
        
        // Normal font for the rest of the text
        [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(6, 9)];
    }
    
    // Set the attributed string as the buttons' title text
    [groupMessageButton setAttributedTitle:groupMessageText forState:UIControlStateNormal];
    
    storeAnnouncementButton.backgroundColor=kmessageunselectedtabcolor;
    storeAnnouncementButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    storeAnnouncementButton.layer.cornerRadius=8;
    storeAnnouncementButton.layer.borderWidth=4;
    storeAnnouncementButton.layer.borderColor=kmessageunselectedtabcolor.CGColor;
    storeAnnouncementButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    storeAnnouncementButton.tag=400;
    [storeAnnouncementButton addTarget:self action:@selector(storeAnnouncementButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    // Setup the string
    NSMutableAttributedString *storeAnnouncementText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@\n%@",[dictLang objectForKey:@"Active"],[dictLang objectForKey:@"Store"],[dictLang objectForKey:@"Announcements"]]];
    
    // Set the font to bold from the beginning of the string to the ","
    NSDictionary *attrDict = @{
                               NSFontAttributeName : [UIFont systemFontOfSize:9],
                               NSForegroundColorAttributeName : [UIColor redColor]
                               };
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
    {
    [storeAnnouncementText addAttributes:attrDict range:NSMakeRange(0, 6)];
    //NSDictionary *attrs = @{ NSForegroundColorAttributeName : [UIColor redColor] };
    [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:9] forKey:NSFontAttributeName] range:NSMakeRange(0, 6)];
    
    [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(6, 6)];
    
    // Normal font for the rest of the text
    [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(12, 14)];
    }
    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
    {
        [storeAnnouncementText addAttributes:attrDict range:NSMakeRange(0, 5)];
        //NSDictionary *attrs = @{ NSForegroundColorAttributeName : [UIColor redColor] };
        [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:9] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
        
        [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(5, 8)];
        
        // Normal font for the rest of the text
        [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(13, 9)];
    }
    
    // Set the attributed string as the buttons' title text
    [storeAnnouncementButton setAttributedTitle:storeAnnouncementText forState:UIControlStateNormal];
    
    inboxButton.backgroundColor=kinboxunselectedtabcolor;
    [inboxButton setTitleColor:kNavBackgroundColor forState:UIControlStateNormal];
    inboxButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    inboxButton.layer.cornerRadius=8;
    inboxButton.layer.borderWidth=4;
    inboxButton.layer.borderColor=kinboxunselectedtabcolor.CGColor;
    inboxButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [inboxButton setTitle:[dictLang objectForKey:@"Inbox"] forState:UIControlStateNormal];
    inboxButton.tag=500;
    [inboxButton addTarget:self action:@selector(inboxButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    savedButton.backgroundColor=kinboxunselectedtabcolor;
    [savedButton setTitleColor:kNavBackgroundColor forState:UIControlStateNormal];
    savedButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    savedButton.layer.cornerRadius=8;
    savedButton.layer.borderWidth=4;
    savedButton.layer.borderColor=kinboxunselectedtabcolor.CGColor;
    savedButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [savedButton setTitle:[dictLang objectForKey:@"Saved"] forState:UIControlStateNormal];
    savedButton.tag=600;
    [savedButton addTarget:self action:@selector(savedButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    sentButton.backgroundColor=kinboxunselectedtabcolor;
    [sentButton setTitleColor:kNavBackgroundColor forState:UIControlStateNormal];
    sentButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    sentButton.layer.cornerRadius=8;
    sentButton.layer.borderWidth=4;
    sentButton.layer.borderColor=kinboxunselectedtabcolor.CGColor;
    sentButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [sentButton setTitle:[dictLang objectForKey:@"Sent"] forState:UIControlStateNormal];
    sentButton.tag=700;
    [sentButton addTarget:self action:@selector(sentButtonAction:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - searchBarCancelButtonClicked method
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar1
{
    [backendView removeFromSuperview];
    searchTag=NO;
    [searchBar removeFromSuperview];
    [searchbutton setEnabled:YES];
    [searchbutton setTintColor: kNavBackgroundColor];
    [searchsepcbutton setEnabled:YES];
    [searchsepcbutton setTintColor: kNavBackgroundColor];
    [speakerButton setEnabled:YES];
    [speakerButton setTintColor: kNavBackgroundColor];
    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: kNavBackgroundColor};
    [self.tableView reloadData];
    
}

#pragma mark - searchBarSearchButtonClicked method
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    // Do the search...
    [searchBar removeFromSuperview];
    [searchbutton setEnabled:YES];
    [searchbutton setTintColor: kNavBackgroundColor];
    [searchsepcbutton setEnabled:YES];
    [searchsepcbutton setTintColor: kNavBackgroundColor];
    [speakerButton setEnabled:YES];
    [speakerButton setTintColor: kNavBackgroundColor];
    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: kNavBackgroundColor};
    [searchBar resignFirstResponder];
    [self.tableView reloadData];
    [backendView removeFromSuperview];
}

#pragma mark - searchBar enter text method
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    searchTag=YES;
    inboxSearchText=searchText;
    [searchButtonalertTable removeFromSuperview];
    [self SearchbarSearchText:searchText];
}

#pragma mark - SearchbarSearchText method
-(void)SearchbarSearchText:(NSString *)searchText
{
    if (searchText.length>0)
    {
        searchResultsAll=[[NSMutableArray alloc]init];
        switch (self.tableView.tag) {
            case 1:
            case 4:
            case 10:
                for (int i =0;i<[inboxArray count];i++)
                    
                {
                    messageinboxdata=[inboxArray objectAtIndex:i];
                    NSString *city;
                    if([searchString isEqualToString:@"store"])
                    {
                        
                        if (messageinboxdata.maCallType==1)
                        {
                            city = messageinboxdata.storeName;
                        }
                        else
                        {
                            city = messageinboxdata.storeNames;
                        }
                    }
                    else
                    {
                        city = messageinboxdata.from;
                    }
                    NSRange range=[city rangeOfString:searchText options:NSCaseInsensitiveSearch];
                    if (range.length >0)
                    {
                        if (![searchResultsAll containsObject:city]) {
                            [searchResultsAll addObject:[inboxArray objectAtIndex:i]];
                        }
                    }
                    
                }
                [self.tableView reloadData];
                break;
                
            case 2:
            case 5:
            case 11:
                for (int i =0;i<[savedArray count];i++)
                    
                {
                    
                    messagesaveddata=[savedArray objectAtIndex:i];
                    NSString *city;
                    if([searchString isEqualToString:@"store"])
                    {
                        if (messagesaveddata.maCallType==1)
                        {
                            city = messagesaveddata.storeName;
                        }
                        else
                        {
                            city = messagesaveddata.storeNames;
                        }
                    }
                    else
                    {
                        city = messagesaveddata.from;
                    }
                    NSRange range=[city rangeOfString:searchText options:NSCaseInsensitiveSearch];
                    if (range.length >0)
                    {
                        if (![searchResultsAll containsObject:city]) {
                            [searchResultsAll addObject:[savedArray objectAtIndex:i]];
                        }
                    }
                    
                }
                [self.tableView reloadData];
                break;
            case 3:
            case 6:
            case 12:
                for (int i =0;i<[sentArray count];i++)
                    
                {
                    messagesentdata=[sentArray objectAtIndex:i];
                    NSString *city;
                    if([searchString isEqualToString:@"store"])
                    {
                        if (messagesentdata.maCallType==1)
                        {
                            city = messagesentdata.storeName;
                        }
                        else
                        {
                            city = messagesentdata.storeNames;
                        }
                    }
                    else
                    {
                        city = messagesentdata.to;
                    }
                    NSRange range=[city rangeOfString:searchText options:NSCaseInsensitiveSearch];
                    if (range.length >0)
                    {
                        if (![searchResultsAll containsObject:city]) {
                            [searchResultsAll addObject:[sentArray objectAtIndex:i]];
                        }
                    }
                    
                }
                [self.tableView reloadData];
                break;
            case 7:
                for (int i =0;i<[inboxArray count];i++)
                    
                {
                    announcementinboxdata=[inboxArray objectAtIndex:i];
                    NSString *city;
                    if([searchString isEqualToString:@"store"])
                    {
                        if (announcementinboxdata.maCallType==1)
                        {
                            city = announcementinboxdata.storeName;
                        }
                        else
                        {
                            city = announcementinboxdata.storeNames;
                        }
                    }
                    else
                    {
                        city = announcementinboxdata.from;
                    }
                    NSRange range=[city rangeOfString:searchText options:NSCaseInsensitiveSearch];
                    if (range.length >0)
                    {
                        if (![searchResultsAll containsObject:city]) {
                            [searchResultsAll addObject:[inboxArray objectAtIndex:i]];
                        }
                    }
                    
                }
                [self.tableView reloadData];
                break;
            case 8:
                for (int i =0;i<[savedArray count];i++)
                {
                    announcementsaveddata=[savedArray objectAtIndex:i];
                    NSString *city;
                    if([searchString isEqualToString:@"store"])
                    {
                        if (announcementsaveddata.maCallType==1)
                        {
                            city = announcementsaveddata.storeName;
                        }
                        else
                        {
                            city = announcementsaveddata.storeNames;
                        }
                    }
                    else
                    {
                        city = announcementsaveddata.from;
                    }
                    NSRange range=[city rangeOfString:searchText options:NSCaseInsensitiveSearch];
                    if (range.length >0)
                    {
                        if (![searchResultsAll containsObject:city]) {
                            [searchResultsAll addObject:[savedArray objectAtIndex:i]];
                        }
                    }
                    
                }
                [self.tableView reloadData];
                break;
            case 9:
                for (int i =0;i<[sentArray count];i++)
                {
                    announcementsentdata=[sentArray objectAtIndex:i];
                    NSString *city;
                    if([searchString isEqualToString:@"store"])
                    {
                        if (announcementsentdata.maCallType==1)
                        {
                            city = announcementsentdata.storeName;
                        }
                        else
                        {
                            city = announcementsentdata.storeNames;
                        }
                    }
                    else
                    {
                        city = announcementsentdata.to;
                    }
                    NSRange range=[city rangeOfString:searchText options:NSCaseInsensitiveSearch];
                    if (range.length >0)
                    {
                        if (![searchResultsAll containsObject:city]) {
                            [searchResultsAll addObject:[sentArray objectAtIndex:i]];
                        }
                    }
                    
                }
                [self.tableView reloadData];
                break;
        }
    }
}

#pragma mark - searchbuttonAction method
-(void)searchbuttonAction:(id)button
{
    [searchsepcbutton setEnabled:NO];
    searchString=@"name";
    
    expandedIndexPath=nil;
    [self.tableView reloadData];
    [self audioPlayerStop];
    inboxSearchText=@"";
    alertArray=[NSArray arrayWithObjects:[dictLang objectForKey:@"Search by Sender's Name"],[dictLang objectForKey:@"Search by Store's Name"], nil];
    self.navigationItem.hidesBackButton = YES;
    searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 20)];
    searchBar.placeholder=[dictLang objectForKey:@"Search by Sender's Name"];
    //searchBar.placeholder=@"Search";
    searchBar.delegate=self;
    searchBar.showsCancelButton = YES;
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitle:[dictLang objectForKey:@"Cancel"]];
    [searchBar sizeToFit];
    [self.navigationController.navigationBar addSubview:searchBar];
    [searchbutton setEnabled:NO];
    [searchbutton setTintColor: [UIColor clearColor]];
    [searchsepcbutton setEnabled:NO];
    [searchsepcbutton setTintColor: [UIColor clearColor]];
    [speakerButton setEnabled:NO];
    [speakerButton setTintColor: [UIColor clearColor]];
    [searchBar becomeFirstResponder];
    [self searchsepcbutton:nil];
}

#pragma mark - SearchsepcbuttonAction method
-(void)searchsepcbuttonAction:(id)button
{
    expandedIndexPath=nil;
    [self.tableView reloadData];
    [searchbutton setEnabled:NO];
    if (searchsepctag)
    {
        [self cancelButtonAction];
    }
    else
    {
        searchsepctag=YES;
        switch (self.tableView.tag) {
            case 3:
            case 6:
            case 9:
            case 12:
                alertArray=[NSArray arrayWithObjects:[dictLang objectForKey:@"Longest Duration"],[dictLang objectForKey:@"Most Recent"],nil];
                searchButtonalertTableHeight=115;
                break;
            case 2:
            case 5:
            case 8:
            case 11:
                alertArray=[NSArray arrayWithObjects:[dictLang objectForKey:@"Longest Duration"],[dictLang objectForKey:@"Most Recent"],[dictLang objectForKey:@"Unread"],[dictLang objectForKey:@"Read"],nil];
                searchButtonalertTableHeight=220;
                break;
                
            default:
                alertArray=[NSArray arrayWithObjects:[dictLang objectForKey:@"Longest Duration"],[dictLang objectForKey:@"Most Recent"],[dictLang objectForKey:@"Expires First"],[dictLang objectForKey:@"Unread"],[dictLang objectForKey:@"Read"],nil];
                searchButtonalertTableHeight=276;
                break;
        }
        [self searchsepcbutton:nil];
    }
}

#pragma mark - searchsepcbutton method
-(void)searchsepcbutton:(UIButton *)button
{
    [self audioPlayerStop];
    [self searchsepcbuttonTableCreation];
    backendView.hidden=NO;
    searchButtonalertTable.delegate=self;
    searchButtonalertTable.dataSource=self;
    [searchButtonalertTable reloadData];
}

#pragma mark - searchsepcbuttonTableCreation method
-(void)searchsepcbuttonTableCreation
{
    backendView=[[UIView alloc]init];
    backendView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    backendView.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    searchButtonalertTable=[[UITableView alloc]init];
    if (searchsepctag)
    {
        searchButtonalertTable.frame=CGRectMake(10, 0, self.view.frame.size.width-20, searchButtonalertTableHeight);
    }
    else
    {
        searchButtonalertTable.frame=CGRectMake(10, 0, self.view.frame.size.width-20, 115);
        
    }
    searchButtonalertTable.layer.cornerRadius=10;
    searchButtonalertTable.layer.borderWidth=2;
    searchButtonalertTable.layer.borderColor=[UIColor lightGrayColor].CGColor;
    cancelButton=[UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame=CGRectMake(self.view.frame.size.width-55, (self.view.frame.size.height/2)-128, 30, 30);
    [cancelButton setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonAction) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.backgroundColor=[UIColor whiteColor];
    cancelButton.clipsToBounds = YES;
    cancelButton.layer.cornerRadius = cancelButton.frame.size.width/2.0f;
    cancelButton.layer.borderWidth=1.0f;
    backendView.hidden=YES;
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(searchsepcbuttonTableCreationCancel)];
    tap.delegate=self;
    [backendView addSubview:searchButtonalertTable];
    [backendView addGestureRecognizer:tap];
    [self.view addSubview:backendView];
}

#pragma mark - searchsepcbuttonTableCreationCancel method
-(void)searchsepcbuttonTableCreationCancel
{
    if (searchsepctag)
    {
        [self cancelButtonAction];
    }
    
}

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:searchButtonalertTable]) {
        searchbackendView=YES;
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark inbox/saved/sent TabChange methods
-(void)inbox_saved_sent_TabChange:(NSInteger)sender
{
    if (sender==1)
    {
        inboxClicked=YES;
    }
    else if (sender==2)
    {
        savedClicked=YES;
    }
    else if (sender==3)
    {
        sentClicked=YES;
    }
}

#pragma mark - viewDidLoad method
- (void)viewDidLoad
{
     dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    
    MarqueeLabel *_lectureName = [[MarqueeLabel alloc] initWithFrame:CGRectMake(100,20,60, 20) duration:8.0 andFadeLength:10.0f];
    [_lectureName setTextAlignment:NSTextAlignmentCenter];
    [_lectureName setBackgroundColor:[UIColor clearColor]];
    
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    
    
    storeAnnouncementCount.backgroundColor=kGreencolor;
    groupMessageCount.hidden=YES;
    privateMessageCount.hidden=YES;
    storeAnnouncementCount.hidden=YES;
    everyoneMessageCount.hidden=YES;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kNavBackgroundColor,
       NSFontAttributeName:[UIFont fontWithName:kFontName size:13]}];
    [self inboxtotalcount ];
    privateMessageCount.layer.cornerRadius = privateMessageCount.frame.size.width / 2;
    privateMessageCount.clipsToBounds=YES;
    everyoneMessageCount.layer.cornerRadius = everyoneMessageCount.frame.size.width / 2;
    everyoneMessageCount.clipsToBounds=YES;
    groupMessageCount.layer.cornerRadius = groupMessageCount.frame.size.width / 2;
    groupMessageCount.clipsToBounds=YES;
    storeAnnouncementCount.layer.cornerRadius = storeAnnouncementCount.frame.size.width / 2;
    storeAnnouncementCount.clipsToBounds=YES;
    [self creationUIbuttons_Private_everyone_announcement_group];
    NSDictionary *inboxpreviousTabDic =[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"inboxpreviousTab"]];
    
    self.tableView.tag=[[inboxpreviousTabDic objectForKey:@"tabletag"] integerValue];
    if (self.tableView.tag==1 || self.tableView.tag==2 || self.tableView.tag==3 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"4"])
    {
        [self inbox_saved_sent_TabChange:[[inboxpreviousTabDic objectForKey:@"bottomsegmentindex"] integerValue]];
        privateMessageClicked=YES;
    }
    else if (self.tableView.tag==4 || self.tableView.tag==5 || self.tableView.tag==6 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"17"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"108"])
    {
        [self inbox_saved_sent_TabChange:[[inboxpreviousTabDic objectForKey:@"bottomsegmentindex"] integerValue]];
        groupMessageClicked=YES;
    }
    else if (self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"10"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"16"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"21"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"22"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"23"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"25"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"200"])
    {
        [self inbox_saved_sent_TabChange:[[inboxpreviousTabDic objectForKey:@"bottomsegmentindex"] integerValue]];
        storeAnnouncementClicked=YES;
    }
    else if (self.tableView.tag==10 || self.tableView.tag==11 || self.tableView.tag==12 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"33"])
    {
        [self inbox_saved_sent_TabChange:[[inboxpreviousTabDic objectForKey:@"bottomsegmentindex"] integerValue]];
        everyoneMessageClicked=YES;
    }
    else
    {
        privateMessageButton.backgroundColor=kmessageselectedtabcolor;
        privateMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        // Setup the string
        NSMutableAttributedString *privateMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Private"],[dictLang objectForKey:@"Messages"]]];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 7)];
            
            // Normal font for the rest of the text
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(7, 9)];
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
            
            // Normal font for the rest of the text
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(5, 9)];
        }
        
        // Set the attributed string as the buttons' title text
        [privateMessageButton setAttributedTitle:privateMessageText forState:UIControlStateNormal];
        privateMessageClicked=YES;
        inboxButton.backgroundColor=kinboxselectedtabcolor;
        inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        inboxClicked=YES;
        self.tableView.tag=1;
    }
    searchbackendView=YES;
    NSInteger row = 1;
    NSInteger section = 1;
    expandedIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
    searchbutton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"search"] style:UIBarButtonItemStyleDone target:self action:@selector(searchbuttonAction:)];
    searchsepcbutton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"sort"] style:UIBarButtonItemStyleDone target:self action:@selector(searchsepcbuttonAction:)];
    speakerButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"loud"] style:UIBarButtonItemStyleDone target:self action:@selector(speakerButtonAction:)];
    self.navigationItem.leftItemsSupplementBackButton=YES;
    self.navigationItem.leftBarButtonItem = searchbutton;
    self.navigationItem.rightBarButtonItems=@[speakerButton,searchsepcbutton];
    
    [searchbutton setEnabled:NO];
    [searchsepcbutton setEnabled:NO];
    store = [EarboxDataStore sharedStore];
    earboxinboxmodelStore=[Earboxinboxmodel sharedModel];
    
    [_lectureName setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"inbox_Tcm"]];
    storeidString=0;
    deselected=YES;
    isReplying = NO;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    storeNameString=[[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
    [self speakerAudioChange];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: kNavBackgroundColor};
    self.tableView.layer.cornerRadius=4;
    self.tableView.layer.borderWidth=1;
    self.tableView.layer.borderColor=kNavBackgroundColor.CGColor;
    [self.tableView setAllowsMultipleSelection:YES];
    companyId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"companyId"]intValue];
    loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    apploginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
    [self inboxFullprivate_everyone_group_announcement_Apicall];
    
    _lectureName.adjustsFontSizeToFitWidth=NO;
    [_lectureName setAnimationCurve:UIViewAnimationOptionCurveEaseIn];
    _lectureName.marqueeType = MLContinuous;
    [self.navigationItem setTitleView:_lectureName];
    
    [super viewDidLoad];
}

#pragma mark - inboxFullprivate_everyone_group_announcement_Apicall
-(void)inboxFullprivate_everyone_group_announcement_Apicall
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"earboxv2"] && ![[[NSUserDefaults standardUserDefaults]objectForKey:@"undeliveredfailedInbox"] isEqualToString:@"undeliveredfailedInbox"])
    {
        
        [self requestUndeliveredMsgsinbox];
    }
    else
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"InboxVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"EarboxListV2"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
            
        }
        else
        {
            [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
            NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
            [myQueue addOperationWithBlock:^{
                
                NSDictionary *twearboxListdic= [HttpHandler earboxlistV2:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId userScreen:@"InboxVC"];
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    if ([[twearboxListdic objectForKey:@"earbox"] isKindOfClass:[NSNull class]] || [twearboxListdic objectForKey:@"message"])
                    {
                        if ([twearboxListdic objectForKey:@"message"])
                        {
                            [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[twearboxListdic objectForKey:@"message"],[[NSUserDefaults standardUserDefaults] objectForKey:@"staticerrorcode"]]];
                        }
                        
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredsucessInbox" forKey:@"undeliveredfailedInbox"];
                        NSArray *messageinbox,*messagesent,*messagesaved,*announcementinbox,*announcementsent,*announcementsaved,
                        *groupinbox,*groupsent,*groupsaved,*everyoneinbox,*everyonesaved,*everyonesent;
                        NSDictionary *message=[[twearboxListdic objectForKey:@"earbox"]objectForKey:@"message"];
                        
                        for (int i=0; i<message.count; i++)
                        {
                            messageinbox=[message objectForKey:@"inbox"];
                            
                            messagesent=[message  objectForKey:@"sent"];
                            
                            messagesaved=[message objectForKey:@"saved"];
                            
                        }
                        if (messageinbox.count>0)
                        {
                            [earboxinboxmodelStore messageinboxSave:messageinbox];
                        }
                        if (messagesent.count>0)
                        {
                            [earboxinboxmodelStore messagesentSave:messagesent];
                        }
                        if (messagesaved.count>0)
                        {
                            [earboxinboxmodelStore messagesavedSave:messagesaved];
                        }
                        
                        NSDictionary *announcement=[[twearboxListdic objectForKey:@"earbox"] objectForKey:@"announcement"];
                        for (int i=0; i<announcement.count; i++)
                        {
                            announcementinbox=[announcement objectForKey:@"inbox"];
                            
                            announcementsent=[announcement  objectForKey:@"sent"];
                            
                            announcementsaved=[announcement  objectForKey:@"saved"];
                            
                        }
                        if (announcementinbox.count>0)
                        {
                            [earboxinboxmodelStore announcementinboxSave:announcementinbox];
                        }
                        if (announcementsent.count>0)
                        {
                            [earboxinboxmodelStore announcementsentSave:announcementsent];
                        }
                        if (announcementsaved.count>0)
                        {
                            [earboxinboxmodelStore announcementsavedSave:announcementsaved];
                        }
                        
                        NSDictionary *group=[[twearboxListdic objectForKey:@"earbox"] objectForKey:@"groupMessage"];
                        for (int i=0; i<group.count; i++)
                        {
                            groupinbox=[group objectForKey:@"inbox"];
                            
                            groupsent=[group  objectForKey:@"sent"];
                            
                            groupsaved=[group  objectForKey:@"saved"];
                            
                        }
                        if (groupinbox.count>0)
                        {
                            [earboxinboxmodelStore messageinboxSave:groupinbox];
                        }
                        if (groupsent.count>0)
                        {
                            [earboxinboxmodelStore messagesentSave:groupsent];
                        }
                        if (groupsaved.count>0)
                        {
                            [earboxinboxmodelStore messagesavedSave:groupsaved];
                        }
                        
                        NSDictionary *everyone=[[twearboxListdic objectForKey:@"earbox"] objectForKey:@"everyone"];
                        for (int i=0; i<everyone.count; i++)
                        {
                            everyoneinbox=[everyone objectForKey:@"inbox"];
                            
                            everyonesent=[everyone  objectForKey:@"sent"];
                            
                            everyonesaved=[everyone  objectForKey:@"saved"];
                            
                        }
                        if (everyoneinbox.count>0)
                        {
                            [earboxinboxmodelStore messageinboxSave:everyoneinbox];
                        }
                        if (everyonesent.count>0)
                        {
                            [earboxinboxmodelStore messagesentSave:everyonesent];
                        }
                        if (everyonesaved.count>0)
                        {
                            [earboxinboxmodelStore messagesavedSave:everyonesaved];
                        }
                        
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"earboxv2"];
                        
                        
                        // [self tableViewUpdate];
                        [self inboxtabIndexchange];
                        [UIView animateWithDuration:0 animations:^{
                            [self tableViewUpdate];
                        } completion:^(BOOL finished) {
                            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                        }];
                        
                    }
                    
                }];
            }];
        }
    }
    
    
}

#pragma mark - inboxpreviousTabMethod
-(void)inboxpreviousTabMethod
{
    [self creationUIbuttons_Private_everyone_announcement_group ];
    NSDictionary *inboxpreviousTabDic =[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"inboxpreviousTab"]];
    
    self.tableView.tag=[[inboxpreviousTabDic objectForKey:@"tabletag"] integerValue];
    
    if (self.tableView.tag ==1 || self.tableView.tag ==2 || self.tableView.tag ==3 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"4"])
    {
        
        privateMessageButton.backgroundColor=kmessageselectedtabcolor;
        privateMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        // Setup the string
        NSMutableAttributedString *privateMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Private"],[dictLang objectForKey:@"Messages"]]];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 7)];
            
            // Normal font for the rest of the text
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(7, 9)];
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
            
            // Normal font for the rest of the text
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(5, 9)];
        }
        
        // Set the attributed string as the buttons' title text
        [privateMessageButton setAttributedTitle:privateMessageText forState:UIControlStateNormal];
        [self inboxpreviousTabCreation:1 bottom:1 tabletag:1];
        if (self.tableView.tag==1)
        {
            [self inboxpreviousTabCreation:1 bottom:1 tabletag:1];
            inboxButton.backgroundColor=kinboxselectedtabcolor;
            inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        }
        if (self.tableView.tag==2)
        {
            [self inboxpreviousTabCreation:1 bottom:2 tabletag:2];
            savedButton.backgroundColor=kinboxselectedtabcolor;
            savedButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        }
        if (self.tableView.tag==3)
        {
            [self inboxpreviousTabCreation:1 bottom:3 tabletag:3];
            sentButton.backgroundColor=kinboxselectedtabcolor;
            sentButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        }
    }
    else if (self.tableView.tag ==4 || self.tableView.tag ==5 || self.tableView.tag ==6 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"17"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"108"])
    {
        
        groupMessageButton.backgroundColor=kmessageselectedtabcolor;
        groupMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        // Setup the string
        NSMutableAttributedString *groupMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Group"],[dictLang objectForKey:@"Messages"]]];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
            
            // Normal font for the rest of the text
            [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(5, 9)];
        }
        else  if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 6)];
            
            // Normal font for the rest of the text
            [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(6, 9)];
        }
        // Set the attributed string as the buttons' title text
        [groupMessageButton setAttributedTitle:groupMessageText forState:UIControlStateNormal];
        [self inboxpreviousTabCreation:3 bottom:1 tabletag:4];
        if (self.tableView.tag==4)
        {
            [self inboxpreviousTabCreation:3 bottom:1 tabletag:4];
            inboxButton.backgroundColor=kinboxselectedtabcolor;
            inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        }
        if (self.tableView.tag==5)
        {
            [self inboxpreviousTabCreation:3 bottom:2 tabletag:5];
            savedButton.backgroundColor=kinboxselectedtabcolor;
            savedButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        }
        if (self.tableView.tag==6)
        {
            [self inboxpreviousTabCreation:3 bottom:3 tabletag:6];
            sentButton.backgroundColor=kinboxselectedtabcolor;
            sentButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        }
    }
    else if (self.tableView.tag ==7 || self.tableView.tag ==8 || self.tableView.tag ==9 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"10"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"16"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"21"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"22"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"23"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"25"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"200"])
    {
        
        storeAnnouncementButton.backgroundColor=kmessageselectedtabcolor;
        storeAnnouncementButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        // Setup the string
        NSMutableAttributedString *storeAnnouncementText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@\n%@",[dictLang objectForKey:@"Active"],[dictLang objectForKey:@"Store"],[dictLang objectForKey:@"Announcements"]]];
        
        // Set the font to bold from the beginning of the string to the ","
        NSDictionary *attrDict = @{
                                   NSFontAttributeName : [UIFont systemFontOfSize:9],
                                   NSForegroundColorAttributeName : [UIColor redColor]
                                   };
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
        {
            [storeAnnouncementText addAttributes:attrDict range:NSMakeRange(0, 6)];
            //NSDictionary *attrs = @{ NSForegroundColorAttributeName : [UIColor redColor] };
            [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:9] forKey:NSFontAttributeName] range:NSMakeRange(0, 6)];
            
            [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(6, 6)];
            
            // Normal font for the rest of the text
            [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(12, 14)];
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
        {
            [storeAnnouncementText addAttributes:attrDict range:NSMakeRange(0, 5)];
            //NSDictionary *attrs = @{ NSForegroundColorAttributeName : [UIColor redColor] };
            [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:9] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
            
            [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(5, 8)];
            
            // Normal font for the rest of the text
            [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(13, 9)];
        }
        
        // Set the attributed string as the buttons' title text
        [storeAnnouncementButton setAttributedTitle:storeAnnouncementText forState:UIControlStateNormal];
        [self inboxpreviousTabCreation:4 bottom:1 tabletag:7];
        if (self.tableView.tag==7)
        {
            [self inboxpreviousTabCreation:4 bottom:1 tabletag:7];
            inboxButton.backgroundColor=kinboxselectedtabcolor;
            inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        }
        if (self.tableView.tag==8)
        {
            [self inboxpreviousTabCreation:4 bottom:2 tabletag:8];
            savedButton.backgroundColor=kinboxselectedtabcolor;
            savedButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        }
        if (self.tableView.tag==9)
        {
            [self inboxpreviousTabCreation:4 bottom:3 tabletag:9];
            sentButton.backgroundColor=kinboxselectedtabcolor;
            sentButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        }
    }
    else if (self.tableView.tag ==10 || self.tableView.tag ==11 || self.tableView.tag ==12 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"33"])
    {
        
        everyoneMessageButton.backgroundColor=kmessageselectedtabcolor;
        everyoneMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        // Setup the string
        NSMutableAttributedString *everyoneMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Everyone"],[dictLang objectForKey:@"Messages"]]];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 8)];
            // Normal font for the rest of the text
            [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(8, 9)];
            
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 6)];
            // Normal font for the rest of the text
            [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(6, 9)];
        }
        
        // Set the attributed string as the buttons' title text
        [everyoneMessageButton setAttributedTitle:everyoneMessageText forState:UIControlStateNormal];
        [self inboxpreviousTabCreation:2 bottom:1 tabletag:10];
        if (self.tableView.tag==10)
        {
            [self inboxpreviousTabCreation:2 bottom:1 tabletag:10];
            inboxButton.backgroundColor=kinboxselectedtabcolor;
            inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        }
        if (self.tableView.tag==11)
        {
            [self inboxpreviousTabCreation:2 bottom:2 tabletag:11];
            savedButton.backgroundColor=kinboxselectedtabcolor;
            savedButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        }
        if (self.tableView.tag==12)
        {
            [self inboxpreviousTabCreation:2 bottom:3 tabletag:12];
            sentButton.backgroundColor=kinboxselectedtabcolor;
            sentButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        }
    }
    else
    {
        
        privateMessageButton.backgroundColor=kmessageselectedtabcolor;
        privateMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        // Setup the string
        NSMutableAttributedString *privateMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Private"],[dictLang objectForKey:@"Messages"]]];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 7)];
            
            // Normal font for the rest of the text
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(7, 9)];
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
            
            // Normal font for the rest of the text
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(5, 9)];
        }
        
        // Set the attributed string as the buttons' title text
        [privateMessageButton setAttributedTitle:privateMessageText forState:UIControlStateNormal];
        [self inboxpreviousTabCreation:1 bottom:1 tabletag:1];
        inboxButton.backgroundColor=kinboxselectedtabcolor;
        inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        
    }
}

#pragma mark - inboxtabIndexchange
-(void)inboxtabIndexchange
{
    [self creationUIbuttons_Private_everyone_announcement_group ];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"4"])
    {
        self.tableView.tag=1;
        privateMessageButton.backgroundColor=kmessageselectedtabcolor;
        privateMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        // Setup the string
        NSMutableAttributedString *privateMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Private"],[dictLang objectForKey:@"Messages"]]];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 7)];
            
            // Normal font for the rest of the text
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(7, 9)];
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
            
            // Normal font for the rest of the text
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(5, 9)];
        }
        
        // Set the attributed string as the buttons' title text
        [privateMessageButton setAttributedTitle:privateMessageText forState:UIControlStateNormal];
        inboxButton.backgroundColor=kinboxselectedtabcolor;
        inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:1 bottom:1 tabletag:1];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"10"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"16"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"21"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"22"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"23"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"25"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"200"])
    {
        
        
        self.tableView.tag=7;
        
        storeAnnouncementButton.backgroundColor=kmessageselectedtabcolor;
        storeAnnouncementButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        // Setup the string
        NSMutableAttributedString *storeAnnouncementText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@\n%@",[dictLang objectForKey:@"Active"],[dictLang objectForKey:@"Store"],[dictLang objectForKey:@"Announcements"]]];
        
        // Set the font to bold from the beginning of the string to the ","
        NSDictionary *attrDict = @{
                                   NSFontAttributeName : [UIFont systemFontOfSize:9],
                                   NSForegroundColorAttributeName : [UIColor redColor]
                                   };
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
        {
            [storeAnnouncementText addAttributes:attrDict range:NSMakeRange(0, 6)];
            //NSDictionary *attrs = @{ NSForegroundColorAttributeName : [UIColor redColor] };
            [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:9] forKey:NSFontAttributeName] range:NSMakeRange(0, 6)];
            
            [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(6, 6)];
            
            // Normal font for the rest of the text
            [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(12, 14)];
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
        {
            [storeAnnouncementText addAttributes:attrDict range:NSMakeRange(0, 5)];
            //NSDictionary *attrs = @{ NSForegroundColorAttributeName : [UIColor redColor] };
            [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:9] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
            
            [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(5, 8)];
            
            // Normal font for the rest of the text
            [storeAnnouncementText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(13, 9)];
        }
        // Set the attributed string as the buttons' title text
        [storeAnnouncementButton setAttributedTitle:storeAnnouncementText forState:UIControlStateNormal];
        inboxButton.backgroundColor=kinboxselectedtabcolor;
        inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:2 bottom:2 tabletag:7];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"17"]||[[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"108"])
        
    {
        self.tableView.tag=4;
        
        groupMessageButton.backgroundColor=kmessageselectedtabcolor;
        groupMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        // Setup the string
        NSMutableAttributedString *groupMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Group"],[dictLang objectForKey:@"Messages"]]];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
            
            // Normal font for the rest of the text
            [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(5, 9)];
        }
        else  if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 6)];
            
            // Normal font for the rest of the text
            [groupMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(6, 9)];
        }
        
        // Set the attributed string as the buttons' title text
        [groupMessageButton setAttributedTitle:groupMessageText forState:UIControlStateNormal];
        inboxButton.backgroundColor=kinboxselectedtabcolor;
        inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:3 bottom:1 tabletag:4];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"33"])
        
    {
        self.tableView.tag=10;
        everyoneMessageButton.backgroundColor=kmessageselectedtabcolor;
        everyoneMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        // Setup the string
        NSMutableAttributedString *everyoneMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Everyone"],[dictLang objectForKey:@"Messages"]]];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 8)];
            // Normal font for the rest of the text
            [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(8, 9)];
            
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 6)];
            // Normal font for the rest of the text
            [everyoneMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(6, 9)];
        }
        
        // Set the attributed string as the buttons' title text
        [everyoneMessageButton setAttributedTitle:everyoneMessageText forState:UIControlStateNormal];
        inboxButton.backgroundColor=kinboxselectedtabcolor;
        inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:4 bottom:1 tabletag:10];
    }
    else
    {
        self.tableView.tag=1;
        privateMessageButton.backgroundColor=kmessageselectedtabcolor;
        privateMessageButton.layer.borderColor=kmessageselectedtabbordercolor.CGColor;
        // Setup the string
        NSMutableAttributedString *privateMessageText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",[dictLang objectForKey:@"Private"],[dictLang objectForKey:@"Messages"]]];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"english"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 7)];
            
            // Normal font for the rest of the text
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(7, 9)];
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"Languageselected"] isEqualToString:@"french"])
        {
            // Set the font to bold from the beginning of the string to the ","
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] range:NSMakeRange(0, 5)];
            
            // Normal font for the rest of the text
            [privateMessageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:7] forKey:NSFontAttributeName] range:NSMakeRange(5, 9)];
        }
        
        // Set the attributed string as the buttons' title text
        [privateMessageButton setAttributedTitle:privateMessageText forState:UIControlStateNormal];
        inboxButton.backgroundColor=kinboxselectedtabcolor;
        inboxButton.layer.borderColor=kinboxselectedtabbordercolor.CGColor;
        [self inboxpreviousTabCreation:1 bottom:1 tabletag:1];
    }
}

#pragma mark - viewWillAppear
-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceLogOut:) name:@"ForceLogOut" object:nil];
    
    tablecellclose=NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    
    inboxsortText=@"";
    [speakerButton setEnabled:YES];
    NSLog(@"Inbox viewWillAppear");
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterLockNotification) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatetimer_EarBox) name:@"pushnotificationcount" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(percentageCompleted:) name:@"percentageCompleted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushnotificationrecieved:) name:@"pushnotificationrecieved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedAnnouncement:)
                                                 name:@"recievedAnnouncementsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendAnnouncement:)
                                                 name:@"audioNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(replayCancelNotification:)
                                                 name:@"replayCancelNotification"
                                               object:nil];
    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"pushForMultiStore"];
    
    [super viewWillAppear:animated];
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark - viewWillDisappear
-(void)viewWillDisappear:(BOOL)animated
{
    isReplying=NO;
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"notificationtype"];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"pushForMultiStore"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self audioPlayerStop];
    [self replayCancelNotification:0];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushnotificationcount" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushnotificationrecieved" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"audioNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"replayCancelNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recievedAnnouncementsNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"percentageCompleted" object:nil];
    [super viewWillDisappear:animated];
}


#pragma mark - Database get message/everyone/group inboxdata method
-(void)getmessageinboxdata:(int)calltype
{
    inboxArray=nil;
    inboxArray= [store inboxartists:@"EarboxInbox" calltype:calltype];
    
}

#pragma mark - Database get message/everyone/group inboxdata method
-(void)getFreshInboxList:(int)calltype
{
    
    freshInboxList = [store inboxartists:@"EarboxInbox" calltype:calltype];
    
}

#pragma mark - Database get message/everyone/group saveddata method
-(void)getFreshSavedList:(int)calltype
{
    
    freshSavedList = [store inboxartists:@"EarboxSaved" calltype:calltype];
    
}

#pragma mark - Database get message/everyone/group sentdata method
-(void)getmessagesentdata:(int)calltype
{
    
    sentArray=[store inboxartists:@"EarboxSent" calltype:calltype];
    
}

#pragma mark - Database get message/everyone/group saveddata method
-(void)getmessagesaveddata:(int)calltype
{
    
    savedArray= [store inboxartists:@"EarboxSaved" calltype:calltype];
    
}

#pragma mark - Database get announcement inboxdata method
-(void)getannouncementinboxdata:(NSString *)earboxname
{
    inboxArray= [store artists:earboxname storeid:storeidString calltype:0];
    
}

#pragma mark - Database get announcement sentdata method
-(void)getannouncementsentdata:(NSString *)earboxname
{
    
    sentArray= [store artists:earboxname storeid:storeidString calltype:0];
    
}

#pragma mark - Database get announcement saveddata method
-(void)getannouncementsaveddata:(NSString *)earboxname
{
    savedArray = [store artists:earboxname storeid:storeidString calltype:0];
    
}


#pragma mark - UITable Delegate methods
-(void)expandrow
{
    CGRect rectOfCellInTableView = [self.tableView rectForRowAtIndexPath: indexpathcell];
    NSArray *visibleCells = [self.tableView visibleCells];
    cell=[visibleCells lastObject];
    if (cell.frame.origin.y==rectOfCellInTableView.origin.y)
    {
        if (visibleCells.count>6)
        {
            cell=[visibleCells firstObject];
            [self.tableView setContentOffset:CGPointMake(0,cell.frame.origin.y+130) animated:YES];
        }
    }
    cell=[visibleCells firstObject];
    
    if (rectOfCellInTableView.origin.y==cell.frame.origin.y)
    {
        if (rowindex==0)
        {
            
        }
        else
        {
            [self.tableView setContentOffset:CGPointMake(0,cell.frame.origin.y-30) animated:YES];
        }
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==nameButtonalertTable )
    {
        return 45;
    }
    else if (tableView==searchButtonalertTable)
    {
        return 55;
    }
    else
    {
        /* NSArray<NSIndexPath *> *selectedRows = [tableView indexPathsForSelectedRows];
         if (selectedRows && [selectedRows containsObject:indexPath])
         if ([indexpathcell isEqual:indexPath])
         {
         return 170.0; // Expanded height
         }
         return 55.0;*/
        
        if ([indexPath compare:expandedIndexPath] == NSOrderedSame) {
            
            return 170.0; // Expanded height
        }
        return 60.0; // Normal height
    }
    
}


#pragma mark - UITable Datasource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        if(tableView==nameButtonalertTable)
        {
            return [[groupDict objectForKey:@"Stores"] count];
        }
        else if (tableView==searchButtonalertTable)
        {
            return [alertArray count];
        }
        else
        {
            if(searchTag)
            {
                return [searchResultsAll count];
                
            }
            else
            {
                if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
                {
                    return [inboxArray count];
                }
                else if (self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
                {
                    return [savedArray count];
                }
                else if (self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
                {
                    return [sentArray  count];
                }
                else if (self.tableView.tag==7)
                {
                    return [inboxArray count];
                }
                else if (self.tableView.tag==8)
                {
                    return  [savedArray count];
                }
                else if (self.tableView.tag==9)
                {
                    return [sentArray count];
                }
            }
        }
    }
    else if(section==1)
    {
        return [[groupDict objectForKey:@"Groups"] count];
    }
    else
    {
        return [[groupDict objectForKey:@"Districts"] count];
    }
    return 0;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView==nameButtonalertTable)
    {
        if([[groupDict objectForKey:@"Districts"] count]!=0)
        {
        return 3;
        }
        else if([[groupDict objectForKey:@"Groups"] count]!=0)
        {
            return 2;
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==nameButtonalertTable)
    {
        return 40;
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView==nameButtonalertTable)
    {
        if (section==0)
        {
            return @"Stores";
        }
        else if(section==1)
        {
            return @"Groups";
        }
        else
        {
            return @"Districts";
        }
    }
    else
    {
        
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==nameButtonalertTable)
    {
        static NSString *cellIdentifier=@"cell";
        UITableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell1 == nil) {
            cell1 = [[UITableViewCell alloc]
                     initWithStyle:UITableViewCellStyleDefault
                     reuseIdentifier:cellIdentifier];
        }
        cell1.textLabel.font=[UIFont systemFontOfSize: 15.0];
        if (indexPath.section==0)
        {
            cell1.textLabel.text=[[groupDict objectForKey:@"Stores"] objectAtIndex:indexPath.row];
        }
        else if (indexPath.section==1)
        {
            
            cell1.textLabel.text=[[groupDict objectForKey:@"Groups"] objectAtIndex:indexPath.row];
        }
        else
        {
            cell1.textLabel.text=[[groupDict objectForKey:@"Districts"] objectAtIndex:indexPath.row];
        }
        
        return cell1;
    }
    else if (tableView==searchButtonalertTable)
    {
        static NSString *cellIdentifier=@"cell";
        UITableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell1 == nil) {
            cell1 = [[UITableViewCell alloc]
                     initWithStyle:UITableViewCellStyleDefault
                     reuseIdentifier:cellIdentifier];
        }
        cell1.textLabel.text=[alertArray objectAtIndex:indexPath.row];
        cell1.textLabel.textAlignment=NSTextAlignmentCenter;
        cell1.textLabel.textColor=kNavBackgroundColor;
        
        return cell1;
        
        
    }
    else
    {
        static NSString *cellIdentifier=@"cell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[TableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:cellIdentifier];
        }
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 2)];
        [cell.replyTextButton setTitle:[dictLang objectForKey:@"Reply"] forState:UIControlStateNormal];
         [cell.sendTextButton setTitle:[dictLang objectForKey:@"Save"] forState:UIControlStateNormal];
         [cell.deleteTextButton setTitle:[dictLang objectForKey:@"Delete"] forState:UIControlStateNormal];
        separatorLineView.backgroundColor = [UIColor grayColor];// you can also put image here
        cell.activityView.hidden=YES;
        cell.circularProgressView.progressTintColor=kNavBackgroundColor;
        cell.circularProgressView.trackTintColor=kNavBackgroundColor;
        [cell.contentView addSubview:separatorLineView];
        cell.sentLabel.text=[NSString stringWithFormat:@"%@:",[dictLang objectForKey:@"Sent"]];
        cell.lengthLabel.text=[NSString stringWithFormat:@"%@:",[dictLang objectForKey:@"Length"]];
        cell.expriesLabel.text=[NSString stringWithFormat:@"%@:",[dictLang objectForKey:@"Expires"]];
        cell.nameTextButton.userInteractionEnabled=NO;
        cell.selectionStyle=UITableViewCellAccessoryNone;
        cell.timeElapsed.hidden=YES;
        cell.storenameLabel.hidden=NO;
        cell.replyimage.image=[UIImage imageNamed:@""];
        cell.groupnameLabel.hidden=YES;
        [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
        
        
        cell.nameTextLabel.marqueeType = MLContinuous;
        cell.nameTextLabel.scrollDuration = 8.0;
        cell.nameTextLabel.animationCurve = UIViewAnimationOptionCurveEaseInOut;
        cell.nameTextLabel.fadeLength = 0.0f;
        cell.nameTextLabel.leadingBuffer = 0.0f;
        cell.nameTextLabel.trailingBuffer = 10.0f;
        
        if(searchTag)
        {
            if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
            {
                if (self.tableView.tag==1 || self.tableView.tag==10)
                {
                    cell.groupnameLabel.hidden=YES;
                }
                else
                {
                    cell.nameTextButton.userInteractionEnabled=YES;
                    cell.groupnameLabel.hidden=NO;
                }
                if (searchResultsAll.count>indexPath.row)
                {
                    messageinboxdata= [searchResultsAll objectAtIndex:indexPath.row];
                    cell.replyimage.hidden=NO;
                    cell.expriesLabel.hidden=NO;
                    cell.expriesTextLabel.hidden=NO;
                    if (![messageinboxdata.status isEqualToString:@"new"])
                    {
                        [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [cell.nameTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
                    }
                    
                    if ([messageinboxdata.reply isEqualToString:@"1"])
                    {
                        cell.replyimage.image=[UIImage imageNamed:@"Reply"];
                    }
                    else
                    {
                        cell.replyimage.image=[UIImage imageNamed:@""];
                    }
                    
                    if ([messageinboxdata.from isEqualToString:@"All"])
                    {
                        cell.nameTextLabel.text=[dictLang objectForKey:@"All"];
                    }
                    else
                    {
                    cell.nameTextLabel.text=[messageinboxdata.from capitalizedString];
                    }
                    
                    if (messageinboxdata.maCallType==1)
                    {
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],messageinboxdata.groupName];
                        cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"], messageinboxdata.storeName];
                    }
                    else
                    {
                        if (messageinboxdata.callType==102 || messageinboxdata.callType==110)
                        {
                            cell.storenameLabel.text=@"ASMessage";
                            cell.groupnameLabel.hidden=YES;
                        }
                        else
                        {
                            cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],messageinboxdata.groupNames];
                            if ([messageinboxdata.nodeName isEqualToString:@""])
                            {
                                cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],messageinboxdata.storeNames];
                            }
                            else
                            {
                                
                                cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@,%@",[dictLang objectForKey:@"Store"],messageinboxdata.nodeName,messageinboxdata.storeNames];
                            }
                        }
                    }
                    if (messageinboxdata.maCallType==3 ||messageinboxdata.callType==104)
                    {
                        cell.storenameLabel.hidden=YES;
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"ASGroup"],messageinboxdata.groupName];
                        
                    }
                    cell.sentTextLabel.text= [NSDateClass dateformate:messageinboxdata.callStartTime];
                    cell.expriesTextLabel.text=[NSDateClass ttlformate:messageinboxdata.ttl];
                    cell.lengthTextLabel.text=[NSDateClass lengthformate:messageinboxdata.duration];
                    [cell.nameTextButton setTitle:[[messageinboxdata.from substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                }
            }
            else if (self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
                
            {
                if (self.tableView.tag==2 || self.tableView.tag==11)
                {
                    cell.groupnameLabel.hidden=YES;
                }
                else
                {
                    cell.nameTextButton.userInteractionEnabled=YES;
                    cell.groupnameLabel.hidden=NO;
                }
                if (searchResultsAll.count>indexPath.row)
                {
                    cell.replyimage.hidden=NO;
                    cell.expriesLabel.hidden=YES;
                    cell.expriesTextLabel.hidden=YES;
                    messagesaveddata = [searchResultsAll objectAtIndex:indexPath.row];
                    if (![messagesaveddata.status isEqualToString:@"new"])
                    {
                        [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [cell.nameTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
                    }
                    if ([messagesaveddata.reply isEqualToString:@"1"])
                    {
                        cell.replyimage.image=[UIImage imageNamed:@"Reply"];
                    }
                    else
                    {
                        cell.replyimage.image=[UIImage imageNamed:@""];
                    }
                    
                    if ([messagesaveddata.from isEqualToString:@"All"])
                    {
                        cell.nameTextLabel.text=[dictLang objectForKey:@"All"];
                    }
                    else
                    {
                    cell.nameTextLabel.text=[messagesaveddata.from capitalizedString];
                    }
                    
                    if (messagesaveddata.maCallType==1)
                    {
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],messagesaveddata.groupName];
                        cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],messagesaveddata.storeName];
                    }
                    else
                    {
                        if (messagesaveddata.callType==102 || messageinboxdata.callType==110)
                        {
                            cell.storenameLabel.text=@"ASMessage";
                            cell.groupnameLabel.hidden=YES;
                        }
                        else
                        {
                            cell.groupnameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],messagesaveddata.groupNames];
                            if ([messagesaveddata.nodeName isEqualToString:@""])
                            {
                                cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],messagesaveddata.storeNames];
                            }
                            else
                            {
                                
                                cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@,%@",[dictLang objectForKey:@"Store"],messagesaveddata.nodeName,messagesaveddata.storeNames];
                            }
                        }
                    }
                    if (messagesaveddata.maCallType==3 ||messagesaveddata.callType==104)
                    {
                        cell.storenameLabel.hidden=YES;
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"ASGroup"],messagesaveddata.groupName];
                        
                    }
                    
                    cell.sentTextLabel.text= [NSDateClass dateformate:messagesaveddata.callStartTime];
                    cell.expriesTextLabel.text=[NSDateClass ttlformate:messagesaveddata.ttl];
                    cell.lengthTextLabel.text=[NSDateClass lengthformate:messagesaveddata.duration];
                    [cell.nameTextButton setTitle:[[messagesaveddata.from substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                }
            }
            else if (self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
            {
                if (self.tableView.tag==3 || self.tableView.tag==12)
                {
                    cell.groupnameLabel.hidden=YES;
                }
                else
                {
                    cell.nameTextButton.userInteractionEnabled=YES;
                    cell.groupnameLabel.hidden=NO;
                }
                cell.contentView.backgroundColor = [UIColor whiteColor];
                if (searchResultsAll.count>indexPath.row)
                {
                    cell.replyimage.hidden=YES;
                    cell.expriesLabel.hidden=YES;
                    cell.expriesTextLabel.hidden=YES;
                    messagesentdata= [searchResultsAll objectAtIndex:indexPath.row];
                    
                    if ([messagesentdata.to isEqualToString:@"all"] ||[messagesentdata.to isEqualToString:@"All"])
                    {
                        cell.nameTextLabel.text=[dictLang objectForKey:@"All"];
                    }
                    else
                    {
                    cell.nameTextLabel.text=[messagesentdata.to capitalizedString];
                    }
                    
                    if (messagesentdata.maCallType==1)
                    {
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],messagesentdata.groupName];
                        cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],messagesentdata.storeName];
                    }
                    else
                    {
                        if (messagesentdata.callType==110 || messageinboxdata.callType==102)
                        {
                            cell.storenameLabel.text=@"ASMessage";
                            cell.groupnameLabel.hidden=YES;
                        }
                        else
                        {
                            cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],messagesentdata.groupNames];
                            if (self.tableView.tag==3)
                            {
                                cell.groupnameLabel.text=@"";
                            }
                            
                            if ([messagesentdata.nodeName isEqualToString:@""])
                            {
                                if ([messagesentdata.storeNames isEqualToString:@""])
                                {
                                    cell.storenameLabel.hidden=YES;
                                }
                               cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],messagesentdata.storeNames];
                                if (self.tableView.tag==3)
                                {
                                cell.storenameLabel.text=@"";
                                }
                            }
                            else
                            {
                                
                               cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@,%@",[dictLang objectForKey:@"Store"],messagesentdata.nodeName,messagesentdata.storeNames];
                                if (self.tableView.tag==3)
                                {
                                cell.storenameLabel.text=@"";
                                }
                            }
                        }
                    }
                    if (messagesentdata.callType==108)
                    {
                        cell.storenameLabel.hidden=YES;
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"ASGroup"],messagesentdata.groupName];
                        
                    }
                    UIFont *font = [UIFont fontWithName:@"Helvetica" size:13];
                    NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                                     NSForegroundColorAttributeName: [UIColor blackColor]};
                    const CGSize textSize = [messagesentdata.groupNames sizeWithAttributes: userAttributes];
                    CGRect      buttonFrame = cell.groupnameLabel.frame;
                    buttonFrame.size = CGSizeMake(textSize.width+20, 30);
                    cell.groupnameLabel.frame=buttonFrame;
                    [self loadViewIfNeeded];
                    cell.sentTextLabel.text= [NSDateClass dateformate:messagesentdata.callStartTime];
                    cell.expriesTextLabel.text=[NSDateClass ttlformate:messagesentdata.ttl];
                    cell.lengthTextLabel.text=[NSDateClass lengthformate:messagesentdata.duration];
                    [cell.nameTextButton setTitle:[[messagesentdata.to substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                }
            }
            else if (self.tableView.tag==7)
                
            {
                cell.groupnameLabel.hidden=YES;
                
                if (searchResultsAll.count>indexPath.row)
                {
                    cell.expriesLabel.hidden=NO;
                    cell.expriesTextLabel.hidden=NO;
                    announcementinboxdata = [searchResultsAll objectAtIndex:indexPath.row];
                    
                    if (![announcementinboxdata.status isEqualToString:@"new"])
                    {
                        [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [cell.nameTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
                    }
                    
                    if ([announcementinboxdata.from isEqualToString:@"All"])
                    {
                        cell.nameTextLabel.text=[dictLang objectForKey:@"All"];
                    }
                    else
                    {
                    cell.nameTextLabel.text=[announcementinboxdata.from capitalizedString];
                    }
                    if (announcementinboxdata.maCallType==1)
                    {
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],announcementinboxdata.groupName];
                        cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],announcementinboxdata.storeName];
                        cell.groupnameLabel.hidden=YES;
                        
                        
                    }
                    else
                    {
                        if ([announcementinboxdata.groupNames isEqualToString:@""])
                        {
                            cell.groupnameLabel.hidden=YES;
                        }
                        else
                        {
                            cell.groupnameLabel.hidden=NO;
                        }
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],announcementinboxdata.groupNames];
                        if ([announcementinboxdata.nodeName isEqualToString:@""])
                        {
                            cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],announcementinboxdata.storeNames];
                        }
                        else
                        {
                            
                            cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@,%@",[dictLang objectForKey:@"Store"],announcementinboxdata.nodeName,announcementinboxdata.storeNames];
                        }
                        cell.nameTextButton.userInteractionEnabled=YES;
                       // cell.groupnameLabel.hidden=NO;
                    }
                    
                    cell.sentTextLabel.text= [NSDateClass dateformate:announcementinboxdata.callStartTime];
                    cell.expriesTextLabel.text=[NSDateClass ttlformate:announcementinboxdata.ttl];
                    cell.lengthTextLabel.text=[NSDateClass lengthformate:announcementinboxdata.duration];
                    [cell.nameTextButton setTitle:[[announcementinboxdata.from substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                }
            }
            else if (self.tableView.tag==8)
                
            {
                cell.groupnameLabel.hidden=YES;
                if (searchResultsAll.count>indexPath.row)
                {
                    cell.expriesLabel.hidden=YES;
                    cell.expriesTextLabel.hidden=YES;
                    announcementsaveddata= [searchResultsAll objectAtIndex:indexPath.row];
                    
                    if (![announcementsaveddata.status isEqualToString:@"new"])
                    {
                        [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [cell.nameTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
                    }
                    if ([announcementsaveddata.from isEqualToString:@"All"])
                    {
                        cell.nameTextLabel.text=[dictLang objectForKey:@"All"];
                    }
                    else
                    {
                    cell.nameTextLabel.text=[announcementsaveddata.from capitalizedString];
                    }
                    if (announcementsaveddata.maCallType==1)
                    {
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],announcementsaveddata.groupName];
                        cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],announcementsaveddata.storeName];
                        cell.groupnameLabel.hidden=YES;
                    }
                    else
                    {
                        if ([announcementsaveddata.groupNames isEqualToString:@""])
                        {
                            cell.groupnameLabel.hidden=YES;
                        }
                        else
                        {
                            cell.groupnameLabel.hidden=NO;
                        }
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],announcementsaveddata.groupNames];
                        if ([announcementsaveddata.nodeName isEqualToString:@""])
                        {
                            cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],announcementsaveddata.storeNames];
                        }
                        else
                        {
                            
                            cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@,%@",[dictLang objectForKey:@"Store"],announcementsaveddata.nodeName,announcementsaveddata.storeNames];
                        }
                        cell.nameTextButton.userInteractionEnabled=YES;
                        //cell.groupnameLabel.hidden=NO;
                    }
                    
                    cell.sentTextLabel.text= [NSDateClass dateformate:announcementsaveddata.callStartTime];
                    cell.expriesTextLabel.text=[NSDateClass ttlformate:announcementsaveddata.ttl];
                    cell.lengthTextLabel.text=[NSDateClass lengthformate:announcementsaveddata.duration];
                    [cell.nameTextButton setTitle:[[announcementsaveddata.from substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                }
            }
            else if (self.tableView.tag==9)
            {
                cell.groupnameLabel.hidden=YES;
                cell.contentView.backgroundColor = [UIColor whiteColor];
                if (searchResultsAll.count>indexPath.row)
                {
                    cell.expriesLabel.hidden=YES;
                    cell.expriesTextLabel.hidden=YES;
                    announcementsentdata = [searchResultsAll objectAtIndex:indexPath.row];
                    
                    if ([announcementsentdata.to isEqualToString:@"all"] ||[announcementsentdata.to isEqualToString:@"All"])
                    {
                        cell.nameTextLabel.text=[dictLang objectForKey:@"All"];
                    }
                    else
                    {
                    cell.nameTextLabel.text=[announcementsentdata.to capitalizedString];
                    }
                    if (announcementsentdata.maCallType==1)
                    {
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],announcementsentdata.groupName];
                        cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],announcementsentdata.storeName];
                        cell.groupnameLabel.hidden=YES;
                    }
                    else
                    {
                        if ([announcementsentdata.groupNames isEqualToString:@""])
                        {
                            cell.groupnameLabel.hidden=YES;
                        }
                        else
                        {
                            cell.groupnameLabel.hidden=NO;
                        }
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],announcementsentdata.groupNames];
                        //cell.groupnameLabel.text=@"";
                        if ([announcementsentdata.nodeName isEqualToString:@""])
                        {
                            cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],announcementsentdata.storeNames];
                            //cell.storenameLabel.text=@"";
                        }
                        else
                        {
                            
                            cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@,%@",[dictLang objectForKey:@"Store"],announcementsentdata.nodeName,announcementsentdata.storeNames];
                            //cell.storenameLabel.text=@"";
                        }
                        cell.nameTextButton.userInteractionEnabled=YES;
                        //cell.groupnameLabel.hidden=NO;
                    }
                    
                    
                    cell.sentTextLabel.text= [NSDateClass dateformate:announcementsentdata.callStartTime];
                    cell.expriesTextLabel.text=[NSDateClass ttlformate:announcementsentdata.ttl];
                    cell.lengthTextLabel.text=[NSDateClass lengthformate:announcementsentdata.duration];
                    [cell.nameTextButton setTitle:@"A" forState:UIControlStateNormal];
                }
            }
        }
        else
        {
            if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
            {
                if (self.tableView.tag==1 || self.tableView.tag==10)
                {
                    cell.groupnameLabel.hidden=YES;
                }
                else
                {
                    cell.nameTextButton.userInteractionEnabled=YES;
                    cell.groupnameLabel.hidden=NO;
                }
                if (inboxArray.count>indexPath.row)
                {
                    messageinboxdata= [inboxArray objectAtIndex:indexPath.row];
                    cell.replyimage.hidden=NO;
                    cell.expriesLabel.hidden=NO;
                    cell.expriesTextLabel.hidden=NO;
                    if (![messageinboxdata.status isEqualToString:@"new"])
                    {
                        [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [cell.nameTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
                    }
                    
                    if ([messageinboxdata.reply isEqualToString:@"1"])
                    {
                        cell.replyimage.image=[UIImage imageNamed:@"Reply"];
                    }
                    else
                    {
                        cell.replyimage.image=[UIImage imageNamed:@""];
                    }
                    if ([messageinboxdata.from isEqualToString:@"All"])
                    {
                        cell.nameTextLabel.text=[dictLang objectForKey:@"All"];
                    }
                    else
                    {
                    cell.nameTextLabel.text=[messageinboxdata.from capitalizedString];
                    }
                    
                    if (messageinboxdata.maCallType==1)
                    {
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],messageinboxdata.to];
                        cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],messageinboxdata.storeName];
                    }
                    else
                    {
                        if (messageinboxdata.callType==102 || messageinboxdata.callType==110)
                        {
                            cell.groupnameLabel.hidden=YES;
                            cell.storenameLabel.text=@"ASMessage";
                        }
                        else
                        {
                            cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],messageinboxdata.groupNames];
                            if ([messageinboxdata.nodeName isEqualToString:@""])
                            {
                                NSString *str=messageinboxdata.storeNames;
                                if ([str isEqualToString:@"All"])
                                {
                                    cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],[dictLang objectForKey:@"All"]];
                                }
                                else
                                {
                                cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],messageinboxdata.storeNames];
                                }
                            }
                            else
                            {
                                
                                cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@,%@",[dictLang objectForKey:@"Store"],messageinboxdata.nodeName,messageinboxdata.storeNames];
                            }
                        }
                    }
                    if (messageinboxdata.maCallType==3 ||messageinboxdata.callType==104)
                    {
                        cell.storenameLabel.hidden=YES;
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"ASGroup"],messageinboxdata.groupName];
                        
                    }
                    cell.sentTextLabel.text= [NSDateClass dateformate:messageinboxdata.callStartTime];
                    cell.expriesTextLabel.text=[NSDateClass ttlformate:messageinboxdata.ttl];
                    cell.lengthTextLabel.text=[NSDateClass lengthformate:messageinboxdata.duration];
                    [cell.nameTextButton setTitle:[[messageinboxdata.from substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                }
            }
            else if (self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
                
            {
                if (self.tableView.tag==2 || self.tableView.tag==11)
                {
                    cell.groupnameLabel.hidden=YES;
                }
                else
                {
                    cell.nameTextButton.userInteractionEnabled=YES;
                    cell.groupnameLabel.hidden=NO;
                }
                if (savedArray.count>indexPath.row)
                {
                    cell.replyimage.hidden=NO;
                    cell.expriesLabel.hidden=YES;
                    cell.expriesTextLabel.hidden=YES;
                    messagesaveddata = [savedArray objectAtIndex:indexPath.row];
                    if (![messagesaveddata.status isEqualToString:@"new"])
                    {
                        [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [cell.nameTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
                    }
                    if ([messagesaveddata.reply isEqualToString:@"1"])
                    {
                        cell.replyimage.image=[UIImage imageNamed:@"Reply"];
                    }
                    else
                    {
                        cell.replyimage.image=[UIImage imageNamed:@""];
                    }
                    
                    if ([messagesaveddata.from isEqualToString:@"All"])
                    {
                        cell.nameTextLabel.text=[dictLang objectForKey:@"All"];
                    }
                    else
                    {
                    cell.nameTextLabel.text=[messagesaveddata.from capitalizedString];
                    }
                    
                    if (messagesaveddata.maCallType==1)
                    {
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],messagesaveddata.to];
                        cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],messagesaveddata.storeName];
                    }
                    else
                    {
                        if (messagesaveddata.callType==102 || messageinboxdata.callType==110)
                        {
                            cell.storenameLabel.text=@"ASMessage";
                            cell.groupnameLabel.hidden=YES;
                        }
                        else
                        {
                            cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],messagesaveddata.groupNames];
                            if ([messagesaveddata.nodeName isEqualToString:@""])
                            {
                                cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],messagesaveddata.storeNames];
                            }
                            else
                            {
                                
                                cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@,%@",[dictLang objectForKey:@"Store"],messagesaveddata.nodeName,messagesaveddata.storeNames];
                            }
                        }
                    }
                    if (messagesaveddata.maCallType==3 ||messagesaveddata.callType==104)
                    {
                        cell.storenameLabel.hidden=YES;
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"ASGroup"],messagesaveddata.groupName];
                        
                    }
                    
                    cell.sentTextLabel.text= [NSDateClass dateformate:messagesaveddata.callStartTime];
                    cell.expriesTextLabel.text=[NSDateClass ttlformate:messagesaveddata.ttl];
                    cell.lengthTextLabel.text=[NSDateClass lengthformate:messagesaveddata.duration];
                    [cell.nameTextButton setTitle:[[messagesaveddata.from substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                }
            }
            else if (self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
            {
                if (self.tableView.tag==3 || self.tableView.tag==12)
                {
                    cell.groupnameLabel.hidden=YES;
                }
                else
                {
                    cell.nameTextButton.userInteractionEnabled=YES;
                    cell.groupnameLabel.hidden=NO;
                }
                cell.contentView.backgroundColor = [UIColor whiteColor];
                if (sentArray.count>indexPath.row)
                {
                    cell.replyimage.hidden=YES;
                    cell.expriesLabel.hidden=YES;
                    cell.expriesTextLabel.hidden=YES;
                    messagesentdata= [sentArray objectAtIndex:indexPath.row];
                    if ([messagesentdata.to isEqualToString:@"All"] ||[messagesentdata.to isEqualToString:@"all"])
                    {
                        cell.nameTextLabel.text=[dictLang objectForKey:@"All"];
                    }
                    else
                    {
                    cell.nameTextLabel.text=[messagesentdata.to capitalizedString];
                    }
                    
                    if (messagesentdata.maCallType==1)
                    {
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],messagesentdata.groupName];
                        cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],messagesentdata.storeName];
                    }
                    else
                    {
                        if (messagesentdata.callType==110 || messagesentdata.callType==102)
                        {
                            cell.storenameLabel.text=@"ASMessage";
                            cell.groupnameLabel.hidden=YES;
                        }
                        else
                        {
                            cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],messagesentdata.groupNames];
                            if (self.tableView.tag==3)
                            {
                            cell.groupnameLabel.text=@"";
                            }
                            if ([messagesentdata.nodeName isEqualToString:@""])
                            {
                                if ([messagesentdata.storeNames isEqualToString:@""])
                                {
                                    cell.storenameLabel.hidden=YES;
                                }
                               cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],messagesentdata.storeNames];
                                if (self.tableView.tag==3)
                                {
                                cell.storenameLabel.text=@"";
                                }
                            }
                            else
                            {
                                
                                cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@,%@",[dictLang objectForKey:@"Store"],messagesentdata.nodeName,messagesentdata.storeNames];
                                if (self.tableView.tag==3)
                                {
                                cell.storenameLabel.text=@"";
                                }
                            }
                        }
                    }
                    if (messagesentdata.callType==108)
                    {
                        cell.storenameLabel.hidden=YES;
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"ASGroup"],messagesentdata.groupName];
                        
                    }
                    UIFont *font = [UIFont fontWithName:@"Helvetica" size:13];
                    NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                                     NSForegroundColorAttributeName: [UIColor blackColor]};
                    const CGSize textSize = [messagesentdata.groupNames sizeWithAttributes: userAttributes];
                    CGRect      buttonFrame = cell.groupnameLabel.frame;
                    buttonFrame.size = CGSizeMake(textSize.width+20, 30);
                    cell.groupnameLabel.frame=buttonFrame;
                    [self loadViewIfNeeded];
                    cell.sentTextLabel.text= [NSDateClass dateformate:messagesentdata.callStartTime];
                    cell.expriesTextLabel.text=[NSDateClass ttlformate:messagesentdata.ttl];
                    cell.lengthTextLabel.text=[NSDateClass lengthformate:messagesentdata.duration];
                    [cell.nameTextButton setTitle:[[messagesentdata.to substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                }
            }
            else if (self.tableView.tag==7)
                
            {
                cell.groupnameLabel.hidden=YES;
                
                if (inboxArray.count>indexPath.row)
                {
                    cell.expriesLabel.hidden=NO;
                    cell.expriesTextLabel.hidden=NO;
                    announcementinboxdata = [inboxArray objectAtIndex:indexPath.row];
                    
                    if (![announcementinboxdata.status isEqualToString:@"new"])
                    {
                        [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [cell.nameTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
                    }
                    
                    if ([announcementinboxdata.from isEqualToString:@"All"])
                    {
                        cell.nameTextLabel.text=[dictLang objectForKey:@"All"];
                    }
                    else
                    {
                    cell.nameTextLabel.text=[announcementinboxdata.from capitalizedString];
                    }
                    if (announcementinboxdata.maCallType==1)
                    {
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],announcementinboxdata.groupName];
                        cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],announcementinboxdata.storeName];
                        cell.groupnameLabel.hidden=YES;
                        
                        
                    }
                    else
                    {
                        if ([announcementinboxdata.groupNames isEqualToString:@""])
                        {
                            cell.groupnameLabel.hidden=YES;
                        }
                        else
                        {
                            cell.groupnameLabel.hidden=NO;
                        }
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],announcementinboxdata.groupNames];
                        if ([announcementinboxdata.nodeName isEqualToString:@""])
                        {
                            cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],announcementinboxdata.storeNames];
                        }
                        else
                        {
                            
                            cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@,%@",[dictLang objectForKey:@"Store"],announcementinboxdata.nodeName,announcementinboxdata.storeNames];
                        }
                        cell.nameTextButton.userInteractionEnabled=YES;
                        //cell.groupnameLabel.hidden=NO;
                    }
                    
                    cell.sentTextLabel.text= [NSDateClass dateformate:announcementinboxdata.callStartTime];
                    cell.expriesTextLabel.text=[NSDateClass ttlformate:announcementinboxdata.ttl];
                    cell.lengthTextLabel.text=[NSDateClass lengthformate:announcementinboxdata.duration];
                    [cell.nameTextButton setTitle:[[announcementinboxdata.from substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                }
                
                
            }
            else if (self.tableView.tag==8)
                
            {
                cell.groupnameLabel.hidden=YES;
                if (savedArray.count>indexPath.row)
                {
                    cell.expriesLabel.hidden=YES;
                    cell.expriesTextLabel.hidden=YES;
                    announcementsaveddata= [savedArray objectAtIndex:indexPath.row];
                    
                    if (![announcementsaveddata.status isEqualToString:@"new"])
                    {
                        [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [cell.nameTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
                    }
                    if ([announcementsaveddata.from isEqualToString:@"All"])
                    {
                        cell.nameTextLabel.text=[dictLang objectForKey:@"All"];
                    }
                    else
                    {
                    cell.nameTextLabel.text=[announcementsaveddata.from capitalizedString];
                    }
                    if (announcementsaveddata.maCallType==1)
                    {
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],announcementsaveddata.groupName];
                        cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],announcementsaveddata.storeName];
                        cell.groupnameLabel.hidden=YES;
                    }
                    else
                    {
                        if ([announcementsaveddata.groupNames isEqualToString:@""])
                        {
                            cell.groupnameLabel.hidden=YES;
                        }
                        else
                        {
                            cell.groupnameLabel.hidden=NO;
                        }
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],announcementsaveddata.groupNames];
                        if ([announcementsaveddata.nodeName isEqualToString:@""])
                        {
                            cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],announcementsaveddata.storeNames];
                        }
                        else
                        {
                            
                            cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@,%@",[dictLang objectForKey:@"Store"],announcementsaveddata.nodeName,announcementsaveddata.storeNames];
                        }
                        cell.nameTextButton.userInteractionEnabled=YES;
                        //cell.groupnameLabel.hidden=NO;
                    }
                    
                    cell.sentTextLabel.text= [NSDateClass dateformate:announcementsaveddata.callStartTime];
                    cell.expriesTextLabel.text=[NSDateClass ttlformate:announcementsaveddata.ttl];
                    cell.lengthTextLabel.text=[NSDateClass lengthformate:announcementsaveddata.duration];
                    [cell.nameTextButton setTitle:[[announcementsaveddata.from substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                }
            }
            else if (self.tableView.tag==9)
            {
                cell.groupnameLabel.hidden=YES;
                cell.contentView.backgroundColor = [UIColor whiteColor];
                if (sentArray.count>indexPath.row)
                {
                    cell.expriesLabel.hidden=YES;
                    cell.expriesTextLabel.hidden=YES;
                    announcementsentdata = [sentArray objectAtIndex:indexPath.row];
                    if ([announcementsentdata.to isEqualToString:@"all"] ||[announcementsentdata.to isEqualToString:@"All"])
                    {
                        cell.nameTextLabel.text=[dictLang objectForKey:@"All"];
                    }
                    else
                    {
                    cell.nameTextLabel.text=[announcementsentdata.to capitalizedString];
                    }
                    if (announcementsentdata.maCallType==1)
                    {
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],announcementsentdata.groupName];
                        cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],announcementsentdata.storeName];
                        cell.groupnameLabel.hidden=YES;
                    }
                    else
                    {
                        if ([announcementsentdata.groupNames isEqualToString:@""])
                        {
                            cell.groupnameLabel.hidden=YES;
                        }
                        else
                        {
                            cell.groupnameLabel.hidden=NO;
                        }
                        cell.groupnameLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Group"],announcementsentdata.groupNames];
                        //cell.groupnameLabel.text=@"";
                        if ([announcementsentdata.nodeName isEqualToString:@""])
                        {
                            cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Store"],announcementsentdata.storeNames];
                            //cell.storenameLabel.text=@"";
                        }
                        else
                        {
                            
                            cell.storenameLabel.text=[NSString stringWithFormat:@"%@: %@,%@",[dictLang objectForKey:@"Store"],announcementsentdata.nodeName,announcementsentdata.storeNames];
                            //cell.storenameLabel.text=@"";
                        }
                        cell.nameTextButton.userInteractionEnabled=YES;
                       // cell.groupnameLabel.hidden=NO;
                    }
                    
                    
                    cell.sentTextLabel.text= [NSDateClass dateformate:announcementsentdata.callStartTime];
                    cell.expriesTextLabel.text=[NSDateClass ttlformate:announcementsentdata.ttl];
                    cell.lengthTextLabel.text=[NSDateClass lengthformate:announcementsentdata.duration];
                    [cell.nameTextButton setTitle:@"A" forState:UIControlStateNormal];
                }
            }
        }
        cell.playButton.layer.cornerRadius=8;
        [cell.sendTextButton addTarget:self action:@selector(saveButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.sendTextButton.tag=indexPath.row;
        
        [cell.replyTextButton addTarget:self action:@selector(replybuttonDownPressed:) forControlEvents: UIControlEventTouchDown];
        [cell.replyTextButton addTarget:self action:@selector(replybuttonUpPressed:) forControlEvents: UIControlEventTouchUpInside];
        [cell.replyTextButton addTarget:self action:@selector(replybuttonoutsidePressed:) forControlEvents: UIControlEventTouchDragOutside];
        
        cell.replyTextButton.tag=indexPath.row;
        [cell.deleteTextButton addTarget:self action:@selector(deleteTextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.deleteTextButton.tag=indexPath.row;
        
        [cell.nameTextButton addTarget:self action:@selector(nameTextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.nameTextButton.tag=indexPath.row;
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.playButton.layer.cornerRadius=4.0;
    if (tableView==searchButtonalertTable)
    {
        if (!searchsepctag)
        {
            if ([[alertArray objectAtIndex:indexPath.row] isEqualToString:@"Search by Sender's Name"] ||[[alertArray objectAtIndex:indexPath.row] isEqualToString:@"Par nom d’expéditeur"])
            {
                searchString=@"name";
                searchBar.placeholder=[alertArray objectAtIndex:indexPath.row];
            }
            if ([[alertArray objectAtIndex:indexPath.row] isEqualToString:@"Search by Store's Name"]||[[alertArray objectAtIndex:indexPath.row] isEqualToString:@"Par nom de magasin"])
            {
                searchString=@"store";
                searchBar.placeholder=[alertArray objectAtIndex:indexPath.row];
            }
            
        }
        else
        {
            [self sortingprivate_everyone_group_announcement:[alertArray objectAtIndex:indexPath.row]];
        }
        [self cancelButtonAction];
    }
    else if (tableView==nameButtonalertTable)
    {
    }
    else
    {
        if ([indexPath compare:expandedIndexPath] == NSOrderedSame) {
            expandedIndexPath = nil;
        } else {
            expandedIndexPath = indexPath;
        }
        deselected=NO;
        
        
        [self.tableView beginUpdates];
        if (searchsepctag || searchTag)
        {
            if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
            {
                freshReplyList=searchResultsAll;
                messageinboxdata=[searchResultsAll objectAtIndex:indexPath.row];
                selectedCallId=messageinboxdata.callId;

                [[NSUserDefaults standardUserDefaults] setObject:messageinboxdata.pushTxId forKey:@"call_ReplyID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else if(self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
            {
                messagesaveddata=[searchResultsAll objectAtIndex:indexPath.row];
                selectedCallId=messagesaveddata.callId;
                [[NSUserDefaults standardUserDefaults] setObject:messagesaveddata.pushTxId forKey:@"call_ReplyID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            else if(self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
            {
                messagesentdata=[searchResultsAll objectAtIndex:indexPath.row];
                selectedCallId=messagesentdata.callId;
                [[NSUserDefaults standardUserDefaults] setObject:messagesentdata.callId forKey:@"call_ReplyID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            else if(self.tableView.tag==7)
            {
                announcementsentdata=[searchResultsAll objectAtIndex:indexPath.row];
                selectedCallId=announcementsentdata.callId;
                [[NSUserDefaults standardUserDefaults] setObject:announcementsentdata.callId forKey:@"call_ReplyID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            isRowClosed=NO;
            cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.contentView.backgroundColor = [UIColor whiteColor];
            indexpathcell=indexPath;
            rowindex=indexPath.row;
            [cell.replyTextButton setTitle:[dictLang objectForKey:@"Reply"] forState:UIControlStateNormal];
            [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                             forState:UIControlStateNormal];
            self.isPaused=FALSE;
            if (self.tableView.tag==1 || self.tableView.tag==2 || self.tableView.tag==3 || self.tableView.tag==4 || self.tableView.tag==5 || self.tableView.tag==6 || self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9 || self.tableView.tag==10 || self.tableView.tag==11 || self.tableView.tag==12)
            {
                cell.replyTextButton.userInteractionEnabled=YES;
                cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
                {
                    cell.sendTextButton.userInteractionEnabled=YES;
                    cell.sendTextButton.backgroundColor=kNavBackgroundColor;
                    messageinboxdata = [searchResultsAll objectAtIndex:indexPath.row];
                    selectedCallId=messageinboxdata.callId;
                    // [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxInbox" object:messageinboxdata];
                    audiolength=[NSDateClass lengthformate:messageinboxdata.duration];
                    audioString=[NSString stringWithFormat:@"%@%d%d",messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid];
                    audiologString=[NSString stringWithFormat:@"callId:%@ callType:%d storeid:%d from:%@ callStartTime:%f audiolength:%@",messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid,messageinboxdata.from,messageinboxdata.callStartTime,audiolength];
                    
                    cell.replyTextButton.userInteractionEnabled=YES;
                    cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                    
                }
                else if (self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
                {
                    cell.sendTextButton.userInteractionEnabled=NO;
                    cell.sendTextButton.backgroundColor=[UIColor grayColor];
                    messagesaveddata= [searchResultsAll objectAtIndex:indexPath.row];
                    selectedCallId=messagesaveddata.callId;
                    // [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxSaved" object:messagesaveddata];
                    audiolength=[NSDateClass lengthformate:messagesaveddata.duration];
                    audioString=[NSString stringWithFormat:@"%@%d%d",messagesaveddata.callId,messagesaveddata.callType,messagesaveddata.storeid];
                    audiologString=[NSString stringWithFormat:@"callId:%@ callType:%d storeid:%d from:%@ callStartTime:%f audiolength:%@",messagesaveddata.callId,messagesaveddata.callType,messagesaveddata.storeid,messagesaveddata.from,messagesaveddata.callStartTime,audiolength];
                    
                    cell.replyTextButton.userInteractionEnabled=YES;
                    cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                    
                }
                else if(self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
                {
                    cell.sendTextButton.userInteractionEnabled=NO;
                    cell.sendTextButton.backgroundColor=[UIColor grayColor];
                    [cell.replyTextButton setTitle:[dictLang objectForKey:@"Repost"] forState:UIControlStateNormal];
                    messagesentdata= [searchResultsAll objectAtIndex:indexPath.row];
                    selectedCallId=messagesentdata.callId;
                    // [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxSent" object:messagesentdata];
                    audiolength=[NSDateClass lengthformate:messagesentdata.duration];
                    audioString=[NSString stringWithFormat:@"%@",messagesentdata.callId];
                    audiologString=[NSString stringWithFormat:@"callId:%@ from:%@ callStartTime:%f audiolength:%@",messagesentdata.callId,messagesentdata.to,messagesentdata.callStartTime,audiolength];
                    
                    cell.replyTextButton.userInteractionEnabled=YES;
                    cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                    
                }
            }
            else if(self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9)
            {
                if (self.tableView.tag==7)
                {
                    cell.replyTextButton.userInteractionEnabled=NO;
                    cell.replyTextButton.backgroundColor=[UIColor grayColor];
                    cell.sendTextButton.userInteractionEnabled=YES;
                    cell.sendTextButton.backgroundColor=kNavBackgroundColor;
                    announcementinboxdata = [searchResultsAll objectAtIndex:indexPath.row];
                    selectedCallId=announcementinboxdata.callId;
                    // [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementInbox" object:announcementinboxdata];
                    audiolength=[NSDateClass lengthformate:announcementinboxdata.duration];
                    audioString=[NSString stringWithFormat:@"%@%d%d",announcementinboxdata.callId,announcementinboxdata.callType,announcementinboxdata.storeid];
                    audiologString=[NSString stringWithFormat:@"callId:%@ callType:%d storeid:%d from:%@ callStartTime:%f audiolength:%@",announcementinboxdata.callId,announcementinboxdata.callType,announcementinboxdata.storeid,announcementinboxdata.from,announcementinboxdata.callStartTime,audiolength];
                    
                }
                else  if (self.tableView.tag==8)
                {
                    cell.replyTextButton.userInteractionEnabled=NO;
                    cell.replyTextButton.backgroundColor=[UIColor grayColor];
                    cell.sendTextButton.userInteractionEnabled=NO;
                    cell.sendTextButton.backgroundColor=[UIColor grayColor];
                    announcementsaveddata = [searchResultsAll objectAtIndex:indexPath.row];
                    selectedCallId=announcementsaveddata.callId;
                    // [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementSaved" object:announcementsaveddata];
                    audiolength=[NSDateClass lengthformate:announcementsaveddata.duration];
                    audioString=[NSString stringWithFormat:@"%@%d%d",announcementsaveddata.callId,announcementsaveddata.callType,announcementsaveddata.storeid];
                    audiologString=[NSString stringWithFormat:@"callId:%@ callType:%d storeid:%d from:%@ callStartTime:%f audiolength:%@",announcementsaveddata.callId,announcementsaveddata.callType,announcementsaveddata.storeid,announcementsaveddata.from,announcementsaveddata.callStartTime,audiolength];
                    
                }
                else if (self.tableView.tag==9)
                {
                    announcementsentdata = [searchResultsAll objectAtIndex:indexPath.row];
                    
                    selectedCallId=announcementsentdata.callId;
                    // [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementSent" object:announcementsentdata];
                    audiolength=[NSDateClass lengthformate:announcementsentdata.duration];
                    if (announcementsentdata.callType==10 || announcementsentdata.callType==16)
                    {
                        cell.replyTextButton.userInteractionEnabled=NO;
                        cell.replyTextButton.backgroundColor=[UIColor grayColor];
                    }
                    else
                    {
                        cell.replyTextButton.userInteractionEnabled=YES;
                        cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                    }
                    cell.sendTextButton.userInteractionEnabled=NO;
                    cell.sendTextButton.backgroundColor=[UIColor grayColor];
                    [cell.replyTextButton setTitle:[dictLang objectForKey:@"Repost"] forState:UIControlStateNormal];
                    
                    audioString=[NSString stringWithFormat:@"%@",announcementsentdata.callId];
                    audiologString=[NSString stringWithFormat:@"callId:%@ from:%@ callStartTime:%f audiolength:%@",announcementsentdata.callId,announcementsentdata.to,announcementsentdata.callStartTime,audiolength];
                }
            }
        }
        else
        {
            if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
            {
                freshReplyList=inboxArray;
                messageinboxdata=[inboxArray objectAtIndex:indexPath.row];
                selectedCallId=messageinboxdata.callId;
                [[NSUserDefaults standardUserDefaults] setObject:messageinboxdata.pushTxId forKey:@"call_ReplyID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else if(self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
            {
                messagesaveddata=[savedArray objectAtIndex:indexPath.row];
                selectedCallId=messagesaveddata.callId;
                [[NSUserDefaults standardUserDefaults] setObject:messagesaveddata.pushTxId forKey:@"call_ReplyID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            else if(self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
            {
                messagesentdata=[sentArray objectAtIndex:indexPath.row];
                selectedCallId=messagesentdata.callId;
                [[NSUserDefaults standardUserDefaults] setObject:messagesentdata.callId forKey:@"call_ReplyID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            else if(self.tableView.tag==9)
            {
                announcementsentdata=[sentArray objectAtIndex:indexPath.row];
                selectedCallId=announcementsentdata.callId;
                [[NSUserDefaults standardUserDefaults] setObject:announcementsentdata.callId forKey:@"call_ReplyID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            isRowClosed=NO;
            cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.contentView.backgroundColor = [UIColor whiteColor];
           // cell.circularProgressView.progressTintColor=kNavBackgroundColor;
            indexpathcell=indexPath;
            rowindex=indexPath.row;
            [cell.replyTextButton setTitle:[dictLang objectForKey:@"Reply"] forState:UIControlStateNormal];
            //[cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
            // forState:UIControlStateNormal];
            self.isPaused=FALSE;
            if (self.tableView.tag==1 || self.tableView.tag==2 || self.tableView.tag==3 || self.tableView.tag==4 || self.tableView.tag==5 || self.tableView.tag==6 || self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9 || self.tableView.tag==10 || self.tableView.tag==11 || self.tableView.tag==12)
            {
                cell.replyTextButton.userInteractionEnabled=YES;
                cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
                {
                    cell.sendTextButton.userInteractionEnabled=YES;
                    cell.sendTextButton.backgroundColor=kNavBackgroundColor;
                    messageinboxdata = [inboxArray objectAtIndex:indexPath.row];
                    selectedCallId=messageinboxdata.callId;
                    //   [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxInbox" object:messageinboxdata];
                    audiolength=[NSDateClass lengthformate:messageinboxdata.duration];
                    audioString=[NSString stringWithFormat:@"%@%d%d",messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid];
                    audiologString=[NSString stringWithFormat:@"callId:%@ callType:%d storeid:%d from:%@ callStartTime:%f audiolength:%@",messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid,messageinboxdata.from,messageinboxdata.callStartTime,audiolength];
                    cell.replyTextButton.userInteractionEnabled=YES;
                    cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                    
                }
                else if (self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
                {
                    cell.sendTextButton.userInteractionEnabled=NO;
                    cell.sendTextButton.backgroundColor=[UIColor grayColor];
                    messagesaveddata= [savedArray objectAtIndex:indexPath.row];
                    selectedCallId=messagesaveddata.callId;
                    //  [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxSaved" object:messagesaveddata];
                    audiolength=[NSDateClass lengthformate:messagesaveddata.duration];
                    audioString=[NSString stringWithFormat:@"%@%d%d",messagesaveddata.callId,messagesaveddata.callType,messagesaveddata.storeid];
                    audiologString=[NSString stringWithFormat:@"callId:%@ callType:%d storeid:%d from:%@ callStartTime:%f audiolength:%@",messagesaveddata.callId,messagesaveddata.callType,messagesaveddata.storeid,messagesaveddata.from,messagesaveddata.callStartTime,audiolength];
                    cell.replyTextButton.userInteractionEnabled=YES;
                    cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                    
                }
                else if(self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
                {
                    cell.sendTextButton.userInteractionEnabled=NO;
                    cell.sendTextButton.backgroundColor=[UIColor grayColor];
                    [cell.replyTextButton setTitle:[dictLang objectForKey:@"Repost"] forState:UIControlStateNormal];
                    messagesentdata= [sentArray objectAtIndex:indexPath.row];
                    selectedCallId=messagesentdata.callId;
                    //  [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxSent" object:messagesentdata];
                    audiolength=[NSDateClass lengthformate:messagesentdata.duration];
                    audioString=[NSString stringWithFormat:@"%@",messagesentdata.callId];
                    audiologString=[NSString stringWithFormat:@"callId:%@ from:%@ callStartTime:%f audiolength:%@",messagesentdata.callId,messagesentdata.to,messagesentdata.callStartTime,audiolength];
                    cell.replyTextButton.userInteractionEnabled=YES;
                    cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                }
            }
            if (self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9)
                
            {
                if (self.tableView.tag==7)
                {
                    cell.replyTextButton.userInteractionEnabled=NO;
                    cell.replyTextButton.backgroundColor=[UIColor grayColor];
                    
                    cell.sendTextButton.userInteractionEnabled=YES;
                    cell.sendTextButton.backgroundColor=kNavBackgroundColor;
                    announcementinboxdata = [inboxArray objectAtIndex:indexPath.row];
                    selectedCallId=announcementinboxdata.callId;
                    //  [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementInbox" object:announcementinboxdata];
                    audiolength=[NSDateClass lengthformate:announcementinboxdata.duration];
                    audioString=[NSString stringWithFormat:@"%@%d%d",announcementinboxdata.callId,announcementinboxdata.callType,announcementinboxdata.storeid];
                    audiologString=[NSString stringWithFormat:@"callId:%@ callType:%d storeid:%d from:%@ callStartTime:%f audiolength:%@",announcementinboxdata.callId,announcementinboxdata.callType,announcementinboxdata.storeid,announcementinboxdata.from,announcementinboxdata.callStartTime,audiolength];
                    
                }
                else  if (self.tableView.tag==8)
                {
                    cell.replyTextButton.userInteractionEnabled=NO;
                    cell.replyTextButton.backgroundColor=[UIColor grayColor];
                    cell.sendTextButton.userInteractionEnabled=NO;
                    cell.sendTextButton.backgroundColor=[UIColor grayColor];
                    announcementsaveddata = [savedArray objectAtIndex:indexPath.row];
                    selectedCallId=announcementsaveddata.callId;
                    //  [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementSaved" object:announcementsaveddata];
                    audiolength=[NSDateClass lengthformate:announcementsaveddata.duration];
                    audioString=[NSString stringWithFormat:@"%@%d%d",announcementsaveddata.callId,announcementsaveddata.callType,announcementsaveddata.storeid];
                    audiologString=[NSString stringWithFormat:@"callId:%@ callType:%d storeid:%d from:%@ callStartTime:%f audiolength:%@",announcementsaveddata.callId,announcementsaveddata.callType,announcementsaveddata.storeid,announcementsaveddata.from,announcementsaveddata.callStartTime,audiolength];
                    
                }
                else if (self.tableView.tag==9)
                {
                    announcementsentdata = [sentArray objectAtIndex:indexPath.row];
                    selectedCallId=announcementsentdata.callId;
                    // [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementSent" object:announcementsentdata];
                    audiolength=[NSDateClass lengthformate:announcementsentdata.duration];
                    /* if (announcementsentdata.callType==10 || announcementsentdata.callType==16)
                     {
                     cell.replyTextButton.userInteractionEnabled=NO;
                     cell.replyTextButton.backgroundColor=[UIColor grayColor];
                     }
                     else
                     {
                     cell.replyTextButton.userInteractionEnabled=YES;
                     cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                     }*/
                    cell.replyTextButton.userInteractionEnabled=NO;
                    cell.replyTextButton.backgroundColor=[UIColor grayColor];
                    cell.sendTextButton.userInteractionEnabled=NO;
                    cell.sendTextButton.backgroundColor=[UIColor grayColor];
                    [cell.replyTextButton setTitle:[dictLang objectForKey:@"Repost"] forState:UIControlStateNormal];
                    
                    audioString=[NSString stringWithFormat:@"%@",announcementsentdata.callId];
                    audiologString=[NSString stringWithFormat:@"callId:%@ from:%@ callStartTime:%f audiolength:%@",announcementsentdata.callId,announcementsentdata.to,announcementsentdata.callStartTime,audiolength];
                }
            }
        }
        
        cell.currentTimeSlider.value = 00.00;
        NSError *error;
        
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[[AudioManager getsharedInstance]getEarboxAudioPath:audioString] error:&error];
        self.audioPlayer.delegate=self;
        if (error)
        {
            NSLog(@"Inbox error in audioPlayer: %@",
                  [error localizedDescription]);
        }
        if ([self.audioPlayer duration] >1.0)
        {
            cell.duration.text = audiolength;
            
            cell.playButton.userInteractionEnabled=YES;
            cell.playButton.backgroundColor=kNavBackgroundColor;
            [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                             forState:UIControlStateNormal];
            NSLog(@"Inbox message/announcement taped-This %@ audio file found in Database play button enabled",audiologString);
        }
        else
        {
            cell.playButton.backgroundColor=[UIColor grayColor];
            //cell.playButton.userInteractionEnabled=NO;
            cell.circularProgressView.trackTintColor=[UIColor whiteColor];
            [cell.playButton setImage:[UIImage imageNamed:@"download"]
                             forState:UIControlStateNormal];
            NSLog(@"Inbox message/announcement taped-This %@ audio file not found in Database play button disabled",audiologString);
        }
        [self.tableView endUpdates];
        [self expandrow];
    }
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==nameButtonalertTable)
    {
    }
    else
    {
        if ([indexPath compare:expandedIndexPath] == NSOrderedSame) {
            expandedIndexPath = nil;
        } else {
            expandedIndexPath = indexPath;
        }
        
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
        if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
        {
            if(searchTag || searchsepctag)
            {
                messageinboxdata= [searchResultsAll objectAtIndex:indexPath.row];
            }
            else
            {
                messageinboxdata= [inboxArray objectAtIndex:indexPath.row];
            }
            selectedCallId=messageinboxdata.callId;
            [[NSUserDefaults standardUserDefaults] setObject:messageinboxdata.callId forKey:@"call_ReplyID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if(self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
        {
            if(searchTag || searchsepctag)
            {
                messagesaveddata= [searchResultsAll objectAtIndex:indexPath.row];
            }
            else
            {
                messagesaveddata= [savedArray objectAtIndex:indexPath.row];
            }
            selectedCallId=messagesaveddata.callId;
            [[NSUserDefaults standardUserDefaults] setObject:messagesaveddata.callId forKey:@"call_ReplyID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        
        isRowClosed=YES;
        cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.currentTimeSlider.value = 00.00;
        cell.timeElapsed.text = @"00:00";
        [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                         forState:UIControlStateNormal];
        self.isPaused=FALSE;
        indexpathcell=indexPath;
        [self.audioPlayer pause];
        [self.tableView reloadData];
        deselected=YES;
        [self tableViewUpdate];
    }
}

#pragma mark - sortingprivate_everyone_group_announcement
-(void)sortingprivate_everyone_group_announcement:(NSString *)sortString
{
    inboxsortText=sortString;
    switch (self.tableView.tag) {
        case 1:
        case 4:
        case 10:
            if ([sortString isEqualToString:[dictLang objectForKey:@"Longest Duration"]])
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"duration" ascending:NO];
                
                [inboxArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Most Recent"]])
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"callStartTime" ascending:NO];
                [inboxArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Expires First"]])
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"ttl" ascending:YES];
                [inboxArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Unread"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                
                [inboxArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Read"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:YES];
                
                [inboxArray sortUsingDescriptors:@[descriptor]];
            }
            [self.tableView reloadData];
            break;
            
        case 2:
        case 5:
        case 11:
            if ([sortString isEqualToString:[dictLang objectForKey:@"Longest Duration"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"duration" ascending:NO];
                [savedArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Most Recent"]])
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"callStartTime" ascending:NO];
                [savedArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Expires First"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"ttl" ascending:YES];
                [savedArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Unread"]])
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                
                [savedArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Read"]])
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:YES];
                
                [savedArray sortUsingDescriptors:@[descriptor]];
            }
            [self.tableView reloadData];
            break;
        case 3:
        case 6:
        case 12:
            if (self.tableView.tag==3)
            {
                [self getmessagesentdata:4];
            }
            else
            {
                [self getmessagesentdata:17];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Longest Duration"]])
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"duration" ascending:NO];
                [sentArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Most Recent"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"callStartTime" ascending:NO];
                [sentArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Expires First"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"ttl" ascending:YES];
                [sentArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Unread"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                
                [sentArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Read"]])
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:YES];
                
                [sentArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:@"One Hour"])
            {
                NSMutableArray *sentmessagefilterArray=[[NSMutableArray alloc]init];
                for (int i=0; i<[sentArray count]; i++)
                {
                    messagesentdata = [sentArray objectAtIndex:i];
                    
                    NSString *dateStr=[NSDateClass dateformatesort:messagesentdata.callStartTime];
                    
                    if ([dateStr isEqualToString:@"1"])
                    {
                        [sentmessagefilterArray addObject:messagesentdata];
                    }
                    
                }
                sentArray=nil;
                sentArray = [sentmessagefilterArray copy];
            }
            if ([sortString isEqualToString:@"One Day"])
            {
                NSMutableArray *sentmessagefilterArray=[[NSMutableArray alloc]init];
                for (int i=0; i<[sentArray count]; i++)
                {
                    messagesentdata = [sentArray objectAtIndex:i];
                    
                    NSString *dateStr=[NSDateClass dateformatesort:messagesentdata.callStartTime];
                    
                    if ([dateStr isEqualToString:@"24"] || [dateStr isEqualToString:@"1"])
                    {
                        [sentmessagefilterArray addObject:messagesentdata];
                    }
                    
                }
                sentArray=nil;
                sentArray = [sentmessagefilterArray copy];
            }
            if ([sortString isEqualToString:@"One Week"])
            {
                [self getmessagesentdata:17];
                NSMutableArray *sentmessagefilterArray=[[NSMutableArray alloc]init];
                for (int i=0; i<[sentArray count]; i++)
                {
                    messagesentdata = [sentArray objectAtIndex:i];
                    
                    NSString *dateStr=[NSDateClass dateformatesort:messagesentdata.callStartTime];
                    
                    if ([dateStr isEqualToString:@"168"] || [dateStr isEqualToString:@"24"] || [dateStr isEqualToString:@"1"])
                    {
                        [sentmessagefilterArray addObject:messagesentdata];
                    }
                    
                }
                sentArray=nil;
                sentArray = [sentmessagefilterArray copy];
            }
            [self.tableView reloadData];
            break;
        case 7:
            if ([sortString isEqualToString:[dictLang objectForKey:@"Longest Duration"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"duration" ascending:NO];
                [inboxArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Most Recent"]])
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"callStartTime" ascending:NO];
                [inboxArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Expires First"]])
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"ttl" ascending:YES];
                [inboxArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Unread"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                
                [inboxArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Read"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:YES];
                
                [inboxArray sortUsingDescriptors:@[descriptor]];
            }
            [self.tableView reloadData];
            break;
        case 8:
            if ([sortString isEqualToString:[dictLang objectForKey:@"Longest Duration"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"duration" ascending:NO];
                [savedArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Most Recent"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"callStartTime" ascending:NO];
                [savedArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Expires First"] ])
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"ttl" ascending:YES];
                [savedArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Unread"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                
                [savedArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Read"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:YES];
                
                [savedArray sortUsingDescriptors:@[descriptor]];
            }
            [self.tableView reloadData];
            break;
        case 9:
            if ([sortString isEqualToString:[dictLang objectForKey:@"Longest Duration"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"duration" ascending:NO];
                [sentArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Most Recent"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"ttl" ascending:NO];
                [sentArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Unread"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                
                [sentArray sortUsingDescriptors:@[descriptor]];
            }
            if ([sortString isEqualToString:[dictLang objectForKey:@"Read"]] )
            {
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:YES];
                
                [sentArray sortUsingDescriptors:@[descriptor]];
            }
            [self.tableView reloadData];
            break;
    }
}


#pragma mark - Nameicon pressed method
-(void)nameTextButtonClicked:(UIButton *)button
{
    
    if ( self.tableView.tag==4 || self.tableView.tag==5 || self.tableView.tag==6 || self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9 || self.tableView.tag==10 || self.tableView.tag==11 || self.tableView.tag==12)
    {
        NSArray *stores,*groups,*districts;
        if (self.tableView.tag==7)
        {
            if (inboxArray.count>0)
            {
                announcementinboxdata=[inboxArray objectAtIndex:button.tag];
                groups = [announcementinboxdata.groupNames componentsSeparatedByString:@","];
                stores = [announcementinboxdata.storeNames componentsSeparatedByString:@","];
                districts = [announcementinboxdata.nodeName componentsSeparatedByString:@","];
            }
        }
        if (self.tableView.tag==8)
        {
            if (savedArray.count>0)
            {
                announcementsaveddata=[savedArray objectAtIndex:button.tag];
                groups = [announcementsaveddata.groupNames componentsSeparatedByString:@","];
                stores = [announcementsaveddata.storeNames componentsSeparatedByString:@","];
                districts = [announcementsaveddata.nodeName componentsSeparatedByString:@","];
            }
        }
        if (self.tableView.tag==9)
        {
            if (sentArray.count>0)
            {
                announcementsentdata=[sentArray objectAtIndex:button.tag];
                groups = [announcementsentdata.groupNames componentsSeparatedByString:@","];
                stores = [announcementsentdata.storeNames componentsSeparatedByString:@","];
                districts = [announcementsentdata.nodeName componentsSeparatedByString:@","];
            }
        }
        if (self.tableView.tag==4)
        {
            if (inboxArray.count>0)
            {
                messageinboxdata=[inboxArray objectAtIndex:button.tag];
                groups = [messageinboxdata.groupNames componentsSeparatedByString:@","];
                stores = [messageinboxdata.storeNames componentsSeparatedByString:@","];
                districts = [messageinboxdata.nodeName componentsSeparatedByString:@","];
            }
        }
        if (self.tableView.tag==5)
        {
            if (savedArray.count>0)
            {
                messagesaveddata=[savedArray objectAtIndex:button.tag];
                groups = [messagesaveddata.groupNames componentsSeparatedByString:@","];
                stores = [messagesaveddata.storeNames componentsSeparatedByString:@","];
                districts = [messagesaveddata.nodeName componentsSeparatedByString:@","];
            }
        }
        if (self.tableView.tag==6)
        {
            if (sentArray.count>0)
            {
                messagesentdata=[sentArray objectAtIndex:button.tag];
                groups = [messagesentdata.groupNames componentsSeparatedByString:@","];
                stores = [messagesentdata.storeNames componentsSeparatedByString:@","];
                districts = [messagesentdata.nodeName componentsSeparatedByString:@","];
            }
        }
        NSString *storesempty = [stores componentsJoinedByString:@" "];
        NSString *groupsempty = [groups componentsJoinedByString:@" "];
        NSString *districtsempty = [districts componentsJoinedByString:@" "];
        if (storesempty.length!=0 || groupsempty.length!=0 || districtsempty.length!=0)
        {
            
            [self nameButtonalert];
            backendView.hidden=NO;
            nameButtonalertTable.delegate=self;
            nameButtonalertTable.dataSource=self;
            if (districtsempty.length!=0) {
                
            groupDict=[[NSDictionary alloc]initWithObjectsAndKeys:stores,@"Stores",groups,@"Groups",districts,@"Districts", nil];
            }
            else if (groupsempty.length!=0)
            {
                groupDict=[[NSDictionary alloc]initWithObjectsAndKeys:stores,@"Stores",groups,@"Groups", nil];
            }
            else
            {
                groupDict=[[NSDictionary alloc]initWithObjectsAndKeys:stores,@"Stores", nil];
            }
            NSInteger height;
            
            height = 90+45*([stores count]+[groups count]+[districts count]);
            
            if (height+40> self.view.frame.size.height)
            {
                height =self.view.frame.size.height-100;
            }
            nameButtonalertTable.frame= CGRectMake(nameButtonalertTable.frame.origin.x, nameButtonalertTable.frame.origin.y, nameButtonalertTable.frame.size.width, height);
            [nameButtonalertTable reloadData];
        }
        
    }
}

-(void)nameButtonalert
{
    backendView=[[UIView alloc]init];
    backendView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    backendView.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    nameButtonalertTable=[[UITableView alloc]init];
    nameButtonalertTable.frame=CGRectMake(20, 50, self.view.frame.size.width-40, 380);
    nameButtonalertTable.layer.cornerRadius=10;
    nameButtonalertTable.layer.borderWidth=2;
    nameButtonalertTable.layer.borderColor=[UIColor lightGrayColor].CGColor;
    cancelButton=[UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame=CGRectMake(self.view.frame.size.width-35, 23, 30, 30);
    [cancelButton setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(namecancelButtonAction) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.backgroundColor=[UIColor whiteColor];
    cancelButton.clipsToBounds = YES;
    cancelButton.layer.cornerRadius = cancelButton.frame.size.width/2.0f;
    cancelButton.layer.borderColor=kNavBackgroundColor.CGColor;
    cancelButton.layer.borderWidth=1.0f;
    [backendView addSubview:cancelButton];
    backendView.hidden=YES;
    nameButtonalertTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [backendView addSubview:nameButtonalertTable];
    [self.view addSubview:backendView];
}

-(void)namecancelButtonAction
{
    [backendView removeFromSuperview];
}

-(void)cancelButtonAction
{
    if(searchbackendView && searchsepctag)
    {
        [backendView removeFromSuperview];
    }
    else
    {
        [searchButtonalertTable removeFromSuperview];
        searchbackendView=NO;
        
    }
    searchsepctag=NO;
    searchTag=NO;
    [searchbutton setEnabled:YES];
    [searchsepcbutton setEnabled:YES];
    
}

#pragma mark - Saveicon pressed method
-(void)saveButtonPressed:(UIButton *)button
{
    expandedIndexPath = nil;
    [self audioPlayerStop];
    if (self.tableView.tag==1  || self.tableView.tag==4 || self.tableView.tag==10)
        
    {
        if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
        {
            messagesaveddata=[inboxArray objectAtIndex:button.tag];
            if(messagesaveddata.fault)
            {
                NSString *screencode;
                if (self.tableView.tag==1)
                {
                    screencode=@"InboxPrivateMsgInboxVC";
                }
                if (self.tableView.tag==10)
                {
                    screencode=@"InboxEveryoneInboxVC";
                }
                if (self.tableView.tag==4)
                {
                    screencode=@"InboxGroupMsgInboxVC";
                }
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you are trying to save is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"UpdatemodeV2"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to save is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                alert.tag=250;
                [alert show];
                
            }
            else
            {
                [earboxinboxmodelStore messageSave:messagesaveddata];
                if (self.tableView.tag==1)
                {
                    savedArray=[store artists:@"EarboxSaved" storeid:storeidString calltype:4];
                }
                else if (self.tableView.tag==10)
                {
                    savedArray=[store artists:@"EarboxSaved" storeid:storeidString calltype:33];
                }
                else if (self.tableView.tag==4)
                {
                    savedArray=[store artists:@"EarboxSaved" storeid:storeidString calltype:17];
                }
                
                messageinboxdata=[inboxArray objectAtIndex:button.tag];
                [self updatemodestatus:messageinboxdata.pushTxId status:@"message" modeId:@"save" tabletag:0];
            }
            
        }
        
    }
    else if (self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9)
    {
        if (self.tableView.tag==7)
        {
            announcementsaveddata=[inboxArray objectAtIndex:button.tag];
            if(announcementsaveddata.fault)
            {
               UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The announcement that you are trying to save is expired."],[[DiagnosticTool sharedManager] generateScreenCode:@"InboxStoreMsgInboxVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"UpdatemodeV2"],[[DiagnosticTool sharedManager]generateErrorCode:@"The announcement that you are trying to save is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                alert.tag=250;
                [alert show];
                
            }
            else
            {
                [earboxinboxmodelStore announcementSave:announcementsaveddata];
                savedArray=[store artists:@"AnnouncementSaved" storeid:storeidString calltype:0];
                announcementinboxdata=[inboxArray objectAtIndex:button.tag];
                [self updatemodestatus:announcementinboxdata.pushTxId status:@"announcement" modeId:@"save" tabletag:0];
            }
            
        }
    }
}

#pragma mark - Updatemodestatus Delete
-(void)updatemodestatus:(NSString *)pushTxId status:(NSString *)type modeId:(NSString *)modeId tabletag:(int)tabletag
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
       [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"InboxVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"UpdatemodeV2"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            NSDictionary *twearboxListdic= [HttpHandler updatemodeV2:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] pushTxId:pushTxId modeId:modeId userScreen:[NSString stringWithFormat:@"%d",tabletag]];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if ([[twearboxListdic objectForKey:@"twsuccessMessage"] isEqualToString:@"success"])
                {
                    expandedIndexPath = nil;
                    //tablecellclose=YES;
                    if ([type isEqualToString:@"message"])
                    {
                        [store deleteEarboxInbox:messageinboxdata];
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Success"] message:[dictLang objectForKey:@"Message saved successfully"] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                        [alert show];
                        [self tableViewUpdate];
                    }
                    else if ([type isEqualToString:@"announcement"])
                    {
                        [store deleteAnnouncementInbox:announcementinboxdata];
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Success"] message:[dictLang objectForKey:@"Announcement saved successfully"] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                        [alert show];
                        [self tableViewUpdate];
                    }
                    else if ([type isEqualToString:@"messageinbox"])
                    {
                        
                        if (tabletag==1)
                        {
                            NSString *heardStatus=messageinboxdata.status;
                            if ([heardStatus isEqualToString:@"new"])
                            {
                                int privateMessageCountdef =[[[NSUserDefaults standardUserDefaults]objectForKey:@"privateMessageCountdef"] intValue];
                                privateMessageCountdef--;
                                if (privateMessageCountdef>0)
                                {
                                    privateMessageCount.hidden=NO;
                                    privateMessageCount.text= [NSString stringWithFormat:@"%d",privateMessageCountdef];
                                    
                                }
                                else
                                {
                                    privateMessageCount.hidden=YES;
                                }
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:privateMessageCountdef] forKey:@"privateMessageCountdef"];
                            }
                        }
                        if (tabletag==4)
                        {
                            NSString *heardStatus=messageinboxdata.status;
                            if ([heardStatus isEqualToString:@"new"])
                            {
                                int groupMessageCountdef= [[[NSUserDefaults standardUserDefaults]objectForKey:@"groupMessageCountdef"] intValue];
                                groupMessageCountdef--;
                                if (groupMessageCountdef>0)
                                {
                                    groupMessageCount.hidden=NO;
                                    groupMessageCount.text= [NSString stringWithFormat:@"%d",groupMessageCountdef];
                                    
                                }
                                else
                                {
                                    groupMessageCount.hidden=YES;
                                }
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:groupMessageCountdef] forKey:@"groupMessageCountdef"];
                            }
                        }
                        if (tabletag==10)
                        {
                            NSString *heardStatus=messageinboxdata.status;
                            if ([heardStatus isEqualToString:@"new"])
                            {
                                int everyoneMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"everyoneMessageCountdef"]intValue];
                                everyoneMessageCountdef--;
                                if (everyoneMessageCountdef>0)
                                {
                                    everyoneMessageCount.hidden=NO;
                                    everyoneMessageCount.text= [NSString stringWithFormat:@"%d",everyoneMessageCountdef];
                                    
                                }
                                else
                                {
                                    everyoneMessageCount.hidden=YES;
                                }
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:everyoneMessageCountdef] forKey:@"everyoneMessageCountdef"];
                            }
                        }
                        
                       
                        messageinboxdata = [[store updateArtist:@"EarboxInbox" callId:selectedCallId] objectAtIndex:0];
                        NSString *callId=[NSString stringWithFormat:@"%@%d%d",messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid];
                        [[AudioManager getsharedInstance]deleteEarboxAudioPath:callId];
                        [store deleteEarboxInbox:messageinboxdata];
                        
                        [self tableViewUpdate];
                        
                    }
                    else if ([type isEqualToString:@"messagesaved"])
                    {
                        
                        if (tabletag==2)
                        {
                            NSString *heardStatus=messagesaveddata.status;
                            if ([heardStatus isEqualToString:@"new"])
                            {
                                int  privateMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey: @"privateMessageCountdef"] intValue];
                                privateMessageCountdef--;
                                if (privateMessageCountdef>0)
                                {
                                    privateMessageCount.hidden=NO;
                                    privateMessageCount.text= [NSString stringWithFormat:@"%d",privateMessageCountdef];
                                    
                                }
                                else
                                {
                                    privateMessageCount.hidden=YES;
                                }
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:privateMessageCountdef] forKey:@"privateMessageCountdef"];
                            }
                            
                        }
                        if (tabletag==5)
                        {
                            NSString *heardStatus=messagesaveddata.status;
                            if ([heardStatus isEqualToString:@"new"])
                            {
                                int groupMessageCountdef= [[[NSUserDefaults standardUserDefaults]objectForKey:@"groupMessageCountdef"] intValue];
                                groupMessageCountdef--;
                                if (groupMessageCountdef>0)
                                {
                                    groupMessageCount.hidden=NO;
                                    groupMessageCount.text= [NSString stringWithFormat:@"%d",groupMessageCountdef];
                                    
                                }
                                else
                                {
                                    groupMessageCount.hidden=YES;
                                }
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:groupMessageCountdef] forKey:@"groupMessageCountdef"];
                            }
                            
                        }
                        if (tabletag==11)
                        {
                            NSString *heardStatus=messagesaveddata.status;
                            if ([heardStatus isEqualToString:@"new"])
                            {
                                int everyoneMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"everyoneMessageCountdef"] intValue];
                                everyoneMessageCountdef--;
                                if (everyoneMessageCountdef>0)
                                {
                                    everyoneMessageCount.hidden=NO;
                                    everyoneMessageCount.text= [NSString stringWithFormat:@"%d",everyoneMessageCountdef];
                                    
                                }
                                else
                                {
                                    everyoneMessageCount.hidden=YES;
                                }
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:everyoneMessageCountdef] forKey:@"everyoneMessageCountdef"];
                            }
                            
                        }
                        
                        messagesaveddata = [[store updateArtist:@"EarboxSaved" callId:selectedCallId] objectAtIndex:0];
                       NSString *callId=[NSString stringWithFormat:@"%@%d%d",messagesaveddata.callId,messagesaveddata.callType,messagesaveddata.storeid];
                        [[AudioManager getsharedInstance]deleteEarboxAudioPath:callId];
                        [store deleteEarboxSaved:messagesaveddata];
                        
                        [self tableViewUpdate];
                    }
                    else if ([type isEqualToString:@"messagesent"])
                    {
                        NSString *callId=[NSString stringWithFormat:@"%@%d%d",messagesentdata.callId,messagesentdata.callType,messagesentdata.storeid ];
                        [[AudioManager getsharedInstance]deleteEarboxAudioPath:callId];
                        [store deleteEarboxSent:messagesentdata];
                        [self tableViewUpdate];
                    }
                    else if ([type isEqualToString:@"announcementinbox"])
                    {
                       
                        if (tabletag==7)
                        {
                            NSString *heardStatus=announcementinboxdata.status;
                            if ([heardStatus isEqualToString:@"new"])
                            {
                                int storeAnnouncementCountdef=[[[NSUserDefaults standardUserDefaults] objectForKey:@"storeAnnouncementCountdef"] intValue];
                                storeAnnouncementCountdef--;
                                if (storeAnnouncementCountdef>0)
                                {
                                    storeAnnouncementCount.hidden=NO;
                                    storeAnnouncementCount.text= [NSString stringWithFormat:@"%d",storeAnnouncementCountdef];
                                    
                                }
                                else
                                {
                                    storeAnnouncementCount.hidden=YES;
                                }
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:storeAnnouncementCountdef ] forKey:@"storeAnnouncementCountdef"];
                            }
                        }
                        
                        announcementinboxdata = [[store updateArtist:@"AnnouncementInbox" callId:selectedCallId] objectAtIndex:0];
                       NSString *callId=[NSString stringWithFormat:@"%@%d%d",announcementinboxdata.callId,announcementinboxdata.callType,announcementinboxdata.storeid ];
                            [[AudioManager getsharedInstance]deleteEarboxAudioPath:callId];
                            [store deleteAnnouncementInbox:announcementinboxdata];
                            
                            [self tableViewUpdate];
                        
//                        [[AudioManager getsharedInstance]deleteEarboxAudioPath:callId];
//                        [store deleteAnnouncementInbox:announcementinboxdata];
//                        [self tableViewUpdate];
                    }
                    else if ([type isEqualToString:@"announcementsaved"])
                    {
                       
                        if (tabletag==8)
                        {
                            NSString *heardStatus=announcementsaveddata.status;
                            if ([heardStatus isEqualToString:@"new"])
                            {
                                int storeAnnouncementCountdef=[[[NSUserDefaults standardUserDefaults] objectForKey:@"storeAnnouncementCountdef"] intValue];
                                storeAnnouncementCountdef--;
                                if (storeAnnouncementCountdef>0)
                                {
                                    storeAnnouncementCount.hidden=NO;
                                    storeAnnouncementCount.text= [NSString stringWithFormat:@"%d",storeAnnouncementCountdef];
                                    
                                }
                                else
                                {
                                    storeAnnouncementCount.hidden=YES;
                                }
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:storeAnnouncementCountdef ] forKey:@"storeAnnouncementCountdef"];
                            }
                            
                        }
                        announcementsaveddata = [[store updateArtist:@"AnnouncementSaved" callId:selectedCallId] objectAtIndex:0];
                         NSString *callId=[NSString stringWithFormat:@"%@%d%d",announcementsaveddata.callId,announcementsaveddata.callType,announcementsaveddata.storeid ];
                        [[AudioManager getsharedInstance]deleteEarboxAudioPath:callId];
                        [store deleteAnnouncementSaved:announcementsaveddata];
                        
                        [self tableViewUpdate];
//                        [[AudioManager getsharedInstance]deleteEarboxAudioPath:callId];
//                        [store deleteAnnouncementSaved:announcementsaveddata];
//                        [self tableViewUpdate];
                    }
                    else if ([type isEqualToString:@"announcementsent"])
                    {
                        NSString *callId=[NSString stringWithFormat:@"%@%d%d",announcementsentdata.callId,announcementsentdata.callType,announcementsentdata.storeid ];
                        [[AudioManager getsharedInstance]deleteEarboxAudioPath:callId];
                        [store deleteAnnouncementSent:announcementsentdata];
                        [self tableViewUpdate];
                    }
                    
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                }
                else
                {
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Please try again after some time"],[[DiagnosticTool sharedManager] generateScreenCode:@"InboxVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"UpdatemodeV2"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please try again after some time"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
        }];
        
    }
    
}

#pragma mark - updateheardstatus
-(void)updateheardstatus:(NSString *)pushTxId status:(NSString *)type modeId:(NSString *)modeId tabletag:(NSInteger)tabletag
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"InboxVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"UpdateheardstatusV2"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            NSDictionary *twearboxListdic= [HttpHandler updateheardstatusV2:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] pushTxId:pushTxId heardStatusId:modeId userScreen:[NSString stringWithFormat:@"%ld",(long)tabletag]];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if (![[twearboxListdic objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
                {
                    if ([type isEqualToString:@"messageinbox"])
                    {
                        if (tabletag==1)
                        {
                            int privateMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"privateMessageCountdef"] intValue];
                            privateMessageCountdef--;
                            if (privateMessageCountdef>0)
                            {
                                privateMessageCount.hidden=NO;
                                privateMessageCount.text= [NSString stringWithFormat:@"%d",privateMessageCountdef];
                                
                            }
                            else
                            {
                                privateMessageCount.hidden=YES;
                            }
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:privateMessageCountdef] forKey:@"privateMessageCountdef"];
                            
                        }
                        if (tabletag==4)
                        {
                            int groupMessageCountdef= [[[NSUserDefaults standardUserDefaults]objectForKey:@"groupMessageCountdef"] intValue];
                            groupMessageCountdef--;
                            if (groupMessageCountdef>0)
                            {
                                groupMessageCount.hidden=NO;
                                groupMessageCount.text= [NSString stringWithFormat:@"%d",groupMessageCountdef];
                                
                            }
                            else
                            {
                                groupMessageCount.hidden=YES;
                            }
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:groupMessageCountdef] forKey:@"groupMessageCountdef"];
                            
                        }
                        if (tabletag==10)
                        {
                            int everyoneMessageCountdef= [[[NSUserDefaults standardUserDefaults]objectForKey:@"everyoneMessageCountdef"] intValue];
                            everyoneMessageCountdef--;
                            if (everyoneMessageCountdef>0)
                            {
                                everyoneMessageCount.hidden=NO;
                                everyoneMessageCount.text= [NSString stringWithFormat:@"%d",everyoneMessageCountdef];
                                
                            }
                            else
                            {
                                everyoneMessageCount.hidden=YES;
                            }
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:everyoneMessageCountdef] forKey:@"everyoneMessageCountdef"];
                            
                        }
                        messageinboxdata = [[store updateArtist:@"EarboxInbox" callId:messageinboxdata.callId] objectAtIndex:0];
                        messageinboxdata.status = @"HEARD";
                        NSError *error=nil;
                        [store save:error];
                        // NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                        // messageArray=[messageArray sortedArrayUsingDescriptors:@[descriptor]];
                        int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                        if (messagecount==0)
                        {
                            messagecount=0;
                        }
                        else
                        {
                            messagecount--;
                        }
                        
                        
                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagetotalcount"];
                        int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    else if ([type isEqualToString:@"messagesaved"])
                    {
                        if (tabletag==2)
                        {
                            int  privateMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"privateMessageCountdef"] intValue];
                            privateMessageCountdef--;
                            if (privateMessageCountdef>0)
                            {
                                privateMessageCount.hidden=NO;
                                privateMessageCount.text= [NSString stringWithFormat:@"%d",privateMessageCountdef];
                                
                            }
                            else
                            {
                                privateMessageCount.hidden=YES;
                            }
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:privateMessageCountdef] forKey:@"privateMessageCountdef"];
                            
                        }
                        if (tabletag==5)
                        {
                            int groupMessageCountdef= [[[NSUserDefaults standardUserDefaults]objectForKey:@"groupMessageCountdef"] intValue];
                            groupMessageCountdef--;
                            if (groupMessageCountdef>0)
                            {
                                groupMessageCount.hidden=NO;
                                
                                groupMessageCount.text= [NSString stringWithFormat:@"%d",groupMessageCountdef];
                                
                            }
                            else
                            {
                                groupMessageCount.hidden=YES;
                            }
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:groupMessageCountdef] forKey:@"groupMessageCountdef"];
                            
                        }
                        if (tabletag==11)
                        {
                            int everyoneMessageCountdef= [[[NSUserDefaults standardUserDefaults]objectForKey:@"everyoneMessageCountdef"] intValue];
                            everyoneMessageCountdef--;
                            if (everyoneMessageCountdef>0)
                            {
                                everyoneMessageCount.hidden=NO;
                                everyoneMessageCount.text= [NSString stringWithFormat:@"%d",everyoneMessageCountdef];
                                
                            }
                            else
                            {
                                everyoneMessageCount.hidden=YES;
                            }
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:everyoneMessageCountdef] forKey:@"everyoneMessageCountdef"];
                        }
                        messagesaveddata = [[store updateArtist:@"EarboxSaved" callId:messagesaveddata.callId] objectAtIndex:0];
                        messagesaveddata.status = @"HEARD";
                        NSError *error;
                        [store save:error];
                        // NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                        // savedArray=[savedArray sortedArrayUsingDescriptors:@[descriptor]];
                        int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                        if (messagecount==0)
                        {
                            messagecount=0;
                        }
                        else
                        {
                            messagecount--;
                        }
                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagetotalcount"];
                        int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    else if ([type isEqualToString:@"messagesent"])
                    {
                    }
                    else if ([type isEqualToString:@"announcementinbox"])
                    {
                        if (tabletag==7)
                        {
                            int storeAnnouncementCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeAnnouncementCountdef"] intValue];
                            storeAnnouncementCountdef--;
                            if (storeAnnouncementCountdef>0)
                            {
                                storeAnnouncementCount.hidden=NO;
                                storeAnnouncementCount.text= [NSString stringWithFormat:@"%d",storeAnnouncementCountdef];
                                
                            }
                            else
                            {
                                storeAnnouncementCount.hidden=YES;
                            }
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:storeAnnouncementCountdef] forKey:@"storeAnnouncementCountdef"];
                            
                        }
                        
                        announcementinboxdata = [[store updateArtist:@"AnnouncementInbox" callId:announcementinboxdata.callId] objectAtIndex:0];
                        announcementinboxdata.status = @"HEARD";
                        NSError *error;
                        [store save:error];
                        //NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                        // inboxArray=[inboxArray sortedArrayUsingDescriptors:@[descriptor]];
                        int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementtotalcount"] intValue];
                        if (announcementcount==0)
                        {
                            announcementcount=0;
                        }
                        else
                        {
                            announcementcount--;
                        }
                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:announcementcount]
                                                                 forKey:@"announcementtotalcount"];
                        int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    else if ([type isEqualToString:@"announcementsaved"])
                    {
                        if (tabletag==8)
                        {
                            int storeAnnouncementCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeAnnouncementCountdef"] intValue];
                            storeAnnouncementCountdef--;
                            if (storeAnnouncementCountdef>0)
                            {
                                storeAnnouncementCount.hidden=NO;
                                storeAnnouncementCount.text= [NSString stringWithFormat:@"%d",storeAnnouncementCountdef];
                                
                            }
                            else
                            {
                                storeAnnouncementCount.hidden=YES;
                            }
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:storeAnnouncementCountdef] forKey:@"storeAnnouncementCountdef"];
                        }
                        
                        announcementsaveddata = [[store updateArtist:@"AnnouncementSaved" callId:announcementsaveddata.callId] objectAtIndex:0];
                        announcementsaveddata.status = @"HEARD";
                        NSError *error;
                        [store save:error];
                        // NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                        // savedArray=[savedArray sortedArrayUsingDescriptors:@[descriptor]];
                        int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementtotalcount"] intValue];
                        if (announcementcount==0)
                        {
                            announcementcount=0;
                        }
                        else
                        {
                            announcementcount--;
                        }
                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:announcementcount]
                                                                 forKey:@"announcementtotalcount"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    else if ([type isEqualToString:@"announcementsent"])
                    {
                        
                    }
                    
                }
                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
            }];
        }];
        
    }
    
}

#pragma mark - Reply pressed method
-(void)replybuttonDownPressed:(UIButton *)button
{
    replayCancelEnable=YES;
    isReplying=YES;
    [self audioPlayerStop];
    if (self.tableView.tag==6 || self.tableView.tag==3 || self.tableView.tag==9 || self.tableView.tag==12)
    {
        [self.view addSubview: [ActivityIndicatorView alphaView:self.view]];
    }
    else
    {
        rootView = [[[NSBundle mainBundle] loadNibNamed:@"ReplayView" owner:self options:nil] objectAtIndex:0];
        [rootView alphaView:self.view];
        rootView.frame = CGRectMake(10, (self.view.frame.size.height/2)-150, self.view.frame.size.width-20, rootView.frame.size.height);
        rootView.layer.cornerRadius=10;
        rootView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:rootView];
        [speakerButton setEnabled:NO];
        [searchbutton setEnabled:NO];
        [searchsepcbutton setEnabled:NO];
        [rootView load];
        if (self.tableView.tag==1 || self.tableView.tag==4)
        {
            replyingTo = [NSString stringWithFormat:@"%@",@"tag1"];
        }
        else if (self.tableView.tag==2 || self.tableView.tag==5)
        {
            replyingTo = [NSString stringWithFormat:@"%@",@"tag2"];
        }
        replyClicked=NO;
    }
}

-(void)animation
{
    [UIView animateWithDuration:0.5 animations:^{
        imagebutton.transform = CGAffineTransformMakeScale(7.21, 7.21);
    }completion:^(BOOL finished){
        // Finished scaling down imageview, now resize it
        [UIView animateWithDuration:0.5 animations:^{
            imagebutton.transform = CGAffineTransformIdentity;
            
        }completion:^(BOOL finished){
            if (replyClicked==YES)
            {
                //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"audioNotification" object:nil];
            }
            else
            {
                [self animationZoom];
            }
        }];
    }];
}

-(void)animationZoom
{
    [UIView animateWithDuration:0.5 animations:^{
        imagebutton.transform = CGAffineTransformMakeScale(7.21, 7.21);
    }completion:^(BOOL finished){
        // Finished scaling down imageview, now resize it
        [UIView animateWithDuration:0.5 animations:^{
            imagebutton.transform = CGAffineTransformIdentity;
            
        }completion:^(BOOL finished){
            if (replyClicked==YES)
            {
                //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"audioNotification" object:nil];
            }
            else
            {
                [self animation];
            }
        }];
    }];
}

-(void)replybuttonUpPressed:(UIButton *)button
{
    isReplying=NO;
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:rowindex inSection:0] ;
    cell = [self.tableView cellForRowAtIndexPath:myIP];
    cell.replyTextButton.userInteractionEnabled=NO;
    cell.replyTextButton.backgroundColor=[UIColor grayColor];
    replyClicked=YES;
    if (self.tableView.tag==6 || self.tableView.tag==3 || self.tableView.tag==9 || self.tableView.tag==12)
    {
        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
        [[AudioManager getsharedInstance] sendTapped];
    }
    else
    {
    }
    
}

-(void)replybuttonoutsidePressed:(UIButton *)button
{
    if (isReplying)
    {
        [self replybuttonUpPressed:nil];
    }
}

-(void)replayCancelNotification:(NSNotification *)sender
{
    [self tableViewUpdate];
    replayCancelEnable=NO;
    [ActivityIndicatorView activityIndicatorViewRemove1:self.view];
    [rootView removeFromSuperview];
    cell.replyTextButton.userInteractionEnabled=YES;
    [speakerButton setEnabled:YES];
    [searchbutton setEnabled:YES];
    [searchsepcbutton setEnabled:YES];
    cell.replyTextButton.backgroundColor=kNavBackgroundColor;
    
}

#pragma mark - Audio send upload method
-(void)sendAnnouncement:(NSNotification *)sender
{
    NSURL *replyAudiourl;
    //Uploading audio to TCM
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ASGroup"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Select"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"MultiStore"];
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"InboxViewController"])
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
           [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"InboxVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
            
        }
        else
        {
            NSURL *audiourl=sender.object;
            
            if (self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
            {
                messagesentdata=[sentArray objectAtIndex:rowindex];
                audiourl=[[AudioManager getsharedInstance]getEarboxAudioPath: [NSString stringWithFormat:@"%@",messagesentdata.callId]];
            }
            else if(self.tableView.tag==9)
            {
                announcementsentdata=[sentArray objectAtIndex:rowindex];
                audiourl=[[AudioManager getsharedInstance]getEarboxAudioPath: [NSString stringWithFormat:@"%@",announcementsentdata.callId]];
            }
            //Checking audio length is less than one sec
            NSString *screencode;
            if ([[AudioManager getsharedInstance]getAudioduration:audiourl]<1.0)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"audiolengthshort" object:nil];
                
                switch (self.tableView.tag) {
                    case 1:
                        screencode=@"InboxPrivateMsgInboxVC";
                        break;
                        
                    case 2:
                        screencode=@"InboxPrivateMsgSavedVC";
                        break;
                    case 3:
                        screencode=@"InboxPrivateMsgSentVC";
                        break;
                    case 10:
                        screencode=@"InboxEveryoneInboxVC";
                        break;
                    case 11:
                        screencode=@"InboxEveryoneSavedVC";
                        break;
                    case 12:
                        screencode=@"InboxEveryoneSentVC";
                        break;
                    case 4:
                        screencode=@"InboxGroupMsgInboxVC";
                        break;
                    case 5:
                        screencode=@"InboxGroupMsgSavedVC";
                        break;
                    case 6:
                        screencode=@"InboxGroupMsgSentVC";
                        break;
                    case 7:
                        screencode=@"InboxStoreMsgInboxVC";
                        break;
                    case 8:
                        screencode=@"InboxStoreMsgSavedVC";
                        break;
                    case 9:
                        screencode=@"InboxStoreMsgSentVC";
                        break;
                    
                }
                [AlertView alert:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Audio length is too short to send"],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"Audio length is too short to send"]]];
                cell.replyTextButton.userInteractionEnabled=YES;
                cell.replyTextButton.backgroundColor=kNavBackgroundColor;
            }
            else
            {
                if (self.tableView.tag==6 || self.tableView.tag==3 || self.tableView.tag==9 || self.tableView.tag==12)
                {
                    [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
                }
                
                
                NSString *callTxId;
                int calltype;
                NSString *millisec=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ];
                NSArray *tempArray = [millisec componentsSeparatedByString:@"."];
                NSString *macId;
                if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
                    macId=[[NSString stringWithFormat:@"%@",
                            [[[[UIDevice currentDevice] identifierForVendor] UUIDString]substringFromIndex:24]]lowercaseString];
                }
                NSData *file1Data;
                if (self.tableView.tag==1 || self.tableView.tag==2 || self.tableView.tag==3 || self.tableView.tag==4 || self.tableView.tag==5 || self.tableView.tag==6 || self.tableView.tag==10 || self.tableView.tag==11 || self.tableView.tag==12)
                {
                    if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
                    {
                        if (self.tableView.tag==1)
                        {
                            [self getFreshInboxList:4];
                        }
                        else if(self.tableView.tag==4)
                        {
                            [self getFreshInboxList:17];
                        }
                        else if(self.tableView.tag==10)
                        {
                            [self getFreshInboxList:33];
                        }
                        
                        NSString *callID_Defaults = [[NSUserDefaults standardUserDefaults] objectForKey:@"call_ReplyID"];
                        
                        BOOL found= NO;
                        
                        for (int i=0; i<[freshInboxList count]; i++)
                        {
                            messageFreshList =[freshInboxList objectAtIndex:i];
                            
                            if ([messageFreshList.pushTxId isEqualToString: callID_Defaults])
                            {
                                found = YES;
                                break;
                            }
                        }
                        if (found)
                        {
                            
                            messageinboxdata=[inboxArray objectAtIndex:rowindex];
                            contactNameString=messageinboxdata.from;
                            // calltype=4;
                            // callTxId=[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"];
                            replyAudiourl=sender.object;
                            file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
                            if (messageinboxdata.maCallType==1)
                            {
                                calltype=4;
                            }
                            else if([messageinboxdata.managerAppId length]!=0)
                            {
                                calltype=110;
                            }
                            else
                            {
                                calltype=104;
                            }
                            self.pnsdic=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",contactNameString,@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:replyAudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithInt:calltype],@"callType",[NSNumber numberWithDouble:0],@"ttl",messageinboxdata.pushTxId,@"pushTxnId", nil];
                            NSDictionary *whoDic;
                            if (self.tableView.tag==1 || self.tableView.tag==10)
                            {
                                if (messageinboxdata.maCallType==1)
                                {
                                    whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[NSArray arrayWithObject:messageinboxdata.storeName],@"store", [NSArray arrayWithObject:[apploginDetails objectForKey:@"chainName"]],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[tempArray objectAtIndex:0],@"request_time",@[@""],@"groups",nil];
                                }
                                else if (messageinboxdata.maCallType==3)
                                {
                                    [[NSUserDefaults standardUserDefaults] setObject:@"ASGroup" forKey:@"ASGroup"];
                                    whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[apploginDetails objectForKey:@"chainName"],@"chain",@[contactNameString],@"users",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",[messageinboxdata.groupIds componentsSeparatedByString:@","],@"groups",@"msg_quickreply",@"featureType",nil];
                                }
                                else
                                {
                                    if(messageinboxdata.storeid==0)
                                    {
                                        if (messageinboxdata.callType==102 || messageinboxdata.callType==110)
                                        {
                                            [[NSUserDefaults standardUserDefaults] setObject:@"HQ" forKey:@"Select"];
                                            whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",millisec,@"request_time",@[messageinboxdata.managerAppId],@"users",@"msg_quickreply",@"featureType",nil];
                                        }
                                        else
                                        {
                                            whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",@[],@"groups",@[],@"store",@"msg_quickreply",@"featureType",nil];
                                        }
                                    }
                                    else
                                    {
                                        whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",@[],@"groups",@[[NSNumber numberWithInt:messageinboxdata.storeid]],@"store",@"msg_quickreply",@"featureType",nil];
                                    }
                                    
                                }
                            }
                            else
                            {
                                if (messageinboxdata.maCallType==3)
                                {
                                    [[NSUserDefaults standardUserDefaults] setObject:@"ASGroup" forKey:@"ASGroup"];
                                    whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[apploginDetails objectForKey:@"chainName"],@"chain",@[contactNameString],@"users",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",[messageinboxdata.groupIds componentsSeparatedByString:@","],@"groups",@"msg_quickreply",@"featureType",nil];
                                }
                                else if (messageinboxdata.maCallType==1)
                                {
                                    whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[NSArray arrayWithObject:messageinboxdata.storeName],@"store", [NSArray arrayWithObject:[apploginDetails objectForKey:@"chainName"]],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[tempArray objectAtIndex:0],@"request_time",@[@""],@"groups",nil];
                                }
                                else
                                {
                                    if(messageinboxdata.storeid==0)
                                    {
                                        whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",@[],@"groups",@[],@"store",@"msg_quickreply",@"featureType",nil];
                                    }
                                    else
                                    {
                                        whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",@[],@"groups",@[[NSNumber numberWithInt:messageinboxdata.storeid]],@"store",@"msg_quickreply",@"featureType",nil];
                                        
                                    }
                                    
                                }
                            }
                            [HttpHandler audioFileSend:whoDic audioFile:file1Data pns: self.pnsdic userScreen:[NSString stringWithFormat:@"%ld",(long)self.tableView.tag]];
                        }
                        else
                        {
                            if (self.tableView.tag==1)
                            {
                                [self getmessageinboxdata:4];
                            }
                            else if(self.tableView.tag==4)
                            {
                                [self getmessageinboxdata:17];
                            }
                            else if(self.tableView.tag==10)
                            {
                                [self getmessageinboxdata:33];
                            }
                            [self.tableView reloadData];
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you have just replied is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you have just replied is expired"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                            alert.tag=300;
                            [alert show];
                            [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
                        }
                        
                    }
                    else if (self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
                    {
                        if (self.tableView.tag==2)
                        {
                            [self getmessagesaveddata:4];
                        }
                        else if(self.tableView.tag==5)
                        {
                            [self getmessagesaveddata:17];
                        }
                        else if(self.tableView.tag==11)
                        {
                            [self getmessagesaveddata:33];
                        }
                        messagesaveddata=[savedArray objectAtIndex:rowindex];
                        contactNameString=messagesaveddata.from;
                        //calltype=4;
                        //callTxId=[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"];
                        replyAudiourl=sender.object;
                        file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
                        if (messagesaveddata.maCallType==1)
                        {
                            calltype=4;
                        }
                        else if([messagesaveddata.managerAppId length]!=0)
                        {
                            calltype=110;
                        }
                        else
                        {
                            calltype=104;
                        }
                        self.pnsdic=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",contactNameString,@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:replyAudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithInt:calltype],@"callType",[NSNumber numberWithDouble:0],@"ttl", messagesaveddata.pushTxId,@"pushTxnId",nil];
                        
                        NSDictionary *whoDic;
                        if (self.tableView.tag==2 || self.tableView.tag==11)
                        {
                            if (messagesaveddata.maCallType==1)
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[NSArray arrayWithObject:messagesaveddata.storeName],@"store", [NSArray arrayWithObject:[apploginDetails objectForKey:@"chainName"]],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[tempArray objectAtIndex:0],@"request_time",@[@""],@"groups",nil];
                            }
                            else if(messagesaveddata.maCallType==3)
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:@"ASGroup" forKey:@"ASGroup"];
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[apploginDetails objectForKey:@"chainName"],@"chain",@[contactNameString],@"users",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",[messagesaveddata.groupIds componentsSeparatedByString:@","],@"groups",@"msg_quickreply",@"featureType",nil];
                            }
                            else
                            {
                                if(messagesaveddata.storeid==0)
                                {
                                    if (messagesaveddata.callType==102 || messagesaveddata.callType==110)
                                    {
                                        [[NSUserDefaults standardUserDefaults] setObject:@"HQ" forKey:@"Select"];
                                        whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",millisec,@"request_time",@[messageinboxdata.managerAppId],@"users",@"msg_quickreply",@"featureType",nil];
                                    }
                                    else
                                    {
                                        whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",@[],@"groups",@[],@"store",@"msg_quickreply",@"featureType",nil];
                                    }
                                }
                                else
                                {
                                    whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",@[],@"groups",@[[NSNumber numberWithInt:messagesaveddata.storeid]],@"store",@"msg_quickreply",@"featureType",nil];
                                }
                                
                            }
                        }
                        else
                        {
                            if (messagesaveddata.maCallType==3)
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:@"ASGroup" forKey:@"ASGroup"];
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[apploginDetails objectForKey:@"chainName"],@"chain",@[contactNameString],@"users",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",[messagesaveddata.groupIds componentsSeparatedByString:@","],@"groups",@"msg_quickreply",@"featureType",nil];
                            }
                            else if (messagesaveddata.maCallType==1)
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[NSArray arrayWithObject:messagesaveddata.storeName],@"store", [NSArray arrayWithObject:[apploginDetails objectForKey:@"chainName"]],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[tempArray objectAtIndex:0],@"request_time",@[@""],@"groups",nil];
                            }
                            else
                            {
                                if (messagesaveddata.storeid==0)
                                {
                                    whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",@[],@"groups",@[],@"store",@"msg_quickreply",@"featureType",nil];
                                }
                                else
                                {
                                    if (messagesaveddata.storeIds!=0)
                                    {
                                        whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[@"all"],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",millisec,@"request_time",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groups",[messagesaveddata.storeIds componentsSeparatedByString:@","],@"store",@"msg_quickreply",@"featureType",nil];
                                    }
                                    else
                                    {
                                        whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",@[],@"groups",@[[NSNumber numberWithInt:messagesaveddata.storeid]],@"store",@"msg_quickreply",@"featureType",nil];
                                    }
                                    
                                    
                                }
                                
                            }
                        }
                        [HttpHandler audioFileSend:whoDic audioFile:file1Data pns: self.pnsdic userScreen:[NSString stringWithFormat:@"%ld",(long)self.tableView.tag]];
                    }
                    else if (self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
                    {
                        NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
                        if (selectedRows==0)
                        {
                            if (self.tableView.tag==3)
                            {
                                [self getmessagesentdata:4];
                            }
                            else if(self.tableView.tag==6)
                            {
                                [self getmessagesentdata:17];
                            }
                            else if(self.tableView.tag==12)
                            {
                                [self getmessagesentdata:33];
                            }
                        }
                        messagesentdata=[sentArray objectAtIndex:rowindex];
                        contactNameString=messagesentdata.to;
                        
                        //calltype=messagesentdata.callType;
                        
                        NSString *milliseccallTxId=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]*1000];
                        NSArray *tempArraycallTxId = [milliseccallTxId componentsSeparatedByString:@"."];
                        callTxId=[tempArraycallTxId objectAtIndex:0];
                        file1Data = [[NSData alloc] initWithContentsOfURL:[[AudioManager getsharedInstance]getEarboxAudioPath: [NSString stringWithFormat:@"%@",messagesentdata.callId]]];
                        replyAudiourl=[[AudioManager getsharedInstance]getEarboxAudioPath: [NSString stringWithFormat:@"%@",messagesentdata.callId]];
                        [[AudioManager getsharedInstance]sentaudioPath:callTxId oldurl:replyAudiourl];
                        self.pnsdic=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",contactNameString,@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0] doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:replyAudiourl]],@"duration",@"medium",@"priority",callTxId,@"callTxId" ,[NSNumber numberWithDouble:messagesentdata.callType],@"callType",[NSNumber numberWithDouble:0],@"ttl",messagesentdata.pushTxId,@"pushTxnId",nil];
                        NSDictionary *whoDic;
                        if(self.tableView.tag==3 || self.tableView.tag==12)
                        {
                            if (messagesentdata.callType==108)
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:@"ASGroup" forKey:@"ASGroup"];
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[apploginDetails objectForKey:@"chainName"],@"chain",@[contactNameString],@"users",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",callTxId,@"request_time",[messagesentdata.groupIds componentsSeparatedByString:@","],@"groups",@"msg_quickreply",@"featureType",nil];
                            }
                            else
                            {
                                if (messagesentdata.storeid==0)
                                {
                                    if (messagesentdata.callType==102 || messagesentdata.callType==110)
                                    {
                                        [[NSUserDefaults standardUserDefaults] setObject:@"HQ" forKey:@"Select"];
                                        whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",millisec,@"request_time",@[messagesentdata.managerAppId],@"users",@"msg_quickreply",@"featureType",nil];
                                    }
                                    else
                                    {
                                        if ([messagesentdata.storeIds isEqualToString:@""] || [messagesaveddata.groupIds length]==0)
                                        {
                                            if([messagesentdata.storeIds length]==0)
                                            {
                                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",@[],@"store",@"msg_quickreply",@"featureType",nil];
                                            }
                                            
                                            else
                                            {
                                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",[messagesentdata.storeIds componentsSeparatedByString:@","],@"store",@"msg_quickreply",@"featureType",nil];
                                            }
                                        }
                                        
                                        else if (messagesentdata.callType==104)
                                        {
                                            [[NSUserDefaults standardUserDefaults] setObject:@"ASGroup" forKey:@"ASGroup"];
                                            whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[apploginDetails objectForKey:@"chainName"],@"chain",@[contactNameString],@"users",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",[messagesentdata.groupIds componentsSeparatedByString:@","],@"groups",@"msg_quickreply",@"featureType",nil];
                                        }
                                        
                                    }
                                }
                                else
                                {
                                    if (messagesentdata.storeIds!=0)
                                    {
                                        if (messagesentdata.storeid!=0)
                                        {
                                            whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",@[[NSNumber numberWithInt:messagesentdata.storeid]],@"store",@"msg_quickreply",@"featureType",nil];
                                        }
                                        else
                                        {
                                            whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[@"all"],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",millisec,@"request_time",[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedGroup"],@"groups",[messagesentdata.storeIds componentsSeparatedByString:@","],@"store",@"msg_quickreply",@"featureType",nil];
                                        }
                                    }
                                    
                                    
                                    
                                    
                                    
                                    else
                                    {
                                        if (messagesentdata.maCallType==1)
                                        {
                                            whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[NSArray arrayWithObject:messagesentdata.storeName],@"store", [NSArray arrayWithObject:[apploginDetails objectForKey:@"chainName"]],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",[tempArray objectAtIndex:0],@"request_time",@[@""],@"groups",nil];
                                        }
                                        else
                                        {
                                            whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",@[[NSNumber numberWithInt:messagesentdata.storeid]],@"store",@"msg_quickreply",@"featureType",nil];
                                        }
                                    }
                                }
                                
                            }
                        }
                        else
                        {
                            if (messagesentdata.callType==108)
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:@"ASGroup" forKey:@"ASGroup"];
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[apploginDetails objectForKey:@"chainName"],@"chain",@[contactNameString],@"users",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",callTxId,@"request_time",[messagesentdata.groupIds componentsSeparatedByString:@","],@"groups",@"msg_quickreply",@"featureType",nil];
                            }
                            else
                            {
                                if (messagesentdata.callType==102 || messagesentdata.callType==110)
                                {
                                    [[NSUserDefaults standardUserDefaults] setObject:@"HQ" forKey:@"Select"];
                                    whoDic=[NSDictionary dictionaryWithObjectsAndKeys:[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",millisec,@"request_time",@[messagesentdata.managerAppId],@"users",@"msg_quickreply",@"featureType",nil];
                                }
                                else
                                {
                                    if (!(messagesentdata.callType==17 && messagesentdata.maCallType==1))
                                    {
                                        whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",callTxId,@"request_time",[messagesentdata.groupNames componentsSeparatedByString:@","],@"groups",[messagesentdata.storeIds componentsSeparatedByString:@","],@"store",@"msg_quickreply",@"featureType",nil];
                                    }
                                }
                            }
                        }
                        
                        if (messagesentdata.callType==17 && messagesentdata.maCallType==1)
                        {
                            if (messagesentdata.storeid==0)
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",callTxId,@"request_time",@[messagesentdata.groupName],@"groups",@[],@"store",@"msg_quickreply",@"featureType",nil];
                            }
                            else
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"message",@"type",macId,@"macId",callTxId,@"request_time",@[messagesentdata.groupName],@"groups",@[[NSNumber numberWithInt:messagesentdata.storeid]],@"store",@"msg_quickreply",@"featureType",nil];
                                
                            }
                            
                        }
                        
                        [HttpHandler audioFileSend:whoDic audioFile:file1Data pns: self.pnsdic userScreen:[NSString stringWithFormat:@"%ld",(long)self.tableView.tag]];
                        
                    }
                    
                    
                    
                }
                else if(self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9)
                {
                    NSDictionary *whoDic;
                    int storeid;
                    if (self.tableView.tag==7)
                    {
                        [self getannouncementinboxdata:@"AnnouncementInbox"];
                        announcementinboxdata=[inboxArray objectAtIndex:rowindex];
                        contactNameString=announcementinboxdata.from;
                        calltype=announcementinboxdata.callType;
                        storeid=announcementinboxdata.storeid;
                        callTxId=[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"];
                        replyAudiourl=sender.object;
                        file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
                        if (announcementinboxdata.maCallType==2)
                        {
                            if (storeid==0)
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"announcement_now",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",@[],@"store",@"msg_quickreply",@"featureType",nil];
                            }
                            else
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"announcement_now",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",@[[NSNumber numberWithInt:storeid]],@"store",@"msg_quickreply",@"featureType",nil];
                            }
                            
                        }
                        else
                        {
                            if (storeid==0)
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"announcement_now",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",@[],@"store",@"msg_quickreply",@"featureType",nil];
                            }
                            else
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"announcement_now",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",@[[NSNumber numberWithInt:storeid]],@"store",@"msg_quickreply",@"featureType",nil];
                            }
                            
                        }
                    }
                    else if (self.tableView.tag==8)
                    {
                        [self getannouncementsaveddata:@"AnnouncementSaved"];
                        announcementsaveddata=[savedArray objectAtIndex:rowindex];
                        contactNameString=announcementsaveddata.from;
                        calltype=announcementsaveddata.callType;
                        storeid=announcementsaveddata.storeid;
                        callTxId=[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"];
                        replyAudiourl=sender.object;
                        file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
                        if (announcementsaveddata.maCallType==1)
                        {
                            if (storeid==0)
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"announcement_now",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",@[],@"store",@"msg_quickreply",@"featureType",nil];
                            }
                            else
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"announcement_now",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",@[[NSNumber numberWithInt:storeid]],@"store",@"msg_quickreply",@"featureType",nil];
                            }
                            
                        }
                        else
                        {
                            if (storeid==0)
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"announcement_now",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",@[],@"store",@"msg_quickreply",@"featureType",nil];
                            }
                            else
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"announcement_now",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",@[[NSNumber numberWithInt:storeid]],@"store",@"msg_quickreply",@"featureType",nil];
                            }
                            
                        }
                    }
                    else {
                        NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
                        if (selectedRows==0 || isRowClosed)
                        {
                            [self getannouncementsentdata:@"AnnouncementSent"];
                        }
                        announcementsentdata=[sentArray objectAtIndex:rowindex];
                        if (![announcementsentdata.to isEqualToString:@"all"])
                        {
                            contactNameString=announcementsentdata.to;
                        }
                        else
                        {
                            contactNameString=@"all";
                        }
                        calltype=announcementsentdata.callType;
                        storeid=announcementsentdata.storeid;
                        NSString *milliseccallTxId=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]*1000];
                        NSArray *tempArraycallTxId = [milliseccallTxId componentsSeparatedByString:@"."];
                        callTxId=[tempArraycallTxId objectAtIndex:0];
                        file1Data = [[NSData alloc] initWithContentsOfURL:[[AudioManager getsharedInstance]getEarboxAudioPath: [NSString stringWithFormat:@"%@",announcementsentdata.callId]]];
                        replyAudiourl=[[AudioManager getsharedInstance]getEarboxAudioPath: [NSString stringWithFormat:@"%@",announcementsentdata.callId]];
                        [[AudioManager getsharedInstance]sentaudioPath:callTxId oldurl:replyAudiourl];
                        if (announcementsentdata.maCallType==1)
                        {
                            if(storeid==0)
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"announcement_now",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",@[],@"store",@"msg_quickreply",@"featureType",nil];
                            }
                            else
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"announcement_now",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",@[[NSNumber numberWithInt:storeid]],@"store",@"msg_quickreply",@"featureType",nil];
                            }
                            
                        }
                        else
                        {
                            if ([announcementsentdata.groupNames length]!=0)
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"announcement_now",@"type",macId,@"macId",callTxId,@"request_time",[announcementsentdata.groupNames componentsSeparatedByString:@","],@"groups",[announcementsentdata.storeIds componentsSeparatedByString:@","],@"store",@"msg_quickreply",@"featureType",nil];
                                
                            }
                            else
                            {
                                whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[],@"users",[apploginDetails objectForKey:@"chainName"],@"chain",@"MS_managerApp",@"originatorType",@"announcement_now",@"type",macId,@"macId",callTxId,@"request_time",@[],@"groups",[announcementsentdata.storeIds componentsSeparatedByString:@","],@"store",@"msg_quickreply",@"featureType",nil];
                            }
                        }
                    }
                    self.pnsdic=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",contactNameString,@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0] doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:replyAudiourl]],@"duration",@"medium",@"priority",callTxId,@"callTxId" ,[NSNumber numberWithDouble:calltype],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
                    
                    [HttpHandler audioFileSend:whoDic audioFile:file1Data pns: self.pnsdic userScreen:[NSString stringWithFormat:@"%ld",(long)self.tableView.tag]];
                    
                }
            }
        }
    }
    
}

#pragma mark - Audio recieved method
-(void)recievedAnnouncement:(NSNotification *)sender
{
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"InboxViewController"])
    {
        if (self.tableView.tag==6 || self.tableView.tag==3 || self.tableView.tag==9 || self.tableView.tag==12)
        {
            
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"replayNotification" object:sender.object];
        }
        NSString *screencode;
        if ([[sender.object objectAtIndex:0] isEqualToString:@"error"])
        {
            switch (self.tableView.tag) {
                case 1:
                    screencode=@"InboxPrivateMsgInboxVC";
                    break;
                    
                case 2:
                    screencode=@"InboxPrivateMsgSavedVC";
                    break;
                case 3:
                    screencode=@"InboxPrivateMsgSentVC";
                    break;
                case 10:
                    screencode=@"InboxEveryoneInboxVC";
                    break;
                case 11:
                    screencode=@"InboxEveryoneSavedVC";
                    break;
                case 12:
                    screencode=@"InboxEveryoneSentVC";
                    break;
                case 4:
                    screencode=@"InboxGroupMsgInboxVC";
                    break;
                case 5:
                    screencode=@"InboxGroupMsgSavedVC";
                    break;
                case 6:
                    screencode=@"InboxGroupMsgSentVC";
                    break;
                case 7:
                    screencode=@"InboxStoreMsgInboxVC";
                    break;
                case 8:
                    screencode=@"InboxStoreMsgSavedVC";
                    break;
                case 9:
                    screencode=@"InboxStoreMsgSentVC";
                    break;
                    
            }
            //@"Message not Sent";
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Server error"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Message/Announcement not sent"],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"Message/Announcement not sent"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
            [alert show];
            cell.replyTextButton.userInteractionEnabled=YES;
            cell.replyTextButton.backgroundColor=kNavBackgroundColor;
            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
        }
        else if ([[sender.object objectAtIndex:0] isEqualToString:@"message"])
        {
            if(self.tableView.tag==9)
            {
                if (self.audioPlayer.isPlaying)
                {
                    
                }
                else
                {
                    [self.pnsdic setObject:@"HEARD" forKey:@"status"];
                    [self.pnsdic setObject:@"1" forKey:@"replyStatus"];
                    [self.pnsdic setObject:[NSNumber numberWithInt:0] forKey:@"storeId"];
                    if (self.tableView.tag==9)
                    {
                        if (announcementsentdata.maCallType==1)
                        {
                            [self.pnsdic setObject:announcementsentdata.storeName forKey:@"storeName"];
                            [self.pnsdic setObject:announcementsentdata.groupName forKey:@"groupName"];
                            [self.pnsdic setObject:[NSNumber numberWithInt:announcementsentdata.storeid] forKey:@"storeId"];
                        }
                        [self.pnsdic setObject:[NSNumber numberWithInt:announcementsentdata.maCallType] forKey:@"maCallType"];
                        [self.pnsdic setObject:announcementsentdata.pushTxId forKey:@"pushTxId"];
                    }
                    [[Earboxinboxmodel sharedModel]announcementsentSave:[NSArray arrayWithObject: self.pnsdic]];
                    cell.replyTextButton.userInteractionEnabled=YES;
                    cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                }
                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
            }
            else
            {
                
                NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
                [myQueue addOperationWithBlock:^{
                    
                    companyId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"companyId"]intValue];
                    NSString *pushTxId= [[NSUserDefaults standardUserDefaults] objectForKey:@"call_ReplyID"];
                    
                    NSDictionary *notifydic= [HttpHandler replystatusV2:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] pushTxId:pushTxId companyId:companyId userScreen:[NSString stringWithFormat:@"%ld",(long)self.tableView.tag]];
                    
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        if ([[notifydic objectForKey:@"twsuccessMessage"] isEqualToString:@"success"])
                        {
                            cell.replyTextButton.userInteractionEnabled=YES;
                            cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                            BOOL inboxTag = NO;
                            BOOL saveTag = NO;
                            
                            //@"Message Sent";
                            if (self.tableView.tag==1 || self.tableView.tag==2 || self.tableView.tag==3 || self.tableView.tag==4 || self.tableView.tag==5 || self.tableView.tag==6 || self.tableView.tag==10 || self.tableView.tag==11 || self.tableView.tag==12)
                            {
                                if ([replyingTo isEqualToString:@"tag1"])
                                {
                                    inboxTag = YES;
                                }
                                else if ([replyingTo isEqualToString:@"tag2"])
                                {
                                    saveTag = YES;
                                }
                                
                                if (self.audioPlayer.isPlaying)
                                {
                                    
                                }
                                else
                                {
                                    
                                    NSString *callID_Defaults = [[NSUserDefaults standardUserDefaults] objectForKey:@"call_ReplyID"];
                                    BOOL found= NO;
                                    if(self.tableView.tag==1)
                                    {
                                        [self getFreshInboxList:4];
                                        messageinboxdata=[freshInboxList objectAtIndex:rowindex];
                                        if (messageinboxdata.maCallType==1)
                                        {
                                            [self.pnsdic setObject:[NSNumber numberWithInt:messageinboxdata.storeid] forKey:@"storeId"];
                                            [self.pnsdic setObject:messageinboxdata.groupName forKey:@"groupName"];
                                            [self.pnsdic setObject:messageinboxdata.storeName forKey:@"storeName"];
                                        }
                                        else if(messageinboxdata.callType==104)
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.groupIds componentsSeparatedByString:@","],@"groupId",[messageinboxdata.groupNames componentsSeparatedByString:@","],@"groupName", nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.storeNames componentsSeparatedByString:@","],@"storeName",[messageinboxdata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                        }
                                        
                                        else if(messageinboxdata.callType==102 || messageinboxdata.callType==110)
                                        {
                                            [self.pnsdic setObject:@[] forKey:@"groupName"];
                                            NSDictionary *hq=[NSDictionary dictionaryWithObjectsAndKeys:messageinboxdata.managerAppId,@"managerAppId",messageinboxdata.tagOutName,@"tagOutName", nil];
                                            [self.pnsdic setObject:hq forKey:@"hq"];
                                            [self.pnsdic setObject:messageinboxdata.managerAppId forKey:@"managerAppId"];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:@[],@"groups",nil] forKey:@"audience"];
                                        }
                                        else
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.groupNames componentsSeparatedByString:@","],@"groupName" ,nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.storeNames componentsSeparatedByString:@","],@"storeName",[messageinboxdata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                        }
                                        [self.pnsdic setObject:[NSNumber numberWithInt:messageinboxdata.maCallType] forKey:@"maCallType"];
                                        [self.pnsdic setObject:messageinboxdata.pushTxId forKey:@"pushTxId"];
                                        
                                        
                                    }
                                    if (self.tableView.tag==10)
                                    {
                                        [self getFreshInboxList:33];
                                        messageinboxdata=[freshInboxList objectAtIndex:rowindex];
                                        if (messageinboxdata.maCallType==1)
                                        {
                                            [self.pnsdic setObject:[NSNumber numberWithInt:messageinboxdata.storeid] forKey:@"storeId"];
                                            [self.pnsdic setObject:messageinboxdata.groupName forKey:@"groupName"];
                                            [self.pnsdic setObject:messageinboxdata.storeName forKey:@"storeName"];
                                        }
                                        else if(messageinboxdata.callType==104)
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.groupIds componentsSeparatedByString:@","],@"groupId",[messageinboxdata.groupNames componentsSeparatedByString:@","],@"groupName", nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.storeNames componentsSeparatedByString:@","],@"storeName",[messageinboxdata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                        }
                                        
                                        else if(messageinboxdata.callType==102 || messageinboxdata.callType==110)
                                        {
                                            [self.pnsdic setObject:@[] forKey:@"groupName"];
                                            NSDictionary *hq=[NSDictionary dictionaryWithObjectsAndKeys:messageinboxdata.managerAppId,@"managerAppId",messageinboxdata.tagOutName,@"tagOutName", nil];
                                            [self.pnsdic setObject:hq forKey:@"hq"];
                                            [self.pnsdic setObject:messageinboxdata.managerAppId forKey:@"managerAppId"];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:@[],@"groups",nil] forKey:@"audience"];
                                        }
                                        else
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.groupNames componentsSeparatedByString:@","],@"groupName" ,nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.storeNames componentsSeparatedByString:@","],@"storeName",[messageinboxdata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                        }
                                        [self.pnsdic setObject:[NSNumber numberWithInt:messageinboxdata.maCallType] forKey:@"maCallType"];
                                        [self.pnsdic setObject:messageinboxdata.pushTxId forKey:@"pushTxId"];
                                        
                                        
                                    }
                                    if (self.tableView.tag==4)
                                    {
                                        [self getFreshInboxList:17];
                                        messageinboxdata=[freshInboxList objectAtIndex:rowindex];
                                        if (messageinboxdata.maCallType==3)
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.groupIds componentsSeparatedByString:@","],@"groupId",[messageinboxdata.groupNames componentsSeparatedByString:@","],@"groupName", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",nil] forKey:@"audience"];
                                            [self.pnsdic setObject:messageinboxdata.groupName forKey:@"groupName"];
                                        }
                                        else  if (messageinboxdata.maCallType==1)
                                        {
                                            [self.pnsdic setObject:[NSNumber numberWithInt:messageinboxdata.storeid] forKey:@"storeId"];
                                            [self.pnsdic setObject:messageinboxdata.groupName forKey:@"groupName"];
                                            [self.pnsdic setObject:messageinboxdata.storeName forKey:@"storeName"];
                                        }
                                        else
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.groupIds componentsSeparatedByString:@","],@"groupId",[messageinboxdata.groupNames componentsSeparatedByString:@","],@"groupName", nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.storeNames componentsSeparatedByString:@","],@"storeName",[messageinboxdata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                        }
                                        [self.pnsdic setObject:[NSNumber numberWithInt:messageinboxdata.maCallType] forKey:@"maCallType"];
                                        [self.pnsdic setObject:messageinboxdata.pushTxId forKey:@"pushTxId"];
                                    }
                                    if(self.tableView.tag==2)
                                    {
                                        [self getFreshSavedList:4];
                                        messagesaveddata=[freshSavedList objectAtIndex:rowindex];
                                        if (messagesaveddata.maCallType==1)
                                        {
                                            [self.pnsdic setObject:[NSNumber numberWithInt:messagesaveddata.storeid] forKey:@"storeId"];
                                            [self.pnsdic setObject:messagesaveddata.groupName forKey:@"groupName"];
                                            [self.pnsdic setObject:messagesaveddata.storeName forKey:@"storeName"];
                                            
                                        }
                                        else if(messageinboxdata.callType==104)
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesaveddata.groupIds componentsSeparatedByString:@","],@"groupId",[messagesaveddata.groupNames componentsSeparatedByString:@","],@"groupName", nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.storeNames componentsSeparatedByString:@","],@"storeName",[messageinboxdata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                        }
                                        
                                        else if(messagesaveddata.callType==102 || messagesaveddata.callType==110)
                                        {
                                            [self.pnsdic setObject:@[] forKey:@"groupName"];
                                            NSDictionary *hq=[NSDictionary dictionaryWithObjectsAndKeys:messagesaveddata.managerAppId,@"managerAppId",messagesaveddata.tagOutName,@"tagOutName", nil];
                                            [self.pnsdic setObject:hq forKey:@"hq"];
                                            [self.pnsdic setObject:messagesaveddata.managerAppId forKey:@"managerAppId"];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:@[],@"groups",nil] forKey:@"audience"];
                                        }
                                        else
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesaveddata.groupNames componentsSeparatedByString:@","],@"groupName" ,nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messagesaveddata.storeNames componentsSeparatedByString:@","],@"storeName",[messagesaveddata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                        }
                                        [self.pnsdic setObject:[NSNumber numberWithInt:messagesaveddata.maCallType] forKey:@"maCallType"];
                                        [self.pnsdic setObject:messagesaveddata.pushTxId forKey:@"pushTxId"];
                                    }
                                    if(self.tableView.tag==11)
                                    {
                                        [self getFreshSavedList:33];
                                        messagesaveddata=[freshSavedList objectAtIndex:rowindex];
                                        if (messagesaveddata.maCallType==1)
                                        {
                                            [self.pnsdic setObject:[NSNumber numberWithInt:messagesaveddata.storeid] forKey:@"storeId"];
                                            [self.pnsdic setObject:messagesaveddata.groupName forKey:@"groupName"];
                                            [self.pnsdic setObject:messagesaveddata.storeName forKey:@"storeName"];
                                            
                                        }
                                        else if(messageinboxdata.callType==104)
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesaveddata.groupIds componentsSeparatedByString:@","],@"groupId",[messagesaveddata.groupNames componentsSeparatedByString:@","],@"groupName", nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.storeNames componentsSeparatedByString:@","],@"storeName",[messageinboxdata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                        }
                                        
                                        else if(messagesaveddata.callType==102 || messagesaveddata.callType==110)
                                        {
                                            [self.pnsdic setObject:@[] forKey:@"groupName"];
                                            NSDictionary *hq=[NSDictionary dictionaryWithObjectsAndKeys:messagesaveddata.managerAppId,@"managerAppId",messagesaveddata.tagOutName,@"tagOutName", nil];
                                            [self.pnsdic setObject:hq forKey:@"hq"];
                                            [self.pnsdic setObject:messagesaveddata.managerAppId forKey:@"managerAppId"];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:@[],@"groups",nil] forKey:@"audience"];
                                        }
                                        else
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesaveddata.groupNames componentsSeparatedByString:@","],@"groupName" ,nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messagesaveddata.storeNames componentsSeparatedByString:@","],@"storeName",[messagesaveddata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                        }
                                        [self.pnsdic setObject:[NSNumber numberWithInt:messagesaveddata.maCallType] forKey:@"maCallType"];
                                        [self.pnsdic setObject:messagesaveddata.pushTxId forKey:@"pushTxId"];
                                    }
                                    if (self.tableView.tag==5)
                                    {
                                        [self getFreshSavedList:17];
                                        messagesaveddata=[freshSavedList objectAtIndex:rowindex];
                                        if (messagesaveddata.maCallType==3)
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesaveddata.groupIds componentsSeparatedByString:@","],@"groupId",[messagesaveddata.groupNames componentsSeparatedByString:@","],@"GroupName", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",nil] forKey:@"audience"];
                                            [self.pnsdic setObject:messagesaveddata.groupName forKey:@"groupName"];
                                        }
                                        
                                        else  if (messagesaveddata.maCallType==1)
                                        {
                                            [self.pnsdic setObject:[NSNumber numberWithInt:messagesaveddata.storeid] forKey:@"storeId"];
                                            [self.pnsdic setObject:messagesaveddata.groupName forKey:@"groupName"];
                                            [self.pnsdic setObject:messagesaveddata.storeName forKey:@"storeName"];
                                        }
                                        else
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesaveddata.groupNames componentsSeparatedByString:@","],@"groupName" ,nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messagesaveddata.storeNames componentsSeparatedByString:@","],@"storeName",[messagesaveddata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                        }
                                        [self.pnsdic setObject:[NSNumber numberWithInt:messagesaveddata.maCallType] forKey:@"maCallType"];
                                        [self.pnsdic setObject:messagesaveddata.pushTxId forKey:@"pushTxId"];
                                    }
                                    if (self.tableView.tag==3)
                                    {
                                        NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
                                        if (selectedRows==0 || isRowClosed)
                                        {
                                            [self getmessagesentdata:4];
                                        }
                                        messagesentdata=[sentArray objectAtIndex:rowindex];
                                        if (messagesentdata.maCallType==1)
                                        {
                                            [self.pnsdic setObject:[NSNumber numberWithInt:messagesentdata.storeid] forKey:@"storeId"];
                                            [self.pnsdic setObject:messagesentdata.groupName forKey:@"groupName"];
                                            [self.pnsdic setObject:messagesentdata.storeName forKey:@"storeName"];
                                            
                                        }
                                        else if (messagesentdata.maCallType==3)
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesentdata.groupIds componentsSeparatedByString:@","],@"groupId",[messagesentdata.groupNames componentsSeparatedByString:@","],@"groupName", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",nil] forKey:@"audience"];
                                            [self.pnsdic setObject:messagesentdata.groupName forKey:@"groupName"];
                                        }
                                        else if(messagesentdata.callType==110 || messagesentdata.callType==102)
                                        {
                                            
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:messagesentdata.managerAppId,@"managerAppId",messagesentdata.tagOutName,@"tagOutName",nil] forKey:@"hq"];
                                            [self.pnsdic setObject:messagesentdata.managerAppId forKey:@"managerAppId"];
                                            
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesaveddata.groupIds componentsSeparatedByString:@","],@"groupId",[messagesaveddata.groupNames componentsSeparatedByString:@","],@"groupName", nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.storeNames componentsSeparatedByString:@","],@"storeName",[messageinboxdata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                            
                                        }
                                        
                                        else
                                        {
                                            
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesentdata.groupNames componentsSeparatedByString:@","],@"groupName" ,nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messagesentdata.storeNames componentsSeparatedByString:@","],@"storeName",[messagesentdata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                            
                                        }
                                        [self.pnsdic setObject:[NSNumber numberWithInt:messagesentdata.maCallType] forKey:@"maCallType"];
                                        // [self.pnsdic setObject:messagesentdata.managerAppId forKey:@"managerAppId"];
                                        [self.pnsdic setObject:messagesentdata.pushTxId forKey:@"pushTxId"];
                                    }
                                    if (self.tableView.tag==12)
                                    {
                                        NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
                                        if (selectedRows==0 || isRowClosed)
                                        {
                                            [self getmessagesentdata:33];
                                        }
                                        messagesentdata=[sentArray objectAtIndex:rowindex];
                                        if (messagesentdata.maCallType==1)
                                        {
                                            [self.pnsdic setObject:[NSNumber numberWithInt:messagesentdata.storeid] forKey:@"storeId"];
                                            [self.pnsdic setObject:messagesentdata.groupName forKey:@"groupName"];
                                            [self.pnsdic setObject:messagesentdata.storeName forKey:@"storeName"];
                                            
                                        }
                                        else if (messagesentdata.maCallType==3)
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesentdata.groupIds componentsSeparatedByString:@","],@"groupId",[messagesentdata.groupNames componentsSeparatedByString:@","],@"groupName", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",nil] forKey:@"audience"];
                                            [self.pnsdic setObject:messagesentdata.groupName forKey:@"groupName"];
                                        }
                                        else if(messagesentdata.callType==110)
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesaveddata.groupIds componentsSeparatedByString:@","],@"groupId",[messagesaveddata.groupNames componentsSeparatedByString:@","],@"groupName", nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.storeNames componentsSeparatedByString:@","],@"storeName",[messageinboxdata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                        }
                                        
                                        else
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesentdata.groupNames componentsSeparatedByString:@","],@"groupName" ,nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messagesentdata.storeNames componentsSeparatedByString:@","],@"storeName",[messagesentdata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                        }
                                        [self.pnsdic setObject:[NSNumber numberWithInt:messagesentdata.maCallType] forKey:@"maCallType"];
                                        [self.pnsdic setObject:messagesentdata.pushTxId forKey:@"pushTxId"];
                                    }
                                    if (self.tableView.tag==6)
                                    {
                                        NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
                                        if (selectedRows==0 || isRowClosed)
                                        {
                                            [self getmessagesentdata:17];
                                        }
                                        messagesentdata=[sentArray objectAtIndex:rowindex];
                                        if (messagesentdata.callType==108)
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesentdata.groupIds componentsSeparatedByString:@","],@"groupId",[messagesentdata.groupNames componentsSeparatedByString:@","],@"groupName", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",nil] forKey:@"audience"];
                                            [self.pnsdic setObject:messagesentdata.groupName forKey:@"groupName"];
                                        }
                                        else if(messagesentdata.callType==110)
                                        {
                                            NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesaveddata.groupIds componentsSeparatedByString:@","],@"groupId",[messagesaveddata.groupNames componentsSeparatedByString:@","],@"groupName", nil];
                                            NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messageinboxdata.storeNames componentsSeparatedByString:@","],@"storeName",[messageinboxdata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                            [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                        }
                                        
                                        else
                                        {
                                            if (messagesentdata.maCallType==1 && messagesentdata.callType==17)
                                            {
                                                [self.pnsdic setObject:[NSNumber numberWithInt:messagesentdata.storeid]  forKey:@"storeId"];
                                                [self.pnsdic setObject:messagesentdata.groupName  forKey:@"to"];
                                                [self.pnsdic setObject:messagesentdata.storeName forKey:@"storeName"];
                                            }
                                            else
                                            {
                                                NSDictionary *groups=[NSDictionary dictionaryWithObjectsAndKeys:[messagesentdata.groupNames componentsSeparatedByString:@","],@"groupName" ,nil];
                                                NSDictionary *stores=[NSDictionary dictionaryWithObjectsAndKeys:[messagesentdata.storeNames componentsSeparatedByString:@","],@"storeName",[messagesentdata.storeIds componentsSeparatedByString:@","],@"storeId", nil];
                                                [self.pnsdic setObject:[NSDictionary dictionaryWithObjectsAndKeys:groups,@"groups",stores,@"stores",nil] forKey:@"audience"];
                                            }
                                        }
                                        [self.pnsdic setObject:[NSNumber numberWithInt:messagesentdata.maCallType] forKey:@"maCallType"];
                                        [self.pnsdic setObject:messagesentdata.pushTxId forKey:@"pushTxId"];
                                    }
                                    
                                    for (int i=0; i<[freshInboxList count]; i++)
                                    {
                                        messageFreshList =[freshInboxList objectAtIndex:i];
                                        
                                        if ([messageFreshList.pushTxId isEqualToString: callID_Defaults])
                                        {
                                            found = YES;
                                            break;
                                        }
                                    }
                                    
                                    if(found == NO)
                                    {
                                        for (int i=0; i<[freshSavedList count]; i++)
                                        {
                                            messageFreshSavedData =[freshSavedList objectAtIndex:i];
                                            
                                            if ([messageFreshSavedData.pushTxId isEqualToString: callID_Defaults])
                                            {
                                                found = YES;
                                                break;
                                            }
                                        }
                                    }
                                    [self.pnsdic setObject:@"HEARD" forKey:@"status"];
                                    
                                    [[Earboxinboxmodel sharedModel]messagesentSave:[NSArray arrayWithObject: self.pnsdic]];
                                    
                                    
                                    if(found)
                                    {
                                        
                                        if (self.tableView.tag==1 || inboxTag || self.tableView.tag==4 || self.tableView.tag==10)
                                        {
                                            messageinboxdata=[freshReplyList objectAtIndex:rowindex];
                                            messageinboxdata.reply =@"1";
                                            NSError *error=nil;
                                            [store save:error];
                                        }
                                        else if (self.tableView.tag==2  || saveTag || self.tableView.tag==5 || self.tableView.tag==11)
                                        {
                                            if (self.tableView.tag==2)
                                            {
                                                [self getFreshSavedList:4];
                                            }
                                            else if(self.tableView.tag==5)
                                            {
                                                [self getFreshSavedList:17];
                                            }
                                            else if(self.tableView.tag==11)
                                            {
                                                [self getFreshSavedList:33];
                                            }
                                            
                                            messageFreshSavedData=[freshSavedList objectAtIndex:rowindex];
                                            messageFreshSavedData.reply =@"1";
                                            NSError *error=nil;
                                            [store save:error];
                                        }
                                    }
                                    
                                }
                            }
                            else if(self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9)
                            {
                                if (self.audioPlayer.isPlaying)
                                {
                                    
                                }
                                else
                                {
                                    [self.pnsdic setObject:@"HEARD" forKey:@"status"];
                                    [self.pnsdic setObject:@"1" forKey:@"replyStatus"];
                                    announcementsentdata=[sentArray objectAtIndex:rowindex];
                                    [self.pnsdic setObject:[NSNumber numberWithInt:announcementsentdata.storeid] forKey:@"storeId"];
                                    [[Earboxinboxmodel sharedModel]announcementsentSave:[NSArray arrayWithObject: self.pnsdic]];
                                    cell.replyTextButton.userInteractionEnabled=YES;
                                    cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                                }
                                
                            }
                            if (self.tableView.tag==6 || self.tableView.tag==3 || self.tableView.tag==9 || self.tableView.tag==12)
                            {
                                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                                
                            }
                            else
                            {
                            }
                            
                        }
                        else
                        {
                           UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Server error"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Message/Announcement not sent"],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"ReplystatusV2"],[[DiagnosticTool sharedManager]generateErrorCode:@"Message/Announcement not sent"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                            [alert show];
                            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                        }
                    }];
                }];
            }
        }
    }
    
}

#pragma mark - Delete pressed method
-(void)deleteTextButtonClicked:(UIButton *)button
{
    deletebuttonindex=button.tag;
    
    if(self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Confirm delete"] message:[dictLang objectForKey:@"Are you sure you want to delete this Announcement?"] delegate:self cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Confirm delete"] message:[dictLang objectForKey:@"Are you sure you want to delete this Message?"] delegate:self cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
        [alert show];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self audioPlayerStop];
    NSString *status;
    if (alertView.tag==300)
    {
        [self replayCancelNotification:nil];
    }
    else if(alertView.tag==250)
    {
        [self tableViewUpdate];
    }
    else
    {
        if (buttonIndex==0)
        {
            
        }
        else
        {
            NSString *screencode;
            switch (self.tableView.tag) {
                case 1:
                    screencode=@"InboxPrivateMsgInboxVC";
                    break;
                    
                case 2:
                    screencode=@"InboxPrivateMsgSavedVC";
                    break;
                case 3:
                    screencode=@"InboxPrivateMsgSentVC";
                    break;
                case 10:
                    screencode=@"InboxEveryoneInboxVC";
                    break;
                case 11:
                    screencode=@"InboxEveryoneSavedVC";
                    break;
                case 12:
                    screencode=@"InboxEveryoneSentVC";
                    break;
                case 4:
                    screencode=@"InboxGroupMsgInboxVC";
                    break;
                case 5:
                    screencode=@"InboxGroupMsgSavedVC";
                    break;
                case 6:
                    screencode=@"InboxGroupMsgSentVC";
                    break;
                case 7:
                    screencode=@"InboxStoreMsgInboxVC";
                    break;
                case 8:
                    screencode=@"InboxStoreMsgSavedVC";
                    break;
                case 9:
                    screencode=@"InboxStoreMsgSentVC";
                    break;
                    
            }
            deselected=YES;
            if(searchsepctag || searchTag)
            {
                
                if (self.tableView.tag==1 || self.tableView.tag==2 || self.tableView.tag==3 || self.tableView.tag==4 || self.tableView.tag==5 || self.tableView.tag==6 || self.tableView.tag==10 || self.tableView.tag==11 || self.tableView.tag==12)
                {
                    if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
                    {
                        if (searchResultsAll.count>deletebuttonindex) {
                            messageinboxdata=[searchResultsAll objectAtIndex:deletebuttonindex];
                            if (messageinboxdata.fault)
                            {
                                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                                alert.tag=250;
                                [alert show];
                            }
                            else
                            {
                                status=messageinboxdata.status;
                                [self messageDelete:status];
                                if(self.tableView.tag==1)
                                {
                                    [self getmessageinboxdata:4];
                                    [self updatemodestatus:messageinboxdata.pushTxId status:@"messageinbox" modeId:@"delete" tabletag:1];
                                }
                                else if(self.tableView.tag==4)
                                {
                                    [self getmessageinboxdata:17];
                                    [self updatemodestatus:messageinboxdata.pushTxId status:@"messageinbox" modeId:@"delete" tabletag:4];
                                }
                                else if(self.tableView.tag==10)
                                {
                                    [self getmessageinboxdata:33];
                                    [self updatemodestatus:messageinboxdata.pushTxId status:@"messageinbox" modeId:@"delete" tabletag:10];
                                }
                            }
                        }
                        else
                        {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                            alert.tag=250;
                            [alert show];
                        }
                    }
                    else if (self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
                    {
                        if (searchResultsAll.count>deletebuttonindex) {
                            messagesaveddata=[searchResultsAll objectAtIndex:deletebuttonindex];
                            if (messagesaveddata.fault)
                            {
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                                alert.tag=250;
                                [alert show];
                            }
                            else
                            {
                                status=messagesaveddata.status;
                                [self messageDelete:status];
                                if(self.tableView.tag==2)
                                {
                                    [self getmessagesaveddata:4];
                                    [self updatemodestatus:messagesaveddata.pushTxId status:@"messagesaved" modeId:@"delete" tabletag:2];
                                }
                                else if(self.tableView.tag==5)
                                {
                                    [self getmessagesaveddata:17];
                                    [self updatemodestatus:messagesaveddata.pushTxId status:@"messagesaved" modeId:@"delete" tabletag:5];
                                }
                                else if(self.tableView.tag==11)
                                {
                                    [self getmessagesaveddata:33];
                                    [self updatemodestatus:messagesaveddata.pushTxId status:@"messagesaved" modeId:@"delete" tabletag:11];
                                }
                            }
                        }
                        else
                        {
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                            alert.tag=250;
                            [alert show];
                        }
                    }
                    else if (self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
                    {
                        if(self.tableView.tag==3)
                        {
                            [self getmessagesentdata:4];
                        }
                        else if(self.tableView.tag==6)
                        {
                            [self getmessagesentdata:17];
                        }
                        else if(self.tableView.tag==12)
                        {
                            [self getmessagesentdata:33];
                        }
                        messagesentdata=[searchResultsAll objectAtIndex:deletebuttonindex];
                        //status=messagesentdata.status;
                        
                        [self updatemodestatus:messagesentdata.pushTxId status:@"messagesent" modeId:@"delete" tabletag:0];
                        
                    }
                    
                }
                else if(self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9)
                {
                    if (self.tableView.tag==7 )
                    {
                        [self getannouncementinboxdata:@"AnnouncementInbox"];
                        if (searchResultsAll.count>deletebuttonindex) {
                            announcementinboxdata=[searchResultsAll objectAtIndex:deletebuttonindex];
                            if (announcementinboxdata.fault)
                            {
                                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The announcement that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The announcement that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                                alert.tag=250;
                                [alert show];
                            }
                            else
                            {
                                status=announcementinboxdata.status;
                                [self announcementDelete:status];
                                
                                [self updatemodestatus:announcementinboxdata.pushTxId status:@"announcementinbox" modeId:@"delete" tabletag:7];
                            }
                        }
                        else
                        {
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The announcement that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The announcement that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                            alert.tag=250;
                            [alert show];
                        }
                        
                    }
                    else if (self.tableView.tag==8)
                    {
                        [self getannouncementsaveddata:@"AnnouncementSaved"];
                        if (searchResultsAll.count>deletebuttonindex) {
                            announcementsaveddata=[searchResultsAll objectAtIndex:deletebuttonindex];
                            if (announcementsaveddata.fault)
                            {
                               UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The announcement that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The announcement that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                                alert.tag=250;
                                [alert show];
                            }
                            else
                            {
                                status=announcementsaveddata.status;
                                [self announcementDelete:status];
                                
                                [self updatemodestatus:announcementsaveddata.pushTxId status:@"announcementsaved" modeId:@"delete" tabletag:8];
                            }
                        }
                        else
                        {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The announcement that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The announcement that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                            alert.tag=250;
                            [alert show];
                        }
                        
                    }
                    else
                    {
                        [self getannouncementsentdata:@"AnnouncementSent"];
                        announcementsentdata=[searchResultsAll objectAtIndex:deletebuttonindex];
                        // status=announcementsentdata.status;
                        
                        [self updatemodestatus:announcementsentdata.pushTxId status:@"announcementsent" modeId:@"delete" tabletag:9];
                        
                    }
                    
                }
            }
            else
            {
                if (self.tableView.tag==1 || self.tableView.tag==2 || self.tableView.tag==3 || self.tableView.tag==4 || self.tableView.tag==5 || self.tableView.tag==6 || self.tableView.tag==10 || self.tableView.tag==11 || self.tableView.tag==12)
                {
                    if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
                    {
                        if (inboxArray.count>deletebuttonindex) {
                            messageinboxdata=[inboxArray objectAtIndex:deletebuttonindex];
                            if (messageinboxdata.fault)
                            {
                                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                                alert.tag=250;
                                [alert show];
                            }
                            else
                            {
                                status=messageinboxdata.status;
                                [self messageDelete:status];
                                if(self.tableView.tag==1)
                                {
                                    [self getmessageinboxdata:4];
                                    [self updatemodestatus:messageinboxdata.pushTxId status:@"messageinbox" modeId:@"delete" tabletag:1];
                                }
                                else if(self.tableView.tag==4)
                                {
                                    [self getmessageinboxdata:17];
                                    [self updatemodestatus:messageinboxdata.pushTxId status:@"messageinbox" modeId:@"delete" tabletag:4];
                                }
                                else if(self.tableView.tag==10)
                                {
                                    [self getmessageinboxdata:33];
                                    [self updatemodestatus:messageinboxdata.pushTxId status:@"messageinbox" modeId:@"delete" tabletag:10];
                                }
                            }
                        }
                        else
                        {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                            alert.tag=250;
                            [alert show];
                        }
                    }
                    else if (self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
                    {
                        if (savedArray.count>deletebuttonindex) {
                            messagesaveddata=[savedArray objectAtIndex:deletebuttonindex];
                            if (messagesaveddata.fault)
                            {
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                                alert.tag=250;
                                [alert show];
                            }
                            else
                            {
                                status=messagesaveddata.status;
                                [self messageDelete:status];
                                if(self.tableView.tag==2)
                                {
                                    [self getmessagesaveddata:4];
                                    [self updatemodestatus:messagesaveddata.pushTxId status:@"messagesaved" modeId:@"delete" tabletag:2];
                                }
                                else if(self.tableView.tag==5)
                                {
                                    [self getmessagesaveddata:17];
                                    [self updatemodestatus:messagesaveddata.pushTxId status:@"messagesaved" modeId:@"delete" tabletag:5];
                                }
                                else if(self.tableView.tag==11)
                                {
                                    [self getmessagesaveddata:33];
                                    [self updatemodestatus:messagesaveddata.pushTxId status:@"messagesaved" modeId:@"delete" tabletag:11];
                                }
                            }
                        }
                        else
                        {
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                            alert.tag=250;
                            [alert show];
                        }
                        
                    }
                    else if (self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
                    {
                        if(self.tableView.tag==3)
                        {
                            [self getmessagesentdata:4];
                        }
                        else if(self.tableView.tag==6)
                        {
                            [self getmessagesentdata:17];
                        }
                        else if(self.tableView.tag==12)
                        {
                            [self getmessagesentdata:33];
                        }
                        messagesentdata=[sentArray objectAtIndex:deletebuttonindex];
                        //  status=messagesentdata.status;
                        
                        [self updatemodestatus:messagesentdata.pushTxId status:@"messagesent" modeId:@"delete" tabletag:0];
                    }
                }
                else if(self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9)
                {
                    if (self.tableView.tag==7 )
                    {
                        [self getannouncementinboxdata:@"AnnouncementInbox"];
                        if (inboxArray.count>deletebuttonindex) {
                            
                            announcementinboxdata=[inboxArray objectAtIndex:deletebuttonindex];
                            if (announcementinboxdata.fault)
                            {
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The announcement that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The announcement that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                                alert.tag=250;
                                [alert show];
                            }
                            else
                            {
                                status=announcementinboxdata.status;
                                [self announcementDelete:status];
                                
                                [self updatemodestatus:announcementinboxdata.pushTxId status:@"announcementinbox" modeId:@"delete" tabletag:7];
                            }
                        }
                        else
                        {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The announcement that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The announcement that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                            alert.tag=250;
                            [alert show];
                        }
                        
                    }
                    else if (self.tableView.tag==8)
                    {
                        [self getannouncementsaveddata:@"AnnouncementSaved"];
                        if (savedArray.count>deletebuttonindex) {
                            announcementsaveddata=[savedArray objectAtIndex:deletebuttonindex];
                            if (announcementsaveddata.fault)
                            {
                                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The announcement that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The announcement that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                                alert.tag=250;
                                [alert show];
                            }
                            else
                            {
                                status=announcementsaveddata.status;
                                [self announcementDelete:status];
                                
                                [self updatemodestatus:announcementsaveddata.pushTxId status:@"announcementsaved" modeId:@"delete" tabletag:8];
                            }
                        }
                        else
                        {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The announcement that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV3"],[[DiagnosticTool sharedManager]generateErrorCode:@"The announcement that you are trying to delete is expired"]] delegate:self cancelButtonTitle:nil otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                            alert.tag=250;
                            [alert show];
                        }
                        
                    }
                    else
                    {
                        [self getannouncementsentdata:@"AnnouncementSent"];
                        announcementsentdata=[sentArray objectAtIndex:deletebuttonindex];
                        //status=announcementsentdata.status;
                        
                        [self updatemodestatus:announcementsentdata.pushTxId status:@"announcementsent" modeId:@"delete" tabletag:9];
                    }
                }
            }
        }
    }
}

-(void)messageDelete:(NSString *)status
{
    if ([status isEqualToString:@"new"])
    {
        int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
        if (messagecount==0)
        {
            messagecount=0;
        }
        else
        {
            messagecount--;
        }
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagetotalcount"];
        int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
}

-(void)announcementDelete:(NSString *)status
{
    if ([status isEqualToString:@"new"])
    {
        int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementtotalcount"] intValue];
        if (announcementcount==0)
        {
            announcementcount=0;
        }
        else
        {
            announcementcount--;
        }
        
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:announcementcount] forKey:@"announcementtotalcount"];
        
        int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
}

#pragma mark - Earpiece method
-(void)speakerButtonAction:(UIBarButtonItem *)button
{
    if (button.tag==1)
    {
        [speakerButton setImage:[UIImage imageNamed:@"headphones"]];
        speakerButton.tag=2;
        [[NSUserDefaults standardUserDefaults]setObject:@"mic" forKey:@"loud"];
    }
    else
    {
        speakerButton.tag=1;
        [speakerButton setImage:[UIImage imageNamed:@"loud"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"loud" forKey:@"loud"];
    }
    [[AudioManager getsharedInstance] switchAudioOutput];
}

-(void)speakerAudioChange
{
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"loud"] isEqualToString:@"mic"])
    {
        [speakerButton setImage:[UIImage imageNamed:@"headphones"]];
        speakerButton.tag=2;
        [[NSUserDefaults standardUserDefaults]setObject:@"mic" forKey:@"loud"];
    }
    else
    {
        speakerButton.tag=1;
        [speakerButton setImage:[UIImage imageNamed:@"loud"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"loud" forKey:@"loud"];
    }
    [[AudioManager getsharedInstance] switchAudioOutput];
}


#pragma mark - PushNotification recieve methods
-(void)pushnotificationrecieved:(NSNotification *)notification
{
    pushnotificationRecieved=YES;
    [self inboxtotalcount];
    [self requestUndeliveredMsgsinbox];
    [self speakerAudioChange];
    
}

#pragma mark - inboxtotalcount
-(void)inboxtotalcount
{
    NSDictionary *loginDetail=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
       [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"InboxVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"UnheardV2"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
        [myQueueSales addOperationWithBlock:^{
            NSDictionary *responsDic=[HttpHandler unheardV2:[loginDetail objectForKey:@"twUserName"] twPassword:[loginDetail objectForKey:@"twPassword"] twTxId:[loginDetail objectForKey:@"twTxId"] twMoment:[loginDetail objectForKey:@"twMoment"] companyId:[[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"] intValue] userScreen:@"InboxVC"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if([responsDic objectForKey:@"earBoxUnheard"] && ![[responsDic objectForKey:@"earBoxUnheard"] isKindOfClass:[NSNull class]])
                {
                    NSData *announcementEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:responsDic];
                    [[NSUserDefaults standardUserDefaults] setObject:announcementEncodedObject forKey:@"inboxcount"];
                    if([[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"groupMessage"] intValue]!=0)
                    {
                        groupMessageCount.hidden=NO;
                        groupMessageCount.text=[[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"groupMessage"]stringValue];
                        [[NSUserDefaults standardUserDefaults]setObject:[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"groupMessage"] forKey:@"groupMessageCountdef"];
                    }
                    else
                    {
                        groupMessageCount.hidden=YES;
                        [[NSUserDefaults standardUserDefaults]setObject:[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"groupMessage"] forKey:@"groupMessageCountdef"];
                    }
                    if ([[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"message"] intValue]!=0)
                    {
                        privateMessageCount.hidden=NO;
                        privateMessageCount.text=[[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"message"] stringValue];
                        [[NSUserDefaults standardUserDefaults]setObject:[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"message"] forKey:@"privateMessageCountdef"];
                    }
                    else
                        
                    {
                        privateMessageCount.hidden=YES;
                        [[NSUserDefaults standardUserDefaults]setObject:[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"message"] forKey:@"privateMessageCountdef"];
                    }
                    if ([[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"announcement"] intValue]!=0)
                    {
                        storeAnnouncementCount.hidden=NO;
                        storeAnnouncementCount.text=[[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"announcement"] stringValue];
                        [[NSUserDefaults standardUserDefaults]setObject:[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"announcement"] forKey:@"storeAnnouncementCountdef"];
                    }
                    else
                    {
                        storeAnnouncementCount.hidden=YES;
                        [[NSUserDefaults standardUserDefaults]setObject:[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"announcement"] forKey:@"storeAnnouncementCountdef"];
                    }
                    if ([[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"everyone"] intValue]!=0)
                    {
                        everyoneMessageCount.hidden=NO;
                        everyoneMessageCount.text=[[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"everyone"] stringValue];
                        [[NSUserDefaults standardUserDefaults]setObject:[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"everyone"] forKey:@"everyoneMessageCountdef"];
                    }
                    else
                    {
                        everyoneMessageCount.hidden=YES;
                        [[NSUserDefaults standardUserDefaults]setObject:[[responsDic objectForKey:@"earBoxUnheard"]objectForKey:@"everyone"] forKey:@"everyoneMessageCountdef"];
                    }
                }
            }];
        }];
    }
    
}

#pragma mark - Audio completed(%) method
-(void)percentageCompleted:(NSNotification *)notification
{
    if ([[notification.object objectAtIndex:2] isEqualToString:@"EarboxInbox"])
    {
        if(inboxArray.count>0)
        {
            messageinboxdata=[inboxArray objectAtIndex:rowindex];
            
            
            NSString *str1=messageinboxdata.callId;
            NSString *str2=[NSString stringWithFormat:@"%@",[notification.object objectAtIndex:1]];
            
            if ([str1 isEqualToString:str2])
            {
                NSIndexPath *myIP = [NSIndexPath indexPathForRow:rowindex inSection:0] ;
                cell = [self.tableView cellForRowAtIndexPath:myIP];
                if ([[notification.object objectAtIndex:0] floatValue]<1.000000)
                {
                    cell.playButton.backgroundColor=[UIColor grayColor];
                    cell.playButton.userInteractionEnabled=NO;
                    [cell.playButton setImage:[UIImage imageNamed:@""]
                                     forState:UIControlStateNormal];
                    cell.circularProgressView.progress=[[notification.object objectAtIndex:0] floatValue];
                    cell.circularProgressView.progressTintColor=kNavBackgroundColor;
                    [cell.activityView stopAnimating];
                    cell.activityView.hidden=YES;
                    
                }
                else
                {
                    cell.circularProgressView.progress=1.0;
                    cell.circularProgressView.trackTintColor=kNavBackgroundColor;
                    cell.playButton.userInteractionEnabled=YES;
                    cell.playButton.backgroundColor=kNavBackgroundColor;
                    [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                                     forState:UIControlStateNormal];
                    cell.circularProgressView.progressTintColor=kNavBackgroundColor;
                    [cell.activityView stopAnimating];
                    cell.activityView.hidden=YES;
                }
                
            }
        }
    }
    else  if ([[notification.object objectAtIndex:2] isEqualToString:@"AnnouncementInbox"])
    {
        if(inboxArray.count>0)
        {
            announcementinboxdata=[inboxArray objectAtIndex:rowindex];
            NSString *str1=announcementinboxdata.callId;
            NSString *str2=[NSString stringWithFormat:@"%@",[notification.object objectAtIndex:1]];
            
            if ([str1 isEqualToString:str2])
            {
                NSIndexPath *myIP = [NSIndexPath indexPathForRow:rowindex inSection:0] ;
                cell = [self.tableView cellForRowAtIndexPath:myIP];
                if ([[notification.object objectAtIndex:0] floatValue]<1.000000)
                {
                    cell.playButton.backgroundColor=[UIColor grayColor];
                    cell.playButton.userInteractionEnabled=NO;
                    [cell.playButton setImage:[UIImage imageNamed:@""]
                                     forState:UIControlStateNormal];
                    cell.circularProgressView.progress=[[notification.object objectAtIndex:0] floatValue];
                    cell.circularProgressView.progressTintColor=kNavBackgroundColor;
                    [cell.activityView stopAnimating];
                    cell.activityView.hidden=YES;
                    
                }
                else
                {
                    cell.circularProgressView.progress=1.0;
                    cell.circularProgressView.trackTintColor=kNavBackgroundColor;
                    cell.playButton.userInteractionEnabled=YES;
                    cell.playButton.backgroundColor=kNavBackgroundColor;
                    [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                                     forState:UIControlStateNormal];
                    cell.circularProgressView.progressTintColor=kNavBackgroundColor;
                    
                }
                
            }
        }
    }
}

#pragma mark AudioPlayer methods
- (IBAction)playAudioPressed:(id)playButton
{
    UIImage *downloadpng = [UIImage imageNamed:@"download"];
    //UIImage *audioplaypng = [UIImage imageNamed:@"audioplayer_play"];
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:rowindex inSection:0] ;
    cell = [self.tableView cellForRowAtIndexPath:myIP];
    
    if ([[playButton currentImage] isEqual:downloadpng])
    {
        
        if (searchsepctag || searchTag)
        {
            if (self.tableView.tag==1 || self.tableView.tag==2 || self.tableView.tag==3 || self.tableView.tag==4 || self.tableView.tag==5 || self.tableView.tag==6 || self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9 || self.tableView.tag==10 || self.tableView.tag==11 || self.tableView.tag==12)
            {
                if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
                {
                    [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxInbox" object:messageinboxdata];
                    
                }
                else if (self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
                {
                    [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxSaved" object:messagesaveddata];
                }
                else if(self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
                {
                    [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxSent" object:messagesentdata];
                    
                }
            }
            else if(self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9)
            {
                if (self.tableView.tag==7)
                {
                    [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementInbox" object:announcementinboxdata];
                    
                }
                else  if (self.tableView.tag==8)
                {
                    [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementSaved" object:announcementsaveddata];
                    
                }
                else if (self.tableView.tag==9)
                {
                    [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementSent" object:announcementsentdata];
                }
            }
        }
        else
        {
            if (self.tableView.tag==1 || self.tableView.tag==2 || self.tableView.tag==3 || self.tableView.tag==4 || self.tableView.tag==5 || self.tableView.tag==6 || self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9 || self.tableView.tag==10 || self.tableView.tag==11 || self.tableView.tag==12)
            {
                if (self.tableView.tag==1 || self.tableView.tag==4 || self.tableView.tag==10)
                {
                    [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxInbox" object:messageinboxdata];
                    
                }
                else if (self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
                {
                    [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxSaved" object:messagesaveddata];
                    
                }
                else if(self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
                {
                    [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxSent" object:messagesentdata];
                }
            }
            if (self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9)
                
            {
                if (self.tableView.tag==7)
                {
                    [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementInbox" object:announcementinboxdata];
                    
                }
                else  if (self.tableView.tag==8)
                {
                    [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementSaved" object:announcementsaveddata];
                    
                }
                else if (self.tableView.tag==9)
                {
                    [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementSent" object:announcementsentdata];
                }
            }
        }
        [cell.playButton setImage:[UIImage imageNamed:@""]
                         forState:UIControlStateNormal];
        cell.playButton.userInteractionEnabled=NO;
        cell.activityView.hidden=NO;
        [cell.activityView startAnimating];
    }
    else
    {
        if (self.audioPlayer.duration<1.0)
        {
            NSError *error;
            
            self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[[AudioManager getsharedInstance]getEarboxAudioPath:audioString] error:&error];
            self.audioPlayer.delegate=self;
            if (error)
            {
                NSLog(@"Inbox error in audioPlayer: %@",
                      [error localizedDescription]);
            }
            if ([self.audioPlayer duration] >1.0)
            {
                cell.duration.text = audiolength;
                
                cell.playButton.userInteractionEnabled=YES;
                cell.playButton.backgroundColor=kNavBackgroundColor;
                [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                                 forState:UIControlStateNormal];
                NSLog(@"Inbox play button pressed --This %@ audio file found in Database play button enabled",audiologString);
            }
            else
            {
                cell.playButton.backgroundColor=[UIColor grayColor];
                //cell.playButton.userInteractionEnabled=NO;
                cell.circularProgressView.trackTintColor=[UIColor whiteColor];
                [cell.playButton setImage:[UIImage imageNamed:@"download"]
                                 forState:UIControlStateNormal];
                NSLog(@"Inbox play button pressed --This %@ audio file not found in Database play button disabled",audiologString);
            }
        }
        [self.timer invalidate];
        //play audio for the first time or if pause was pressed
        if (!self.isPaused) {
            
            [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_pause"]
                             forState:UIControlStateNormal];
            // Set a timer which keep getting the current music time and update the UISlider in 1 sec interval
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
            // Set the maximum value of the UISlider
            cell.currentTimeSlider.maximumValue = self.audioPlayer.duration;
            // Set the valueChanged target
            [cell.currentTimeSlider addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
            
            // Play the audio
            [self.audioPlayer prepareToPlay];
            [self.audioPlayer play];
            self.isPaused = TRUE;
            [self setupSliderTap];
            
        } else {
            //player is paused and Button is pressed again
            [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                             forState:UIControlStateNormal];
            
            [self.audioPlayer pause];
            self.isPaused = FALSE;
        }
        
    }
}

- (void)updateSlider
{
    if (self.audioPlayer.playing)
    {
        // Update the slider about the music time
        cell.currentTimeSlider.value = self.audioPlayer.currentTime;
        cell.timeElapsed.text = [NSString stringWithFormat:@"%@",
                                 [self timeFormat:[self.audioPlayer currentTime]]];
        
        cell.duration.text = [NSString stringWithFormat:@"%@",
                              [self timeFormat:[self.audioPlayer duration] - [self.audioPlayer currentTime]]];
    }
}

- (IBAction)sliderChanged:(UISlider *)sender {
    // Fast skip the music when user scroll the UISlider
    if (self.audioPlayer.playing)
    {
        [self.audioPlayer stop];
        [self.audioPlayer setCurrentTime:cell.currentTimeSlider.value];
        [self.audioPlayer prepareToPlay];
        [self.audioPlayer play];
        [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_pause"]
                         forState:UIControlStateNormal];
    }
}


-(NSString*)timeFormat:(float)value{
    
    float minutes = floor(lroundf(value)/60);
    float seconds = lroundf(value) - (minutes * 60);
    
    long roundedSeconds = lroundf(seconds);
    long roundedMinutes = lroundf(minutes);
    
    NSString *time = [[NSString alloc]
                      initWithFormat:@"%ld:%02ld",
                      roundedMinutes, roundedSeconds];
    return time;
}

- (void)setupSliderTap
{
    UITapGestureRecognizer *gesture;
    
    if(cell.currentTimeSlider == nil)
        return;
    
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSliderTap:)];
    [cell.currentTimeSlider addGestureRecognizer:gesture];
}

- (void)handleSliderTap:(UIGestureRecognizer *)gesture
{
    CGPoint point;
    CGFloat ratio;
    CGFloat delta;
    CGFloat value;
    CGFloat thumbWidth;
    BOOL isPlaying;
    if (self.audioPlayer.playing)
    {
        // tap on thumb, let slider deal with it
        if (cell.currentTimeSlider.highlighted)
            return;
        
        isPlaying = (self.audioPlayer.rate > 0);
        CGRect trackRect = [cell.currentTimeSlider trackRectForBounds:cell.currentTimeSlider.bounds];
        CGRect thumbRect = [cell.currentTimeSlider thumbRectForBounds:cell.currentTimeSlider.bounds trackRect:trackRect value:0];
        CGSize thumbSize = thumbRect.size;
        thumbWidth = thumbSize.width;
        point = [gesture locationInView: cell.currentTimeSlider];
        if(point.x < thumbWidth/2)
        {
            ratio = 0;
        }
        else if(point.x > cell.currentTimeSlider.bounds.size.width - thumbWidth/2)
        {
            ratio = 1;
        }
        else
        {
            ratio = (point.x - thumbWidth/2) / (cell.currentTimeSlider.bounds.size.width - thumbWidth);
        }
        delta = ratio * (cell.currentTimeSlider.maximumValue - cell.currentTimeSlider.minimumValue);
        value =cell.currentTimeSlider.minimumValue + delta;
        [cell.currentTimeSlider setValue:value animated:YES];
        [self updatePlayer:isPlaying];
    }
}

- (void)updatePlayer:(BOOL)playIfNeeded
{
    [self.audioPlayer stop];
    [self.audioPlayer setCurrentTime:cell.currentTimeSlider.value];
    [self.audioPlayer prepareToPlay];
    [self.audioPlayer play];
    [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_pause"]
                     forState:UIControlStateNormal];
}

-(void)audioPlayerStop
{
    if ([self.audioPlayer isPlaying])
    {
        [self.audioPlayer pause];
        [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                         forState:UIControlStateNormal];
        self.isPaused = FALSE;
    }
    self.audioPlayer = nil;
    [[AudioManager getsharedInstance]audioplayerstop];
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [self.tableView beginUpdates];
    if (self.tableView.tag==1 || self.tableView.tag==2 || self.tableView.tag==3 || self.tableView.tag==4 || self.tableView.tag==5 || self.tableView.tag==6 || self.tableView.tag==10 || self.tableView.tag==11 || self.tableView.tag==12)
    {
        if (self.tableView.tag==1 || self.tableView.tag==4 ||  self.tableView.tag==10)
        {
            [self audioplayerended];
            messageinboxdata=[inboxArray objectAtIndex:rowindex];
            cell.duration.text=[NSDateClass lengthformate:messageinboxdata.duration];
            if ([messageinboxdata.status isEqualToString:@"new"])
            {
                [self updateheardstatus:messageinboxdata.pushTxId status:@"messageinbox" modeId:@"heard" tabletag:self.tableView.tag];
            }
            
        }
        else if (self.tableView.tag==2 || self.tableView.tag==5 || self.tableView.tag==11)
        {
            [self audioplayerended];
            messagesaveddata=[savedArray objectAtIndex:rowindex];
            cell.duration.text=[NSDateClass lengthformate:messagesaveddata.duration];
            if ([messagesaveddata.status isEqualToString:@"new"])
            {
                [self updateheardstatus:messagesaveddata.pushTxId status:@"messagesaved" modeId:@"heard" tabletag:self.tableView.tag];
            }
            
        }
        else if (self.tableView.tag==3 || self.tableView.tag==6 || self.tableView.tag==12)
        {
            [self audioplayerended];
            messagesentdata=[sentArray objectAtIndex:rowindex];
            cell.duration.text=[NSDateClass lengthformate:messagesentdata.duration];
        }
    }
    else if(self.tableView.tag==7 || self.tableView.tag==8 || self.tableView.tag==9)
    {
        if (self.tableView.tag==7)
        {
            [self audioplayerended];
            announcementinboxdata=[inboxArray objectAtIndex:rowindex];
            cell.duration.text=[NSDateClass lengthformate:announcementinboxdata.duration];
            if ([announcementinboxdata.status isEqualToString:@"new"])
            {
                [self updateheardstatus:announcementinboxdata.pushTxId status:@"announcementinbox" modeId:@"heard" tabletag:self.tableView.tag];
            }
        }
        else if (self.tableView.tag==8)
        {
            [self audioplayerended];
            announcementsaveddata=[savedArray objectAtIndex:rowindex];
            cell.duration.text=[NSDateClass lengthformate:announcementsaveddata.duration];
            if ([announcementsaveddata.status isEqualToString:@"new"])
            {
                [self updateheardstatus:announcementsaveddata.pushTxId status:@"announcementsaved" modeId:@"heard" tabletag:self.tableView.tag];
            }
        }
        else if (self.tableView.tag==9)
        {
            [self audioplayerended];
            announcementsentdata=[sentArray objectAtIndex:rowindex];
            cell.duration.text=[NSDateClass lengthformate:announcementsentdata.duration];
        }
    }
}

-(void)audioplayerended
{
    cell.currentTimeSlider.value = 00.00;
    cell.timeElapsed.text = @"00:00";
    [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                     forState:UIControlStateNormal];
    
    [self.tableView endUpdates];
    self.isPaused = FALSE;
}


#pragma mark deleteExpiredMsgs methods
-(void)updatetimer_EarBox
{
    if (!isReplying)
    {
        int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
        int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementtotalcount"] intValue];
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount+announcementcount] forKey:@"badgecount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
        if (selectedRows.count==0 )
        {
            [self tableViewUpdate];
        }
    }
}

-(void)deleteExpiredMsgs
{
    if (self.tableView.tag==1)
    {
        [self getmessageinboxdata:4];
        for (int i=0; i<[inboxArray count]; i++)
        {
            messageinboxdata=[inboxArray objectAtIndex:i];
            if ([[NSDateClass ttlformate:messageinboxdata.ttl] isEqualToString:@"0"])
            {
                
                
                if ([messageinboxdata.status isEqualToString:@"new"])
                {
                    int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                    if (messagecount==0)
                    {
                        messagecount=0;
                    }
                    else
                    {
                        messagecount--;
                    }
                    int privateMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"privateMessageCountdef"] intValue];
                    privateMessageCountdef--;
                    if (privateMessageCountdef>0)
                    {
                        privateMessageCount.hidden=NO;
                        privateMessageCount.text= [NSString stringWithFormat:@"%d",privateMessageCountdef];
                        
                    }
                    else
                    {
                        privateMessageCount.hidden=YES;
                    }
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:privateMessageCountdef] forKey:@"privateMessageCountdef"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagetotalcount"];
                    int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@%d%d", messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid]];
                [store deleteEarboxInbox:messageinboxdata];
            }
        }
        int privateMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"privateMessageCountdef"] intValue];
        
        if (privateMessageCountdef>0)
        {
            privateMessageCount.hidden=NO;
            privateMessageCount.text= [NSString stringWithFormat:@"%d",privateMessageCountdef];
            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:privateMessageCountdef] forKey:@"privateMessageCountdef"];
        }
        else
        {
            privateMessageCount.hidden=YES;
        }
    }
    if (self.tableView.tag==7)
    {
        [self getannouncementinboxdata:@"AnnouncementInbox"];
        for (int i=0; i<[inboxArray count]; i++)
        {
            announcementinboxdata=[inboxArray objectAtIndex:i];
            
            if ([[NSDateClass ttlformate:announcementinboxdata.ttl] isEqualToString:@"0"])
            {
                
                
                if ([announcementinboxdata.status isEqualToString:@"new"])
                {
                    int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementtotalcount"] intValue];
                    if (announcementcount==0)
                    {
                        announcementcount=0;
                    }
                    else
                    {
                        announcementcount--;
                    }
                    int storeAnnouncementCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeAnnouncementCountdef"] intValue];
                    storeAnnouncementCountdef--;
                    if (storeAnnouncementCountdef>0)
                    {
                        storeAnnouncementCount.hidden=NO;
                        storeAnnouncementCount.text= [NSString stringWithFormat:@"%d",storeAnnouncementCountdef];
                        
                    }
                    else
                    {
                        storeAnnouncementCount.hidden=YES;
                    }
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:storeAnnouncementCountdef]
                                                             forKey:@"storeAnnouncementCountdef"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:announcementcount]
                                                             forKey:@"announcementtotalcount"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@%d%d",announcementinboxdata.callId,announcementinboxdata.callType,announcementinboxdata.storeid]];
                [store deleteAnnouncementInbox:announcementinboxdata];
            }
        }
        int storeAnnouncementCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeAnnouncementCountdef"] intValue];
        if (storeAnnouncementCountdef>0)
        {
            storeAnnouncementCount.hidden=NO;
            storeAnnouncementCount.text= [NSString stringWithFormat:@"%d",storeAnnouncementCountdef];
            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:storeAnnouncementCountdef]
                                                     forKey:@"storeAnnouncementCountdef"];
        }
        else
        {
            storeAnnouncementCount.hidden=YES;
        }
    }
    if (self.tableView.tag==4)
    {
        [self getmessageinboxdata:17];
        for (int i=0; i<[inboxArray count]; i++)
        {
            messageinboxdata=[inboxArray objectAtIndex:i];
            if ([[NSDateClass ttlformate:messageinboxdata.ttl] isEqualToString:@"0"])
            {
                
                
                if ([messageinboxdata.status isEqualToString:@"new"])
                {
                    int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                    if (messagecount==0)
                    {
                        messagecount=0;
                    }
                    else
                    {
                        messagecount--;
                    }
                    int groupMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"groupMessageCountdef"] intValue];
                    groupMessageCountdef--;
                    if (groupMessageCountdef>0)
                    {
                        groupMessageCount.hidden=NO;
                        groupMessageCount.text= [NSString stringWithFormat:@"%d",groupMessageCountdef];
                        
                    }
                    else
                    {
                        groupMessageCount.hidden=YES;
                    }
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:groupMessageCountdef] forKey:@"groupMessageCountdef"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagetotalcount"];
                    int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@%d%d", messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid]];
                [store deleteEarboxInbox:messageinboxdata];
            }
        }
        int groupMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"groupMessageCountdef"] intValue];
        
        if (groupMessageCountdef>0)
        {
            groupMessageCount.hidden=NO;
            groupMessageCount.text= [NSString stringWithFormat:@"%d",groupMessageCountdef];
            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:groupMessageCountdef] forKey:@"groupMessageCountdef"];
        }
        else
        {
            groupMessageCount.hidden=YES;
        }
    }
    if (self.tableView.tag==10)
    {
        [self getmessageinboxdata:33];
        for (int i=0; i<[inboxArray count]; i++)
        {
            messageinboxdata=[inboxArray objectAtIndex:i];
            if ([[NSDateClass ttlformate:messageinboxdata.ttl] isEqualToString:@"0"])
            {
                
                
                if ([messageinboxdata.status isEqualToString:@"new"])
                {
                    int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                    if (messagecount==0)
                    {
                        //messagecount=0;
                    }
                    else
                    {
                        messagecount--;
                    }
                    int everyoneMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"everyoneMessageCountdef"] intValue];
                    everyoneMessageCountdef--;
                    if (everyoneMessageCountdef>0)
                    {
                        everyoneMessageCount.hidden=NO;
                        everyoneMessageCount.text= [NSString stringWithFormat:@"%d",everyoneMessageCountdef];
                        
                    }
                    else
                    {
                        everyoneMessageCount.hidden=YES;
                    }
                    
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:everyoneMessageCountdef] forKey:@"everyoneMessageCountdef"];
                    int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@%d%d", messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid]];
                [store deleteEarboxInbox:messageinboxdata];
            }
        }
        int everyoneMessageCountdef=[[[NSUserDefaults standardUserDefaults]objectForKey:@"everyoneMessageCountdef"] intValue];
        
        if (everyoneMessageCountdef>0)
        {
            everyoneMessageCount.hidden=NO;
            everyoneMessageCount.text= [NSString stringWithFormat:@"%d",everyoneMessageCountdef];
            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:everyoneMessageCountdef] forKey:@"everyoneMessageCountdef"];
        }
        else
        {
            everyoneMessageCount.hidden=YES;
        }
    }
}

#pragma mark tableViewUpdate
-(void)tableViewUpdate
{
    NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
    if (self.audioPlayer.isPlaying)
    {
        
    }
    else
    {
        [self.tableView reloadData];
        
        
        if (selectedRows.count==0 )
        {
            
            [self deleteExpiredMsgs];
            
        }
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        
        switch (self.tableView.tag)
        {
            case 1:
                [self getmessageinboxdata:4];
                if (inboxArray.count==0)
                {
                    searchbutton.enabled=NO;
                    searchsepcbutton.enabled=NO;
                }
                else
                {
                    if(!replayCancelEnable)
                    {
                        searchbutton.enabled=YES;
                        searchsepcbutton.enabled=YES;
                    }
                }
                
                break;
            case 2:
                [self getmessagesaveddata:4];
                if (savedArray.count==0)
                {
                    searchbutton.enabled=NO;
                    searchsepcbutton.enabled=NO;
                }
                else
                {
                    if(!replayCancelEnable)
                    {
                        searchbutton.enabled=YES;
                        searchsepcbutton.enabled=YES;
                    }
                }
                
                break;
            case 3:
                [self getmessagesentdata:4];
                if (sentArray.count==0)
                {
                    searchbutton.enabled=NO;
                    searchsepcbutton.enabled=NO;
                }
                else
                {
                    searchbutton.enabled=YES;
                    searchsepcbutton.enabled=YES;
                }
                break;
            case 7:
                [self getannouncementinboxdata:@"AnnouncementInbox"];
                if (inboxArray.count==0)
                {
                    searchbutton.enabled=NO;
                    searchsepcbutton.enabled=NO;
                }
                else
                {
                    searchbutton.enabled=YES;
                    searchsepcbutton.enabled=YES;
                }
                
                break;
            case 8:
                [self getannouncementsaveddata:@"AnnouncementSaved"];
                if (savedArray.count==0)
                {
                    searchbutton.enabled=NO;
                    searchsepcbutton.enabled=NO;
                }
                else
                {
                    searchbutton.enabled=YES;
                    searchsepcbutton.enabled=YES;
                }
                
                break;
            case 9:
                [self getannouncementsentdata:@"AnnouncementSent"];
                if (sentArray.count==0)
                {
                    searchbutton.enabled=NO;
                    searchsepcbutton.enabled=NO;
                }
                else
                {
                    searchbutton.enabled=YES;
                    searchsepcbutton.enabled=YES;
                }
                
                break;
            case 4:
                [self getmessageinboxdata:17];
                if (inboxArray.count==0)
                {
                    searchbutton.enabled=NO;
                    searchsepcbutton.enabled=NO;
                }
                else
                {
                    if(!replayCancelEnable)
                    {
                        searchbutton.enabled=YES;
                        searchsepcbutton.enabled=YES;
                    }
                }
                
                break;
            case 5:
                [self getmessagesaveddata:17];
                if (savedArray.count==0)
                {
                    searchbutton.enabled=NO;
                    searchsepcbutton.enabled=NO;
                }
                else
                {
                    if(!replayCancelEnable)
                    {
                        searchbutton.enabled=YES;
                        searchsepcbutton.enabled=YES;
                    }
                }
                
                break;
            case 6:
                [self getmessagesentdata:17];
                if (sentArray.count==0)
                {
                    searchbutton.enabled=NO;
                    searchsepcbutton.enabled=NO;
                }
                else
                {
                    searchbutton.enabled=YES;
                    searchsepcbutton.enabled=YES;
                }
                
                break;
            case 10:
                [self getmessageinboxdata:33];
                if (inboxArray.count==0)
                {
                    searchbutton.enabled=NO;
                    searchsepcbutton.enabled=NO;
                }
                else
                {
                    if(!replayCancelEnable)
                    {
                        searchbutton.enabled=YES;
                        searchsepcbutton.enabled=YES;
                    }
                }
                
                break;
            case 11:
                [self getmessagesaveddata:33];
                if (savedArray.count==0)
                {
                    if(!replayCancelEnable)
                    {
                        searchbutton.enabled=NO;
                        searchsepcbutton.enabled=NO;
                    }
                }
                else
                {
                    searchbutton.enabled=YES;
                    searchsepcbutton.enabled=YES;
                }
                
                break;
            case 12:
                [self getmessagesentdata:33];
                if (sentArray.count==0)
                {
                    searchbutton.enabled=NO;
                    searchsepcbutton.enabled=NO;
                }
                else
                {
                    searchbutton.enabled=YES;
                    searchsepcbutton.enabled=YES;
                }
                
                break;
        }
        if (searchTag)
        {
            [self SearchbarSearchText:inboxSearchText];
        }
        else if (![inboxsortText isEqualToString:@""])
        {
            [self sortingprivate_everyone_group_announcement:inboxsortText];
            
        }
        else
        {
            [UIView animateWithDuration:0 animations:^{
                [self.tableView reloadData];
            } completion:^(BOOL finished) {
            }];
        }
        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
    }
    
}

#pragma mark deviceLockNotification
-(void)enterLockNotification
{
    [self.tableView reloadData];
    [self audioPlayerStop];
    if (isReplying)
    {
        [self replybuttonUpPressed:nil];
    }
}

#pragma mark requestUndeliveredMsgsinbox for private/everyone/group/announcement
-(void)requestUndeliveredMsgsinbox
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"InboxVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"EarboxundeliveredlistV2"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        //requesting for undelivered messages and announcements......
        companyId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"companyId"]intValue];
        NSOperationQueue *myQueuemessage = [[NSOperationQueue alloc] init];
        [myQueuemessage addOperationWithBlock:^{
            NSDictionary *twundeliveredMsgListdic= [HttpHandler earboxundeliveredlistV2:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId twreqFor:@"message" userScreen:@"InboxPrivateMsgInboxVC"];
            NSDictionary *twundeliveredAnccListdic= [HttpHandler earboxundeliveredlistV2:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId twreqFor:@"announcement" userScreen:@"InboxStoreMsgInboxVC"];
            NSDictionary *twundeliveredGroupListdic= [HttpHandler earboxundeliveredlistV2:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId twreqFor:@"groupmessage" userScreen:@"InboxGroupMsgInboxVC"];
            NSDictionary *twundeliveredEveryoneListdic= [HttpHandler earboxundeliveredlistV2:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId twreqFor:@"everyone" userScreen:@"InboxEveryoneInboxVC"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if (![[[twundeliveredMsgListdic objectForKey:@"twResponseHeader" ] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]] || ![[[twundeliveredMsgListdic objectForKey:@"twResponseHeader" ] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]] || ![[[twundeliveredMsgListdic objectForKey:@"twResponseHeader" ] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
                {
                    NSArray *messageinbox,*messagesent,*messagesaved,*announcementinbox,*announcementsent,*announcementsaved,
                    *groupinbox,*groupsent,*groupsaved,*everyoneinbox,*everyonesent,*everyonesaved;
                    if ([[twundeliveredMsgListdic objectForKey:@"earbox"] isKindOfClass:[NSNull class]])
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredfailedInbox" forKey:@"undeliveredfailedInbox"];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    else
                    {
                        NSDictionary *message=[[twundeliveredMsgListdic objectForKey:@"earbox"]objectForKey:@"message"];
                        
                        for (int i=0; i<message.count; i++)
                        {
                            messageinbox=[message objectForKey:@"inbox"];
                            messagesent=[message  objectForKey:@"sent"];
                            messagesaved=[message objectForKey:@"saved"];
                        }
                        if (messageinbox.count>0)
                        {
                            [earboxinboxmodelStore messageinboxSave:messageinbox];
                            NSInteger messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:messagecount+[messageinbox count]] forKey:@"messagetotalcount"];
                        }
                        if (messagesaved.count>0)
                        {
                            [earboxinboxmodelStore messagesavedSave:messagesaved];
                            NSInteger messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:messagecount+[messagesaved count]] forKey:@"messagetotalcount"];
                        }
                        
                        if (messagesent.count>0)
                        {
                            [earboxinboxmodelStore messagesentSave:messagesent];
                        }
                    }
                    if ([[twundeliveredAnccListdic objectForKey:@"earbox"] isKindOfClass:[NSNull class]])
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredfailedInbox" forKey:@"undeliveredfailedInbox"];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    else
                    {
                        NSDictionary *announcement=[[twundeliveredAnccListdic objectForKey:@"earbox"] objectForKey:@"announcement"];
                        
                        for (int i=0; i<announcement.count; i++)
                        {
                            announcementinbox=[announcement objectForKey:@"inbox"];
                            
                            announcementsent=[announcement  objectForKey:@"sent"];
                            
                            announcementsaved=[announcement  objectForKey:@"saved"];
                            
                        }
                        if (announcementinbox.count>0)
                        {
                            [earboxinboxmodelStore announcementinboxSave:announcementinbox];
                            int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementtotalcount"] intValue];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:[announcementinbox count]+announcementcount] forKey:@"announcementtotalcount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        if (announcementsaved.count>0)
                        {
                            [earboxinboxmodelStore announcementsavedSave:announcementsaved];
                            int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementtotalcount"] intValue];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:[announcementsaved count]+announcementcount] forKey:@"announcementtotalcount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        
                        if (announcementsent.count>0)
                        {
                            [earboxinboxmodelStore announcementsentSave:announcementsent];
                        }
                    }
                    if ([[twundeliveredGroupListdic objectForKey:@"earbox"] isKindOfClass:[NSNull class]])
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredfailedInbox" forKey:@"undeliveredfailedInbox"];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    else
                    {
                        NSDictionary *group=[[twundeliveredGroupListdic objectForKey:@"earbox"] objectForKey:@"groupMessage"];
                        
                        for (int i=0; i<group.count; i++)
                        {
                            groupinbox=[group objectForKey:@"inbox"];
                            
                            groupsent=[group  objectForKey:@"sent"];
                            
                            groupsaved=[group  objectForKey:@"saved"];
                            
                        }
                        if (groupinbox.count>0)
                        {
                            [earboxinboxmodelStore messageinboxSave:groupinbox];
                            int groupcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:[groupinbox count]+groupcount] forKey:@"messagetotalcount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        if (groupsaved.count>0)
                        {
                            [earboxinboxmodelStore messagesavedSave:groupsaved];
                            int groupcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:[groupsaved count]+groupcount] forKey:@"messagetotalcount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        if (groupsent.count>0)
                        {
                            [earboxinboxmodelStore messagesentSave:groupsent];
                        }
                    }
                    
                    if ([[twundeliveredEveryoneListdic objectForKey:@"earbox"] isKindOfClass:[NSNull class]])
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredfailedInbox" forKey:@"undeliveredfailedInbox"];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    else
                    {
                        NSDictionary *everyone=[[twundeliveredEveryoneListdic objectForKey:@"earbox"] objectForKey:@"everyone"];
                        
                        for (int i=0; i<everyone.count; i++)
                        {
                            everyoneinbox=[everyone objectForKey:@"inbox"];
                            
                            everyonesent=[everyone  objectForKey:@"sent"];
                            
                            everyonesaved=[everyone  objectForKey:@"saved"];
                            
                        }
                        if (everyoneinbox.count>0)
                        {
                            [earboxinboxmodelStore messageinboxSave:everyoneinbox];
                            int everyonecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:[everyoneinbox count]+everyonecount] forKey:@"messagetotalcount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        if (everyonesaved.count>0)
                        {
                            [earboxinboxmodelStore messagesavedSave:everyonesaved];
                            int everyonecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:[everyonesaved count]+everyonecount] forKey:@"messagetotalcount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        if (everyonesent.count>0)
                        {
                            [earboxinboxmodelStore messagesentSave:everyonesent];
                        }
                    }
                    NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
                    if (selectedRows.count==0 )
                    {
                        if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"]isEqualToString:@""])
                        {
                            [self inboxpreviousTabMethod];
                        }
                        else
                        {
                            if (!pushnotificationRecieved)
                            {
                                [self inboxtabIndexchange];
                                pushnotificationRecieved=NO;
                            }
                        }
                        [UIView animateWithDuration:0 animations:^{
                            [self tableViewUpdate];
                        } completion:^(BOOL finished) {
                        }];
                    }
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                }
                else
                {
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                }
            }];
        }];
    }
}

#pragma mark backtoforgroundNotification
-(void)reOpenClosedSocketsNotification
{
    [self inboxtotalcount ];
}

@end
