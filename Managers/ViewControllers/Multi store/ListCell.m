//
//  ListCell.m
//  RoomsToNite
//
//  Created by RoomsToniteMac3 on 25/06/15.
//  Copyright (c) 2015 RoomsToniteMac3. All rights reserved.
//

#import "ListCell.h"

@implementation ListCell


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawRect:(CGRect)rect
{
    self.count_lbl.layer.cornerRadius = self.count_lbl.frame.size.width / 2;
    self.count_lbl.clipsToBounds=YES;
    [super drawRect:rect];
}

@end
