//
//  MutilMessageViewController.h
//  Managers
//
//  Created by Ravi on 05/10/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EarboxDataStore.h"
#import "MarqueeLabel.h"

@interface MutilMessageViewController : UIViewController
{
    IBOutlet UILabel *messageLabel,*receivedMessageLabel;
    IBOutlet UITabBar *ann_TabBar;
    UIButton *nowbtn,*Hrsbtn,*weekbtn;
        NSInteger callType;
    IBOutlet UILabel *lbl_Green;
}
@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property(nonatomic,strong)NSString *contactString,*contactStatusString;
@property(nonatomic,assign) BOOL earbox;
-(void)audioInterruptionEnd;
+(MutilMessageViewController *)getsharedInstance;

- (IBAction)sendFileTapped:(id)sender;
- (IBAction)deleteFileTapped:(id)sender;
- (IBAction)playTapped:(id)sender;
@end
