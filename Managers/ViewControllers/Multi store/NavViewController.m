//
//  NavViewController.m
//  MultiStoreNavigation
//
//  Created by sandeepchalla on 11/3/16.
//  Copyright © 2016 sandeepchalla. All rights reserved.
//

#import "NavViewController.h"
#import "NavCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Constant.h"
#import "PlacesTableViewCell.h"
#import "ContactsViewController.h"
#import "Reachability.h"
#import "AlertView.h"
#import "HttpHandler.h"
#import "ActivityIndicatorView.h"
#import "InstoreData.h"
#import "MarqueeLabel.h"
#import "DiagnosticTool.h"

@interface NavViewController ()<UICollectionViewDelegateFlowLayout>
{
    NSArray *dataArray,*chainArray;
    NSMutableArray *chainSelectedArray,*levelArray,*searchResultsAll,*hierarchyAllArray;
    NSInteger cellrow;
    NSIndexPath *chainindexpath;
    NSString *universalId,*levelId,*storesState;
    NSMutableArray *mainArray;
    BOOL chainClicked;
    NSString *userName,*password;
    IBOutlet UISearchBar *searchBar;
    NSDictionary *dictLang;
    MarqueeLabel *_lectureName;
    
}
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) IBOutlet UITableView *chainTableview;

@end

@implementation NavViewController

- (void)viewDidLoad
{
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    
    
     _lectureName= [[MarqueeLabel alloc] initWithFrame:CGRectMake(100,20,60, 20) duration:8.0 andFadeLength:10.0f];
    [_lectureName setTextAlignment:NSTextAlignmentCenter];
    [_lectureName setBackgroundColor:[UIColor clearColor]];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    self.searchDisplayController.searchBar.placeholder=[dictLang objectForKey:@"Search"];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor=[UIColor greenColor];
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kNavBackgroundColor,
       NSFontAttributeName:[UIFont fontWithName:kFontName size:13]}];
   // self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    cellrow=0;
    [super viewDidLoad];
    clickedOnChildNode=NO;
    storeScreen=@"false";
    storesState=@"false";
    chainClicked=NO;
    dataArray = [[NSArray alloc] init];
    chainSelectedArray=[[NSMutableArray alloc]init];
    chainArray=[[NSArray alloc]init];
    mainArray = [[NSMutableArray alloc] init];
    levelArray = [[NSMutableArray alloc] init];
    hierarchyAllArray = [[NSMutableArray alloc] init];
    favArray = [[NSMutableArray alloc] init];
    
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    
    userName= [loginDetails objectForKey:@"twUserName"];
    password=[loginDetails objectForKey:@"twPassword"];
    
    // Create data for collection views
    NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
    
   
    favoritsButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"favorite"] style:UIBarButtonItemStyleDone target:self action:@selector(favoritsButtonAction)];
    self.navigationItem.rightBarButtonItem=favoritsButton;
    if ([fav isEqualToString:@"fav"])
    {
        [favoritsButton setEnabled:NO];
    }
    favoritsButton.enabled=NO;
    [self.collectionView registerClass:[NavCollectionViewCell class] forCellWithReuseIdentifier:@"cvCell"];
    // Configure layout
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    self.collectionView.layer.cornerRadius=4;
    self.collectionView.layer.borderWidth=1;
    [self.collectionView setCollectionViewLayout:flowLayout];
    [self.collectionView setContentOffset:CGPointMake(0, searchBar.frame.size.height)];
    
   
   
    [self hierarchy];
    
    
    _lectureName.adjustsFontSizeToFitWidth=NO;
    [_lectureName setAnimationCurve:UIViewAnimationOptionCurveEaseIn];
    _lectureName.marqueeType = MLContinuous;
    [self.navigationItem setTitleView:_lectureName];
    
}


-(void)favoritsButtonAction
{
     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Please enter the favorite name"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyNavVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"Please enter the favorite name"]] message:nil delegate:self cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
    alert.alertViewStyle=UIAlertViewStylePlainTextInput;
    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeASCIICapable];
    
    //[[alert textFieldAtIndex:1] becomeFirstResponder];
    [alert show];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

/*
 [self.view layoutIfNeeded];
 [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
 */

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==108)
    {
        [self favoritsButtonAction];
    }
    else
    {
        if (buttonIndex==1)
        {
            UITextField *group = [actionSheet textFieldAtIndex:0];
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
                
                group.text= [group.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                if (group.text.length>0 )
                {
                   // group.text= [group.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    NSMutableArray *newArray; //= [[NSMutableArray alloc] init];
                    NSMutableArray *nameArray = [[NSMutableArray alloc]init];
                    newArray= [[EarboxDataStore sharedStore] favoritesArtists];
                    
                    for (int i=0; i<[newArray count]; i++)
                    {
                        Favorites *fav;
                        fav=[newArray objectAtIndex:i];
                        
                        [nameArray addObject:fav.favouriteName];
                    }
                    
                    if ([nameArray containsObject:group.text])
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@ %@ (%@%@%@%@i)",[dictLang objectForKey:@"This"],[[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"]capitalizedString],[dictLang objectForKey:@"name already exists"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyNavVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"favorite name already exists"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                        alert.tag=108;
                        [alert show];
                    }
                    else
                    {
                        NSString *sl;
                        if (cellrow >=[dataArray count])
                        {
                            sl =[NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:cellrow-1] objectForKey:@"hierarchy_level_name"]];
                        }
                        else
                        {
                            sl =[NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:cellrow] objectForKey:@"hierarchy_level_name"]];
                        }
                    
                    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
                    [postData setObject:@"" forKey:@"PlayAudioType"];
                    [postData setObject:@"Nav" forKey:@"FinalView"];
                    [postData setObject:@[] forKey:@"Groups"];
                    [postData setObject:@[] forKey:@"Stores"];
                    //[postData setObject:@"" forKey:@"ChannelSected"];
                    [postData setObject:group.text forKey:@"FavouriteName"];
                    [postData setObject:sl forKey:@"MessageType"];
                    //[postData setObject:@"" forKey:@"Sender"];//featureType
                    [postData setObject:[NSString stringWithFormat:@"%ld",(long)cellrow] forKey:@"FeatureType"];
                    [postData setObject:@"MS-MA" forKey:@"Originator"];//PlatForm
                    [postData setObject:@"iOS" forKey:@"PlatForm"];//FavouriteID
                    [postData setObject:@"Hierarchy" forKey:@"FavouriteType"];
                    [postData setObject:storesState forKey:@"StoreScreen"];
                        [postData setObject:@[] forKey:@"StoreNames"];
                        
                        [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"Select"] forKey:@"MultiStore"];
                        [postData setObject:@"" forKey:@"HQTagOutName"];
                        [postData setObject:@[] forKey:@"HQManagerAppID"];
                        [postData setObject:@[] forKey:@"Nodes"];
                        [postData setObject:@[] forKey:@"MultiStoreNodeId"];
                        [postData setObject:@[] forKey:@"MultiStoreNodesNames"];
                        [postData setObject:@"" forKey:@"levelName"];
                        [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] forKey:@"storeselect"];
                        [postData setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"title"] forKey:@"title"];
                        //levelName
                    if (clickedOnChildNode)
                    {
                        [postData setObject:levelId forKey:@"NodeId"];
                        [postData setObject:universalId forKey:@"ParentId"];
                        [postData setObject:@"" forKey:@"LevelId"];
                    }
                    else
                    {
                        [postData setObject:@"" forKey:@"NodeId"];
                        [postData setObject:@"" forKey:@"ParentId"];
                        [postData setObject:hierarchyLevel forKey:@"LevelId"];
                    }
                    
                    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
                    
                    
                    
                    
                    NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
                    [myQueueSales addOperationWithBlock:^{
                        NSDictionary *responsDic=[HttpHandler favSave:[loginDetails objectForKey:@"twUserName"] password:[loginDetails objectForKey:@"twPassword"] jSonStr:postData userScreen:@"HierarchyNavVC"];
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            if ([responsDic objectForKey:@"Error"])
                            {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                                
                                [alert show];
                            }
                            else if ([responsDic objectForKey:@"FavouriteID"])
                            {
                            [postData setObject:[responsDic objectForKey:@"FavouriteID"] forKey:@"FavouriteID"];
                            [array addObject:postData];
                            [[Earboxinboxmodel sharedModel]favoritesInsert:array];
                            //[array addObject:responsDic];
                            
                            }
                            
                        }];
                    }];
                    }
                    
                }
                else
                {
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ %@ (%@%@%@%@i)",[[NSUserDefaults standardUserDefaults]objectForKey:@"fav_Tcm"],[dictLang objectForKey:@"name cannot be empty"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:@"favorite name cannot be empty"]] delegate:self cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
                    alert.tag=108;
                    [alert show];
                }

            
            
        }
    }
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        lbl_Green.backgroundColor = [UIColor greenColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}


-(void)viewWillAppear:(BOOL)animated
{
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"store"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"InstoreWebSocket"];
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{//OneStore
    //[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
    NSString *str =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
    
    if ([str isEqualToString:@"SL"])
    {
       [[NSUserDefaults standardUserDefaults] setObject:@"OneStore" forKey:@"Select"];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [super viewWillDisappear:animated];
}

-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ( [fromPush1 isEqualToString:@"YES"])
    {
        
        NSArray *viewcontrollers=[self.navigationController viewControllers];
        for (int i=0;i<[viewcontrollers count];i++)
        {
            UIViewController *vc=[viewcontrollers objectAtIndex:i];
            NSString *currentView=NSStringFromClass([vc class]);
            if ([currentView isEqualToString:@"VPViewController"])
            {
                [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
            }
        }
        
    }
    
    
}
-(void)hierarchy
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
         [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyNavVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"HierarchyLevels"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            NSDictionary * responsDic=[HttpHandler hierarchyV2:userName password:password userScreen:@"HierarchyNavVC"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                
                if ([[responsDic objectForKey:@"nodes"]  isKindOfClass:[NSNull class]] ||[[responsDic objectForKey:@"type"]  isKindOfClass:[NSNull class]])
                {
                    [AlertView alert:@"Error" message:[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]];
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                }
                else
                {
                    if ([responsDic objectForKey:@"Error"])
                    {
                        [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    else
                    {
                        NSArray *sortDataArray = [responsDic objectForKey:@"nodes"];
                        
                        if ([sortDataArray count]==0)
                        {
                            
                        }
                        else
                        {
                            [self.collectionView reloadData];
                            universalId = @"0";
                            levelId = @"0";
                            [mainArray addObject:@"0"];
                            [levelArray addObject:@"0"];
                            
                            NSSortDescriptor *descriptors=[[NSSortDescriptor alloc]initWithKey:@"hierarchy_level_weight" ascending:NO];
                            dataArray=[sortDataArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptors, nil]];
                            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeselect"]||[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"] ||[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"OneStore"])//storeEveryOne
                            {
                                if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"])
                                {
                                    _lectureName.text=[dictLang objectForKey:@"Message to Everyone"];//OneStore
                                }
                                else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"OneStore"])
                                {
                                    _lectureName.text=[dictLang objectForKey:@"Group Message"];
                                }
                                else
                                {
                                    _lectureName.text=[dictLang objectForKey:@"Message to an Individual"];
                                }
                                
                                
                            }
                            else
                            {
                            _lectureName.text=[dictLang objectForKey:@"Store Hierarchy"];//[[dataArray objectAtIndex:0] objectForKey:@"hierarchy_level_name"];
                            }
                            for (int i=0; i<[dataArray count]; i++)
                            {
                                if (i==0)
                                {
                                    [chainSelectedArray addObject:@"1"];
                                }
                                else
                                {
                                    [chainSelectedArray addObject:@"0"];
                                }
                            }
                            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                            [self hierarchyAll];
                            
                        }
                    }
                }
            }];
        }];
    }
}

-(void)childFav:(NSString *)levelIdFav universalIdFav:(NSString *)universalIdFav stores:(NSString *)stores
{
    if ([stores isEqualToString:@"false"])
    {
        storesState=@"false";
    }
    else
    {
        storesState=@"true";
    }
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
       [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyNavVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"HierarchyLevelParent"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            NSData * data=[HttpHandler hierarchyChildV2:levelIdFav universalId:universalIdFav userName:userName password:password storesState:storesState userScreen:@"HierarchyNavVC"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                
                NSHTTPURLResponse *responce=nil;
                NSError *error;
                NSDictionary *userInfo = [error userInfo];
                NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
                NSDictionary *responsDic;
                if ([data length]==0)
                {
                    
                    if (responce.statusCode==0)
                    {
                        //responsDic=[NSDictionary dictionaryWithObjectsAndKeys:errorString,@"Error", nil];
                        NSLog(@"Hierarchy error is:%@",errorString);
                    }
                    else
                    {
                       // responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[HttpHandler statusString:responce.statusCode],@"Error", nil];
                    }
                }
                else
                {
                    if (responce.statusCode==502 || responce.statusCode==500)
                    {
                       
                       // responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[HttpHandler statusString:responce.statusCode],@"Error", nil];
                    }
                    else
                    {
                        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                        NSLog(@"MS-Hierarchy child responce %@",responsDic);
                        if ([[responsDic objectForKey:@"nodes"]  isKindOfClass:[NSNull class]] ||[[responsDic objectForKey:@"type"]  isKindOfClass:[NSNull class]])
                        {
                            if ([responsDic objectForKey:@"Error"])
                            {
                                [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                            }
                        }
                        else
                        {
                            if ([responsDic objectForKey:@"Error"])
                            {
                                [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"(%@i)",[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                            }
                            else
                            {
                                NSArray *count; //= [[NSArray alloc] init];
                                count=[responsDic objectForKey:@"nodes"];
                                
                                
                                if ([count count]==0)
                                {
                                    
                                    
                                }
                                else
                                {
                                    
                                        //[chainSelectedArray replaceObjectAtIndex:cellrow withObject:@"1"];
                                        //self.navigationItem.title=[[[dataArray objectAtIndex:cellrow] objectForKey:@"hierarchy_level_name"]componentsSeparatedByString:@">"][0];
                                        [self.collectionView reloadData];
                                        if (cellrow>([dataArray count]/[dataArray count]))
                                        {
                                            /*
                                            [self.collectionView scrollToItemAtIndexPath:chainindexpath
                                                                        atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
                                             */
                                            [self.collectionView scrollToItemAtIndexPath:chainindexpath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
                                            
                                        }
                                    
                                    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"hierarchyNodeName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                                    chainArray=[count sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
                                    [self.collectionView reloadData];
                                    [self.chainTableview reloadData];
                                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                                }
                                
                            }
                        }
                    }
                    
                    
                }
            }];
        }];
    }

}

-(void)hierarchyChild
{
    if (cellrow==([dataArray count]-1))
    {
        storesState=@"true";
        storeScreen=@"true";
    }
    else
    {
        storesState = @"false";
    }
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyNavVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"HierarchyLevelParent"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            NSData * data=[HttpHandler hierarchyChildV2:levelId universalId:universalId userName:userName password:password storesState:storesState userScreen:@"HierarchyNavVC"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                
                NSHTTPURLResponse *responce=nil;
                NSError *error;
                NSDictionary *userInfo = [error userInfo];
                NSString *errorString = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
                NSDictionary *responsDic;
                if ([data length]==0)
                {
                    if (cellrow>0)
                    {
                        cellrow--;
                    }
                    
                    if (responce.statusCode==0)
                    {
                       // responsDic=[NSDictionary dictionaryWithObjectsAndKeys:errorString,@"Error", nil];
                        NSLog(@"Hierarchy error is:%@",errorString);
                    }
                    else
                    {
                        //responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[HttpHandler statusString:responce.statusCode],@"Error", nil];
                    }
                }
                else
                {
                    if (responce.statusCode==502 || responce.statusCode==500)
                    {
                        if (cellrow>0)
                        {
                            cellrow--;
                        }
                       // responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[HttpHandler statusString:responce.statusCode],@"Error", nil];
                    }
                    else
                    {
                        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                        NSLog(@"MS-Hierarchy child responce %@",responsDic);
                        if ([[responsDic objectForKey:@"nodes"]  isKindOfClass:[NSNull class]] ||[[responsDic objectForKey:@"type"]  isKindOfClass:[NSNull class]])
                        {
                            if ([responsDic objectForKey:@"Error"])
                            {
                                [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                            }
                            else if([[responsDic objectForKey:@"nodes"]  isKindOfClass:[NSNull class]] ||[[responsDic objectForKey:@"type"]  isKindOfClass:[NSNull class]])
                            {
                                NSLog(@"Hierarchy-%@",[[dataArray objectAtIndex:cellrow-1] objectForKey:@"hierarchy_level_name"]);
                                if (cellrow>0)
                                {
                                    NSString *string;
                                    NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
                                    if ([fav isEqualToString:@"fav"])
                                    {
                                        string = [NSString stringWithFormat:@"%@ %@",[[dataArray objectAtIndex:cellrow-1] objectForKey:@"hierarchy_level_name"],[dictLang objectForKey:@"are not configured"]];
                                    }
                                    else
                                    {
                                       string = [NSString stringWithFormat:@"%@ %@",[[dataArray objectAtIndex:cellrow] objectForKey:@"hierarchy_level_name"],[dictLang objectForKey:@"are not configured"]];
                                    }
                                    
//                                    [mainArray removeObjectAtIndex:cellrow];
//                                    
//                                    [levelArray removeObjectAtIndex:cellrow];
                                    
                                    cellrow--;
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                                    message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",string,[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"HierarchyLevelParent"],[[DiagnosticTool sharedManager]generateErrorCode:@"hierarchy_level_name are not configured"]]
                                                                                   delegate:self
                                                                          cancelButtonTitle:[dictLang objectForKey:@"OK"]
                                                                          otherButtonTitles:nil];
                                    [alert show];
                                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                                }
                            }
                        }
                        else
                        {
                            if ([responsDic objectForKey:@"Error"])
                            {
                                [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"(%@i)",[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                            }
                            else
                            {
                                NSArray *count; //= [[NSArray alloc] init];
                                count=[responsDic objectForKey:@"nodes"];
                                
                                
                                if ([count count]==0)
                                {
                                    if (cellrow>0)
                                    {
                                        [levelArray removeObjectAtIndex:cellrow];
                                        [mainArray removeObjectAtIndex:cellrow];
                                        cellrow--;
                                    }
                                    
                                }
                                else
                                {
                                    if (chainClicked)
                                    {
                                        chainClicked=NO;
                                    }
                                    else
                                    {
                                        if(cellrow==0)
                                        {
                                            
                                        }
                                        else
                                        {
                                            
                                        }
                                    }
                                    
                                    if (cellrow==0)
                                    {
                                        
                                    }
                                    else
                                    {
                                        [chainSelectedArray replaceObjectAtIndex:cellrow withObject:@"1"];
                                        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeselect"]||[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"] ||[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"OneStore"])//storeEveryOne
                                        {
                                            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"])
                                            {
                                                _lectureName.text=[dictLang objectForKey:@"Message to Everyone"];//OneStore
                                            }
                                            else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"OneStore"])
                                            {
                                                _lectureName.text=[dictLang objectForKey:@"Group Message"];
                                            }
                                            else
                                            {
                                                _lectureName.text=[dictLang objectForKey:@"Message to an Individual"];
                                            }
                                            
                                            
                                        }
                                        else
                                        {
                                        _lectureName.text=[dictLang objectForKey:@"Store Hierarchy"];//[[[dataArray objectAtIndex:cellrow] objectForKey:@"hierarchy_level_name"]
                                                                   //componentsSeparatedByString:@">"][0];
                                        }
                                        
                                        [self.collectionView reloadData];
                                        if (cellrow>([dataArray count]/[dataArray count]))
                                        {
//                                           [self.collectionView scrollToItemAtIndexPath:chainindexpath
//                                                                       atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
                                            
                                            [self.collectionView scrollToItemAtIndexPath:chainindexpath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
                                            
                                        }
                                    }
                                    
                                    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"hierarchyNodeName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                                    chainArray=[count sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
                                    [self.collectionView reloadData];
                                    [self.chainTableview reloadData];
                                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                                }
                                
                            }
                        }
                    }
                    
                    
                }
            }];
        }];
    }
    
}

#pragma mark Collection view layout things
// Layout: Set cell size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellData = [[dataArray objectAtIndex:indexPath.row] objectForKey:@"hierarchy_level_name"];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:17];
    NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                     NSForegroundColorAttributeName: [UIColor blackColor]};
    const CGSize textSize = [cellData sizeWithAttributes: userAttributes];
    CGSize mElementSize = CGSizeMake(textSize.width+20, 40);
    return mElementSize;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [dataArray count];
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // Setup cell identifier
    static NSString *cellIdentifier = @"cvCell";
    NavCollectionViewCell *cell = (NavCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    NSString *cellData = [[dataArray objectAtIndex:indexPath.row] objectForKey:@"hierarchy_level_name"];
    [cell.titleButton setTitle:cellData forState:UIControlStateNormal];
    cell.titleButton.tag=indexPath.row;
    cell.titleButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [cell.titleButton addTarget:self action:@selector(chainAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //int selected=[[chainSelectedArray objectAtIndex:indexPath.row] intValue];
    
//    switch (selected) {
//        case 0:
//            [cell.titleButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//            cell.titleButton.userInteractionEnabled=NO;
//            chainindexpath=indexPath;
//            break;
//            
//        case 1:
//            [cell.titleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            cell.titleButton.userInteractionEnabled=YES;
//            chainindexpath=indexPath;
//            break;
//    }
    [cell.titleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
               cell.titleButton.userInteractionEnabled=YES;
                chainindexpath=indexPath;
    if (cellrow==indexPath.row)
    {
        cell.titleButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17.0];
    }
    else
    {
        cell.titleButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:17.0];
    }
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:17];
    NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                     NSForegroundColorAttributeName: [UIColor blackColor]};
    const CGSize textSize = [cellData sizeWithAttributes: userAttributes];
    CGRect      buttonFrame = cell.titleButton.frame;
    buttonFrame.size = CGSizeMake(textSize.width+20, 30);
    cell.titleButton.frame=buttonFrame;
    return cell;
    
}

-(void)hierarchyAll
{
    hierarchyLevel=[[dataArray objectAtIndex:0] objectForKey:@"hierarchy_level_id"];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
       [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyNavVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"HierarchyNodes"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
    }
    else
    {
        
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            NSDictionary * responsDic=[HttpHandler hierarchyAll:userName password:password userScreen:@"HierarchyNavVC"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                
                NSLog(@"MS-Hierarchy All responce %@",responsDic);
                if ([[responsDic objectForKey:@"nodes"]  isKindOfClass:[NSNull class]] ||[[responsDic objectForKey:@"type"]  isKindOfClass:[NSNull class]])
                {
                    if ([responsDic objectForKey:@"Error"])
                    {
                        [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[responsDic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    else
                    {
                    
                    }
                }
                else
                {
                    if ([responsDic objectForKey:@"Error"])
                    {
                        [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"(%@i)",[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    else
                    {
                        //dataArray
                        NSArray *count; //= [[NSArray alloc] init];
                        count=[responsDic objectForKey:@"nodes"];
                        hierarchyAllArray=[responsDic objectForKey:@"nodes"];
                        
                        NSMutableArray *topArray = [[NSMutableArray alloc] init];
                        NSString *fav = [[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"];
                        
                        if ([fav isEqualToString:@"fav"])
                        {
                            [favArray addObjectsFromArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"FavInfo"]];
                            
                            
                            NSString *str =[NSString stringWithFormat:@"%@",[[favArray objectAtIndex:0] objectForKey:@"LevelId"]] ;
                            
                            if (str.length>0)
                            {
                                int k=0;
                                for (int i=0; i<[count count]; i++)
                                {
                                    NSString *strId=[NSString stringWithFormat:@"%@",[[count objectAtIndex:i] objectForKey:@"hierarchyLevelId"]];
                                    //NSString *strLevelId = [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:0] objectForKey:@"hierarchy_level_id"]];
                                    if ([strId isEqualToString:str])
                                    {
                                        [topArray addObject:[count objectAtIndex:i]];
                                    }
                                    
                                }
                                for (int j=0; j<[dataArray count]; j++)
                                {
                                    NSString *strLevelId = [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:j] objectForKey:@"hierarchy_level_id"]];
                                    if ([str isEqualToString:strLevelId])
                                    {
                                        k=j;
                                    }
                                }
                                if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeselect"]||[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"] ||[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"OneStore"])//storeEveryOne
                                {
                                    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"])
                                    {
                                        _lectureName.text=[dictLang objectForKey:@"Message to Everyone"];//OneStore
                                    }
                                    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"OneStore"])
                                    {
                                        _lectureName.text=[dictLang objectForKey:@"Group Message"];
                                    }
                                    else
                                    {
                                        _lectureName.text=[dictLang objectForKey:@"Message to an Individual"];
                                    }
                                    
                                }
                                else
                                {
                                _lectureName.text=[dictLang objectForKey:@"Store Hierarchy"];//[[dataArray objectAtIndex:k] objectForKey:@"hierarchy_level_name"];
                                }
                                cellrow=k;
                            }
                            else
                            {
                                NSString *levelID = [NSString stringWithFormat:@"%@",[[favArray objectAtIndex:0] objectForKey:@"NodeId"]];
                                NSString *uniID = [NSString stringWithFormat:@"%@",[[favArray objectAtIndex:0] objectForKey:@"ParentId"]];
                                NSString *strStores=[NSString stringWithFormat:@"%@",[[favArray objectAtIndex:0] objectForKey:@"StoreScreen"]];
                              //  NSString *strTitle = [NSString stringWithFormat:@"%@",[[favArray objectAtIndex:0] objectForKey:@"MessageType"]];
                                if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeselect"]||[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"] ||[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"OneStore"])//storeEveryOne
                                {
                                    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"])
                                    {
                                        _lectureName.text=[dictLang objectForKey:@"Message to Everyone"];//OneStore
                                    }
                                    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"OneStore"])
                                    {
                                        _lectureName.text=[dictLang objectForKey:@"Group Message"];
                                    }
                                    else
                                    {
                                        _lectureName.text=[dictLang objectForKey:@"Message to an Individual"];
                                    }
                                    
                                }
                                else
                                {
                                _lectureName.text=[dictLang objectForKey:@"Store Hierarchy"];//strTitle;
                                }
                                //[self childFav:levelID :uniID:storeValue];//FeatureType
                                NSString *strCell=[NSString stringWithFormat:@"%@",[[favArray objectAtIndex:0] objectForKey:@"FeatureType"]];
                                cellrow=[strCell intValue];
                                [self childFav:levelID universalIdFav:uniID stores:strStores];
                            }
                            //StoreScreen
                        }
                        else
                        {
                            for (int i=0; i<[count count]; i++)
                            {
                                NSString *strId=[NSString stringWithFormat:@"%@",[[count objectAtIndex:i] objectForKey:@"hierarchyLevelId"]];
                                NSString *strLevelId = [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:0] objectForKey:@"hierarchy_level_id"]];
                                if ([strId isEqualToString:strLevelId])
                                {
                                    [topArray addObject:[count objectAtIndex:i]];
                                }
                            }
                        }
                        
                        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"hierarchyNodeName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                        chainArray=[topArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
                        [self.collectionView reloadData];
                        [self.chainTableview reloadData];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                        
                        if ([fav isEqualToString:@"fav"])
                        {
                            [favoritsButton setEnabled:NO];
                        }
                        else
                        {
                           // fav=@"";
                        favoritsButton.enabled=YES;
                        }
                        
                        
                    }
                }
                
                
            }];
        }];
    }
}

- (void)chainAction:(UIButton *)sender
{
    [favoritsButton setEnabled:YES];
    //[[dataArray objectAtIndex:indexPath.row] objectForKey:@"hierarchy_level_name"]
    cellrow=sender.tag;
    hierarchyLevel=[[dataArray objectAtIndex:cellrow] objectForKey:@"hierarchy_level_id"];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeselect"]||[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"] ||[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"OneStore"])//storeEveryOne
    {
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"])
        {
            _lectureName.text=[dictLang objectForKey:@"Message to Everyone"];//OneStore
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"OneStore"])
        {
            _lectureName.text=[dictLang objectForKey:@"Group Message"];
            
        }
        else
        {
            _lectureName.text=[dictLang objectForKey:@"Message to an Individual"];
        }
        
    }
    else
    {
    _lectureName.text=[dictLang objectForKey:@"Store Hierarchy"];//[[dataArray objectAtIndex:cellrow] objectForKey:@"hierarchy_level_name"];
    }
    clickedOnChildNode=NO;
    NSMutableArray *topArray = [[NSMutableArray alloc] init];
    
    for (int i=0; i<[hierarchyAllArray count]; i++)
    {
        NSString *strId=[NSString stringWithFormat:@"%@",[[hierarchyAllArray objectAtIndex:i] objectForKey:@"hierarchyLevelId"]];
        NSString *strLevelId = [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:cellrow] objectForKey:@"hierarchy_level_id"]];
        if ([strId isEqualToString:strLevelId])
        {
            [topArray addObject:[hierarchyAllArray objectAtIndex:i]];
        }
    }
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"hierarchyNodeName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    chainArray=[topArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
    [self.collectionView reloadData];
    [self.chainTableview reloadData];
}

#pragma mark - Tableview methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView !=self.chainTableview) {
        return [searchResultsAll count];
        
    } else {
        return [chainArray  count];
    }
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"cell";
    PlacesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[PlacesTableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.placeButton.layer.cornerRadius=8;
    cell.placeButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    cell.placeButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:17.0];
    if (tableView != self.chainTableview)
    {
        cell.textLabel.text = [[searchResultsAll objectAtIndex:indexPath.row]objectForKey:@"hierarchyNodeName"];
        cell.textLabel.textColor=kNavBackgroundColor;
        
    }
    
    else {
        [cell.placeButton setTitle:[[chainArray objectAtIndex:indexPath.row]objectForKey:@"hierarchyNodeName"] forState:UIControlStateNormal];
        
        
        [cell.placeButton addTarget:self action:@selector(placeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.placeButton.backgroundColor=kNavBackgroundColor;
    NSInteger placeButtonTag=indexPath.row;
    cell.placeButton.tag=placeButtonTag;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
       [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyNavVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"HierarchyNav"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        
        if (tableView != self.chainTableview)
        {
            cellrow++;
            
            if (cellrow>[dataArray count])
            {
                cellrow--;
            }
            
            if (cellrow<=[chainSelectedArray count])
            {
                if ([[InstoreData sharedManager] isInStoreTGS])
                {
                    NSString *storeNameTgs=[[[InstoreData sharedManager] getInStoreTgsStoreName] uppercaseString];
                    
                    NSString *storeName2=[[[searchResultsAll objectAtIndex:indexPath.row]objectForKey:@"hierarchyNodeName"] uppercaseString];
                    if (![storeName2 isEqualToString:storeNameTgs])
                    {
                        BOOL status = [HttpHandler checkCentralServerAvailability];
                        if (status==YES)
                        {
                            [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"InstoreWebSocket"];
                        }
                        else
                        {
                          [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyNavVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"HierarchyNav"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
                            return;
                        }
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"InstoreWebSocket"];
                    }
                }
                
                if (cellrow==[dataArray count])
                {
                    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"]||[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"OneStore"])
                    {
                        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"])
                        {
                            NSMutableArray *storesUpload=[[NSMutableArray alloc] init];
                            NSMutableArray *selectedstoresname=[[NSMutableArray alloc] init];
                            
                            [storesUpload addObject:[NSNumber numberWithInt:[[[searchResultsAll objectAtIndex:indexPath.row] objectForKey:@"hierarchyNodeId"] intValue]]];
                            
                            [selectedstoresname addObject:[[searchResultsAll objectAtIndex:indexPath.row]objectForKey:@"hierarchyNodeName"]];
                            
                            [[NSUserDefaults standardUserDefaults] setObject:selectedstoresname forKey:@"selectedstoresname"];
                            
                            [[NSUserDefaults standardUserDefaults] setObject:storesUpload forKey:@"selectedstores"];
                            
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedGroup"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            
                            [[NSUserDefaults standardUserDefaults] setObject:@"OneStore" forKey:@"featureType"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            [[NSUserDefaults standardUserDefaults] setObject:@"SL" forKey:@"Select"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            
                            [[NSUserDefaults standardUserDefaults]setObject:[[searchResultsAll objectAtIndex:indexPath.row]objectForKey:@"hierarchyNodeName"] forKey:@"store"];
                        }
                        else
                        {
                            NSMutableArray *storesUpload=[[NSMutableArray alloc] init];
                            NSMutableArray *selectedstoresname=[[NSMutableArray alloc] init];
                            
                            [storesUpload addObject:[NSNumber numberWithInt:[[[searchResultsAll objectAtIndex:indexPath.row] objectForKey:@"hierarchyNodeId"] intValue]]];
                            
                            [selectedstoresname addObject:[[searchResultsAll objectAtIndex:indexPath.row]objectForKey:@"hierarchyNodeName"]];
                            
                            [[NSUserDefaults standardUserDefaults] setObject:selectedstoresname forKey:@"selectedstoresname"];
                            
                            [[NSUserDefaults standardUserDefaults] setObject:storesUpload forKey:@"selectedstores"];
                            
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            [[NSUserDefaults standardUserDefaults] setObject:@"OneStore" forKey:@"featureType"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            [[NSUserDefaults standardUserDefaults] setObject:@"SL" forKey:@"Select"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            [[NSUserDefaults standardUserDefaults]setObject:[[searchResultsAll objectAtIndex:indexPath.row]objectForKey:@"hierarchyNodeName"] forKey:@"store"];
                        }
                        
                        [[NSUserDefaults standardUserDefaults] setObject:@"Message" forKey:@"MessageFrom"];
                        [self performSegueWithIdentifier:@"Multi" sender:self];
                    }
                    
                    else
                    {
                    if (tableView != self.chainTableview)
                    {
                        [self.searchDisplayController setActive:NO animated:YES];
                        //self.searchDisplayController.active=false;
                        ContactsViewController *contactsController;
                        contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactsViewController"];
                        [[NSUserDefaults standardUserDefaults] setObject:[[searchResultsAll objectAtIndex:indexPath.row]objectForKey:@"hierarchyNodeName"] forKey:@"storeNameString"];
                        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:[[[searchResultsAll objectAtIndex:indexPath.row] objectForKey:@"hierarchyNodeId"] intValue]] forKey:@"storeid"];
                        [self.navigationController pushViewController:contactsController animated:YES];
                    }
                    }
                    
                }
                else
                {
                    if (tableView != self.chainTableview)
                    {
                        if (searchResultsAll.count>0)
                        {
                            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            universalId =[[searchResultsAll objectAtIndex:indexPath.row] objectForKey:@"hierarchyNodeId"];
                            levelId =[[searchResultsAll objectAtIndex:indexPath.row] objectForKey:@"hierarchyLevelId"];
                            
                            [mainArray addObject: universalId];
                            [levelArray addObject:levelId];
                            
                            
                        }
                        [self hierarchyChild];
                        [self.searchDisplayController setActive:NO animated:YES];
                        
                    }
                    
                }
            }
        }
    }
}

- (void)placeButtonAction:(UIButton *)sender
{
    [favoritsButton setEnabled:YES];
    //[[NSUserDefaults standardUserDefaults] objectForKey:@"FromFav"]
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FromFav"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    clickedOnChildNode=YES;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
       [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyNavVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"HierarchyNav"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        
        cellrow++;
        
        if (cellrow>[dataArray count])
        {
            cellrow--;
        }
        
        if (cellrow<=[chainSelectedArray count])
        {//OneStore
            
            if (cellrow==[dataArray count])
            {
                if ([[InstoreData sharedManager] isInStoreTGS])
                {
                    NSString *storeNameTgs=[[[InstoreData sharedManager] getInStoreTgsStoreName] uppercaseString];
                    
                    NSString *storeName2=[[[chainArray objectAtIndex:sender.tag]objectForKey:@"hierarchyNodeName"] uppercaseString];
                    
                    if (![storeName2 isEqualToString:storeNameTgs])
                    {
                            BOOL status = [HttpHandler checkCentralServerAvailability];
                        if (status==YES)
                        {
                            [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"InstoreWebSocket"];
                        }
                        else
                        {
                            [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"HierarchyNavVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"HierarchyNav"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
                            return;
                        }
                        
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"InstoreWebSocket"];
                    }
                }
                
                if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"]||[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"OneStore"])
                {
                    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeselect"] isEqualToString:@"storeEveryOne"])
                    {
                        NSMutableArray *storesUpload=[[NSMutableArray alloc] init];
                        NSMutableArray *selectedstoresname=[[NSMutableArray alloc] init];
                        
                        [storesUpload addObject:[NSNumber numberWithInt:[[[chainArray objectAtIndex:sender.tag] objectForKey:@"hierarchyNodeId"] intValue]]];
                        
                        [selectedstoresname addObject:[[chainArray objectAtIndex:sender.tag]objectForKey:@"hierarchyNodeName"]];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:selectedstoresname forKey:@"selectedstoresname"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:storesUpload forKey:@"selectedstores"];
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"selectedGroup"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        [[NSUserDefaults standardUserDefaults] setObject:@"OneStore" forKey:@"featureType"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [[NSUserDefaults standardUserDefaults] setObject:@"SL" forKey:@"Select"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        [[NSUserDefaults standardUserDefaults]setObject:[[chainArray objectAtIndex:sender.tag]objectForKey:@"hierarchyNodeName"] forKey:@"store"];
                    }
                    else
                    {
                        NSMutableArray *storesUpload=[[NSMutableArray alloc] init];
                        NSMutableArray *selectedstoresname=[[NSMutableArray alloc] init];
                        
                        [storesUpload addObject:[NSNumber numberWithInt:[[[chainArray objectAtIndex:sender.tag] objectForKey:@"hierarchyNodeId"] intValue]]];
                        
                        [selectedstoresname addObject:[[chainArray objectAtIndex:sender.tag]objectForKey:@"hierarchyNodeName"]];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:selectedstoresname forKey:@"selectedstoresname"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:storesUpload forKey:@"selectedstores"];
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:@"OneStore" forKey:@"featureType"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [[NSUserDefaults standardUserDefaults] setObject:@"SL" forKey:@"Select"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [[NSUserDefaults standardUserDefaults]setObject:[[chainArray objectAtIndex:sender.tag]objectForKey:@"hierarchyNodeName"] forKey:@"store"];
                    }
                   
                    [[NSUserDefaults standardUserDefaults] setObject:@"Message" forKey:@"MessageFrom"];
                    [self performSegueWithIdentifier:@"Multi" sender:self];
                }
                else
                {
                    ContactsViewController *contactsController;
                    contactsController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactsViewController"];
                    [[NSUserDefaults standardUserDefaults] setObject:[[chainArray objectAtIndex:sender.tag]objectForKey:@"hierarchyNodeName"] forKey:@"storeNameString"];
                    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:[[[chainArray objectAtIndex:sender.tag] objectForKey:@"hierarchyNodeId"] intValue]] forKey:@"storeid"];
                    [self.navigationController pushViewController:contactsController animated:YES];
                }
                
            }
            else
            {
                if (chainArray.count>0)
                {
                    universalId =[[chainArray objectAtIndex:sender.tag] objectForKey:@"hierarchyNodeId"];
                    levelId =[[chainArray objectAtIndex:sender.tag] objectForKey:@"hierarchyLevelId"];
                    
                    [mainArray addObject: universalId];
                    [levelArray addObject:levelId];
                    
                }
                [self hierarchyChild];
            }
            
        }
    }
}


#pragma mark - UISearchDisplayController delegate methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    searchResultsAll=[[NSMutableArray alloc]init];
    for (int i =0;i<[chainArray count];i++)
        
    {
        
        NSString *city = [[chainArray objectAtIndex:i] objectForKey:@"hierarchyNodeName"];
        NSRange range=[city rangeOfString:searchText options:NSCaseInsensitiveSearch];
        if (range.length >0)
        {
            if (![searchResultsAll containsObject:city]) {
                [searchResultsAll addObject:[chainArray objectAtIndex:i]];
            }
        }
        
    }
    
}

-(BOOL)searchDisplayController:(UISearchController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[controller.searchBar scopeButtonTitles]
                                      objectAtIndex:[controller.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}



@end
