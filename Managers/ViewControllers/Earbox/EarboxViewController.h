//
//  EarboxViewController.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface EarboxViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,AVAudioPlayerDelegate>
{
    IBOutlet UISegmentedControl* segCtl,*messagesegmentControl;
    int selectedrow;
    BOOL selected;
    NSString *audioId;
    BOOL isReplying;
    NSString *replyingTo;
    BOOL deselected;
     IBOutlet UILabel *lbl_Green;
    
}
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property(nonatomic,strong) NSMutableArray *statusArray;
@property BOOL isPaused;
@property BOOL scrubbing;
@property NSTimer *timer;
+(EarboxViewController *)getsharedInstance;
-(IBAction)messagesegmentControl:(id)sender;
-(IBAction)segmentControl:(id)sender;

@end
