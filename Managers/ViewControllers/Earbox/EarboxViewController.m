//
//  EarboxViewController.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "EarboxViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "StateMachineHandler.h"
#import "BroadcastViewController.h"
#import "UIViewController.h"
#import "Constant.h"
#import "AudioManager.h"
#import "TableViewCell.h"
#import "EarboxDataStore.h"
#import "Earboxinboxmodel.h"
#import "HttpHandler.h"
#import "AlertView.h"
#import "Reachability.h"
#import "NSDateClass.h"
#import "ActivityIndicatorView.h"
#import "ReplayView.h"
#import "DiagnosticTool.h"

@interface EarboxViewController ()
{
    NSArray *savedmessagesArray,*messageArray,*announcementArray,*savedannouncementArray,*sentmessageArray,*sentannouncementArray,*messageinbox,*messagesent,*messagesaved,*announcementinbox,*announcementsent,*announcementsaved,*freshInboxList,*freshSavedList,*freshReplyList,*groupinbox,*groupsent,*groupsaved;
    TableViewCell *cell;
    BOOL replyClicked,connectionAvailable,isRowClosed;
    UIButton *imagebutton,*cancelButton;
    NSString *storeNameString,*backgroundpushnotificationstring,*contactNameString,*audioString,*audiologString,*audiolength,*staticerrorcode,*selectedCallId;
    NSIndexPath *indexpathcell,*expandedIndexPath;
    NSInteger deletebuttonindex,rowindex;
    NSDictionary *loginDetails,*apploginDetails,*groupDict,*dictLang;
    UIBarButtonItem *speakerButton;
    UILabel *connectionLabel;
    EarboxInbox *messageinboxdata, *messageFreshList;
    EarboxSaved *messagesaveddata, *messageFreshSavedData;
    EarboxSent *messagesentdata;
    AnnouncementInbox *announcementinboxdata;
    AnnouncementSaved *announcementsaveddata;
    AnnouncementSent *announcementsentdata;
    NSURL *replyAudiourl;
    int storeId,companyId;
    UITableView *nameButtonalertTable;
    UIView *alerttable;
    ReplayView *rootView;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSMutableDictionary *pnsdic;
@end

@implementation EarboxViewController
@synthesize statusArray;
static EarboxViewController  *sharedMgr = nil;

+(EarboxViewController *)getsharedInstance {
    if (sharedMgr == nil)
    {
        sharedMgr = [[EarboxViewController alloc] init];
    }
    return sharedMgr;
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

#pragma mark - SegmentControl inbox/saved/sent method
-(IBAction)messagesegmentControl:(UISegmentedControl *)sender
{
    expandedIndexPath=nil;
    deselected=YES;
    [self audioPlayerStop];
    if (segCtl.tag==1)
    {
        if( [sender selectedSegmentIndex] == 0 || sender.tag==1)
        {
            messagesegmentControl.tag=1;
            self.tableView.tag=1;
            [self.tableView reloadData];
        }
        
        if( [sender selectedSegmentIndex] == 1 || sender.tag==2)
        {
            messagesegmentControl.tag=2;
            self.tableView.tag=2;
            [self.tableView reloadData];
        }
        if( [sender selectedSegmentIndex] == 2 || sender.tag==3)
        {
            messagesegmentControl.tag=3;
            self.tableView.tag=3;
            [self.tableView reloadData];
        }
    }
    else if (segCtl.tag==2)
    {
        if( [sender selectedSegmentIndex] == 0 || sender.tag==1)
        {
            messagesegmentControl.tag=4;
            self.tableView.tag=4;
            [self.tableView reloadData];
        }
        
        if( [sender selectedSegmentIndex] == 1 || sender.tag==2)
        {
            messagesegmentControl.tag=5;
            self.tableView.tag=5;
            [self.tableView reloadData];
        }
        if( [sender selectedSegmentIndex] == 2 || sender.tag==3)
        {
            messagesegmentControl.tag=6;
            self.tableView.tag=6;
            [self.tableView reloadData];
        }
    }
    else
    {
        if( [sender selectedSegmentIndex] == 0 || sender.tag==1)
        {
            messagesegmentControl.tag=7;
            self.tableView.tag=7;
            [self.tableView reloadData];
        }
        
        if( [sender selectedSegmentIndex] == 1 || sender.tag==2)
        {
            messagesegmentControl.tag=8;
            self.tableView.tag=8;
            [self.tableView reloadData];
        }
        if( [sender selectedSegmentIndex] == 2 || sender.tag==3)
        {
            messagesegmentControl.tag=9;
            self.tableView.tag=9;
            [self.tableView reloadData];
        }
    }
    [self tableViewUpdate];
}


#pragma mark - SegmentControl message/Announcement method
-(IBAction)segmentControl:(UISegmentedControl *)sender
{
    expandedIndexPath=nil;
    deselected=YES;
    [self audioPlayerStop];
    if( [sender selectedSegmentIndex] == 0 )
    {
        segCtl.tag=1;
        if (messagesegmentControl.tag==1 || messagesegmentControl.tag==4 || messagesegmentControl.tag==7)
        {
            self.tableView.tag=1;
        }
        else  if (messagesegmentControl.tag==2 || messagesegmentControl.tag==5 || messagesegmentControl.tag==8)
        {
            self.tableView.tag=2;
        }
        else  if (messagesegmentControl.tag==3 || messagesegmentControl.tag==6 || messagesegmentControl.tag==9)
        {
            self.tableView.tag=3;
        }
        [self.tableView reloadData];
    }
    
    if( [sender selectedSegmentIndex] == 1 )
    {
        segCtl.tag=2;
        if (messagesegmentControl.tag==4 || messagesegmentControl.tag==1 || messagesegmentControl.tag==7)
        {
            self.tableView.tag=4;
        }
        else  if (messagesegmentControl.tag==5 || messagesegmentControl.tag==2 || messagesegmentControl.tag==8)
        {
            self.tableView.tag=5;
        }
        else  if (messagesegmentControl.tag==6 || messagesegmentControl.tag==3 || messagesegmentControl.tag==9)
        {
            self.tableView.tag=6;
        }
        [self.tableView reloadData];
        
    }
    if( [sender selectedSegmentIndex] == 2 )
    {
        segCtl.tag=3;
        if (messagesegmentControl.tag==7 || messagesegmentControl.tag==1 || messagesegmentControl.tag==4)
        {
            self.tableView.tag=7;
        }
        else  if (messagesegmentControl.tag==8 || messagesegmentControl.tag==2 || messagesegmentControl.tag==5)
        {
            self.tableView.tag=8;
        }
        else  if (messagesegmentControl.tag==9 || messagesegmentControl.tag==3 || messagesegmentControl.tag==6)
        {
            self.tableView.tag=9;
        }
        [self.tableView reloadData];
        
    }
    [self tableViewUpdate];
}


#pragma mark - viewDidLoad method
- (void)viewDidLoad
{
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    NSInteger row = 1;
    NSInteger section = 1;
    expandedIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont fontWithName:@"Helvetica" size:12], NSFontAttributeName, nil];
    
    [segCtl setTitleTextAttributes:attributes forState:UIControlStateNormal];
    NSDictionary *attributes1 = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIFont fontWithName:@"Helvetica" size:12], NSFontAttributeName, nil];
    
    [segCtl setTitle:[dictLang objectForKey:@"Messages"] forSegmentAtIndex:0];
    
    [segCtl setTitle:[dictLang objectForKey:@"Announcements"] forSegmentAtIndex:1];
    
    [segCtl setTitle:[dictLang objectForKey:@"Group Messages"] forSegmentAtIndex:2];
    
    
    [messagesegmentControl setTitleTextAttributes:attributes1 forState:UIControlStateNormal];
    
    [messagesegmentControl setTitle:[dictLang objectForKey:@"Inbox"] forSegmentAtIndex:0];
    
    [messagesegmentControl setTitle:[dictLang objectForKey:@"Saved"] forSegmentAtIndex:1];
    
    [messagesegmentControl setTitle:[dictLang objectForKey:@"Sent"] forSegmentAtIndex:2];
    
    
    companyId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"companyId"]intValue];
    storeId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue];
    deselected=YES;
    isReplying = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coveragejson:)
                                                 name:@"coveragejson"
                                               object:nil];
    connectionLabel=[[UILabel alloc]init];
    connectionLabel.textColor=[UIColor whiteColor];
    connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
    connectionLabel.frame= CGRectMake(0,self.view.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height);
    connectionLabel.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    connectionLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:connectionLabel];
    connectionLabel.hidden=YES;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    storeNameString=[[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
    self.navigationItem.title=storeNameString;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: kNavBackgroundColor};
    self.tableView.layer.cornerRadius=4;
    self.tableView.layer.borderWidth=1;
    self.tableView.layer.borderColor=kNavBackgroundColor.CGColor;
    [super viewDidLoad];
    
    [self.tableView setAllowsMultipleSelection:YES];
    
    loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    apploginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
    NSMutableArray *firstTimeStoreArray=[[NSMutableArray alloc]init];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"firstTimeStore"])
    {
        if([[[NSUserDefaults standardUserDefaults] objectForKey:@"firstTimeStore"] containsObject:storeNameString]&& [[[NSUserDefaults standardUserDefaults]objectForKey:@"appversioncheck"]isEqualToString:@"NO"] && ![[[NSUserDefaults standardUserDefaults]objectForKey:@"undeliveredfailedEarbox"] isEqualToString:@"undeliveredfailedEarbox"])
        {
            [self notificationrecieved];
            
            [self requestUndeliveredMsgs];
        }
        else
        {
            [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
            NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
            [myQueue addOperationWithBlock:^{
                [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"appversioncheck"];
                NSDictionary *twearboxListdic= [HttpHandler earboxlist:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId storeId:storeId userScreen:@"EarboxVC"];
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredsucessEarbox" forKey:@"undeliveredfailedEarbox"];
                    NSDictionary *message=[[twearboxListdic objectForKey:@"earbox"]objectForKey:@"message"];
                    
                    for (int i=0; i<message.count; i++)
                    {
                        messageinbox=[message objectForKey:@"inbox"];
                        
                        messagesent=[message  objectForKey:@"sent"];
                        
                        messagesaved=[message objectForKey:@"saved"];
                        
                    }
                    if (messageinbox.count>0)
                    {
                        [self insertmessageinboxdata:messageinbox ];
                    }
                    if (messagesent.count>0)
                    {
                        [self insertmessagesentdata:messagesent ];
                    }
                    if (messagesaved.count>0)
                    {
                        [self insertmessagesaveddata:messagesaved];
                    }
                    
                    NSDictionary *announcement=[[twearboxListdic objectForKey:@"earbox"] objectForKey:@"announcement"];
                    for (int i=0; i<announcement.count; i++)
                    {
                        announcementinbox=[announcement objectForKey:@"inbox"];
                        
                        announcementsent=[announcement  objectForKey:@"sent"];
                        
                        announcementsaved=[announcement  objectForKey:@"saved"];
                        
                    }
                    if (announcementinbox.count>0)
                    {
                        [self insertannouncementinboxdata:announcementinbox ];
                    }
                    if (announcementsent.count>0)
                    {
                        [self insertannouncementsentdata:announcementsent ];
                    }
                    if (announcementsaved.count>0)
                    {
                        [self insertannouncementsaveddata:announcementsaved ];
                    }
                    [self employeelistdata];
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    [firstTimeStoreArray addObjectsFromArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"firstTimeStore"]];
                    [firstTimeStoreArray addObject:storeNameString];
                    [[NSUserDefaults standardUserDefaults]setObject:firstTimeStoreArray forKey:@"firstTimeStore"];
                    
                }];
            }];
        }
        
    }
    else
    {
        [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            
            NSDictionary *twearboxListdic= [HttpHandler earboxlist:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId storeId:storeId userScreen:@"EarboxVC"];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"appversioncheck"];
                NSDictionary *message=[[twearboxListdic objectForKey:@"earbox"]objectForKey:@"message"];
                
                for (int i=0; i<message.count; i++)
                {
                    messageinbox=[message objectForKey:@"inbox"];
                    
                    messagesent=[message  objectForKey:@"sent"];
                    
                    messagesaved=[message objectForKey:@"saved"];
                    
                }
                if (messageinbox.count>0)
                {
                    [self insertmessageinboxdata:messageinbox ];
                }
                if (messagesent.count>0)
                {
                    [self insertmessagesentdata:messagesent ];
                }
                if (messagesaved.count>0)
                {
                    [self insertmessagesaveddata:messagesaved];
                }
                
                NSDictionary *announcement=[[twearboxListdic objectForKey:@"earbox"] objectForKey:@"announcement"];
                for (int i=0; i<announcement.count; i++)
                {
                    announcementinbox=[announcement objectForKey:@"inbox"];
                    
                    announcementsent=[announcement  objectForKey:@"sent"];
                    
                    announcementsaved=[announcement  objectForKey:@"saved"];
                    
                }
                if (announcementinbox.count>0)
                {
                    [self insertannouncementinboxdata:announcementinbox ];
                }
                if (announcementsent.count>0)
                {
                    [self insertannouncementsentdata:announcementsent ];
                }
                if (announcementsaved.count>0)
                {
                    [self insertannouncementsaveddata:announcementsaved ];
                }
                [self employeelistdata];
                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                [firstTimeStoreArray addObject:storeNameString];
                [[NSUserDefaults standardUserDefaults]setObject:firstTimeStoreArray forKey:@"firstTimeStore"];
                
            }];
        }];
    }
    [self tabIndexchange];
}

-(void)tabIndexchange
{
    segCtl.tag=1;
    messagesegmentControl.tag=1;
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"4"])
    {
        self.tableView.tag=1;
        
        segCtl.selectedSegmentIndex = 0;
        messagesegmentControl.selectedSegmentIndex = 0;
        [segCtl sendActionsForControlEvents:UIControlEventValueChanged];
        [[[segCtl subviews] objectAtIndex:0] setTintColor:kNavBackgroundColor];
        [messagesegmentControl sendActionsForControlEvents:UIControlEventValueChanged];
        [[[messagesegmentControl subviews] objectAtIndex:0] setTintColor:kNavBackgroundColor];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"10"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"16"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"21"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"22"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"23"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"25"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"200"])
    {
        
        [[NSUserDefaults standardUserDefaults]setObject:@"4" forKey:@"notificationtype"];
        self.tableView.tag=4;
        segCtl.selectedSegmentIndex = 1;
        messagesegmentControl.selectedSegmentIndex = 0;
        
        [segCtl sendActionsForControlEvents:UIControlEventValueChanged];
        [[[segCtl subviews] objectAtIndex:0] setTintColor:kNavBackgroundColor];
        [messagesegmentControl sendActionsForControlEvents:UIControlEventValueChanged];
        [[[messagesegmentControl subviews] objectAtIndex:0] setTintColor:kNavBackgroundColor];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"17"]||[[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationtype"] isEqualToString:@"108"])
        
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"4" forKey:@"notificationtype"];
        self.tableView.tag=7;
        
        segCtl.selectedSegmentIndex = 2;
        messagesegmentControl.selectedSegmentIndex = 0;
        [segCtl sendActionsForControlEvents:UIControlEventValueChanged];
        [[[segCtl subviews] objectAtIndex:0] setTintColor:kNavBackgroundColor];
        [messagesegmentControl sendActionsForControlEvents:UIControlEventValueChanged];
        [[[messagesegmentControl subviews] objectAtIndex:0] setTintColor:kNavBackgroundColor];
    }
    else
    {
        self.tableView.tag=1;
        
        segCtl.selectedSegmentIndex = 0;
        messagesegmentControl.selectedSegmentIndex = 0;
        [segCtl sendActionsForControlEvents:UIControlEventValueChanged];
        [[[segCtl subviews] objectAtIndex:0] setTintColor:kNavBackgroundColor];
        [messagesegmentControl sendActionsForControlEvents:UIControlEventValueChanged];
        [[[messagesegmentControl subviews] objectAtIndex:0] setTintColor:kNavBackgroundColor];
    }
}

-(void)employeelistdata
{
    NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
    if (selectedRows.count==0 )
    {
        [self tableViewUpdate];
    }
}

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    [speakerButton setEnabled:YES];
    NSLog(@"Earbox view viewWillAppear");
    UIBarButtonItem *broadCostButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"broadcast"] style:UIBarButtonItemStyleDone target:self action:@selector(broadCostButtonAction)];
    self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
    speakerButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"loud"] style:UIBarButtonItemStyleDone target:self action:@selector(speakerButtonAction:)];
    [self speakerAudioChange];
    self.navigationItem.rightBarButtonItems=@[broadCostButton,speakerButton];
    [[UIBarButtonItem appearance] setTintColor:kNavBackgroundColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    connectionAvailable=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterLockNotification) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatetimer_EarBox) name:@"pushnotificationcount" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(percentageCompleted:) name:@"percentageCompleted" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushnotificationrecieved:) name:@"pushnotificationrecieved" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coverageRequestToServertask:)
                                                 name:@"coverageRequestToServertask"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedAnnouncement:)
                                                 name:@"recievedAnnouncementsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(replayCancelNotification:)
                                                 name:@"replayCancelNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendAnnouncement:)
                                                 name:@"audioNotification"
                                               object:nil];
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    isReplying=NO;
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"pushForDiffStore"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self audioPlayerStop];
    [self replayCancelNotification:0];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushnotificationcount" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coverageRequestToServertask" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushnotificationrecieved" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"audioNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recievedAnnouncementsNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"replayCancelNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"percentageCompleted" object:nil];
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound)
    {
        
        if ([backgroundpushnotificationstring isEqualToString:@"yes"])
        {
            // Navigation button was pressed. Do some stuff
            NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
            [myQueue addOperationWithBlock:^{
                [[StateMachineHandler sharedManager] killTheatroNative];
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                }];
            }];
        }
        if (connectionAvailable==NO)
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"StoreListViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
            
        }
        
    }
    [super viewWillDisappear:animated];
}

#pragma mark - insertmessageinboxdata to db method
-(void)insertmessageinboxdata:(NSArray *)earboxarray
{
    [[Earboxinboxmodel sharedModel]messageinboxSave:earboxarray];
}

#pragma mark - Database insertmessagesaveddata method
-(void)insertmessagesaveddata:(NSArray *)earboxarray
{
    [[Earboxinboxmodel sharedModel]messagesavedSave:earboxarray];
}

#pragma mark - Database insertmessagesentdata method
-(void)insertmessagesentdata:(NSArray *)earboxarray
{
    [[Earboxinboxmodel sharedModel]messagesentSave:earboxarray];
}

#pragma mark - Database insertannouncementinboxdata method
-(void)insertannouncementinboxdata:(NSArray *)earboxarray
{
    [[Earboxinboxmodel sharedModel]announcementinboxSave:earboxarray];
}

#pragma mark - Database insertannouncementsaveddata db method
-(void)insertannouncementsaveddata:(NSArray *)earboxarray
{
    [[Earboxinboxmodel sharedModel]announcementsavedSave:earboxarray];
}

#pragma mark - Database insertannouncementsentdata method
-(void)insertannouncementsentdata:(NSArray *)earboxarray
{
    [[Earboxinboxmodel sharedModel]announcementsentSave:earboxarray];
}

#pragma mark - Database savemessagedata method
-(void)savemessagedata:(EarboxSaved *)save
{
    [[Earboxinboxmodel sharedModel]messageSave:save];
}

#pragma mark - Database saveannouncementdata method
-(void)saveannouncementdata:(AnnouncementSaved *)save
{
    [[Earboxinboxmodel sharedModel]announcementSave:save ];
}

#pragma mark - Database getmessageinboxdata method
-(void)getmessageinboxdata:(NSString *)earboxname calltype:(int)calltype
{
    messageArray=nil;
    messageArray= [[EarboxDataStore sharedStore] artists:earboxname storeid:storeId calltype:calltype];
}

#pragma mark - Database getmessageinboxdata method
-(void)getFreshInboxList:(NSString *)earboxname calltype:(int)calltype
{
    freshInboxList = [[EarboxDataStore sharedStore] artists:earboxname storeid:storeId calltype:calltype];
}

#pragma mark - Database getmessageinboxdata method
-(void)getFreshSavedList:(NSString *)earboxname calltype:(int)calltype
{
    freshSavedList = [[EarboxDataStore sharedStore] artists:earboxname storeid:storeId calltype:calltype];
}

#pragma mark - Database getmessagesentdata method
-(void)getmessagesentdata:(NSString *)earboxname calltype:(int)calltype
{
    sentmessageArray= [[EarboxDataStore sharedStore] artists:earboxname storeid:storeId calltype:calltype];
}

#pragma mark - Database getmessagesaveddata method
-(void)getmessagesaveddata:(NSString *)earboxname calltype:(int)calltype
{
    savedmessagesArray= [[EarboxDataStore sharedStore] artists:earboxname storeid:storeId calltype:calltype];
}

#pragma mark - Database getannouncementinboxdata method
-(void)getannouncementinboxdata:(NSString *)earboxname calltype:(int)calltype
{
    announcementArray= [[EarboxDataStore sharedStore] artists:earboxname storeid:storeId calltype:calltype];
}

#pragma mark - Database getannouncementsentdata method
-(void)getannouncementsentdata:(NSString *)earboxname calltype:(int)calltype
{
    sentannouncementArray= [[EarboxDataStore sharedStore] artists:earboxname storeid:storeId calltype:calltype];
}

#pragma mark - Database getannouncementsaveddata method
-(void)getannouncementsaveddata:(NSString *)earboxname calltype:(int)calltype
{
    savedannouncementArray = [[EarboxDataStore sharedStore] artists:earboxname storeid:storeId calltype:calltype];
}

#pragma mark - UITable Delegate methods
-(void)expandrow
{
    CGRect rectOfCellInTableView = [self.tableView rectForRowAtIndexPath: indexpathcell];
    NSArray *visibleCells = [self.tableView visibleCells];
    cell=[visibleCells lastObject];
    if (cell.frame.origin.y==rectOfCellInTableView.origin.y)
    {
        if (visibleCells.count>6)
        {
            cell=[visibleCells firstObject];
            [self.tableView setContentOffset:CGPointMake(0,cell.frame.origin.y+130) animated:YES];
        }
    }
    cell=[visibleCells firstObject];
    
    if (rectOfCellInTableView.origin.y==cell.frame.origin.y)
    {
        if (rowindex==0)
        {
            
        }
        else
        {
            [self.tableView setContentOffset:CGPointMake(0,cell.frame.origin.y-30) animated:YES];
        }
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==nameButtonalertTable)
    {
        return 45;
    }
    else
    {
        if ([indexPath compare:expandedIndexPath] == NSOrderedSame) {
            
            return 170.0; // Expanded height
        }
        return 60.0; // Normal height
    }
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView==nameButtonalertTable)
    {
        if (section==0)
        {
            return [dictLang objectForKey:@"Stores"];
        }
        else
        {
            return [dictLang objectForKey:@"Groups"];
        }
        
    }
    return 0;
}


#pragma mark - UITable Datasource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        if(tableView==nameButtonalertTable)
        {
            return [[groupDict objectForKey:@"Stores"] count];
        }
        else
        {
            if (self.tableView.tag==1 || self.tableView.tag==7)
            {
                return [messageArray count];
            }
            else if (self.tableView.tag==2 || self.tableView.tag==8)
            {
                return [savedmessagesArray count];
            }
            else if (self.tableView.tag==3 || self.tableView.tag==9)
            {
                return [sentmessageArray  count];
            }
            else if (self.tableView.tag==4)
            {
                return [announcementArray count];
            }
            else if (self.tableView.tag==5)
            {
                return  [savedannouncementArray count];
            }
            else if (self.tableView.tag==6)
            {
                return [sentannouncementArray count];
            }
        }
    }
    else
    {
        return [[groupDict objectForKey:@"Groups"] count];
    }
    
    return 0;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    if(tableView==nameButtonalertTable)
    {
        return 2;
    }
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==nameButtonalertTable)
    {
        static NSString *cellIdentifier=@"cell";
        UITableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell1 == nil) {
            cell1 = [[UITableViewCell alloc]
                     initWithStyle:UITableViewCellStyleDefault
                     reuseIdentifier:cellIdentifier];
        }
        cell1.textLabel.font=[UIFont systemFontOfSize: 15.0];
        if (indexPath.section==0)
        {
            cell1.textLabel.text=[[groupDict objectForKey:@"Stores"] objectAtIndex:indexPath.row];
        }
        else
        {
            
            cell1.textLabel.text=[[groupDict objectForKey:@"Groups"] objectAtIndex:indexPath.row];
        }
        return cell1;
    }
    else
    {
        static NSString *cellIdentifier=@"cell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[TableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:cellIdentifier];
        }
        [cell.replyTextButton setTitle:[dictLang objectForKey:@"Reply"] forState:UIControlStateNormal];
        [cell.sendTextButton setTitle:[dictLang objectForKey:@"Save"] forState:UIControlStateNormal];
        [cell.deleteTextButton setTitle:[dictLang objectForKey:@"Delete"] forState:UIControlStateNormal];
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 2)];/// change size as you need.
        
        separatorLineView.backgroundColor = [UIColor grayColor];
        [cell.contentView addSubview:separatorLineView];
        cell.activityView.hidden=YES;
        cell.circularProgressView.progressTintColor=kNavBackgroundColor;
        cell.nameTextButton.userInteractionEnabled=NO;
        cell.selectionStyle=UITableViewCellAccessoryNone;
        cell.groupnameLabel.text=@"TheatroManagar";
        cell.groupLabel.text=[NSString stringWithFormat:@"%@:",[dictLang objectForKey:@"Group"]];
        cell.storeLabel.text=[NSString stringWithFormat:@"%@:",[dictLang objectForKey:@"Store"]];
        cell.statusimage.image=[UIImage imageNamed:@"grey_circle.png"];
        cell.timeElapsed.hidden=YES;
        cell.replyimage.image=[UIImage imageNamed:@""];
        cell.hidden=YES;
        
        cell.nameTextLabel.marqueeType = MLContinuous;
        cell.nameTextLabel.scrollDuration = 8.0;
        cell.nameTextLabel.animationCurve = UIViewAnimationOptionCurveEaseInOut;
        cell.nameTextLabel.fadeLength = 0.0f;
        cell.nameTextLabel.leadingBuffer = 0.0f;
        cell.nameTextLabel.trailingBuffer = 10.0f;
        
        if (self.tableView.tag==1 || self.tableView.tag==7)
        {
            if (self.tableView.tag==1)
            {
                cell.groupLabel.hidden=YES;
                cell.groupnameLabel.hidden=YES;
            }
            else
            {
                cell.groupLabel.hidden=NO;
                cell.groupnameLabel.hidden=NO;
            }
            if (messageArray.count>indexPath.row)
            {
                
                messageinboxdata= [messageArray objectAtIndex:indexPath.row];
                if (messageinboxdata.from.length==0)
                {
                    cell.hidden=YES;
                }
                else
                {
                    cell.hidden=NO;
                    cell.replyimage.hidden=NO;
                    cell.expriesLabel.hidden=NO;
                    NSArray*as =[statusArray valueForKey:@"Name"];
                    if([as containsObject:messageinboxdata.from])
                    {
                        NSUInteger index = [as indexOfObject:messageinboxdata.from];
                        int status=[[[statusArray  objectAtIndex:index] objectForKey:@"Status"] intValue];
                        switch (status) {
                            case 0:
                                cell.statusimage.image=[UIImage imageNamed:@"green_circle"];
                                
                                break;
                                
                            case 1:
                                cell.statusimage.image=[UIImage imageNamed:@"red_circle"];
                                
                            case 2:
                                cell.statusimage.image=[UIImage imageNamed:@"red_circle"];
                                
                            case 3:
                                cell.statusimage.image=[UIImage imageNamed:@"red_circle"];
                                
                                break;
                            case 6:
                                cell.statusimage.image=[UIImage imageNamed:@"grey_circle"];
                                
                                break;
                                
                        }
                    }
                    if (![messageinboxdata.status isEqualToString:@"new"])
                    {
                        [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [cell.nameTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
                    }
                    
                    if ([messageinboxdata.reply isEqualToString:@"1"])
                    {
                        cell.replyimage.image=[UIImage imageNamed:@"Reply"];
                    }
                    else
                    {
                        cell.replyimage.image=[UIImage imageNamed:@""];
                    }
                    
                    cell.nameTextLabel.text=[messageinboxdata.from capitalizedString];
                    cell.groupnameLabel.text=messageinboxdata.to;
                    
                    cell.sentLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Sent"],[NSDateClass dateformate:messageinboxdata.callStartTime]];
                    cell.expriesLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Expires"],[NSDateClass ttlformate:messageinboxdata.ttl]];
                    cell.lengthLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Length"],[NSDateClass lengthformate:messageinboxdata.duration]];
                    [cell.nameTextButton setTitle:[[messageinboxdata.from substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                    
                }
            }
        }
        else if (self.tableView.tag==2 || self.tableView.tag==8)
            
        {
            if (self.tableView.tag==2)
            {
                cell.groupLabel.hidden=YES;
                cell.groupnameLabel.hidden=YES;
            }
            else
            {
                cell.groupLabel.hidden=NO;
                cell.groupnameLabel.hidden=NO;
            }
            if (savedmessagesArray.count>indexPath.row)
            {
                messagesaveddata = [savedmessagesArray objectAtIndex:indexPath.row];
                if (messagesaveddata.from.length==0)
                {
                    cell.hidden=YES;
                }
                else
                {
                    cell.hidden=NO;
                    cell.replyimage.hidden=NO;
                    cell.expriesLabel.hidden=YES;
                    NSArray*as =[statusArray valueForKey:@"Name"];
                    if([as containsObject:messagesaveddata.from])
                    {
                        NSUInteger index = [as indexOfObject:messagesaveddata.from];
                        int status=[[[statusArray  objectAtIndex:index] objectForKey:@"Status"] intValue];
                        switch (status) {
                            case 0:
                                cell.statusimage.image=[UIImage imageNamed:@"green_circle"];
                                
                                break;
                                
                            case 1:
                                cell.statusimage.image=[UIImage imageNamed:@"red_circle"];
                                
                            case 2:
                                cell.statusimage.image=[UIImage imageNamed:@"red_circle"];
                                
                            case 3:
                                cell.statusimage.image=[UIImage imageNamed:@"red_circle"];
                                
                                break;
                            case 6:
                                cell.statusimage.image=[UIImage imageNamed:@"grey_circle"];
                                
                                break;
                                
                        }
                    }
                    if (![messagesaveddata.status isEqualToString:@"new"])
                    {
                        [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [cell.nameTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
                    }
                    if ([messagesaveddata.reply isEqualToString:@"1"])
                    {
                        cell.replyimage.image=[UIImage imageNamed:@"Reply"];
                    }
                    else
                    {
                        cell.replyimage.image=[UIImage imageNamed:@""];
                    }
                    cell.nameTextLabel.text=[messagesaveddata.from capitalizedString];
                    cell.groupnameLabel.text=messagesaveddata.to;
                    
                    cell.sentLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Sent"],[NSDateClass dateformate:messagesaveddata.callStartTime]];
                    cell.expriesLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Expires"],[NSDateClass ttlformate:messagesaveddata.ttl]];
                    cell.lengthLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Length"],[NSDateClass lengthformate:messagesaveddata.duration]];
                    [cell.nameTextButton setTitle:[[messagesaveddata.from substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                }
            }
        }
        else if (self.tableView.tag==3|| self.tableView.tag==9)
        {
            if (self.tableView.tag==3)
            {
                cell.groupLabel.hidden=YES;
                cell.groupnameLabel.hidden=YES;
            }
            else
            {
                cell.nameTextButton.userInteractionEnabled=YES;
                cell.groupLabel.hidden=NO;
                cell.groupnameLabel.hidden=NO;
            }
            cell.contentView.backgroundColor = [UIColor whiteColor];
            if (sentmessageArray.count>indexPath.row)
            {
                cell.hidden=NO;
                cell.replyimage.hidden=YES;
                cell.expriesLabel.hidden=YES;
                messagesentdata= [sentmessageArray objectAtIndex:indexPath.row];
                cell.statusimage.hidden=YES;
                cell.nameTextLabel.text=[messagesentdata.to capitalizedString];
                if ([messagesentdata.groupName length]==0)
                {
                    cell.groupnameLabel.text=[messagesentdata.groupNames capitalizedString];
                }
                else
                {
                    cell.groupnameLabel.text=messagesentdata.groupName;
                }
                [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                cell.sentLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Sent"],[NSDateClass dateformate:messagesentdata.callStartTime]];
                cell.expriesLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Expires"],[NSDateClass ttlformate:messagesentdata.ttl]];
                cell.lengthLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Length"],[NSDateClass lengthformate:messagesentdata.duration]];
                [cell.nameTextButton setTitle:[[messagesentdata.to substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
            }
        }
        else if (self.tableView.tag==4)
            
        {
            cell.groupnameLabel.text=@"";
            cell.groupLabel.hidden=YES;
            cell.groupnameLabel.hidden=YES;
            if (announcementArray.count>indexPath.row)
            {
                announcementinboxdata = [announcementArray objectAtIndex:indexPath.row];
                if (announcementinboxdata.from.length==0)
                {
                    cell.hidden=YES;
                }
                else
                {
                    
                    cell.hidden=NO;
                    cell.expriesLabel.hidden=NO;
                    NSArray*as =[statusArray valueForKey:@"Name"];
                    if([as containsObject:announcementinboxdata.from])
                    {
                        NSUInteger index = [as indexOfObject:announcementinboxdata.from];
                        int status=[[[statusArray  objectAtIndex:index] objectForKey:@"Status"] intValue];
                        switch (status) {
                            case 0:
                                cell.statusimage.image=[UIImage imageNamed:@"green_circle"];
                                
                                break;
                                
                            case 1:
                                cell.statusimage.image=[UIImage imageNamed:@"red_circle"];
                                
                            case 2:
                                cell.statusimage.image=[UIImage imageNamed:@"red_circle"];
                                
                            case 3:
                                cell.statusimage.image=[UIImage imageNamed:@"red_circle"];
                                
                                break;
                            case 6:
                                cell.statusimage.image=[UIImage imageNamed:@"grey_circle"];
                                
                                break;
                                
                        }
                    }
                    if (![announcementinboxdata.status isEqualToString:@"new"])
                    {
                        [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [cell.nameTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
                    }
                    
                    cell.nameTextLabel.text=[announcementinboxdata.from capitalizedString];
                    
                    
                    cell.sentLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Sent"],[NSDateClass dateformate:announcementinboxdata.callStartTime]];
                    cell.expriesLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Expires"],[NSDateClass ttlformate:announcementinboxdata.ttl]];
                    cell.lengthLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Length"],[NSDateClass lengthformate:announcementinboxdata.duration]];
                    [cell.nameTextButton setTitle:[[announcementinboxdata.from substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                }
            }
            
            
        }
        else if (self.tableView.tag==5)
            
        {
            cell.groupnameLabel.text=@"";
            cell.groupLabel.hidden=YES;
            cell.groupnameLabel.hidden=YES;
            if (savedannouncementArray.count>indexPath.row)
            {
                announcementsaveddata= [savedannouncementArray objectAtIndex:indexPath.row];
                if (announcementsaveddata.from.length==0)
                {
                    cell.hidden=YES;
                }
                else
                {
                    
                    cell.hidden=NO;
                    cell.expriesLabel.hidden=YES;
                    NSArray*as =[statusArray valueForKey:@"Name"];
                    if([as containsObject:announcementsaveddata.from])
                    {
                        NSUInteger index = [as indexOfObject:announcementsaveddata.from];
                        int status=[[[statusArray  objectAtIndex:index] objectForKey:@"Status"] intValue];
                        switch (status) {
                            case 0:
                                cell.statusimage.image=[UIImage imageNamed:@"green_circle"];
                                
                                break;
                                
                            case 1:
                                cell.statusimage.image=[UIImage imageNamed:@"red_circle"];
                                
                            case 2:
                                cell.statusimage.image=[UIImage imageNamed:@"red_circle"];
                                
                            case 3:
                                cell.statusimage.image=[UIImage imageNamed:@"red_circle"];
                                
                                break;
                            case 6:
                                cell.statusimage.image=[UIImage imageNamed:@"grey_circle"];
                                
                                break;
                                
                        }
                    }
                    if (![announcementsaveddata.status isEqualToString:@"new"])
                    {
                        [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [cell.nameTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
                    }
                    
                    cell.nameTextLabel.text=[announcementsaveddata.from capitalizedString];
                    
                    
                    cell.sentLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Sent"],[NSDateClass dateformate:announcementsaveddata.callStartTime]];
                    cell.expriesLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Expires"],[NSDateClass ttlformate:announcementsaveddata.ttl]];
                    cell.lengthLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Length"],[NSDateClass lengthformate:announcementsaveddata.duration]];
                    [cell.nameTextButton setTitle:[[announcementsaveddata.from substringToIndex:1] capitalizedString] forState:UIControlStateNormal];
                }
            }
        }
        else if (self.tableView.tag==6)
        {
            cell.groupLabel.hidden=YES;
            cell.groupnameLabel.hidden=YES;
            cell.contentView.backgroundColor = [UIColor whiteColor];
            cell.groupnameLabel.text=@"";
            if (sentannouncementArray.count>indexPath.row)
            {
                announcementsentdata = [sentannouncementArray objectAtIndex:indexPath.row];
                
                cell.hidden=NO;
                cell.expriesLabel.hidden=YES;
                cell.statusimage.hidden=YES;
                cell.nameTextLabel.text=[dictLang objectForKey:@"All"];
                [cell.nameTextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [cell.nameTextButton setBackgroundImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                cell.sentLabel.text= [NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Sent"],[NSDateClass dateformate:announcementsentdata.callStartTime]];
                cell.expriesLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Expires"],[NSDateClass ttlformate:announcementsentdata.ttl]];
                cell.lengthLabel.text=[NSString stringWithFormat:@"%@: %@",[dictLang objectForKey:@"Length"],[NSDateClass lengthformate:announcementsentdata.duration]];
                [cell.nameTextButton setTitle:@"A" forState:UIControlStateNormal];
            }
        }
        [cell.sendTextButton addTarget:self action:@selector(saveButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.sendTextButton.tag=indexPath.row;
        [cell.replyTextButton addTarget:self action:@selector(replybuttonDownPressed:) forControlEvents: UIControlEventTouchDown];
        [cell.replyTextButton addTarget:self action:@selector(replybuttonUpPressed:) forControlEvents: UIControlEventTouchUpInside];
        [cell.replyTextButton addTarget:self action:@selector(replybuttonoutsidePressed:) forControlEvents: UIControlEventTouchDragOutside];
        cell.replyTextButton.tag=indexPath.row;
        [cell.deleteTextButton addTarget:self action:@selector(deleteTextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.deleteTextButton.tag=indexPath.row;
        
        cell.storenameLabel.text=storeNameString;
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.playButton.layer.cornerRadius=4.0;
    if (tableView==nameButtonalertTable)
    {
    }
    else
    {
        if ([indexPath compare:expandedIndexPath] == NSOrderedSame) {
            expandedIndexPath = nil;
        } else {
            expandedIndexPath = indexPath;
        }
        //if (deselected)
        {
            deselected=NO;
            
            
            [self.tableView beginUpdates];
            
            if (self.tableView.tag==1 || self.tableView.tag==7)
            {
                freshReplyList=messageArray;
                messageinboxdata=[messageArray objectAtIndex:indexPath.row];
                selectedCallId=messageinboxdata.callId;
                [[NSUserDefaults standardUserDefaults] setObject:messageinboxdata.callId forKey:@"call_ReplyID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else if(self.tableView.tag==2 || self.tableView.tag==8)
            {
                messagesaveddata=[savedmessagesArray objectAtIndex:indexPath.row];
                selectedCallId=messagesaveddata.callId;
                [[NSUserDefaults standardUserDefaults] setObject:messagesaveddata.callId forKey:@"call_ReplyID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            else if(self.tableView.tag==3 || self.tableView.tag==9)
            {
                messagesentdata=[sentmessageArray objectAtIndex:indexPath.row];
                selectedCallId=messagesentdata.callId;
                [[NSUserDefaults standardUserDefaults] setObject:messagesentdata.callId forKey:@"call_ReplyID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            else if(self.tableView.tag==6)
            {
                announcementsentdata=[sentannouncementArray objectAtIndex:indexPath.row];
                selectedCallId=announcementsentdata.callId;
                [[NSUserDefaults standardUserDefaults] setObject:announcementsentdata.callId forKey:@"call_ReplyID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            isRowClosed=NO;
            cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.contentView.backgroundColor = [UIColor whiteColor];
            indexpathcell=indexPath;
            rowindex=indexPath.row;
            [cell.replyTextButton setTitle:[dictLang objectForKey:@"Reply"] forState:UIControlStateNormal];
            
            self.isPaused=FALSE;
            if (segCtl.tag==1 || segCtl.tag==3)
            {
                cell.replyTextButton.userInteractionEnabled=YES;
                cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                if (self.tableView.tag==1 || self.tableView.tag==7)
                {
                    cell.sendTextButton.userInteractionEnabled=YES;
                    cell.sendTextButton.backgroundColor=kNavBackgroundColor;
                    messageinboxdata = [messageArray objectAtIndex:indexPath.row];
                    selectedCallId=messageinboxdata.callId;
                    audiolength=[NSDateClass lengthformate:messageinboxdata.duration];
                    audioString=[NSString stringWithFormat:@"%@%d%d",messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid];
                    audiologString=[NSString stringWithFormat:@"callId:%@ callType:%d storeid:%d from:%@ callStartTime:%f audiolength:%@",messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid,messageinboxdata.from,messageinboxdata.callStartTime,audiolength];
                }
                else if (self.tableView.tag==2 || self.tableView.tag==8)
                {
                    cell.sendTextButton.userInteractionEnabled=NO;
                    cell.sendTextButton.backgroundColor=[UIColor grayColor];
                    messagesaveddata= [savedmessagesArray objectAtIndex:indexPath.row];
                    selectedCallId=messagesaveddata.callId;
                    audiolength=[NSDateClass lengthformate:messagesaveddata.duration];
                    audioString=[NSString stringWithFormat:@"%@%d%d",messagesaveddata.callId,messagesaveddata.callType,messagesaveddata.storeid];
                    audiologString=[NSString stringWithFormat:@"callId:%@ callType:%d storeid:%d from:%@ callStartTime:%f audiolength:%@",messagesaveddata.callId,messagesaveddata.callType,messagesaveddata.storeid,messagesaveddata.from,messagesaveddata.callStartTime,audiolength];
                }
                else if(self.tableView.tag==3 || self.tableView.tag==9)
                {
                    
                    cell.sendTextButton.userInteractionEnabled=NO;
                    cell.sendTextButton.backgroundColor=[UIColor grayColor];
                    [cell.replyTextButton setTitle:[dictLang objectForKey:@"Repost"] forState:UIControlStateNormal];
                    messagesentdata= [sentmessageArray objectAtIndex:indexPath.row];
                    selectedCallId=messagesentdata.callId;
                    audiolength=[NSDateClass lengthformate:messagesentdata.duration];
                    audioString=[NSString stringWithFormat:@"%@",messagesentdata.callId];
                    audiologString=[NSString stringWithFormat:@"callId:%@ from:%@ callStartTime:%f audiolength:%@",messagesentdata.callId,messagesentdata.to,messagesentdata.callStartTime,audiolength];
                }
                
                
            }
            else if(segCtl.tag==2)
            {
                if (self.tableView.tag==4)
                {
                    cell.replyTextButton.userInteractionEnabled=NO;
                    cell.replyTextButton.backgroundColor=[UIColor grayColor];
                    cell.sendTextButton.userInteractionEnabled=YES;
                    cell.sendTextButton.backgroundColor=kNavBackgroundColor;
                    announcementinboxdata = [announcementArray objectAtIndex:indexPath.row];
                    selectedCallId=announcementinboxdata.callId;
                    audiolength=[NSDateClass lengthformate:announcementinboxdata.duration];
                    audioString=[NSString stringWithFormat:@"%@%d%d",announcementinboxdata.callId,announcementinboxdata.callType,announcementinboxdata.storeid];
                    audiologString=[NSString stringWithFormat:@"callId:%@ callType:%d storeid:%d from:%@ callStartTime:%f audiolength:%@",announcementinboxdata.callId,announcementinboxdata.callType,announcementinboxdata.storeid,announcementinboxdata.from,announcementinboxdata.callStartTime,audiolength];
                }
                else  if (self.tableView.tag==5)
                {
                    cell.replyTextButton.userInteractionEnabled=NO;
                    cell.replyTextButton.backgroundColor=[UIColor grayColor];
                    cell.sendTextButton.userInteractionEnabled=NO;
                    cell.sendTextButton.backgroundColor=[UIColor grayColor];
                    announcementsaveddata = [savedannouncementArray objectAtIndex:indexPath.row];
                    selectedCallId=announcementsaveddata.callId;
                    audiolength=[NSDateClass lengthformate:announcementsaveddata.duration];
                    audioString=[NSString stringWithFormat:@"%@%d%d",announcementsaveddata.callId,announcementsaveddata.callType,announcementsaveddata.storeid];
                    audiologString=[NSString stringWithFormat:@"callId:%@ callType:%d storeid:%d from:%@ callStartTime:%f audiolength:%@",announcementsaveddata.callId,announcementsaveddata.callType,announcementsaveddata.storeid,announcementsaveddata.from,announcementsaveddata.callStartTime,audiolength];
                }
                else if (self.tableView.tag==6)
                {
                    announcementsentdata = [sentannouncementArray objectAtIndex:indexPath.row];
                    selectedCallId=announcementsentdata.callId;
                    audiolength=[NSDateClass lengthformate:announcementsentdata.duration];
                    if (announcementsentdata.callType==10 || announcementsentdata.callType==16)
                    {
                        
                        
                        cell.replyTextButton.userInteractionEnabled=NO;
                        cell.replyTextButton.backgroundColor=[UIColor grayColor];
                    }
                    else
                    {
                        cell.replyTextButton.userInteractionEnabled=YES;
                        cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                    }
                    cell.sendTextButton.userInteractionEnabled=NO;
                    cell.sendTextButton.backgroundColor=[UIColor grayColor];
                    [cell.replyTextButton setTitle:[dictLang objectForKey:@"Repost"] forState:UIControlStateNormal];
                    
                    audioString=[NSString stringWithFormat:@"%@",announcementsentdata.callId];
                    audiologString=[NSString stringWithFormat:@"callId:%@ from:%@ callStartTime:%f audiolength:%@",announcementsentdata.callId,announcementsentdata.to,announcementsentdata.callStartTime,audiolength];
                    
                }
            }
            
            cell.currentTimeSlider.value = 00.00;
            self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[[AudioManager getsharedInstance]getEarboxAudioPath:audioString] error:NULL];
            self.audioPlayer.delegate=self;
            if ([self.audioPlayer duration]>1.0)
            {
                cell.duration.text =audiolength;
                cell.playButton.userInteractionEnabled=YES;
                cell.playButton.backgroundColor=kNavBackgroundColor;
                [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                                 forState:UIControlStateNormal];
                NSLog(@"EarboxViewController message/announcement taped-This %@ audio file found in Database play button enabled",audiologString);
                
            }
            else
            {
                cell.playButton.backgroundColor=[UIColor grayColor];
                [cell.playButton setImage:[UIImage imageNamed:@"download"]
                                 forState:UIControlStateNormal];
                NSLog(@"EarboxViewController message/announcement taped-This %@ audio file not found in Database play button disabled",audiologString);
                
            }
            [self.tableView endUpdates];
            
            [self expandrow];
        }
    }
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    if (tableView==nameButtonalertTable)
    {
    }
    else
    {
        if ([indexPath compare:expandedIndexPath] == NSOrderedSame) {
            expandedIndexPath = nil;
        } else {
            expandedIndexPath = indexPath;
        }
        if (self.tableView.tag==1 || self.tableView.tag==7)
        {
            messageinboxdata= [messageArray objectAtIndex:indexPath.row];
            selectedCallId=messageinboxdata.callId;
            [[NSUserDefaults standardUserDefaults] setObject:messageinboxdata.callId forKey:@"call_ReplyID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if(self.tableView.tag==2 || self.tableView.tag==8)
        {
            messagesaveddata= [savedmessagesArray objectAtIndex:indexPath.row];
            selectedCallId=messagesaveddata.callId;
            [[NSUserDefaults standardUserDefaults] setObject:messagesaveddata.callId forKey:@"call_ReplyID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        isRowClosed=YES;
        cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.currentTimeSlider.value = 00.00;
        cell.timeElapsed.text = @"00:00";
        [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                         forState:UIControlStateNormal];
        self.isPaused=FALSE;
        indexpathcell=indexPath;
        [self.audioPlayer pause];
        [self.tableView reloadData];
        deselected=YES;
        [self tableViewUpdate];
    }
}

#pragma mark - Nameicon pressed method
-(void)nameTextButtonClicked:(UIButton *)button
{
    [self nameButtonalert];
    alerttable.hidden=NO;
    nameButtonalertTable.delegate=self;
    nameButtonalertTable.dataSource=self;
    [nameButtonalertTable reloadData];
    
    if (segCtl.tag==3)
    {
        NSArray *stores=[NSArray arrayWithObjects:@"Store1",@"Store2",@"Store3",@"Store4",@"Store5", nil];
        NSArray *groups=[NSArray arrayWithObjects:@"Groups1",@"Groups2",@"Groups3",@"Groups4",@"Groups5", nil];
        groupDict=[NSDictionary dictionaryWithObjectsAndKeys:stores,@"Stores",groups,@"Groups", nil];
        NSInteger height;
        
        height = 40+50*[stores count]+[groups count];
        
        if (height> self.view.frame.size.height)
        {
            height =self.view.frame.size.height-120;
        }
        nameButtonalertTable.frame= CGRectMake(nameButtonalertTable.frame.origin.x, nameButtonalertTable.frame.origin.y, nameButtonalertTable.frame.size.width, height);
        [nameButtonalertTable reloadData];
    }
}

-(void)nameButtonalert
{
    alerttable=[[UIView alloc]init];
    alerttable.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    alerttable.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    nameButtonalertTable=[[UITableView alloc]init];
    nameButtonalertTable.frame=CGRectMake(20, (self.view.frame.size.height/2)-170, self.view.frame.size.width-40, 380);
    nameButtonalertTable.layer.cornerRadius=10;
    nameButtonalertTable.layer.borderWidth=2;
    nameButtonalertTable.layer.borderColor=[UIColor lightGrayColor].CGColor;
    cancelButton=[UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame=CGRectMake(self.view.frame.size.width-35, (self.view.frame.size.height/2)-197, 30, 30);
    [cancelButton setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonAction) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.backgroundColor=[UIColor whiteColor];
    cancelButton.clipsToBounds = YES;
    cancelButton.layer.cornerRadius = cancelButton.frame.size.width/2.0f;
    cancelButton.layer.borderColor=kNavBackgroundColor.CGColor;
    cancelButton.layer.borderWidth=1.0f;
    [alerttable addSubview:cancelButton];
    alerttable.hidden=YES;
    nameButtonalertTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [alerttable addSubview:nameButtonalertTable];
    [self.view addSubview:alerttable];
}

-(void)cancelButtonAction
{
    [alerttable removeFromSuperview];
}

#pragma mark - Saveicon pressed method
-(void)saveButtonPressed:(UIButton *)button
{
    expandedIndexPath = nil;
    [self audioPlayerStop];
    if (segCtl.tag==1 || segCtl.tag==3)
    {
        if (self.tableView.tag==1)
        {
            EarboxDataStore *store = [EarboxDataStore sharedStore];
            [self getmessageinboxdata:@"EarboxInbox" calltype:4];
            [self savemessagedata:[messageArray objectAtIndex:button.tag]];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Messages"] message:[dictLang objectForKey:@"Message saved successfully"] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
            [alert show];
            
            savedmessagesArray=[store artists:@"EarboxSaved" storeid:storeId calltype:4];
            messageinboxdata=[messageArray objectAtIndex:button.tag];
            [[StateMachineHandler sharedManager]sendMsgActionToNative:3 txnid:[NSString stringWithFormat:@"%@",messageinboxdata.callId]];
            [store deleteEarboxInbox:messageinboxdata];
            messageArray= [store artists:@"EarboxInbox" storeid:storeId calltype:4];
        }
        if (self.tableView.tag==7)
        {
            
            EarboxDataStore *store = [EarboxDataStore sharedStore];
            [self getmessageinboxdata:@"EarboxInbox" calltype:17];
            [self savemessagedata:[messageArray objectAtIndex:button.tag]];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Messages"] message:[dictLang objectForKey:@"Message saved successfully"] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
            [alert show];
            
            savedmessagesArray=[store artists:@"EarboxSaved" storeid:storeId calltype:17];
            messageinboxdata=[messageArray objectAtIndex:button.tag];
            [[StateMachineHandler sharedManager]sendMsgActionToNative:3 txnid:[NSString stringWithFormat:@"%@",messageinboxdata.callId]];
            [store deleteEarboxInbox:messageinboxdata];
            messageArray= [store artists:@"EarboxInbox" storeid:storeId calltype:17];
        }
        
    }
    else if (segCtl.tag==2)
    {
        if (self.tableView.tag==4)
        {
            EarboxDataStore *store = [EarboxDataStore sharedStore];
            [self saveannouncementdata:[announcementArray objectAtIndex:button.tag]];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Announcements"] message:[dictLang objectForKey:@"Announcement saved successfully"] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"OK"] otherButtonTitles:nil, nil];
            [alert show];
            savedannouncementArray=[store artists:@"AnnouncementSaved" storeid:storeId calltype:0];
            announcementinboxdata=[announcementArray objectAtIndex:button.tag];
            [[StateMachineHandler sharedManager]sendMsgActionToNative:3 txnid:[NSString stringWithFormat:@"%@",announcementinboxdata.callId]];
            [store deleteAnnouncementInbox:announcementinboxdata];
            announcementArray= [store artists:@"AnnouncementInbox" storeid:storeId calltype:0];
            
        }
    }
    [self.tableView reloadData];
}


#pragma mark - Reply pressed method
-(void)replybuttonDownPressed:(UIButton *)button
{
    isReplying=YES;
    [self audioPlayerStop];
    if (self.tableView.tag==6 || self.tableView.tag==3 || self.tableView.tag==9)
    {
        [self.view addSubview: [ActivityIndicatorView alphaView:self.view]];
    }
    else
    {
        rootView = [[[NSBundle mainBundle] loadNibNamed:@"ReplayView" owner:self options:nil] objectAtIndex:0];
        [rootView alphaView:self.view];
        rootView.frame = CGRectMake(10, (self.view.frame.size.height/2)-150, self.view.frame.size.width-20, rootView.frame.size.height);
        rootView.layer.cornerRadius=10;
        rootView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:rootView];
        [speakerButton setEnabled:NO];
        [rootView load];
        isReplying=YES;
        [self audioPlayerStop];
        if (self.tableView.tag==6 || self.tableView.tag==3 || self.tableView.tag==9)
        {
            
        }
        else
        {
            if (self.tableView.tag==1 || self.tableView.tag==7)
            {
                replyingTo = [NSString stringWithFormat:@"%@",@"tag1"];
            }
            else if (self.tableView.tag==2 || self.tableView.tag==8)
            {
                replyingTo = [NSString stringWithFormat:@"%@",@"tag2"];
            }
            replyClicked=NO;
        }
    }
}

-(void)animation
{
    [UIView animateWithDuration:0.5 animations:^{
        imagebutton.transform = CGAffineTransformMakeScale(7.21, 7.21);
    }completion:^(BOOL finished){
        [UIView animateWithDuration:0.5 animations:^{
            imagebutton.transform = CGAffineTransformIdentity;
            
        }completion:^(BOOL finished){
            if (!replyClicked)
            {
                [self animationZoom];
            }
        }];
    }];
}

-(void)animationZoom
{
    [UIView animateWithDuration:0.5 animations:^{
        imagebutton.transform = CGAffineTransformMakeScale(7.21, 7.21);
    }completion:^(BOOL finished){
        [UIView animateWithDuration:0.5 animations:^{
            imagebutton.transform = CGAffineTransformIdentity;
            
        }completion:^(BOOL finished){
            if (!replyClicked)
            {
                [self animation];
            }
        }];
    }];
}

-(void)replybuttonUpPressed:(UIButton *)button
{
    isReplying=NO;
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:rowindex inSection:0] ;
    cell = [self.tableView cellForRowAtIndexPath:myIP];
    cell.replyTextButton.userInteractionEnabled=NO;
    cell.replyTextButton.backgroundColor=[UIColor grayColor];
    replyClicked=YES;
    if (self.tableView.tag==6 || self.tableView.tag==3 || self.tableView.tag==9)
    {
        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
        [[AudioManager getsharedInstance] sendTapped];
    }
}

-(void)replybuttonoutsidePressed:(UIButton *)button
{
    if (isReplying)
    {
        [self replybuttonUpPressed:nil];
    }
}


-(NSString *)announcsmentType:(int)type
{
    switch (type)
    {
        case 4:
            return @"message";
            break;
        case 17:
            return @"message";
            break;
        case 10:
            return @"announcement_huddle";
            break;
        case 16:
            return @"announcement_sales";
            break;
        case 21:
            return @"announcement_now";
            break;
        case 23:
            return @"announcement_today";
            break;
        case 25:
            return @"announcement_week";
        case 200:
            return @"announcement_week";
            break;
    }
    return 0;
}

-(void)replayCancelNotification:(NSNotification *)sender
{
    [ActivityIndicatorView activityIndicatorViewRemove1:self.view];
    [rootView removeFromSuperview];
    cell.replyTextButton.userInteractionEnabled=YES;
    [speakerButton setEnabled:YES];
    cell.replyTextButton.backgroundColor=kNavBackgroundColor;
    
}

#pragma mark - Audio send/recieve method
-(void)sendAnnouncement:(NSNotification *)sender
{
    
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"EarboxViewController"])
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"EarboxVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
            
        }
        else
        {
            NSURL *audiourl=sender.object;
            {
                if (self.tableView.tag==3)
                {
                    messagesentdata=[sentmessageArray objectAtIndex:rowindex];
                    audiourl=[[AudioManager getsharedInstance]getEarboxAudioPath: [NSString stringWithFormat:@"%@",messagesentdata.callId]];
                }
                else if(self.tableView.tag==9)
                {
                    messagesentdata=[sentmessageArray objectAtIndex:rowindex];
                    audiourl=[[AudioManager getsharedInstance]getEarboxAudioPath: [NSString stringWithFormat:@"%@",messagesentdata.callId]];
                }
                else if(self.tableView.tag==6)
                {
                    announcementsentdata=[sentannouncementArray objectAtIndex:rowindex];
                    audiourl=[[AudioManager getsharedInstance]getEarboxAudioPath: [NSString stringWithFormat:@"%@",announcementsentdata.callId]];
                }
                
            }
            
            if ([[AudioManager getsharedInstance]getAudioduration:audiourl]<1.0)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"audiolengthshort" object:nil];
                NSString *screencode;
                switch (self.tableView.tag)
                {
                    case 1:
                        screencode=@"EarboxMessageInboxVC";
                        break;
                    case 2:
                        screencode=@"EarboxMessageSavedVC";
                        break;
                    case 3:
                        screencode=@"EarboxMessageSentVC";
                        break;
                    case 4:
                        screencode=@"EarboxAnnouncementInboxVC";
                        break;
                    case 5:
                        screencode=@"EarboxAnnouncementSavedVC";
                        break;
                    case 6:
                        screencode=@"EarboxAnnouncementSentVC";
                        break;
                    case 7:
                        screencode=@"EarboxGroupInboxVC";
                        break;
                    case 8:
                        screencode=@"EarboxGroupSavedVC";
                        break;
                    case 9:
                        screencode=@"EarboxGroupSentVC";
                        break;
                }
                NSString *errorcode=[NSString stringWithFormat:@"(%@%@%@%@i)",[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"Audio length is too short to send"]];
                [AlertView alert:nil message:[NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Audio length is too short to send"],errorcode]];
                NSLog(@"Audio length is:%f and Audio length is too short to send %@",[[AudioManager getsharedInstance]getAudioduration:audiourl],errorcode);
                cell.replyTextButton.userInteractionEnabled=YES;
                cell.replyTextButton.backgroundColor=kNavBackgroundColor;
            }
            else
            {
                if (self.tableView.tag==6 || self.tableView.tag==3 || self.tableView.tag==9)
                {
                    [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
                }
                else
                {
                }
                NSString *callTxId,*pushTxId;
                double calltype;
                NSString *millisec=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ];
                NSArray *tempArray = [millisec componentsSeparatedByString:@"."];
                NSString *macId;
                if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
                    macId=[[NSString stringWithFormat:@"%@",
                            [[[[UIDevice currentDevice] identifierForVendor] UUIDString]substringFromIndex:24]]lowercaseString];
                }
                NSData *file1Data;
                if (segCtl.tag==1 || segCtl.tag==3)
                {
                    if (self.tableView.tag==1 || self.tableView.tag==7)
                    {
                        if (self.tableView.tag==1)
                        {
                            [self getFreshInboxList:@"EarboxInbox" calltype:4];
                        }
                        else if(self.tableView.tag==7)
                        {
                            [self getFreshInboxList:@"EarboxInbox" calltype:17];
                        }
                        
                        NSString *callID_Defaults = [[NSUserDefaults standardUserDefaults] objectForKey:@"call_ReplyID"];
                        
                        BOOL found= NO;
                        
                        for (int i=0; i<[freshInboxList count]; i++)
                        {
                            messageFreshList =[freshInboxList objectAtIndex:i];
                            
                            if ([messageFreshList.callId isEqualToString: callID_Defaults])
                            {
                                found = YES;
                                break;
                            }
                        }
                        if (found)
                        {
                            
                            messageinboxdata=[messageArray objectAtIndex:rowindex];
                            contactNameString=messageinboxdata.from;
                            calltype=4;
                            replyAudiourl=sender.object;
                            file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
                            self.pnsdic=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",contactNameString,@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:replyAudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithInt:calltype],@"callType",[NSNumber numberWithDouble:0],@"ttl",messageinboxdata.pushTxId,@"pushTxnId",nil];
                            if (calltype==4)
                            {
                                NSDictionary *whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[NSArray arrayWithObject:storeNameString],@"store", [NSArray arrayWithObject:[apploginDetails objectForKey:@"chainName"]],@"chain",@"managerApp",@"originatorType",[self announcsmentType:calltype],@"type",macId,@"macId",[tempArray objectAtIndex:0],@"request_time",@[@""],@"groups",nil];
                                [HttpHandler audioFileSend:whoDic audioFile:file1Data pns: self.pnsdic userScreen:@"EarboxMessageInboxVC"];
                                [self.pnsdic setObject:@"HEARD" forKey:@"status"];
                            }
                            else
                            {
                                NSDictionary *whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[@""],@"users",[NSArray arrayWithObject:storeNameString],@"store", [NSArray arrayWithObject:[apploginDetails objectForKey:@"chainName"]],@"chain",@"managerApp",@"originatorType",[self announcsmentType:calltype],@"type",macId,@"macId",[tempArray objectAtIndex:0],@"request_time",@[contactNameString],@"groups",nil];
                                [HttpHandler audioFileSend:whoDic audioFile:file1Data pns: self.pnsdic userScreen:@"EarboxGroupInboxVC"];
                                [self.pnsdic setObject:messageinboxdata.groupName forKey:@"groupName"];
                                [self.pnsdic setObject:@"HEARD" forKey:@"status"];
                            }
                        }
                        else
                        {
                            if (self.tableView.tag==1)
                            {
                                [self getmessageinboxdata:@"EarboxInbox" calltype:4];
                            }
                            else if(self.tableView.tag==7)
                            {
                                [self getmessageinboxdata:@"EarboxInbox" calltype:17];
                            }
                            [UIView animateWithDuration:2 animations:^{
                                dispatch_async(dispatch_get_main_queue(), ^{ [self.tableView reloadData]; });
                            }
                                             completion:^(BOOL finished) {
                                                 NSString *screencode;
                                                 if (self.tableView.tag==1)
                                                 {
                                                     screencode=@"EarboxMessageInboxVC";
                                                 }
                                                 if (self.tableView.tag==7)
                                                 {
                                                     screencode=@"EarboxGroupInboxVC";
                                                 }
                                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you have just replied is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you have just replied is expired"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                                                 [alert show];
                                             } ];
                        }
                        
                    }
                    else if (self.tableView.tag==2 || self.tableView.tag==8)
                    {
                        if (self.tableView.tag==2)
                        {
                            [self getmessagesaveddata:@"EarboxSaved" calltype:4];
                        }
                        else if(self.tableView.tag==8)
                        {
                            [self getmessagesaveddata:@"EarboxSaved" calltype:17];
                        }
                        messagesaveddata=[savedmessagesArray objectAtIndex:rowindex];
                        contactNameString=messagesaveddata.from;
                        calltype=4;
                        replyAudiourl=sender.object;
                        file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
                        
                        self.pnsdic=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",contactNameString,@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:replyAudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithInt:calltype],@"callType",[NSNumber numberWithDouble:0],@"ttl",messagesaveddata.pushTxId,@"pushTxnId",nil];
                        if (calltype==4)
                        {
                            NSDictionary *whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[NSArray arrayWithObject:storeNameString],@"store", [NSArray arrayWithObject:[apploginDetails objectForKey:@"chainName"]],@"chain",@"managerApp",@"originatorType",[self announcsmentType:calltype],@"type",macId,@"macId",[tempArray objectAtIndex:0],@"request_time",@[@""],@"groups",nil];
                            [HttpHandler audioFileSend:whoDic audioFile:file1Data pns: self.pnsdic userScreen:@"EarboxMessageSavedVC"];
                            [self.pnsdic setObject:@"HEARD" forKey:@"status"];
                        }
                        else
                        {
                            NSDictionary *whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[@""],@"users",[NSArray arrayWithObject:storeNameString],@"store", [NSArray arrayWithObject:[apploginDetails objectForKey:@"chainName"]],@"chain",@"managerApp",@"originatorType",[self announcsmentType:calltype],@"type",macId,@"macId",[tempArray objectAtIndex:0],@"request_time",@[contactNameString],@"groups",nil];
                            [HttpHandler audioFileSend:whoDic audioFile:file1Data pns: self.pnsdic userScreen:@"EarboxGroupSavedVC"];
                            [self.pnsdic setObject:messagesaveddata.groupName forKey:@"groupName"];
                            [self.pnsdic setObject:@"HEARD" forKey:@"status"];
                        }
                    }
                    else if (self.tableView.tag==3 || self.tableView.tag==9)
                    {
                        NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
                        if (selectedRows==0)
                        {
                            if (self.tableView.tag==3)
                            {
                                [self getmessagesentdata:@"EarboxSent" calltype:4];
                            }
                            else if(self.tableView.tag==9)
                            {
                                [self getmessagesentdata:@"EarboxSent" calltype:17];
                            }
                        }
                        messagesentdata=[sentmessageArray objectAtIndex:rowindex];
                        contactNameString=messagesentdata.to;
                        calltype=messagesentdata.callType;
                        
                        NSString *milliseccallTxId=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]*1000];
                        NSArray *tempArraycallTxId = [milliseccallTxId componentsSeparatedByString:@"."];
                        callTxId=[tempArraycallTxId objectAtIndex:0];
                        file1Data = [[NSData alloc] initWithContentsOfURL:[[AudioManager getsharedInstance]getEarboxAudioPath: [NSString stringWithFormat:@"%@",messagesentdata.callId]]];
                        replyAudiourl=[[AudioManager getsharedInstance]getEarboxAudioPath: [NSString stringWithFormat:@"%@",messagesentdata.callId]];
                        [[AudioManager getsharedInstance]sentaudioPath:callTxId oldurl:replyAudiourl];
                        self.pnsdic=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",contactNameString,@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0] doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:replyAudiourl]],@"duration",@"medium",@"priority",callTxId,@"callTxId" ,[NSNumber numberWithDouble:calltype],@"callType",[NSNumber numberWithDouble:0],@"ttl",messagesentdata.pushTxId,@"pushTxnId",nil];
                        if (calltype==4)
                        {
                            NSDictionary *whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[NSArray arrayWithObject:storeNameString],@"store", [NSArray arrayWithObject:[apploginDetails objectForKey:@"chainName"]],@"chain",@"managerApp",@"originatorType",[self announcsmentType:calltype],@"type",macId,@"macId",[tempArray objectAtIndex:0],@"request_time",@[@""],@"groups",nil];
                            [HttpHandler audioFileSend:whoDic audioFile:file1Data pns: self.pnsdic userScreen:@"EarboxMessageSentVC"];
                            [self.pnsdic setObject:@"HEARD" forKey:@"status"];
                        }
                        else
                        {
                            NSDictionary *whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[@""],@"users",[NSArray arrayWithObject:storeNameString],@"store", [NSArray arrayWithObject:[apploginDetails objectForKey:@"chainName"]],@"chain",@"managerApp",@"originatorType",[self announcsmentType:calltype],@"type",macId,@"macId",[tempArray objectAtIndex:0],@"request_time",@[messagesentdata.groupName],@"groups",nil];
                            [HttpHandler audioFileSend:whoDic audioFile:file1Data pns: self.pnsdic userScreen:@"EarboxGroupSentVC"];
                            [self.pnsdic setObject:@"HEARD" forKey:@"status"];
                            [self.pnsdic setObject:messagesentdata.groupName forKey:@"to"];
                        }
                        
                    }
                }
                else if(segCtl.tag==2)
                {
                    if (self.tableView.tag==4)
                    {
                        [self getannouncementinboxdata:@"AnnouncementInbox" calltype:0];
                        announcementinboxdata=[announcementArray objectAtIndex:rowindex];
                        contactNameString=announcementinboxdata.from;
                        calltype=announcementinboxdata.callType;
                        callTxId=[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"];
                        pushTxId=announcementinboxdata.pushTxId;
                        replyAudiourl=sender.object;
                        file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
                    }
                    else if (self.tableView.tag==5)
                    {
                        [self getannouncementsaveddata:@"AnnouncementSaved" calltype:0];
                        announcementsaveddata=[savedannouncementArray objectAtIndex:rowindex];
                        contactNameString=announcementsaveddata.from;
                        calltype=announcementsaveddata.callType;
                        callTxId=[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"];
                        pushTxId=announcementsaveddata.pushTxId;
                        replyAudiourl=sender.object;
                        file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
                    }
                    else {
                        NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
                        if (selectedRows==0 || isRowClosed)
                        {
                            [self getannouncementsentdata:@"AnnouncementSent" calltype:0];
                        }
                        announcementsentdata=[sentannouncementArray objectAtIndex:rowindex];
                        if (![announcementsentdata.to isEqualToString:@"All"])
                        {
                            contactNameString=announcementsentdata.to;
                        }
                        else
                        {
                            contactNameString=@"all";
                        }
                        calltype=announcementsentdata.callType;
                        NSString *milliseccallTxId=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]*1000];
                        NSArray *tempArraycallTxId = [milliseccallTxId componentsSeparatedByString:@"."];
                        callTxId=[tempArraycallTxId objectAtIndex:0];
                        pushTxId=announcementsentdata.pushTxId;
                        file1Data = [[NSData alloc] initWithContentsOfURL:[[AudioManager getsharedInstance]getEarboxAudioPath: [NSString stringWithFormat:@"%@",announcementsentdata.callId]]];
                        replyAudiourl=[[AudioManager getsharedInstance]getEarboxAudioPath: [NSString stringWithFormat:@"%@",announcementsentdata.callId]];
                        [[AudioManager getsharedInstance]sentaudioPath:callTxId oldurl:replyAudiourl];
                    }
                    self.pnsdic=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",contactNameString,@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0] doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:replyAudiourl]],@"duration",@"medium",@"priority",callTxId,@"callTxId" ,[NSNumber numberWithDouble:calltype],@"callType",[NSNumber numberWithDouble:0],@"ttl",pushTxId,@"pushTxnId",nil];
                    if (announcementsentdata.callType==200)
                    {
                        NSDictionary *whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[@""],@"users",[NSArray arrayWithObject:storeNameString],@"store", [NSArray arrayWithObject:[apploginDetails objectForKey:@"chainName"]],@"chain",@"managerApp",@"originatorType",[self announcsmentType:calltype],@"type",macId,@"macId",[tempArray objectAtIndex:0],@"request_time",@[announcementsentdata.groupName],@"groups",nil];
                        [HttpHandler audioFileSend:whoDic audioFile:file1Data pns: self.pnsdic userScreen:@"EarboxAnnouncementSentVC"];
                        [self.pnsdic setObject:@"HEARD" forKey:@"status"];
                    }
                    else
                    {
                        NSDictionary *whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactNameString],@"users",[NSArray arrayWithObject:storeNameString],@"store", [NSArray arrayWithObject:[apploginDetails objectForKey:@"chainName"]],@"chain",@"managerApp",@"originatorType",[self announcsmentType:calltype],@"type",macId,@"macId",[tempArray objectAtIndex:0],@"request_time",@[@""],@"groups",nil];
                        [HttpHandler audioFileSend:whoDic audioFile:file1Data pns: self.pnsdic userScreen:@"EarboxAnnouncementSentVC"];
                        [self.pnsdic setObject:@"HEARD" forKey:@"status"];
                    }
                    
                    
                }
            }
        }
    }
    
}

-(void)recievedAnnouncement:(NSNotification *)sender
{
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"EarboxViewController"])
    {
        if (self.tableView.tag==6 || self.tableView.tag==3 || self.tableView.tag==9)
        {
            
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"replayNotification" object:sender.object];
        }
        if ([[sender.object objectAtIndex:0] isEqualToString:@"error"])
        {
            //@"Message not Sent";
            
        }
        else if ([[sender.object objectAtIndex:0] isEqualToString:@"message"])
        {
            [self notifyTcm];
            cell.replyTextButton.userInteractionEnabled=YES;
            cell.replyTextButton.backgroundColor=kNavBackgroundColor;
            BOOL inboxTag = NO;
            BOOL saveTag = NO;
            
            //@"Message Sent";
            if (segCtl.tag==1 || segCtl.tag==3)
            {
                if ([replyingTo isEqualToString:@"tag1"])
                {
                    inboxTag = YES;
                }
                else if ([replyingTo isEqualToString:@"tag2"])
                {
                    saveTag = YES;
                }
                
                if (self.audioPlayer.isPlaying)
                {
                    
                }
                else
                {
                    
                    NSString *callID_Defaults = [[NSUserDefaults standardUserDefaults] objectForKey:@"call_ReplyID"];
                    BOOL found= NO;
                    if (self.tableView.tag==1 || self.tableView.tag==2)
                    {
                        [self getFreshInboxList:@"EarboxInbox" calltype:4];
                        [self getFreshSavedList:@"EarboxSaved" calltype:4];
                    }
                    if (self.tableView.tag==7 || self.tableView.tag==8)
                    {
                        [self getFreshInboxList:@"EarboxInbox" calltype:17];
                        [self getFreshSavedList:@"EarboxSaved" calltype:17];
                    }
                    for (int i=0; i<[freshInboxList count]; i++)
                    {
                        messageFreshList =[freshInboxList objectAtIndex:i];
                        
                        if ([messageFreshList.callId isEqualToString:callID_Defaults])
                        {
                            found = YES;
                            break;
                        }
                    }
                    
                    if(found == NO)
                    {
                        for (int i=0; i<[freshSavedList count]; i++)
                        {
                            messageFreshSavedData =[freshSavedList objectAtIndex:i];
                            
                            if ([messageFreshSavedData.callId isEqualToString: callID_Defaults])
                            {
                                found = YES;
                                break;
                            }
                        }
                    }
                    [self.pnsdic setObject:[NSNumber numberWithInt:1] forKey:@"maCallType"];
                    [[Earboxinboxmodel sharedModel]messagesentSave:[NSArray arrayWithObject: self.pnsdic]];
                    
                    if(found)
                    {
                        
                        if (self.tableView.tag==1 || inboxTag || self.tableView.tag==7)
                        {
                            
                            messageinboxdata=[freshReplyList objectAtIndex:rowindex];
                            messageinboxdata.reply =@"1";
                            NSError *error=nil;
                            [[EarboxDataStore sharedStore] save:error];
                        }
                        else if (self.tableView.tag==2  || saveTag || self.tableView.tag==8)
                        {
                            
                            messageFreshSavedData=[freshSavedList objectAtIndex:rowindex];
                            messageFreshSavedData.reply =@"1";
                            NSError *error=nil;
                            [[EarboxDataStore sharedStore] save:error];
                        }
                    }
                    
                }
            }
            else
            {
                if (self.audioPlayer.isPlaying)
                {
                    
                }
                else
                {
                    [self.pnsdic setObject:@"Yes" forKey:@"reply"];
                    [self.pnsdic setObject:announcementsentdata.groupName forKey:@"groupName"];
                    [self.pnsdic setObject:[NSNumber numberWithInt:1] forKey:@"maCallType"];
                    [[Earboxinboxmodel sharedModel]announcementsentSave:[NSArray arrayWithObject: self.pnsdic]];
                    cell.replyTextButton.userInteractionEnabled=YES;
                    cell.replyTextButton.backgroundColor=kNavBackgroundColor;
                }
                
            }
            if (self.tableView.tag==6 || self.tableView.tag==3 || self.tableView.tag==9)
            {
                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                
            }
            else
            {
            }
            
        }
    }
    
}

-(void)notifyTcm
{
    [HttpHandler notifyTcm:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId storeId:storeId userScreen:@"EarboxVC"];
    
}
#pragma mark - Delete pressed method
-(void)deleteTextButtonClicked:(UIButton *)button
{
    deletebuttonindex=button.tag;
    if (segCtl.tag==2)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Alert"] message:[dictLang objectForKey:@"Are you sure you want to delete this Announcement?"] delegate:self cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dictLang objectForKey:@"Alert"] message:[dictLang objectForKey:@"Are you sure you want to delete this Message?"] delegate:self cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
        [alert show];
    }
    
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self audioPlayerStop];
    NSString *status;
    NSString *callId;
    if(alertView.tag==250)
    {
        [self tableViewUpdate];
    }
    else if (buttonIndex==0)
    {
        
    }
    else
    {
        deselected=YES;
        
        EarboxDataStore *store = [EarboxDataStore sharedStore];
        expandedIndexPath = nil;
        if (segCtl.tag==1 || segCtl.tag==3)
        {
            if (self.tableView.tag==1 || self.tableView.tag==7)
            {
                if (self.tableView.tag==1)
                {
                    [self getmessageinboxdata:@"EarboxInbox" calltype:4];
                }
                else if(self.tableView.tag==7)
                {
                    [self getmessageinboxdata:@"EarboxInbox" calltype:17];
                }
                NSString *screencode;
                if (self.tableView.tag==1)
                {
                    screencode=@"EarboxMessageInboxVC";
                }
                if (self.tableView.tag==7)
                {
                    screencode=@"EarboxGroupInboxVC";
                }
                if (messageArray.count>deletebuttonindex) {
                    messageinboxdata=[messageArray objectAtIndex:deletebuttonindex];
                    
                    
                    if (messageinboxdata.fault)
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                        alert.tag=250;
                        [alert show];
                    }
                    else
                    {
                        messageinboxdata = [[store updateArtist:@"EarboxInbox" callId:selectedCallId] objectAtIndex:0];
                        status=messageinboxdata.status;
                        [self messageDelete:status];
                        
                        [[StateMachineHandler sharedManager]sendMsgActionToNative:2 txnid:selectedCallId];
                        [store deleteEarboxInbox:messageinboxdata];
                    }
                }
                else
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                    alert.tag=250;
                    [alert show];
                }
            }
            else if (self.tableView.tag==2 || self.tableView.tag==8)
            {
                if(self.tableView.tag==2)
                {
                    [self getmessagesaveddata:@"EarboxSaved" calltype:4];
                }
                else if(self.tableView.tag==8)
                {
                    [self getmessagesaveddata:@"EarboxSaved" calltype:17];
                }
                NSString *screencode;
                if (self.tableView.tag==2)
                {
                    screencode=@"EarboxMessageSavedVC";
                }
                if (self.tableView.tag==8)
                {
                    screencode=@"EarboxGroupSavedVC";
                }
                if (savedmessagesArray.count>deletebuttonindex) {
                    messagesaveddata=[savedmessagesArray objectAtIndex:deletebuttonindex];
                    if (messagesaveddata.fault)
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                        alert.tag=250;
                        [alert show];
                    }
                    else
                    {
                        messagesaveddata = [[store updateArtist:@"EarboxSaved" callId:selectedCallId] objectAtIndex:0];
                        status=messagesaveddata.status;
                        [self messageDelete:status];
                        
                        [[StateMachineHandler sharedManager]sendMsgActionToNative:2 txnid:selectedCallId];
                        [store deleteEarboxSaved:messagesaveddata];
                    }
                }
                else
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The message that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:screencode],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                    alert.tag=250;
                    [alert show];
                }
            }
            else
            {
                if(self.tableView.tag==3)
                {
                    [self getmessagesentdata:@"EarboxSent" calltype:4];
                }
                else if(self.tableView.tag==9)
                {
                    [self getmessagesentdata:@"EarboxSent" calltype:17];
                }
                messagesentdata=[sentmessageArray objectAtIndex:deletebuttonindex];
                callId=[NSString stringWithFormat:@"%@",messagesentdata.callId ];
                [[StateMachineHandler sharedManager]sendMsgActionToNative:2 txnid:callId];
                [store deleteEarboxSent:messagesentdata];
            }
            
        }
        else
        {
            if (self.tableView.tag==4 )
            {
                [self getannouncementinboxdata:@"AnnouncementInbox" calltype:0];
                if (announcementArray.count>deletebuttonindex) {
                    
                    announcementinboxdata=[announcementArray objectAtIndex:deletebuttonindex];
                    if (announcementinboxdata.fault)
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The announcement that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:@"EarboxAnnouncementInboxVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                        alert.tag=250;
                        [alert show];
                    }
                    else
                    {
                        announcementinboxdata = [[store updateArtist:@"AnnouncementInbox" callId:selectedCallId] objectAtIndex:0];
                        status=announcementinboxdata.status;
                        [self announcementDelete:status];
                        
                        [[StateMachineHandler sharedManager]sendMsgActionToNative:2 txnid:selectedCallId];
                        [store deleteAnnouncementInbox:announcementinboxdata];
                    }
                }
                else
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The announcement that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:@"EarboxAnnouncementInboxVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                    alert.tag=250;
                    [alert show];
                }
                
            }
            else if (self.tableView.tag==5)
            {
                [self getannouncementsaveddata:@"AnnouncementSaved" calltype:0];
                if (savedannouncementArray.count>deletebuttonindex) {
                    
                    announcementsaveddata=[savedannouncementArray objectAtIndex:deletebuttonindex];
                    if (announcementsaveddata.fault)
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The announcement that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:@"EarboxAnnouncementSavedVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                        alert.tag=250;
                        [alert show];
                    }
                    else
                    {
                        announcementsaveddata = [[store updateArtist:@"AnnouncementSaved" callId:selectedCallId] objectAtIndex:0];
                        status=announcementsaveddata.status;
                        [self announcementDelete:status];
                        
                        [[StateMachineHandler sharedManager]sendMsgActionToNative:2 txnid:selectedCallId];
                        [store deleteAnnouncementSaved:announcementsaveddata];
                    }
                }
                else
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"The announcement that you are trying to delete is expired."],[[DiagnosticTool sharedManager] generateScreenCode:@"EarboxAnnouncementSavedVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"The message that you are trying to delete is expired"]] delegate:nil cancelButtonTitle:[dictLang objectForKey:@"Cancel"] otherButtonTitles:[dictLang objectForKey:@"OK"], nil];
                    alert.tag=250;
                    [alert show];
                }
            }
            else
            {
                [self getannouncementsentdata:@"AnnouncementSent" calltype:0];
                announcementsentdata=[sentannouncementArray objectAtIndex:deletebuttonindex];
                callId=[NSString stringWithFormat:@"%@",announcementsentdata.callId ];
                [[StateMachineHandler sharedManager]sendMsgActionToNative:2 txnid:callId];
                [store deleteAnnouncementSent:announcementsentdata];
            }
            
        }
        
        [[AudioManager getsharedInstance]deleteEarboxAudioPath:selectedCallId];
        [self tableViewUpdate];
        
    }
}

-(void)messageDelete:(NSString *)status
{
    if ([status isEqualToString:@"new"])
    {
        int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
        if (messagecount==0)
        {
            messagecount=0;
        }
        else
        {
            messagecount--;
        }
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagecount"];
        int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
}

-(void)announcementDelete:(NSString *)status
{
    if ([status isEqualToString:@"new"])
    {
        int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementcount"] intValue];
        if (announcementcount==0)
        {
            announcementcount=0;
        }
        else
        {
            announcementcount--;
        }
        
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:announcementcount] forKey:@"announcementcount"];
        
        int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
}

#pragma mark - Earpiece method
-(void)speakerButtonAction:(UIBarButtonItem *)button
{
    if (button.tag==1)
    {
        [speakerButton setImage:[UIImage imageNamed:@"headphones"]];
        speakerButton.tag=2;
        [[NSUserDefaults standardUserDefaults]setObject:@"mic" forKey:@"loud"];
    }
    else
    {
        speakerButton.tag=1;
        [speakerButton setImage:[UIImage imageNamed:@"loud"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"loud" forKey:@"loud"];
    }
    [[AudioManager getsharedInstance] switchAudioOutput];
}


-(void)speakerAudioChange
{
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"loud"] isEqualToString:@"mic"])
    {
        [speakerButton setImage:[UIImage imageNamed:@"headphones"]];
        speakerButton.tag=2;
        [[NSUserDefaults standardUserDefaults]setObject:@"mic" forKey:@"loud"];
        
    }
    else
    {
        speakerButton.tag=1;
        [speakerButton setImage:[UIImage imageNamed:@"loud"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"loud" forKey:@"loud"];
        
        
    }
    [[AudioManager getsharedInstance] switchAudioOutput];
}


#pragma BroadCast method
-(void)broadCostButtonAction
{
    BroadcastViewController *broadcastController = [self.storyboard instantiateViewControllerWithIdentifier:@"BroadCostViewController"];
    [self.navigationController pushViewController:broadcastController animated:YES];
    
}

#pragma mark - PushNotification recieve methods
-(void)pushnotificationrecieved:(NSNotification *)notification
{
    
    [self requestUndeliveredMsgs];
    [self speakerAudioChange];
}

-(void)notificationrecieved
{
    connectionLabel.hidden=YES;
}

#pragma mark - Audio completed(%) method
-(void)percentageCompleted:(NSNotification *)notification
{
    if ([[notification.object objectAtIndex:2] isEqualToString:@"EarboxInbox"])
    {
        if(messageArray.count>0)
        {
            messageinboxdata=[messageArray objectAtIndex:rowindex];
            
            NSString *str1=messageinboxdata.callId;
            NSString *str2=[NSString stringWithFormat:@"%@",[notification.object objectAtIndex:1]];
            
            if ([str1 isEqualToString:str2])
            {
                NSIndexPath *myIP = [NSIndexPath indexPathForRow:rowindex inSection:0] ;
                cell = [self.tableView cellForRowAtIndexPath:myIP];
                if ([[notification.object objectAtIndex:0] floatValue]<1.000000)
                {
                    cell.playButton.backgroundColor=[UIColor grayColor];
                    cell.playButton.userInteractionEnabled=NO;
                    [cell.playButton setImage:[UIImage imageNamed:@""]
                                     forState:UIControlStateNormal];
                    cell.circularProgressView.progress=[[notification.object objectAtIndex:0] floatValue];
                    cell.circularProgressView.progressTintColor=kNavBackgroundColor;
                    [cell.activityView stopAnimating];
                    cell.activityView.hidden=YES;
                    
                }
                else
                {
                    cell.circularProgressView.progress=1.0;
                    cell.playButton.userInteractionEnabled=YES;
                    cell.playButton.backgroundColor=kNavBackgroundColor;
                    [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                                     forState:UIControlStateNormal];
                    cell.circularProgressView.progressTintColor=kNavBackgroundColor;
                    
                }
                
            }
        }
    }
    else  if ([[notification.object objectAtIndex:2] isEqualToString:@"AnnouncementInbox"])
    {
        if(announcementArray.count>0)
        {
            announcementinboxdata=[announcementArray objectAtIndex:rowindex];
            
            NSString *str1=announcementinboxdata.callId;
            NSString *str2=[NSString stringWithFormat:@"%@",[notification.object objectAtIndex:1]];
            
            if ([str1 isEqualToString:str2])            {
                NSIndexPath *myIP = [NSIndexPath indexPathForRow:rowindex inSection:0] ;
                cell = [self.tableView cellForRowAtIndexPath:myIP];
                if ([[notification.object objectAtIndex:0] floatValue]<1.000000)
                {
                    cell.playButton.backgroundColor=[UIColor grayColor];
                    cell.playButton.userInteractionEnabled=NO;
                    [cell.playButton setImage:[UIImage imageNamed:@""]
                                     forState:UIControlStateNormal];
                    cell.circularProgressView.progress=[[notification.object objectAtIndex:0] floatValue];
                    cell.circularProgressView.progressTintColor=kNavBackgroundColor;
                    [cell.activityView stopAnimating];
                    cell.activityView.hidden=YES;
                    
                }
                else
                {
                    cell.circularProgressView.progress=1.0;
                    cell.playButton.userInteractionEnabled=YES;
                    cell.playButton.backgroundColor=kNavBackgroundColor;
                    [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                                     forState:UIControlStateNormal];
                    cell.circularProgressView.progressTintColor=kNavBackgroundColor;
                    
                }
                
            }
        }
    }
}

#pragma mark AudioPlayer methods
- (IBAction)playAudioPressed:(id)playButton
{
    UIImage *downloadpng = [UIImage imageNamed:@"download"];
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:rowindex inSection:0] ;
    cell = [self.tableView cellForRowAtIndexPath:myIP];
    
    if ([[playButton currentImage] isEqual:downloadpng])
    {
        if (segCtl.tag==1 || segCtl.tag==3)
        {
            if (self.tableView.tag==1 || self.tableView.tag==7)
            {
                [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxInbox" object:messageinboxdata];
            }
            else if (self.tableView.tag==2 || self.tableView.tag==8)
            {
                [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxSaved" object:messagesaveddata];
            }
            else if(self.tableView.tag==3 || self.tableView.tag==9)
            {
                [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"EarboxSent" object:messagesentdata];
            }
        }
        else if(segCtl.tag==2)
        {
            if (self.tableView.tag==4)
            {
                [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementInbox" object:announcementinboxdata];
            }
            else  if (self.tableView.tag==5)
            {
                [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementSaved" object:announcementsaveddata];
            }
            else if (self.tableView.tag==6)
            {
                [[EarboxDataStore sharedStore]audioDownloadRetryCelltapped:@"AnnouncementSent" object:announcementsentdata];
            }
        }
        [cell.playButton setImage:[UIImage imageNamed:@""]
                         forState:UIControlStateNormal];
        cell.playButton.userInteractionEnabled=NO;
        cell.activityView.hidden=NO;
        [cell.activityView startAnimating];
    }
    else
    {
        if (self.audioPlayer.duration<1.0)
        {
            NSError *error;
            
            self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[[AudioManager getsharedInstance]getEarboxAudioPath:audioString] error:&error];
            self.audioPlayer.delegate=self;
            if (error)
            {
                NSLog(@"Earbox error in audioPlayer: %@",
                      [error localizedDescription]);
            }
            if ([self.audioPlayer duration] >1.0)
            {
                cell.duration.text = audiolength;
                
                cell.playButton.userInteractionEnabled=YES;
                cell.playButton.backgroundColor=kNavBackgroundColor;
                [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                                 forState:UIControlStateNormal];
                NSLog(@"Earbox play button pressed --This %@ audio file found in Database play button enabled",audiologString);
            }
            else
            {
                cell.playButton.backgroundColor=[UIColor grayColor];
                cell.circularProgressView.trackTintColor=[UIColor whiteColor];
                [cell.playButton setImage:[UIImage imageNamed:@"download"]
                                 forState:UIControlStateNormal];
                NSLog(@"Earbox play button pressed --This %@ audio file not found in Database play button disabled",audiologString);
            }
        }
        [self.timer invalidate];
        //play audio for the first time or if pause was pressed
        if (!self.isPaused) {
            
            [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_pause"]
                             forState:UIControlStateNormal];
            // Set a timer which keep getting the current music time and update the UISlider in 1 sec interval
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
            // Set the maximum value of the UISlider
            cell.currentTimeSlider.maximumValue = self.audioPlayer.duration;
            // Set the valueChanged target
            [cell.currentTimeSlider addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
            
            // Play the audio
            [self.audioPlayer prepareToPlay];
            [self.audioPlayer play];
            self.isPaused = TRUE;
            [self setupSliderTap];
            
        } else {
            //player is paused and Button is pressed again
            [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                             forState:UIControlStateNormal];
            
            [self.audioPlayer pause];
            self.isPaused = FALSE;
        }
        
    }
}

- (void)updateSlider {
    if (self.audioPlayer.playing)
    {
        // Update the slider about the music time
        cell.currentTimeSlider.value = self.audioPlayer.currentTime;
        cell.timeElapsed.text = [NSString stringWithFormat:@"%@",
                                 [self timeFormat:[self.audioPlayer currentTime]]];
        
        cell.duration.text = [NSString stringWithFormat:@"%@",
                              [self timeFormat:[self.audioPlayer duration] - [self.audioPlayer currentTime]]];
    }
}

- (IBAction)sliderChanged:(UISlider *)sender {
    // Fast skip the music when user scroll the UISlider
    if (self.audioPlayer.playing)
    {
        [self.audioPlayer stop];
        [self.audioPlayer setCurrentTime:cell.currentTimeSlider.value];
        [self.audioPlayer prepareToPlay];
        [self.audioPlayer play];
        [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_pause"]
                         forState:UIControlStateNormal];
    }
}


-(NSString*)timeFormat:(float)value{
    
    float minutes = floor(lroundf(value)/60);
    float seconds = lroundf(value) - (minutes * 60);
    
    long roundedSeconds = lroundf(seconds);
    long roundedMinutes = lroundf(minutes);
    
    NSString *time = [[NSString alloc]
                      initWithFormat:@"%ld:%02ld",
                      roundedMinutes, roundedSeconds];
    return time;
}

- (void)setupSliderTap
{
    UITapGestureRecognizer *gesture;
    
    if(cell.currentTimeSlider == nil)
        return;
    
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSliderTap:)];
    [cell.currentTimeSlider addGestureRecognizer:gesture];
}

- (void)handleSliderTap:(UIGestureRecognizer *)gesture
{
    CGPoint point;
    CGFloat ratio;
    CGFloat delta;
    CGFloat value;
    CGFloat thumbWidth;
    BOOL isPlaying;
    if (self.audioPlayer.playing)
    {
        // tap on thumb, let slider deal with it
        if (cell.currentTimeSlider.highlighted)
            return;
        
        isPlaying = (self.audioPlayer.rate > 0);
        CGRect trackRect = [cell.currentTimeSlider trackRectForBounds:cell.currentTimeSlider.bounds];
        CGRect thumbRect = [cell.currentTimeSlider thumbRectForBounds:cell.currentTimeSlider.bounds trackRect:trackRect value:0];
        CGSize thumbSize = thumbRect.size;
        thumbWidth = thumbSize.width;
        point = [gesture locationInView: cell.currentTimeSlider];
        if(point.x < thumbWidth/2)
        {
            ratio = 0;
        }
        else if(point.x > cell.currentTimeSlider.bounds.size.width - thumbWidth/2)
        {
            ratio = 1;
        }
        else
        {
            ratio = (point.x - thumbWidth/2) / (cell.currentTimeSlider.bounds.size.width - thumbWidth);
        }
        delta = ratio * (cell.currentTimeSlider.maximumValue - cell.currentTimeSlider.minimumValue);
        value =cell.currentTimeSlider.minimumValue + delta;
        [cell.currentTimeSlider setValue:value animated:YES];
        [self updatePlayer:isPlaying];
    }
}

- (void)updatePlayer:(BOOL)playIfNeeded
{
    [self.audioPlayer stop];
    [self.audioPlayer setCurrentTime:cell.currentTimeSlider.value];
    [self.audioPlayer prepareToPlay];
    [self.audioPlayer play];
    [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_pause"]
                     forState:UIControlStateNormal];
}

-(void)audioPlayerStop
{
    if ([self.audioPlayer isPlaying])
    {
        [self.audioPlayer pause];
        [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                         forState:UIControlStateNormal];
        self.isPaused = FALSE;
    }
    self.audioPlayer = nil;
    [[AudioManager getsharedInstance]audioplayerstop];
    
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [self.tableView beginUpdates];
    if (segCtl.tag==1 || segCtl.tag==3)
    {
        if (self.tableView.tag==1 || self.tableView.tag==7)
        {
            messageinboxdata=[messageArray objectAtIndex:rowindex];
            cell.duration.text=[NSDateClass lengthformate:messageinboxdata.duration];
            if ([messageinboxdata.status isEqualToString:@"new"])
            {
                [[StateMachineHandler sharedManager]sendMsgActionToNative:1 txnid:[NSString stringWithFormat:@"%@",messageinboxdata.callId]];
                messageinboxdata = [[[EarboxDataStore sharedStore] updateArtist:@"EarboxInbox" callId:messageinboxdata.callId] objectAtIndex:0];
                messageinboxdata.status = @"HEARD";
                NSError *error=nil;
                [[EarboxDataStore sharedStore] save:error];
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                messageArray=[messageArray sortedArrayUsingDescriptors:@[descriptor]];
                int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
                if (messagecount==0)
                {
                    messagecount=0;
                }
                else
                {
                    messagecount--;
                }
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagecount"];
                int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
        }
        else if (self.tableView.tag==2 || self.tableView.tag==8)
        {
            messagesaveddata=[savedmessagesArray objectAtIndex:rowindex];
            cell.duration.text=[NSDateClass lengthformate:messagesaveddata.duration];
            if ([messagesaveddata.status isEqualToString:@"new"])
            {
                [[StateMachineHandler sharedManager]sendMsgActionToNative:1 txnid:[NSString stringWithFormat:@"%@",messagesaveddata.callId]];
                messagesaveddata = [[[EarboxDataStore sharedStore] updateArtist:@"EarboxSaved" callId:messagesaveddata.callId] objectAtIndex:0];
                messagesaveddata.status = @"HEARD";
                NSError *error;
                [[EarboxDataStore sharedStore] save:error];
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                savedmessagesArray=[savedmessagesArray sortedArrayUsingDescriptors:@[descriptor]];
                int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
                if (messagecount==0)
                {
                    messagecount=0;
                }
                else
                {
                    messagecount--;
                }
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagecount"];
                int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
        }
        else if (self.tableView.tag==3 || self.tableView.tag==9)
        {
            messagesentdata=[sentmessageArray objectAtIndex:rowindex];
            cell.duration.text=[NSDateClass lengthformate:messagesentdata.duration];
        }
    }
    else
    {
        if (self.tableView.tag==4)
        {
            announcementinboxdata=[announcementArray objectAtIndex:rowindex];
            cell.duration.text=[NSDateClass lengthformate:announcementinboxdata.duration];
            if ([announcementinboxdata.status isEqualToString:@"new"])
            {
                [[StateMachineHandler sharedManager]sendMsgActionToNative:1 txnid:[NSString stringWithFormat:@"%@",announcementinboxdata.callId]];
                announcementinboxdata = [[[EarboxDataStore sharedStore] updateArtist:@"AnnouncementInbox" callId:announcementinboxdata.callId] objectAtIndex:0];
                announcementinboxdata.status = @"HEARD";
                NSError *error;
                [[EarboxDataStore sharedStore] save:error];
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                announcementArray=[announcementArray sortedArrayUsingDescriptors:@[descriptor]];
                int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementcount"] intValue];
                if (announcementcount==0)
                {
                    announcementcount=0;
                }
                else
                {
                    announcementcount--;
                }
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:announcementcount]
                                                         forKey:@"announcementcount"];
                int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
        }
        else if (self.tableView.tag==5)
        {
            announcementsaveddata=[savedannouncementArray objectAtIndex:rowindex];
            cell.duration.text=[NSDateClass lengthformate:announcementsaveddata.duration];
            if ([announcementsaveddata.status isEqualToString:@"new"])
            {
                [[StateMachineHandler sharedManager]sendMsgActionToNative:1 txnid:[NSString stringWithFormat:@"%@",announcementsaveddata.callId]];
                announcementsaveddata = [[[EarboxDataStore sharedStore] updateArtist:@"AnnouncementSaved" callId:announcementsaveddata.callId] objectAtIndex:0];
                announcementsaveddata.status = @"HEARD";
                NSError *error;
                [[EarboxDataStore sharedStore] save:error];
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"status" ascending:NO];
                savedannouncementArray=[savedannouncementArray sortedArrayUsingDescriptors:@[descriptor]];
                int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementcount"] intValue];
                if (announcementcount==0)
                {
                    announcementcount=0;
                }
                else
                {
                    announcementcount--;
                }
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:announcementcount]
                                                         forKey:@"announcementcount"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        else if (self.tableView.tag==6)
        {
            announcementsentdata=[sentannouncementArray objectAtIndex:rowindex];
            cell.duration.text=[NSDateClass lengthformate:announcementsentdata.duration];
        }
    }
    cell.currentTimeSlider.value = 00.00;
    cell.timeElapsed.text = @"00:00";
    [cell.playButton setImage:[UIImage imageNamed:@"audioplayer_play"]
                     forState:UIControlStateNormal];
    
    [self.tableView endUpdates];
    self.isPaused = FALSE;
}



-(void)updatetimer_EarBox
{
    if (!isReplying)
    {
        int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
        int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementcount"] intValue];
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount+announcementcount] forKey:@"badgecount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self deleteExpiredMsgs];
        
    }
    
}

-(void)deleteExpiredMsgs
{
    [self getFreshInboxList:@"EarboxInbox" calltype:4];
    for (int i=0; i<[freshInboxList count]; i++)
    {
        messageinboxdata=[freshInboxList objectAtIndex:i];
        if ([[NSDateClass ttlformate:messageinboxdata.ttl] isEqualToString:@"0"])
        {
            [[StateMachineHandler sharedManager]sendMsgActionToNative:2 txnid:[NSString stringWithFormat:@"%@", messageinboxdata.callId]];
            [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@", messageinboxdata.callId]];
            
            if ([messageinboxdata.status isEqualToString:@"new"])
            {
                int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
                if (messagecount==0)
                {
                    messagecount=0;
                }
                else
                {
                    messagecount--;
                }
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagecount"];
                int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            [[EarboxDataStore sharedStore] deleteEarboxInbox:messageinboxdata];
        }
    }
    
    [self getannouncementinboxdata:@"AnnouncementInbox" calltype:0];
    for (int i=0; i<[announcementArray count]; i++)
    {
        announcementinboxdata=[announcementArray objectAtIndex:i];
        
        if ([[NSDateClass ttlformate:announcementinboxdata.ttl] isEqualToString:@"0"])
        {
            [[StateMachineHandler sharedManager]sendMsgActionToNative:2 txnid:[NSString stringWithFormat:@"%@",announcementinboxdata.callId]];
            [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@",announcementinboxdata.callId]];
            
            if ([announcementinboxdata.status isEqualToString:@"new"])
            {
                int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementcount"] intValue];
                if (announcementcount==0)
                {
                    announcementcount=0;
                }
                else
                {
                    announcementcount--;
                }
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:announcementcount]
                                                         forKey:@"announcementcount"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            [[EarboxDataStore sharedStore] deleteAnnouncementInbox:announcementinboxdata];
            
            
        }
    }
    
    [self getFreshInboxList:@"EarboxInbox" calltype:17];
    for (int i=0; i<[freshInboxList count]; i++)
    {
        messageinboxdata=[freshInboxList objectAtIndex:i];
        if ([[NSDateClass ttlformate:messageinboxdata.ttl] isEqualToString:@"0"])
        {
            [[StateMachineHandler sharedManager]sendMsgActionToNative:2 txnid:[NSString stringWithFormat:@"%@", messageinboxdata.callId]];
            [[AudioManager getsharedInstance]deleteEarboxAudioPath:[NSString stringWithFormat:@"%@", messageinboxdata.callId]];
            
            if ([messageinboxdata.status isEqualToString:@"new"])
            {
                int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
                if (messagecount==0)
                {
                    messagecount=0;
                }
                else
                {
                    messagecount--;
                }
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagecount"];
                int badgecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]intValue];
                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:badgecount-1] forKey:@"badgecount"];
                [[UIApplication sharedApplication]setApplicationIconBadgeNumber:badgecount-1];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            [[EarboxDataStore sharedStore] deleteEarboxInbox:messageinboxdata];
        }
    }
}

-(void)tableViewUpdate
{
    if (self.audioPlayer.isPlaying)
    {
        
    }
    else
    {
        [self.tableView reloadData];
        
        NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
        if (selectedRows.count==0 )
        {
            
            [self deleteExpiredMsgs];
            
        }
        if (self.tableView.tag==1)
        {
            [self getmessageinboxdata:@"EarboxInbox" calltype:4];
            
        }
        
        if (self.tableView.tag==2)
        {
            [self getmessagesaveddata:@"EarboxSaved" calltype:4];
            
        }
        if (self.tableView.tag==3)
        {
            
            [self getmessagesentdata:@"EarboxSent" calltype:4];
            
        }
        if (self.tableView.tag==4)
        {
            
            [self getannouncementinboxdata:@"AnnouncementInbox" calltype:0];
            
        }
        if (self.tableView.tag==5)
        {
            
            [self getannouncementsaveddata:@"AnnouncementSaved" calltype:0];
            
        }
        if (self.tableView.tag==6)
        {
            
            [self getannouncementsentdata:@"AnnouncementSent" calltype:0];
            
        }
        if (self.tableView.tag==7)
        {
            [self getmessageinboxdata:@"EarboxInbox" calltype:17];
        }
        if (self.tableView.tag==8)
        {
            
            [self getmessagesaveddata:@"EarboxSaved" calltype:17];
        }
        if (self.tableView.tag==9)
        {
            
            [self getmessagesentdata:@"EarboxSent" calltype:17];
        }
        
        [self.tableView reloadData];
    }
}

- (void)reloadTableView
{
    NSArray *indexPaths = [self.tableView indexPathsForSelectedRows];
    
    [self.tableView reloadData];
    for (NSIndexPath *path in indexPaths) {
        [self.tableView selectRowAtIndexPath:path animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
}

#pragma mark - coveragejson(lib) method
-(void)coveragejson:(NSNotification *)notification
{
    self.tableView.userInteractionEnabled=YES;
    connectionLabel.hidden=YES;
    segCtl.userInteractionEnabled=YES;
    messagesegmentControl.userInteractionEnabled=YES;
    self.navigationItem.rightBarButtonItem.enabled=YES;
    speakerButton.enabled=YES;
    connectionAvailable=YES;
    
    //Coverage jsonstring getting from ntive c2.
    
    NSArray *coverageArray=[notification.object objectForKey:@"coverage"];
    
    for (int i=0; i<[coverageArray count]; i++)
    {
        // getting all covarageTagOutNames from jsonstring
        NSString *ct = [NSString stringWithFormat:@"%@",[[coverageArray objectAtIndex:i]objectForKey:@"ct"]];
        NSString *name=[[[coverageArray objectAtIndex:i]objectForKey:@"name"] componentsSeparatedByString:@"_"][1];
        
        if([ct isEqualToString:@"3"])
        {
            NSArray *tempArray = [name componentsSeparatedByString:@"|"];
            name = [tempArray objectAtIndex:0];
        }
        NSString *status=[[[coverageArray objectAtIndex:i]objectForKey:@"status"] componentsSeparatedByString:@"_"][0] ;
        NSArray<NSIndexPath *> *selectedRows = [self.tableView indexPathsForSelectedRows];
        if (selectedRows.count==0 )
        {
            NSArray*as =[statusArray valueForKey:@"Name"];
            if ([as containsObject:name])
            {
                NSUInteger index = [as indexOfObject:name];
                NSDictionary *replacedic=[NSDictionary dictionaryWithObjectsAndKeys:name,@"Name",status,@"Status", nil];
                [statusArray replaceObjectAtIndex:index withObject:replacedic];
                
                [self tableViewUpdate];
            }
        }
        
    }
    
}

-(void)coverageRequestToServertask:(NSNotification *)notification
{
    // Network messages getting from native c2 and updating to UI
    [self connectionRequest];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"texttime"])
    {
        if ([notification.object isEqualToString:@"server connection lost..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"server connection lost..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"could not start a connection..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"could not start a connection..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not logon to store..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not logon to store..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not connect to Proxy server..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not connect to Proxy server..."],staticerrorcode];
        }
        else
        {
            //Second time login ->please wait
            connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
        }
        
    }
    else
    {
        //First time login ,connecting to server->fetching emp list
        connectionLabel.text=notification.object;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"texttime"];
    }
    
}

-(void)tgsAlarmerror:(NSString *)tgsLoginerror
{
    staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:@"EarboxVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"TgsLogin"],[[DiagnosticTool sharedManager]generateErrorCode:tgsLoginerror]];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        [HttpHandler alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"TgsLogin"] severity:@"1" alarmdesc:staticerrorcode];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
    }];
}

-(void)connectionRequest
{
    connectionAvailable=NO;
    self.tableView.userInteractionEnabled=NO;
    connectionLabel.hidden=NO;
    segCtl.userInteractionEnabled=NO;
    messagesegmentControl.userInteractionEnabled=NO;
    self.navigationItem.rightBarButtonItem.enabled=NO;
    speakerButton.enabled=NO;
}


-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"EarboxViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [ self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void)reOpenClosedSocketsNotifiacation
{
    connectionLabel.hidden=NO;
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"BroadcastViewController"])
    {
        
    }
    else
    {
        [[StateMachineHandler sharedManager] reOpenClosedSockets];
        [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
    }
    
    
}

-(void)enterLockNotification
{
    [self.tableView reloadData];
    [self audioPlayerStop];
    if (isReplying)
    {
        [self replybuttonUpPressed:nil];
    }
}

-(void)requestUndeliveredMsgs
{
    [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
    //requesting for undelivered messages and announcements......
    companyId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"companyId"]intValue];
    
    NSOperationQueue *myQueuemessage = [[NSOperationQueue alloc] init];
    [myQueuemessage addOperationWithBlock:^{
        
        NSDictionary *twundeliveredMsgListdic= [HttpHandler earboxundeliveredlistVeb:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId storeId:storeId twreqFor:@"message" userScreen:@"EarboxMessageInboxVC"];
        NSDictionary *twundeliveredAnccListdic= [HttpHandler earboxundeliveredlistVeb:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId storeId:storeId twreqFor:@"announcement" userScreen:@"EarboxAnnouncementInboxVC"];
        NSDictionary *twundeliveredGroupListdic= [HttpHandler earboxundeliveredlistVeb:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId storeId:storeId twreqFor:@"groupmessage" userScreen:@"EarboxGroupInboxVC"];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (![[[twundeliveredMsgListdic objectForKey:@"twResponseHeader" ] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]] || ![[[twundeliveredGroupListdic objectForKey:@"twResponseHeader" ] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]] || ![[[twundeliveredAnccListdic objectForKey:@"twResponseHeader" ] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
            {
                
                if ([[twundeliveredMsgListdic objectForKey:@"earbox"] isKindOfClass:[NSNull class]] )
                    
                {
                    [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredfailedEarbox" forKey:@"undeliveredfailedEarbox"];
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                }
                else
                {
                    if ([[[twundeliveredMsgListdic objectForKey:@"earbox"] objectForKey:@"message"] isKindOfClass:[NSNull class]] )
                        
                    {
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    else
                    {
                        NSDictionary *message=[[twundeliveredMsgListdic objectForKey:@"earbox"]objectForKey:@"message"];
                        
                        for (int i=0; i<message.count; i++)
                        {
                            messageinbox=[message objectForKey:@"inbox"];
                            
                            messagesent=[message  objectForKey:@"sent"];
                            
                            messagesaved=[message objectForKey:@"saved"];
                            
                        }
                        
                        if (messageinbox.count>0)
                        {
                            [self insertmessageinboxdata:messageinbox];
                            
                            NSInteger messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
                            
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:messagecount+[messageinbox count]] forKey:@"messagecount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        if (messagesaved.count>0)
                        {
                            [self insertmessagesaveddata:messagesaved];
                            
                            NSInteger messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
                            
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:messagecount+[messagesaved count]] forKey:@"messagecount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        if (messagesent.count>0)
                        {
                            [self insertmessagesentdata:messagesent];
                        }
                    }
                    
                    if ([[[twundeliveredAnccListdic objectForKey:@"earbox"] objectForKey:@"announcement"] isKindOfClass:[NSNull class]] )
                        
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredfailedEarbox" forKey:@"undeliveredfailedEarbox"];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    else
                    {
                        NSDictionary *announcement=[[twundeliveredAnccListdic objectForKey:@"earbox"] objectForKey:@"announcement"];
                        
                        for (int i=0; i<announcement.count; i++)
                        {
                            announcementinbox=[announcement objectForKey:@"inbox"];
                            
                            announcementsent=[announcement  objectForKey:@"sent"];
                            
                            announcementsaved=[announcement  objectForKey:@"saved"];
                            
                        }
                        if (announcementinbox.count>0)
                        {
                            [self insertannouncementinboxdata:announcementinbox];
                            int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementcount"] intValue];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:announcementcount+[announcementinbox count]] forKey:@"announcementcount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        if (announcementsaved.count>0)
                        {
                            [self insertannouncementsaveddata:announcementsaved];
                            int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementcount"] intValue];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:announcementcount+[announcementsaved count]] forKey:@"announcementcount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        if (announcementsent.count>0)
                        {
                            [self insertannouncementsentdata:announcementsent];
                        }
                    }
                    if ([[[twundeliveredGroupListdic objectForKey:@"earbox"]objectForKey:@"groupMessage"] isKindOfClass:[NSNull class]] )
                        
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredfailedEarbox" forKey:@"undeliveredfailedEarbox"];
                        [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    }
                    else
                    {
                        NSDictionary *group=[[twundeliveredGroupListdic objectForKey:@"earbox"] objectForKey:@"groupMessage"];
                        
                        for (int i=0; i<group.count; i++)
                        {
                            groupinbox=[group objectForKey:@"inbox"];
                            
                            groupsent=[group  objectForKey:@"sent"];
                            
                            groupsaved=[group  objectForKey:@"saved"];
                            
                        }
                        if (groupinbox.count>0)
                        {
                            [self insertmessageinboxdata:groupinbox];
                            int groupcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:groupcount+[groupinbox count]] forKey:@"messagecount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        if (groupsaved.count>0)
                        {
                            [self insertmessagesaveddata:groupsaved];
                            int groupcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:groupcount+[groupsaved count]] forKey:@"messagecount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        if (groupsent.count>0)
                        {
                            [self insertmessagesentdata:groupsent];
                        }
                    }
                }
                [self employeelistdata];
                [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                
            }
        }];
    }];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coveragejson" object:nil];
}

@end
