//
//  BroadCostViewController.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "BroadcastViewController.h"
#import "StateMachineHandler.h"
#import "ViewController.h"
#import "UIViewController.h"

@interface BroadcastViewController ()


@end

@implementation BroadcastViewController

-(void)viewWillDisappear:(BOOL)animated
{
     NSLog(@"Broadcastview viewWillDisappear");
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        
            // Navigation button was pressed. Do some stuff
            [[StateMachineHandler sharedManager]sendPlaybufferenable:NO];
            [[StateMachineHandler sharedManager] enableBroadcastScreen:NO];
            [[StateMachineHandler sharedManager]sendCancelPlayoutToNative:YES];
    }
   
    
    [super viewWillDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"Broadcastview viewDidAppear");
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterLockNotification) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
        // Navigation button was pressed. Do some stuff
        [[StateMachineHandler sharedManager]sendPlaybufferenable:YES];
        [[StateMachineHandler sharedManager] enableBroadcastScreen:YES];
        [[StateMachineHandler sharedManager]sendCancelPlayoutToNative:NO];
   
    [super viewDidAppear:animated];
}

-(void)enterLockNotification
{
    [[StateMachineHandler sharedManager] doQueryButtonAction:NO];
    [[StateMachineHandler sharedManager]enterLockScreen];
}

-(void)coverageRequestToServertask:(NSNotification *)notification
{
}

- (void)viewDidLoad {
    NSLog(@"Broadcastview viewDidLoad");
    self.navigationItem.title=[[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coverageRequestToServertask:)
                                                 name:@"coverageRequestToServertask"
                                               object:nil];
    [broadCastButton addTarget:self action:@selector(buttonDownPressed:) forControlEvents: UIControlEventTouchDown];
    [broadCastButton addTarget:self action:@selector(buttonUpPressed:) forControlEvents: UIControlEventTouchUpInside];
    [broadCastButton addTarget:self action:@selector(buttonDragOutPressed:) forControlEvents: UIControlEventTouchDragOutside];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"BroadcastViewController"])
    {
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            
            [[StateMachineHandler sharedManager]sendPlaybufferenable:NO];
            [[StateMachineHandler sharedManager] enableBroadcastScreen:NO];
            [[StateMachineHandler sharedManager]sendCancelPlayoutToNative:YES];
            [[StateMachineHandler sharedManager] killTheatroNative];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [[NSUserDefaults standardUserDefaults]setObject:@"loggedoff" forKey:@"loggedoff"];
                [ self.navigationController popToRootViewControllerAnimated:YES];
            }];
        }];
        
       /* NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
           
            [[StateMachineHandler sharedManager] killTheatroNative];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
            }];
        }];*/
        
    }
}

-(void)reOpenClosedSocketsNotifiacation
{
    [[StateMachineHandler sharedManager]sendPlaybufferenable:YES];
    [[StateMachineHandler sharedManager] enableBroadcastScreen:YES];
    [[StateMachineHandler sharedManager]sendCancelPlayoutToNative:NO];
    
    [[StateMachineHandler sharedManager]reOpenClosedSockets];
    [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
}

- (void)buttonDownPressed:(UIButton *)sender
{
    NSLog(@"Broadcastview buttonDownPressed");
   self.navigationController.navigationBar.userInteractionEnabled = NO;
    [[StateMachineHandler sharedManager] doQueryButtonAction:YES];
}

- (void)buttonDragOutPressed:(UIButton *)sender
{
   NSLog(@"Broadcastview buttonDragOutPressed");
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [[StateMachineHandler sharedManager] doQueryButtonAction:NO];
}



- (void)buttonUpPressed:(UIButton *)sender
{
    NSLog(@"Broadcastview buttonUpPressed");
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [[StateMachineHandler sharedManager] doQueryButtonAction:NO];
}

-(void)dealloc
{
    
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}


@end
