//
//  NowViewController.h
//  Managers
//
//  Created by sandeepchalla on 9/4/15.
//  Copyright (c) 2015 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NowViewController : UIViewController<UIAlertViewDelegate>
{
    IBOutlet UILabel *announcementLabel;
     IBOutlet UILabel *lbl_Green;
}
@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

-(void)audioInterruptionEnd;
+(NowViewController *)getsharedInstance;

- (IBAction)sendFileTapped:(id)sender;
- (IBAction)deleteFileTapped:(id)sender;
- (IBAction)playTapped:(id)sender;

@end
