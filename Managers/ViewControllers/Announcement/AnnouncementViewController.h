//
//  AnnouncementViewController.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnnouncementViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UIAlertViewDelegate>
{
    IBOutlet UILabel *announcementLabel;
    IBOutlet UITabBarItem *tabbar;
     IBOutlet UILabel *lbl_Green;
}
@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

-(void)audioInterruptionEnd;
+(AnnouncementViewController *)getsharedInstance;

- (IBAction)sendFileTapped:(id)sender;
- (IBAction)deleteFileTapped:(id)sender;
- (IBAction)playTapped:(id)sender;

@end
