//
//  AnnouncementHomeViewController.h
//  Managers
//
//  Created by sandeepchalla on 9/30/15.
//  Copyright © 2015 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnnouncementHomeViewController : UIViewController<UITabBarDelegate,UIAlertViewDelegate>
{
    IBOutlet UITabBar *Announcementhometabbar;
     IBOutlet UILabel *lbl_Green;
     UILabel *connectionLabel;
    
}
@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@end
