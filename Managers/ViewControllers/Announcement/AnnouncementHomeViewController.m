//
//  AnnouncementHomeViewController.m
//  Managers
//
//  Created by sandeepchalla on 9/30/15.
//  Copyright © 2015 theatro. All rights reserved.
//

#import "AnnouncementHomeViewController.h"
#import "Constant.h"
#import "ViewController.h"
#import "UIViewController.h"
#import "LeftMenuViewController.h"
#import "StateMachineHandler.h"

@interface AnnouncementHomeViewController ()
{
    NSMutableArray *announcementtabwidth,*tabsArray;
    int tabitemwidth;
    NSDictionary *dictLang;
}

@end

@implementation AnnouncementHomeViewController



-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    NSLog(@"Announcement home viewWillAppear");
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        //[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"config_message"]]
        NSDictionary *uiElementsDic =[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"uiElements"]];
        //NSDictionary *uiElementsDic=[[NSUserDefaults standardUserDefaults]objectForKey:@"uiElements"];
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
        {
            
            if ([uiElementsDic count]!=0)
            {
                NSString *announcementhuddle,*announcemnt_24hrs;
                if(![uiElementsDic objectForKey:@"huddle"])
                {
                    announcementhuddle=[dictLang objectForKey:@"Huddle"];
                }
                else
                {
                    announcementhuddle=[uiElementsDic objectForKey:@"huddle"];
                }
                if(![uiElementsDic objectForKey:@"AnnouncementToday"])
                    
                {
                    
                    announcemnt_24hrs=[dictLang objectForKey:@"24Hrs"];
                    
                }
                
                else
                    
                {
                    
                    announcemnt_24hrs=[uiElementsDic objectForKey:@"AnnouncementToday"];
                    
                }
                
                
                
                tabsArray=[[NSMutableArray alloc]initWithObjects:[dictLang objectForKey:@"Now"],announcemnt_24hrs,[dictLang objectForKey:@"Week"],announcementhuddle,[dictLang objectForKey:@"Sales"], nil];
            }
            else
            {
                tabsArray=[[NSMutableArray alloc]initWithObjects:[dictLang objectForKey:@"Now"],[dictLang objectForKey:@"24Hrs"],[dictLang objectForKey:@"Week"],[dictLang objectForKey:@"Huddle"],[dictLang objectForKey:@"Sales"], nil];
            }
            
            if (screenSize.width ==414)
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"10",@"92",@"175",@"255",@"335", nil];
                tabitemwidth=70;
            }
            else if (screenSize.width ==375)
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"15",@"85",@"155",@"225",@"295", nil];
                tabitemwidth=60;
            }
            else
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"7",@"70",@"133",@"195",@"258", nil];
                tabitemwidth=55;
            }
            
        }
        else
        {
            if ([uiElementsDic count]!=0)
            {
                NSString *announcementhuddle,*announcemnt_24hrs;
                if(![uiElementsDic objectForKey:@"huddle"] )
                {
                    announcementhuddle=[dictLang objectForKey:@"Huddle"];
                }
                else
                {
                    announcementhuddle=[uiElementsDic objectForKey:@"huddle"];
                }
                if(![uiElementsDic objectForKey:@"AnnouncementToday"] )
                    
                {
                    
                    announcemnt_24hrs=[dictLang objectForKey:@"24Hrs"];
                    
                }
                
                else
                    
                {
                    
                    announcemnt_24hrs=[uiElementsDic objectForKey:@"AnnouncementToday"];
                    
                }
                
                
                
                tabsArray=[[NSMutableArray alloc]initWithObjects:[dictLang objectForKey:@"Now"],announcemnt_24hrs,[dictLang objectForKey:@"Week"],announcementhuddle, nil];
            }
            else
            {
                tabsArray=[[NSMutableArray alloc]initWithObjects:[dictLang objectForKey:@"Now"],[dictLang objectForKey:@"24Hrs"],[dictLang objectForKey:@"Week"],[dictLang objectForKey:@"Huddle"], nil];
            }
            if (screenSize.width ==414)
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"29",@"125",@"222",@"317", nil];
                tabitemwidth=70;
            }
            else if (screenSize.width ==375)
            {
                
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"20",@"108",@"196",@"284", nil];
                tabitemwidth=70;
                
                
            }
            else
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"20",@"96",@"170",@"245", nil];
                tabitemwidth=55;
            }
        }
        
        
    }
    else
    {
        //[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"config_message"]]
        NSDictionary *uiElementsDic =[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"uiElements"]];
        //NSDictionary *uiElementsDic=[[NSUserDefaults standardUserDefaults]objectForKey:@"uiElements"];
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
        {
            
            if ([uiElementsDic count]!=0)
            {
                NSString *announcementhuddle,*announcemnt_24hrs;
                if(![uiElementsDic objectForKey:@"huddle"])
                {
                    announcementhuddle=[dictLang objectForKey:@"Huddle"];
                }
                else
                {
                    announcementhuddle=[uiElementsDic objectForKey:@"huddle"];
                }
                if(![uiElementsDic objectForKey:@"AnnouncementToday"])
                    
                {
                    
                    announcemnt_24hrs=[dictLang objectForKey:@"24Hrs"];
                    
                }
                
                else
                    
                {
                    
                    announcemnt_24hrs=[uiElementsDic objectForKey:@"AnnouncementToday"];
                    
                }
                
                
                
                tabsArray=[[NSMutableArray alloc]initWithObjects:[dictLang objectForKey:@"Now"],announcemnt_24hrs,[dictLang objectForKey:@"Week"],announcementhuddle,[dictLang objectForKey:@"Sales"], nil];
            }
            else
            {
                tabsArray=[[NSMutableArray alloc]initWithObjects:[dictLang objectForKey:@"Now"],[dictLang objectForKey:@"24Hrs"],[dictLang objectForKey:@"Week"],[dictLang objectForKey:@"Huddle"],[dictLang objectForKey:@"Sales"], nil];
            }
            announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"130",@"240",@"350",@"470",@"590", nil];
            tabitemwidth=70;
            
            
        }
        else
        {
            if ([uiElementsDic count]!=0)
            {
                NSString *announcementhuddle,*announcemnt_24hrs;
                if(![uiElementsDic objectForKey:@"huddle"] )
                {
                    announcementhuddle=[dictLang objectForKey:@"Huddle"];
                }
                else
                {
                    announcementhuddle=[uiElementsDic objectForKey:@"huddle"];
                }
                if(![uiElementsDic objectForKey:@"AnnouncementToday"] )
                    
                {
                    
                    announcemnt_24hrs=[dictLang objectForKey:@"24Hrs"];
                    
                }
                
                else
                    
                {
                    
                    announcemnt_24hrs=[uiElementsDic objectForKey:@"AnnouncementToday"];
                    
                }
                
                
                
                tabsArray=[[NSMutableArray alloc]initWithObjects:[dictLang objectForKey:@"Now"],announcemnt_24hrs,[dictLang objectForKey:@"Week"],announcementhuddle, nil];
            }
            else
            {
                tabsArray=[[NSMutableArray alloc]initWithObjects:[dictLang objectForKey:@"Now"],[dictLang objectForKey:@"24Hrs"],[dictLang objectForKey:@"Week"],[dictLang objectForKey:@"Huddle"], nil];
            }
            announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"130",@"240",@"350",@"470", nil];
            tabitemwidth=70;
        }
        
        
    }
    
    
    for (int i=0;i<[announcementtabwidth count];i++)
    {
        UILabel *button=[[UILabel alloc]init];
        button.frame=CGRectMake([[announcementtabwidth objectAtIndex:i]intValue], 7, tabitemwidth, 35);
        button.backgroundColor=kNavBackgroundColor;
        button.layer.cornerRadius=6;
        button.font=[UIFont fontWithName:kFontName size:12];
        button.textColor=[UIColor whiteColor];
        button.textAlignment=NSTextAlignmentCenter;
        button.text=[tabsArray objectAtIndex:i];
        button.clipsToBounds=YES;
        [Announcementhometabbar addSubview:button];
    }
    self.recordPauseButton.userInteractionEnabled=NO;
    
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [Announcementhometabbar invalidateIntrinsicContentSize];
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = kNavBackgroundColor;
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
    titleLabel.text = [dictLang objectForKey:@"Announcements"];
    [titleLabel sizeToFit];
    UIView *navigationTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    [navigationTitleView addSubview:titleLabel];
    self.navigationItem.titleView=navigationTitleView;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"AnnouncementHomeViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [ self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    UITabBarController *tabController;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
    {
        tabController=[self.storyboard instantiateViewControllerWithIdentifier:@"AnnouncementSalesTabbar"];
        switch (item.tag) {
            case 0:
                [tabController setSelectedIndex:0];
                [self.navigationController pushViewController:tabController animated:YES];
                break;
            case 1:
                [tabController setSelectedIndex:1];
                [self.navigationController pushViewController:tabController animated:YES];
                break;
            case 2:
                [tabController setSelectedIndex:2];
                [self.navigationController pushViewController:tabController animated:YES];
                break;
            case 3:
                [tabController setSelectedIndex:3];
                [self.navigationController pushViewController:tabController animated:YES];
                break;
            case 4:
                [tabController setSelectedIndex:4];
                [self.navigationController pushViewController:tabController animated:YES];
                break;
        }
    }
    else
    {
        tabController=[self.storyboard instantiateViewControllerWithIdentifier:@"AnnouncementTabbar"];
        switch (item.tag) {
            case 0:
                [tabController setSelectedIndex:0];
                [self.navigationController pushViewController:tabController animated:YES];
                break;
            case 1:
                [tabController setSelectedIndex:1];
                [self.navigationController pushViewController:tabController animated:YES];
                break;
            case 2:
                [tabController setSelectedIndex:2];
                [self.navigationController pushViewController:tabController animated:YES];
                break;
            case 3:
                [tabController setSelectedIndex:3];
                [self.navigationController pushViewController:tabController animated:YES];
                break;
        }
    }
    
    
}

@end
