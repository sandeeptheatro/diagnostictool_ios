//
//  AnnouncementViewController.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.


#import "AnnouncementViewController.h"
#import "GroupsDetailViewController.h"
#import "AudioManager.h"
#import "Constant.h"
#import "Reachability.h"
#import "AlertView.h"
#import "HttpHandler.h"
#import "StateMachineHandler.h"
#import "UIViewController.h"
#import "Constant.h"
#import "ViewController.h"
#import "UIViewController.h"
#import "Earboxinboxmodel.h"
#import "MarqueeLabel.h"
#import "DiagnosticTool.h"


@interface AnnouncementViewController ()
{
    NSMutableArray *announcementtabwidth;
    int tabitemwidth;
    NSString *storeNameString;
    NSURL *announcementhuddleaudiourl;
    BOOL audioSendButton,huddletimecheck;
    UIView *alphaView;
    UIButton *timerokButton,*starttimeButton,*endtimeButton,*confrimButton,*deleteHuddleButton;
    NSString *huddlestarttimeString,*huddleendtimeString,*huddleTimeHour,*huddleTimeMin,*huddleTimeSec;
    NSArray *huddleminutesArray,*huddlehoursArray;
    UIPickerView *picker;
    UILabel *announcementnow,*announcement24hrs,*announcementweek,*announcementhuddle,*announcementsales;
    NSMutableDictionary *pns;
    BOOL audioInterruption;
    NSDictionary *dictLang;
}

@end

@implementation AnnouncementViewController
@synthesize deleteButton, playButton, recordPauseButton,sendButton;

static  AnnouncementViewController *sharedMgr = nil;

+(AnnouncementViewController *)getsharedInstance {
    if (sharedMgr == nil)
    {
        sharedMgr = [[AnnouncementViewController alloc] init];
    }
    return sharedMgr;
}


-(void)broadCostButtonAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - reOpenClosedSockets method
-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
     NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush isEqualToString:@"YES"])
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"StoreListViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        else
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"VPViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        
    }
    else
    {
    if (audioInterruption==YES)
    {
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
        recordPauseButton.userInteractionEnabled=YES;
        audioInterruption=NO;
    }
    [[StateMachineHandler sharedManager]reOpenClosedSockets];
        [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
    }
    
}




#pragma mark - view method

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)viewDidLoad {
    
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    
    [deleteButton setTitle:[dictLang objectForKey:@"Delete"] forState:UIControlStateNormal];
    [sendButton setTitle:[dictLang objectForKey:@"Send"] forState:UIControlStateNormal];
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    playButton.layer.cornerRadius=8;
    deleteButton.layer.cornerRadius=8;
    sendButton.layer.cornerRadius=8;
    [super viewDidLoad];
    [recordPauseButton addTarget:self action:@selector(buttondDownPressed:) forControlEvents: UIControlEventTouchDown];
    [recordPauseButton addTarget:self action:@selector(buttonUpPressed:) forControlEvents: UIControlEventTouchUpInside];
    [recordPauseButton addTarget:self action:@selector(buttonDragOutside:) forControlEvents: UIControlEventTouchDragOutside];
    [[AudioManager getsharedInstance] path];
    
    
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAnnouncementsNotification" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"audioNotification" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recievedAnnouncementsNotification" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAnnouncementsSingleTapNotification" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"huddleNotificationnew" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAnnouncementsfullrecordNotification" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIApplicationDidBecomeActiveNotification
                                                      object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"ForceLogOut"
                                                  object:nil];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        [[AudioManager getsharedInstance]audioplayerstop];
        [[AudioManager getsharedInstance]recordstop];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
        
    }];
    
    [super viewWillDisappear:animated];
}


-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    
    NSLog(@"Announcement chat viewWillAppear");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playAnnouncement:)
                                                 name:@"playAnnouncementsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendAnnouncement:)
                                                 name:@"audioNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedAnnouncement:)
                                                 name:@"recievedAnnouncementsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(huddlenotification:)
                                                 name:@"huddleNotificationnew"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(buttonSingleTap)
                                                 name:@"playAnnouncementsSingleTapNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recordfullnotification)
                                                 name:@"playAnnouncementsfullrecordNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
        {
            
            if (screenSize.width ==414)
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"10",@"92",@"175",@"255",@"335", nil];
                tabitemwidth=70;
            }
            else if (screenSize.width ==375)
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"15",@"85",@"155",@"225",@"295", nil];
                tabitemwidth=60;
            }
            else
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"7",@"70",@"133",@"195",@"258", nil];
                tabitemwidth=55;
            }
            announcementsales=[[UILabel alloc]init];
            announcementsales.frame=CGRectMake([[announcementtabwidth objectAtIndex:4]intValue], 7, tabitemwidth, 35);
            announcementsales.backgroundColor=kNavBackgroundColor;
            announcementsales.font=[UIFont fontWithName:kFontName size:12];
            announcementsales.layer.cornerRadius=6;
            announcementsales.textColor=[UIColor whiteColor];
            announcementsales.textAlignment=NSTextAlignmentCenter;
            announcementsales.text=[dictLang objectForKey:@"SALES"];
            announcementsales.clipsToBounds=YES;
            [self.tabBarController.tabBar addSubview:announcementsales];
            
        }
        else
        {
            if (screenSize.width ==414)
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"29",@"125",@"222",@"317", nil];
                tabitemwidth=70;
            }
            else if (screenSize.width ==375)
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"20",@"108",@"196",@"284", nil];
                tabitemwidth=70;
            }
            else
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"20",@"96",@"170",@"245", nil];
                tabitemwidth=55;
            }
        }
        
        
    }
    else
    {
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
        {
            
            announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"130",@"240",@"350",@"470",@"590", nil];
            tabitemwidth=70;
            announcementsales=[[UILabel alloc]init];
            announcementsales.frame=CGRectMake([[announcementtabwidth objectAtIndex:4]intValue], 7, tabitemwidth, 35);
            announcementsales.backgroundColor=kNavBackgroundColor;
            announcementsales.font=[UIFont fontWithName:kFontName size:12];
            announcementsales.layer.cornerRadius=6;
            announcementsales.textColor=[UIColor whiteColor];
            announcementsales.textAlignment=NSTextAlignmentCenter;
            announcementsales.text=[dictLang objectForKey:@"SALES"];
            announcementsales.clipsToBounds=YES;
            [self.tabBarController.tabBar addSubview:announcementsales];
            
        }
        else
        {
            announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"130",@"240",@"350",@"470", nil];
            tabitemwidth=70;
        }
        
        
    }
    
    announcementnow=[[UILabel alloc]init];
    announcementnow.frame=CGRectMake([[announcementtabwidth objectAtIndex:0] intValue], 7, tabitemwidth, 35);
    announcementnow.backgroundColor=kNavBackgroundColor;
    announcementnow.font=[UIFont fontWithName:kFontName size:12];
    announcementnow.layer.cornerRadius=6;
    announcementnow.textColor=[UIColor whiteColor];
    announcementnow.textAlignment=NSTextAlignmentCenter;
    announcementnow.text=[dictLang objectForKey:@"Now"];
    announcementnow.clipsToBounds=YES;
    [self.tabBarController.tabBar addSubview:announcementnow];
    
    
    announcement24hrs=[[UILabel alloc]init];
    announcement24hrs.frame=CGRectMake([[announcementtabwidth objectAtIndex:1]intValue], 7, tabitemwidth, 35);
    announcement24hrs.backgroundColor=kNavBackgroundColor;
    announcement24hrs.font=[UIFont fontWithName:kFontName size:12];
    announcement24hrs.layer.cornerRadius=6;
    announcement24hrs.textColor=[UIColor whiteColor];
    announcement24hrs.textAlignment=NSTextAlignmentCenter;
    announcement24hrs.text=[dictLang objectForKey:@"24Hrs"];
    announcement24hrs.clipsToBounds=YES;
    [self.tabBarController.tabBar addSubview:announcement24hrs];
    
    
    announcementweek=[[UILabel alloc]init];
    announcementweek.frame=CGRectMake([[announcementtabwidth objectAtIndex:2]intValue], 7, tabitemwidth, 35);
    announcementweek.backgroundColor=kNavBackgroundColor;
    announcementweek.font=[UIFont fontWithName:kFontName size:12];
    announcementweek.layer.cornerRadius=6;
    announcementweek.textColor=[UIColor whiteColor];
    announcementweek.textAlignment=NSTextAlignmentCenter;
    announcementweek.text=[dictLang objectForKey:@"Week"];
    announcementweek.clipsToBounds=YES;
    [self.tabBarController.tabBar addSubview:announcementweek];
    
    
    announcementhuddle=[[UILabel alloc]init];
    announcementhuddle.frame=CGRectMake([[announcementtabwidth objectAtIndex:3]intValue], 7, tabitemwidth, 35);
    announcementhuddle.backgroundColor=kTabbarColor;
    announcementhuddle.font=[UIFont fontWithName:kFontName size:12];
    announcementhuddle.layer.cornerRadius=6;
    announcementhuddle.textColor=[UIColor whiteColor];
    announcementhuddle.textAlignment=NSTextAlignmentCenter;
    announcementhuddle.text=[dictLang objectForKey:@"Huddle"];
    NSDictionary *uiElementsDic=[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"uiElements"]];
    if ([uiElementsDic count]!=0)
    {
        if(![uiElementsDic objectForKey:@"huddle"] )
        {
            announcementhuddle.text=[dictLang objectForKey:@"Huddle"];
        }
        else
        {
            announcementhuddle.text=[uiElementsDic objectForKey:@"huddle"];
        }
        if(![uiElementsDic objectForKey:@"AnnouncementToday"] )
            
        {
            
            announcement24hrs.text=[dictLang objectForKey:@"24Hrs"];
            
        }
        
        else
            
        {
            
            announcement24hrs.text=[uiElementsDic objectForKey:@"AnnouncementToday"];
            
        }
        
    }
    announcementhuddle.clipsToBounds=YES;
    [self.tabBarController.tabBar addSubview:announcementhuddle];
    
    
    [self announcementTitle];
    UILongPressGestureRecognizer *longRecog = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(huddleButtonLongPressed:)];
    
    [self.tabBarController.tabBar addGestureRecognizer:longRecog];
    
    [self huddlenotification];
    
    huddlehoursArray=[NSArray arrayWithObjects:@"00",@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",nil];
    huddleminutesArray=[NSArray arrayWithObjects:@"00",@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",@"31",@"32",@"33",@"34",@"35",@"36",@"37",@"38",@"39",@"40",@"41",@"42",@"43",@"44",@"45",@"46",@"47",@"48",@"49",@"50",@"51",@"52",@"53",@"54",@"55",@"56",@"57",@"58",@"59", nil];
    [super viewWillAppear:animated];
}


-(void)announcementTitle
{
    storeNameString=[[NSUserDefaults standardUserDefaults]objectForKey:@"storeNameString"];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = kNavBackgroundColor;
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
    titleLabel.text = [dictLang objectForKey:@"Announcement"];
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = kNavBackgroundColor;
    subTitleLabel.font = [UIFont systemFontOfSize:12];
    NSDictionary *uiElementsDic=[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"uiElements"]];
    if ([uiElementsDic count]!=0)
    {
        if(![uiElementsDic objectForKey:@"huddle"] )
        {
            subTitleLabel.text=[dictLang objectForKey:@"Huddle"];
        }
        else
        {
            if( [[uiElementsDic objectForKey:@"huddle"] caseInsensitiveCompare:@"ITK"] == NSOrderedSame )
            {
                subTitleLabel.text=[dictLang objectForKey:@"In The Know"]; //[[uiElementsDic objectForKey:@"huddle"]capitalizedString];
            }
            else
            {
                if(![uiElementsDic objectForKey:@"huddle"] )
                {
                subTitleLabel.text=[dictLang objectForKey:@"Huddle"];
                }
                else
                {
                    subTitleLabel.text=[uiElementsDic objectForKey:@"huddle"];
                }
            }
        }
        
    }
    
    [subTitleLabel sizeToFit];
    
    UIView *navigationTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [navigationTitleView addSubview:titleLabel];
    [navigationTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    CGRect titleframe = titleLabel.frame;
    if (widthDiff > 0) {
        
        titleframe.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(titleframe);
        
    }else{
        titleframe.size.width=self.view.frame.size.width-100;
        titleLabel.frame = CGRectIntegral(titleframe);
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
    self.parentViewController.navigationItem.titleView=navigationTitleView;
}


-(void)timerokButtonAction
{
    [picker removeFromSuperview];
    timerokButton.hidden=YES;
    
    if (huddletimecheck==YES)
    {
        huddlestarttimeString=[NSString stringWithFormat:@"%@:%@:%@",huddleTimeHour,huddleTimeMin,huddleTimeSec];
        [starttimeButton setTitle:huddlestarttimeString forState:UIControlStateNormal];
    }
    else
    {
        huddleendtimeString=[NSString stringWithFormat:@"%@:%@:%@",huddleTimeHour,huddleTimeMin,huddleTimeSec];
        [endtimeButton setTitle:huddleendtimeString forState:UIControlStateNormal];
    }
}

#pragma mark - huddle methods

-(void)huddleButtonLongPressed:(UILongPressGestureRecognizer *)sender
{
    if([announcementLabel.text isEqualToString:[dictLang objectForKey:@"Replaying..."]] || [announcementLabel.text isEqualToString:[dictLang objectForKey:@"Announcement not sent"] ]|| [announcementLabel.text isEqualToString:[dictLang objectForKey:@"Announcement Sent"]])
    {
        
    }
    else
    {
        if (sender.state == UIGestureRecognizerStateBegan) {
            
            /*NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
             [myQueue addOperationWithBlock:^{
             
             huddleListArray= [[HttpHandler huddleList:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:[loginDetails objectForKey:@"companyId"] storeId:[NSString stringWithFormat:@"%d",storeId]] objectForKey:@"huddleList"];
             
             [[NSOperationQueue mainQueue] addOperationWithBlock:^{
             
             [self huddleAction];
             }];
             
             
             }];*/
            
            self.navigationController.navigationBar.userInteractionEnabled = NO;
            alphaView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            alphaView.tag=200;
            alphaView.backgroundColor=[UIColor lightGrayColor];
            UIView  *huddleView=[[UIView alloc] initWithFrame:CGRectMake(10, (self.view.frame.size.height-400)/2, self.view.frame.size.width-20, 300)];
            huddleView.backgroundColor=kSettingsColor;
            huddleView.layer.cornerRadius=8;
            
            MarqueeLabel *huddleLabel = [[MarqueeLabel alloc] initWithFrame:CGRectMake((huddleView.frame.size.width/2)-140, 30, 280, 30) duration:8.0 andFadeLength:10.0f];
            [huddleLabel setTextAlignment:NSTextAlignmentCenter];
            [huddleLabel setBackgroundColor:[UIColor clearColor]];
            huddleLabel.adjustsFontSizeToFitWidth=NO;
            [huddleLabel setAnimationCurve:UIViewAnimationOptionCurveEaseIn];
            huddleLabel.marqueeType = MLContinuous;
            //MarqueeLabel *huddleLabel = [[MarqueeLabel alloc] initWithFrame:CGRectMake((huddleView.frame.size.width/2)-140, 20, 200, 30)];
            huddleLabel.font=[UIFont fontWithName:kFontName size:18];
            huddleLabel.textColor=kNavBackgroundColor;
            huddleLabel.textAlignment=NSTextAlignmentCenter;
            //huddleLabel.backgroundColor=[UIColor redColor];
            NSDictionary *uiElementsDic=[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"uiElements"]];
            if ([uiElementsDic count]!=0)
            {
                if(![uiElementsDic objectForKey:@"huddle"] )
                {
                    huddleLabel.text = [dictLang objectForKey:@"Select huddle time"];
                }
                else
                {
                    huddleLabel.text=[NSString stringWithFormat:@"%@ %@ %@",[dictLang objectForKey:@"Select"],[uiElementsDic objectForKey:@"huddle"],[dictLang objectForKey:@"time"]];
                }
                
            }
            else
            {
                huddleLabel.text = [dictLang objectForKey:@"Select huddle time"];
            }
            
            [huddleView addSubview:huddleLabel];
            
            timerokButton=[UIButton buttonWithType:UIButtonTypeCustom];
            timerokButton.frame=CGRectMake(self.view.frame.size.width-85, 5, 60, 30);
            [timerokButton addTarget:self action:@selector(timerokButtonAction) forControlEvents:UIControlEventTouchUpInside];
            timerokButton.titleLabel.font=[UIFont fontWithName:kFontName size:14];
            [timerokButton setTitle:[dictLang objectForKey:@"OK"] forState:UIControlStateNormal];
            timerokButton.backgroundColor=kNavBackgroundColor;
            timerokButton.layer.cornerRadius=7;
            timerokButton.hidden=YES;
            [huddleView addSubview:timerokButton];
            
            
            UILabel *starttimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 100, 150, 30)];
            starttimeLabel.textColor=[UIColor blackColor];
            starttimeLabel.layer.masksToBounds = YES;
            starttimeLabel.layer.cornerRadius=8;
            starttimeLabel.font=[UIFont fontWithName:kFontName size:18];
            starttimeLabel.text = [dictLang objectForKey:@"Start time"];
            [huddleView addSubview:starttimeLabel];
            
            starttimeButton=[UIButton buttonWithType:UIButtonTypeCustom];
            starttimeButton.frame=CGRectMake(self.view.frame.size.width-177, 100, 150, 30);
            [starttimeButton addTarget:self action:@selector(starttimeButtonAction) forControlEvents:UIControlEventTouchUpInside];
            [starttimeButton setTitle:[dictLang objectForKey:@"Start time"] forState:UIControlStateNormal];
            starttimeButton.backgroundColor=kNavBackgroundColor;
            starttimeButton.layer.cornerRadius=7;
            starttimeButton.userInteractionEnabled=YES;
            [huddleView addSubview:starttimeButton];
            
            NSString *htString=[[[NSUserDefaults standardUserDefaults] objectForKey:@"huddle"] objectForKey:@"ht"];
            [self timerChange:0 timer:[[htString componentsSeparatedByString:@":"][0]intValue]];
            [self timerChange:1 timer:[[htString componentsSeparatedByString:@":"][1]intValue]];
            [self timerChange:2 timer:0];
            huddlestarttimeString=[NSString stringWithFormat:@"%@:%@:%@",huddleTimeHour,huddleTimeMin,huddleTimeSec];
            [starttimeButton setTitle:huddlestarttimeString forState:UIControlStateNormal];
            
            confrimButton=[UIButton buttonWithType:UIButtonTypeCustom];
            confrimButton.frame=CGRectMake((huddleView.frame.size.width)/2-130, 170, 100, 30);
            [confrimButton addTarget:self action:@selector(huddleConfrimAction) forControlEvents:UIControlEventTouchUpInside];
            [confrimButton setTitle:[dictLang objectForKey:@"Confirm"] forState:UIControlStateNormal];
            confrimButton.backgroundColor=[UIColor grayColor];
            confrimButton.layer.cornerRadius=7;
            confrimButton.userInteractionEnabled=NO;
            [huddleView addSubview:confrimButton];
            UIButton *cancelButton=[UIButton buttonWithType:UIButtonTypeCustom];
            cancelButton.frame=CGRectMake((huddleView.frame.size.width)/2+30, 170, 100, 30);
            [cancelButton addTarget:self action:@selector(huddleCancelAction) forControlEvents:UIControlEventTouchUpInside];
            [cancelButton setTitle:[dictLang objectForKey:@"Cancel"] forState:UIControlStateNormal];
            cancelButton.backgroundColor=[UIColor redColor];
            cancelButton.layer.cornerRadius=7;
            [huddleView addSubview:cancelButton];
            deleteHuddleButton=[UIButton buttonWithType:UIButtonTypeCustom];
            deleteHuddleButton.frame=CGRectMake((huddleView.frame.size.width)/2-80, 230, 160, 30);
            [deleteHuddleButton addTarget:self action:@selector(huddleDeleteAction) forControlEvents:UIControlEventTouchUpInside];
            if ([uiElementsDic count]!=0)
            {
                if(![uiElementsDic objectForKey:@"huddle"])
                {
                    [deleteHuddleButton setTitle:[dictLang objectForKey:@"Delete Huddle"] forState:UIControlStateNormal];
                }
                else
                {
                    [deleteHuddleButton setTitle:[NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Delete"],[uiElementsDic objectForKey:@"huddle"]] forState:UIControlStateNormal];
                }
                
            }
            else
            {
                [deleteHuddleButton setTitle:[dictLang objectForKey:@"Delete Huddle"] forState:UIControlStateNormal];
            }
            
            deleteHuddleButton.backgroundColor=[UIColor grayColor];
            deleteHuddleButton.layer.cornerRadius=7;
            [huddleView addSubview:deleteHuddleButton];
            [alphaView addSubview:huddleView];
            [self.view addSubview:alphaView];
            [self huddleAction];
            
            
        }
    }
    
}

-(void)huddleAction
{
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:NO];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:NO];
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:NO];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
    {
        [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:NO];
    }
    announcementnow.backgroundColor=kGreyColor;
    announcementweek.backgroundColor=kGreyColor;
    announcement24hrs.backgroundColor=kGreyColor;
    announcementsales.backgroundColor=kGreyColor;
    NSString *htString=[[[NSUserDefaults standardUserDefaults] objectForKey:@"huddle"] objectForKey:@"ha"];
    if (![htString isEqualToString:@"true"])
    {
        deleteHuddleButton.userInteractionEnabled=NO;
        deleteHuddleButton.backgroundColor=[UIColor grayColor];
        
        recordPauseButton.userInteractionEnabled=YES;
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
        
    }
    else
    {
        deleteHuddleButton.userInteractionEnabled=YES;
        deleteHuddleButton.backgroundColor=kNavBackgroundColor;
        recordPauseButton.userInteractionEnabled=NO;
        
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_gray"] forState:UIControlStateNormal];
        
        deleteButton.backgroundColor=kGreyColor;
        playButton.backgroundColor=kGreyColor;
        sendButton.backgroundColor=kGreyColor;
        [playButton setEnabled:NO];
        [deleteButton setEnabled:NO];
        [sendButton setEnabled:NO];
    }
}

-(void)huddleConfrimAction
{
    [[StateMachineHandler sharedManager] sendHuddleInfoToNative:huddlestarttimeString huddleType:1];
    
    UIView *v = [self.view viewWithTag:200];
    [v removeFromSuperview];
    
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:YES];
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:YES];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
    {
        [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:YES];
    }
    announcementnow.backgroundColor=kNavBackgroundColor;
    announcementweek.backgroundColor=kNavBackgroundColor;
    announcement24hrs.backgroundColor=kNavBackgroundColor;
    announcementsales.backgroundColor=kNavBackgroundColor;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    
}

-(void)huddleCancelAction
{
    
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:YES];
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:YES];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
    {
        [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:YES];
    }
    announcementnow.backgroundColor=kNavBackgroundColor;
    announcementweek.backgroundColor=kNavBackgroundColor;
    announcement24hrs.backgroundColor=kNavBackgroundColor;
    announcementsales.backgroundColor=kNavBackgroundColor;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    UIView *v = [self.view viewWithTag:200];
    [v removeFromSuperview];
}


-(void)huddleDeleteAction
{
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:YES];
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:YES];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
    {
        [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:YES];
    }
    announcementnow.backgroundColor=kNavBackgroundColor;
    announcementweek.backgroundColor=kNavBackgroundColor;
    announcement24hrs.backgroundColor=kNavBackgroundColor;
    announcementsales.backgroundColor=kNavBackgroundColor;
    NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:@"false",@"ha" ,nil];
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:@"huddleha"];
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    //[self huddlenotification];
    [[StateMachineHandler sharedManager] sendHuddleInfoToNative:@"" huddleType:2];
    UIView *v = [self.view viewWithTag:200];
    [v removeFromSuperview];
}

-(void)huddlenotification
{
    NSDictionary *huddleha=[[NSUserDefaults standardUserDefaults]objectForKey:@"huddleha"];
    if ([[huddleha objectForKey:@"ha"] isEqualToString:@"false"])
    {
        announcementLabel.text=@"";
        deleteButton.backgroundColor=kGreyColor;
        sendButton.backgroundColor=kGreyColor;
        playButton.backgroundColor=kGreyColor;
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
        recordPauseButton.userInteractionEnabled=YES;
        /*if (playButton.isEnabled)
        {
            [playButton setEnabled:NO];
        }
        if (deleteButton.isEnabled)
        {
            [deleteButton setEnabled:NO];
        }
        if (sendButton.isEnabled)
        {
            [sendButton setEnabled:NO];
        }*/
        
        
    }
    else
    {
        announcementLabel.text=@"";
        deleteButton.backgroundColor=kGreyColor;
        sendButton.backgroundColor=kGreyColor;
        playButton.backgroundColor=kGreyColor;
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_gray"] forState:UIControlStateNormal];
        recordPauseButton.userInteractionEnabled=NO;
        if (playButton.isEnabled)
        {
            [playButton setEnabled:NO];
        }
        if (deleteButton.isEnabled)
        {
            [deleteButton setEnabled:NO];
        }
        if (sendButton.isEnabled)
        {
            [sendButton setEnabled:NO];
        }
    }
}


-(void)huddlenotification:(NSNotification *)notification
{
    NSDictionary *huddleha=notification.object;
    if ([[huddleha objectForKey:@"ha"] isEqualToString:@"false"])
    {
        announcementLabel.text=@"";
        deleteButton.backgroundColor=kGreyColor;
        sendButton.backgroundColor=kGreyColor;
        playButton.backgroundColor=kGreyColor;
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
        recordPauseButton.userInteractionEnabled=YES;
        if (playButton.isEnabled)
        {
            [playButton setEnabled:NO];
        }
        if (deleteButton.isEnabled)
        {
            [deleteButton setEnabled:NO];
        }
        if (sendButton.isEnabled)
        {
            [sendButton setEnabled:NO];
        }
        
        
    }
    else
    {
        announcementLabel.text=@"";
        deleteButton.backgroundColor=kGreyColor;
        sendButton.backgroundColor=kGreyColor;
        playButton.backgroundColor=kGreyColor;
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_gray"] forState:UIControlStateNormal];
        recordPauseButton.userInteractionEnabled=NO;
        if (playButton.isEnabled)
        {
            [playButton setEnabled:NO];
        }
        if (deleteButton.isEnabled)
        {
            [deleteButton setEnabled:NO];
        }
        if (sendButton.isEnabled)
        {
            [sendButton setEnabled:NO];
        }
    }
}




#pragma mark - logonStealing method
-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"AnnouncementViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [ self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - button single tap method
-(void)buttonSingleTap
{
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"AnnouncementViewController"])
    {
        deleteButton.backgroundColor=kGreyColor;
        sendButton.backgroundColor=kGreyColor;
        playButton.backgroundColor=kGreyColor;
        playButton.enabled=NO;
        deleteButton.enabled=NO;
        sendButton.enabled=NO;
        announcementLabel.text=@"";
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
        
        
        [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];
        [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:YES];
        [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:YES];
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
        {
            [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:YES];
        }
        announcementnow.backgroundColor=kNavBackgroundColor;
        announcementweek.backgroundColor=kNavBackgroundColor;
        announcement24hrs.backgroundColor=kNavBackgroundColor;
        announcementsales.backgroundColor=kNavBackgroundColor;
    }
}


#pragma mark - audio notification methods
-(void)audioInterruptionEnd
{
    audioInterruption=YES;
    
}

-(void)recordfullnotification
{
    announcementnow.backgroundColor=kGreyColor;
    announcementweek.backgroundColor=kGreyColor;
    announcement24hrs.backgroundColor=kGreyColor;
    announcementsales.backgroundColor=kGreyColor;
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:NO];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:NO];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
    {
        [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:NO];
    }
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:NO];
}

-(void)playAnnouncement:(NSNotification *)sender
{
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    announcementLabel.text=@"";
    deleteButton.backgroundColor=kNavBackgroundColor;
    [deleteButton setEnabled:YES];
    
    if (audioSendButton==YES)
    {
        sendButton.backgroundColor=kGreyColor;
        [sendButton setEnabled:NO];
        
    }
    else
    {
        announcementLabel.text=[dictLang objectForKey:@"Ready to send"];
        sendButton.backgroundColor=kNavBackgroundColor;
        [sendButton setEnabled:YES];
        
    }
    
}

-(void)sendAnnouncement:(NSNotification *)sender
{
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    self.navigationController.navigationBar.tintColor = kGreyColor;
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"AnnouncementViewController"])
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"AnnouncementhuddleVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
            
        }
        else
        {
            announcementhuddleaudiourl=sender.object;
            if ([[AudioManager getsharedInstance]getAudioduration:announcementhuddleaudiourl]<1.0)
            {
                [AlertView alert:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Audio length is too short to send"],[[DiagnosticTool sharedManager] generateScreenCode:@"AnnouncementhuddleVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"Audio length is too short to send"]]];
                announcementLabel.text=@"";
                recordPauseButton.userInteractionEnabled=YES;
                [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
                self.navigationController.navigationBar.userInteractionEnabled = YES;
                self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
            }
            else
            {
                NSString *millisec=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
                NSArray *tempArray = [millisec componentsSeparatedByString:@"."];
                NSString *macId;
                if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
                    macId=[[NSString stringWithFormat:@"%@",
                            [[[[UIDevice currentDevice] identifierForVendor] UUIDString]substringFromIndex:24]]lowercaseString];
                }
                
                NSDictionary  *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
                NSData *file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
                pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",@"All",@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0] doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:announcementhuddleaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithDouble:10],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
                NSDictionary *whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[@"all"],@"users",[NSArray arrayWithObject:storeNameString],@"store", [NSArray arrayWithObject:[loginDetails objectForKey:@"chainName"]],@"chain",@"managerApp",@"originatorType",@"announcement_huddle",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",@[@""],@"groups",nil];
                [HttpHandler audioFileSend:whoDic audioFile:file1Data pns:pns userScreen:@"AnnouncementhuddleVC"];
                
            }
        }
    }
    
    
}

-(void)recievedAnnouncement:(NSNotification *)sender
{
    
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"AnnouncementViewController"])
    {
        NSString *millisec=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
        NSArray *tempArray = [millisec componentsSeparatedByString:@"."];
        pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",@"All",@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0] doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:announcementhuddleaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithDouble:10],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
        
        if ([[sender.object objectAtIndex:0] isEqualToString:@"error"])
        {
            recordPauseButton.userInteractionEnabled=YES;
            [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
            [playButton setEnabled:YES];
            [deleteButton setEnabled:YES];
            playButton.backgroundColor=kNavBackgroundColor;
            deleteButton.backgroundColor=kNavBackgroundColor;
            announcementLabel.text=[dictLang objectForKey:@"Announcement not sent"];
            sendButton.backgroundColor=kNavBackgroundColor;
            [sendButton setEnabled:YES];
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
            
        }
        else if ([[sender.object objectAtIndex:0] isEqualToString:@"message"])
        {
            NSDictionary *huddledic=[NSDictionary dictionaryWithObjectsAndKeys:@"true",@"ha" ,nil];
            [[NSUserDefaults standardUserDefaults] setObject:huddledic forKey:@"huddleha"];
            [self huddlenotification];
            
            announcementLabel.text=[dictLang objectForKey:@"Announcement Sent"];
            audioSendButton=YES;
            [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];
            [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:YES];
            [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:YES];
            [[[[self.tabBarController tabBar]items]objectAtIndex:3]setEnabled:YES];
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
            {
                [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:YES];
            }
            announcementnow.backgroundColor=kNavBackgroundColor;
            announcementweek.backgroundColor=kNavBackgroundColor;
            announcement24hrs.backgroundColor=kNavBackgroundColor;
            announcementsales.backgroundColor=kNavBackgroundColor;
            [pns setObject:storeNameString forKey:@"storeName"];
            [pns setObject:@"HEARD" forKey:@"status"];
            [pns setObject:[NSNumber numberWithInt:1] forKey:@"maCallType"];
            [pns setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] forKey:@"storeId"];
            //[pns setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"] forKey:@"pushTxId"];
            
            if ([sender.object count]==2)
            {
                [pns setObject:[sender.object objectAtIndex:1] forKey:@"pushTxId"];
            }
            if ([pns count]>0)
            {
                [[Earboxinboxmodel sharedModel]announcementsentSave:[NSArray arrayWithObject:pns]];
            }
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
        }
    }
}

#pragma mark - button down method
- (void)buttondDownPressed:(UIButton *)sender
{
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    announcementLabel.text=[dictLang objectForKey:@"Recording..."];
    [[AudioManager getsharedInstance] recordPauseTapped];
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue_white.png"] forState:UIControlStateNormal];
    
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:NO];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:NO];
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:NO];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
    {
        [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:NO];
    }
    announcementnow.backgroundColor=kGreyColor;
    announcementweek.backgroundColor=kGreyColor;
    announcement24hrs.backgroundColor=kGreyColor;
    announcementsales.backgroundColor=kGreyColor;
}


#pragma mark - button up method
- (void)buttonUpPressed:(UIButton *)sender
{
    announcementLabel.text=[dictLang objectForKey:@"Ready to send"];
    audioSendButton=NO;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
    
     [playButton setEnabled:YES];
     [deleteButton setEnabled:YES];
     [sendButton setEnabled:YES];
    
    deleteButton.backgroundColor=kNavBackgroundColor;
    sendButton.backgroundColor=kNavBackgroundColor;
    playButton.backgroundColor=kNavBackgroundColor;
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    [[AudioManager getsharedInstance] stopTapped];
}


- (void)buttonDragOutside:(UIButton *)sender
{
    announcementLabel.text=[dictLang objectForKey:@"Ready to send"];
    audioSendButton=NO;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
    [playButton setEnabled:YES];
    [deleteButton setEnabled:YES];
    [sendButton setEnabled:YES];
    deleteButton.backgroundColor=kNavBackgroundColor;
    sendButton.backgroundColor=kNavBackgroundColor;
    playButton.backgroundColor=kNavBackgroundColor;
    [[AudioManager getsharedInstance] stopTapped];
}

#pragma mark - play button method
- (IBAction)playTapped:(UIButton *)sender {
    if ([playButton.titleLabel.text isEqualToString:@"Stop"])
    {
        if (audioSendButton==YES)
        {
            [deleteButton setEnabled:YES];
            [sendButton setEnabled:NO];
            deleteButton.backgroundColor=kNavBackgroundColor;
            sendButton.backgroundColor=kGreyColor;
            announcementLabel.text=@"";
            [[AudioManager getsharedInstance]audioplayerstop];
            [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
        }
        else
        {
            [deleteButton setEnabled:YES];
            [sendButton setEnabled:YES];
            deleteButton.backgroundColor=kNavBackgroundColor;
            sendButton.backgroundColor=kNavBackgroundColor;
            announcementLabel.text=@"";
            [[AudioManager getsharedInstance]audioplayerstop];
            [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
        }
    }
    else
    {
        [playButton setTitle:[dictLang objectForKey:@"Stop"] forState:UIControlStateNormal];
        [deleteButton setEnabled:NO];
        [sendButton setEnabled:NO];
        deleteButton.backgroundColor=kGreyColor;
        sendButton.backgroundColor=kGreyColor;
        announcementLabel.text=[dictLang objectForKey:@"Replaying..."];
        [[AudioManager getsharedInstance] playTapped];
    }
}

#pragma mark - send button method
- (IBAction)sendFileTapped:(id)sender
{
    [playButton setEnabled:NO];
    [deleteButton setEnabled:NO];
    [sendButton setEnabled:NO];
    announcementLabel.text=[dictLang objectForKey:@"Sending..."];
    playButton.backgroundColor=kGreyColor;
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    recordPauseButton.userInteractionEnabled=NO;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_gray.png"] forState:UIControlStateNormal];
    [[AudioManager getsharedInstance] sendTapped];
}

#pragma mark - delete button method
- (IBAction)deleteFileTapped:(id)sender
{
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:YES];
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:YES];
    [[[[self.tabBarController tabBar]items]objectAtIndex:3]setEnabled:YES];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
    {
        [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:YES];
    }
    announcementnow.backgroundColor=kNavBackgroundColor;
    announcementweek.backgroundColor=kNavBackgroundColor;
    announcement24hrs.backgroundColor=kNavBackgroundColor;
    announcementsales.backgroundColor=kNavBackgroundColor;
    announcementLabel.text=[dictLang objectForKey:@"Announcement deleted"];
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    playButton.backgroundColor=kGreyColor;
    [playButton setEnabled:NO];
    [deleteButton setEnabled:NO];
    [sendButton setEnabled:NO];
    [[AudioManager getsharedInstance] deleteTapped];
}


#pragma mark - pickerview methods

-(void)starttimeButtonAction
{
    [self pickerViewAction];
    huddletimecheck=YES;
}


-(void)endtimeButtonAction
{
    [self pickerViewAction];
    huddletimecheck=NO;
}

-(void)pickerViewAction
{
    timerokButton.hidden=NO;
    // assumes global UIPickerView declared. Move the frame to wherever you want it
    picker = [[UIPickerView alloc] initWithFrame:CGRectMake(10, (self.view.frame.size.height-270)/2, self.view.frame.size.width-20, 300)];
    picker.backgroundColor=kNavBackgroundColor;
    picker.dataSource = self;
    picker.delegate = self;
    NSString *htString=[[[NSUserDefaults standardUserDefaults] objectForKey:@"huddle"] objectForKey:@"ht"];
    
    if ([[htString componentsSeparatedByString:@":"] count]==1)
    {
        [picker selectRow:[[htString componentsSeparatedByString:@":"][0] intValue] inComponent:0 animated:YES];
        
        [self timerChange:0 timer:[[htString componentsSeparatedByString:@":"][0]intValue]];
        
    }
    if ([[htString componentsSeparatedByString:@":"] count]==2)
    {
        [picker selectRow:[[htString componentsSeparatedByString:@":"][0] intValue] inComponent:0 animated:YES];
        [picker selectRow:[[htString componentsSeparatedByString:@":"][1] intValue] inComponent:1 animated:YES];
        
        [self timerChange:0 timer:[[htString componentsSeparatedByString:@":"][0]intValue]];
        [self timerChange:1 timer:[[htString componentsSeparatedByString:@":"][1]intValue]];
    }
    if ([[htString componentsSeparatedByString:@":"] count]==3)
    {
        [picker selectRow:[[htString componentsSeparatedByString:@":"][0] intValue] inComponent:0 animated:YES];
        [picker selectRow:[[htString componentsSeparatedByString:@":"][1] intValue] inComponent:1 animated:YES];
        [picker selectRow:[[htString componentsSeparatedByString:@":"][2] intValue] inComponent:2 animated:YES];
        
        [self timerChange:0 timer:[[htString componentsSeparatedByString:@":"][0]intValue]];
        [self timerChange:1 timer:[[htString componentsSeparatedByString:@":"][1]intValue]];
        [self timerChange:2 timer:[[htString componentsSeparatedByString:@":"][2]intValue]];
    }
    [picker selectRow:0 inComponent:2 animated:YES];
    UILabel *hourLabel = [[UILabel alloc] initWithFrame:CGRectMake(42, picker.frame.size.height / 2 - 15, 75, 30)];
    hourLabel.textColor=[UIColor whiteColor];
    hourLabel.text = @"hour";
    [picker addSubview:hourLabel];
    
    UILabel *minsLabel = [[UILabel alloc] initWithFrame:CGRectMake(42 + (picker.frame.size.width / 3), picker.frame.size.height / 2 - 15, 75, 30)];
    minsLabel.textColor=[UIColor whiteColor];
    minsLabel.text = @"min";
    [picker addSubview:minsLabel];
    
    UILabel *secsLabel = [[UILabel alloc] initWithFrame:CGRectMake(42 + ((picker.frame.size.width / 3) * 2), picker.frame.size.height / 2 - 15, 75, 30)];
    secsLabel.textColor=[UIColor whiteColor];
    secsLabel.text = @"sec";
    [picker addSubview:secsLabel];
    [self.view addSubview:picker];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component == 0)
        return [huddlehoursArray count];
    
    return [huddleminutesArray count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *columnView = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, self.view.frame.size.width/3 - 35, 30)];
    
    columnView.textAlignment = NSTextAlignmentLeft;
    if (component==0)
    {
        columnView.text = [NSString stringWithFormat:@"%@",[huddlehoursArray objectAtIndex:row]];
    }
    else
    {
        columnView.text = [NSString stringWithFormat:@"%@",[huddleminutesArray objectAtIndex:row]];
    }
    
    return columnView;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    confrimButton.userInteractionEnabled=YES;
    confrimButton.backgroundColor=kNavBackgroundColor;
    NSInteger huddleHour=[pickerView selectedRowInComponent:component];
    switch (component) {
        case 0:
            [self timerChange:0 timer:huddleHour];
            break;
        case 1:
            [self timerChange:1 timer:huddleHour];
            break;
        case 2:
            [self timerChange:2 timer:huddleHour];
            break;
    }
}


-(void)timerChange:(int)component timer:(NSInteger)timer
{
    switch (component) {
        case 0:
        switch (timer)
        {
            case 0:
                huddleTimeHour=@"00";
                break;
            case 1:
                huddleTimeHour=@"01";
                break;
            case 2:
                huddleTimeHour=@"02";
                break;
            case 3:
                huddleTimeHour=@"03";
                break;
            case 4:
                huddleTimeHour=@"04";
                break;
            case 5:
                huddleTimeHour=@"05";
                break;
            case 6:
                huddleTimeHour=@"06";
                break;
            case 7:
                huddleTimeHour=@"07";
                break;
            case 8:
                huddleTimeHour=@"08";
                break;
            case 9:
                huddleTimeHour=@"09";
                break;
                
            default:
                huddleTimeHour=[NSString stringWithFormat:@"%ld",(long)timer];
                break;
        }
            break;
    case 1:
        switch (timer)
        {
            case 0:
                huddleTimeMin=@"00";
                break;
            case 1:
                huddleTimeMin=@"01";
                break;
            case 2:
                huddleTimeMin=@"02";
                break;
            case 3:
                huddleTimeMin=@"03";
                break;
            case 4:
                huddleTimeMin=@"04";
                break;
            case 5:
                huddleTimeMin=@"05";
                break;
            case 6:
                huddleTimeMin=@"06";
                break;
            case 7:
                huddleTimeMin=@"07";
                break;
            case 8:
                huddleTimeMin=@"08";
                break;
            case 9:
                huddleTimeMin=@"09";
                break;
                
            default:
                huddleTimeMin=[NSString stringWithFormat:@"%ld",(long)timer];
                break;
        }
            break;
    case 2:
        switch (timer)
        {
            case 0:
                huddleTimeSec=@"00";
                break;
            case 1:
                huddleTimeSec=@"01";
                break;
            case 2:
                huddleTimeSec=@"02";
                break;
            case 3:
                huddleTimeSec=@"03";
                break;
            case 4:
                huddleTimeSec=@"04";
                break;
            case 5:
                huddleTimeSec=@"05";
                break;
            case 6:
                huddleTimeSec=@"06";
                break;
            case 7:
                huddleTimeSec=@"07";
                break;
            case 8:
                huddleTimeSec=@"08";
                break;
            case 9:
                huddleTimeSec=@"09";
                break;
                
            default:
                huddleTimeSec=[NSString stringWithFormat:@"%ld",(long)timer];
                break;
        }
            break;
    }
}

@end


