//
//  WeekViewController.m
//  Managers
//
//  Created by sandeepchalla on 9/26/15.
//  Copyright © 2015 theatro. All rights reserved.
//

#import "WeekViewController.h"

#import "GroupsDetailViewController.h"
#import "AudioManager.h"
#import "Constant.h"
#import "Reachability.h"
#import "AlertView.h"
#import "HttpHandler.h"
#import "StateMachineHandler.h"
#import "UIViewController.h"
#import "ViewController.h"
#import "UIViewController.h"
#import "Earboxinboxmodel.h"
#import "DiagnosticTool.h"


@interface WeekViewController ()
{
    NSMutableArray *announcementtabwidth;
    int tabitemwidth;
    NSString *storeNameString;
    NSURL *announcementweekaudiourl;
    BOOL audioSendButton;
    UILabel *announcementnow,*announcement24hrs,*announcementweek,*announcementhuddle,*announcementsales;
    NSMutableDictionary *pns;
    BOOL audioInterruption;
    NSDictionary *dictLang;
}

@end

@implementation WeekViewController
@synthesize deleteButton, playButton, recordPauseButton,sendButton;

static  WeekViewController *sharedMgr = nil;

+(WeekViewController *)getsharedInstance {
    if (sharedMgr == nil)
    {
        sharedMgr = [[WeekViewController alloc] init];
    }
    return sharedMgr;
}

-(void)broadCostButtonAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)audioInterruptionEnd
{
    audioInterruption=YES;
    
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush isEqualToString:@"YES"])
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"StoreListViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        else
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"VPViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        
    }
    
    else
    {
        if (audioInterruption==YES)
        {
            [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
            recordPauseButton.userInteractionEnabled=YES;
            audioInterruption=NO;
        }
        [[StateMachineHandler sharedManager]reOpenClosedSockets];
        [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
    }
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAnnouncementsNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"audioNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recievedAnnouncementsNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAnnouncementsSingleTapNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAnnouncementsfullrecordNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        [[AudioManager getsharedInstance]audioplayerstop];
        [[AudioManager getsharedInstance]recordstop];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
        
    }];
    
    [super viewWillDisappear:animated];
}


-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    
    NSLog(@"Announcement Week viewWillAppear");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playAnnouncement:)
                                                 name:@"playAnnouncementsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendAnnouncement:)
                                                 name:@"audioNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedAnnouncement:)
                                                 name:@"recievedAnnouncementsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(buttonSingleTap)
                                                 name:@"playAnnouncementsSingleTapNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recordfullnotification)
                                                 name:@"playAnnouncementsfullrecordNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
        {
            
            if (screenSize.width ==414)
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"10",@"92",@"175",@"255",@"335", nil];
                tabitemwidth=70;
            }
            else if (screenSize.width ==375)
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"15",@"85",@"155",@"225",@"295", nil];
                tabitemwidth=60;
            }
            else
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"7",@"70",@"133",@"195",@"258", nil];
                tabitemwidth=55;
            }
            announcementsales=[[UILabel alloc]init];
            announcementsales.frame=CGRectMake([[announcementtabwidth objectAtIndex:4]intValue], 7, tabitemwidth, 35);
            announcementsales.backgroundColor=kNavBackgroundColor;
            announcementsales.font=[UIFont fontWithName:kFontName size:12];
            announcementsales.layer.cornerRadius=6;
            announcementsales.textColor=[UIColor whiteColor];
            announcementsales.textAlignment=NSTextAlignmentCenter;
            announcementsales.text=[dictLang objectForKey:@"SALES"];
            announcementsales.clipsToBounds=YES;
            [self.tabBarController.tabBar addSubview:announcementsales];
            
        }
        else
        {
            if (screenSize.width ==414)
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"29",@"125",@"222",@"317", nil];
                tabitemwidth=70;
            }
            else if (screenSize.width ==375)
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"20",@"108",@"196",@"284", nil];
                tabitemwidth=70;
            }
            else
            {
                announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"20",@"96",@"170",@"245", nil];
                tabitemwidth=55;
            }
        }
        
        
    }
    else
    {
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
        {
            
            announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"130",@"240",@"350",@"470",@"590", nil];
            tabitemwidth=70;
            announcementsales=[[UILabel alloc]init];
            announcementsales.frame=CGRectMake([[announcementtabwidth objectAtIndex:4]intValue], 7, tabitemwidth, 35);
            announcementsales.backgroundColor=kNavBackgroundColor;
            announcementsales.font=[UIFont fontWithName:kFontName size:12];
            announcementsales.layer.cornerRadius=6;
            announcementsales.textColor=[UIColor whiteColor];
            announcementsales.textAlignment=NSTextAlignmentCenter;
            announcementsales.text=[dictLang objectForKey:@"SALES"];
            announcementsales.clipsToBounds=YES;
            [self.tabBarController.tabBar addSubview:announcementsales];
            
        }
        else
        {
            announcementtabwidth=[[NSMutableArray alloc]initWithObjects:@"130",@"240",@"350",@"470", nil];
            tabitemwidth=70;
        }
        
        
    }
    
    announcementnow=[[UILabel alloc]init];
    announcementnow.frame=CGRectMake([[announcementtabwidth objectAtIndex:0] intValue], 7, tabitemwidth, 35);
    announcementnow.backgroundColor=kNavBackgroundColor;
    announcementnow.font=[UIFont fontWithName:kFontName size:12];
    announcementnow.layer.cornerRadius=6;
    announcementnow.textColor=[UIColor whiteColor];
    announcementnow.textAlignment=NSTextAlignmentCenter;
    announcementnow.text=[dictLang objectForKey:@"Now"];
    announcementnow.clipsToBounds=YES;
    [self.tabBarController.tabBar addSubview:announcementnow];
    
    
    announcement24hrs=[[UILabel alloc]init];
    announcement24hrs.frame=CGRectMake([[announcementtabwidth objectAtIndex:1]intValue], 7, tabitemwidth, 35);
    announcement24hrs.backgroundColor=kNavBackgroundColor;
    announcement24hrs.font=[UIFont fontWithName:kFontName size:12];
    announcement24hrs.layer.cornerRadius=6;
    announcement24hrs.textColor=[UIColor whiteColor];
    announcement24hrs.textAlignment=NSTextAlignmentCenter;
    announcement24hrs.text=[dictLang objectForKey:@"24Hrs"];
    announcement24hrs.clipsToBounds=YES;
    [self.tabBarController.tabBar addSubview:announcement24hrs];
    
    
    announcementweek=[[UILabel alloc]init];
    announcementweek.frame=CGRectMake([[announcementtabwidth objectAtIndex:2]intValue], 7, tabitemwidth, 35);
    announcementweek.backgroundColor=kTabbarColor;
    announcementweek.font=[UIFont fontWithName:kFontName size:12];
    announcementweek.layer.cornerRadius=6;
    announcementweek.textColor=[UIColor whiteColor];
    announcementweek.textAlignment=NSTextAlignmentCenter;
    announcementweek.text=[dictLang objectForKey:@"Week"];
    announcementweek.clipsToBounds=YES;
    [self.tabBarController.tabBar addSubview:announcementweek];
    
    
    announcementhuddle=[[UILabel alloc]init];
    announcementhuddle.frame=CGRectMake([[announcementtabwidth objectAtIndex:3]intValue], 7, tabitemwidth, 35);
    announcementhuddle.backgroundColor=kNavBackgroundColor;
    announcementhuddle.font=[UIFont fontWithName:kFontName size:12];
    announcementhuddle.layer.cornerRadius=6;
    announcementhuddle.textColor=[UIColor whiteColor];
    announcementhuddle.textAlignment=NSTextAlignmentCenter;
    announcementhuddle.text=[dictLang objectForKey:@"Huddle"];
    NSDictionary *uiElementsDic=[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"uiElements"]];
    if ([uiElementsDic count]!=0)
    {
        if(![uiElementsDic objectForKey:@"huddle"] )
        {
            announcementhuddle.text=[dictLang objectForKey:@"Huddle"];
        }
        else
        {
            announcementhuddle.text=[uiElementsDic objectForKey:@"huddle"];
        }
        if(![uiElementsDic objectForKey:@"AnnouncementToday"] )
            
        {
            
            announcement24hrs.text=[dictLang objectForKey:@"24Hrs"];
            
        }
        
        else
            
        {
            
            announcement24hrs.text=[uiElementsDic objectForKey:@"AnnouncementToday"];
            
        }
        
    }
    announcementhuddle.clipsToBounds=YES;
    [self.tabBarController.tabBar addSubview:announcementhuddle];
    
    
    
    
    [self announcementTitle];
    announcementLabel.text=@"";
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    playButton.backgroundColor=kGreyColor;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
    playButton.userInteractionEnabled=NO;
    deleteButton.userInteractionEnabled=NO;
    sendButton.userInteractionEnabled=NO;
    [super viewWillAppear:animated];
}


-(void)announcementTitle
{
    storeNameString=[[NSUserDefaults standardUserDefaults]objectForKey:@"storeNameString"];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = kNavBackgroundColor;
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
    titleLabel.text = [dictLang objectForKey:@"Announcement"];
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = kNavBackgroundColor;
    subTitleLabel.font = [UIFont systemFontOfSize:12];
    subTitleLabel.text =[dictLang objectForKey:@"Week"];
    [subTitleLabel sizeToFit];
    
    UIView *navigationTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [navigationTitleView addSubview:titleLabel];
    [navigationTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    CGRect titleframe = titleLabel.frame;
    if (widthDiff > 0) {
        
        titleframe.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(titleframe);
        
    }else{
        titleframe.size.width=self.view.frame.size.width-100;
        titleLabel.frame = CGRectIntegral(titleframe);
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
    self.parentViewController.navigationItem.titleView=navigationTitleView;
}


-(void)viewDidLoad {
    
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    [deleteButton setTitle:[dictLang objectForKey:@"Delete"] forState:UIControlStateNormal];
    [sendButton setTitle:[dictLang objectForKey:@"Send"] forState:UIControlStateNormal];
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    playButton.layer.cornerRadius=8;
    deleteButton.layer.cornerRadius=8;
    sendButton.layer.cornerRadius=8;
    [super viewDidLoad];
    [recordPauseButton addTarget:self action:@selector(buttondDownPressed:) forControlEvents: UIControlEventTouchDown];
    [recordPauseButton addTarget:self action:@selector(buttonUpPressed:) forControlEvents: UIControlEventTouchUpInside];
    [recordPauseButton addTarget:self action:@selector(buttonDragOutside:) forControlEvents: UIControlEventTouchDragOutside];
    [[AudioManager getsharedInstance] path];
    
    
}

-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"WeekViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [ self.navigationController popToRootViewControllerAnimated:YES];
    }
}


-(void)buttonSingleTap
{
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"WeekViewController"])
    {
        deleteButton.backgroundColor=kGreyColor;
        sendButton.backgroundColor=kGreyColor;
        playButton.backgroundColor=kGreyColor;
        playButton.userInteractionEnabled=NO;
        deleteButton.userInteractionEnabled=NO;
        sendButton.userInteractionEnabled=NO;
        announcementLabel.text=@"";
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
        
        [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:YES];
        [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];
        [[[[self.tabBarController tabBar]items]objectAtIndex:3]setEnabled:YES];
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
        {
            [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:YES];
        }
        announcementnow.backgroundColor=kNavBackgroundColor;
        announcement24hrs.backgroundColor=kNavBackgroundColor;
        announcementhuddle.backgroundColor=kNavBackgroundColor;
        announcementsales.backgroundColor=kNavBackgroundColor;
    }
}

-(void)playAnnouncement:(NSNotification *)sender
{
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    announcementLabel.text=@"";
    deleteButton.backgroundColor=kNavBackgroundColor;
    [deleteButton setEnabled:YES];
    
    if (audioSendButton==YES)
    {
        sendButton.backgroundColor=kGreyColor;
        [sendButton setEnabled:NO];
        
    }
    else
    {
        announcementLabel.text=[dictLang objectForKey:@"Ready to send"];
        sendButton.backgroundColor=kNavBackgroundColor;
        [sendButton setEnabled:YES];
        
    }
    
}

-(void)sendAnnouncement:(NSNotification *)sender
{
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    self.navigationController.navigationBar.tintColor = kGreyColor;
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"WeekViewController"])
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
           [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"AnnouncementweekVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
            
        }
        else
        {
            announcementweekaudiourl=sender.object;
            if ([[AudioManager getsharedInstance]getAudioduration:announcementweekaudiourl]<1.0)
            {
                [AlertView alert:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Audio length is too short to send"],[[DiagnosticTool sharedManager] generateScreenCode:@"AnnouncementweekVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"Audio length is too short to send"]]];
                announcementLabel.text=@"";
                recordPauseButton.userInteractionEnabled=YES;
                [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
                self.navigationController.navigationBar.userInteractionEnabled = YES;
                self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
            }
            else
            {
                NSString *millisec=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ];
                NSArray *tempArray = [millisec componentsSeparatedByString:@"."];
                NSString *macId;
                if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
                    macId=[[NSString stringWithFormat:@"%@",
                            [[[[UIDevice currentDevice] identifierForVendor] UUIDString]substringFromIndex:24]]lowercaseString];
                }
                NSDictionary  *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
                
                NSData *file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
                pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",@"All",@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0] doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:announcementweekaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithDouble:25],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
                NSDictionary *whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[@"all"],@"users",[NSArray arrayWithObject:storeNameString],@"store", [NSArray arrayWithObject:[loginDetails objectForKey:@"chainName"]],@"chain",@"managerApp",@"originatorType",@"announcement_week",@"type",macId,@"macId",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"request_time",@[@""],@"groups",nil];
                [HttpHandler audioFileSend:whoDic audioFile:file1Data pns:pns userScreen:@"AnnouncementweekVC"];
                
            }
        }
    }
    
    
}

-(void)recievedAnnouncement:(NSNotification *)sender
{
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"WeekViewController"])
    {
        recordPauseButton.userInteractionEnabled=YES;
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
        playButton.userInteractionEnabled=YES;
        deleteButton.userInteractionEnabled=YES;
        playButton.backgroundColor=kNavBackgroundColor;
        deleteButton.backgroundColor=kNavBackgroundColor;
        NSString *millisec=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ];
        NSArray *tempArray = [millisec componentsSeparatedByString:@"."];
        pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",@"All",@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0] doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:announcementweekaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithDouble:25],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
        if ([[sender.object objectAtIndex:0] isEqualToString:@"error"])
        {
            announcementLabel.text=[dictLang objectForKey:@"Announcement not sent"];
            sendButton.backgroundColor=kNavBackgroundColor;
            sendButton.userInteractionEnabled=YES;
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
            
        }
        else if ([[sender.object objectAtIndex:0] isEqualToString:@"message"])
        {
            [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:YES];
            [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];
            [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:YES];
            [[[[self.tabBarController tabBar]items]objectAtIndex:3]setEnabled:YES];
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
            {
                [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:YES];
            }
            announcementnow.backgroundColor=kNavBackgroundColor;
            announcement24hrs.backgroundColor=kNavBackgroundColor;
            announcementhuddle.backgroundColor=kNavBackgroundColor;
            announcementsales.backgroundColor=kNavBackgroundColor;
            announcementLabel.text=[dictLang objectForKey:@"Announcement Sent"];
            audioSendButton=YES;
            sendButton.backgroundColor=kGreyColor;
            sendButton.userInteractionEnabled=NO;
            [pns setObject:storeNameString forKey:@"storeName"];
            [pns setObject:@"HEARD" forKey:@"status"];
            [pns setObject:[NSNumber numberWithInt:1] forKey:@"maCallType"];
            [pns setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] forKey:@"storeId"];
            // [pns setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"] forKey:@"pushTxId"];
            if ([sender.object count]==2)
            {
                [pns setObject:[sender.object objectAtIndex:1] forKey:@"pushTxId"];
            }
            if ([pns count]>0)
            {
                [[Earboxinboxmodel sharedModel]announcementsentSave:[NSArray arrayWithObject:pns]];
            }
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
        }
    }
    
}

- (void)buttondDownPressed:(UIButton *)sender
{
    playButton.userInteractionEnabled=NO;
    deleteButton.userInteractionEnabled=NO;
    sendButton.userInteractionEnabled=NO;
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    playButton.backgroundColor=kGreyColor;
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    announcementLabel.text=[dictLang objectForKey:@"Recording..."];
    [[AudioManager getsharedInstance] recordPauseTapped];
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue_white.png"] forState:UIControlStateNormal];
    
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:NO];
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:NO];
    [[[[self.tabBarController tabBar]items]objectAtIndex:3]setEnabled:NO];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
    {
        [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:NO];
    }
    announcementnow.backgroundColor=kGreyColor;
    announcement24hrs.backgroundColor=kGreyColor;
    announcementhuddle.backgroundColor=kGreyColor;
    announcementsales.backgroundColor=kGreyColor;
}


-(void)recordfullnotification
{
    announcementnow.backgroundColor=kGreyColor;
    announcement24hrs.backgroundColor=kGreyColor;
    announcementhuddle.backgroundColor=kGreyColor;
    announcementsales.backgroundColor=kGreyColor;
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:NO];
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:NO];
    [[[[self.tabBarController tabBar]items]objectAtIndex:3]setEnabled:NO];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
    {
        [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:NO];
    }
}

- (void)buttonUpPressed:(UIButton *)sender {
    
    
    announcementLabel.text=[dictLang objectForKey:@"Ready to send"];
    audioSendButton=NO;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
    playButton.userInteractionEnabled=YES;
    deleteButton.userInteractionEnabled=YES;
    sendButton.userInteractionEnabled=YES;
    deleteButton.backgroundColor=kNavBackgroundColor;
    sendButton.backgroundColor=kNavBackgroundColor;
    playButton.backgroundColor=kNavBackgroundColor;
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    [[AudioManager getsharedInstance] stopTapped];
}

-(void)buttonDragOutside:(UIButton *)sender
{
    announcementLabel.text=[dictLang objectForKey:@"Ready to send"];
    audioSendButton=NO;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
    [playButton setEnabled:YES];
    [deleteButton setEnabled:YES];
    [sendButton setEnabled:YES];
    deleteButton.backgroundColor=kNavBackgroundColor;
    sendButton.backgroundColor=kNavBackgroundColor;
    playButton.backgroundColor=kNavBackgroundColor;
    [[AudioManager getsharedInstance] stopTapped];
}

- (IBAction)playTapped:(UIButton *)sender {
    if ([playButton.titleLabel.text isEqualToString:[dictLang objectForKey:@"Stop"]])
    {
        if (audioSendButton==YES)
        {
            [deleteButton setEnabled:YES];
            [sendButton setEnabled:NO];
            deleteButton.backgroundColor=kNavBackgroundColor;
            sendButton.backgroundColor=kGreyColor;
            announcementLabel.text=@"";
            [[AudioManager getsharedInstance]audioplayerstop];
            [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
        }
        else
        {
            [deleteButton setEnabled:YES];
            [sendButton setEnabled:YES];
            deleteButton.backgroundColor=kNavBackgroundColor;
            sendButton.backgroundColor=kNavBackgroundColor;
            announcementLabel.text=@"";
            [[AudioManager getsharedInstance]audioplayerstop];
            [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
        }
    }
    else
    {
        [playButton setTitle:[dictLang objectForKey:@"Stop"] forState:UIControlStateNormal];
        [deleteButton setEnabled:NO];
        [sendButton setEnabled:NO];
        deleteButton.backgroundColor=kGreyColor;
        sendButton.backgroundColor=kGreyColor;
        announcementLabel.text=[dictLang objectForKey:@"Replaying..."];
        [[AudioManager getsharedInstance] playTapped];
    }
}


- (IBAction)sendFileTapped:(id)sender
{
    recordPauseButton.userInteractionEnabled=NO;
    playButton.userInteractionEnabled=NO;
    deleteButton.userInteractionEnabled=NO;
    sendButton.userInteractionEnabled=NO;
    announcementLabel.text=[dictLang objectForKey:@"Sending..."];
    playButton.backgroundColor=kGreyColor;
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_gray.png"] forState:UIControlStateNormal];
    [[AudioManager getsharedInstance] sendTapped];
}

- (IBAction)deleteFileTapped:(id)sender
{
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:YES];
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:YES];
    [[[[self.tabBarController tabBar]items]objectAtIndex:3]setEnabled:YES];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
    {
        [[[[self.tabBarController tabBar]items]objectAtIndex:4]setEnabled:YES];
    }
    announcementnow.backgroundColor=kNavBackgroundColor;
    announcement24hrs.backgroundColor=kNavBackgroundColor;
    announcementhuddle.backgroundColor=kNavBackgroundColor;
    announcementsales.backgroundColor=kNavBackgroundColor;
    announcementLabel.text=[dictLang objectForKey:@"Announcement deleted"];
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    playButton.backgroundColor=kGreyColor;
    playButton.userInteractionEnabled=NO;
    deleteButton.userInteractionEnabled=NO;
    sendButton.userInteractionEnabled=NO;
    [[AudioManager getsharedInstance] deleteTapped];
}

@end
