//
//  WeekViewController.h
//  Managers
//
//  Created by sandeepchalla on 9/26/15.
//  Copyright © 2015 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeekViewController : UIViewController<UIAlertViewDelegate>

{
    IBOutlet UILabel *announcementLabel;
     IBOutlet UILabel *lbl_Green;
}
@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property(nonatomic,strong)NSString *contactStatusString;

-(void)audioInterruptionEnd;
+(WeekViewController *)getsharedInstance;

- (IBAction)sendFileTapped:(id)sender;
- (IBAction)deleteFileTapped:(id)sender;
- (IBAction)playTapped:(id)sender;

@end
