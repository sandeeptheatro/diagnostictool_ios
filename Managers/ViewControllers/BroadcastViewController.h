//
//  BroadCostViewController.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BroadcastViewController : UIViewController<UIAlertViewDelegate>
{
    IBOutlet UIButton *broadCastButton;
}

@end
