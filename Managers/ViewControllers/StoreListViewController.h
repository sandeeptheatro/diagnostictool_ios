//
//  PlacesViewController.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 12/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,UIGestureRecognizerDelegate,UISearchBarDelegate,UITextFieldDelegate>
{
    IBOutlet UITableView *storesTableview;
    IBOutlet UISearchBar *searchBar;
}
@property(nonatomic,strong)NSString *logonFailed,*loggedoff;

@end
