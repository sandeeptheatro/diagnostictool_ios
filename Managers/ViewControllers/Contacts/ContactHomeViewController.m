//
//  ContactHomeViewController.m
//  Managers
//
//  Created by sandeepchalla on 10/7/15.
//  Copyright © 2015 theatro. All rights reserved.
//

#import "ContactHomeViewController.h"
#import "Constant.h"
#import "MessageViewController.h"
#import "ContactsDetailViewController.h"
#import "InterruptViewController.h"
#import "ViewController.h"
#import "UIViewController.h"
#import "StateMachineHandler.h"
#import "HttpHandler.h"
#import "DiagnosticTool.h"

@interface ContactHomeViewController ()
{
    UIImageView *contactStatusImage;
    int covarageStatus;
    UILabel *contactPlaceLabel,*connectionLabel;
    NSDictionary *dictLang;
    NSString *staticerrorcode;
}

@end

@implementation ContactHomeViewController
@synthesize contactStatusString,contactString;


-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [contacthometabbar invalidateIntrinsicContentSize];
    [super viewDidAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.hidesBackButton = NO;
    //self.navigationItem.leftItemsSupplementBackButton = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    NSLog(@"Employee home view viewWillApear");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:@"localnotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coverageRequestToServertask:)
                                                 name:@"coverageRequestToServertask"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    
    covarageStatus=[[[NSUserDefaults standardUserDefaults]objectForKey:@"contactstatus"] intValue];
    
    if (covarageStatus==6 || covarageStatus==3 )
    {
        if (covarageStatus==6)
        {
            contactStatusString=@"grey_circle";
        }
        else
        {
            contactStatusString=@"red_circle";
        }
        [[[contacthometabbar items]objectAtIndex:1]setEnabled:NO];
        [[[contacthometabbar items]objectAtIndex:2]setEnabled:NO];
    }
    
    if (covarageStatus==2)
    {
        contactStatusString=@"red_circle";
        [[[contacthometabbar items]objectAtIndex:2]setEnabled:YES];
    }
    if (covarageStatus==0)
    {
        contactStatusString=@"green_circle";
    }
    if (covarageStatus==1)
    {
        contactStatusString=@"red_circle";
    }
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"contactmanagericon"] intValue]==2)
    {
        [[[contacthometabbar items]objectAtIndex:1]setEnabled:NO];
        [[[contacthometabbar items]objectAtIndex:2]setEnabled:NO];
    }
    contactPlaceLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"contactlocation"];
    contactStatusImage.image=[UIImage imageNamed:contactStatusString];
    self.recordPauseButton.userInteractionEnabled=NO;
    [super viewWillAppear:animated];
}

-(void)reOpenClosedSocketsNotifiacation
{
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"ContactHomeViewController"])
    {
        
    }
    else
    {
        NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
        NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
        if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
        {
            if ([fromPush isEqualToString:@"YES"])
            {
                NSArray *viewcontrollers=[self.navigationController viewControllers];
                for (int i=0;i<[viewcontrollers count];i++)
                {
                    UIViewController *vc=[viewcontrollers objectAtIndex:i];
                    NSString *currentView=NSStringFromClass([vc class]);
                    if ([currentView isEqualToString:@"StoreListViewController"])
                    {
                        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                    }
                }
            }
            else
            {
                NSArray *viewcontrollers=[self.navigationController viewControllers];
                for (int i=0;i<[viewcontrollers count];i++)
                {
                    UIViewController *vc=[viewcontrollers objectAtIndex:i];
                    NSString *currentView=NSStringFromClass([vc class]);
                    if ([currentView isEqualToString:@"VPViewController"])
                    {
                        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                    }
                }
            }
            
        }
        
        else
        {
            connectionLabel.hidden=NO;
            [[StateMachineHandler sharedManager]reOpenClosedSockets];
            [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
        }
    }
    
    
}

-(void)coverageRequestToServertask:(NSNotification *)notification
{
    // Network messages getting from native c2 and updating to UI
    [self connectionRequest];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"texttime"])
    {
        if ([notification.object isEqualToString:@"server connection lost..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"server connection lost..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"could not start a connection..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"could not start a connection..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not logon to store..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not logon to store..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not connect to Proxy server..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not connect to Proxy server..."],staticerrorcode];
        }
        else
        {
            //Second time login ->please wait
            connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
        }
        
    }
    else
    {
        //First time login ,connecting to server->fetching emp list
        connectionLabel.text=notification.object;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"texttime"];
    }
    
    
}

-(void)tgsAlarmerror:(NSString *)tgsLoginerror
{
    staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:@"ContactVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"TgsLogin"],[[DiagnosticTool sharedManager]generateErrorCode:tgsLoginerror]];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        [HttpHandler alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"TgsLogin"] severity:@"1" alarmdesc:staticerrorcode];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
    }];
}

-(void)connectionRequest
{
    contacthometabbar.userInteractionEnabled=NO;
    connectionLabel.hidden=NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coverageRequestToServertask" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForceLogOut" object:nil];
    [super viewWillDisappear:animated];
}


-(void)coveragejson:(NSNotification *)notification
{
    contacthometabbar.userInteractionEnabled=YES;
    connectionLabel.hidden=YES;
    NSArray *coverageArray=[notification.object objectForKey:@"coverage"];
    for (int i=0; i<[coverageArray count]; i++)
    {
        // getting all covarageTagOutNames from jsonstring
        
        NSString *covarageTagOutName=[[[coverageArray objectAtIndex:i]objectForKey:@"name"] componentsSeparatedByString:@"_"][1];
        covarageStatus=[[[[coverageArray objectAtIndex:i]objectForKey:@"status"] componentsSeparatedByString:@"_"][0] intValue];
        if ([[covarageTagOutName componentsSeparatedByString:@"|"] count]>1)
        {
            covarageTagOutName=[covarageTagOutName componentsSeparatedByString:@"|"][0];
        }
        if ([covarageTagOutName isEqualToString:contactString])
        {
            covarageStatus=[[[[coverageArray objectAtIndex:i]objectForKey:@"status"] componentsSeparatedByString:@"_"][0] intValue];
            
            NSString *location=[[[coverageArray objectAtIndex:i]objectForKey:@"status"] componentsSeparatedByString:@"_"][1];
            [[[contacthometabbar items]objectAtIndex:1]setEnabled:NO];
            switch (covarageStatus) {
                case 0:
                    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"contactmanagericon"] intValue]==2)
                    {
                        [[[contacthometabbar items]objectAtIndex:1]setEnabled:NO];
                        [[[contacthometabbar items]objectAtIndex:2]setEnabled:NO];
                    }
                    else
                    {
                        [[[contacthometabbar items]objectAtIndex:1]setEnabled:YES];
                        [[[contacthometabbar items]objectAtIndex:2]setEnabled:YES];
                        
                    }
                    contactStatusImage.image=[UIImage imageNamed:@"green_circle"];
                    break;
                    
                    
                case 2:
                case 1:
                case 3:
                    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"contactmanagericon"] intValue]==2)
                    {
                        [[[contacthometabbar items]objectAtIndex:1]setEnabled:NO];
                        [[[contacthometabbar items]objectAtIndex:2]setEnabled:NO];
                    }
                    else
                    {
                        [[[contacthometabbar items]objectAtIndex:1]setEnabled:NO];
                        
                    }
                    contactStatusImage.image=[UIImage imageNamed:@"red_circle"];
                    break;
                    
                case 6:
                    /* if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"contactmanagericon"] intValue]==2)
                     {
                     [[[contacthometabbar items]objectAtIndex:1]setEnabled:NO];
                     [[[contacthometabbar items]objectAtIndex:2]setEnabled:NO];
                     }
                     else
                     {
                     
                     }*/
                    
                    [[[contacthometabbar items]objectAtIndex:1]setEnabled:NO];
                    [[[contacthometabbar items]objectAtIndex:2]setEnabled:NO];
                    contactStatusImage.image=[UIImage imageNamed:@"grey_circle"];
                    location=@"";
                    break;
                    
            }
            contactPlaceLabel.text=location;
            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",covarageStatus] forKey:@"contactstatus"];
            [[NSUserDefaults standardUserDefaults]setObject:location forKey:@"contactlocation"];
        }
        
        else if(covarageStatus==5)
        {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"contactmanagericon"] intValue]==2)
            {
                [[[contacthometabbar items]objectAtIndex:1]setEnabled:NO];
                [[[contacthometabbar items]objectAtIndex:2]setEnabled:NO];
            }
            else
            {
                
            }
            
            contactStatusImage.image=[UIImage imageNamed:@"grey_circle"];
        }
        
    }
    
}


-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"ContactHomeViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [ self.navigationController popToRootViewControllerAnimated:YES];
    }
}


-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    connectionLabel=[[UILabel alloc]init];
    connectionLabel.textColor=[UIColor whiteColor];
    connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
    connectionLabel.frame= CGRectMake(0,self.view.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height);
    connectionLabel.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    connectionLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:connectionLabel];
    connectionLabel.hidden=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coveragejson:)
                                                 name:@"coveragejson"
                                               object:nil];
    
    covarageStatus=[[[NSUserDefaults standardUserDefaults]objectForKey:@"contactstatus"] intValue];
    
    [[[contacthometabbar items] objectAtIndex:0] setTitle:[dictLang objectForKey:@"Message"]];
    [[[contacthometabbar items] objectAtIndex:1] setTitle:[dictLang objectForKey:@"One-to-One"]];
    [[[contacthometabbar items] objectAtIndex:2] setTitle:[dictLang objectForKey:@"Interrupt"]];
    [self intialView];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = kNavBackgroundColor;
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
    titleLabel.text = [[[NSUserDefaults standardUserDefaults]objectForKey:@"contactname"]capitalizedString];
    [titleLabel sizeToFit];
    
    contactString=[[NSUserDefaults standardUserDefaults]objectForKey:@"contactname"];
    
    contactPlaceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 18, self.view.frame.size.width-150, 20)];
    contactPlaceLabel.backgroundColor = [UIColor clearColor];
    contactPlaceLabel.textColor = kNavBackgroundColor;
    contactPlaceLabel.font = [UIFont systemFontOfSize:12];
    
    contactPlaceLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"contactlocation"];
    
    contactPlaceLabel.textAlignment=NSTextAlignmentCenter;
    [contactPlaceLabel sizeToFit];
    
    UIView *navigationTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-160, 30)];
    
    contactStatusImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    float widthDiff = contactPlaceLabel.frame.size.width - titleLabel.frame.size.width;
    CGRect titleframe = titleLabel.frame;
    CGRect subtitleframe = contactPlaceLabel.frame;
    if (widthDiff > 0)
    {
        titleLabel.frame =CGRectMake(((self.view.frame.size.width-100)/2)-titleframe.size.width/2, titleframe.origin.y, self.view.frame.size.width, titleframe.size.height); //CGRectIntegral(titleframe);
        
        contactPlaceLabel.frame = CGRectMake(((self.view.frame.size.width-100)/2)-subtitleframe.size.width/2, subtitleframe.origin.y, self.view.frame.size.width, subtitleframe.size.height);
        contactStatusImage.frame=CGRectMake(((self.view.frame.size.width-90)/2)-(titleframe.size.width/2)-20,05,12,12);
        
        
    }
    else{
        titleLabel.frame =CGRectMake(((self.view.frame.size.width-100)/2)-titleframe.size.width/2, titleframe.origin.y, self.view.frame.size.width-100, titleframe.size.height);
        subtitleframe.size.width=self.view.frame.size.width-200;
        subtitleframe.origin.x=20;
        contactPlaceLabel.frame = CGRectIntegral(subtitleframe);
        contactStatusImage.frame=CGRectMake(((self.view.frame.size.width-90)/2)-(titleframe.size.width/2)-20,05,12,12);
    }
    contactStatusImage.image=[UIImage imageNamed:contactStatusString];
    [navigationTitleView addSubview:titleLabel];
    [navigationTitleView addSubview:contactPlaceLabel];
    [navigationTitleView addSubview:contactStatusImage];
    self.navigationItem.titleView=navigationTitleView;
    
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
    
    if ([str isEqualToString:@"store"])
    {
        contacthometabbar.hidden=YES;
        _recordPauseButton.userInteractionEnabled=YES;
        [_recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
    }
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)intialView
{
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:NO];
    switch (covarageStatus) {
        case 0:
            [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];
            contactStatusString=@"green_circle";
            break;
        case 1:
        case 2:
            [[[contacthometabbar items]objectAtIndex:1]setEnabled:NO];
            contactStatusString=@"red_circle";
            break;
        case 3:
            [[[contacthometabbar items]objectAtIndex:1]setEnabled:NO];
            [[[contacthometabbar items]objectAtIndex:2]setEnabled:NO];
            contactStatusString=@"red_circle";
            break;
        case 6:
            contactStatusString=@"grey_circle";
            break;
    }
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    UITabBarController *tabController=[self.storyboard instantiateViewControllerWithIdentifier:@"ContactTabbbar"];
    switch (item.tag) {
        case 0:
            
            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",covarageStatus] forKey:@"contactstatus"];
            [tabController setSelectedIndex:0];
            break;
        case 1:
            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",covarageStatus] forKey:@"contactstatus"];
            [tabController setSelectedIndex:1];
            break;
        case 2:
            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",covarageStatus] forKey:@"contactstatus"];
            [tabController setSelectedIndex:2];
            break;
    }
    [self.navigationController pushViewController:tabController animated:YES];
}


-(void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coveragejson" object:nil];
}

@end
