//
//  MessageViewController.m
//  Managers
//
//  Created by sandeepchalla on 10/7/15.
//  Copyright © 2015 theatro. All rights reserved.
//

#import "MessageViewController.h"

#import "GroupsDetailViewController.h"
#import "AudioManager.h"
#import "Constant.h"
#import "Reachability.h"
#import "AlertView.h"
#import "HttpHandler.h"
#import "StateMachineHandler.h"
#import "UIViewController.h"
#import "ViewController.h"
#import "UIViewController.h"
#import "Earboxinboxmodel.h"
#import "MarqueeLabel.h"
#import "DiagnosticTool.h"

@interface MessageViewController ()
{
    NSString *storeNameString,*covarageTagOutName,*staticerrorcode;
    NSURL *messageaudiourl;
    BOOL audioSendButton;
    UIImageView  *contactStatusImage;
    int covarageStatus,messageHeardCount;
    UILabel  *contactPlaceLabel;
    NSMutableArray *request_timemillisecsArray,*messageHeardCountArray;
    UILabel *connectionLabel;
    NSMutableDictionary *pns;
    BOOL audioInterruption;
    NSDictionary *dictLang;
    
}

@end

@implementation MessageViewController
@synthesize deleteButton, playButton, recordPauseButton,sendButton,contactString,contactStatusString,earbox;

static MessageViewController  *sharedMgr = nil;

+(MessageViewController *)getsharedInstance {
    if (sharedMgr == nil)
    {
        sharedMgr = [[MessageViewController alloc] init];
    }
    return sharedMgr;
}

-(void)broadCostButtonAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)audioInterruptionEnd
{
    audioInterruption=YES;
    
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush isEqualToString:@"YES"])
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"StoreListViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        else
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"VPViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        
    }
    else
    {
        if (audioInterruption==YES)
        {
            [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
            recordPauseButton.userInteractionEnabled=YES;
            audioInterruption=NO;
        }
        connectionLabel.hidden=NO;
        [[StateMachineHandler sharedManager]reOpenClosedSockets];
        [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
    }
    
    
}


-(void)coveragejson:(NSNotification *)notification
{
    [playButton setEnabled:YES];
    [deleteButton setEnabled:YES];
    [sendButton setEnabled:YES];
    recordPauseButton.userInteractionEnabled=YES;
    connectionLabel.hidden=YES;
    NSArray *coverageArray=[notification.object objectForKey:@"coverage"];
    for (int i=0; i<[coverageArray count]; i++)
    {
        // getting all covarageTagOutNames from jsonstring
        
        covarageTagOutName=[[[coverageArray objectAtIndex:i]objectForKey:@"name"] componentsSeparatedByString:@"_"][1];
        if ([[covarageTagOutName componentsSeparatedByString:@"|"] count]>1)
        {
            covarageTagOutName=[covarageTagOutName componentsSeparatedByString:@"|"][0];
        }
        if ([covarageTagOutName isEqualToString:contactString])
        {
            covarageStatus=[[[[coverageArray objectAtIndex:i]objectForKey:@"status"] componentsSeparatedByString:@"_"][0] intValue];
            NSString *location=[[[coverageArray objectAtIndex:i]objectForKey:@"status"] componentsSeparatedByString:@"_"][1];
            switch (covarageStatus) {
                case 0:
                    contactStatusString=@"green_circle";
                    break;
                case 1:
                case 2:
                case 3:
                    contactStatusString=@"red_circle";
                    break;
                case 6:
                    location=@"";
                    contactStatusString=@"grey_circle";
                    break;
                case 9:
                    [self userHearedMessage:[notification.object objectForKey:@"tid"]];
                    break;
            }
            contactPlaceLabel.text=location;
            contactStatusImage.image=[UIImage imageNamed:contactStatusString];
            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",covarageStatus] forKey:@"contactstatus"];
            [[NSUserDefaults standardUserDefaults]setObject:location forKey:@"contactlocation"];
        }
    }
    
    
}

-(void)userHearedMessage:(NSString *)millisecStr
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"MessageViewController"])
    {
        for(int i=0;i<[request_timemillisecsArray count];i++)
        {
            if([millisecStr isEqualToString:[request_timemillisecsArray objectAtIndex:i]])
            {
                if ([[millisecStr componentsSeparatedByString:@"_"][1] isEqualToString:@"1"])
                {
                    //receivedMessageLabel.text=[NSString stringWithFormat:@"%@ has heard your message %@",[covarageTagOutName capitalizedString],[messageHeardCountArray objectAtIndex:i]];
                    messageLabel.text=[NSString stringWithFormat:@"%@ %@",[[covarageTagOutName componentsSeparatedByString:@" "][0] capitalizedString],[dictLang objectForKey:@"has heard your message"]];
                }
                else
                {
                    messageLabel.text=[NSString stringWithFormat:@"%@ %@",[[covarageTagOutName componentsSeparatedByString:@" "][0] capitalizedString],[dictLang objectForKey:@"has heard your messages"]];
                }
                
            }
            else
            {
                
            }
            
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAnnouncementsNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"audioNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recievedAnnouncementsNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAnnouncementsSingleTapNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coverageRequestToServertask" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        [[AudioManager getsharedInstance]audioplayerstop];
        [[AudioManager getsharedInstance]recordstop];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
        
    }];
    
    [super viewWillDisappear:animated];
}


-(void)viewWillAppear:(BOOL)animated
{
    
    MarqueeLabel *_lectureName = [[MarqueeLabel alloc] initWithFrame:CGRectMake(100,20,60, 20) duration:8.0 andFadeLength:10.0f];
    [_lectureName setTextAlignment:NSTextAlignmentCenter];
    [_lectureName setBackgroundColor:[UIColor clearColor]];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    NSLog(@"Message view viewWillAppear");
    connectionLabel=[[UILabel alloc]init];
    connectionLabel.textColor=[UIColor whiteColor];
connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
    connectionLabel.frame= CGRectMake(0,self.view.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height);
    connectionLabel.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    connectionLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:connectionLabel];
    connectionLabel.hidden=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playAnnouncement:)
                                                 name:@"playAnnouncementsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendAnnouncement:)
                                                 name:@"audioNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedAnnouncement:)
                                                 name:@"recievedAnnouncementsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(buttonSingleTap)
                                                 name:@"playAnnouncementsSingleTapNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coverageRequestToServertask:)
                                                 name:@"coverageRequestToServertask"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    messageHeardCount=0;
    request_timemillisecsArray=[[NSMutableArray alloc]init];
    messageHeardCountArray=[[NSMutableArray alloc]init];
    messageLabel.text=@"";
    messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    messageLabel.numberOfLines = 0;
    covarageStatus=[[[NSUserDefaults standardUserDefaults]objectForKey:@"contactstatus"] intValue];
    [self intialView];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = kNavBackgroundColor;
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
    if ([str isEqualToString:@"store"])
    {
        titleLabel.text=[NSString stringWithFormat:@"%@ %@",[dictLang objectForKey:@"Message to"],[[[NSUserDefaults standardUserDefaults]objectForKey:@"contactname"]capitalizedString]];
        titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
    }
    else
    {
    titleLabel.text = [[[NSUserDefaults standardUserDefaults]objectForKey:@"contactname"]capitalizedString];
    }
    //titleLabel.textAlignment=NSTextAlignmentCenter;
    [titleLabel sizeToFit];
   
   
    contactString=[[NSUserDefaults standardUserDefaults]objectForKey:@"contactname"];
    
    
    contactPlaceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 18, self.view.frame.size.width-150, 20)];
    contactPlaceLabel.backgroundColor = [UIColor clearColor];
    contactPlaceLabel.textColor = kNavBackgroundColor;
    contactPlaceLabel.font = [UIFont systemFontOfSize:12];
    
    contactPlaceLabel.text =[[NSUserDefaults standardUserDefaults]objectForKey:@"contactlocation"];
    
    contactPlaceLabel.textAlignment=NSTextAlignmentCenter;
    [contactPlaceLabel sizeToFit];
    
    UIView *navigationTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-160, 30)];
   
    contactStatusImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    float widthDiff = contactPlaceLabel.frame.size.width - titleLabel.frame.size.width;
    CGRect titleframe = titleLabel.frame;
    CGRect subtitleframe = contactPlaceLabel.frame;
    if (widthDiff > 0)
    {
        if ([str isEqualToString:@"store"])
        {
             titleLabel.frame =CGRectMake(((self.view.frame.size.width-90)/2)-titleframe.size.width/2, titleframe.origin.y, self.view.frame.size.width-100, titleframe.size.height);
            contactPlaceLabel.frame = CGRectMake(((self.view.frame.size.width-90)/2)-subtitleframe.size.width/2, subtitleframe.origin.y, self.view.frame.size.width, subtitleframe.size.height);
             contactStatusImage.frame=CGRectMake(((self.view.frame.size.width-90)/2)-(titleframe.size.width/2)-20,05,12,12);
        }
        else
        {
        titleLabel.frame =CGRectMake(((self.view.frame.size.width-100)/2)-titleframe.size.width/2, titleframe.origin.y, self.view.frame.size.width, titleframe.size.height); //CGRectIntegral(titleframe);
            subtitleframe.size.width=self.view.frame.size.width-200;
            subtitleframe.origin.x=20;
            contactPlaceLabel.frame = CGRectIntegral(subtitleframe);
      //  contactPlaceLabel.frame = CGRectMake(((self.view.frame.size.width-120)/2)-subtitleframe.size.width/2, subtitleframe.origin.y, self.view.frame.size.width, subtitleframe.size.height);
             contactStatusImage.frame=CGRectMake(((self.view.frame.size.width-90)/2)-(titleframe.size.width/2)-20,05,12,12);
        }
        
    }
    else{
        if ([str isEqualToString:@"store"])
        {
//             titleLabel.frame =CGRectMake(((self.view.frame.size.width-90)/2)-titleframe.size.width/2, titleframe.origin.y, self.view.frame.size.width-100, titleframe.size.height);
//            subtitleframe.size.width=self.view.frame.size.width-200;
//            subtitleframe.origin.x=20;
//            contactPlaceLabel.frame = CGRectIntegral(subtitleframe);
//
//             contactStatusImage.frame=CGRectMake(((self.view.frame.size.width-80)/2)-(titleframe.size.width/2)-20,05,12,12);
        }
        else
        {
        titleLabel.frame =CGRectMake(((self.view.frame.size.width-100)/2)-titleframe.size.width/2, titleframe.origin.y, self.view.frame.size.width-100, titleframe.size.height);
        subtitleframe.size.width=self.view.frame.size.width-200;
        subtitleframe.origin.x=20;
        contactPlaceLabel.frame = CGRectIntegral(subtitleframe);
             contactStatusImage.frame=CGRectMake(((self.view.frame.size.width-90)/2)-(titleframe.size.width/2)-20,05,12,12);
        }
    }
    contactStatusImage.image=[UIImage imageNamed:contactStatusString];
    
    
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = contactStatusImage.image;
    

    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"   %@",titleLabel.text]];
    
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.image = contactStatusImage.image;
    
    NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
    
    [attributedString replaceCharactersInRange:NSMakeRange(1, 1) withAttributedString:attrStringWithImage];
    
    _lectureName.attributedText = attributedString;
    
    _lectureName.adjustsFontSizeToFitWidth=NO;
    [_lectureName setAnimationCurve:UIViewAnimationOptionCurveEaseIn];
    _lectureName.marqueeType = MLContinuous;
    
    
    
    if (earbox==YES)
    {
        [self.navigationItem setTitleView:_lectureName];
    }
    else
    {
        
        [navigationTitleView addSubview:titleLabel];
        [navigationTitleView addSubview:contactPlaceLabel];
        [navigationTitleView addSubview:contactStatusImage];
        self.parentViewController. navigationItem.titleView=navigationTitleView;
    }
    
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:NO];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:NO];
    messageLabel.text=@"";
    
    [super viewWillAppear:animated];
}


-(void)viewDidLoad
{
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setTitle:[dictLang objectForKey:@"Message"]];
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setTitle:[dictLang objectForKey:@"One-to-One"]];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setTitle:[dictLang objectForKey:@"Interrupt"]];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    [deleteButton setTitle:[dictLang objectForKey:@"Delete"] forState:UIControlStateNormal];
    [sendButton setTitle:[dictLang objectForKey:@"Send"] forState:UIControlStateNormal];
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coveragejson:)
                                                 name:@"coveragejson"
                                               object:nil];
    storeNameString=[[NSUserDefaults standardUserDefaults]objectForKey:@"storeNameString"];
    
    playButton.layer.cornerRadius=8;
    deleteButton.layer.cornerRadius=8;
    sendButton.layer.cornerRadius=8;
    playButton.backgroundColor=kGreyColor;
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    [self buttonsDisable];
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    [super viewDidLoad];
    [recordPauseButton addTarget:self action:@selector(buttondDownPressed:) forControlEvents: UIControlEventTouchDown];
    [recordPauseButton addTarget:self action:@selector(buttonUpPressed:) forControlEvents: UIControlEventTouchUpInside];
    [recordPauseButton addTarget:self action:@selector(buttonDragOutside:) forControlEvents: UIControlEventTouchDragOutside];
    [[AudioManager getsharedInstance] path];
    
    
}

-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"MessageViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [ self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void)coverageRequestToServertask:(NSNotification *)notification
{
    // Network messages getting from native c2 and updating to UI
    [self connectionRequest];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"texttime"])
    {
        if ([notification.object isEqualToString:@"server connection lost..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"server connection lost..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"could not start a connection..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"could not start a connection..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not logon to store..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not logon to store..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not connect to Proxy server..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not connect to Proxy server..."],staticerrorcode];
        }
        else
        {
            //Second time login ->please wait
            connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
        }
        
    }
    else
    {
        //First time login ,connecting to server->fetching emp list
        connectionLabel.text=notification.object;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"texttime"];
    }
    
    
}

-(void)tgsAlarmerror:(NSString *)tgsLoginerror
{
    staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:@"MessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"TgsLogin"],[[DiagnosticTool sharedManager]generateErrorCode:tgsLoginerror]];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        [HttpHandler alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"TgsLogin"] severity:@"1" alarmdesc:staticerrorcode];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
    }];
}

-(void)connectionRequest
{
    [playButton setEnabled:NO];
    [deleteButton setEnabled:NO];
    [sendButton setEnabled:NO];
    recordPauseButton.userInteractionEnabled=NO;
    connectionLabel.hidden=NO;
}

-(void)intialView
{
    switch (covarageStatus)
    {
        case 0:
            contactStatusString=@"green_circle";
            break;
        case 1:
        case 2:
        case 3:
            contactStatusString=@"red_circle";
            break;
            
        case 6:
            contactStatusString=@"grey_circle";
            break;
    }
}

-(void)buttonSingleTap
{
    playButton.backgroundColor=kGreyColor;
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    
    [self buttonsDisable];
    messageLabel.text=@"";
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    
}


-(void)buttonsDisable
{
    if (playButton.isEnabled)
    {
        [playButton setEnabled:NO];
    }
    if (deleteButton.isEnabled)
    {
        [deleteButton setEnabled:NO];
    }
    if (sendButton.isEnabled)
    {
        [sendButton setEnabled:NO];
    }
}

-(void)playAnnouncement:(NSNotification *)sender
{
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    messageLabel.text=@"";
    deleteButton.backgroundColor=kNavBackgroundColor;
    [deleteButton setEnabled:YES];
    
    if (audioSendButton==YES)
    {
        sendButton.backgroundColor=kGreyColor;
        [sendButton setEnabled:NO];
        
    }
    else
    {
        messageLabel.text=[dictLang objectForKey:@"Ready to send"];
        sendButton.backgroundColor=kNavBackgroundColor;
        [sendButton setEnabled:YES];
        
    }
    
}


-(void)sendAnnouncement:(NSNotification *)sender
{
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    self.navigationController.navigationBar.tintColor = kGreyColor;
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"MessageViewController"])
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"MessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
            
        }
        else
        {
            messageaudiourl=sender.object;
            if ([[AudioManager getsharedInstance]getAudioduration:messageaudiourl]<1.0)
            {
                [AlertView alert:nil message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Audio length is too short to send"],[[DiagnosticTool sharedManager] generateScreenCode:@"MessageVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioFileSendV1"],[[DiagnosticTool sharedManager]generateErrorCode:@"Audio length is too short to send"]]];
                messageLabel.text=@"";
                recordPauseButton.userInteractionEnabled=YES;
                [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
                self.navigationController.navigationBar.userInteractionEnabled = YES;
                self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
            }
            else
            {
            NSString *millisec=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ];
            NSArray *tempArray = [millisec componentsSeparatedByString:@"."];
            messageHeardCount++;
            millisec = [NSString stringWithFormat:@"%@_%d",[tempArray objectAtIndex:0],messageHeardCount];
          
            NSData *file1Data = [[NSData alloc] initWithContentsOfURL:sender.object];
            [request_timemillisecsArray addObject:millisec];
            [messageHeardCountArray addObject:[NSNumber numberWithInt:messageHeardCount]];
            NSString *macId;
            if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
                macId=[[NSString stringWithFormat:@"%@",
                        [[[[UIDevice currentDevice] identifierForVendor] UUIDString]substringFromIndex:24]]lowercaseString];
            }
            pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",contactString,@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:messageaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithDouble:4],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
            NSDictionary  *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
            NSDictionary *whoDic=[NSDictionary dictionaryWithObjectsAndKeys:@[contactString],@"users",[NSArray arrayWithObject:storeNameString],@"store", [NSArray arrayWithObject:[loginDetails objectForKey:@"chainName"]],@"chain",@"managerApp",@"originatorType",@"message",@"type",macId,@"macId",millisec,@"request_time",@[@""],@"groups",nil];
            [HttpHandler audioFileSend:whoDic audioFile:file1Data pns:pns userScreen:@"MessageVC"];
            
        }
        }
    }
    
    
}


-(void)recievedAnnouncement:(NSNotification *)sender
{
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"MessageViewController"])
    {
        recordPauseButton.userInteractionEnabled=YES;
        [playButton setEnabled:YES];
        [deleteButton setEnabled:YES];
        playButton.backgroundColor=kNavBackgroundColor;
        deleteButton.backgroundColor=kNavBackgroundColor;
        [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
        NSString *millisec=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ];
        NSArray *tempArray = [millisec componentsSeparatedByString:@"."];
        pns=[NSMutableDictionary dictionaryWithObjectsAndKeys:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"]objectForKey:@"tagOutName"],@"from",contactString,@"to",[NSNumber numberWithDouble:[[tempArray objectAtIndex:0]doubleValue]],@"callStartTime",[NSNumber numberWithDouble:[[AudioManager getsharedInstance]getAudioduration:messageaudiourl]],@"duration",@"medium",@"priority",[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"],@"callTxId" ,[NSNumber numberWithDouble:4],@"callType",[NSNumber numberWithDouble:0],@"ttl",nil];
        
        if ([[sender.object objectAtIndex:0] isEqualToString:@"error"])
        {
             messageLabel.text=[dictLang objectForKey:@"Message not sent"];
            sendButton.backgroundColor=kNavBackgroundColor;
            [sendButton setEnabled:YES];
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
            
        }
        else if ([[sender.object objectAtIndex:0] isEqualToString:@"message"])
        {
            messageLabel.text=[dictLang objectForKey:@"Message Sent"];
            audioSendButton=YES;
            sendButton.backgroundColor=kGreyColor;
            
            [sendButton setEnabled:NO];
            [pns setObject:storeNameString forKey:@"storeName"];
            [pns setObject:@"HEARD" forKey:@"status"];
            [pns setObject:[NSNumber numberWithInt:1] forKey:@"maCallType"];
            [pns setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] forKey:@"storeId"];
            //[pns setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"calltxid"] forKey:@"pushTxId"];
            if ([sender.object count]==2)
            {
                [pns setObject:[sender.object objectAtIndex:1] forKey:@"pushTxId"];
            }
            if ([pns count]>0)
            {
                [[Earboxinboxmodel sharedModel]messagesentSave:[NSArray arrayWithObject:pns]];
            }
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
            
        }
    }
    
}

- (void)buttondDownPressed:(UIButton *)sender
{
    [self buttonsDisable];
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    playButton.backgroundColor=kGreyColor;
    [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    messageLabel.text=[dictLang objectForKey:@"Recording..."];
    receivedMessageLabel.text=@"";
    [[AudioManager getsharedInstance] recordPauseTapped];
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue_white.png"] forState:UIControlStateNormal];
}



- (void)buttonUpPressed:(UIButton *)sender {
    messageLabel.text=[dictLang objectForKey:@"Ready to send"];
    audioSendButton=NO;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
    [playButton setEnabled:YES];
    [deleteButton setEnabled:YES];
    [sendButton setEnabled:YES];
    deleteButton.backgroundColor=kNavBackgroundColor;
    sendButton.backgroundColor=kNavBackgroundColor;
    playButton.backgroundColor=kNavBackgroundColor;
    [[AudioManager getsharedInstance] stopTapped];
}

-(void)buttonDragOutside:(UIButton *)sender
{
    messageLabel.text=[dictLang objectForKey:@"Ready to send"];
    audioSendButton=NO;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue.png"] forState:UIControlStateNormal];
    [playButton setEnabled:YES];
    [deleteButton setEnabled:YES];
    [sendButton setEnabled:YES];
    deleteButton.backgroundColor=kNavBackgroundColor;
    sendButton.backgroundColor=kNavBackgroundColor;
    playButton.backgroundColor=kNavBackgroundColor;
    [[AudioManager getsharedInstance] stopTapped];
}

- (IBAction)playTapped:(UIButton *)sender {
    if ([playButton.titleLabel.text isEqualToString:[dictLang objectForKey:@"Stop"]])
    {
        if (audioSendButton==YES)
        {
            [deleteButton setEnabled:YES];
            [sendButton setEnabled:NO];
            deleteButton.backgroundColor=kNavBackgroundColor;
            sendButton.backgroundColor=kGreyColor;
            messageLabel.text=@"";
            [[AudioManager getsharedInstance]audioplayerstop];
            [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
        }
        else
        {
            [deleteButton setEnabled:YES];
            [sendButton setEnabled:YES];
            deleteButton.backgroundColor=kNavBackgroundColor;
            sendButton.backgroundColor=kNavBackgroundColor;
            messageLabel.text=@"";
            [[AudioManager getsharedInstance]audioplayerstop];
            [playButton setTitle:[dictLang objectForKey:@"Replay"] forState:UIControlStateNormal];
        }
    }
    else
    {
        [playButton setTitle:[dictLang objectForKey:@"Stop"] forState:UIControlStateNormal];
        [deleteButton setEnabled:NO];
        [sendButton setEnabled:NO];
        deleteButton.backgroundColor=kGreyColor;
        sendButton.backgroundColor=kGreyColor;
        messageLabel.text=[dictLang objectForKey:@"Replaying..."];
        receivedMessageLabel.text=@"";
        [[AudioManager getsharedInstance] playTapped];
    }
}


- (IBAction)sendFileTapped:(id)sender
{
    
    [deleteButton setEnabled:NO];
    [sendButton setEnabled:NO];
    [playButton setEnabled:NO];
    playButton.backgroundColor=[UIColor grayColor];
    deleteButton.backgroundColor=[UIColor grayColor];
    sendButton.backgroundColor=[UIColor grayColor];
    recordPauseButton.userInteractionEnabled=NO;
    messageLabel.text=[dictLang objectForKey:@"Sending..."];
    receivedMessageLabel.text=@"";
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_gray.png"] forState:UIControlStateNormal];
    [[AudioManager getsharedInstance] sendTapped];
    
}

- (IBAction)deleteFileTapped:(id)sender
{
    messageLabel.text=[dictLang objectForKey:@"Message deleted"];
    receivedMessageLabel.text=@"";
    deleteButton.backgroundColor=[UIColor grayColor];
    sendButton.backgroundColor=[UIColor grayColor];
    playButton.backgroundColor=[UIColor grayColor];
    [sendButton setEnabled:NO];
    [playButton setEnabled:NO];
    [deleteButton setEnabled:NO];
    [[AudioManager getsharedInstance] deleteTapped];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coveragejson" object:nil];
}


@end
