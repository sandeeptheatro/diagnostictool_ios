//
//  InterruptViewController.h
//  Managers
//
//  Created by sandeepchalla on 10/7/15.
//  Copyright © 2015 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterruptViewController : UIViewController
{
     IBOutlet UILabel *lbl_Green,*lbl_Msg;
}
@property(nonatomic,strong)NSString *contactString,*contactStatusString;
@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *endButton;
- (IBAction)endButtonTapped:(id)sender;
@end
