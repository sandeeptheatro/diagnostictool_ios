//
//  FavouritesViewController.m
//  Managers
//
//  Created by sandeepchalla on 1/11/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "FavouritesViewController.h"
#import "HttpHandler.h"
#import "ContactsList.h"
#import "UIViewController.h"
#import "Reachability.h"
#import "AlertView.h"
#import "Constant.h"
#import "StateMachineHandler.h"
#import "ContactsList.h"
#import "FavouritesAddViewController.h"
#import "ContactHomeViewController.h"
#import "BroadcastViewController.h"
#import "AlertView.h"
#import "ActivityIndicatorView.h"
#import "DiagnosticTool.h"

@interface FavouritesViewController ()
{
    NSMutableArray *favouritestagoutArray,*favouritesdeleteArray,*favouritesidArray,*favouritesArray;
    NSDictionary *loginDetails;
    UILabel *favouritesLabel;
    int storeId,companyId;
    ContactsList *contactsList;
    UIBarButtonItem *favouritesAddbutton;
    UILabel *connectionLabel;
    NSDictionary *dictLang;
    NSString *staticerrorcode;
}


@end

@implementation FavouritesViewController
@synthesize NewContactsListArray,modString;



-(void)coverageRequestToServertask:(NSNotification *)notification
{
    // Network messages getting from native c2 and updating to UI
    [self connectionRequest];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"texttime"])
    {
        if ([notification.object isEqualToString:@"server connection lost..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"server connection lost..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"could not start a connection..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"could not start a connection..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not logon to store..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not logon to store..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not connect to Proxy server..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not connect to Proxy server..."],staticerrorcode];
        }
        else
        {
            //Second time login ->please wait
            connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
        }
        
    }
    else
    {
        //First time login ,connecting to server->fetching emp list
        connectionLabel.text=notification.object;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"texttime"];
    }
    
    
}


-(void)tgsAlarmerror:(NSString *)tgsLoginerror
{
    staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:@"FavoritesVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"TgsLogin"],[[DiagnosticTool sharedManager]generateErrorCode:tgsLoginerror]];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        [HttpHandler alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"TgsLogin"] severity:@"1" alarmdesc:staticerrorcode];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
    }];
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)connectionRequest
{
    favouritesViewTableview.userInteractionEnabled=NO;
    connectionLabel.hidden=NO;
    self.navigationItem.rightBarButtonItem.enabled=NO;
}

-(void)coveragejson:(NSNotification *)notification
{
    favouritesViewTableview.userInteractionEnabled=YES;
    connectionLabel.hidden=YES;
    self.navigationItem.rightBarButtonItem.enabled=YES;
    // 7 for add  and 8 for delete
    
    //Coverage jsonstring getting from ntive c2.
    
    NSArray *coverageArray=[notification.object objectForKey:@"coverage"];
    
    NSMutableArray *fullCoverageArray=[[NSMutableArray alloc]init];
    for (int i=0; i<[coverageArray count]; i++)
    {
        // getting all covarageTagOutNames from jsonstring
        
        NSString *covarageTagOutName=[[[coverageArray objectAtIndex:i]objectForKey:@"name"] componentsSeparatedByString:@"_"][1];
        
        [fullCoverageArray addObject:covarageTagOutName];
        
    }
    
    for (int i=0; i<[favouritesArray count]; i++)
    {
        
        for (int j=0; j<[fullCoverageArray count]; j++)
        {
            NSDictionary *dic=[favouritesArray objectAtIndex:i];
            
            // Checking tagoutname in coverage jsonstring
            
            id tagOutName=[NSString stringWithFormat:@"%@",[dic objectForKey:@"Name"]];
            id coverageName=[NSString stringWithFormat:@"%@",[fullCoverageArray objectAtIndex:j]];
            
            if ([[coverageName componentsSeparatedByString:@"|"] count]>1)
            {
                modString=[coverageName componentsSeparatedByString:@"|"][1];
                coverageName=[coverageName componentsSeparatedByString:@"|"][0];
                
            }
            if([tagOutName isEqualToString:coverageName])
            {
                if([[fullCoverageArray objectAtIndex:j] isEqualToString:[loginDetails objectForKey:@"twUserId"]])
                {
                    
                }
                else
                {
                    
                    // getting all covaragestatus from jsonstring
                    
                    int status=[[[[coverageArray objectAtIndex:j]objectForKey:@"status"] componentsSeparatedByString:@"_"][0] intValue];
                    
                    // getting all covaraglocations from jsonstring
                    NSString *location=[[[coverageArray objectAtIndex:j]objectForKey:@"status"] componentsSeparatedByString:@"_"][1];
                    NSDictionary *dic=[favouritesArray objectAtIndex:i];
                    NSMutableDictionary *replacedic=[[NSMutableDictionary alloc]init];
                    [replacedic setObject:[NSString stringWithFormat:@"%d",status] forKey:@"Status"];
                    if (status==6)
                    {
                        [replacedic setObject:@"" forKey:@"location"];
                    }
                    else
                    {
                        [replacedic setObject:location forKey:@"location"];
                    }
                    [replacedic setObject:[[coverageArray objectAtIndex:j]objectForKey:@"ct"] forKey:@"manageriCon"];
                    [replacedic setObject:[dic objectForKey:@"empid"] forKey:@"empid"];
                    [replacedic setObject:[dic objectForKey:@"Name"] forKey:@"Name"];
                    [favouritesArray replaceObjectAtIndex:i withObject:replacedic];
                    
                }
            }
            else
            {
                //tagoutname not avaliable in coverage jsonstring
                
            }
            
        }
        
    }
    
    // Reloading view
    
    [favouritesViewTableview reloadData];
    
}

-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"FavouritesViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [ self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coverageRequestToServertask" object:nil];
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    NSLog(@"Favorites viewWillAppear");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coverageRequestToServertask:)
                                                 name:@"coverageRequestToServertask"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:@"localnotification" object:nil];
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    self.searchDisplayController.searchBar.placeholder=[dictLang objectForKey:@"Search"];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }    connectionLabel=[[UILabel alloc]init];
    connectionLabel.textColor=[UIColor whiteColor];
    connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
    connectionLabel.frame= CGRectMake(0,self.view.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height);
    connectionLabel.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    connectionLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:connectionLabel];
    connectionLabel.hidden=YES;
    favouritesViewTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coveragejson:)
                                                 name:@"coveragejson"
                                               object:nil];
    self.navigationItem.title=[dictLang objectForKey:@"Favorites"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kNavBackgroundColor}];
    favouritesLabel=[[UILabel alloc]init];
    favouritesLabel.text=@"";
    favouritesLabel.textColor=[UIColor darkGrayColor];
    favouritesLabel.frame= CGRectMake(0,-70,self.view.frame.size.width,self.view.frame.size.height);
    favouritesLabel.font=[UIFont fontWithName:kFontName size:23.0];
    favouritesLabel.textAlignment=NSTextAlignmentCenter;
    [favouritesViewTableview addSubview:favouritesLabel];
    
    favouritesAddbutton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"plus.png"] style:UIBarButtonItemStyleDone target:self action:@selector(favouritesAddbuttonAction)];
    self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
    self.navigationItem.rightBarButtonItems=@[favouritesAddbutton];
    favouritesAddbutton.enabled=NO;
    favouritestagoutArray=[[NSMutableArray alloc]init];
    favouritesdeleteArray=[[NSMutableArray alloc]init];
    favouritesidArray=[[NSMutableArray alloc]init];
    [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
    storeId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"]intValue];
    companyId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"companyId"]intValue];
    loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    favouritesArray=[[NSMutableArray alloc]init];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"FavoritesVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Favoritelist"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        NSDictionary *twFaviouritesListdic= [HttpHandler favoritelist:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId storeId:storeId userScreen:@"FavoritesVC"];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            if ([[twFaviouritesListdic objectForKey:@"mAppFavorites"]isKindOfClass:[NSNull class]] || [twFaviouritesListdic objectForKey:@"Error"]!=nil)
            {
                if( [twFaviouritesListdic objectForKey:@"Error"]!=nil)
                {
                    [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@ (%@i)",[twFaviouritesListdic objectForKey:@"Error"],[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                }
                else
                {
                    favouritesAddbutton.enabled=YES;
                    favouritesLabel.text=[dictLang objectForKey:@"No Favorites"];
                    
                }
                
                
            }
            else
            {
                NSArray *twFaviouritesListArray=[[twFaviouritesListdic objectForKey:@"mAppFavorites"]objectForKey:@"favoriteUserList"];
                for (int i=0;i<[twFaviouritesListArray count];i++)
                {
                    if ([favouritestagoutArray count]<10)
                    {
                        [favouritestagoutArray addObject:[[twFaviouritesListArray objectAtIndex:i]objectForKey:@"tagoutName"]];
                        [favouritesidArray addObject:[[twFaviouritesListArray objectAtIndex:i]objectForKey:@"employeeId"]];
                        NSMutableDictionary *favDic=[[NSMutableDictionary alloc]init];
                        [favDic setObject:[[twFaviouritesListArray objectAtIndex:i]objectForKey:@"tagoutName"] forKey:@"Name"];
                        [favDic setObject:[[twFaviouritesListArray objectAtIndex:i]objectForKey:@"employeeId"] forKey:@"empid"];
                        [favDic setObject:@"6" forKey:@"Status"];
                        [favDic setObject:@"" forKey:@"location"];
                        [favDic setObject:@"1" forKey:@"manageriCon"];
                        [favouritesArray addObject:favDic];
                    }
                    
                }
                
                favouritesAddbutton.enabled=YES;
                
            }
            [self favourietablelist];
            
            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
        }];
    }];
    
    }
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



-(void)favourietablelist
{
    favouritesArray=[[NSMutableArray alloc]init];
    if ([favouritestagoutArray count]>0)
    {
        for (int j=0; j<[favouritestagoutArray count]; j++)
        {
            
            for (int i=0; i<[NewContactsListArray count]; i++)
            {
                if ([[[NewContactsListArray objectAtIndex:i]objectForKey:@"Name"] isEqualToString:[favouritestagoutArray objectAtIndex:j]])
                {
                    [favouritesArray addObject:[NewContactsListArray objectAtIndex:i]];
                }
            }
        }
    }
    if ([favouritesArray count]>0)
    {
        [favouritesViewTableview reloadData];
        favouritesLabel.text=@"";
    }
    else
    {
        favouritesLabel.text=[dictLang objectForKey:@"No Favorites"];
    }
    
}



-(void)FavouritesArrayDelegateMethod:(NSString *)names employeeid:(int)employeeid status:(NSString *)status location:(NSString *)location manageriCon:(NSString *)manageriCon
{
    favouritesdeleteArray=[[NSMutableArray alloc]init];
    NSMutableDictionary *favDic=[[NSMutableDictionary alloc]init];
    [favDic setObject:names forKey:@"Name"];
    [favDic setObject:[NSNumber numberWithInt:employeeid] forKey:@"empid"];
    [favDic setObject:status forKey:@"Status"];
    [favDic setObject:location forKey:@"location"];
    [favDic setObject:manageriCon forKey:@"manageriCon"];
    if ([[favouritesArray valueForKey:@"Name"] containsObject:[favDic objectForKey:@"Name"]])
    {
        
    }
    else
    {
        [favouritesArray addObject:favDic];
        [favouritesidArray addObject:[NSNumber numberWithInt:employeeid]];
    }
    if ([favouritesArray count]<10)
    {
        favouritesLabel.text=@"";
    }
    [favouritesViewTableview reloadData];
    [self favourietsAdd];
    
}

-(void)favouritesAddbuttonAction
{
    if ([favouritesArray count]<10)
    {
        FavouritesAddViewController *favouritesVC=[self.storyboard instantiateViewControllerWithIdentifier:@"FavouritesAddViewController"];
        favouritesVC.delegate=self;
        favouritesVC.newContactsListArray=NewContactsListArray;
        favouritesVC.modString=modString;
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:favouritesVC];
        [self presentViewController:nav animated:YES completion:nil];
    }
    else
    {
        [AlertView alert:[dictLang objectForKey:@"Favorites list is full!"] message:[dictLang objectForKey:@"Add up to 10 favorites to the list. To add another favorite, you must first delete a favorite from the list."]];
    }
    
}

-(void)favourietsAdd
{
    [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        [[HttpHandler favoriteAdd:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId storeId:storeId addFavoriteUsers:favouritesidArray userScreen:@"FavoritesVC"]objectForKey:@"favoriteUserList"];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [ActivityIndicatorView activityIndicatorViewRemove:self.view];
            
        }];
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [favouritesArray count];
    return 0;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"cell";
    UITableViewCell *cell;
    UIImageView *managersView;
    UIButton *modView;
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle
                reuseIdentifier:cellIdentifier];
        managersView=[[UIImageView alloc]initWithFrame:CGRectMake(240, 10, 30, 30)];
        modView=[UIButton buttonWithType:UIButtonTypeCustom];
        modView.titleLabel.font = [UIFont fontWithName:kFontName size:13];
        [modView setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        modView .frame=CGRectMake(230, 10, 50, 30);
        
        [cell.contentView addSubview:managersView];
        [cell.contentView addSubview:modView];;
    }
    if ([favouritesLabel.text isEqualToString:@""])
    {
        favouritesViewTableview.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    else
    {
        favouritesViewTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    cell.textLabel.font=[UIFont fontWithName:kFontName size:kFontSize];
    cell.detailTextLabel.textColor=kNavBackgroundColor;
    cell.textLabel.textColor=kNavBackgroundColor;
    contactsList=[[ContactsList alloc]init];
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"Name" ascending:YES];
    [favouritesArray sortUsingDescriptors:@[descriptor]];
    
    // Sorting Statusicon based upon status
    
    NSSortDescriptor *descriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"Status" ascending:YES];
    [favouritesArray sortUsingDescriptors:@[descriptor1]];
    
    
    for (int idx = 0;idx<[favouritesArray count];idx++) {
        NSDictionary *dict = [favouritesArray objectAtIndex:idx];
        
        if ([[dict objectForKey:@"manageriCon" ] intValue]==3)
        {
            if ([[dict objectForKey:@"manageriCon" ] intValue]==3 && ([[dict objectForKey:@"Status" ] intValue]==6))
            {
                
            }
            else
            {
                [favouritesArray removeObjectAtIndex:idx];
                [favouritesArray insertObject:dict atIndex:0];
            }
        }
        
    }
    cell.textLabel.text=[[[favouritesArray objectAtIndex:indexPath.row] objectForKey:@"Name"]capitalizedString];
    cell.detailTextLabel.text=[[favouritesArray objectAtIndex:indexPath.row] objectForKey:@"location"];
    int status =[[[favouritesArray objectAtIndex:indexPath.row] objectForKey:@"Status"]intValue];
    
    switch (status) {
        case 0:
            contactsList.status=@"green_circle.png";
            
            break;
            
        case 1:
            contactsList.status=@"red_circle.png";
            
        case 2:
            contactsList.status=@"red_circle.png";
            
        case 3:
            contactsList.status=@"red_circle.png";
            
            break;
        case 6:
            contactsList.status=@"grey_circle.png";
            
            break;
        default:
            // contactsList.status=@"grey_circle.png";
            break;
    }
    
    if ([[[favouritesArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"]intValue] == 2)
        
    {
        managersView.image=[UIImage imageNamed:@"manager_icon.png"];
        
        
    }
    if ([[[favouritesArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"]intValue] == 3)
    {
        
        modView.layer.cornerRadius=7;
        modView.layer.borderWidth=3;
        modView.layer.borderColor=kNavBackgroundColor.CGColor;
        [modView setTitle:modString forState:UIControlStateNormal];
        
    }
    if(status==6)
    {
        modView.hidden=YES;
    }
    cell.imageView.image=[UIImage imageNamed:contactsList.status];
    
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle=UITableViewCellEditingStyleNone;
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ContactHomeViewController *contactsDetailVC=[self.storyboard instantiateViewControllerWithIdentifier:@"ContactHomeViewController"];
    [[NSUserDefaults standardUserDefaults]setObject:[[favouritesArray objectAtIndex:indexPath.row]objectForKey:@"Name"] forKey:@"contactname"];
    [[NSUserDefaults standardUserDefaults]setObject:[[favouritesArray objectAtIndex:indexPath.row]objectForKey:@"location"] forKey:@"contactlocation"];
    [[NSUserDefaults standardUserDefaults]setObject:[[favouritesArray objectAtIndex:indexPath.row]objectForKey:@"manageriCon"] forKey:@"contactmanagericon"];
    [[NSUserDefaults standardUserDefaults]setObject:[[favouritesArray objectAtIndex:indexPath.row]objectForKey:@"Status"] forKey:@"contactstatus"];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"]
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    [self.navigationController pushViewController:contactsDetailVC animated:YES];
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[tableView beginUpdates];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Do whatever data deletion you need to do...
        // Delete the row from the data source
        favouritesidArray=[[NSMutableArray alloc]init];
        [favouritesArray removeObjectAtIndex:indexPath.row];
        for (int i=0; i<[favouritesArray count]; i++)
        {
            [favouritesidArray addObject:[[favouritesArray objectAtIndex:i]objectForKey:@"empid"]];
        }
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationFade];
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            
            [[HttpHandler favoriteAdd:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId storeId:storeId addFavoriteUsers:favouritesidArray userScreen:@"FavoritesVC"]objectForKey:@"favoriteUserList"];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                // [tableView endUpdates];
                if ([favouritesArray count]==0)
                {
                    favouritesViewTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
                    favouritesLabel.text=[dictLang objectForKey:@"No Favorites"];;
                }
                
                [favouritesViewTableview reloadData];
                
            }];
        }];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [dictLang objectForKey:@"Delete"];
}

-(void)reOpenClosedSocketsNotifiacation
{
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"BroadcastViewController"])
    {
        
    }
    else
    {
        NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
        NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
        if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
        {
            if ([fromPush isEqualToString:@"YES"])
            {
                NSArray *viewcontrollers=[self.navigationController viewControllers];
                for (int i=0;i<[viewcontrollers count];i++)
                {
                    UIViewController *vc=[viewcontrollers objectAtIndex:i];
                    NSString *currentView=NSStringFromClass([vc class]);
                    if ([currentView isEqualToString:@"StoreListViewController"])
                    {
                        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                    }
                }
            }
            else
            {
                NSArray *viewcontrollers=[self.navigationController viewControllers];
                for (int i=0;i<[viewcontrollers count];i++)
                {
                    UIViewController *vc=[viewcontrollers objectAtIndex:i];
                    NSString *currentView=NSStringFromClass([vc class]);
                    if ([currentView isEqualToString:@"VPViewController"])
                    {
                        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                    }
                }
            }
            
        }
        
        else
        {
            connectionLabel.hidden=NO;
            [[StateMachineHandler sharedManager]reOpenClosedSockets];
            [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
        }
    }
    
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coveragejson" object:nil];
}

@end
