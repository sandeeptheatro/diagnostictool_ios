//
//  FavouritesViewController.m
//  Managers
//
//  Created by sandeepchalla on 1/11/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "FavouritesAddViewController.h"
#import "HttpHandler.h"
#import "ContactsList.h"
#import "UIViewController.h"
#import "Reachability.h"
#import "AlertView.h"
#import "Constant.h"
#import "StateMachineHandler.h"
#import "DiagnosticTool.h"

@interface FavouritesAddViewController ()
{
    NSMutableArray *contactnamesArray,*contactsearchResultsfullArray,*favouritestagoutArray,*favouritesidArray,*contactsearchResultsAll;
    ContactsList *contactsList;
    NSDictionary *loginDetails;
    int storeId,companyId;
    UILabel *connectionLabel;
    NSDictionary *dictLang;
    NSString *staticerrorcode;
}

@end

@implementation FavouritesAddViewController
@synthesize newContactsListArray,modString;


-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)rightButtonAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"FavouritesViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        
    }
    
    
}


-(void)coverageRequestToServertask:(NSNotification *)notification
{
    // Network messages getting from native c2 and updating to UI
    [self connectionRequest];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"texttime"])
    {
        if ([notification.object isEqualToString:@"server connection lost..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"server connection lost..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"could not start a connection..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"could not start a connection..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not logon to store..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not logon to store..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not connect to Proxy server..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not connect to Proxy server..."],staticerrorcode];
        }
        else
        {
            //Second time login ->please wait
            connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
        }
        
    }
    else
    {
        //First time login ,connecting to server->fetching emp list
        connectionLabel.text=notification.object;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"texttime"];
    }
    
}


-(void)tgsAlarmerror:(NSString *)tgsLoginerror
{
    staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:@"FavoritesAddVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"TgsLogin"],[[DiagnosticTool sharedManager]generateErrorCode:tgsLoginerror]];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        [HttpHandler alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"TgsLogin"] severity:@"1" alarmdesc:staticerrorcode];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
    }];
}


-(void)connectionRequest
{
    favouritesAddTableview.userInteractionEnabled=NO;
    connectionLabel.hidden=NO;
}

-(void)coveragejson:(NSNotification *)notification
{
    favouritesAddTableview.userInteractionEnabled=YES;
    connectionLabel.hidden=YES;
    
    // 7 for add  and 8 for delete
    
    //Coverage jsonstring getting from ntive c2.
    
    NSArray *coverageArray=[notification.object objectForKey:@"coverage"];
    
    NSMutableArray *fullCoverageArray=[[NSMutableArray alloc]init];
    for (int i=0; i<[coverageArray count]; i++)
    {
        NSString *covarageTagOutName=[[[coverageArray objectAtIndex:i]objectForKey:@"name"] componentsSeparatedByString:@"_"][1];
        [fullCoverageArray addObject:covarageTagOutName];
        
    }
    
    for (int i=0; i<[newContactsListArray count]; i++)
    {
        
        for (int j=0; j<[fullCoverageArray count]; j++)
        {
            NSDictionary *dic=[newContactsListArray objectAtIndex:i];
            
            // Checking tagoutname in coverage jsonstring
            
            id tagOutName=[NSString stringWithFormat:@"%@",[dic objectForKey:@"Name"]];
            id coverageName=[NSString stringWithFormat:@"%@",[fullCoverageArray objectAtIndex:j]];
            
            if ([[coverageName componentsSeparatedByString:@"|"] count]>1)
            {
                modString=[coverageName componentsSeparatedByString:@"|"][1];
                coverageName=[coverageName componentsSeparatedByString:@"|"][0];
            }
            if([tagOutName isEqualToString:coverageName])
            {
                if([[fullCoverageArray objectAtIndex:j] isEqualToString:[loginDetails objectForKey:@"twUserId"]])
                {
                    
                }
                else
                {
                    
                    int status=[[[[coverageArray objectAtIndex:j]objectForKey:@"status"] componentsSeparatedByString:@"_"][0] intValue];
                    NSString *location=[[[coverageArray objectAtIndex:j]objectForKey:@"status"] componentsSeparatedByString:@"_"][1];
                    NSDictionary *dic=[newContactsListArray objectAtIndex:i];
                    NSMutableDictionary *replacedic=[[NSMutableDictionary alloc]init];
                    [replacedic setObject:[NSString stringWithFormat:@"%d",status] forKey:@"Status"];
                    if (status==6)
                    {
                        [replacedic setObject:@"" forKey:@"location"];
                    }
                    else
                    {
                        [replacedic setObject:location forKey:@"location"];
                    }
                    [replacedic setObject:[[coverageArray objectAtIndex:j]objectForKey:@"ct"] forKey:@"manageriCon"];
                    [replacedic setObject:[dic objectForKey:@"empid"] forKey:@"empid"];
                    [replacedic setObject:[dic objectForKey:@"Name"] forKey:@"Name"];
                    [newContactsListArray replaceObjectAtIndex:i withObject:replacedic];
                    
                }
            }
            else
            {
                //tagoutname not avaliable in coverage jsonstring
                
            }
            
        }
        
    }
    
    // Sorting names based upon Alphabat order
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"Name" ascending:YES];
    [newContactsListArray sortUsingDescriptors:@[descriptor]];
    
    // Sorting Statusicon based upon status
    
    NSSortDescriptor *descriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"Status" ascending:YES];
    [newContactsListArray sortUsingDescriptors:@[descriptor1]];
    
    
    for (int idx = 0;idx<[newContactsListArray count];idx++) {
        NSDictionary *dict = [newContactsListArray objectAtIndex:idx];
        
        if ([[dict objectForKey:@"manageriCon" ] intValue]==3)
        {
            if ([[dict objectForKey:@"manageriCon" ] intValue]==3 && ([[dict objectForKey:@"Status" ] intValue]==6))
            {
                
            }
            else
            {
                [newContactsListArray removeObjectAtIndex:idx];
                [newContactsListArray insertObject:dict atIndex:0];
            }
        }
        
    }
    
    // Reloading view
    
    [favouritesAddTableview reloadData];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coverageRequestToServertask" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    NSLog(@"Favorites Add view viewWillAppear");
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coverageRequestToServertask:)
                                                 name:@"coverageRequestToServertask"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:@"localnotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    self.searchDisplayController.searchBar.placeholder=[dictLang objectForKey:@"Search"];
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }    connectionLabel=[[UILabel alloc]init];
    connectionLabel.textColor=[UIColor whiteColor];
    connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
    connectionLabel.frame= CGRectMake(0,self.view.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height);
    connectionLabel.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    connectionLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:connectionLabel];
    connectionLabel.hidden=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coveragejson:)
                                                 name:@"coveragejson"
                                               object:nil];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [favouritesAddTableview setContentOffset:CGPointMake(0, searchBar.frame.size.height)];
    
    
    contactnamesArray=[[NSMutableArray alloc]init];
    contactsearchResultsfullArray=[[NSMutableArray alloc]init];
    for (int i=0; i<[newContactsListArray count]; i++)
    {
        contactsList=[[ContactsList alloc]init];
        
        if ([[[newContactsListArray objectAtIndex:i]objectForKey:@"Name"] length]>0)
        {
            contactsList.tagOutName=[[[newContactsListArray objectAtIndex:i]objectForKey:@"Name"]lowercaseString];
            [ contactnamesArray addObject:contactsList];
        }
    }
    storeId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"]intValue];
    companyId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"companyId"]intValue];
    
    
    self.navigationItem.title=[dictLang objectForKey:@"Employees"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kNavBackgroundColor}];
    UIBarButtonItem *rightButton=[[UIBarButtonItem alloc]initWithTitle:[dictLang objectForKey:@"Cancel"] style:UIBarButtonItemStylePlain target:self action:@selector(rightButtonAction)];
    self.navigationItem.rightBarButtonItem=rightButton;
    
    loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    // Connectivity and connecting to itg sever for getting employeeList
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"FavoritesAddVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavoritesAdd"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    [favouritesAddTableview setContentOffset:CGPointZero animated:YES];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView != favouritesAddTableview) {
        return [contactsearchResultsAll count];
        
    }
    
    else
    {
        return [newContactsListArray count];
    }
    
    return 0;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    contactsList=[[ContactsList alloc]init];
    static NSString *cellIdentifier=@"cell";
    UITableViewCell *cell;
    UIImageView *managersView;
    UIButton *modView;
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle
                reuseIdentifier:cellIdentifier];
        managersView=[[UIImageView alloc]initWithFrame:CGRectMake(240, 10, 30, 30)];
        modView=[UIButton buttonWithType:UIButtonTypeCustom];
        modView.titleLabel.font = [UIFont fontWithName:kFontName size:13];
        [modView setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        modView .frame=CGRectMake(230, 10, 50, 30);
        
        
        [cell.contentView addSubview:managersView];
        [cell.contentView addSubview:modView];
    }
    cell.textLabel.font=[UIFont fontWithName:kFontName size:kFontSize];
    cell.detailTextLabel.textColor=kNavBackgroundColor;
    cell.textLabel.textColor=kNavBackgroundColor;
    if (tableView != favouritesAddTableview)
    {
        
        cell.textLabel.text=[[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"Name"]capitalizedString];
        cell.detailTextLabel.text=[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"location"];
        int status =[[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"Status"]intValue];
        
        switch (status) {
            case 0:
                contactsList.status=@"green_circle";
                
                break;
                
            case 1:
                contactsList.status=@"red_circle";
                
            case 2:
                contactsList.status=@"red_circle";
                
            case 3:
                contactsList.status=@"red_circle";
                
                break;
            case 6:
                contactsList.status=@"grey_circle";
                
                break;
            default:
                // contactsList.status=@"grey_circle";
                break;
        }
        
        if ([[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"]intValue] == 2)
            
        {
            managersView.image=[UIImage imageNamed:@"manager_icon"];
            
            
        }
        if ([[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"]intValue] == 3)
        {
            
            
            modView.layer.cornerRadius=7;
            modView.layer.borderWidth=3;
            modView.layer.borderColor=kNavBackgroundColor.CGColor;
            [modView setTitle:modString forState:UIControlStateNormal];
            
            
        }
        if(status==6)
        {
            modView.hidden=YES;
        }
        cell.imageView.image=[UIImage imageNamed:contactsList.status];
    }
    else
    {
        cell.textLabel.text=[[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"Name"]capitalizedString];
        cell.detailTextLabel.text=[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"location"];
        int status =[[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"Status"]intValue];
        
        switch (status) {
            case 0:
                contactsList.status=@"green_circle";
                
                break;
                
            case 1:
                contactsList.status=@"red_circle";
                
            case 2:
                contactsList.status=@"red_circle";
                
            case 3:
                contactsList.status=@"red_circle";
                
                break;
            case 6:
                contactsList.status=@"grey_circle";
                
                break;
            default:
                // contactsList.status=@"grey_circle";
                break;
        }
        
        if ([[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"]intValue] == 2)
            
        {
            managersView.image=[UIImage imageNamed:@"manager_icon"];
            
            
        }
        if ([[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"]intValue] == 3)
        {
            
            modView.layer.cornerRadius=7;
            modView.layer.borderWidth=3;
            modView.layer.borderColor=kNavBackgroundColor.CGColor;
            [modView setTitle:modString forState:UIControlStateNormal];
            
        }
        if(status==6)
        {
            modView.hidden=YES;
        }
        cell.imageView.image=[UIImage imageNamed:contactsList.status];
    }
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle=UITableViewCellEditingStyleNone;
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView != favouritesAddTableview)
    {
        [_delegate FavouritesArrayDelegateMethod:[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"Name"]  employeeid:[[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"empid"] intValue] status:[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"Status"] location:[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"location"] manageriCon:[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"] ];
    }
    else
    {
        
        [_delegate FavouritesArrayDelegateMethod:[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"Name"]  employeeid:[[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"empid"] intValue] status:[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"Status"] location:[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"location"] manageriCon:[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"] ];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)filterContentForSearchText:(NSString*)searchTextName scope:(NSString*)scope
{
    contactsearchResultsAll=[[NSMutableArray alloc]init];
    
    for (int i =0;i<[contactnamesArray count];i++)
    {
        contactsList = [contactnamesArray objectAtIndex:i];
        NSRange range=[contactsList.tagOutName rangeOfString:searchTextName options:NSCaseInsensitiveSearch];
        if (range.length >0)
        {
            if (![contactsearchResultsAll containsObject:contactsList.tagOutName]) {
                [contactsearchResultsAll addObject:contactsList];
            }
        }
        
    }
    [contactsearchResultsfullArray removeAllObjects];
    contactsearchResultsfullArray=[[NSMutableArray alloc]init];
    
    for (ContactsList *contacts in contactsearchResultsAll)
    {
        for (NSDictionary *contactsList1 in newContactsListArray) {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            if ([contacts.tagOutName isEqualToString:[contactsList1 objectForKey:@"Name"]])
            {
                [dic setObject:[contactsList1 objectForKey:@"Name"] forKey:@"Name"];
                [dic setObject:[contactsList1 objectForKey:@"location"] forKey:@"location"];
                [dic setObject:[contactsList1 objectForKey:@"Status"] forKey:@"Status"];
                [dic setObject:[contactsList1 objectForKey:@"manageriCon"] forKey:@"manageriCon"];
                [dic setObject:[contactsList1 objectForKey:@"empid"] forKey:@"empid"];
                [contactsearchResultsfullArray addObject:dic];
                
            }
        }
    }
}

#pragma mark - UISearchDisplayController delegate methods
-(BOOL)searchDisplayController:(UISearchController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[controller.searchBar scopeButtonTitles]
                                      objectAtIndex:[controller.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}


-(void)reOpenClosedSocketsNotifiacation
{
    NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"BroadcastViewController"])
    {
        
    }
    else
    {
        NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
        NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
        if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
        {
            if ([fromPush isEqualToString:@"YES"])
            {
                NSArray *viewcontrollers=[self.navigationController viewControllers];
                for (int i=0;i<[viewcontrollers count];i++)
                {
                    UIViewController *vc=[viewcontrollers objectAtIndex:i];
                    NSString *currentView=NSStringFromClass([vc class]);
                    if ([currentView isEqualToString:@"StoreListViewController"])
                    {
                        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                    }
                }
            }
            else
            {
                NSArray *viewcontrollers=[self.navigationController viewControllers];
                for (int i=0;i<[viewcontrollers count];i++)
                {
                    UIViewController *vc=[viewcontrollers objectAtIndex:i];
                    NSString *currentView=NSStringFromClass([vc class]);
                    if ([currentView isEqualToString:@"VPViewController"])
                    {
                        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                    }
                }
            }
            
        }
        else
        {
            connectionLabel.hidden=NO;
            [[StateMachineHandler sharedManager]reOpenClosedSockets];
            [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
        }
    }
    
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coveragejson" object:nil];
}

@end
