//
//  InterruptViewController.m
//  Managers
//
//  Created by sandeepchalla on 10/7/15.
//  Copyright © 2015 theatro. All rights reserved.
//

#import "InterruptViewController.h"
#import "Constant.h"
#import "StateMachineHandler.h"
#import "ViewController.h"
#import "UIViewController.h"
#import "AudioManager.h"
#import "DiagnosticTool.h"
#import "HttpHandler.h"

@interface InterruptViewController ()
{
    UIImageView *contactStatusImage;
    int covarageStatus;
    UILabel  *contactPlaceLabel,*connectionLabel;
    UIBarButtonItem *speakerButton;
    NSDictionary *dictLang;
    NSString *staticerrorcode;
}

@end

@implementation InterruptViewController
@synthesize contactString,recordPauseButton,endButton,contactStatusString;


- (IBAction)endButtonTapped:(id)sender
{
    [[StateMachineHandler sharedManager]sendPlaybufferenable:NO];
    [[StateMachineHandler sharedManager]sendCallEndToNative];
    [[StateMachineHandler sharedManager]sendCancelPlayoutToNative:YES];
    endButton.backgroundColor=[UIColor grayColor];
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    NSLog(@"Interrupt viewWillAppear");
    connectionLabel=[[UILabel alloc]init];
    connectionLabel.textColor=[UIColor whiteColor];
    connectionLabel.text=[dictLang objectForKey:@"Please wait..."];

    connectionLabel.frame= CGRectMake(0,self.view.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height);
    connectionLabel.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    connectionLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:connectionLabel];
    connectionLabel.hidden=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:@"localnotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coverageRequestToServertask:)
                                                 name:@"coverageRequestToServertask"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotification) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:NO];
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setEnabled:NO];
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    endButton.userInteractionEnabled=NO;
    covarageStatus=[[[NSUserDefaults standardUserDefaults]objectForKey:@"contactstatus"] intValue];
    [self intialView];
    endButton.backgroundColor=kGreyColor;
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coverageRequestToServertask" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForceLogOut" object:nil];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        [[StateMachineHandler sharedManager]sendPlaybufferenable:NO];
        [[StateMachineHandler sharedManager]sendCallEndToNative];
        [[StateMachineHandler sharedManager]sendCancelPlayoutToNative:YES];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
        }];
    }];
    [super viewWillDisappear:animated];
}

-(void)reOpenClosedSocketsNotification
{
    NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush isEqualToString:@"YES"])
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"StoreListViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        else
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"VPViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        
    }
    
    else
    {
        connectionLabel.hidden=NO;
        [[StateMachineHandler sharedManager] reOpenClosedSockets];
        [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
    }
    
}

-(void)coverageRequestToServertask:(NSNotification *)notification
{
    // Network messages getting from native c2 and updating to UI
    [self connectionRequest];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"texttime"])
    {
        if ([notification.object isEqualToString:@"server connection lost..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"server connection lost..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"could not start a connection..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"could not start a connection..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not logon to store..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not logon to store..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not connect to Proxy server..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not connect to Proxy server..."],staticerrorcode];
        }
        else
        {
            //Second time login ->please wait
            connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
        }
        
    }
    else
    {
        //First time login ,connecting to server->fetching emp list
        connectionLabel.text=notification.object;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"texttime"];
    }
    
    
}

-(void)tgsAlarmerror:(NSString *)tgsLoginerror
{
    staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:@"InterruptVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"TgsLogin"],[[DiagnosticTool sharedManager]generateErrorCode:tgsLoginerror]];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        [HttpHandler alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"TgsLogin"] severity:@"1" alarmdesc:staticerrorcode];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
    }];
}

-(void)connectionRequest
{
    recordPauseButton.userInteractionEnabled=NO;
    connectionLabel.hidden=NO;
}

-(void)coveragejson:(NSNotification *)notification
{
    recordPauseButton.userInteractionEnabled=YES;
    connectionLabel.hidden=YES;
    NSArray *coverageArray=[notification.object objectForKey:@"coverage"];
    for (int i=0; i<[coverageArray count]; i++)
    {
        // getting all covarageTagOutNames from jsonstring
        
        NSString *covarageTagOutName=[[[coverageArray objectAtIndex:i]objectForKey:@"name"] componentsSeparatedByString:@"_"][1];
        covarageStatus=[[[[coverageArray objectAtIndex:i]objectForKey:@"status"] componentsSeparatedByString:@"_"][0] intValue];
        NSString *location=[[[coverageArray objectAtIndex:i]objectForKey:@"status"] componentsSeparatedByString:@"_"][1];
        if ([[covarageTagOutName componentsSeparatedByString:@"|"] count]>1)
        {
            covarageTagOutName=[covarageTagOutName componentsSeparatedByString:@"|"][0];
        }
        if ([covarageTagOutName isEqualToString:contactString])
        {
            
            switch (covarageStatus) {
                case 0:
                    contactStatusString=@"green_circle";
                    break;
                case 1:
                case 2:
                case 3:
                    contactStatusString=@"red_circle";
                    break;
                case 6:
                    location=@"";
                    contactStatusString=@"grey_circle";
                    break;
            }
            contactStatusImage.image=[UIImage imageNamed:contactStatusString];
            contactPlaceLabel.text=location;
            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",covarageStatus] forKey:@"contactstatus"];
            [[NSUserDefaults standardUserDefaults]setObject:location forKey:@"contactlocation"];
        }
        else if(covarageStatus==5)
        {
            endButton.backgroundColor=[UIColor grayColor];
            [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
            recordPauseButton.userInteractionEnabled=YES;
            contactStatusString=@"grey_circle";
        }
    }
    
    
}
-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"InterruptViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        
        [ self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)viewDidLoad
{
     dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    lbl_Msg.text=[dictLang objectForKey:@"Transmit"];
    [[[[self.tabBarController tabBar]items]objectAtIndex:0]setTitle:[dictLang objectForKey:@"Message"]];
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setTitle:[dictLang objectForKey:@"One-to-One"]];
    [[[[self.tabBarController tabBar]items]objectAtIndex:2]setTitle:[dictLang objectForKey:@"Interrupt"]];
    
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coveragejson:)
                                                 name:@"coveragejson"
                                               object:nil];
    covarageStatus=[[[NSUserDefaults standardUserDefaults]objectForKey:@"contactstatus"] intValue];
    [self intialView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = kNavBackgroundColor;
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
    titleLabel.text = [[[NSUserDefaults standardUserDefaults]objectForKey:@"contactname"]capitalizedString];
    [titleLabel sizeToFit];
    contactString=[[NSUserDefaults standardUserDefaults]objectForKey:@"contactname"];
    
    contactPlaceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 18, self.view.frame.size.width-150, 20)];
    contactPlaceLabel.backgroundColor = [UIColor clearColor];
    contactPlaceLabel.textColor = kNavBackgroundColor;
    contactPlaceLabel.font = [UIFont systemFontOfSize:12];
    
    contactPlaceLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"contactlocation"];
    
    contactPlaceLabel.textAlignment=NSTextAlignmentCenter;
    [contactPlaceLabel sizeToFit];
    
    UIView *navigationTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-160, 30)];
    
    contactStatusImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    float widthDiff = contactPlaceLabel.frame.size.width - titleLabel.frame.size.width;
    CGRect titleframe = titleLabel.frame;
    CGRect subtitleframe = contactPlaceLabel.frame;
    if (widthDiff > 0)
    {
            titleLabel.frame =CGRectMake(((self.view.frame.size.width-100)/2)-titleframe.size.width/2, titleframe.origin.y, self.view.frame.size.width, titleframe.size.height); //CGRectIntegral(titleframe);
            
            contactPlaceLabel.frame = CGRectMake(((self.view.frame.size.width-100)/2)-subtitleframe.size.width/2, subtitleframe.origin.y, self.view.frame.size.width, subtitleframe.size.height);
            contactStatusImage.frame=CGRectMake(((self.view.frame.size.width-90)/2)-(titleframe.size.width/2)-20,05,12,12);
    }
    else{
        
            titleLabel.frame =CGRectMake(((self.view.frame.size.width-100)/2)-titleframe.size.width/2, titleframe.origin.y, self.view.frame.size.width-100, titleframe.size.height);
            subtitleframe.size.width=self.view.frame.size.width-200;
            subtitleframe.origin.x=20;
            contactPlaceLabel.frame = CGRectIntegral(subtitleframe);
            contactStatusImage.frame=CGRectMake(((self.view.frame.size.width-90)/2)-(titleframe.size.width/2)-20,05,12,12);
    }   
    contactStatusImage.image=[UIImage imageNamed:contactStatusString];
    [navigationTitleView addSubview:titleLabel];
    [navigationTitleView addSubview:contactPlaceLabel];
    [navigationTitleView addSubview:contactStatusImage];
    self.parentViewController.navigationItem.titleView=navigationTitleView;
    endButton.layer.cornerRadius=8;
    [super viewDidLoad];
    [recordPauseButton addTarget:self action:@selector(buttondDownPressed:) forControlEvents: UIControlEventTouchDown];
    [recordPauseButton addTarget:self action:@selector(buttonUpPressed:) forControlEvents: UIControlEventTouchUpInside];
    [recordPauseButton addTarget:self action:@selector(buttonDragOutside:) forControlEvents: UIControlEventTouchDragOutside];
    speakerButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"loud"] style:UIBarButtonItemStyleDone target:self action:@selector(speakerButtonAction:)];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"loud"] isEqualToString:@"mic"])
    {
       // [speakerButton setTintColor:[UIColor grayColor]];
        [speakerButton setImage:[UIImage imageNamed:@"headphones"]];
        speakerButton.tag=2;
        [[NSUserDefaults standardUserDefaults]setObject:@"mic" forKey:@"loud"];
        
    }
    else
    {
        speakerButton.tag=1;
       // [speakerButton setTintColor:kNavBackgroundColor];
        [speakerButton setImage:[UIImage imageNamed:@"loud"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"loud" forKey:@"loud"];
        
    }
    
    self.parentViewController.navigationItem.rightBarButtonItem=speakerButton;
    [[AudioManager getsharedInstance] switchAudioOutput];
    // Do any additional setup after loading the view.
    
     
    [endButton setTitle:[dictLang objectForKey:@"End"] forState:UIControlStateNormal];
}

-(void)speakerButtonAction:(UIBarButtonItem *)button
{
    if (button.tag==1)
    {
        //[speakerButton setTintColor:[UIColor grayColor]];
        [speakerButton setImage:[UIImage imageNamed:@"headphones"]];
        speakerButton.tag=2;
        [[NSUserDefaults standardUserDefaults]setObject:@"mic" forKey:@"loud"];
    }
    else
    {
        speakerButton.tag=1;
        //[speakerButton setTintColor:kNavBackgroundColor];
        [speakerButton setImage:[UIImage imageNamed:@"loud"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"loud" forKey:@"loud"];
    }
    [[AudioManager getsharedInstance] switchAudioOutput];
}



-(void)intialView
{
    switch (covarageStatus)
    {
        case 0:
            contactStatusString=@"green_circle";
            break;
        case 1:
        case 2:
        case 3:
            contactStatusString=@"red_circle";
            break;
            
        case 6:
            contactStatusString=@"grey_circle";
            break;
    }
}


- (void)buttondDownPressed:(UIButton *)sender
{
    lbl_Msg.text=[dictLang objectForKey:@"Transmitting"];
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    [[StateMachineHandler sharedManager]mediabuttonAction:@"interrupt" nameStr:contactString Button:YES];
    
    [[StateMachineHandler sharedManager]sendPlaybufferenable:YES];
    
}



- (void)buttonUpPressed:(UIButton *)sender {
    
    lbl_Msg.text=[dictLang objectForKey:@"Transmit"];
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    endButton.userInteractionEnabled=YES;
    endButton.backgroundColor=[UIColor redColor];
    [[StateMachineHandler sharedManager]sendPlaybufferenable:YES];
    [[StateMachineHandler sharedManager] mediabuttonAction:@"interrupt" nameStr:contactString Button:NO];
}

-(void)buttonDragOutside:(UIButton *)sender
{
    lbl_Msg.text=[dictLang objectForKey:@"Transmitting"];
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    endButton.userInteractionEnabled=YES;
    endButton.backgroundColor=[UIColor redColor];
    [[StateMachineHandler sharedManager]sendPlaybufferenable:YES];
    [[StateMachineHandler sharedManager] mediabuttonAction:@"interrupt" nameStr:contactString Button:NO];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coveragejson" object:nil];
}

@end

