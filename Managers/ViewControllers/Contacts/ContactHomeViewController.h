//
//  ContactHomeViewController.h
//  Managers
//
//  Created by sandeepchalla on 10/7/15.
//  Copyright © 2015 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactHomeViewController : UIViewController<UITabBarDelegate>
{
    IBOutlet UITabBar *contacthometabbar;
     IBOutlet UILabel *lbl_Green;
}
@property(nonatomic,strong)NSString *contactString,*contactStatusString;
@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@end
