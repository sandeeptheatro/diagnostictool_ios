//
//  PersonsViewController.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 12/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"

@interface ContactsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,UISearchBarDelegate>
{
    IBOutlet UITableView *contactsTableview;
    IBOutlet UISearchBar *searchBar;
    IBOutlet UITabBarItem *eraboxTabitem,*announcementTabitem,*groupsTabitem,*interruptallTabitem;
     UILabel *connectionLabel;
    UISearchController *searchController;
     IBOutlet UILabel *lbl_Green;
   
    
}
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@property (strong, nonatomic) IBOutlet UITabBar *tabBarObj;
@property(nonatomic,strong)NSMutableArray *twEmployeeListArray;
@end
