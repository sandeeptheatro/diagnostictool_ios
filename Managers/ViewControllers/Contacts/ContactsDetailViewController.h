//
//  PersonsDetailViewController.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsDetailViewController : UIViewController
{
     IBOutlet UILabel *lbl_Green,*lbl_Msg;
}

- (IBAction)deleteFileTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property(nonatomic,assign)int ctManagers;
@property(nonatomic,strong)NSString *contactString,*contactStatusString;
@property(nonatomic,assign) BOOL earbox;

@end
