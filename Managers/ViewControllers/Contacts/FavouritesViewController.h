//
//  FavouritesViewController.h
//  Managers
//
//  Created by sandeepchalla on 1/11/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavouritesAddViewController.h"


@interface FavouritesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,FavouritesArrayDelegate>
{
    IBOutlet UITableView *favouritesViewTableview;
     IBOutlet UILabel *lbl_Green;
}
@property(nonatomic,strong)NSMutableArray *NewContactsListArray;
@property(nonatomic,strong)NSString *modString;
@end
