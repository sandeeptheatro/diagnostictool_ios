//
//  FavouritesAddViewController.h
//  Managers
//
//  Created by sandeepchalla on 2/16/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FavouritesArrayDelegate <NSObject>
-(void)FavouritesArrayDelegateMethod:(NSString*)names employeeid:(int)employeeid status:(NSString *)status location:(NSString *)location manageriCon:(NSString *)manageriCon;
@end

@interface FavouritesAddViewController : UIViewController

{
    IBOutlet UITableView *favouritesAddTableview;
    IBOutlet UISearchBar *searchBar;
     IBOutlet UILabel *lbl_Green;
}
@property(weak,nonatomic)id<FavouritesArrayDelegate>delegate;
@property(nonatomic,strong)NSString *modString;
@property (strong, nonatomic, getter=thenewContactsListArray)NSMutableArray *newContactsListArray;
@end
