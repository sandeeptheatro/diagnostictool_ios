//
//  PersonsViewController.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 12/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "ContactsViewController.h"
#import "BroadcastViewController.h"
#import "AnnouncementViewController.h"
#import "GroupsViewController.h"
#import "InterruptAllViewController.h"
#import "EarboxViewController.h"
#import "ContactsDetailViewController.h"
#import "Constant.h"
#import "HttpHandler.h"
#import "ContactsList.h"
#import "Reachability.h"
#import "AlertView.h"
#import "StateMachineHandler.h"
#import "ViewController.h"
#import "UIViewController.h"
#import "AnnouncementHomeViewController.h"
#import "ContactHomeViewController.h"
#import "UIViewController.h"
#import "FavouritesViewController.h"
#import "AppDelegate.h"
#import "ActivityIndicatorView.h"
#import "MessageViewController.h"
#import "MarqueeLabel.h"
#import "DiagnosticTool.h"

@interface ContactsViewController ()
{
    UILabel *messageLabel,*announcementLabel;
    NSArray *contactsPlaceArray,*contactStatusArray,*featureInfoListArray;
    NSMutableArray *contactsListArray,*newContactsListArray,*statusArray,*contactsearchResultsfullArray,*contactsearchResultsAll,*httpArray;
    ContactsList *contactsList;
    NSDictionary *loginDetails;
    BOOL newContactsListbool,httpupdatecheck,favupdatecheckcov;
    NSString *announcemenString,*messageString,*storeNameString,*modString;
    int storeId,companyId;
    UIBarButtonItem *favoritsButton,*broadCostButton;
    NSString *str,*staticerrorcode;
    NSDictionary *dictLang;

}

@end

@implementation ContactsViewController
@synthesize tabBarObj,twEmployeeListArray;


#pragma mark - Coveragejson(lib) method
-(void)communicatorStart
{
    // Communicator starts here
    [[StateMachineHandler sharedManager] communicator:NO];
    
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        //
    }
    else
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
            
           lbl_Green.backgroundColor=[UIColor whiteColor];
            
        }];
    }

    
}

-(void)connectionRequest
{
    contactsTableview.userInteractionEnabled=NO;
    connectionLabel.hidden=NO;
    tabBarObj.userInteractionEnabled=NO;
    searchBar.userInteractionEnabled=NO;
    self.navigationItem.rightBarButtonItem.enabled=NO;
     favoritsButton.enabled=NO;
    broadCostButton.enabled=NO;
}


-(void)coverageRequestToServertask:(NSNotification *)notification
{
    // Network messages getting from native c2 and updating to UI
    [self connectionRequest];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"texttime"])
    {
        if ([notification.object isEqualToString:@"server connection lost..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"server connection lost..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"could not start a connection..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"could not start a connection..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not logon to store..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not logon to store..."],staticerrorcode];
        }
        else if([notification.object isEqualToString:@"Could not connect to Proxy server..."])
        {
            [self tgsAlarmerror:notification.object];
            connectionLabel.text=[NSString stringWithFormat:@"%@ (%@i)",[dictLang objectForKey:@"Could not connect to Proxy server..."],staticerrorcode];
        }
        else
        {
            //Second time login ->please wait
            connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
        }
        
    }
    else
    {
        //First time login ,connecting to server->fetching emp list
        connectionLabel.text=notification.object;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"texttime"];
    }
    
    
}

-(void)tgsAlarmerror:(NSString *)tgsLoginerror
{
    staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:@"ContactVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"TgsLogin"],[[DiagnosticTool sharedManager]generateErrorCode:tgsLoginerror]];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        [HttpHandler alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"TgsLogin"] severity:@"1" alarmdesc:staticerrorcode];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
        }];
    }];
}

-(void)earboxAnnouncements
{
    
    if([messageString isEqualToString:@"0"] && [announcemenString isEqualToString:@"0"])
    {
        messageLabel.hidden=YES;
        announcementLabel.hidden=YES;
    }
    else if ([messageString isEqualToString:@"0"])
    {
        messageLabel.hidden=YES;
        announcementLabel.hidden=NO;
    }
    else if ([announcemenString isEqualToString:@"0"])
    {
        announcementLabel.hidden=YES;
        messageLabel.hidden=NO;
    }
    else
    {
        messageLabel.hidden=NO;
        announcementLabel.hidden=NO;
    }
    messageLabel.text=messageString;
    announcementLabel.text=announcemenString;
    
    
}

-(void)coveragejson:(NSNotification *)notification
{
    
    // 7 for add  and 8 for delete
    
    self.navigationItem.rightBarButtonItem.enabled=YES;
    tabBarObj.userInteractionEnabled=YES;
    searchBar.userInteractionEnabled=YES;
    contactsTableview.userInteractionEnabled=YES;
    connectionLabel.hidden=YES;
    broadCostButton.enabled=YES;
    // favoritsButton.enabled=YES;
   favupdatecheckcov = false;
    if ([[featureInfoListArray valueForKey:@"featureName"] containsObject:@"Favorites"] ||[[featureInfoListArray valueForKey:@"featureName"] containsObject:@"Favoris"])
    {
        favupdatecheckcov=YES;
        favoritsButton.enabled=YES;
        
    }
    else
    {
        if (favupdatecheckcov!=YES)
        {
            favoritsButton.enabled=NO;
        }
    }
    //Coverage jsonstring getting from ntive c2.
    
    NSArray *coverageArray=[notification.object objectForKey:@"coverage"];
    NSMutableArray *fullCoverageArray=[[NSMutableArray alloc]init];
    for (int i=0; i<[coverageArray count]; i++)
    {
        // getting all covarageTagOutNames from jsonstring
        
        NSString *covarageTagOutName=[[[coverageArray objectAtIndex:i]objectForKey:@"name"] componentsSeparatedByString:@"_"][1];
        [fullCoverageArray addObject:covarageTagOutName];
        
    }
    
    for (int i=0; i<[contactsListArray count]; i++)
    {
        
        for (int j=0; j<[fullCoverageArray count]; j++)
        {
            contactsList=[contactsListArray objectAtIndex:i];
            
            // Checking tagoutname in coverage jsonstring
            
            id tagOutName=[NSString stringWithFormat:@"%@",contactsList.tagOutName];
            id coverageName=[NSString stringWithFormat:@"%@",[fullCoverageArray objectAtIndex:j]];
            
            if ([[coverageName componentsSeparatedByString:@"|"] count]>1)
            {
                modString=[coverageName componentsSeparatedByString:@"|"][1];
                coverageName=[coverageName componentsSeparatedByString:@"|"][0];
                
            }
            if([tagOutName isEqualToString:coverageName])
            {
                if([[fullCoverageArray objectAtIndex:j] isEqualToString:[loginDetails objectForKey:@"twUserId"]])
                {
                    
                }
                else
                {
                    int status=[[[[coverageArray objectAtIndex:j]objectForKey:@"status"] componentsSeparatedByString:@"_"][0] intValue];
                    if (status!=9)
                    {
                        
                    [statusArray replaceObjectAtIndex:i withObject:[NSString stringWithFormat:@"%d",status]];
                    
                    NSString *location=[[[coverageArray objectAtIndex:j]objectForKey:@"status"] componentsSeparatedByString:@"_"][1];
                    if (status==6)
                    {
                        contactsList.location=@"";
                    }
                    else
                    {
                        contactsList.location=location;
                    }
                    
                    [contactsListArray replaceObjectAtIndex:i withObject:contactsList];
                    contactsList.manageriCon=[[coverageArray objectAtIndex:j]objectForKey:@"ct"];
                    [contactsListArray replaceObjectAtIndex:i withObject:contactsList];
                    }
                }
            }
            else
            {
                //tagoutname not avaliable in coverage jsonstring
                
            }
            
        }
        
    }
    newContactsListArray = [[NSMutableArray alloc]init];
    NSMutableArray *names = [NSMutableArray array];
    NSMutableArray *location = [NSMutableArray array];
    NSMutableArray *manageriCon = [NSMutableArray array];
    NSMutableArray *empid = [NSMutableArray array];
    for (int z=0; z<[contactsListArray count]; z++)
    {
        contactsList=[contactsListArray objectAtIndex:z];
        [names addObject:contactsList.tagOutName];
        [location addObject:contactsList.location];
        [manageriCon addObject:contactsList.manageriCon];
        [empid addObject:[NSNumber numberWithInt:contactsList.employeeId]];
    }
    
    // Forming all names,location and status as single dictionary with key value pair and adding to Array
    
    for (int idx = 0;idx<[names count];idx++) {
        NSDictionary *dict = @{@"Name": names[idx],@"location":location[idx],@"manageriCon":manageriCon[idx ], @"Status":statusArray[idx],@"empid":empid[idx ]};
        
        [newContactsListArray addObject:dict];
        
        if ([[manageriCon objectAtIndex:idx] intValue]==3)
        {
            if ([[dict objectForKey:@"manageriCon" ] intValue]==3 && ([[dict objectForKey:@"Status" ] intValue]==6))
            {
                
            }
            else
            {
                [newContactsListArray removeObjectAtIndex:idx];
                [newContactsListArray insertObject:dict atIndex:0];
            }
            
        }
    }
    
    // Sorting names based upon Alphabat order
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"Name" ascending:YES];
    [newContactsListArray sortUsingDescriptors:@[descriptor]];
    
    // Sorting Statusicon based upon status
    
    NSSortDescriptor *descriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"Status" ascending:YES];
    [newContactsListArray sortUsingDescriptors:@[descriptor1]];
    
    
    for (int idx = 0;idx<[names count];idx++) {
        NSDictionary *dict = [newContactsListArray objectAtIndex:idx];
        
        if ([[dict objectForKey:@"manageriCon" ] intValue]==3)
        {
            if ([[dict objectForKey:@"manageriCon" ] intValue]==3 && ([[dict objectForKey:@"Status" ] intValue]==6))
            {
                
            }
            else
            {
                [newContactsListArray removeObjectAtIndex:idx];
                [newContactsListArray insertObject:dict atIndex:0];
            }
        }
        
    }
    newContactsListbool=YES;
    [[NSUserDefaults standardUserDefaults]setObject:newContactsListArray forKey:@"contactlist"];
    // Reloading view
    NSArray* sortedArray = [newContactsListArray sortedArrayUsingFunction:comparator context:NULL];
    
    [newContactsListArray removeAllObjects];
    newContactsListArray=[NSMutableArray arrayWithArray:sortedArray];
    NSSortDescriptor *descriptor12 = [NSSortDescriptor sortDescriptorWithKey:@"Status" ascending:YES];
    [newContactsListArray sortUsingDescriptors:@[descriptor12]];
    
    [contactsTableview reloadData];
    
}

-(void)logonStealing:(NSNotification *)notification
{
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"ContactsViewController"])
    {
        [[StateMachineHandler sharedManager] killTheatroNativelogonstealing];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

-(void)instoreUpdate:(NSNotification *)inStoreUpdate
{
    NSString *inStore=inStoreUpdate.object;
    if ([inStore isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
           lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else{
            lbl_Green.backgroundColor = [UIColor whiteColor];
        }
       
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
    }
    else
    {
        lbl_Green.backgroundColor = [UIColor whiteColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
        
    }
}

-(void)employeeNameUpdate:(NSNotification *)employeeUpdate
{
    if (contactsListArray.count>0)
    {
    contactsList=[contactsListArray objectAtIndex:0];
    int severTime=[employeeUpdate.object intValue];
    NSString *localTime=contactsList.hiphopTime;
    
    if (severTime >[localTime intValue])
    {
        NSString *hiphopTime=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ];
        NSOperationQueue *myQueueone = [[NSOperationQueue alloc] init];
        [myQueueone addOperationWithBlock:^{
            
            NSArray  *employeeListArray= [[HttpHandler employeeList:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] storeId:storeId userScreen:@"ContactVC"] objectForKey:@"twEmployeeList"];
            NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"tagOutName" ascending:YES];
           
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                 twEmployeeListArray =[[employeeListArray sortedArrayUsingDescriptors:@[descriptor]]mutableCopy];
                httpArray=[[NSMutableArray alloc]init];
                statusArray=[[NSMutableArray alloc]init];
                for (int i=0;i<[twEmployeeListArray count];i++)
                {
                    [statusArray addObject:[NSString stringWithFormat:@"%d",6]];
                    if ([[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] isEqualToString:[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"]])
                        
                    {
                        
                    }
                    else
                    {
                        contactsList=[[ContactsList alloc]init];
                        
                        if ([[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"] length]>0)
                        {
                            contactsList.tagOutName=[[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"] lowercaseString];
                        }
                        contactsList.location=@"";
                        contactsList.manageriCon=@"1";
                        contactsList.employeeId=[[[twEmployeeListArray objectAtIndex:i] objectForKey:@"employeeId"] intValue];
                        contactsList.hiphopTime=hiphopTime;
                        [httpArray addObject:contactsList];
                    }
                }
                if ([httpArray isEqualToArray:contactsListArray]) {
                    NSLog(@"ContactsViewController-Employ list Http and DB both have same elements");
                }
                else{
                    contactsListArray=[[NSMutableArray alloc]init];
                    NSLog(@"ContactsViewController-Employee list is:%@",twEmployeeListArray);
                    for (int i=0;i<[twEmployeeListArray count];i++)
                    {
                        [statusArray addObject:[NSString stringWithFormat:@"%d",6]];
                        if ([[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] isEqualToString:[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"]])
                            
                        {
                            
                        }
                        else
                        {
                            contactsList=[[ContactsList alloc]init];
                            
                            if ([[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"] length]>0)
                            {
                                contactsList.tagOutName=[[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"]lowercaseString];
                            }
                            contactsList.location=@"";
                            contactsList.manageriCon=@"1";
                            contactsList.hiphopTime=hiphopTime;
                            contactsList.employeeId=[[[twEmployeeListArray objectAtIndex:i] objectForKey:@"employeeId"] intValue];
                            [contactsListArray addObject:contactsList];
                        }
                    }
                }
                [contactsTableview reloadData];
                [self createCachesDirectory:storeNameString];
                [[StateMachineHandler sharedManager] sendRefreshEvent];
                
            }];
        }];
    }
    }

}

-(void)reOpenClosedSocketsNotifiacation
{
    
    
        NSString *currentView=NSStringFromClass([[UIViewController currentViewController]class]);
    if ([currentView isEqualToString:@"BroadcastViewController"] || [currentView isEqualToString:@"UIAlertController"])
    {
        
    }
    else
    {
        [self unheardcount];
        NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
        NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
        if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
        {
            if ([fromPush isEqualToString:@"YES"])
            {
                NSArray *viewcontrollers=[self.navigationController viewControllers];
                for (int i=0;i<[viewcontrollers count];i++)
                {
                    UIViewController *vc=[viewcontrollers objectAtIndex:i];
                    NSString *currentView=NSStringFromClass([vc class]);
                    if ([currentView isEqualToString:@"StoreListViewController"])
                    {
                        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                    }
                }
            }
            else
            {
                NSArray *viewcontrollers=[self.navigationController viewControllers];
                for (int i=0;i<[viewcontrollers count];i++)
                {
                    UIViewController *vc=[viewcontrollers objectAtIndex:i];
                    NSString *currentView=NSStringFromClass([vc class]);
                    if ([currentView isEqualToString:@"VPViewController"])
                    {
                        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                    }
                }
            }
            
        }
        
        else
        {
            if (twEmployeeListArray.count==0)
            {
                [self connectionRequest];
            }
            else
            {
                [[StateMachineHandler sharedManager]reOpenClosedSockets];
                [[StateMachineHandler sharedManager]coverageRequestToServertaskbackgroundTheard];
            }
        }
    }
    
    
}

#pragma mark - Database Employ method
-(void)createCachesDirectory:(NSString *)emplist
{
    if ([twEmployeeListArray count]>0)
    {
        NSDictionary *cache = [NSDictionary dictionaryWithObject:twEmployeeListArray forKey:emplist];
        NSArray *cacheDirectories = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *cacheDirectory = [cacheDirectories objectAtIndex:0];
        NSString* cachePath = [cacheDirectory stringByAppendingPathComponent:emplist];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:cachePath];
        if (!fileExists) {
            [cache writeToFile: cachePath atomically: TRUE];
        }
    }
    
}

-(void)unheardcount
{
    [NSThread detachNewThreadSelector:@selector(earboxunheardcount) toTarget:self withObject:nil];
}

-(void)earboxunheardcount
{
    NSDictionary *twearboxunheardcounttdic= [HttpHandler earboxunheardcount:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId storeId:storeId userScreen:@"ContactVC"];
    [self performSelectorOnMainThread:@selector(earboxunheardcountupdate:) withObject:twearboxunheardcounttdic waitUntilDone:NO];
}

-(void)earboxunheardcountupdate:(NSDictionary *)twearboxunheardcounttdic
{
    if ([[twearboxunheardcounttdic objectForKey:@"earBoxUnheard"] isKindOfClass:[NSNull class]])
    {
        NSLog(@"ContactsViewController-Store list earBoxUnheard count is null");
    }
    else
    {
        int announcementcount= [[[twearboxunheardcounttdic objectForKey:@"earBoxUnheard"]objectForKey:@"announcement"] intValue];
        int messagecount= [[[twearboxunheardcounttdic objectForKey:@"earBoxUnheard"]objectForKey:@"message"] intValue];
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:messagecount] forKey:@"messagecount"];
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:announcementcount] forKey:@"announcementcount"];
        messageString=[NSString stringWithFormat:@"%d",messagecount];
        announcemenString=[NSString stringWithFormat:@"%d",announcementcount];
        [self earboxAnnouncements];
    }
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [tabBarObj invalidateIntrinsicContentSize];
    [super viewDidAppear:animated];
}

#pragma mark - View methods
- (void)viewDidLoad
{
        dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[dictLang objectForKey:@"Back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    self.searchDisplayController.searchBar.placeholder=[dictLang objectForKey:@"Search"];
    contactsTableview.userInteractionEnabled=NO;
    connectionLabel.hidden=NO;
    tabBarObj.userInteractionEnabled=NO;
    searchBar.userInteractionEnabled=NO;
    
//    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
//    
//    if ([str_GreenLbl isEqualToString:@"YES"])
//    {
//        lbl_Green.backgroundColor=[UIColor greenColor];
//    }
//    else
//    {
//        lbl_Green.backgroundColor = [UIColor whiteColor];
//    }
    
    NSString *str_GreenLbl =[[NSUserDefaults standardUserDefaults] objectForKey:@"InStore"];
    
    if ([str_GreenLbl isEqualToString:@"YES"])
    {
        NSString *str1=[[NSUserDefaults standardUserDefaults] objectForKey:@"InstoreWebSocket"];
        if (![str1 isEqualToString:@"YES"])
        {
            lbl_Green.backgroundColor = [UIColor greenColor];
        }
        else
        {
            lbl_Green.backgroundColor=[UIColor whiteColor];
        }
        //
    }
    else
    {
        lbl_Green.backgroundColor=[UIColor whiteColor];
    }

    
    //employeeNameUpdate
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"killcommunicator"];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coveragejson:)
                                                 name:@"coveragejson"
                                               object:nil];
    BOOL salesupdatecheck = false;
    BOOL favupdatecheck = false;
    BOOL groupupdatecheck = false;
    
    self.view.backgroundColor=[UIColor whiteColor];
    NSArray *salesenableArray=[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenable"];
    if ([salesenableArray count]>0)
    {
        for (int i=0; i<[salesenableArray count]; i++)
        {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeNameString"] isEqualToString:[[salesenableArray objectAtIndex:i] objectForKey:@"storeName"]])
            {
                for (int j=0; j<[[[salesenableArray objectAtIndex:i] objectForKey:@"featureInfoList"] count]; j++)
                {
                    NSArray *featureName=[[salesenableArray objectAtIndex:i] objectForKey:@"featureInfoList"];
                    
                    if ([[featureName valueForKey:@"featureName"] containsObject:@"Sales Update"]||[[featureName valueForKey:@"featureName"] containsObject:@"Mise à jour"])
                    {
                        salesupdatecheck=YES;
                        [[NSUserDefaults standardUserDefaults]setObject:@"salesenable" forKey:@"salesenabletab"];
                    }
                    else
                    {
                        if (salesupdatecheck==YES)
                        {
                            
                        }
                        else
                        {
                            [[NSUserDefaults standardUserDefaults]setObject:@"salesenableno" forKey:@"salesenabletab"];
                        }
                    }
                }
            }
            else
            {
                if (salesupdatecheck==YES)
                {
                    
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults]setObject:@"salesenableno" forKey:@"salesenabletab"];
                }
            }
        }
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"salesenableno" forKey:@"salesenabletab"];
    }
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (screenSize.height > 730.0f)
        {
            if(screenSize.height == 812.0f)
            {
                announcementLabel=[[UILabel alloc]initWithFrame:CGRectMake(296, 2, 20, 15)];
                messageLabel=[[UILabel alloc]initWithFrame:CGRectMake(339, 2, 20, 15)];
            }
            else
            {
                announcementLabel=[[UILabel alloc]initWithFrame:CGRectMake(330, 2, 20, 15)];
                messageLabel=[[UILabel alloc]initWithFrame:CGRectMake(375, 2, 20, 15)];
            }
        }
        else if (screenSize.height == 667.0f)
        {
            announcementLabel=[[UILabel alloc]initWithFrame:CGRectMake(296, 2, 20, 15)];
            messageLabel=[[UILabel alloc]initWithFrame:CGRectMake(340, 2, 20, 15)];
        }
        
        else
        {
            announcementLabel=[[UILabel alloc]initWithFrame:CGRectMake(247, 2, 20, 15)];
            messageLabel=[[UILabel alloc]initWithFrame:CGRectMake(292, 2, 20, 15)];
        }
    }
    else
    {
        announcementLabel=[[UILabel alloc]initWithFrame:CGRectMake(519, 2, 20, 15)];
        messageLabel=[[UILabel alloc]initWithFrame:CGRectMake(562, 2, 20, 15)];
    }
    messageLabel.backgroundColor=[UIColor redColor];
    messageLabel.textAlignment=NSTextAlignmentCenter;
    messageLabel.textColor=[UIColor whiteColor];
    messageString=@"0";
    announcementLabel.backgroundColor=kGreencolor;
    announcemenString=@"0";
    announcementLabel.textAlignment=NSTextAlignmentCenter;
    messageLabel.layer.cornerRadius=4;
    announcementLabel.textColor=[UIColor whiteColor];
    messageLabel.clipsToBounds=YES;
    announcementLabel.layer.cornerRadius=4;
    announcementLabel.font=[UIFont fontWithName:kFontName size:10.0];
    messageLabel.font=[UIFont fontWithName:kFontName size:10.0];
    announcementLabel.clipsToBounds=YES;
    
    [self.tabBarObj addSubview:messageLabel];
    [self.tabBarObj addSubview:announcementLabel];
    str = [[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
    
    
    connectionLabel=[[UILabel alloc]init];
    connectionLabel.textColor=[UIColor whiteColor];
    connectionLabel.text=[dictLang objectForKey:@"Please wait..."];
    connectionLabel.frame= CGRectMake(0,self.view.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height);
    connectionLabel.backgroundColor= [UIColor colorWithWhite:210/250 alpha:0.6];
    connectionLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:connectionLabel];
    contactsearchResultsfullArray=[[NSMutableArray alloc]init];
    storeId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"]intValue];
    companyId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"companyId"]intValue];
    storeNameString=[[NSUserDefaults standardUserDefaults]objectForKey:@"storeNameString"];
    
    statusArray = [NSMutableArray array];
    // Notifications from native c2
    contactsListArray=[[NSMutableArray alloc]init];
    loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(05, -2, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = kNavBackgroundColor;
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
   // titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(05, 18, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = kNavBackgroundColor;
    subTitleLabel.font = [UIFont systemFontOfSize:12];
    
    //subTitleLabel.textAlignment=NSTextAlignmentCenter;
    //subTitleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    float screenwidth=self.view.frame.size.width;
    if ([str isEqualToString:@"store"])
    {
        self.tabBarObj.hidden=YES;
        //titleLabel.text =@"Message to an Individual";
        //subTitleLabel.text =@"";
        //[titleLabel sizeToFit];
        self.navigationItem.title=[dictLang objectForKey:@"Message to an Individual"];
        
    }
    else
    {
        titleLabel.text =storeNameString;
        [titleLabel sizeToFit];
        subTitleLabel.text =[[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"]capitalizedString];
        [subTitleLabel sizeToFit];
        //[titleLabel sizeToFit];
        UIView *navigationTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(titleLabel.frame.size.width, subTitleLabel.frame.size.width), 30)];
        [navigationTitleView addSubview:titleLabel];
        [navigationTitleView addSubview:subTitleLabel];
        float widthDiff;
        BOOL checkframe = false;
        CGRect titleframe = titleLabel.frame;
        CGRect subtitleframe = subTitleLabel.frame;
        if ((screenwidth-150)< titleLabel.frame.size.width) //Title text big Subtitle text small
        {
            titleframe.size.width =screenwidth-170 ;
            titleLabel.frame=titleframe;
            widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
            checkframe=YES;
        }
        else if ((screenwidth-150)< subTitleLabel.frame.size.width) //Subtitle text big Title text small
        {
            subtitleframe.size.width =screenwidth-170 ;
            subTitleLabel.frame=subtitleframe;
            widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
            
            float newX = widthDiff / 2;
            titleframe.origin.x = newX;
            titleLabel.frame=titleframe;
        }
        else
        {
            widthDiff=subTitleLabel.frame.size.width - titleLabel.frame.size.width;
        }
        if (widthDiff > 0) {
            if (!checkframe)
            {
                float newX = widthDiff / 2;
                titleframe.origin.x = newX;
                titleLabel.frame=titleframe;
                
                subtitleframe.size.width =screenwidth-170 ;
                subTitleLabel.frame=subtitleframe;
            }
        }
        else{
                float newX = widthDiff / 2;
                subtitleframe.origin.x = fabsf(newX);
                subTitleLabel.frame=subtitleframe;
        
        }
       /* UILabel *navBarLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.origin.x+5,-5.0,0,50)];
        //UINavigationItem *item = [[UINavigationItem alloc] init];
        navBarLabel.backgroundColor = [UIColor clearColor];
        navBarLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        navBarLabel.numberOfLines = 1;
        navBarLabel.textAlignment = NSTextAlignmentCenter;
        navBarLabel.textColor = [UIColor colorWithRed:124.0/255.f green:125.0/255.f blue:128.0/255.f alpha:1.0];
        UILabel *navBarLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.origin.x+5,0,self.view.frame.size.width-170,50)];
        //UINavigationItem *item = [[UINavigationItem alloc] init];
        navBarLabel1.backgroundColor = [UIColor clearColor];
        navBarLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        navBarLabel1.numberOfLines = 1;
        navBarLabel1.textAlignment = NSTextAlignmentCenter;
        navBarLabel1.textColor = [UIColor colorWithRed:124.0/255.f green:125.0/255.f blue:128.0/255.f alpha:1.0];
       
        NSTextAttachment *imageAttachment = [[NSTextAttachment alloc] init];
        imageAttachment.image = [UIImage imageNamed:@"green_circle"];
        CGFloat imageOffsetY = -5.0;
        imageAttachment.bounds = CGRectMake(0, imageOffsetY, imageAttachment.image.size.width, imageAttachment.image.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:imageAttachment];
        NSMutableAttributedString *completeText= [[NSMutableAttributedString alloc] initWithString:@""];
        [completeText appendAttributedString:attachmentString];
        NSMutableAttributedString *textAfterIcon= [[NSMutableAttributedString alloc] initWithString:@"is an example hi sandeep how are ypu wr are you wt doing"];
        [completeText appendAttributedString:textAfterIcon];
        navBarLabel.attributedText=completeText;
        [navBarLabel sizeToFit];
        //[navigationTitleView addSubview:titleLabel];
        navBarLabel1.text=@"This hello whwere are you man waiting for you";
       
        [navigationTitleView addSubview:navBarLabel];
        //[navigationTitleView addSubview:navBarLabel1];*/
       
        self.navigationItem.titleView=navigationTitleView;
    }
    
    broadCostButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"broadcast"] style:UIBarButtonItemStyleDone target:self action:@selector(broadCostButtonAction)];
    broadCostButton.enabled=NO;
    favoritsButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"favorite"] style:UIBarButtonItemStyleDone target:self action:@selector(favoritsButtonAction)];
    favoritsButton.imageInsets = UIEdgeInsetsMake(0, 15, 0, -20);
    self.navigationController.navigationBar.tintColor = kNavBackgroundColor;
    NSArray *featureListArray=[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenable"];
    if ([featureListArray count]>0)
    {
        for (int i=0; i<[salesenableArray count]; i++)
        {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeNameString"] isEqualToString:[[salesenableArray objectAtIndex:i] objectForKey:@"storeName"]])
            {
                featureInfoListArray=[[[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenable"]objectAtIndex:i] objectForKey:@"featureInfoList"];
                for (int j=0; j<[featureInfoListArray count]; j++)
                {
                    
                    if ([[featureInfoListArray valueForKey:@"featureName"] containsObject:@"Groups"]||[[featureInfoListArray valueForKey:@"featureName"] containsObject:@"Groupes"])
                    {
                        groupupdatecheck=YES;
                        groupsTabitem.enabled=YES;
                    }
                    else
                    {
                        if (groupupdatecheck!=YES)
                        {
                            groupsTabitem.enabled=NO;
                        }
                    }
                    if ([[featureInfoListArray valueForKey:@"featureName"] containsObject:@"Favorites"] ||[[featureInfoListArray valueForKey:@"featureName"] containsObject:@"Favoris"])
                    {
                        favupdatecheck=YES;
                        favoritsButton.enabled=YES;
                        if (![str isEqualToString:@"store"])
                        {
                           // broadCostButton.enabled=YES;
                            self.navigationItem.rightBarButtonItems=@[broadCostButton,favoritsButton];
                        }
                        
                    }
                    else
                    {
                        if (favupdatecheck!=YES)
                        {
                            //self.navigationItem.rightBarButtonItems=@[broadCostButton];
                            if (![str isEqualToString:@"store"])
                            {
                               // broadCostButton.enabled=YES;
                                self.navigationItem.rightBarButtonItems=@[broadCostButton,favoritsButton];
                                favoritsButton.enabled=NO;
                            }
                            
                        }
                    }
                }
            }
            else
            {
                if (favupdatecheck!=YES)
                {
                    // self.navigationItem.rightBarButtonItems=@[broadCostButton];
                    if (![str isEqualToString:@"store"])
                    {
                      //  broadCostButton.enabled=YES;
                        self.navigationItem.rightBarButtonItems=@[broadCostButton,favoritsButton];
                    }
                    //self.navigationItem.rightBarButtonItems=@[broadCostButton,favoritsButton];
                    favoritsButton.enabled=NO;
                }
                if (groupupdatecheck!=YES)
                {
                    groupsTabitem.enabled=NO;
                }
                
                
                
            }
        }
    }
    else
    {
        groupsTabitem.enabled=NO;
        //self.navigationItem.rightBarButtonItems=@[broadCostButton];
        if (![str isEqualToString:@"store"])
        {
           // broadCostButton.enabled=YES;
            self.navigationItem.rightBarButtonItems=@[broadCostButton,favoritsButton];
        }
        //self.navigationItem.rightBarButtonItems=@[broadCostButton,favoritsButton];
        favoritsButton.enabled=NO;
    }
    
    [[UIBarButtonItem appearance] setTintColor:kNavBackgroundColor];
    
    // Connectivity and connecting to itg sever for getting employeeList
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"ContactVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"EmployeeList"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        // Asynchronous request
        NSString *hiphopTimefull=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] ];
        NSArray *tempArray = [hiphopTimefull componentsSeparatedByString:@"."];
        NSString *hiphopTime=[tempArray objectAtIndex:0];
        NSArray *cacheDirectories = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *cacheDirectory = [cacheDirectories objectAtIndex:0];
        NSString* cachePath = [cacheDirectory stringByAppendingPathComponent:storeNameString];
        NSDictionary* cache = [NSDictionary dictionaryWithContentsOfFile: cachePath];
        if ([[cache objectForKey:storeNameString] count]>0)
        {
            
            NSArray *array=[cache objectForKey:storeNameString];
            NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"tagOutName" ascending:YES];
            twEmployeeListArray=    [[array sortedArrayUsingDescriptors:@[descriptor]]mutableCopy];
            
            
            contactsListArray=[[NSMutableArray alloc]init];
            for (int i=0;i<[twEmployeeListArray count];i++)
            {
                [statusArray addObject:[NSString stringWithFormat:@"%d",6]];
                if ([[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] isEqualToString:[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"]])
                    
                {
                    
                }
                else
                {
                    contactsList=[[ContactsList alloc]init];
                    
                    if ([[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"] length]>0)
                    {
                        contactsList.tagOutName=[[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"]lowercaseString];
                    }
                    
                    contactsList.hiphopTime=hiphopTime;
                    contactsList.location=@"";
                    contactsList.manageriCon=@"1";
                    contactsList.employeeId=[[[twEmployeeListArray objectAtIndex:i] objectForKey:@"employeeId"] intValue];
                    [contactsListArray addObject:contactsList];
                }
            }
            [contactsTableview reloadData];
            [NSThread detachNewThreadSelector:@selector(communicatorStart) toTarget:self withObject:nil];
            NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
            [myQueue addOperationWithBlock:^{
                
                NSArray  *employeeListArray= [[HttpHandler employeeList:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] storeId:storeId userScreen:@"ContactVC"] objectForKey:@"twEmployeeList"];
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"tagOutName" ascending:YES];
                twEmployeeListArray =[[employeeListArray sortedArrayUsingDescriptors:@[descriptor]]mutableCopy];
                if (twEmployeeListArray.count>0)
                {
                    
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    httpArray=[[NSMutableArray alloc]init];
                    
                   
                    for (int i=0;i<[twEmployeeListArray count];i++)
                    {
                        [statusArray addObject:[NSString stringWithFormat:@"%d",6]];
                        if ([[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] isEqualToString:[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"]])
                            
                        {
                            
                        }
                        else
                        {
                            contactsList=[[ContactsList alloc]init];
                            
                            if ([[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"] length]>0)
                            {
                                contactsList.tagOutName=[[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"] lowercaseString];
                            }
                            contactsList.location=@"";
                            contactsList.manageriCon=@"1";
                            contactsList.employeeId=[[[twEmployeeListArray objectAtIndex:i] objectForKey:@"employeeId"] intValue];
                            contactsList.hiphopTime=hiphopTime;
                            [httpArray addObject:contactsList];
                        }
                    }
                    if ([httpArray isEqualToArray:contactsListArray]) {
                        NSLog(@"ContactsViewController-Employ list Http and DB both have same elements");
                    }
                    else{
                        contactsListArray=[[NSMutableArray alloc]init];
                        
                        NSLog(@"ContactsViewController-Employee list is:%@",twEmployeeListArray);
                        for (int i=0;i<[twEmployeeListArray count];i++)
                        {
                            [statusArray addObject:[NSString stringWithFormat:@"%d",6]];
                            if ([[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] isEqualToString:[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"]])
                                
                            {
                                
                            }
                            else
                            {
                                contactsList=[[ContactsList alloc]init];
                                
                                if ([[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"] length]>0)
                                {
                                    contactsList.tagOutName=[[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"]lowercaseString];
                                }
                                contactsList.location=@"";
                                contactsList.manageriCon=@"1";
                                contactsList.hiphopTime=hiphopTime;
                                contactsList.employeeId=[[[twEmployeeListArray objectAtIndex:i] objectForKey:@"employeeId"] intValue];
                                [contactsListArray addObject:contactsList];
                            }
                        }
                    }
                    
                        [contactsTableview reloadData];
                    
                    
                    [self createCachesDirectory:storeNameString];
                    [[StateMachineHandler sharedManager] sendRefreshEvent];
                    
                }];
                }
                else
                {
                    
                }
            }];
        }
        else
        {
            [self.view addSubview: [ActivityIndicatorView activityIndicatorView:self.view]];
            NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
            [myQueue addOperationWithBlock:^{
                
                twEmployeeListArray= [[HttpHandler employeeList:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] storeId:storeId userScreen:@"ContactVC"] objectForKey:@"twEmployeeList"];
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                    if ([twEmployeeListArray count]==0)
                    {
                        [AlertView alert:[dictLang objectForKey:@"Error"] message:[NSString stringWithFormat:@"%@i",[[NSUserDefaults standardUserDefaults]objectForKey:@"staticerrorcode"]]];
                        
                        [self connectionRequest];
                        
                    }
                    else
                    {
                        NSLog(@"ContactsViewController-Employee list is:%@",twEmployeeListArray);
                        for (int i=0;i<[twEmployeeListArray count];i++)
                        {
                            [statusArray addObject:[NSString stringWithFormat:@"%d",6]];
                            if ([[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] isEqualToString:[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"]])
                                
                            {
                                
                            }
                            else
                            {
                                contactsList=[[ContactsList alloc]init];
                                
                                if ([[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"] length]>0)
                                {
                                    contactsList.tagOutName=[[[twEmployeeListArray objectAtIndex:i] objectForKey:@"tagOutName"]lowercaseString];
                                }
                                contactsList.location=@"";
                                contactsList.manageriCon=@"1";
                                contactsList.employeeId=[[[twEmployeeListArray objectAtIndex:i] objectForKey:@"employeeId"] intValue];
                                contactsList.hiphopTime=hiphopTime;
                                [contactsListArray addObject:contactsList];
                            }
                        }
                    }
                    [contactsTableview reloadData];
                    [ActivityIndicatorView activityIndicatorViewRemove:self.view];
                    [self createCachesDirectory:storeNameString];
                    
                }];
                if ([twEmployeeListArray count]!=0)
                {
                    [self communicatorStart];
                    
                }
                
                
            }];
        }
    }
    self.navigationController.navigationBar.translucent = NO;
    eraboxTabitem.image = [[UIImage imageNamed:@"earbox"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    eraboxTabitem.title=[dictLang objectForKey:@"Earbox"];
    announcementTabitem.image = [[UIImage imageNamed:@"announcement"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    announcementTabitem.title=[dictLang objectForKey:@"Announcements"];
    groupsTabitem.image = [[UIImage imageNamed:@"groups"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
     groupsTabitem.title=[dictLang objectForKey:@"Groups"];
    interruptallTabitem.image = [[UIImage imageNamed:@"interrupt"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
     interruptallTabitem.title=[dictLang objectForKey:@"Interrupt All"];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }
                                             forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }
                                             forState:UIControlStateSelected];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    
    NSString *fromPush = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForDiffStore"];
    
    
    
    NSString *fromPush1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushForMultiStore"];
    if ([fromPush isEqualToString:@"YES"] ||[fromPush1 isEqualToString:@"YES"])
    {
        if ([fromPush isEqualToString:@"YES"])
        {
            EarboxViewController *earboxVC=[self.storyboard instantiateViewControllerWithIdentifier:@"EarboxViewController"];
            earboxVC.statusArray=newContactsListArray;
            [self.navigationController pushViewController:earboxVC animated:YES];
        }
        else
        {
            NSArray *viewcontrollers=[self.navigationController viewControllers];
            for (int i=0;i<[viewcontrollers count];i++)
            {
                UIViewController *vc=[viewcontrollers objectAtIndex:i];
                NSString *currentView=NSStringFromClass([vc class]);
                if ([currentView isEqualToString:@"VPViewController"])
                {
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:i] animated:YES];
                }
            }
        }
        
    }
    
    messageLabel.hidden=YES;
    announcementLabel.hidden=YES;
    [self unheardcount];
    [super viewDidLoad];
    
   
    // Do any additional setup after loading the view.
}

    static NSInteger comparator(id a, id b, void* context)
    {
        NSInteger options = NSCaseInsensitiveSearch
        | NSNumericSearch              // Numbers are compared using numeric value
        | NSDiacriticInsensitiveSearch // Ignores diacritics (â == á == a)
        | NSWidthInsensitiveSearch;    // Unicode special width is ignored
        
        
        NSString *nameStr=[a valueForKey:@"Name"];
        NSString *nameStr1=[b valueForKey:@"Name"];
        
        return [(NSString*)nameStr compare:nameStr1 options:options];
    }
    
-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(instoreUpdate:) name:@"InStore" object:nil];
    NSLog(@"ContactsViewController-Employeelist View viewWillAppear");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:@"localnotification" object:nil];
    //employeeNameUpdate
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(employeeNameUpdate:) name:@"employeeNameUpdate" object:nil];
    contactsTableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reOpenClosedSocketsNotifiacation) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"logonStealingNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logonStealing:) name:@"ForceLogOut" object:nil];
    //ForceLogOut
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushnotificationcount:) name:@"pushnotificationcount" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coverageRequestToServertask:)
                                                 name:@"coverageRequestToServertask"
                                               object:nil];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:kFontName size:12], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    [[UITabBarItem appearance] setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitlePositionAdjustment:UIOffsetMake(0.0, 0.0)];
    NSDictionary *attributes1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                            fontWithName:kFontName size:12], NSFontAttributeName,
                                 [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    [[UITabBarItem appearance] setTitleTextAttributes:attributes1 forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitlePositionAdjustment:UIOffsetMake(0.0, 0.0)];
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"]!=nil)
    {
        int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
        messageString=[NSString stringWithFormat:@"%d",messagecount];
    }
    
    if( [[NSUserDefaults standardUserDefaults]objectForKey:@"announcementcount"]!=nil)
    {
        int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementcount"] intValue];
        announcemenString=[NSString stringWithFormat:@"%d",announcementcount];
    }
    
    [self earboxAnnouncements];
    //[self unheardcount];
    
    [super viewWillAppear:animated];
}



-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"localnotification"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushnotificationcount" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coverageRequestToServertask" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logonStealingNotification" object:nil];
    
    
    // Stopping Comminucator when leaving view
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        
        // Navigation button was pressed. Do some stuff
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:0] forKey:@"storeid"];
            if ([twEmployeeListArray count]>0)
            {
                [[StateMachineHandler sharedManager] killTheatroNative];
            }
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                NSLog(@"ContactsViewController-User logout storename is:%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"storeNameString"]);
            }];
        }];
    }
    [super viewWillDisappear:animated];
}


#pragma mark - PushNotification method
-(void)pushnotificationcount:(NSNotification *)sender
{
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"]!=nil)
    {
        int messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagecount"] intValue];
        messageString=[NSString stringWithFormat:@"%d",messagecount];
    }
    
    if( [[NSUserDefaults standardUserDefaults]objectForKey:@"announcementcount"]!=nil)
    {
        int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementcount"] intValue];
        announcemenString=[NSString stringWithFormat:@"%d",announcementcount];
    }
    [self earboxAnnouncements];
    [self unheardcount];
}


-(void)forceLogOut:(NSNotification *)notification
{
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Tabbar methob
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    switch (item.tag)
    {
        case 1:
        {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"salesenabletab"] isEqualToString:@"salesenable"])
            {
                AnnouncementHomeViewController *announcementHomeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"AnnouncementSalesHomeViewController"];
                [self.navigationController pushViewController:announcementHomeVC animated:YES];
            }
            else
            {
                AnnouncementHomeViewController *announcementHomeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"AnnouncementHomeViewController"];
                [self.navigationController pushViewController:announcementHomeVC animated:YES];
            }
        }
            break;
        case 2:
        {
            GroupsViewController *groupsVC=[self.storyboard instantiateViewControllerWithIdentifier:@"GroupsViewController"];
            [self.navigationController pushViewController:groupsVC animated:YES];
        }
            break;
        case 3:
        {
            InterruptAllViewController *interruptAllVC=[self.storyboard instantiateViewControllerWithIdentifier:@"InterruptAllViewController"];
            [self.navigationController pushViewController:interruptAllVC animated:YES];
        }
            break;
        case 4:
        {
            EarboxViewController *earboxVC=[self.storyboard instantiateViewControllerWithIdentifier:@"EarboxViewController"];
            earboxVC.statusArray=newContactsListArray;
            [self.navigationController pushViewController:earboxVC animated:YES];
        }
            break;
    }
    
}

#pragma mark - BroadCast method
-(void)broadCostButtonAction
{
    BroadcastViewController *broadcastController = [self.storyboard instantiateViewControllerWithIdentifier:@"BroadCostViewController"];
    [self.navigationController pushViewController:broadcastController animated:YES];
}

#pragma mark - Favorits method
-(void)favoritsButtonAction
{
    FavouritesViewController *favouritesViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FavouritesViewController"];
    favouritesViewController.NewContactsListArray=newContactsListArray;
    favouritesViewController.modString=modString;
    [self.navigationController pushViewController:favouritesViewController animated:YES];
}


#pragma mark - TableView method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView != contactsTableview) {
        return [contactsearchResultsfullArray count];
        
    }
    if (tableView==contactsTableview)
    {
        if (newContactsListbool ==YES)
        {
            return [newContactsListArray count];
        }
        else
        {
            return [contactsListArray count];
        }
    }
    return 0;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    contactsList=[contactsListArray objectAtIndex:indexPath.row];
    static NSString *cellIdentifier=@"cell";
    UITableViewCell *cell;
    UIImageView *managersView;
    UIButton *modView;
    MarqueeLabel *contactsLbl;
    UILabel *descLbl;
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle
                reuseIdentifier:cellIdentifier];
        managersView=[[UIImageView alloc]initWithFrame:CGRectMake(240, 10, 30, 30)];
        modView=[UIButton buttonWithType:UIButtonTypeCustom];
        modView.titleLabel.font = [UIFont fontWithName:kFontName size:13];
        [modView setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        modView .frame=CGRectMake(self.view.frame.size.width-90, 10, 50, 30);
        contactsLbl = [[MarqueeLabel alloc] initWithFrame:CGRectMake(50, 5,self.view.frame.size.width-150, 30)];
        
        descLbl = [[UILabel alloc] initWithFrame:CGRectMake(50, 22,self.view.frame.size.width-150, 30)];
        //contactsLbl.frame = CGRectMake;
        //contactsLbl.text=@"abcdefghijklmnopqrstuvwxyz123456";
        [cell.contentView addSubview:managersView];
        [cell.contentView addSubview:modView];;
        [cell.contentView addSubview:contactsLbl];
        [cell.contentView addSubview:descLbl];
    }
    contactsLbl.font=[UIFont fontWithName:kFontName size:kFontSize];
    descLbl.textColor=kNavBackgroundColor;
    contactsLbl.textColor=kNavBackgroundColor;
// cell.textLabel.frame=CGRectMake(30, 10,150, 30);
    contactsLbl.marqueeType = MLContinuous;
    contactsLbl.scrollDuration = 20.0;
    contactsLbl.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    contactsLbl.fadeLength = 0.0f;
    contactsLbl.leadingBuffer = 0.0f;
    contactsLbl.trailingBuffer = 10.0f;
    
    if (newContactsListbool==YES)
    {
        
        if (tableView != contactsTableview)
        {
            
            contactsLbl.text=[[[contactsearchResultsfullArray objectAtIndex:indexPath.row]objectForKey:@"Name"]capitalizedString];
            descLbl.text=[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"location"];
            int status =[[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"Status"]intValue];
            
            switch (status) {
                case 0:
                    contactsList.status=@"green_circle";
                    
                    break;
                    
                case 1:
                    contactsList.status=@"red_circle";
                    
                case 2:
                    contactsList.status=@"red_circle";
                    
                case 3:
                    contactsList.status=@"red_circle";
                    
                    break;
                case 6:
                    contactsList.status=@"grey_circle";
                    
                    break;
                    
            }
            
            if ([[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"]intValue] == 2)
                
            {
                managersView.image=[UIImage imageNamed:@"manager_icon"];
                
                
            }
            if ([[[contactsearchResultsfullArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"]intValue] == 3)
            {
                
                
                modView.layer.cornerRadius=7;
                modView.layer.borderWidth=3;
                modView.layer.borderColor=kNavBackgroundColor.CGColor;
                [modView setTitle:modString forState:UIControlStateNormal];
                
                
            }
            if(status==6)
            {
                modView.hidden=YES;
            }
            cell.imageView.image=[UIImage imageNamed:contactsList.status];
        }
        
        else
        {
            contactsLbl.text=[[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"Name"]capitalizedString];
            descLbl.text=[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"location"];
            int status =[[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"Status"]intValue];
            
            switch (status) {
                case 0:
                    contactsList.status=@"green_circle";
                    
                    break;
                    
                case 1:
                    contactsList.status=@"red_circle";
                    
                case 2:
                    contactsList.status=@"red_circle";
                    
                case 3:
                    contactsList.status=@"red_circle";
                    
                    break;
                case 6:
                    contactsList.status=@"grey_circle";
                    break;
            }
            
            if ([[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"]intValue] == 2)
                
            {
                managersView.image=[UIImage imageNamed:@"manager_icon"];
                
                
            }
            if ([[[newContactsListArray objectAtIndex:indexPath.row] objectForKey:@"manageriCon"]intValue] == 3)
            {
                
                modView.layer.cornerRadius=7;
                modView.layer.borderWidth=3;
                modView.layer.borderColor=kNavBackgroundColor.CGColor;
                [modView setTitle:modString forState:UIControlStateNormal];
                
            }
            if(status==6)
            {
                modView.hidden=YES;
            }
            cell.imageView.image=[UIImage imageNamed:contactsList.status];
            
        }
    }
    else
    {
        contactsLbl.text=[contactsList.tagOutName capitalizedString];
        descLbl.text=contactsList.location;
        cell.imageView.image=[UIImage imageNamed:@"grey_circle"];
    }
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle=UITableViewCellEditingStyleNone;
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView != contactsTableview) {
        if ([str isEqualToString:@"store"])
        {
            MessageViewController *contactsDetailVC=[self.storyboard instantiateViewControllerWithIdentifier:@"MessageViewController"];
            contactsDetailVC.earbox=YES;
            [[NSUserDefaults standardUserDefaults]setObject:[[contactsearchResultsfullArray objectAtIndex:indexPath.row]objectForKey:@"Name"]  forKey:@"contactname"];
            [[NSUserDefaults standardUserDefaults]setObject:[[contactsearchResultsfullArray objectAtIndex:indexPath.row]objectForKey:@"location"] forKey:@"contactlocation"];
            [[NSUserDefaults standardUserDefaults]setObject:[[contactsearchResultsfullArray objectAtIndex:indexPath.row]objectForKey:@"Status"] forKey:@"contactstatus"];
            [[NSUserDefaults standardUserDefaults]setObject:[[contactsearchResultsfullArray objectAtIndex:indexPath.row]objectForKey:@"manageriCon"] forKey:@"contactmanagericon"];
            
            [self.navigationController pushViewController:contactsDetailVC animated:YES];
        }
        else
        {
        ContactHomeViewController *contactsDetailVC=[self.storyboard instantiateViewControllerWithIdentifier:@"ContactHomeViewController"];
        
        [[NSUserDefaults standardUserDefaults]setObject:[[contactsearchResultsfullArray objectAtIndex:indexPath.row]objectForKey:@"Name"]  forKey:@"contactname"];
        [[NSUserDefaults standardUserDefaults]setObject:[[contactsearchResultsfullArray objectAtIndex:indexPath.row]objectForKey:@"location"] forKey:@"contactlocation"];
        [[NSUserDefaults standardUserDefaults]setObject:[[contactsearchResultsfullArray objectAtIndex:indexPath.row]objectForKey:@"Status"] forKey:@"contactstatus"];
        [[NSUserDefaults standardUserDefaults]setObject:[[contactsearchResultsfullArray objectAtIndex:indexPath.row]objectForKey:@"manageriCon"] forKey:@"contactmanagericon"];
        
        [self.navigationController pushViewController:contactsDetailVC animated:YES];
        }
    }
    else
    {
        if ([str isEqualToString:@"store"])
        {
            MessageViewController *contactsDetailVC=[self.storyboard instantiateViewControllerWithIdentifier:@"MessageViewController"];
            contactsDetailVC.earbox=YES;
            [[NSUserDefaults standardUserDefaults]setObject:[[newContactsListArray objectAtIndex:indexPath.row]objectForKey:@"Name"] forKey:@"contactname"];
            [[NSUserDefaults standardUserDefaults]setObject:[[newContactsListArray objectAtIndex:indexPath.row]objectForKey:@"location"] forKey:@"contactlocation"];
            [[NSUserDefaults standardUserDefaults]setObject:[[newContactsListArray objectAtIndex:indexPath.row]objectForKey:@"Status"] forKey:@"contactstatus"];
            [[NSUserDefaults standardUserDefaults]setObject:[[newContactsListArray objectAtIndex:indexPath.row]objectForKey:@"manageriCon"] forKey:@"contactmanagericon"];
            [self.navigationController pushViewController:contactsDetailVC animated:YES];
        }
        else
        {
        
        ContactHomeViewController *contactsDetailVC=[self.storyboard instantiateViewControllerWithIdentifier:@"ContactHomeViewController"];
        
        [[NSUserDefaults standardUserDefaults]setObject:[[newContactsListArray objectAtIndex:indexPath.row]objectForKey:@"Name"] forKey:@"contactname"];
        [[NSUserDefaults standardUserDefaults]setObject:[[newContactsListArray objectAtIndex:indexPath.row]objectForKey:@"location"] forKey:@"contactlocation"];
        [[NSUserDefaults standardUserDefaults]setObject:[[newContactsListArray objectAtIndex:indexPath.row]objectForKey:@"Status"] forKey:@"contactstatus"];
        [[NSUserDefaults standardUserDefaults]setObject:[[newContactsListArray objectAtIndex:indexPath.row]objectForKey:@"manageriCon"] forKey:@"contactmanagericon"];
        [self.navigationController pushViewController:contactsDetailVC animated:YES];
        }
    }
    
}


- (void)filterContentForSearchText:(NSString*)searchTextName scope:(NSString*)scope
{
    contactsearchResultsAll=[[NSMutableArray alloc]init];
    
    for (int i =0;i<[contactsListArray count];i++)
        
    {
        contactsList = [contactsListArray objectAtIndex:i];
        NSRange range=[contactsList.tagOutName rangeOfString:searchTextName options:NSCaseInsensitiveSearch];
        if (range.length >0)
        {
            if (![contactsearchResultsAll containsObject:contactsList.tagOutName]) {
                [contactsearchResultsAll addObject:contactsList];
            }
        }
        
    }
    [contactsearchResultsfullArray removeAllObjects];
    contactsearchResultsfullArray=[[NSMutableArray alloc]init];
    
    for (ContactsList *contacts in contactsearchResultsAll)
    {
        for (NSDictionary *contactsList1 in newContactsListArray) {
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            if ([contacts.tagOutName isEqualToString:[contactsList1 objectForKey:@"Name"]])
            {
                if (newContactsListbool==YES)
                {
                    [dic setObject:[contactsList1 objectForKey:@"Name"] forKey:@"Name"];
                    [dic setObject:[contactsList1 objectForKey:@"location"] forKey:@"location"];
                    [dic setObject:[contactsList1 objectForKey:@"Status"] forKey:@"Status"];
                    [dic setObject:[contactsList1 objectForKey:@"manageriCon"] forKey:@"manageriCon"];
                    [contactsearchResultsfullArray addObject:dic];
                }
                
            }
        }
    }
    
    
}

#pragma mark - UISearchDisplayController delegate methods
-(BOOL)searchDisplayController:(UISearchController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[controller.searchBar scopeButtonTitles]
                                      objectAtIndex:[controller.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

-(void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"coveragejson" object:nil];
}

@end
