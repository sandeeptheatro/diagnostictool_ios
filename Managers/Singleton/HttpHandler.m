//
//  HttpHandler.m
//  Managers
//
//  Created by Ravi Shankar on 27/01/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#import "HttpHandler.h"
#import "AFHTTPRequestOperationManager.h"
#import <sys/utsname.h>
#import "UAPush.h"
#import "UAirship.h"
#import "NSLogger.h"
#import "Constant.h"
#import "InstoreData.h"
#import "Reachability.h"
#import "DiagnosticTool.h"


@implementation HttpHandler

+(NSDictionary *)login:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    //serverpathGlobal
    
    if (![[InstoreData sharedManager] isInStoreTGS])
    {
        serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpathGlobal"];
        [[NSUserDefaults standardUserDefaults] setObject:serverPath forKey:@"serverpath"];
    }
    
    if(serverPath.length==0)
    {
        serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpathGlobal"];
        
        if (serverPath.length==0)
        {
            serverPath=@"https://central.theatro.com/";
            [[NSUserDefaults standardUserDefaults] setObject:serverPath forKey:@"serverpath"];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:serverPath forKey:@"serverpath"];
        }
        
    }
    
   
    
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/app/user/%@",serverPath,@"login"];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"login url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    NSLog(@"channel id:%@", [[UAirship push]channelID]);
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
        if ([[[UAirship push]channelID] length]!=0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:[[UAirship push]channelID] forKey:@"deviceTokenString"];
            deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
        }
        
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
    NSString *lang=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"]];
    
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",lang,@"language",nil];
    //,lang,@"language"
    [postData setObject:userAgent forKey:@"userAgent"];
    
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Login-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:lang forHTTPHeaderField:@"X-Theatro-Language"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"login http responce time: %f",sec);
    NSDictionary *responsDic,*dict;
    NSString *responcecode;
    dict=[[NSUserDefaults standardUserDefaults]objectForKey:@"Language"];
    if (responce.statusCode!=200)
    {
        if ([data length]==0)
        {
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[dict objectForKey:@"Connection to server time out, Please check app setings"],@"Server Error", nil];
            NSLog(@"login error is: data length zero");
            responcecode=@"Connection to server time out, Please check app setings";
            
        }
        else
        {
            if (responce.statusCode==404 || responce.statusCode==502 ||responce.statusCode==503 || responce.statusCode==500 ||responce.statusCode==400 || responce.statusCode==405)
            {
                NSLog(@"login error is: data length zero");
                responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
                responcecode=[NSString stringWithFormat:@"%ld",(long)responce.statusCode];
            }
            else
            {
                if ([data length]!=0)
                {
                    responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    responcecode=[NSString stringWithFormat:@"%@", [[responsDic objectForKey:@"twResponseHeader"]objectForKey:@"twInternetStatus"]];//401 twerrorCode=14079
                }
                else
                {
                responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
                    responcecode=[NSString stringWithFormat:@"%@", [[responsDic objectForKey:@"twResponseHeader"]objectForKey:@"twInternetStatus"]];
                }
                NSLog(@"login error is:%@",[self statusString:responce.statusCode]);
            }
        }
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Login"],[[DiagnosticTool sharedManager]generateErrorCode:responcecode]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"Login"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"App login user name is:%@",twUserName);
        NSLog(@"login responce %@",responsDic);
    }
    return responsDic;
}

+(NSDictionary *)createPassWord:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment twUserID:(NSString *)twUserID userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    //POST Method
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/mapp/%@",serverPath,@"create/user"];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    NSLog(@"createPassWord url:%@",url);
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    [postData setObject:twUserID forKey:@"twUserId"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    NSString *deviceTokenString;
    NSLog(@"channel id:%@", [[UAirship push]channelID]);
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
        if ([[[UAirship push]channelID] length]!=0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:[[UAirship push]channelID] forKey:@"deviceTokenString"];
            deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
        }
        
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Creatpassword-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    NSString *lang=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"]];
    [request setValue:lang forHTTPHeaderField:@"X-Theatro-Language"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"createPassWord http responce time: %f",sec);
    NSDictionary *responsDic;
    
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"createPassWord error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"CreatePassWord"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"CreatePassWord"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"createPassWord responce %@",responsDic);
    }
    return responsDic;
}

+(BOOL)checkCentralServerAvailabilityForInStore:(NSString *)IpAddress
{
    Reachability* reachability = [Reachability reachabilityWithHostName:IpAddress];
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    //http://192.168.2.12/
    NSString *statusStr = [NSString stringWithFormat:@"%u",netStatus];
    
    return [statusStr boolValue];
}

+(BOOL)checkCentralServerAvailability
{
    Reachability* reachability = [Reachability reachabilityWithHostName: @"https://central.theatro.com/"];
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    //http://192.168.2.12/
    NSString *statusStr = [NSString stringWithFormat:@"%u",netStatus];
    
    return [statusStr boolValue];
}

+(NSDictionary *)forgotPassWord:(NSString *)twUserName twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    //POST Method
    
    
    if (![[InstoreData sharedManager] isInStoreTGS])
    {
        serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpathGlobal"];
        [[NSUserDefaults standardUserDefaults] setObject:serverPath forKey:@"serverpath"];

    }
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/mapp/%@",serverPath,@"forgotpsw"];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"forgot password url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    NSString *deviceTokenString;
    NSLog(@"channel id:%@", [[UAirship push]channelID]);
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
        if ([[[UAirship push]channelID] length]!=0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:[[UAirship push]channelID] forKey:@"deviceTokenString"];
            deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
        }
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"ForgotPassword-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    NSString *lang=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"]];
    [request setValue:lang forHTTPHeaderField:@"X-Theatro-Language"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"fogotPassWord http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"fogotPassWord error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"ForgotPassword"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:@"" twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"ForgotPassword"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"fogotPassWord responce %@",responsDic);
    }
    return responsDic;
    
}


+(NSDictionary *)changePassword:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment newPassword:(NSString *)newPassword currentPassword:(NSString *)currentPassword userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    //POST Method
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/mapp/%@",serverPath,@"change/password"];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    NSLog(@"changePassword url:%@",url);
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    [postData setObject:newPassword forKey:@"newPassword"];
    [postData setObject:currentPassword forKey:@"currentPassword"];
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    NSString *deviceTokenString;
    NSLog(@"channel id:%@", [[UAirship push]channelID]);
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
        if ([[[UAirship push]channelID] length]!=0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:[[UAirship push]channelID] forKey:@"deviceTokenString"];
            deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
        }
        
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    // [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"ChangePassowrd-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    NSString *lang=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"]];
    [request setValue:lang forHTTPHeaderField:@"X-Theatro-Language"];
    
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"changePassWord http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"changePassWord error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"ChangePassword"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"ChangePassword"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if (![[[responsDic objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
        {
            if ([[[responsDic objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"] isEqualToString:@"Success"])
                
            {
                NSMutableDictionary *postData=[NSMutableDictionary dictionary];
                
                [postData setObject:twUserName forKey:@"twUserName"];
                
                [postData setObject:newPassword forKey:@"twPassword"];
                
                [postData setObject:twTxId forKey:@"twTxId"];
                
                [postData setObject:twMoment forKey:@"twMoment"];
                [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
                
            }
        }
        NSLog(@"changePassWord responce %@",responsDic);
    }
    return responsDic;
}

+(NSDictionary *)storeList:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    
    NSUInteger companyId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"companyId"]intValue];
    //POST Method
    //NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/mapp/%@",serverPath,@"storelist"];
    
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/earbox/unheard/store/list/company/%lu/",serverPath,(unsigned long)companyId];
    
    //https://itg.theatro.com/restapi/earbox/unheard/store/list/company/1/
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"Storelist url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    NSLog(@"channel id:%@", [[UAirship push]channelID]);
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
        if ([[[UAirship push]channelID] length]!=0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:[[UAirship push]channelID] forKey:@"deviceTokenString"];
            deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
        }
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"StoreList-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"Store list http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"Store list error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"StoreList"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"StoreList"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"Store list data responce %@",responsDic);
    }
    return responsDic;
    
}


+(NSDictionary *)employeeList:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment storeId:(int)storeId userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    //POST Method
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/mapp/emplist/%d",serverPath,storeId];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"Employeelist url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                        
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"EmployeeList-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"employeelist http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"employeelist error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"EmployeeList"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"EmployeeList"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
            responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"employeelist responce %@",responsDic);
        
    }
    return responsDic;
    
}

+(NSDictionary *)groupList:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment storeId:(int)storeId userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    //POST Method
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/mapp/grouplist/%d",serverPath,storeId];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"Grouplist url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"GroupList-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"Group list http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"Group list error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"GroupList"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"GroupList"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"Group list data responce %@",responsDic);
    }
    return responsDic;
    
}

+(NSDictionary *)groupdetailList:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment groupId:(int)groupId userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    //POST Method
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/mapp/group/%d",serverPath,groupId];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"Groupdetail list url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"GroupDetail-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"groupdetail list http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"groupdetail list error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"GroupdetailList"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"GroupdetailList"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"groupdetail list responce %@",responsDic);
    }
    return responsDic;
}

+(NSDictionary *)notifyTcm:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId userScreen:(NSString *)userScreen
{
    NSString *serverPath=[NSString stringWithFormat:@"%@restapi/earbox/replied/%@/company/%@/store/%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"],[[NSUserDefaults standardUserDefaults]objectForKey:@"call_ReplyID"],[NSNumber numberWithInt:companyId],[NSNumber numberWithInt:storeId]];
    
    //https://itg.theatro.com/restapi/earbox/replayed/{replayedCallTxId}/company/{companyId}/store/{storeId}
    
    NSURL *url = [NSURL URLWithString:serverPath];
    NSLog(@"NotifyTcm url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData)
    {
        NSLog(@"NotifyTCM-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    NSDate *date=[NSDate date];
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc]init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
         NSLog(@"notifyTcm http responce time: %f",sec);
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
         if (error)
         {
             
         }
         else
         {

             if (httpResponse.statusCode)
             {
                 NSLog(@"notifyTcm responce %@",response);
                 NSLog(@"notifyTcm %@",[self statusString:httpResponse.statusCode]);
             }
             
         }
         NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"NotifyTcm"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)httpResponse.statusCode]]];
         [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
         [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"NotifyTcm"] severity:@"1" alarmdesc:staticerrorcode];
         // do stuff
     }];
    return 0;
}


+(NSDictionary *)audioFileSend:(NSDictionary *)whoDic audioFile:(NSData *)audiodata pns:(NSDictionary *)pns userScreen:(NSString *)userScreen
{
    
    ///restapi/v2/{scope}/upload/audio/common/as
    
    NSString *serverPath,*audioApi;
    
    if([whoDic objectForKey:@"featureType"])
        
    {
        NSString *asGroup = [[NSUserDefaults standardUserDefaults] objectForKey:@"ASGroup"];
        if ([asGroup isEqualToString:@"ASGroup"])
        {
            serverPath=[NSString stringWithFormat:@"%@restapi/v3/app/upload/audio/common/as",[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"]];
            audioApi=@"AudioFileSendV3As";
            
        }
        else
        {
            NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
            
            if ([str isEqualToString:@"HQ"])
                
            {
                serverPath=[NSString stringWithFormat:@"%@restapi/v3/app/upload/audio/as/member",[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"]];
                audioApi=@"AudioFileSendV3member";
                
            }
            else
            {
                serverPath=[NSString stringWithFormat:@"%@restapi/v3/app/upload/audio/common",[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"]];
                audioApi=@"AudioFileSendV3";
                
            }
        }
    }
    else
    {
        serverPath=[NSString stringWithFormat:@"%@restapi/audio/upload/mgrapp",[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"]];
        audioApi=@"AudioFileSendV1";
        
    }
    NSLog(@"Audiofile upload url:%@",serverPath);
    NSError *error;
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString* code = [NSString stringWithCString:systemInfo.machine
                      
                                        encoding:NSUTF8StringEncoding];
    
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    NSDictionary *authDic=[NSDictionary dictionaryWithObjectsAndKeys:[loginDetails objectForKey:@"twUserName"],@"twUserName",[loginDetails objectForKey:@"twPassword"],@"twPassword",[loginDetails objectForKey:@"twTxId"],@"twTxId",[loginDetails objectForKey:@"twMoment"],@"twMoment", nil];
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:whoDic
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                        
                                                         error:&error];
    NSString *jsonString,*userAgentString,*authString,*pnsString;
    NSData *userAgentData = [NSJSONSerialization dataWithJSONObject:userAgent
                                                            options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                              error:&error];
    NSData *authData = [NSJSONSerialization dataWithJSONObject:authDic
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSData *pnsData = [NSJSONSerialization dataWithJSONObject:pns
                                                      options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                        error:&error];
    if (! jsonData && !userAgentData && !authData )
    {
        NSLog(@"Audio upload-Got an error: %@", error);
        
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        userAgentString = [[NSString alloc] initWithData:userAgentData encoding:NSUTF8StringEncoding];
        authString = [[NSString alloc] initWithData:authData encoding:NSUTF8StringEncoding];
        pnsString= [[NSString alloc] initWithData:pnsData encoding:NSUTF8StringEncoding];
    }
    
    // NSDictionary *dicassgin=[NSDictionary dictionaryWithObjectsAndKeys:jsonString,@"who",userAgentString,@"userAgent",pnsString,@"pns",nil];
    
    NSDictionary *dicassgin=[NSDictionary dictionaryWithObjectsAndKeys:jsonString,@"who",userAgentString,@"userAgent",authString,@"auth",pnsString,@"pns",nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDate *date=[NSDate date];
    [manager POST:serverPath parameters:dicassgin constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFormData:audiodata name:@"audio_file"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
        NSLog(@"login http responce time: %f",sec);
        NSLog(@"Audio file who is:%@",whoDic);
        
        NSLog(@"Audio file sent success: %@", responseObject);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recievedAnnouncementsNotification" object:[NSArray arrayWithObjects:@"message",[responseObject objectForKey:@"pushTxId"],nil]];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Audio file who is:%@",whoDic);
        
        NSLog(@"Audio file send error is: %@", error);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recievedAnnouncementsNotification" object:[NSArray arrayWithObjects:@"error",nil]];
        
    }];
    
    return 0;
    
}

+(NSDictionary *)salesEnabled:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/mapp/getFeatureList",serverPath];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"Feature list url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Featurelist-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    NSString *lang=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"]];
    [request setValue:lang forHTTPHeaderField:@"X-Theatro-Language"];
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"Feature list http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"Feature list error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SalesEnabled"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"SalesEnabled"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"Feature list responce %@",responsDic);
    }
    return responsDic;
    
}

+(NSDictionary *)salesReminders:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/mapp/getFeatureList",serverPath];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    NSString *lang=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"]];
    [request setValue:lang forHTTPHeaderField:@"X-Theatro-Language"];
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"salesReminders http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"salesReminders error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SalesReminders"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"SalesReminders"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    }
    
    return responsDic;
    
    return 0;
}

+(NSDictionary *)favoriteAdd:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId addFavoriteUsers:(NSArray *)addFavoriteUsers userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/mapp/favorite/add",serverPath];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"Favoriteadd url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    [postData setObject:addFavoriteUsers forKey:@"addFavoriteUsers"];
    [postData setObject:[NSNumber numberWithInt:companyId] forKey:@"companyId"];
    [postData setObject:[NSNumber numberWithInt:storeId] forKey:@"storeId"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"favoriteadd http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"favoriteadd error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavoriteAdd"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"FavoriteAdd"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"favoriteadd responce %@",responsDic);
    }
    return responsDic;
    
}

+(NSDictionary *)favoritelist:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/mapp/favorite",serverPath];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"Favoritelist url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    [postData setObject:[NSNumber numberWithInt:companyId] forKey:@"companyId"];
    [postData setObject:[NSNumber numberWithInt:storeId] forKey:@"storeId"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"favorite list http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"favorite list error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Favoritelist"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"Favoritelist"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"favorite list responce %@",responsDic);
    }
    return responsDic;
    
}


+(NSDictionary *)earboxlist:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/earbox/company/%@/store/%@",serverPath,[NSNumber numberWithInt:companyId],[NSNumber numberWithInt:storeId]];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"VEB list url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"VEB-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"VEB earbox http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"VEB earbox error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"EarboxList"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"EarboxList"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"VEB earbox responce %@",responsDic);
    }
    return responsDic;
    
}

+(NSDictionary *)earboxundeliveredlist:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId twreqFor:(NSString *)twreqFor twreqType:(NSString *)twreqType userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/earbox/%@/%@/undelivered/company/%@/store/%@",serverPath,twreqType,twreqFor,[NSNumber numberWithInt:companyId],[NSNumber numberWithInt:storeId]];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"VEB undelivered url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"VEB Undelivered-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"VEB undelivered http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"VEB undelivered error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"EarboxundeliveredList"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"EarboxundeliveredList"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"VEB undelivered responce %@",responsDic);
    }
    return responsDic;
    
}


+(NSDictionary *)userlogout:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/mapp/logout",serverPath];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"Userlogout url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Logout-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"App logout http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"App logout error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"UserLogout"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"UserLogout"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"App logout user name is:%@",twUserName);
    }
    return responsDic;
    
}


+(NSDictionary *)earboxunheardcount:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/earbox/unheard/company/%@/store/%@",serverPath,[NSNumber numberWithInt:companyId],[NSNumber numberWithInt:storeId]];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"VEB unheardcount url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"VEB Unheard count-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"VEB unheard count http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"VEB unheard count error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"EarboxunheardCount"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:twUserName twPassword:twPassword twTxId:twTxId twMoment:twMoment alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"EarboxunheardCount"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"VEB unheard count is:%@",responsDic);
    }
    return responsDic;
    
}


+(NSDictionary *)uploadlogfiles:(NSData *)logfiledata userScreen:(NSString *)userScreen
{
    
    NSString *serverPath=[NSString stringWithFormat:@"%@restapi/mapp/upload/logs",[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"]];
    NSLog(@"Uploadlog files url:%@",serverPath);
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    NSString *datetime=[dateFormatter stringFromDate:[NSDate date]];
    NSString *date=[datetime componentsSeparatedByString:@" "][0];
    NSString *time=[datetime componentsSeparatedByString:@" "][1];
    NSDictionary  *twmAppLoginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
    NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
    NSString *store;
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"] != nil)
    {
        store=[[NSUserDefaults standardUserDefaults]objectForKey:@"storeNameString"];
    }
    else
    {
        store=@"";
    }
    NSString *fileName=[NSString stringWithFormat:@"Log_%@-%@_%@_iOSV%@_%@_%@",[twmAppLoginDetails objectForKey:@"chainName"],store,[loginDetails objectForKey:@"twUserName"],version,date,time];
    NSDictionary *dicassgin=[NSDictionary dictionaryWithObjectsAndKeys:[twmAppLoginDetails objectForKey:@"chainName"],@"chainName",store,@"storeName",@"txt",@"fileFormat",[loginDetails objectForKey:@"twUserName"],@"userName",[loginDetails objectForKey:@"twPassword"],@"passWord",fileName,@"fileName",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDate *date1=[NSDate date];
    [manager POST:serverPath parameters:dicassgin constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFormData:logfiledata name:@"logFile"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date1];
        NSLog(@"login http responce time: %f",sec);
        NSLog(@"Upload log file sent success: %@", responseObject);
        [[NSLogger sharedInstance]removefilePath];
        [[NSLogger sharedInstance]writeNSLogToFile];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Upload log file send error is: %@", error);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"Uploadlogfiles"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%d", 33]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"Uploadlogfiles"] severity:@"1" alarmdesc:staticerrorcode];
    }];
    
    return 0;
}

+(NSDictionary *)dynamicLogUpload:(NSString *)userScreen
{
    
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    
    ///restapi/v2/app/dynamic/configurations/
    
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    int storeId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"]intValue];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/v2/app/dynamic/configurations?store=%d",serverPath,storeId];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSLog(@"dynamic log upload url:%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    
    
    //set http method
    
    [request setHTTPMethod:@"POST"];
    
    //initialize a post data
    
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    
    [postData setObject:[loginDetails objectForKey:@"twUserName"] forKey:@"twUserName"];
    
    [postData setObject:[loginDetails objectForKey:@"twPassword"] forKey:@"twPassword"];
    
    [postData setObject:[loginDetails objectForKey:@"twTxId"] forKey:@"twTxId"];
    
    [postData setObject:[loginDetails objectForKey:@"twMoment"] forKey:@"twMoment"];
    
    
    
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                      
                                        encoding:NSUTF8StringEncoding];
    
    NSString *deviceTokenString;
    
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
        
    {
        
        deviceTokenString=@"";
        
    }
    
    else
        
    {
        
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
        
    }
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    
    [postData setObject:userAgent forKey:@"userAgent"];
    
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    
    NSError *error=nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                        
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                        
                                                         error:&error];
    
    NSString *jsonString;
    
    
    
    if (! jsonData) {
        
        NSLog(@"dynamic log upload Got an error: %@", error);
        
    } else {
        
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    
    //set post data of request
    
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"dynamic log upload http responce time: %f",sec);
    NSDictionary *responsDic;
    
    if ([data length]==0 || responce.statusCode!=200)
        
    {
        
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        
        NSLog(@"dynamic log upload error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"DynamicLogUpload"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"DynamicLogUpload"] severity:@"1" alarmdesc:staticerrorcode];
        
    }
    
    else
        
    {
        
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"dynamic log upload responce %@",responsDic);
        
    }
    
    return responsDic;
}


+(NSDictionary *)configsV2:(NSString *)userScreen
{
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/app/chain/%@/role/%@/configuration",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"roleId"]];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-Config url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:[loginDetails objectForKey:@"twUserName"] forKey:@"twUserName"];
    [postData setObject:[loginDetails objectForKey:@"twPassword"] forKey:@"twPassword"];
    [postData setObject:[loginDetails objectForKey:@"twTxId"] forKey:@"twTxId"];
    [postData setObject:[loginDetails objectForKey:@"twMoment"] forKey:@"twMoment"];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"MS-Config Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", [loginDetails objectForKey:@"twUserName"], [loginDetails objectForKey:@"twPassword"]];
    
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    NSString *lang=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"]];
    [request setValue:lang forHTTPHeaderField:@"X-Theatro-Language"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-Config http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"MS-Config error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"ConfigsV2"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"ConfigsV2"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"MS-Config responce %@",responsDic);
    }
    return responsDic;
}

+(NSDictionary *)selectGroupsV2:(NSString *)userScreen
{
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    
    NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
    NSMutableString *urlString;
    if([sl isEqualToString:@"SL"])
    {
        urlString =[NSMutableString stringWithFormat:@"%@restapi/v2/app/chain/%@/stores/%@/groups",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"storeID"]];
    }
    else
    {
        urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/app/chain/%@/groups",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
    }
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-Selectgroups url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:[loginDetails objectForKey:@"twUserName"] forKey:@"twUserName"];
    [postData setObject:[loginDetails objectForKey:@"twPassword"] forKey:@"twPassword"];
    [postData setObject:[loginDetails objectForKey:@"twTxId"] forKey:@"twTxId"];
    [postData setObject:[loginDetails objectForKey:@"twMoment"] forKey:@"twMoment"];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"MS-Group Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", [loginDetails objectForKey:@"twUserName"], [loginDetails objectForKey:@"twPassword"]];
    
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    NSString *lang=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"]];
    [request setValue:lang forHTTPHeaderField:@"X-Theatro-Language"];
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-Selectgroups http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"MS-Selectgroups error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectGroupsV2"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"SelectGroupsV2"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"MS-Selectgroups responce %@",responsDic);
    }
    return responsDic;
    
    return 0;
}

+(NSDictionary *)asMembersAndDistricts:(NSString *)userScreen
{
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString;
    NSString *apicode ;
    NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
    if ([sl isEqualToString:@"HQ"])
    {
        // https://itg0.theatro.com/restapi/v2/app/chain/{chainId}/asusers/
        urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/app/chain/%@/asusers",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
        apicode=@"AsMembersAndUsers";
    }
    else
    {
        //https://itg0.theatro.com/restapi/v2/app/chain/{chainId}/level/nodes
        urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/app/chain/%@/level/nodes",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
        apicode=@"AsMembersAndNodes";
    }
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-SelectStores url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:[loginDetails objectForKey:@"twUserName"] forKey:@"twUserName"];
    [postData setObject:[loginDetails objectForKey:@"twPassword"] forKey:@"twPassword"];
    [postData setObject:[loginDetails objectForKey:@"twTxId"] forKey:@"twTxId"];
    [postData setObject:[loginDetails objectForKey:@"twMoment"] forKey:@"twMoment"];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"MS-Stores Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", [loginDetails objectForKey:@"twUserName"], [loginDetails objectForKey:@"twPassword"]];
    
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    NSString *lang=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"]];
    [request setValue:lang forHTTPHeaderField:@"X-Theatro-Language"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-SelectStores http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"MS-SelectStores error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:apicode],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:apicode] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"MS-SelectStores responce %@",responsDic);
    }
    return responsDic;
    return 0;
}

+(NSDictionary *)selectStoresV2:(NSString *)userScreen
{
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString ;
    NSString *apicode;
    NSString *sl =[[NSUserDefaults standardUserDefaults] objectForKey:@"Select"];
    if ([sl isEqualToString:@"SL"])
    {
        urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/app/chain/%@/stores",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
        apicode=@"SelectStoresV2";
    }
    else if([sl isEqualToString:@"SelectStores"])
    {
        urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/app/chain/%@/stores",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
        apicode=@"SelectStoresV2";
    }
    else if([sl isEqualToString:@"SelectStoresGroup"])
    {
        urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/app/chain/%@/stores",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
        apicode=@"SelectStoresV2";
    }
    else if ([sl isEqualToString:@"HQ"])
    {
        // https://itg0.theatro.com/restapi/v2/app/chain/{chainId}/asusers/
        urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/app/chain/%@/asusers",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
        apicode=@"SelectStoresV2asusers";
    }
    else if ([sl isEqualToString:@"MS"])
    {
        //https://itg0.theatro.com/restapi/v2/app/chain/{chainId}/level/nodes
        urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/app/chain/%@/level/nodes",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
        apicode=@"SelectStoresV2nodes";
    }
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-SelectStores url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:[loginDetails objectForKey:@"twUserName"] forKey:@"twUserName"];
    [postData setObject:[loginDetails objectForKey:@"twPassword"] forKey:@"twPassword"];
    [postData setObject:[loginDetails objectForKey:@"twTxId"] forKey:@"twTxId"];
    [postData setObject:[loginDetails objectForKey:@"twMoment"] forKey:@"twMoment"];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Store-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", [loginDetails objectForKey:@"twUserName"], [loginDetails objectForKey:@"twPassword"]];
    
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-SelectStores http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"MS-SelectStores error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:apicode],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:apicode] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"MS-SelectStores responce %@",responsDic);
    }
    return responsDic;
    return 0;
}

+(NSDictionary *)selectGroupsASV3:(NSString *)userScreen
{
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    
    NSMutableString *urlString;
    
    urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/app/company/%@/as/groups",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
    //urlString =[NSMutableString stringWithFormat:@"%@restapi/v2/app/company/%@/groups/as",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-Selectgroups AS url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:[loginDetails objectForKey:@"twUserName"] forKey:@"twUserName"];
    [postData setObject:[loginDetails objectForKey:@"twPassword"] forKey:@"twPassword"];
    [postData setObject:[loginDetails objectForKey:@"twTxId"] forKey:@"twTxId"];
    [postData setObject:[loginDetails objectForKey:@"twMoment"] forKey:@"twMoment"];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", [loginDetails objectForKey:@"twUserName"], [loginDetails objectForKey:@"twPassword"]];
    
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    NSString *lang=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"]];
    [request setValue:lang forHTTPHeaderField:@"X-Theatro-Language"];
    
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"Selectgroups http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"MS-Selectgroups AS error is:%@",[self statusString:responce.statusCode]);
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"SelectGroupsASV3"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"SelectGroupsASV3"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"MS-Selectgroups AS responce %@",responsDic);
    }
    return responsDic;
}

+(NSDictionary *)hierarchyAll:(NSString *)userName password:(NSString *)password userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString ;
    urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/company/%@/all/mau/hierarchy/nodes?type=app",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-Hierachy url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    //[postData setObject:userName forKey:@"userName"];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", userName, password];
    
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-Hierachy http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"MS-Hierachy error is:%@",[self statusString:responce.statusCode]);
        NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"HierarchyNodes"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"HierarchyNodes"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"MS-Hierachy responce %@",responsDic);
    }
    return responsDic;
}


+(NSDictionary *)favSave :(NSString *)userName password:(NSString *)password jSonStr:(NSDictionary *)jSonStr userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString ;
    urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/company/%@/favourites?type=app",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-favSave url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSError *error=nil;
    
    
    // NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", userName, password];
    
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jSonStr
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-favSave http responce time: %f",sec);
    NSDictionary *responsDic;
    if (responce.statusCode==201)
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavSave"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"FavSave"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"MS-favSave error is:%@",[self statusString:responce.statusCode]);
        
        NSLog(@"MS-favSave responce %@",responsDic);
    }
    return responsDic;
}

+(NSMutableArray *)favFetch :(NSString *)userName password:(NSString *)password userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString ;
    urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/company/%@/favourites/fetch?type=app",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-favFetch url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSError *error=nil;
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", userName, password];
    
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-favFetch http responce time: %f",sec);
    NSMutableArray *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSMutableArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil],nil];
        NSLog(@"MS-favFetch error is:%@",[self statusString:responce.statusCode]);
        NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavFetch"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"FavFetch"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"MS-favFetch responce %@",responsDic);
    }
    return responsDic;
}

+(NSDictionary *)favDelete :(NSString *)userName password:(NSString *)password favDeleteID:(NSString *)favDeleteID userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString ;
    urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/company/%@/favourites/%@?type=app",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"],favDeleteID];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-favDelete url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"DELETE"];
    //initialize a post data
    NSError *error=nil;
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", userName, password];
    
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-favDelete http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"MS-favDelete error is:%@",[self statusString:responce.statusCode]);
        NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FavDelete"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"FavDelete"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"MS-favDelete responce %@",responsDic);
    }
    return responsDic;
}


+(NSDictionary *)hierarchyV2:(NSString *)userName password:(NSString *)password userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString ;
    urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/company/%@/root/hierarchy/levels?type=app",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-Hierachy url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", userName, password];
    
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-Hierachy http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"MS-Hierachy error is:%@",[self statusString:responce.statusCode]);
        NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"HierarchyLevels"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"HierarchyLevels"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"MS-Hierachy responce %@",responsDic);
    }
    return responsDic;
}

+(NSData *)hierarchyChildV2:(NSString *)levelId universalId:(NSString *)universalId userName:(NSString *)userName password:(NSString *)password storesState:(NSString *)storesState userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString ;
    urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/company/%@/hierarchy/level/%@/parent/%@?stores=%@&type=app",serverPath,[[NSUserDefaults standardUserDefaults] objectForKey:@"companyId"],levelId,universalId,storesState];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-HierachyChild url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", userName, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"hierarchyChildV2 http responce time: %f",sec);
    
    return data;
}

+(NSDictionary *)earboxlistV2:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId  userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/earbox/v2/company/%@",serverPath,[NSNumber numberWithInt:companyId]];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-Inboxlist url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-Inbox http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0)
    {
        if (responce.statusCode==200)
        {
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:@"success",@"twsuccessMessage", nil];
        }
        else
        {
            NSLog(@"MS-Inbox undeliverd list error code is:%ld",(long)responce.statusCode);
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:@"Server Error",@"Error", nil];
            NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
            NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"EarboxListV2"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
            [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
            [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"EarboxListV2"] severity:@"1" alarmdesc:staticerrorcode];
        }
    }
    else
    {
        
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
    }
    return responsDic;
    
    return 0;
}


+(NSDictionary *)earboxundeliveredlistV2:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId  twreqFor:(NSString *)twreqFor userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/earbox/v2/%@/inbox/undelivered/company/%@",serverPath,twreqFor,[NSNumber numberWithInt:companyId]];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-Inbox undelivered url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-Inbox undelivered http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0)
    {
        if (responce.statusCode==200)
        {
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:@"success",@"twsuccessMessage", nil];
        }
        else
        {
            NSLog(@"MS-Inbox undelivered error code is:%ld",(long)responce.statusCode);
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:@"Server Error",@"Error", nil];
            NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
            NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"EarboxundeliveredlistV2"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
            [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
            [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"EarboxundeliveredlistV2"] severity:@"1" alarmdesc:staticerrorcode];
        }
    }
    else
    {
        
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
    }
    return responsDic;
    
}

+(NSDictionary *)earboxundeliveredlistVeb:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId  twreqFor:(NSString *)twreqFor userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/earbox/v2/%@/inbox/undelivered/company/%@/store/%@/",serverPath,twreqFor,[NSNumber numberWithInt:companyId],[NSNumber numberWithInt:storeId]];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"Earbox undeliverd list http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 )
    {
        if (responce.statusCode==200)
        {
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:@"success",@"twsuccessMessage", nil];
        }
        else
        {
            NSLog(@"Earbox undeliverd list error code is:%ld",(long)responce.statusCode);
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:@"Server Error",@"Error", nil];
            NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
            NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"EarboxundeliveredlistVeb"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
            [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
            [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"EarboxundeliveredlistVeb"] severity:@"1" alarmdesc:staticerrorcode];
        }
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
    }
    
    return responsDic;
}

+(NSDictionary *)updatemodeV2:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment pushTxId:(NSString *)pushTxId modeId:(NSString *)modeId userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/earbox/v2/id/%@/mode/%@",serverPath,pushTxId,modeId];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-Updatemode url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-Updatemode http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 )
    {
        if (responce.statusCode==200)
        {
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:@"success",@"twsuccessMessage", nil];
        }
        else
        {
            switch ([userScreen intValue]) {
                case 1:
                    userScreen=@"InboxPrivateMsgInboxVC";
                    break;
                case 2:
                    userScreen=@"InboxPrivateMsgSavedVC";
                    break;
                case 3:
                    userScreen=@"InboxPrivateMsgSentVC";
                    break;
                case 4:
                    userScreen=@"InboxEveryoneInboxVC";
                    break;
                case 5:
                    userScreen=@"InboxEveryoneSavedVC";
                    break;
                case 6:
                    userScreen=@"InboxEveryoneSentVC";
                    break;
                case 7:
                    userScreen=@"InboxGroupMsgInboxVC";
                    break;
                case 8:
                    userScreen=@"InboxGroupMsgSavedVC";
                    break;
                case 9:
                    userScreen=@"InboxGroupMsgSentVC";
                    break;
                case 10:
                    userScreen=@"InboxStoreMsgInboxVC";
                    break;
                case 11:
                    userScreen=@"InboxStoreMsgSavedVC";
                    break;
                case 12:
                    userScreen=@"InboxStoreMsgSentVC";
                    break;
                    
                
            }
            NSLog(@"MS-Updatemode error code is:%ld",(long)responce.statusCode);
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:@"Server Error",@"Error", nil];
            NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
            NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"UpdatemodeV2"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
            [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
            [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"UpdatemodeV2"] severity:@"1" alarmdesc:staticerrorcode];
        }
    }
    else
    {
        
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
    }
    return responsDic;
    
}

+(NSDictionary *)updateheardstatusV2:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment pushTxId:(NSString *)pushTxId heardStatusId:(NSString *)heardStatusId userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/earbox/v2/id/%@/heard/%@",serverPath,pushTxId,heardStatusId];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-Update heardstatus url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-Update heardstatus http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 )
    {
        if (responce.statusCode==200)
        {
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:@"success",@"twsuccessMessage", nil];
        }
        else
        {
            switch ([userScreen intValue]) {
                case 1:
                    userScreen=@"InboxPrivateMsgInboxVC";
                    break;
                case 2:
                    userScreen=@"InboxPrivateMsgSavedVC";
                    break;
                case 3:
                    userScreen=@"InboxPrivateMsgSentVC";
                    break;
                case 4:
                    userScreen=@"InboxEveryoneInboxVC";
                    break;
                case 5:
                    userScreen=@"InboxEveryoneSavedVC";
                    break;
                case 6:
                    userScreen=@"InboxEveryoneSentVC";
                    break;
                case 7:
                    userScreen=@"InboxGroupMsgInboxVC";
                    break;
                case 8:
                    userScreen=@"InboxGroupMsgSavedVC";
                    break;
                case 9:
                    userScreen=@"InboxGroupMsgSentVC";
                    break;
                case 10:
                    userScreen=@"InboxStoreMsgInboxVC";
                    break;
                case 11:
                    userScreen=@"InboxStoreMsgSavedVC";
                    break;
                case 12:
                    userScreen=@"InboxStoreMsgSentVC";
                    break;
                    
                    
            }
            NSLog(@"MS-Update heardstatus error code is:%ld",(long)responce.statusCode);
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:@"Server Error",@"Error", nil];
            NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
            NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"UpdateheardstatusV2"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
            [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
            [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"UpdateheardstatusV2"] severity:@"1" alarmdesc:staticerrorcode];
        }
    }
    else
    {
        
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
    }
    return responsDic;
    
}

+(NSDictionary *)unheardV2:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/earbox/v2/unheard/company/%d",serverPath,companyId];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-Inbox unheard count url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-Inbox Unheard count http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"MS-Inbox Unheard count error is:%@",[self statusString:responce.statusCode]);
        NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"UnheardV2"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"UnheardV2"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"MS-Inbox Unheard count responce %@",responsDic);
    }
    return responsDic;
    
}

+(NSDictionary *)replystatusV2:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment pushTxId:(NSString *)pushTxId companyId:(int)companyId  userScreen:(NSString *)userScreen
{
    //POST Method
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/earbox/v2/replied/%@/company/%d",serverPath,pushTxId,companyId];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-reply status url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:twUserName forKey:@"twUserName"];
    [postData setObject:twPassword forKey:@"twPassword"];
    [postData setObject:twTxId forKey:@"twTxId"];
    [postData setObject:twMoment forKey:@"twMoment"];
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-reply status http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 )
    {
        if (responce.statusCode==200)
        {
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:@"success",@"twsuccessMessage", nil];
        }
        else
        {
            switch ([userScreen intValue]) {
                case 1:
                    userScreen=@"InboxPrivateMsgInboxVC";
                    break;
                case 2:
                    userScreen=@"InboxPrivateMsgSavedVC";
                    break;
                case 3:
                    userScreen=@"InboxPrivateMsgSentVC";
                    break;
                case 4:
                    userScreen=@"InboxEveryoneInboxVC";
                    break;
                case 5:
                    userScreen=@"InboxEveryoneSavedVC";
                    break;
                case 6:
                    userScreen=@"InboxEveryoneSentVC";
                    break;
                case 7:
                    userScreen=@"InboxGroupMsgInboxVC";
                    break;
                case 8:
                    userScreen=@"InboxGroupMsgSavedVC";
                    break;
                case 9:
                    userScreen=@"InboxGroupMsgSentVC";
                    break;
                case 10:
                    userScreen=@"InboxStoreMsgInboxVC";
                    break;
                case 11:
                    userScreen=@"InboxStoreMsgSavedVC";
                    break;
                case 12:
                    userScreen=@"InboxStoreMsgSentVC";
                    break;
                    
                    
            }
            NSLog(@"MS-reply status error code is:%ld",(long)responce.statusCode);
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:@"Server Error",@"Error", nil];
            NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
            NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"ReplystatusV2"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
            [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
            [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"ReplystatusV2"] severity:@"1" alarmdesc:staticerrorcode];
        }
    }
    else
    {
        
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
    }
    
    return responsDic;
    
}

+(NSDictionary *)appstoreVersioncheck
{
    NSURL *url = [NSURL URLWithString:@"http://itunes.apple.com/jp/lookup/?id=1016449175"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    NSError *error=nil;
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"appstore http responce time: %f",sec);
    NSDictionary *responsDic;
    if ([data length]==0 || responce.statusCode!=200)
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        NSLog(@"appstore error is:%@",[self statusString:responce.statusCode]);
    }
    else
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"appstore responce is%@",responsDic);
    }
    
    
    return responsDic;
}

+(NSDictionary *)logonC2ApiCall:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment boardid:(NSString *)boardid userScreen:(NSString *)userScreen
{
    NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    
    NSMutableString *urlString;
    //http://localhost:3608/restapi/web/logon/
    //restapi/v2/mapp/communicator/logon
    urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/mapp/communicator/logon",serverPath];
    //urlString =[NSMutableString stringWithFormat:@"http://192.168.1.218/restapi/web/logon/"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"logonC2ApiCall url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
    [postData setObject:[loginDetails objectForKey:@"twUserName"] forKey:@"twUserName"];
    [postData setObject:[loginDetails objectForKey:@"twPassword"] forKey:@"twPassword"];
    [postData setObject:[loginDetails objectForKey:@"twTxId"] forKey:@"twTxId"];
    [postData setObject:[loginDetails objectForKey:@"twMoment"] forKey:@"twMoment"];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [NSString stringWithFormat:@"Version %@",[infoDict objectForKey:@"CFBundleVersion"]];
    NSDictionary *userAgent=[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"os",[[UIDevice currentDevice] systemVersion],@"osVersion",version,@"appVersion",code,@"deviceModel",deviceTokenString,@"deviceId",@"",@"osBuildNumber",nil];
    [postData setObject:userAgent forKey:@"userAgent"];
    NSDictionary *logon=[NSDictionary dictionaryWithObjectsAndKeys:boardid,@"board_id",[[[NSUserDefaults standardUserDefaults] objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"],@"username",nil];
    [postData setObject:logon forKey:@"logon"];
    [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"logincredentials"];
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", twUserName, twPassword];
    
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    NSString *lang=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"]];
    [request setValue:lang forHTTPHeaderField:@"X-Theatro-Language"];
    
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"logonC2ApiCall http responce time: %f",sec);
    NSDictionary *responsDic,*errorDic;
    if ([data length]==0 )
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode] ,@"Error", nil];
        NSLog(@"logonC2ApiCall data nil");
        NSLog(@"logonC2ApiCall error is:%@",[self statusString:responce.statusCode]);
        NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"LogonC2ApiCall"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"LogonC2ApiCall"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        if (!(responce.statusCode==200))
        {
            errorDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSString *str =[errorDic objectForKey:@"message"];
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:str,@"Error", nil];
            NSLog(@"logonC2ApiCall error is:%@",[self statusString:responce.statusCode]);
            NSLog(@"logonC2ApiCall message from TCM is:%@",str);
            NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
            NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"LogonC2ApiCall"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
            [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
            [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"LogonC2ApiCall"] severity:@"1" alarmdesc:staticerrorcode];
            
        }
        else
        {
            
            responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            NSLog(@"logonC2ApiCall responce %@",responsDic);
        }
    }
    return responsDic;
}

+(NSMutableArray *)notificationFetch :(NSString *)userName password:(NSString *)password userScreen:(NSString *)userScreen

{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    
    NSMutableString *urlString ;
    
    //http://localhost/restapi/notification/filter/fetch
    
    urlString =[NSMutableString stringWithFormat:@"%@restapi/notification/filter/fetch",serverPath];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSLog(@"Notification Fetch url:%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    //set http method
    
    [request setHTTPMethod:@"POST"];
    
    //initialize a post data
    
    NSError *error=nil;
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", userName, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    //set post data of request
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"Notification Fetch http responce time: %f",sec);
    NSMutableArray *responsDic;
    
    if ([data length]==0 || responce.statusCode!=200)
        
    {
        responsDic=[NSMutableArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil],nil];
        
        NSLog(@"Notification Fetch error is:%@",[self statusString:responce.statusCode]);
        NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"NotificationFetch"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"NotificationFetch"] severity:@"1" alarmdesc:staticerrorcode];
        
    }
    
    else
        
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"Notification Fetch responce %@",responsDic);
        
    }
    
    return responsDic;
    
}
+(NSDictionary *)NotificationSave :(NSString *)userName password:(NSString *)password jSonStr:(NSMutableArray *)jSonStr userScreen:(NSString *)userScreen

{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    
    NSMutableString *urlString ;
    
    //http://localhost/restapi/notification/filter
    
    urlString =[NSMutableString stringWithFormat:@"%@restapi/notification/filter",serverPath];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSLog(@"MS-Notification Save url:%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    //set http method
    
    [request setHTTPMethod:@"POST"];
    
    //initialize a post data
    
    NSError *error=nil;
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", userName, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    //set post data of request
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jSonStr
                        
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                        
                                                         error:&error];
    
    NSString *jsonString;
    if (! jsonData) {
        
        NSLog(@"Got an error: %@", error);
        
    } else {
        
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-Notification http responce time: %f",sec);
    NSDictionary *responsDic;
    if (responce.statusCode==201)
    {
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
    }
    else
    {
        responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
        
        NSLog(@"MS-Notification save error is:%@",[self statusString:responce.statusCode]);
        NSLog(@"MS-Notification save responce %@",responsDic);
        NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"NotificationSave"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"NotificationSave"] severity:@"1" alarmdesc:staticerrorcode];
    }
    return responsDic;
}


+(NSMutableArray *)featureToggle :(NSString *)userName password:(NSString *)password userScreen:(NSString *)userScreen
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    NSMutableString *urlString ;
    //https://$SERVER/restapi/v2/app/feature/toggle
    urlString =[NSMutableString stringWithFormat:@"%@restapi/v2/app/feature/toggle",serverPath];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"MS-featureToggle url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSError *error=nil;
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", userName, password];
    
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    //set post data of request
    
    
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"MS-featureToggle http responce time: %f",sec);
    NSMutableArray *responsDic;
    if (responce.statusCode!=201)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"NotificationFilter"];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"SwitchtoC2"];
            responsDic=[NSMutableArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil],nil];
            NSLog(@"MS-featureToggle error is:%@",[self statusString:responce.statusCode]);
        NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FeatureToggle"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
        [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
        [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"FeatureToggle"] severity:@"1" alarmdesc:staticerrorcode];
    }
    else
    {
        
        responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if (responsDic.count>0)
        {
            
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"NotificationFilter"];
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"SwitchtoC2"];
            responsDic=[NSMutableArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil],nil];
            NSLog(@"MS-featureToggle error is:%@",[self statusString:responce.statusCode]);
            NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
            NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:userScreen],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"FeatureToggle"],[[DiagnosticTool sharedManager]generateErrorCode:[NSString stringWithFormat:@"%ld", (long)responce.statusCode]]];
            [[NSUserDefaults standardUserDefaults]setObject:staticerrorcode forKey:@"staticerrorcode"];
            [self alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"FeatureToggle"] severity:@"1" alarmdesc:staticerrorcode];
        }
        NSLog(@"MS-featureToggle responce %@",responsDic);
    }
    return responsDic;
}


+(NSDictionary *)alarmNotification:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment alarmcode:(NSString *)alarmcode severity:(NSString *)severity alarmdesc:(NSString *)alarmdesc
{
    NSString *serverPath=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
    //POST Method
    NSMutableString *urlString =[NSMutableString stringWithFormat:@"%@restapi/v3/app/alarms/create",serverPath];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"AlarmNotification url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSString *txId;
    if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
        txId=[[NSString stringWithFormat:@"%@",
                       [[[UIDevice currentDevice] identifierForVendor] UUIDString]]lowercaseString];
    }
   // NSString *storeName= [[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
   // NSString *chainName=[[[NSUserDefaults standardUserDefaults] objectForKey:@"twmAppLogin"] objectForKey:@"chainName"];
    NSMutableDictionary *postData=[NSMutableDictionary dictionary];
//    NSMutableDictionary *source=[NSMutableDictionary dictionary];
//    [source setObject:@"CloudMonitor" forKey:@"type"];
    NSMutableDictionary *alarmDic=[NSMutableDictionary dictionary];
  /*  if (chainName.length)
    {
        [alarmDic setObject:chainName forKey:@"chain"];
    }
    else
    {
        [alarmDic setObject:@"" forKey:@"chain"];
    }
    if (storeName.length)
    {
        [alarmDic setObject:storeName forKey:@"store"];
    }
    else
    {
        [alarmDic setObject:@"" forKey:@"store"];
    }*/
    
    
    
    NSMutableDictionary *details=[NSMutableDictionary dictionary];
    
    [details setObject:alarmcode forKey:@"alarmcode"];
    [details setObject:severity forKey:@"severity"];
    [details setObject:alarmdesc forKey:@"description"];
    [alarmDic setObject:details forKey:@"details"];
   // [alarmDic setObject:source forKey:@"source"];
     [postData setObject:alarmDic forKey:@"alarm"];
    
    NSString *deviceTokenString;
    if(deviceTokenString==nil && [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"]==nil)
    {
        deviceTokenString=@"";
    }
    else
    {
        deviceTokenString= [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceTokenString"];
    }
    NSError *error=nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                        
                                                         error:&error];
    NSString *jsonString;
    
    if (! jsonData) {
        NSLog(@"AlarmNotification-Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //set request content type we MUST set this value.
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", twUserName, twPassword];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //set post data of request
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    NSHTTPURLResponse *responce=nil;
    NSDate *date=[NSDate date];
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:&error];
    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
    NSLog(@"AlarmNotification http responce time: %f",sec);
    NSDictionary *responsDic;
        if (responce.statusCode==200)
        {
            responsDic=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"AlarmNotification responce %@",responsDic);
            
        }
        else
        {
            responsDic=[NSDictionary dictionaryWithObjectsAndKeys:[self statusString:responce.statusCode],@"Error", nil];
            NSLog(@"AlarmNotification error is:%@",[self statusString:responce.statusCode]);
        }
   
    return responsDic;
    
}


+(NSString *)statusString:(NSInteger)statuscode
{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Language"]==nil)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Localizable"
                                                         ofType:@"strings"
                                                    inDirectory:nil
                                                forLocalization:@"en"];
        
        // compiled .strings file becomes a "binary property list"
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
        
        [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"Language"];
        [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"ChoosedLang"];
    }
    
    NSDictionary *dict=[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    /*
     "Request time out"="la demande a expirée";
     "Non-Authoritative Information"="Informations non officielles";
     "No Content"="pas de contenu";
     "Reset Content"="Réinitialiser le contenu";
     "Temporary Redirect"="Redirection temporaire";
     "Permanent Redirect (experiemental)"="Redirection permanente (experiemental)";
     "Bad Request"="Mauvaise Demande";
     "Unauthorized"="Non autorisé";
     "Not Found"="Pas trouvé";
     "Proxy Authentication Required"="Authentification par proxy obligatoire";
     "Request Timeout"="la demande a expirée";
     "Upgrade Required"="Version révisée obligatoire";
     "Too Many Requests"="Trop de demandes";
     "No Response (Nginx)"="Pas de réponse (Nginx)";
     "Internal Server Error"="Erreur Interne du serveur";
     "Bad Gateway"="Mauvaise serveur GW";
     "Gateway Timeout"="serveur GW a expiré";
     "HTTP Version Not Supported"="Version HTTP pas supporté";
     "Check the internet connection"="Vérifiez la connexion internet";
     "Please check your internet connection."="Veuillez vérifier votre connexion internet.";
     */
    switch (statuscode) {
        case 0:
            return [dict objectForKey:@"Request time out"];
            break;
        case 100:
            return @"Continue";
            break;
        case 101:
            return @"Switching Protocols";
            break;
        case 102:
            return @"Processing (WebDAV)";
            break;
        case 200:
            return @"OK";
            break;
        case 201:
            return @"Created";
            break;
        case 202:
            return @"Accepted";
            break;
        case 203:
            return [dict objectForKey:@"Non-Authoritative Information"];
            break;
        case 204:
            return [dict objectForKey:@"No Content"];
            break;
        case 205:
            return [dict objectForKey:@"Reset Content"];
            break;
        case 206:
            return @"Partial Content";
            break;
        case 207:
            return @"Multi-Status (WebDAV)";
            break;
        case 208:
            return @"Already Reported (WebDAV)";
            break;
        case 226:
            return @"IM Used";
            break;
        case 300:
            return @"Multiple Choices";
            break;
        case 301:
            return @"Moved Permanently";
            break;
        case 302:
            return @"Found";
            break;
        case 303:
            return @"See Other";
            break;
        case 304:
            return @"Not Modified";
            break;
        case 305:
            return @"Use Proxy";
            break;
        case 306:
            return @"Unused";
            break;
        case 307:
            return [dict objectForKey:@"Temporary Redirect"];
            break;
        case 308:
            return [dict objectForKey:@"Permanent Redirect (experiemental)"];
            break;
        case 400:
            return [dict objectForKey:@"Bad Request"];
            break;
        case 401:
            return [dict objectForKey:@"Unauthorized"];
            break;
        case 402:
            return @"Payment Required";
            break;
        case 403:
            return @"Forbidden";
            break;
        case 404:
            return [dict objectForKey:@"Not Found"];
            break;
        case 405:
            return @"Method Not Allowed";
            break;
        case 406:
            return @"Not Acceptable";
            break;
        case 407:
            return [dict objectForKey:@"Proxy Authentication Required"];
            break;
        case 408:
            return [dict objectForKey:@"Request Timeout"];
            break;
        case 409:
            return @"Conflict";
            break;
        case 410:
            return @"Gone";
            break;
        case 411:
            return @"Length Required";
            break;
        case 412:
            return @"Precondition Failed";
            break;
        case 413:
            return @"Request Entity Too Large";
            break;
        case 414:
            return @"Request-URI Too Long";
            break;
        case 415:
            return @"Unsupported Media Type";
            break;
        case 416:
            return @"Requested Range Not Satisfiable";
            break;
        case 417:
            return @"Expectation Failed";
            break;
        case 418:
            return @"I'm a teapot (RFC 2324)";
            break;
        case 420:
            return @"Enhance Your Calm (Twitter)";
            break;
        case 422:
            return @"Unprocessable Entity (WebDAV)";
            break;
        case 423:
            return @"Locked (WebDAV)";
            break;
        case 424:
            return @"Failed Dependency (WebDAV)";
            break;
        case 425:
            return @"Reserved for WebDAV";
            break;
        case 426:
            return [dict objectForKey:@"Upgrade Required"];
            break;
        case 428:
            return @"Precondition Required";
            break;
        case 429:
            return [dict objectForKey:@"Too Many Requests"];
            break;
        case 431:
            return @"Request Header Fields Too Large";
            break;
        case 444:
            return [dict objectForKey:@"No Response (Nginx)"];
            break;
        case 449:
            return @"Retry With (Microsoft)";
            break;
        case 450:
            return @"Blocked by Windows Parental Controls (Microsoft)";
            break;
        case 451:
            return @"Unavailable For Legal Reasons";
            break;
        case 499:
            return @"Client Closed Request (Nginx)";
            break;
        case 500:
            return [dict objectForKey:@"Internal Server Error"];
            break;
        case 501:
            return @"Not Implemented";
            break;
        case 502:
            return [dict objectForKey:@"Bad Gateway"];
            break;
        case 503:
            return @"Service Unavailable";
            break;
        case 504:
            return [dict objectForKey:@"Gateway Timeout"];
            break;
        case 505:
            return [dict objectForKey:@"HTTP Version Not Supported"];
            break;
        case 506:
            return @"Variant Also Negotiates (Experimental)";
            break;
        case 507:
            return @"Insufficient Storage (WebDAV)";
            break;
        case 508:
            return @"Loop Detected (WebDAV)";
            break;
        case 509:
            return @"Bandwidth Limit Exceeded (Apache)";
            break;
        case 510:
            return @"Not Extended";
            break;
        case 511:
            return @"Network Authentication Required";
            break;
        case 598:
            return @"Network read timeout error";
            break;
        case 599:
            return @"Network connect timeout error";
            break;
        default:
            return @"Request time out";
            break;
    }
    return 0;
}


@end
