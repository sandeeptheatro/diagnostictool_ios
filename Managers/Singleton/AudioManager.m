//
//  AudioManager.m
//  Managers
//
//  Created by Abhi C on 25/5/2015
//

#import "AudioManager.h"
#import "StateMachineHandler.h"
#import "AnnouncementViewController.h"
#import "ContactsDetailViewController.h"
#import "UIViewController.h"
#import "AFHTTPSessionManager.h"
#import <CoreMedia/CoreMedia.h>
#import "InstoreData.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "HttpHandler.h"
#import "DiagnosticTool.h"

@interface AudioManager ()
{
    NSArray *pathComponents;
    BOOL deletefile;
    UInt32                  m_iOutputBus;
    UInt32                  m_iInputBus;
    AUDIO_DEVICE_STATUS     m_iStatus;
    CBCentralManager *bluetooth;
    BOOL isBlueToothOn;
}
static OSStatus recordingCallback(void* inRefCon,AudioUnitRenderActionFlags* ioActionFlags,const AudioTimeStamp* inTimeStamp,UInt32 inBusNumber,UInt32 inNumberFrames,AudioBufferList* ioData);

static OSStatus playbackCallback(void *inRefCon, AudioUnitRenderActionFlags *ioActionFlags, const AudioTimeStamp *inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList *ioData);

@end

@implementation AudioManager
@synthesize player,recorder;
static  AudioManager *sharedMgr = nil;
BOOL isAudioInitialized,audioPlaybackStarted;


+(AudioManager *)getsharedInstance {
    if (sharedMgr == nil)
    {
        sharedMgr = [[AudioManager alloc] init];
        sharedMgr->m_iStatus = AUDIO_DEVICE_UNINITIALIZED;
    }
    return sharedMgr;
}

AudioComponentInstance ioUnit;

static OSStatus recordingCallback(void* inRefCon,AudioUnitRenderActionFlags* ioActionFlags,const AudioTimeStamp* inTimeStamp,UInt32 inBusNumber,UInt32 inNumberFrames,AudioBufferList* ioData)
{
    AudioBuffer buffer;
    
    buffer.mNumberChannels = 1;
    buffer.mDataByteSize = inNumberFrames * 2;
    buffer.mData = malloc( inNumberFrames * 2 );
    
    // Put buffer in a AudioBufferList
    AudioBufferList bufferList;
    bufferList.mNumberBuffers = 1;
    bufferList.mBuffers[0] = buffer;
    
    // Then:
    // Obtain recorded samples
    
    OSStatus result;
    
    result = AudioUnitRender(ioUnit,
                             ioActionFlags,
                             inTimeStamp,
                             inBusNumber,
                             inNumberFrames,
                             &bufferList);
    printError(result);
    
    //send the recorded data to Native...
    sendDataToNative(buffer.mData,inNumberFrames * sizeof(signed short));
    
    // release the malloc'ed data in the buffer we created earlier
    free(bufferList.mBuffers[0].mData);
    
    return noErr;
    
}

static OSStatus playbackCallback(void *inRefCon, AudioUnitRenderActionFlags *ioActionFlags, const AudioTimeStamp *inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList *ioData)
{
    for (int i=0; i < ioData->mNumberBuffers; i++) { // in practice we will only ever have 1 buffer, since audio format is mono
        AudioBuffer buffer = ioData->mBuffers[i];
        
        
        //Read the data from native...
        unsigned int bytes_read = readDataFromNative(buffer.mData,buffer.mDataByteSize);
        buffer.mDataByteSize = bytes_read;
        
        unsigned int playbackSpace = getAvailablePlaybackDataFromNative();
        
        if(playbackSpace == 0)
        {
            if([AudioManager getsharedInstance]->m_iStatus == AUDIO_DEVICE_NATIVE_PLAYBACK)
            {
                [NSThread detachNewThreadSelector:@selector(stopNativeAudioPlaybackThread) toTarget:sharedMgr withObject:nil];
            }
            
            *ioActionFlags |= kAudioUnitRenderAction_OutputIsSilence;
            memset(buffer.mData,0xff,buffer.mDataByteSize);
        }
        
    }
    
    return noErr;
}

-(void)stopNativeAudioPlaybackThread
{
    [[AudioManager getsharedInstance] stopNativeAudioPlayback];
    clearOraIdFromNative();
}

void printError(int result){
    if (result) {
        NSLog(@"Status not 0! %d\n", result);
        //		exit(1);
    }
}

- (void)interruption:(NSNotification*)notification {
    // get the user info dictionary
    NSDictionary *interuptionDict = notification.userInfo;
    // get the AVAudioSessionInterruptionTypeKey enum from the dictionary
    NSInteger interuptionType = [[interuptionDict valueForKey:AVAudioSessionInterruptionTypeKey] integerValue];
    // decide what to do based on interruption type here...
    switch (interuptionType) {
        case AVAudioSessionInterruptionTypeBegan:
            NSLog(@"Audio Session Interruption case started.");
            [self stopAudio];
            break;
            
        case AVAudioSessionInterruptionTypeEnded:
            NSLog(@"Audio Session Interruption case ended.");
            [self startAudio];
            break;
            
        default:
            NSLog(@"Audio Session Interruption Notification case default.");
            break;
    }
}

- (void)routeChange:(NSNotification*)notification {
    
    
    NSDictionary *interuptionDict = notification.userInfo;
    
    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    
    switch (routeChangeReason) {
        case AVAudioSessionRouteChangeReasonUnknown:
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonUnknown");
            break;
            
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            // a headset was added or removed
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonNewDeviceAvailable");
            break;
            
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
            // a headset was added or removed
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonOldDeviceUnavailable");
            break;
            
        case AVAudioSessionRouteChangeReasonCategoryChange:
            // called at start - also when other audio wants to play
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonCategoryChange");
            break;
            
        case AVAudioSessionRouteChangeReasonOverride:
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonOverride");
            break;
            
        case AVAudioSessionRouteChangeReasonWakeFromSleep:
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonWakeFromSleep");
            break;
            
        case AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory:
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory");
            break;
            
        default:
            break;
    }
}



-(void)startAudio
{
    if (m_iStatus == AUDIO_DEVICE_FILE_PLAYBACK)
    {
        NSLog(@"AUDIO_DEVICE_FILE_PLAYBACK restarted");
        [player play];
    }
    else if (m_iStatus == AUDIO_DEVICE_RECORD_TOFILE)
    {
        NSLog(@"AUDIO_DEVICE_RECORD_TOFILE restarted");
        [recorder record];
        [[AnnouncementViewController getsharedInstance] audioInterruptionEnd];
    }
    else if (m_iStatus == AUDIO_DEVICE_NATIVE_PLAYBACK)
    {
        NSLog(@"AUDIO_DEVICE_NATIVE_PLAYBACK restarted");
        AudioUnitInitialize(ioUnit);
        AudioOutputUnitStart(ioUnit);
    }
    else if (m_iStatus == AUDIO_DEVICE_NATIVE_RECORD)
    {
    }
}

-(void)stopAudio
{
    if (m_iStatus == AUDIO_DEVICE_FILE_PLAYBACK)
    {
        [player stop];
        NSLog(@"AUDIO_DEVICE_FILE_PLAYBACK paused");
    }
    else if (m_iStatus == AUDIO_DEVICE_RECORD_TOFILE)
    {
        [recorder stop];
        NSLog(@"AUDIO_DEVICE_RECORD_TOFILE paused");
    }
    else if (m_iStatus == AUDIO_DEVICE_NATIVE_PLAYBACK)
    {
        NSLog(@"AUDIO_DEVICE_NATIVE_PLAYBACK paused");
        AudioOutputUnitStop(ioUnit);
        AudioUnitUninitialize(ioUnit);
    }
    else if (m_iStatus == AUDIO_DEVICE_NATIVE_RECORD)
    {
        NSLog(@"AUDIO_DEVICE_NATIVE_RECORD paused");
    }
}


-(void) initAudio
{
    [[AudioManager getsharedInstance] initAudioManager];
    [[AudioManager getsharedInstance] createAudioDevice];
    isAudioInitialized = YES;
}


-(BOOL) shutDownAudio
{
    NSLog(@"ShutDown Audio called SuccessFull.........\n");
    if(isAudioInitialized)
    {
        if(m_iStatus != AUDIO_DEVICE_UNINITIALIZED)
        {
            //returning Audio Session...
            NSError *deactivationError = nil;
            BOOL success = [[AVAudioSession sharedInstance] setActive:NO error:&deactivationError];
            if (!success) {
                NSLog(@"%@", [deactivationError localizedDescription]);
            }
            
            [self shutdownNativeAudio];
            isAudioInitialized = NO;
        }
    }
    return YES;
}


-(void)initAudioManager
{
    m_iInputBus = 1;
    m_iOutputBus = 0;
    m_iStatus = AUDIO_DEVICE_FREE;
}

-(void)createAudioDevice
{
    [ self setUpAudioSession];
    [ self setUpAudioDevice];
}


//lots of setup required
-(OSStatus)setUpAudioSession
{
    OSStatus status = 0;
    BOOL success = false;
    
    //Instantiate an instance of the AVAudioSession object.
    AVAudioSession * audioSession = [AVAudioSession sharedInstance];
    
    NSError *activationError = nil;
    success = [[AVAudioSession sharedInstance] setActive: YES error: &activationError];
    if(!success)
    {
        NSLog(@"error doing outputaudioportoverride - %@", [activationError localizedDescription]);
    }
    
    NSError *error = nil;
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"loud"]isEqualToString:@"loud"])
    {
        if([self isBluetoothHeadsetConnected])
        {
            [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:&error];
            [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
            //[[AVAudioSession sharedInstance] setActive: YES error: nil];
            
        }
        else
        {
            [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
        }
        
        
        
        success = [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
        
    }
    else
    {
        if([self isBluetoothHeadsetConnected])
        {
            [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:&error];
            [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
            //[[AVAudioSession sharedInstance] setActive: YES error: nil];
            
        }
        else{
        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
        }
        success = [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
        
    }

    if(!success)
    {
        NSLog(@"error doing outputaudioportoverride - %@", [error localizedDescription]);
    }
    
    
    // register for notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(interruption:)
                                                 name:AVAudioSessionInterruptionNotification
                                               object:nil];
    return status;
}

-(OSStatus)setUpAudioDevice
{
    OSStatus result;
    
    // Describe audio component
    AudioComponentDescription desc;
    desc.componentType = kAudioUnitType_Output;
    desc.componentSubType = kAudioUnitSubType_VoiceProcessingIO;
    desc.componentFlags = 0;
    desc.componentFlagsMask = 0;
    desc.componentManufacturer = kAudioUnitManufacturer_Apple;
    
    // Get component
    AudioComponent inputComponent = AudioComponentFindNext(NULL, &desc);
    
    // Get audio units
    result = AudioComponentInstanceNew(inputComponent, &ioUnit);
    printError(result);
    
#if 1
    // Enable IO for recording
    UInt32 flag = 1;
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Input,
                                  m_iInputBus,
                                  &flag,
                                  sizeof(flag));
    printError(result);
    
    // Enable IO for playback
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Output,
                                  m_iOutputBus,
                                  &flag,
                                  sizeof(flag));
    
    printError(result);
    
#endif
    
    
    // Describe format
    AudioStreamBasicDescription audioFormat;
    audioFormat.mSampleRate			= 8000.00;
    audioFormat.mFormatID			= kAudioFormatLinearPCM;
    audioFormat.mFormatFlags		= kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
    audioFormat.mFramesPerPacket	= 1;
    audioFormat.mChannelsPerFrame	= 1;
    audioFormat.mBitsPerChannel		= 16;
    audioFormat.mBytesPerPacket		= 2;
    audioFormat.mBytesPerFrame		= 2;
    
    // Apply format
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Output,
                                  m_iInputBus,
                                  &audioFormat,
                                  sizeof(audioFormat));
    printError(result);
    
    
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input,
                                  m_iOutputBus,
                                  &audioFormat,
                                  sizeof(audioFormat));
    
    printError(result);
    
    // Set input callback
    AURenderCallbackStruct callbackStruct;
    callbackStruct.inputProc = recordingCallback;
    //callbackStruct.inputProcRefCon = (__bridge void *)self;
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioOutputUnitProperty_SetInputCallback,
                                  kAudioUnitScope_Global,
                                  m_iInputBus,
                                  &callbackStruct,
                                  sizeof(callbackStruct));
    printError(result);
    
    // Set output callback
    callbackStruct.inputProc = playbackCallback;
    //callbackStruct.inputProcRefCon = self;
    
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioUnitProperty_SetRenderCallback,
                                  kAudioUnitScope_Global,
                                  m_iOutputBus,
                                  &callbackStruct,
                                  sizeof(callbackStruct));
    printError(result);
    
    // Disable buffer allocation for the recorder (optional - do this if we want to pass in our own)
    flag = 0;
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioUnitProperty_ShouldAllocateBuffer,
                                  kAudioUnitScope_Output,
                                  m_iInputBus,
                                  &flag,
                                  sizeof(flag));
    
    return result;
    
}


-(void) switchAudioOutput
{
    NSError *error = nil;
    BOOL  success;
    AVAudioSession *session = [AVAudioSession sharedInstance];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"loud"]isEqualToString:@"loud"])
    {
        //
        if([self isBluetoothHeadsetConnected])
        {
        [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:&error];
            [session setCategory:AVAudioSessionCategoryPlayback error:nil];
            //[[AVAudioSession sharedInstance] setActive: YES error: nil];

        }
        else
        {
            [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
        }
        
        success = [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
        
        

        
    }
    else
    {
        if([self isBluetoothHeadsetConnected])
        {
            [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:&error];
            [session setCategory:AVAudioSessionCategoryPlayback error:nil];
            
            
        }
        else
        {
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
        }
        success = [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
    }
    
    if(!success)
    {
        NSLog(@"error doing outputaudioportoverride - %@", [error localizedDescription]);
    }
}

- (BOOL) isBluetoothHeadsetConnected
{
    [self startBluetoothStatusMonitoring];
    
    if (isBlueToothOn)
    {
        
    AVAudioSession *session = [AVAudioSession sharedInstance];
    AVAudioSessionRouteDescription *routeDescription = [session currentRoute];
    
    NSLog(@"Current Routes : %@", routeDescription);
    
    if (routeDescription)
    {
        NSArray *outputs = [routeDescription outputs];
        
        if (outputs && [outputs count] > 0)
        {
            AVAudioSessionPortDescription *portDescription = [outputs objectAtIndex:0];
            NSString *portType = [portDescription portType];
            
            NSLog(@"dataSourceName : %@", portType);
            
            if (portType && ([portType isEqualToString:@"BluetoothA2DPOutput"] ||[portType isEqualToString:@"Receiver"]))
            {
                return YES;
            }
        }
    }
    }
    return NO;
}

- (void)startBluetoothStatusMonitoring {
    // Horrible formatting, but nicer for blog-width!
    bluetooth = [[CBCentralManager alloc]
                             initWithDelegate:self
                 queue:dispatch_get_main_queue()
                 options:@{CBCentralManagerOptionShowPowerAlertKey: @(NO)}];
    
    
}



#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    
    if ([central state] == CBCentralManagerStatePoweredOn)
    {
        NSLog(@"bluetooth on");
        isBlueToothOn=YES;
    }
    else
    {
        NSLog(@"bluetooth not on");
        isBlueToothOn=NO;
    }
}

//native audo start and stop functions....

// shut down the native audio system
-(BOOL) shutdownNativeAudio
{
    NSLog(@"shutdownNativeAudio Audio called SuccessFull ........\n");
    if(m_iStatus != AUDIO_DEVICE_FREE)
    {
        audioPlaybackStarted = NO;
        AudioOutputUnitStop(ioUnit);
        AudioUnitUninitialize(ioUnit);
        m_iStatus = AUDIO_DEVICE_UNINITIALIZED;
    }
    AudioComponentInstanceDispose(ioUnit);
    return YES;
}

-(BOOL) startNativeAudioRecording
{
    
    OSStatus result;
    
    if(m_iStatus == AUDIO_DEVICE_NATIVE_PLAYBACK)
    {
        [[AudioManager getsharedInstance] stopNativeAudioPlayback];
        // sleep(0.1);
    }
    
    m_iStatus = AUDIO_DEVICE_NATIVE_RECORD;
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Enable IO for recording
    UInt32 flag = 1;
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Input,
                                  m_iInputBus,
                                  &flag,
                                  sizeof(flag));
    printError(result);
    
    // Disable IO for playback
    flag = 0;
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Output,
                                  m_iOutputBus,
                                  &flag,
                                  sizeof(flag));
    
    printError(result);
    
    // Initialise
    result = AudioUnitInitialize(ioUnit);
    
    printError(result);
    
    result = AudioOutputUnitStart(ioUnit);
    printError(result);
    
    return true;
}

-(BOOL)stopNativeAudioRecording
{
    AudioOutputUnitStop(ioUnit);
    AudioUnitUninitialize(ioUnit);
    
    m_iStatus = AUDIO_DEVICE_FREE;
    
    if(getAvailablePlaybackDataFromNative() > 400)
    {
        [[AudioManager getsharedInstance] startNativeAudioPlayback];
    }
    
    return YES;
}

-(BOOL) startNativeAudioPlayback
{
    OSStatus result;
    
    if(m_iStatus != AUDIO_DEVICE_NATIVE_RECORD && m_iStatus != AUDIO_DEVICE_NATIVE_PLAYBACK)
    {
        audioPlaybackStarted=YES;
        m_iStatus = AUDIO_DEVICE_NATIVE_PLAYBACK;
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"EarboxViewController"] || [NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"BroadcastViewController"] || [NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"ContactsDetailViewController"] || [NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"GroupsDetailViewController"] || [NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"InterruptViewController"])
        {
            BOOL  success;
            NSError *error = nil;
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"loud"] isEqualToString:@"loud"])
            {
                if([self isBluetoothHeadsetConnected])
                {
                    [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:&error];
                    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
                    //[[AVAudioSession sharedInstance] setActive: YES error: nil];
                    
                }
                else
                {
                [session setCategory:AVAudioSessionCategoryPlayback error:nil];
                }
                success = [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
            }
            else
            {
                if([self isBluetoothHeadsetConnected])
                {
                    [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:&error];
                    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
                    //[[AVAudioSession sharedInstance] setActive: YES error: nil];
                    
                }
                else
                {
                [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
                }
                
                success = [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
                
            }
            if(!success)
            {
                NSLog(@"error doing outputaudioportoverride - %@", [error localizedDescription]);
            }
            
            
        }
        else
        {
            [session setCategory:AVAudioSessionCategoryPlayback error:nil];
        }
        
        
        
        
        // Disable IO for recording
        UInt32 flag = 0;
        result = AudioUnitSetProperty(ioUnit,
                                      kAudioOutputUnitProperty_EnableIO,
                                      kAudioUnitScope_Input,
                                      m_iInputBus,
                                      &flag,
                                      sizeof(flag));
        printError(result);
        
        // Enabel IO for playback
        flag = 1;
        result = AudioUnitSetProperty(ioUnit,
                                      kAudioOutputUnitProperty_EnableIO,
                                      kAudioUnitScope_Output,
                                      m_iOutputBus,
                                      &flag,
                                      sizeof(flag));
        
        printError(result);
        
        // Initialise
        result = AudioUnitInitialize(ioUnit);
        printError(result);
        
        result = AudioOutputUnitStart(ioUnit);
        printError(result);
        return YES;
        
    }
    else
    {
        //not starting the plaback to give the record precedence...
        return NO;
    }
}


-(BOOL) stopNativeAudioPlayback
{
    if (audioPlaybackStarted)
    {
        NSLog(@"Audio: stopNativeAudioPlayback called");
        audioPlaybackStarted=NO;
        AudioOutputUnitStop(ioUnit);
        AudioUnitUninitialize(ioUnit);
        m_iStatus = AUDIO_DEVICE_FREE;
        return YES;
    }
    return 0;
}

//* sandeep app level audio api's*/


-(void)sentaudioPath:(NSString *)audiofile oldurl:url
{
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    NSURL *targetPath= [documentsDirectoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.wav",audiofile]];
    [[NSFileManager defaultManager]copyItemAtURL:url toURL:targetPath error:nil];
}

-(void)createEarboxAudioPath:(NSString *)audiofile callId:(NSString *)audioid earboxName:(NSString *)earboxName callTxid:(NSString *)callTxid
{
    
    NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSError *error = nil;
    NSArray *filecount=[[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:&error];
    NSUInteger dbcount;
    if([filecount containsObject:@"nslog.log"])
    {
        dbcount=filecount.count;
    }
    else
    {
         dbcount=3;
    }
    
    if (dbcount<4 && dbcount<3)
    {
        
    }
    else
    {
        NSString *audiocallid=[NSString stringWithFormat:@"%@.wav",audioid];
        if (![filecount containsObject:audiocallid])
        {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    //Most URLs I come across are in string format so to convert them into an NSURL and then instantiate the actual request
    
    if ([[InstoreData sharedManager] isInStoreTGS])
    {
        NSString *ip=[[InstoreData sharedManager] getInStoreTgsIpAddress];
        
        if (audiofile.length>0)
        {
            NSURL* url = [NSURL URLWithString:audiofile];
            NSArray* reducedUrl2 =url.pathComponents;
            NSMutableString *mutableString=[[NSMutableString alloc]init];
            for (NSString *str in reducedUrl2)
            {
                if ([str isEqualToString:@"/"])
                {
                    //[mutableString appendString:[NSString stringWithFormat:@"%@",str]];
                }
                else
                {
                    [mutableString appendString:[NSString stringWithFormat:@"/%@",str]];
                }
            }
            NSString *finalStr = [NSString stringWithFormat:@"http://%@:8088%@",ip,mutableString];
    
        audiofile=finalStr;
        }
   }

    NSURL *URL = [NSURL URLWithString:audiofile];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
            
            NSDate *date=[NSDate date];
            
            //Watch the manager to see how much of the file it's downloaded
            [manager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
                //Convert totalBytesWritten and totalBytesExpectedToWrite into floats so that percentageCompleted doesn't get rounded to the nearest integer
                CGFloat written = totalBytesWritten;
                CGFloat total = totalBytesExpectedToWrite;
                CGFloat percentageCompleted = written/total;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSTimeInterval sec=[[NSDate date] timeIntervalSinceDate:date];
                    NSLog(@"Audio download http responce time :%f",sec);
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"percentageCompleted" object:[NSArray arrayWithObjects:[[NSNumber numberWithFloat:percentageCompleted] stringValue],callTxid,earboxName, nil]];
                });
            }];

    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        NSLog(@"This %@ %@ audio file is downloaded",audioid,earboxName);
        return [documentsDirectoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.wav",audioid]];
        
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if (error)
        {
            NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
            [myQueue addOperationWithBlock:^{
                NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:earboxName],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioDownload"],[[DiagnosticTool sharedManager]generateErrorCode:@"AudioDownload"]];
                NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
                [HttpHandler alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"AudioDownload"] severity:@"0" alarmdesc:staticerrorcode];
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                    
                }];
            }];
            NSLog(@"This %@ %@ %@ audio file is not downloaded",audioid,earboxName,error);
        }
        else
        {
            NSLog(@"This %@ %@ audio file is downloaded",audioid,earboxName);
        }
    }];
    [downloadTask resume];
  }
        
 }
}

-(void)createEarboxAudioPathCellTapped:(NSString *)audiofile callId:(NSString *)audioid earboxName:(NSString *)earboxName callTxid:(NSString *)callTxid
{
    
    NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSError *error = nil;
    NSArray *filecount=[[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:&error];
    
    
    NSUInteger dbcount;
    if([filecount containsObject:@"nslog.log"])
    {
        dbcount=filecount.count;
    }
    else
    {
        dbcount=3;
    }
    
    if (dbcount<4 && dbcount<3)
    {
        
    }
    else
    {
        NSString *audiocallid=[NSString stringWithFormat:@"%@.wav",audioid];
        if (![filecount containsObject:audiocallid])
        {
            NSURLSessionConfiguration *downloadconfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
            AFURLSessionManager *downloadmanager = [[AFURLSessionManager alloc] initWithSessionConfiguration:downloadconfiguration];
            
            //Most URLs I come across are in string format so to convert them into an NSURL and then instantiate the actual request
            
            if ([[InstoreData sharedManager] isInStoreTGS])
            {
                NSString *ip=[[InstoreData sharedManager] getInStoreTgsIpAddress];
                
                if (audiofile.length>0)
                {
                    NSURL* url = [NSURL URLWithString:audiofile];
                    NSArray* reducedUrl2 =url.pathComponents;
                    NSMutableString *mutableString=[[NSMutableString alloc]init];
                    for (NSString *str in reducedUrl2)
                    {
                        if ([str isEqualToString:@"/"])
                        {
                            //[mutableString appendString:[NSString stringWithFormat:@"%@",str]];
                        }
                        else
                        {
                            [mutableString appendString:[NSString stringWithFormat:@"/%@",str]];
                        }
                    }
                    NSString *finalStr = [NSString stringWithFormat:@"http://%@:8088/%@",ip,mutableString];
                    
                    audiofile=finalStr;
                }
            }
            
            NSURL *URL = [NSURL URLWithString:audiofile];
            NSURLRequest *request = [NSURLRequest requestWithURL:URL];
            
            //Watch the manager to see how much of the file it's downloaded
            [downloadmanager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
                //Convert totalBytesWritten and totalBytesExpectedToWrite into floats so that percentageCompleted doesn't get rounded to the nearest integer
                CGFloat written = totalBytesWritten;
                CGFloat total = totalBytesExpectedToWrite;
                CGFloat percentageCompleted = written/total;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"percentageCompleted" object:[NSArray arrayWithObjects:[[NSNumber numberWithFloat:percentageCompleted] stringValue],callTxid,earboxName, nil]];
                });
            }];
            
            
            
            NSURLSessionDownloadTask *downloadTask = [downloadmanager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
                
                NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
                NSLog(@"This %@ %@ audio file is downloaded",audioid,earboxName);
                return [documentsDirectoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.wav",audioid]];
                
            } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                if (error)
                {
                    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
                    [myQueue addOperationWithBlock:^{
                        NSString *staticerrorcode=[NSString stringWithFormat:@"%@%@%@%@",[[DiagnosticTool sharedManager] generateScreenCode:earboxName],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AudioDownload"],[[DiagnosticTool sharedManager]generateErrorCode:@"AudioDownload"]];
                        NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
                        [HttpHandler alarmNotification:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] alarmcode:[[DiagnosticTool sharedManager]generateAlarmerrorCode:@"AudioDownload"] severity:@"0" alarmdesc:staticerrorcode];
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            
                            
                        }];
                    }];
                    NSLog(@"This %@ %@ %@ audio file is not downloaded",audioid,earboxName,error);
                }
                else
                {
                    NSLog(@"This %@ %@ audio file is downloaded",audioid,earboxName);
                }
            }];
            [downloadTask resume];
        }
    }
}

-(void)deleteEarboxAudioPath:(NSString *)audioid
{
    
    NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSError *error = nil;
    
    for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:&error]) {
        NSString *audiocallid=[NSString stringWithFormat:@"%@.wav",audioid];
        if ([file isEqualToString:audiocallid])
        {
            [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:file] error:&error];
        }
    }
}

-(void)deleteallEarboxAudioPath
{
    
    NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSError *error = nil;
    
    for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:&error]) {
        if ([file isEqualToString:@"Earbox.sqlite"] || [file isEqualToString:@"Earbox.sqlite-shm"] || [file isEqualToString:@"Earbox.sqlite-wal"] || [file isEqualToString:@"nslog.log"])
        {
            //don't delete DataBase
        }
        else
        {
            NSLog(@"Mangers audio file deleted:%@",[folderPath stringByAppendingPathComponent:file]);
            [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:file] error:&error];
        }
    }
    NSString *folderPath1 = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath1 error:&error]) {
        if ([file isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:@"storeNameString"]])
        {
            [[NSFileManager defaultManager] removeItemAtPath:[folderPath1 stringByAppendingPathComponent:file] error:&error];
            
        }
        else
        {
            //don't delete DataBase
        }
    }
}

-(NSURL *)getEarboxAudioPath:(NSString *)audioid
{
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    NSString *audiocallid=[NSString stringWithFormat:@"%@.wav",audioid];
    NSURL *respone= [documentsDirectoryURL URLByAppendingPathComponent:audiocallid];
    return respone;
    
}

-(double)getAudioduration:(NSURL *)audioUrl
{
    AVURLAsset* audioAsset = [AVURLAsset URLAssetWithURL:audioUrl options:nil];
    CMTime audioDuration = audioAsset.duration;
    return CMTimeGetSeconds(audioDuration);
}


-(void)path
{
    NSString *millisec=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]*1000];
    NSArray *tempArray = [millisec componentsSeparatedByString:@"."];
    [[NSUserDefaults standardUserDefaults]setObject:[tempArray objectAtIndex:0] forKey:@"calltxid"];
    NSString *path=[NSString stringWithFormat:@"%@.wav",[tempArray objectAtIndex:0]];
    pathComponents = [NSArray arrayWithObjects:
                      [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                      path,
                      nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatULaw] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithShort:8000] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt:1] forKey:AVNumberOfChannelsKey];
    NSError *error;
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:&error];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    if (error)
    {
        NSLog(@"error: %@", [error localizedDescription]);
    } else {
        [recorder record];
    }
}


- (void)recordPauseTapped
{
    AVAudioSession* audioSession = [AVAudioSession sharedInstance];
    if([self isBluetoothHeadsetConnected])
    {
        
        // create and set up the audio session
        [audioSession setCategory: AVAudioSessionCategoryPlayAndRecord error: nil];
        [audioSession setActive: YES error: nil];
        
        // set up for bluetooth microphone input
        UInt32 allowBluetoothInput = 1;
        OSStatus stat = AudioSessionSetProperty (
                                                 kAudioSessionProperty_OverrideCategoryEnableBluetoothInput,
                                                 sizeof (allowBluetoothInput),
                                                 &allowBluetoothInput
                                                 );
        NSLog(@"status = %x", (int)stat);    // problem if this is not zero
        
        // check the audio route
        //UInt32 size = sizeof(CFStringRef);
      //  CFStringRef route;
        //OSStatus result = AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &size, &route);
       // NSLog(@"route = %@", route);
        // if bluetooth headset connected, should be "HeadsetBT"
        // if not connected, will be "ReceiverAndMicrophone"
        
        
    }
    else
    {
        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    }
      [self path];
   
}

-(void)recordstop
{
    [recorder stop];
}

-(void)audioplayerstop
{
    if (player.playing) {
        [player stop];
    }
}

- (void)stopTapped
{
    [recorder stop];
    m_iStatus = AUDIO_DEVICE_FREE;
}

- (void)playTapped
{
    [recorder stop];
    if (!recorder.recording){
        //Sandeep's code...
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayback error:nil];
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
        [player setDelegate:self];
        [player play];
        
        m_iStatus = AUDIO_DEVICE_FILE_PLAYBACK;
    }
}

- (void)deleteTapped
{
    deletefile=YES;
}

- (void)sendTapped
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"audioNotification" object:recorder.url];
    
}

#pragma mark - AVAudioRecorderDelegate

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag
{
    
    NSData *file1Data = [[NSData alloc] initWithContentsOfURL:recorder.url];
    NSInteger audioBytes=[file1Data length];
    if (audioBytes<5000)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"playAnnouncementsSingleTapNotification" object:nil];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"playAnnouncementsfullrecordNotification" object:nil];
    }
    
}

#pragma mark - AVAudioPlayerDelegate

-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"playAnnouncementsNotification" object:nil];
    m_iStatus = AUDIO_DEVICE_FREE;
}

@end
