//
//  AppDelegate.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 12/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "TWMessageBarManager.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    int listeningSocket;
    NSString *broadcastAddr1;
    NSString *address;
    long int netWorkStatusTime;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
- (id)initWithStyleSheet:(NSObject<TWMessageBarStyleSheet> *)stylesheet;
- (void)checkNetworkStatus:(NSNotification *)notice;
- (void)AppNetworkStatus;
- (NSString *)getIPAddress;
- (void)broadCast;
-(void) initlisten;
-(int) receivePackets;
@end
