//
//  NetworkController.m
//
//  Copyright (c) 2012 Peter Bakhirev <peter@byteclub.com>
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.

/*
 {
 "callStartTime": 1484644052,
 "callDuration":5.6,
 "callFrom": "Prashant BUS",
 "callTTL": 360,
 "callStatus" : "new",
 "callTxId": 1234567890,
 "callType": "Message",
 "groupName":"InStoreAPP",
 "storeName": "HDC_A"
 }
 */

#import "NetworkController.h"
#import "TWMessageBarManager.h"
#import "SlideNavigationController.h"
#import "EarboxViewController.h"
#import "UIViewController.h"
#import "InstoreData.h"
#import "StateMachineHandler.h"
#define HB_ACK_WAIT_THRESHOLD 4

#pragma mark - Private properties and methods

@interface NetworkController () {
    NSInteger  hbinterval;
    NSInteger  hbSentCount;
    BOOL timerStarted;
    NSString* messageDelimiter;
    NSInteger bytesRead;
}

- (BOOL)openConnection;
- (void)closeConnection;
- (void)finishOpeningConnection;
- (void)readFromStreamToInputBuffer;
- (void)writeOutputBufferToStream;
- (void)notifyConnectionBlock:(ConnectionBlock)block;

@end


@implementation NetworkController

@synthesize messageReceivedBlock;
@synthesize connectionOpenedBlock, connectionFailedBlock, connectionClosedBlock;


#pragma mark - Singleton

static id sharedInstance = nil;

+ (void)initialize {
    if (self == [NetworkController class]) {
        sharedInstance = [[self alloc] init];
    }
}

+ (NetworkController*)sharedInstance {
    return sharedInstance;
}


#pragma mark - Public methods

- (void)connect
{
    host = [[InstoreData sharedManager] getInStoreTgsIpAddress];
    if (![self isConnected]) {
        if (![self openConnection]) {
            [self notifyConnectionBlock:connectionFailedBlock];
        }
    }
}

- (void)disconnect {
    [self closeConnection];
}

- (void)sendMessage:(NSString*)message {
    
    [outputBuffer appendBytes:[message cStringUsingEncoding:NSASCIIStringEncoding]
                       length:[message length]];
    [outputBuffer appendBytes:[messageDelimiter cStringUsingEncoding:NSASCIIStringEncoding]
                       length:[messageDelimiter length]];
    
    [self writeOutputBufferToStream];
}


#pragma mark - Private methods

- (id)init {
    // This might come from some configuration store or Bonjour.
    host = [[InstoreData sharedManager] getInStoreTgsIpAddress];
    port =49000; //52001;
    hbinterval  = 2; // by default 2 seconds we will send HB
    hbSentCount = 0 ;
    timerStarted = NO;
    
    messageDelimiter = @"\n";
    return self;
}

- (BOOL)openConnection {
    // Nothing is open at the moment.
    isInputStreamOpen = NO;
    isOutputStreamOpen = NO;
    
    // Setup socket connection
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,
                                       (__bridge CFStringRef)host, port,
                                       &readStream, &writeStream);
    
    if (readStream == nil || writeStream == nil)
        return NO;
    
    // Indicate that we want socket to be closed whenever streams are closed.
    CFReadStreamSetProperty(readStream, kCFStreamPropertyShouldCloseNativeSocket,
                            kCFBooleanTrue);
    CFWriteStreamSetProperty(writeStream, kCFStreamPropertyShouldCloseNativeSocket,
                             kCFBooleanTrue);
    
    // Setup input stream.
    inputStream = (__bridge_transfer NSInputStream*)readStream;
    inputStream.delegate = self;
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [inputStream open];
    
    // Setup output stream.
    outputStream = (__bridge_transfer NSOutputStream*)writeStream;
    outputStream.delegate = self;
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream open];
    
    // Setup buffers.
    inputBuffer = [[NSMutableData alloc] init];
    outputBuffer = [[NSMutableData alloc] init];
    
    return YES;
}

- (void)closeConnection {
    // Notify world.
    if (![self isConnected])
        [self notifyConnectionBlock:connectionFailedBlock];
    else
        [self notifyConnectionBlock:connectionClosedBlock];
    
    // Clean up input stream.
    inputStream.delegate = nil;
    [inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [inputStream close];
    inputStream = nil;
    isInputStreamOpen = NO;
    
    // Clean up output stream.
    outputStream.delegate = nil;
    [outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream close];
    outputStream = nil;
    isOutputStreamOpen = NO;
    
    // Clean up buffers.
    inputBuffer = nil;
    outputBuffer = nil;
}

- (bool)isConnected {
    return (isInputStreamOpen && isOutputStreamOpen);
}

- (void)finishOpeningConnection {
    if (isInputStreamOpen && isOutputStreamOpen) {
        [self notifyConnectionBlock:connectionOpenedBlock];
        //[self writeOutputBufferToStream];
        [NSThread detachNewThreadSelector:@selector(sendHeartBeat) toTarget:self withObject:nil];
        // [self sendHeartBeat];
        
        
    }
}

-(void) stopTCPConnection
{
    [myTimer invalidate];
    timerStarted = NO;
    [self disconnect];
}

- (void)readFromStreamToInputBuffer {
    // Temporary buffer to read data into.
    uint8_t buffer[4096];
    char msgTypeStr[4];
    char msgLengthStr[4];
    
    // uint8_t buffer1= string2sendbuffer(msgTypeStr);
    bytesRead = [inputStream read:( uint8_t *)msgTypeStr maxLength:4];
    
    NSUInteger nMsgType = (  (( msgTypeStr[0] << 24 )& 0xff000000) +
                          (( msgTypeStr[1] << 16 )& 0x00ff0000) +
                          (( msgTypeStr[2] << 8  )& 0x0000ff00) + (msgTypeStr[3] & 0x000000ff));
    
    memset(msgLengthStr,0,4);
    NSLog(@"The msg type is %ld",(long)nMsgType);
    bytesRead = [inputStream read:( uint8_t *)msgLengthStr maxLength:4];
    NSUInteger nMsgLength=0;
    
   
         nMsgLength = (  ( (msgLengthStr[0] << 24 ) & 0xff000000) +
                                ( ( msgLengthStr[1] << 16 ) & 0x00ff0000) +
                                ( ( msgLengthStr[2] << 8  ) & 0x0000ff00) + ( msgLengthStr[3] & 0x000000ff) );
    
    
     NSLog(@"The msg type is %ld and msgLength is %ld",(long)nMsgType,(long)nMsgLength);
    
    memset(buffer,0,0);
    if(nMsgType == 1)
    {
        
        bytesRead = [inputStream read:buffer maxLength:nMsgLength];
        hbSentCount = 0 ;
        if (bytesRead >= 0)
        {
            NSString *str =[NSString stringWithUTF8String:(char *)buffer];
            
            if([str length]>0)
            {
                NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                
                if ([[jsonObject allKeys] containsObject:@"hbinterval"])
                {
                    hbinterval=[[jsonObject objectForKey:@"hbinterval"] integerValue];
                    NSLog(@"%ld",(long)hbinterval);
                    
                }
                else
                {
                    hbinterval = 2;
                    
                }
                
                if(!timerStarted)
                {
                    timerStarted = YES;
                    myTimer= [NSTimer scheduledTimerWithTimeInterval:hbinterval
                                                              target:self
                                                            selector:@selector(sendHeartBeat)
                                                            userInfo:nil
                                                             repeats:YES];
                }
                
            }
        }
        /*
         NSString *str =[NSString stringWithUTF8String:(char *)buffer];
         
         //NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding]options:0 error:NULL];
         
         NSLog(@"The Heartbeat msg received = %s\n",buffer);
         
         // notificationinfo=jsonObject;
         NSDictionary *apsInfo = [notificationinfo objectForKey:@"aps"];
         alertMessage = [apsInfo objectForKey:@"alert"];
         if ([alertMessage containsString:@"/"])
         {
         storeName=[alertMessage componentsSeparatedByString:@"/"][1];
         }
         */
        //[self forgroundNotificationShow];
    }
    else if(nMsgType == 2)
    {
        bytesRead = [inputStream read:buffer maxLength:nMsgLength];
        
        //Local push notification generation....
        NSString *str =[[NSString alloc] initWithBytes:buffer
                                                 length:nMsgLength
                                               encoding:NSUTF8StringEncoding];
        if([str length]>0)
        {
        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
        id jsonString = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        
         notificationinfo=jsonString;
            NSString *callType;
            if ([[notificationinfo objectForKey:@"callType"] isEqualToString:@"4"])
            {
                callType=@"Message";
            }
            if ([[notificationinfo objectForKey:@"callType"] isEqualToString:@"17"])
            {
                callType=@"Group Message";
            }
            if ([[notificationinfo objectForKey:@"callType"] isEqualToString:@"33"])
            {
                callType=@"Everyone Message";
            }
            if ([[notificationinfo objectForKey:@"callType"] isEqualToString:@"108"] ||[[notificationinfo objectForKey:@"callType"] isEqualToString:@"102"])
            {
                callType=@"AS-Group mesage";
            }
            if ([[notificationinfo objectForKey:@"callType"] isEqualToString:@"16"] ||[[notificationinfo objectForKey:@"callType"] isEqualToString:@"10"]||[[notificationinfo objectForKey:@"callType"] isEqualToString:@"21"]||[[notificationinfo objectForKey:@"callType"] isEqualToString:@"22"]||[[notificationinfo objectForKey:@"callType"] isEqualToString:@"23"]||[[notificationinfo objectForKey:@"callType"] isEqualToString:@"25"])
            {
                callType=@"Announcement";
            }
            alertMessage = [NSString stringWithFormat:@"%@ From %@, %@ ",callType,[notificationinfo objectForKey:@"callFrom"],[notificationinfo objectForKey:@"storeName"]];
         [[TWMessageBarManager sharedInstance] showMessageWithTitle:nil
         description:alertMessage
         type:TWMessageBarMessageTypeError
         statusBarStyle:UIStatusBarStyleLightContent
         callback:^{
         [self forgroundNotificationShow];
         
         }];
        }
        
        //[self forgroundNotificationShow];
        
    }
    
}

-(void)forgroundNotificationShow

{
    NSString *role=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"roleId"]];
    
    int roleid = 0;
    
    roleid = [role intValue];
    
        // requestUndeliveredMsgsinbox
        
        [[NSUserDefaults standardUserDefaults]setObject:[notificationinfo objectForKey:@"callType"] forKey:@"notificationtype"];
        if (roleid>10)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"pushForMultiStore"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"localnotification" object:nil];
            NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
            [myQueue addOperationWithBlock:^{
                if( [[StateMachineHandler sharedManager]isNativeStarted])
                {
                    [[StateMachineHandler sharedManager] killTheatroNative];
                }
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                }];
            }];
        }
        else
        {
          NSString  *storeNameStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
            
            if ([storeNameStr isEqualToString:[notificationinfo objectForKey:@"storeName"]])
            {
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                         bundle: nil];
                [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
                EarboxViewController *earboxVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"EarboxViewController"];
                
                NSString *chain=[alertMessage componentsSeparatedByString:@","][1];
                NSString *chainName=[[chain componentsSeparatedByString:@"/"][0]substringFromIndex:1];
                [[NSUserDefaults standardUserDefaults]setObject:storeName forKey:@"storeNameString"];
                NSDictionary *twmAppLogindic=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
                NSMutableDictionary *updatedic=[[NSMutableDictionary alloc]init];
                [updatedic setObject:[twmAppLogindic objectForKey:@"companyId"] forKey:@"companyId"];
                [updatedic setObject:[twmAppLogindic objectForKey:@"firstTimeLogin"] forKey:@"firstTimeLogin"];
                [updatedic setObject:[twmAppLogindic objectForKey:@"tagOutName"] forKey:@"tagOutName"];
                [updatedic setObject:[twmAppLogindic objectForKey:@"userId"] forKey:@"userId"];
                [updatedic setObject:chainName forKey:@"chainName"];
                [[NSUserDefaults standardUserDefaults]setObject:updatedic forKey:@"twmAppLogin"];
                [NSThread detachNewThreadSelector:@selector(communicatorStart) toTarget:self withObject:nil];
                
                if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"EarboxViewController"])
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnotificationrecieved" object:nil];
                }
                else
                {
                    [[SlideNavigationController sharedInstance] pushViewController:earboxVC animated:YES];
                }
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"pushForDiffStore"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] setObject:storeName forKey:@"StoreNameForPush"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"localnotification" object:nil];
            }
        }
    
}

-(void)communicatorStart
{
    // Communicator starts here
    [[StateMachineHandler sharedManager] communicator:NO];
}

// Write whatever data we have, as much of it as stream can handle.
//
- (void)writeOutputBufferToStream {
    // Is connection open?
    if (![self isConnected])
        return;
    
    // Do we have anything to write?
    if ([outputBuffer length] == 0)
        return;
    
    // Can stream take any data in?
    if (![outputStream hasSpaceAvailable])
        return;
    
    // Write as much data as we can.
    NSInteger bytesWritten = [outputStream write:[outputBuffer bytes]
                                       maxLength:[outputBuffer length]];
    
    // Check for errors.
    if (bytesWritten == -1)  {
        [self disconnect];
        return;
    }
    
    // Remove it from the buffer.
    [outputBuffer replaceBytesInRange:NSMakeRange(0, bytesWritten)
                            withBytes:NULL
                               length:0];
}

- (void)notifyConnectionBlock:(ConnectionBlock)block {
    if (block != nil)
        block(self);
}


#pragma mark - NSStreamDelegate methods

- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)streamEvent {
    
    if (stream == inputStream) {
        switch (streamEvent) {
                
            case NSStreamEventNone:
                
                break;
            case NSStreamEventOpenCompleted:
                isInputStreamOpen = YES;
                [self finishOpeningConnection];
                break;
                
            case NSStreamEventHasBytesAvailable:
                [self readFromStreamToInputBuffer];
                break;
                
            case NSStreamEventHasSpaceAvailable:
                // Should not happen for input stream!
                break;
                
            case NSStreamEventErrorOccurred:
                // Treat as "connection should be closed"
            case NSStreamEventEndEncountered:
                [self closeConnection];
                break;
        }
    }
    
    if (stream == outputStream) {
        switch (streamEvent) {
                
            case NSStreamEventNone:
                
                break;
            case NSStreamEventOpenCompleted:
                isOutputStreamOpen = YES;
                [self finishOpeningConnection];
                break;
                
            case NSStreamEventHasBytesAvailable:
                // Should not happen for output stream!
                break;
                
            case NSStreamEventHasSpaceAvailable:
                      //[self sendMessage:@"Prashant"];
                   //[self writeOutputBufferToStream];
                break;
                
            case NSStreamEventErrorOccurred:
                // Treat as "connection should be closed"
            case NSStreamEventEndEncountered:
                [self closeConnection];
                break;
        }
    }
}


- (int)sendHeartBeat {
    /*
     {
     "callStartTime": 1484644052,
     "callDuration":5.6,
     "callFrom": "Prashant BUS",
     "callTTL": 360,
     "callStatus" : "new",
     "callTxId": 1234567890,
     "callType": "Message",
     "groupName":"InStoreAPP",
     "storeName": "HDC_A"
     }
     */
    /*
     NSMutableDictionary *postData=[[NSMutableDictionary alloc] init];
     [postData setObject:[NSNumber numberWithInteger:1484644052] forKey:@"callStartTime"];
     [postData setObject:@"NewMessage" forKey:@"FinalView"];
     [postData setObject:@[] forKey:@"Groups"];
     [postData setObject:@[] forKey:@"Stores"];
     */
    
    // char payload[] = "heartbeat";
    /*char payload[] = "{\"callStartTime\": 1484644052, \
     \"callDuration\":5.6, \
     \"callFrom\": \"Prashant BUS\", \
     \"callTTL\": 360, \
     \"callStatus\" : \"new\", \
     \"callTxId\": 1234567890, \
     \"callType\": \"Message\", \
     \"groupName\":\"InStoreAPP\", \
     \"storeName\": \"HDC_A\"}";
     */
    
    int payloadLen =0;//(int) strlen(payload);
    int msgType = htonl(4);
    int msgLength = htonl( 0);
    char buffer[1024];
    int totallength = 0;
    
    
    if ( hbSentCount < HB_ACK_WAIT_THRESHOLD )
    {
        hbSentCount ++;
    NSLog(@"hbinterval is %ld",(long)hbSentCount);
        memset(buffer, 0, 0);
        
        //Add a msg Type into buffer.
        memcpy(buffer, &msgType ,4);
        //Add  msg length into buffer.
        memcpy(buffer + 4 , &msgLength ,4);
        
        //Add the payload into buffer.
        // memcpy(buffer + 8 ,payload ,payloadLen );
        
        totallength = payloadLen + 8;
        
        
        [outputBuffer appendBytes:buffer
                           length:totallength];
        
        //  [outputBuffer appendBytes:[messageDelimiter cStringUsingEncoding:NSASCIIStringEncoding]
        //                   length:[messageDelimiter totallength]];
         NSLog(@"heart beat sent");
        [self writeOutputBufferToStream];
    }
    else
    {
        [self stopTCPConnection];
    }
    return 0;
    
}

@end
