//
//  InstoreData.m
//  Managers
//
//  Created by theatro2mac on 14/07/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import "InstoreData.h"

@implementation InstoreData

static InstoreData* _sharedMySingleton = nil;


+ (id)sharedManager {
    static InstoreData *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

//getter
-(bool)isInStoreTGS
{
    return inStoreTGS;
}

-(NSString*)getInStoreTgsStoreName
{
    return inStoreTgsStoreName;
}

-(NSString*)getInStoreTgsChainName
{
    return inStoreTgsChainName;
}

-(NSString*)getInStoreTgsIpAddress
{
    return inStoreTgsIpAddress;
}

//setter
-(void)setInStoreTGS:(bool)inStore
{
    inStoreTGS = inStore;
}
-(void)setInStoreTgsStoreName:(NSString*)storeName
{
    inStoreTgsStoreName = storeName;
}

-(void)setInStoreTgsChainName:(NSString*)chainName
{
    inStoreTgsChainName = chainName;
}

-(void)setInStoreTgsIpAddress:(NSString*)ipAddress
{
    inStoreTgsIpAddress = ipAddress;
}



@end
