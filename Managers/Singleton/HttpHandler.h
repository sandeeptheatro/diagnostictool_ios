//
//  HttpHandler.h
//  Managers
//
//  Created by Ravi Shankar on 27/01/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpHandler : NSOperation


+(NSDictionary *)login:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment userScreen:(NSString *)userScreen;
+(NSDictionary *)createPassWord:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment twUserID:(NSString *)twUserID userScreen:(NSString *)userScreen;
+(NSDictionary *)forgotPassWord:(NSString *)twUserName twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment userScreen:(NSString *)userScreen;
+(NSDictionary *)changePassword:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment newPassword:(NSString *)newPassword currentPassword:(NSString *)currentPassword userScreen:(NSString *)userScreen;
+(NSDictionary *)storeList:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment userScreen:(NSString *)userScreen;
+(NSDictionary *)employeeList:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment storeId:(int)storeId userScreen:(NSString *)userScreen;
+(NSDictionary *)groupList:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment storeId:(int)storeId userScreen:(NSString *)userScreen;
+(NSDictionary *)groupdetailList:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment groupId:(int)groupId userScreen:(NSString *)userScreen;
+(NSDictionary *)audioFileSend:(NSDictionary *)whoDic audioFile:(NSData *)audiodata pns:(NSDictionary *)pns userScreen:(NSString *)userScreen;
+(NSDictionary *)salesEnabled:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment userScreen:(NSString *)userScreen;
+(NSDictionary *)salesReminders:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment userScreen:(NSString *)userScreen;
+(NSDictionary *)favoriteAdd:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId addFavoriteUsers:(NSArray *)addFavoriteUsers userScreen:(NSString *)userScreen;
+(NSDictionary *)favoritelist:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId userScreen:(NSString *)userScreen;
+(NSDictionary *)earboxlist:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId userScreen:(NSString *)userScreen;
+(NSDictionary *)userlogout:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment userScreen:(NSString *)userScreen;
+(NSDictionary *)earboxunheardcount:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId userScreen:(NSString *)userScreen;
+(NSDictionary *)earboxundeliveredlist:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId twreqFor:(NSString *)twreqFor twreqType:(NSString *)twreqType userScreen:(NSString *)userScreen;
+(NSDictionary *)uploadlogfiles:(NSData *)logfiledata userScreen:(NSString *)userScreen;
+(NSDictionary *)dynamicLogUpload:(NSString *)userScreen;
+(NSDictionary *)configsV2:(NSString *)userScreen;
+(NSDictionary *)selectGroupsV2:(NSString *)userScreen;
+(NSDictionary *)selectStoresV2:(NSString *)userScreen;
+(NSDictionary *)selectGroupsASV3:(NSString *)userScreen;
+(NSDictionary *)hierarchyV2:(NSString *)userName password:(NSString *)password userScreen:(NSString *)userScreen;
+(NSData *)hierarchyChildV2:(NSString *)levelId universalId:(NSString *)universalId userName:(NSString *)userName password:(NSString *)password storesState:(NSString *)storesState userScreen:(NSString *)userScreen;
+(NSDictionary *)earboxlistV2:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId userScreen:(NSString *)userScreen;
+(NSDictionary *)earboxundeliveredlistV2:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId twreqFor:(NSString *)twreqFor userScreen:(NSString *)userScreen;
+(NSDictionary *)earboxundeliveredlistVeb:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId  twreqFor:(NSString *)twreqFor userScreen:(NSString *)userScreen;
+(NSDictionary *)updatemodeV2:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment pushTxId:(NSString *)pushTxId modeId:(NSString *)modeId userScreen:(NSString *)userScreen;
+(NSDictionary *)updateheardstatusV2:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment pushTxId:(NSString *)pushTxId heardStatusId:(NSString *)heardStatusId userScreen:(NSString *)userScreen;
+(NSDictionary *)unheardV2:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId userScreen:(NSString *)userScreen;
+(NSDictionary *)replystatusV2:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment pushTxId:(NSString *)pushTxId companyId:(int)companyId userScreen:(NSString *)userScreen;
+(NSDictionary *)appstoreVersioncheck;
+(NSDictionary *)alarmNotification:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment alarmcode:(NSString *)alarmcode severity:(NSString *)severity alarmdesc:(NSString *)alarmdesc;
//Ravi Code
+(NSDictionary *)notifyTcm:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment companyId:(int)companyId storeId:(int)storeId userScreen:(NSString *)userScreen;
+(NSString *)statusString:(NSInteger)statuscode;
+(NSDictionary *)favSave:(NSString *)userName password:(NSString *)password jSonStr:(NSDictionary *)jSonStr userScreen:(NSString *)userScreen;
+(NSMutableArray *)favFetch :(NSString *)userName password:(NSString *)password userScreen:(NSString *)userScreen;
+(NSDictionary *)favDelete :(NSString *)userName password:(NSString *)password favDeleteID:(NSString *)favDeleteID userScreen:(NSString *)userScreen;
+(NSDictionary *)hierarchyAll:(NSString *)userName password:(NSString *)password userScreen:(NSString *)userScreen;
//https://itg0.theatro.com/restapi/v2/app/chain/{chainId}/asusers/
+(NSDictionary *)asMembersAndDistricts:(NSString *)userScreen;
+(BOOL)checkCentralServerAvailability;
//Switch to C2 api
+(NSDictionary *)logonC2ApiCall:(NSString *)twUserName twPassword:(NSString *)twPassword twTxId:(NSString *)twTxId twMoment:(NSString *)twMoment boardid:(NSString *)boardid userScreen:(NSString *)userScreen;
+(NSMutableArray *)notificationFetch :(NSString *)userName password:(NSString *)password userScreen:(NSString *)userScreen;
+(NSDictionary *)NotificationSave :(NSString *)userName password:(NSString *)password jSonStr:(NSMutableArray *)jSonStr userScreen:(NSString *)userScreen;
+(NSMutableArray *)featureToggle :(NSString *)userName password:(NSString *)password userScreen:(NSString *)userScreen;
+(BOOL)checkCentralServerAvailabilityForInStore:(NSString *)IpAddress;

@end
