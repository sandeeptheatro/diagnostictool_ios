//
//  AudioManager.h
//
//  Created by AbhiC on 25/5/2015.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

typedef enum AUDIO_DEVICE_STATUS
{
    AUDIO_DEVICE_UNINITIALIZED =0,
    AUDIO_DEVICE_FREE = 1,
    AUDIO_DEVICE_NATIVE_PLAYBACK = 2,
    AUDIO_DEVICE_NATIVE_RECORD = 3,
    AUDIO_DEVICE_FILE_PLAYBACK = 4,
    AUDIO_DEVICE_RECORD_TOFILE = 5
    
} AUDIO_DEVICE_STATUS;


@interface AudioManager :  NSObject
<AVAudioRecorderDelegate, AVAudioPlayerDelegate,AVAudioSessionDelegate,CBCentralManagerDelegate>

/*sandeep app level apis */
@property(nonatomic,strong)AVAudioPlayer *player;
@property(nonatomic,strong)AVAudioRecorder *recorder;
-(void)createEarboxAudioPath:(NSString *)audiofile callId:(NSString *)audioid earboxName:(NSString *)earboxName callTxid:(NSString *)callTxid;
-(void)createEarboxAudioPathCellTapped:(NSString *)audiofile callId:(NSString *)audioid earboxName:(NSString *)earboxName callTxid:(NSString *)callTxid;
-(void)deleteEarboxAudioPath:(NSString *)audioid;
-(void)deleteallEarboxAudioPath;
-(NSURL *)getEarboxAudioPath:(NSString *)audioid;
-(double)getAudioduration:(NSURL *)audioUrl;
-(void)path;
- (void)recordPauseTapped;
- (void)stopTapped;
- (void)playTapped;
- (void)deleteTapped;
- (void)sendTapped;
-(void)recordstop;
-(void)audioplayerstop;
-(void)sentaudioPath:(NSString *)audiofile oldurl:url;
/* Abhi C audio device manager api's */



void callbackInterruptionListener(void* inClientData, UInt32 inInterruption);

void audioRouteChangeEventCallback(void * inClientData,AudioSessionPropertyID inPropertyID, UInt32 inPropertyValueSize,const void * inPropertyValue);

-(OSStatus)setUpAudioDevice;

-(void)initAudio;

-(void)initAudioManager;

-(void)createAudioDevice;

-(OSStatus)setUpAudioSession;


-(void) switchAudioOutput;
-(BOOL)startNativeAudioPlayback;
-(BOOL)startNativeAudioRecording;
-(BOOL)stopNativeAudioPlayback;
-(BOOL)stopNativeAudioRecording;
-(BOOL)shutdownNativeAudio;

-(BOOL) shutDownAudio;

+(AudioManager *)getsharedInstance;


@end
