


//
//  SendOperation.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "StateMachineHandler.h"
#import <sys/socket.h>
#import <sys/sysctl.h>
#import <net/if.h>
#import <net/if_dl.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "AudioManager.h"
#import "AppDelegate.h"
#import "InstoreData.h"
#import "HttpHandler.h"
#import "AlertView.h"
#import "Constant.h"

BOOL logonSuccessFull;

@interface StateMachineHandler()
{
    BOOL isConnectedtoServer;
    BOOL isNativeStarted;
    updateStatus status;
    int count;
    NSOperationQueue *mainQueue;
    NSInvocationOperation *startcoverageOperation;
    NSOperationQueue *mainQueueAudio;
    NSInvocationOperation *startPbkOperation;
    NSInvocationOperation *stopPbkOperation;
    NSInvocationOperation *startRcrdOperation;
    NSInvocationOperation *stopRcrdOperation;
}
@end
@implementation StateMachineHandler
@synthesize isIFCValue;
static StateMachineHandler* _sharedMySingleton = nil;

+ (id)sharedManager {
    static StateMachineHandler *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(void)get
{
    isIFCValue=NO;
}

-(bool)isNativeStarted
{
    return isNativeStarted;
}

- (NSString *)getIPAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

-(void)filledStatus:(const char *)coverageString
{
    mainQueueAudio=[[NSOperationQueue alloc]init];
    mainQueueAudio.maxConcurrentOperationCount=1;
   // NSString *cjsonString =[NSString stringWithFormat:@"%s",coverageString];
    NSString *cjsonString =[NSString stringWithUTF8String:coverageString];
    unsigned long length = cjsonString.length;
    if(length < 15)
    {
        NSArray *items = @[@"connected", @"disconnected",@"sockerr", @"startPbk",@"stopPbk",@"startRcrd",@"stopRcrd",@"websocketred"];
        
        NSInteger item = [items indexOfObject:cjsonString];
        switch (item) {
            case 0:
                if(!isConnectedtoServer)
                {
                    isConnectedtoServer = true;
                    //start a coverage request thread here
                    const char *tagOutName=[[[[NSUserDefaults standardUserDefaults] objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] UTF8String];
                    [self  sendLoginName:tagOutName];
                    // [mainQueue addObserver:self forKeyPath:@"operations" options:0 context:NULL];
                    startcoverageOperation=[[NSInvocationOperation alloc]initWithTarget:self selector:@selector(coverageRequestToServertask) object:nil];
                    [mainQueue addOperation:startcoverageOperation];
                }
                break;
            case 1:
                //post connection to server lost...
                
                isIFCValue=NO;
                isConnectedtoServer = NO;
                
                
                [self performSelectorOnMainThread:@selector(coverageRequestToServertask:) withObject:@"server connection lost..." waitUntilDone:NO];
                break;
                
            case 2:
                //post connection to server lost...
                
                [self performSelectorOnMainThread:@selector(coverageRequestToServertask:) withObject:@"could not start a connection..." waitUntilDone:NO];
                break;
                
            case 3:
                
                NSLog(@"startPlaback stateMachine Handler called...\n");
                startPbkOperation=[[NSInvocationOperation alloc]initWithTarget:self selector:@selector(startNativeAudioPlayback) object:nil];
                [startPbkOperation setQueuePriority:NSOperationQueuePriorityHigh];
                [mainQueueAudio addOperation:startPbkOperation];
                //Ideally send a msg to AudioManager object...for starting playback...
                
                break;
            case 4:
                NSLog(@"stopPlaback stateMachine Handler called...\n");
                stopPbkOperation=[[NSInvocationOperation alloc]initWithTarget:self selector:@selector(stopNativeAudioPlayback) object:nil];
                [stopPbkOperation setQueuePriority:NSOperationQueuePriorityHigh];
                [mainQueueAudio addOperation:stopPbkOperation];
                break;
            case 5:
                if (!recordStarted)
                {
                    recordStarted=YES;
                    
                    NSLog(@"startRecording stateMachine Handler called...\n");
                    startRcrdOperation=[[NSInvocationOperation alloc]initWithTarget:self selector:@selector(startNativeAudioRecording) object:nil];
                    
                    [startRcrdOperation setQueuePriority:NSOperationQueuePriorityHigh];
                    
                    [mainQueueAudio addOperation:startRcrdOperation];
                    
                    break;
                }
            case 6:
                if (recordStarted)
                {
                    recordStarted=NO;
                    stopRcrdOperation=[[NSInvocationOperation alloc]initWithTarget:self selector:@selector(stopNativeAudioRecording) object:nil];
                    
                    NSLog(@"stopRecording stateMachine Handler called...\n");
                    [stopRcrdOperation setQueuePriority:NSOperationQueuePriorityHigh];
                    [stopRcrdOperation addDependency:startRcrdOperation];
                    if (stopRcrdOperation)
                    {
                        [mainQueueAudio addOperation:stopRcrdOperation];
                    }
                    
                    break;
                }
            case 7:
                //post connection to server lost...
                isIFCValue=NO;
                isConnectedtoServer = NO;
                //[self killTheatroNative];
                [self performSelectorOnMainThread:@selector(coverageRequestToServertask:) withObject:@"Could not connect to Proxy server..." waitUntilDone:NO];
                break;
        }
    }
    else
    {
            
        NSInvocationOperation *operation=[[NSInvocationOperation alloc]initWithTarget:self selector:@selector(coverageDecodeTheard:) object:cjsonString];
        [operation setQueuePriority:NSOperationQueuePriorityHigh];
        [mainQueue addOperation:operation];
    }
}

-(void)startNativeAudioPlayback
{
    [[AudioManager getsharedInstance] startNativeAudioPlayback];
}

-(void)stopNativeAudioPlayback
{
    [[AudioManager getsharedInstance] stopNativeAudioPlayback];
}

-(void)startNativeAudioRecording
{
    [[AudioManager getsharedInstance] startNativeAudioRecording];
}

-(void)stopNativeAudioRecording
{
    [[AudioManager getsharedInstance] stopNativeAudioRecording];
}

-(void)coverageDecodeTheard:(NSString *)cjsonString
{
    
    NSData *data = [cjsonString dataUsingEncoding:NSUTF8StringEncoding];
    id jsonString = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    //get the IFC value from json.
    if(isIFCValue == NO)
    {
        isIFCValue =[[jsonString objectForKey:@"IFC"]boolValue];
    }
    
    if(isIFCValue == NO)
    {
        return;
    }
    else
    {
        [self performSelectorOnMainThread:@selector(coveragejsontask:) withObject:jsonString waitUntilDone:NO];
    }
    
    
    if ([jsonString objectForKey:@"ht"]!=nil)
    {
        [[NSUserDefaults standardUserDefaults] setObject:jsonString forKey:@"huddle"];
    }
    
    if ([[jsonString objectForKey:@"ha"] isEqualToString:@"false"] || [[jsonString objectForKey:@"ha"] isEqualToString:@"true"])
    {
        [self performSelectorOnMainThread:@selector(huddleNotificationnew:) withObject:jsonString waitUntilDone:NO];
        
    }
    if ([jsonString objectForKey:@"sa"]!=nil)
    {
        [self performSelectorOnMainThread:@selector(salesreminderNotification:) withObject:jsonString waitUntilDone:NO];
    }
    
    NSArray *coverageArray=[jsonString objectForKey:@"coverage"];
    if ([coverageArray count]>0)
    {
        if ([[[[coverageArray objectAtIndex:0] objectForKey:@"name"]componentsSeparatedByString:@"_"][1] isEqualToString:[[[NSUserDefaults standardUserDefaults] objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"]] && [[[[coverageArray objectAtIndex:0] objectForKey:@"status"]componentsSeparatedByString:@"_"][0] isEqualToString:[NSString stringWithFormat:@"%d",6]])
        {
            
            [self performSelectorOnMainThread:@selector(logonStealing:) withObject:jsonString waitUntilDone:NO];
            
            
        }
    }
}

-(void)huddleNotificationnew:(id)jsonString
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"huddleNotificationnew" object:jsonString];
    
    [[NSUserDefaults standardUserDefaults] setObject:jsonString forKey:@"huddleha"];
}

-(void)salesreminderNotification:(id)jsonString
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"salesreminderNotification" object:jsonString];
    [[NSUserDefaults standardUserDefaults] setObject:jsonString forKey:@"salessa"];
}

-(void)coveragejsontaskQueue:(NSString *)coveragejson
{
    [self performSelectorOnMainThread:@selector(coveragejsontask:) withObject:coveragejson waitUntilDone:NO];
}

-(void)killTheatroNative
{
    isIFCValue=YES;
    isNativeStarted = NO;
    sendLogoffToNative();
    sendSwitchStatusToNative(YES);
    killTheatroNativeLib();
    sendSwitchStatusToNative(NO);
    [[AudioManager getsharedInstance] shutDownAudio];
    NSLog(@"killTheatroNative");
}

-(void)killTheatroNativelogonstealing
{
    isIFCValue=YES;
    isNativeStarted = NO;
    killTheatroNativeLib();
    [[AudioManager getsharedInstance] shutDownAudio];
    NSLog(@"killTheatroNativelogonstealing");
}

-(void)doQueryButtonAction:(BOOL)value
{
    doQueryButtonAction(value);
}

-(void)enableBroadcastScreen:(BOOL)value
{
    EnableBroadcastScreen(value);
    NSLog(@"EnableBroadcastScreen");
}

-(void) sendPlayMsgToNative
{
    sendPlayMsgToNative();
    NSLog(@"sendPlayMsgToNative");
}

-(void)sendRefreshEvent
{
    if(isNativeStarted)
    {
    sendRefreshEvent();
    }
    else
    {
        printf("errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrorrrrrrrrrrrrrrrrrrrrrr");
    }
}

-(void)sendLoginName:(const char *)tagoutName
{
    sendLoginNameToNative(tagoutName);
}

-(void) sendPlayAnncToNative
{
    sendPlayAnncToNative();
    NSLog(@"sendPlayAnncToNative");
}

-(void) sendCancelPlayoutToNative:(BOOL)value
{
    sendCancelPlayOutToNative(value);
    NSLog(@"sendCancelPlayOutToNative");
}

-(void) sendCallEndToNative
{
    sendCallEndToNative();
    NSLog(@"sendCallEndToNative");
}

-(void) sendPlaybufferenable:(BOOL)value
{
    sendPlaybufferenable(value);
    NSLog(@"sendPlaybufferenable");
}

-(void) mediabuttonAction:(NSString *)cmd nameStr:(NSString *)nameStr Button:(BOOL) down
{
    NSString *stringobject=[NSString stringWithFormat:@"%@%@",cmd,nameStr];
    const char *cfilename=[stringobject UTF8String];
    doMediabuttonAction(cfilename, down);
    NSLog(@"doMediabuttonAction");
}

-(void)communicator:(BOOL) isNetworkSwitch
{
    
    if(!isNativeStarted)
    {
        isConnectedtoServer = NO;
        isIFCValue=NO;
        logonSuccessFull = NO;
       
        NSLog(@"commmunicator starting...");
        mainQueue=[[NSOperationQueue alloc]init];
        mainQueue.maxConcurrentOperationCount=4;
        
        NSString *storeName= [[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
        NSString *chainName=[[[NSUserDefaults standardUserDefaults] objectForKey:@"twmAppLogin"] objectForKey:@"chainName"];
        NSString *ipAddress=@"proxy.theatro.com";
        const char *chainNameNative=[[NSString stringWithFormat:@"%@",chainName] UTF8String];
        const char *chainNameipAddress=[[NSString stringWithFormat:@"%@.%@",chainName,ipAddress] UTF8String];
        const char *chainStoreName=[[NSString stringWithFormat:@"%@/%@",chainName,storeName] UTF8String];
        sendProxyAddressToNative(chainNameipAddress); //ipAddress from settings...
        sendChainNameAndStoreNameToNative(chainStoreName);  //login response we will get
        sendChainNameToNative(chainNameNative);
        
        SEL myselector=@selector(filledStatus:);
        IMP imp = [self methodForSelector:myselector];
        status = (void *)imp;
        initCallbacks(status, myselector, self);
        [[AudioManager getsharedInstance] initAudio];
        
        //sendWebSocketPathToNative(char *path)
        NSString *uniqueString=[[NSUserDefaults standardUserDefaults]objectForKey:@"macidfull"];
        const char *macAddress=[uniqueString UTF8String];
        sendMacAddress(macAddress);
        NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
        NSString* version = [NSString stringWithFormat:@"iOSApp_%@",[infoDict objectForKey:@"CFBundleVersion"]];
        const char *appversion=[version UTF8String];
        sendVersionToNative(appversion);
        //Send the network status...
        
        //if(!isNetworkSwitch)
        {
            AppDelegate *AppD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            //AppDelegate *delegate=[[AppDelegate alloc]init];
            [AppD AppNetworkStatus];
        }
        
        if([[InstoreData sharedManager] isInStoreTGS])
        {
            
            NSString *storeNameTgs=[[[InstoreData sharedManager] getInStoreTgsStoreName] uppercaseString];
            
            NSString *storeName2=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeNameString"] uppercaseString];
            
            if (![storeName2 isEqualToString:storeNameTgs])
            {
                BOOL status1 = [HttpHandler checkCentralServerAvailability];
                if (status1==YES)
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InstoreWebSocket"];
                    sendNetworkStatusToNative(WIFI_ON_TGS_OFF);
                    NSString *strPath =[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpathGlobal"];
                    
                    strPath=[strPath stringByReplacingOccurrencesOfString:@"https://" withString:@""];
                    strPath=[strPath stringByReplacingOccurrencesOfString:@"/" withString:@""];
                    
                    const char *websocketPath =[strPath UTF8String];
                    
                    sendWebSocketPathToNative(websocketPath);
                }
                else
                {
                    
                    [self performSelectorOnMainThread:@selector(coverageRequestToServertask:) withObject:@"No internet connection..." waitUntilDone:NO];
                    return;
                }
                
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InstoreWebSocket"];
                const char *Ip = [[[InstoreData sharedManager] getInStoreTgsIpAddress] UTF8String];
                sendIpToNative(Ip);
            }
        }
        
        startTheatroNative();
        
        [self performSelectorOnMainThread:@selector(coverageRequestToServertask:) withObject:@"Connecting to server Please wait..." waitUntilDone:NO];
         isNativeStarted = YES;
    }
}

-(void) sendHuddleInfoToNative:(NSString *)huddleTime huddleType:(int)huddleType
{
    const char *huddleTimeChar=[huddleTime UTF8String];
    sendHuddleInfoToNative(huddleTimeChar, huddleType);
}

-(void)coverageRequestToServertask
{
    unsigned int connectCount = 0;
    //show on the UI fetching employee list...
    [self performSelectorOnMainThread:@selector(coverageRequestToServertask:) withObject:@"fetching employee list..." waitUntilDone:NO];
    while(!isIFCValue)
    {
        if(!isConnectedtoServer)
            break;
        
        //request coverage from native
        if(connectCount % 30 == 0)
        {
            
            const char *tagOutName=[[[[NSUserDefaults standardUserDefaults] objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] UTF8String];
            [self  sendLoginName:tagOutName];
            sendRefreshEvent();
        }
        connectCount++;
        // if we didn't recieve coverage pocket due to pocket loss and after 6 secs
        if(connectCount == 600)
        {
            //Could not logon to TGS...
            [self performSelectorOnMainThread:@selector(coverageRequestToServertask:) withObject:@"Could not logon to store..." waitUntilDone:NO];
            break;
        }
        [NSThread sleepForTimeInterval:0.025];
    }
}

-(void) sendNetworkStatus:(NETWORK_STATE)value
{
    NSLog(@"\n=======**********************Native: sendNetworkStatus %u",value);
    sendNetworkStatusToNative(value);
}

-(void) sendSwitchStatus:(bool)value
{
    sendSwitchStatusToNative(value);
}

-(void)logonStealing:(id)jsonString
{
    
    NSArray *coverageArray=[jsonString objectForKey:@"coverage"];
    if ([[[[coverageArray objectAtIndex:0] objectForKey:@"name"]componentsSeparatedByString:@"_"][1] isEqualToString:[[[NSUserDefaults standardUserDefaults] objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"]] && [[[[coverageArray objectAtIndex:0] objectForKey:@"status"]componentsSeparatedByString:@"_"][0] isEqualToString:[NSString stringWithFormat:@"%d",6]] && [[[coverageArray objectAtIndex:0] objectForKey:@"ct"] isEqualToString:@"2"])
    {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"loggedoff"];
        
    }
    else  if ([[[[coverageArray objectAtIndex:0] objectForKey:@"name"]componentsSeparatedByString:@"_"][1] isEqualToString:[[[NSUserDefaults standardUserDefaults] objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"]] && [[[[coverageArray objectAtIndex:0] objectForKey:@"status"]componentsSeparatedByString:@"_"][0] isEqualToString:[NSString stringWithFormat:@"%d",6]] && [[[coverageArray objectAtIndex:0] objectForKey:@"ct"] isEqualToString:@"1"])
    {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"loggedoff"];
        
    }
    else if ([[[[coverageArray objectAtIndex:0] objectForKey:@"name"]componentsSeparatedByString:@"_"][1] isEqualToString:[[[NSUserDefaults standardUserDefaults] objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"]] && [[[[coverageArray objectAtIndex:0] objectForKey:@"status"]componentsSeparatedByString:@"_"][0] isEqualToString:[NSString stringWithFormat:@"%d",6]] && [[[coverageArray objectAtIndex:0] objectForKey:@"ct"] isEqualToString:@"4"])
    {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"loggedoffsystem"];
    }
    else if ([[[[coverageArray objectAtIndex:0] objectForKey:@"name"]componentsSeparatedByString:@"_"][1] isEqualToString:[[[NSUserDefaults standardUserDefaults] objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"]] && [[[[coverageArray objectAtIndex:0] objectForKey:@"status"]componentsSeparatedByString:@"_"][0] isEqualToString:[NSString stringWithFormat:@"%d",7]] )
    {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"logonFailed"];
        
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logonStealingNotification" object:jsonString];
    
}

-(void)coverageRequestToServertask:(NSString *)networkMessage
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"coverageRequestToServertask" object:networkMessage];
}

-(void)coveragejsontask:(NSString *)coveragejson
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"coveragejson" object:coveragejson];
    NSLog(@"coveragejson is:%@",coveragejson);
}

-(void)enterLockScreen
{
    [[AudioManager getsharedInstance] stopNativeAudioPlayback];
    sendCancelPlayOutToNative(YES);
}

-(void)reOpenClosedSockets
{
    reOpenClosedSockets();
    clearOraIdFromNative();
    sendCancelPlayOutToNative(NO);
    NSLog(@"reOpenClosedSockets");
}

-(void)coverageRequestToServertaskbackgroundTheard
{
    NSOperationQueue *backgroundQueue=[[NSOperationQueue alloc]init];
    NSInvocationOperation *operation=[[NSInvocationOperation alloc]initWithTarget:self selector:@selector(coverageRequestToServertaskbackground) object:nil];
    
    [backgroundQueue addOperation:operation];
}

-(void)sendMsgActionToNative:(int)type txnid:(NSString *)txnid
{
    const char *txid=[txnid UTF8String];
    sendMsgActionToNative(type, txid);
}

-(void)coverageRequestToServertaskbackground
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *endDateString=[formatter stringFromDate:[NSDate date]];
    NSDate *startDate = [formatter dateFromString:endDateString];
    NSDate *endDate = [formatter dateFromString:[[NSUserDefaults standardUserDefaults] objectForKey:@"startdate"]];
    
    NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
    
    double seconds = fabs(timeDifference);
    if (seconds > 60)
    {
        const char *tagOutName=[[[[NSUserDefaults standardUserDefaults] objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] UTF8String];
        [self  sendLoginName:tagOutName];
    }
    [self get];
    [self coverageRequestToServertask];
}

-(void)cancelAllobjoperations
{
    [mainQueue cancelAllOperations];
    [mainQueueAudio cancelAllOperations];
}


@end
