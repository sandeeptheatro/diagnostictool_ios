//
//  InstoreData.h
//  Managers
//
//  Created by theatro2mac on 14/07/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstoreData : NSObject
{
    //Data Member
    bool inStoreTGS;
    NSString *inStoreTgsStoreName;
    NSString *inStoreTgsChainName;
    NSString *inStoreTgsIpAddress;
}

    //getter
    -(bool)isInStoreTGS;
    -(NSString*)getInStoreTgsStoreName;
    -(NSString*)getInStoreTgsChainName;
    -(NSString*)getInStoreTgsIpAddress;

    //setter
    -(void)setInStoreTGS:(bool)inStoreTGS;
    -(void)setInStoreTgsStoreName:(NSString*)storeName;
    -(void)setInStoreTgsChainName:(NSString*)chainName;
    -(void)setInStoreTgsIpAddress:(NSString*)tgsIpAddress;

+ (id)sharedManager;
@end
