//
//  SendOperation.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TheatroComLib.h"

@interface StateMachineHandler : NSOperation
{
    BOOL recordStarted;
}

-(void)communicator:(BOOL)value;
-(void)killTheatroNativelogonstealing;
-(void)enableBroadcastScreen:(BOOL)value;
-(void)doQueryButtonAction:(BOOL)value;
-(void) sendPlayMsgToNative;
-(void) sendPlayAnncToNative;
-(void) sendCancelPlayoutToNative:(BOOL)value;
-(void) sendCallEndToNative;
-(void) sendPlaybufferenable:(BOOL)value;
-(void) mediabuttonAction:(NSString *) cmd nameStr:(NSString *)nameStr Button:(BOOL) down;
-(void) reOpenClosedSockets;
-(void) sendHuddleInfoToNative:(NSString *)huddleTime huddleType:(int)huddleType;
-(void)coverageRequestToServertask;
-(void)coverageRequestToServertaskbackgroundTheard;
-(void)sendMsgActionToNative:(int) type txnid:(NSString *)txnid;
-(void)cancelAllobjoperations;
-(void)sendRefreshEvent;
+ (id)sharedManager;
-(void)enterLockScreen;
-(void) sendNetworkStatus:(NETWORK_STATE)value;
-(void) sendSwitchStatus:(bool)value;
-(bool)isNativeStarted;
-(void)sendLoginName:(const char*)name;
-(void)killTheatroNative;

/*!
 * @discussion This is callback function of c++ when communicator starts and here we are checking whether connected to server are not if connected getting json string.
 * @param coverageString Here we will total coverage of json string.
 */
-(void)filledStatus:(const char *)coverageString;
@property(nonatomic,readwrite)BOOL isIFCValue;

@end
