//
//  AppDelegate.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 12/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "NSLogger.h"
#import "StateMachineHandler.h"
#import "StoreListViewController.h"
#import "Reachability.h"
#import "AlertView.h"
#import "Constant.h"
#import "LeftMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "SlideNavigationController.h"
#import "UAirship.h"
#import "UAConfig.h"
#import "UAPush.h"
#import "HttpHandler.h"
#import "Earboxinboxmodel.h"
#import "VPViewController.h"
#import "UAAppIntegration.h"
#import "UIViewController.h"
#import "EarboxViewController.h"
#import "TWMessageBarManager.h"
#import "StringConstants.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "StateMachineHandler.h"
#import "InstoreData.h"
#import "NetworkController.h"
#import "DiagnosticTool.h"
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#import "EarboxDataStore.h"
#import "AudioManager.h"



static UIColor *kTWMesssageBarDemoControllerButtonColor = nil;
@interface AppDelegate ()<UNUserNotificationCenterDelegate>
{
    Reachability *internetReachable;
    NSString *storeNameStr,*alertMessage;
    NSString *storeName;
    NSDictionary *notificationinfo;
    BOOL isUMAapp;
}

@end

// Strings
NSString * const kAppDelegateDemoStyleSheetImageIconError = @"icon-error";



@implementation AppDelegate


- (id)init
{
    return [self initWithStyleSheet:NULL];
}

+ (void)initialize
{
    kTWMesssageBarDemoControllerButtonColor = [UIColor colorWithWhite:0.0 alpha:0.25];
    
}

- (id)initWithStyleSheet:(NSObject<TWMessageBarStyleSheet> *)stylesheet
{
    self = [super init];
    if (self)
    {
        //[TWMessageBarManager sharedInstance].styleSheet = stylesheet;
    }
    return self;
}

-(void)forgroundNotificationShow
{
    NSString *role=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"roleId"]];
    int roleid = 0;
    roleid = [role intValue];
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"NotificationFilter"] isEqualToString:@"NO"])
    {
        if (roleid>10)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnotificationrecieved" object:nil];
            //[self requestUndeliveredMsgsinbox];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnotificationrecieved" object:nil];
            //[self requestUndeliveredMsgs];
        }
    }
    else
    {
        // requestUndeliveredMsgsinbox
        
        [[NSUserDefaults standardUserDefaults]setObject:[notificationinfo objectForKey:@"callType"] forKey:@"notificationtype"];
        if (roleid>10)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"pushForMultiStore"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"localnotification" object:nil];
            NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
            [myQueue addOperationWithBlock:^{
                if( [[StateMachineHandler sharedManager]isNativeStarted])
                {
                    [[StateMachineHandler sharedManager] killTheatroNative];
                }
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                }];
            }];
        }
        else
        {
            
            if ([storeNameStr isEqualToString:storeName])
            {
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                         bundle: nil];
                [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
                EarboxViewController *earboxVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"EarboxViewController"];
                
                NSString *chain=[alertMessage componentsSeparatedByString:@","][1];
                NSString *chainName=[[chain componentsSeparatedByString:@"/"][0]substringFromIndex:1];
                [[NSUserDefaults standardUserDefaults]setObject:storeName forKey:@"storeNameString"];
                NSDictionary *twmAppLogindic=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
                NSMutableDictionary *updatedic=[[NSMutableDictionary alloc]init];
                [updatedic setObject:[twmAppLogindic objectForKey:@"companyId"] forKey:@"companyId"];
                [updatedic setObject:[twmAppLogindic objectForKey:@"firstTimeLogin"] forKey:@"firstTimeLogin"];
                [updatedic setObject:[twmAppLogindic objectForKey:@"tagOutName"] forKey:@"tagOutName"];
                [updatedic setObject:[twmAppLogindic objectForKey:@"userId"] forKey:@"userId"];
                [updatedic setObject:chainName forKey:@"chainName"];
                [[NSUserDefaults standardUserDefaults]setObject:updatedic forKey:@"twmAppLogin"];
                [NSThread detachNewThreadSelector:@selector(communicatorStart) toTarget:self withObject:nil];
                
                if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"EarboxViewController"])
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnotificationrecieved" object:nil];
                }
                else
                {
                    [[SlideNavigationController sharedInstance] pushViewController:earboxVC animated:YES];
                }
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"pushForDiffStore"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] setObject:storeName forKey:@"StoreNameForPush"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"localnotification" object:nil];
            }
        }
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        //Cancel
    }
    else
    {
        // NSString *urlString = @"https://itunes.apple.com/in/app/managers/id1016449175?mt=8";
        NSString *urlString =@"itms-apps://itunes.apple.com/in/app/managers/id1016449175?mt=8&uo=4";
        NSString *escaped = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escaped]];
        // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/in/app/managers/id1016449175?mt=8"]];
    }
}

/*static void onNotifyCallback(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo)
 {
 NSString* notifyName = (__bridge NSString*)name;
 // this check should really only be necessary if you reuse this one callback method
 //  for multiple Darwin notification events
 if ([notifyName isEqualToString:@"com.apple.system.config.network_change"]) {
 // use the Captive Network API to get more information at this point
 //  http://stackoverflow.com/a/4714842/119114
 NSString *wifiName = nil;
 NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
 for (NSString *ifnam in ifs) {
 NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
 
 NSLog(@"info:%@",info);
 
 if (info[@"SSID"]) {
 wifiName = info[@"SSID"];
 //    [[StateMachineHandler sharedManager] sendNetworkStatus:WIFI_SSID_CHANGED];
 }
 }
 } else {
 NSLog(@"intercepted %@", notifyName);
 }
 }*/

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    NSLog(@"App delegate %@", shortcutItem.type);
    if([shortcutItem.type isEqualToString:@"Inbox"]){
        //ACTION HERE
        [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"pushForMultiStore"];
    }
    if([shortcutItem.type isEqualToString:@"Switch to C2"]){
        //ACTION HERE
        [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"pushForMultiStore"];
        [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"pushForC2"];
        
    }
}

- (NSString *)shortenedVersionNumberString:(NSString *)shortenedVersionNumber {
    static NSString *const unnecessaryVersionSuffix = @".0";
    
    while ([shortenedVersionNumber hasSuffix:unnecessaryVersionSuffix]) {
        shortenedVersionNumber = [shortenedVersionNumber substringToIndex:shortenedVersionNumber.length - unnecessaryVersionSuffix.length];
    }
    
    return shortenedVersionNumber;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSString *uniqueString;
    
    isUMAapp = YES; // Change this variable to enable/disable UMA.
   
//    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"NotificationFilter"] isEqualToString:@"YES"])
//    {
//        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"NotificationFilter"];
//    }
//    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"SwitchtoC2"];
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"UMA"];
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"Toggled"];
    
    
    
//    UIApplication* application1 = [UIApplication sharedApplication];
//
//    [application1 registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    
    // check for wifi connection.
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
    
    /*
     CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), //center
     NULL, // observer
     onNotifyCallback, // callback
     CFSTR("com.apple.system.config.network_change"), // event name
     NULL, // object
     CFNotificationSuspensionBehaviorDeliverImmediately);
     */
    
    UIApplicationShortcutIcon * photoIcon = [UIApplicationShortcutIcon iconWithTemplateImageName: @"inbox"]; // your customize icon
    UIApplicationShortcutItem * photoItem = [[UIApplicationShortcutItem alloc]initWithType: @"Inbox" localizedTitle:@"Inbox" localizedSubtitle: nil icon: photoIcon userInfo: nil];
    UIApplicationShortcutItem * c2 = [[UIApplicationShortcutItem alloc]initWithType: @"Switch to C2" localizedTitle:@"Switch to C2" localizedSubtitle: nil icon: nil userInfo: nil];
    // UIApplicationShortcutItem * videoItem = [[UIApplicationShortcutItem alloc]initWithType: @"video" localizedTitle: @"take video" localizedSubtitle: nil icon: [UIApplicationShortcutIcon iconWithType: UIApplicationShortcutIconTypeCaptureVideo] userInfo: nil];
    
    [UIApplication sharedApplication].shortcutItems = @[photoItem,c2];
    
    NSDictionary *appinfo=[HttpHandler appstoreVersioncheck];
    NSString *appStoreVersion=[[[appinfo objectForKey:@"results"] objectAtIndex:0] objectForKey:@"version"];
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *appversion = [infoDict objectForKey:@"CFBundleVersion"];
    
    appStoreVersion = [self shortenedVersionNumberString:appStoreVersion]; // now 1.2
    appversion = [self shortenedVersionNumberString:appversion]; // still 1.1.5
    
    if ([appStoreVersion compare:appversion options:NSNumericSearch] == NSOrderedDescending)
    {
   
//         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Update Alert" message:@"You have new update of ManagerApp" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
//         [alert show];
    }
    else
    {
        
        //Nothing
    }
    UAConfig *config = [UAConfig defaultConfig];
    [UAirship takeOff:config];
    
    [UAirship push].notificationOptions = (UIUserNotificationTypeAlert |
                                           UIUserNotificationTypeBadge |
                                           UIUserNotificationTypeSound );
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    [UAirship push].autobadgeEnabled=YES;
    [UAirship setLogging:YES];
    [UAirship push].userPushNotificationsEnabled = YES;
    [[NSLogger sharedInstance] writeNSLogToFile];
    if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
        uniqueString=[[NSString stringWithFormat:@"%@",
                       [[[[UIDevice currentDevice] identifierForVendor] UUIDString]substringFromIndex:24]]lowercaseString];
        NSLog(@"CurrentDevice UUIDString is:%@",uniqueString);
       // [AlertView alert:@"UUIDString" message:[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
        NSString *macid=[uniqueString substringFromIndex:7];
        [[NSUserDefaults standardUserDefaults]setObject:macid forKey:@"macid"];
        [[NSUserDefaults standardUserDefaults]setObject:uniqueString forKey:@"macidfull"];
    }
    if ([[[UAirship push]channelID] length]!=0)
    {
        [[NSUserDefaults standardUserDefaults] setObject:[[UAirship push]channelID] forKey:@"deviceTokenString"];
    }
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    LeftMenuViewController *leftMenu = (LeftMenuViewController*)[mainStoryboard
                                                                 instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
    
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
    internetReachable = [Reachability reachabilityForInternetConnection];
    [internetReachable startNotifier];
   [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(checkNetworkStatus:)
                                                 name:kReachabilityChangedNotification object:nil];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"firsttime"])
    {
        //nothing
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeview"] isEqualToString:@"storeview"])
        {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            NSString *role=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"roleId"]];
            int roleid = 0;
            roleid = [role intValue];
            if (roleid>10)
            {
                VPViewController *storeListVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"VPViewController"];
                [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:storeListVC
                                                                         withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                                 andCompletion:nil];
            }
            else
            {
                StoreListViewController *storeListVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"StoreListViewController"];
                [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:storeListVC
                                                                         withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                                 andCompletion:nil];
            }
            
        }
        else
        {
            
            
        }
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"serverpath"];
        
        [[NSUserDefaults standardUserDefaults]setObject:@"https://central.theatro.com/" forKey:@"serverpathGlobal"];
        //[[NSUserDefaults standardUserDefaults]setObject:@"websockets.theatro.com" forKey:@"websocketpath"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firsttime"];
        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
        [[UIApplication sharedApplication]cancelAllLocalNotifications];
        
    }
    
    /*
     NSArray *interfaces = CFBridgingRelease(CNCopySupportedInterfaces());
     for (NSString *interface in interfaces) {
     NSDictionary *networkInfo = CFBridgingRelease(CNCopyCurrentNetworkInfo((__bridge CFStringRef)(interface)));
     
     if (networkInfo != NULL) {
     
     [self getIPAddress];
     [self broadCast];
     
     if( [[InstoreData sharedManager] isInStoreTGS])
     {
     //If instore TGS has got detect , Start a TCP thread...
     static dispatch_once_t pred;
     dispatch_once(&pred, ^{
     [[NetworkController sharedInstance] connect];
     });
     [[StateMachineHandler sharedManager] sendNetworkStatus:WIFI_ON_TGS_ON];
     
     }
     else {
     [[StateMachineHandler sharedManager] sendNetworkStatus:WIFI_ON_TGS_OFF];
     }
     break;
     }
     else
     {
     
     }
     }
     */
    
    [Fabric with:@[[CrashlyticsKit class]]];
    
    NSLog(@" Appdelegate application didFinishLaunchingWithOptions");
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    // Override point for customization after application launch.
    return YES;
}


void uncaughtExceptionHandler(NSException *exception)
{
    NSLog(@"Appdelegate Application Crash details: %@", exception);
    // Internal error reporting
}


#pragma mark - discovery

- (NSString *)getIPAddress {
    
    NSString *address = @"error";
    // NSString *netmask = @"error";
    broadcastAddr1 = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    // netmask = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_netmask)->sin_addr)];
                    
                    
                    broadcastAddr1 = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_dstaddr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

- (void)broadCast
{
    // Configure the port and ip we want to send to
    struct sockaddr_in broadcastAddr;
    const char *datechar = [broadcastAddr1 UTF8String];
    
    memset(&broadcastAddr, 0, sizeof(broadcastAddr));
    broadcastAddr.sin_family = AF_INET;
    inet_pton(AF_INET,datechar, &broadcastAddr.sin_addr);
    broadcastAddr.sin_port = htons(41102);
    
    
    [self initlisten];
    
    NSString *macid=[[NSUserDefaults standardUserDefaults]objectForKey:@"macidfull"];
    
    char temp1[500] = "{\"cmd\":\"auth\",\"ver\":2,\"mac\":\"";
    char request[500];
    
    char temp2[500] = "\",\"svnver\":\"instore\",\"time\":\"369156205\",\"volume\":0,\"battery\":5555,\"chargestat\":0,\"batterytemp\": 0}";
    
    strcpy(request,temp1);
    strcat(request,[macid UTF8String]);
    strcat(request,temp2);
    
    // char request[500] ={"{\"cmd\":\"auth\",\"ver\":2,\"mac\":\"macId\",\"svnver\":\"instore\",\"time\":\"369156205\",\"volume\":0,\"battery\":5555,\"chargestat\":0,\"batterytemp\": 0}"};
    
    int numTries = 0;
    while (1)
    {
        long ret = sendto(listeningSocket, request, strlen(request), 0, (struct sockaddr*)&broadcastAddr, sizeof(broadcastAddr));
        if (ret < 0) {
            NSLog(@"Error: Could not open send broadcast.");
            close(listeningSocket);
            return;
        }
        
        
        if([self receivePackets])
        {
            /*
             NSLog(@"InStore: In store TGS has been detected IP=%@", [[InstoreData sharedManager]getInStoreTgsIpAddress]);
             
             //Send notification to Show green title bar...
             [[NSNotificationCenter defaultCenter]postNotificationName:@"InStore" object:@"YES"];
             [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
             
             
             NSString *localIp=[NSString stringWithFormat:@"http://%@:8088/",[[InstoreData sharedManager]getInStoreTgsIpAddress]];
             // NSString *originalUrl=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
             
             
             // [[NSUserDefaults standardUserDefaults] setObject:originalUrl forKey:@"originalUrl"];
             
             
             [[NSUserDefaults standardUserDefaults] setObject:localIp forKey:@"serverpath"];
             NSString *oldPath =[[NSUserDefaults standardUserDefaults]objectForKey:@"originalUrl"];
             NSString *original =[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
             
             
             break;
             */
            if  ([[InstoreData sharedManager] isInStoreTGS])
            {
                NSLog(@"InStore: In store TGS has been detected IP=%@", [[InstoreData sharedManager]getInStoreTgsIpAddress]);
                
                NSString *ipAddress=[NSString stringWithFormat:@"http://%@/",[[InstoreData sharedManager]getInStoreTgsIpAddress]];
                BOOL status = [HttpHandler checkCentralServerAvailabilityForInStore:ipAddress];
                if (status==YES || [[[NSUserDefaults standardUserDefaults] objectForKey:@"Toggled"] isEqualToString:@"YES"])
                {
                //Send notification to Show green title bar...
                [[NSNotificationCenter defaultCenter]postNotificationName:@"InStore" object:@"YES"];
                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"InStore"];
                
                
                NSString *localIp=[NSString stringWithFormat:@"http://%@:8088/",[[InstoreData sharedManager]getInStoreTgsIpAddress]];
                NSString *originalUrl=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
                
                NSString *globalStr =[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpathGlobal"];
                
                if (globalStr.length==0)
                {
                    [[NSUserDefaults standardUserDefaults] setObject:originalUrl forKey:@"serverpathGlobal"];
                }
                
                [[NSUserDefaults standardUserDefaults] setObject:localIp forKey:@"serverpath"];
                break;
                }
                else
                {
                    [self StopInstore];
                }
            }
        }
        
        if(numTries++ >= 3)
        {
            [self StopInstore];
            break;
        }
    }
    close(listeningSocket);
    NSLog(@"Error: closing down socket.");
    
    
}

-(void)StopInstore
{
    NSLog(@"InStore: TGS is not Instore");
    //Send notification to Remove the green title bar...
    [[NSNotificationCenter defaultCenter]postNotificationName:@"InStore" object:@"NO"];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
    //[[NSUserDefaults standardUserDefaults]setObject:settingsTextField.text forKey:@"serverpathGlobal"]
    
    NSString *originalUrl=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpathGlobal"];
    
    if (originalUrl.length==0)
    {
        NSString *original =[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpath"];
        [[NSUserDefaults standardUserDefaults] setObject:original forKey:@"serverpathGlobal"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:originalUrl forKey:@"serverpath"];
    }
    
    
    
    [[InstoreData sharedManager]setInStoreTGS:false];
    
    if( [[NetworkController sharedInstance] isConnected])
    {
        [[NetworkController sharedInstance] stopTCPConnection];
    }
}

- (void)initlisten
{
    
    listeningSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (listeningSocket <= 0) {
        NSLog(@"Error:  listenForPackets - socket() failed.");
        return;
    }
    
    int broadcastEnable = 1;
    int ret = setsockopt(listeningSocket, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));
    if (ret) {
        NSLog(@"Error: Could not open set socket to broadcast mode");
        close(listeningSocket);
        return;
    }
    
    // set timeout to 2 seconds.
    struct timeval timeV;
    timeV.tv_sec = 0;
    timeV.tv_usec = 300000;
    
    if (setsockopt(listeningSocket, SOL_SOCKET, SO_RCVTIMEO, &timeV, sizeof(timeV)) == -1) {
        NSLog(@"Error: listenForPackets - setsockopt failed");
        close(listeningSocket);
        return;
    }
    
    
    // bind the port
    struct sockaddr_in sockaddr;
    memset(&sockaddr, 0, sizeof(sockaddr));
    
    sockaddr.sin_len = sizeof(sockaddr);
    sockaddr.sin_family = AF_INET;
    sockaddr.sin_port = htons(51101);
    sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    
    int status = bind(listeningSocket, (struct sockaddr *)&sockaddr, sizeof(sockaddr));
    if (status == -1) {
        close(listeningSocket);
        NSLog(@"Error: listenForPackets - bind() failed.");
        return;
    }
    
}

char buf[300];
NSData *data = nil;

- (int) receivePackets
{
    
    NSLog(@"Receive called ...socket.");
    
    
    // receive
    struct sockaddr_in receiveSockaddr;
    socklen_t receiveSockaddrLen = sizeof(receiveSockaddr);
    
    ssize_t result = recvfrom(listeningSocket, buf, 1024, 0, (struct sockaddr *)&receiveSockaddr, &receiveSockaddrLen);
    
    
    if (result > 0) {
        
        data = [NSData dataWithBytesNoCopy:buf length:result freeWhenDone:NO];
        
        char addrBuf[INET_ADDRSTRLEN];
        if (inet_ntop(AF_INET, &receiveSockaddr.sin_addr, addrBuf, (size_t)sizeof(addrBuf)) == NULL) {
            addrBuf[0] = '\0';
        }
        
        address = [NSString stringWithCString:addrBuf encoding:NSASCIIStringEncoding];
        
        NSString *msg = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] lowercaseString];
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[msg dataUsingEncoding:NSUTF8StringEncoding]options:0 error:NULL];
        
        NSString *storename = [dict objectForKey:@"servername"];
        storename=[[storename componentsSeparatedByString:@"/"] lastObject];
        
        NSString *companyName = [dict objectForKey:@"servername"];
        companyName=[[companyName componentsSeparatedByString:@"/"] objectAtIndex:0];
        
        if (!([dict objectForKey:@"servername"]==nil) && storename.length>0)
        {
            [[InstoreData sharedManager]setInStoreTGS:true];
            [[InstoreData sharedManager]setInStoreTgsStoreName:[storename uppercaseString]];
            [[InstoreData sharedManager]setInStoreTgsChainName:[companyName uppercaseString]];
            [[InstoreData sharedManager]setInStoreTgsIpAddress:address];
        }
        else
        {
            [[InstoreData sharedManager]setInStoreTGS:false];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"InStore" object:@"NO"];
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
            
            if( [[NetworkController sharedInstance] isConnected])
            {
                [[NetworkController sharedInstance] stopTCPConnection];
            }
            
        }
        
        
        
        [[NSUserDefaults standardUserDefaults] setObject:address forKey:@"Path"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //
        NSLog(@"Instore: TGS Ip:%@",address);
        
        return 1;
    }
    else
    {
        [[InstoreData sharedManager]setInStoreTGS:false];
        return 0;
    }
    
}



#pragma mark - Network status

-(BOOL)dataNetwork
{
    CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
    //NSString *carrier = [[netinfo subscriberCellularProvider] carrierName];
    
    if ([netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyGPRS] || [netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyEdge] || [netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyWCDMA] || [netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyHSDPA] || [netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyHSUPA] || [netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyCDMA1x] || [netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyCDMAEVDORev0] || [netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyCDMAEVDORevA] || [netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyCDMAEVDORevB] || [netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyeHRPD] || [netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyLTE])
        
    {
        NSLog(@"2G  2G 3G");
        return YES;
        
    }
    else
    {
        return NO;
    }
}

-(long int)getCurrentTimeinSeconds
{
    return  [[NSDate date] timeIntervalSince1970];
}


- (void)checkNetworkStatus:(NSNotification *)notice {
    
    // called after network status changes
    // Set up Reachability
    
   // if(isUMAapp)
    //{
    NSString *str=[[NSUserDefaults standardUserDefaults] objectForKey:@"UMA"];
    //NSString *str=@"NO";//[[NSUserDefaults standardUserDefaults] objectForKey:@"UMA"];
    if ([str isEqualToString:@"YES"])
    {
        
        bool isCommStopped = false;
        
        if ( netWorkStatusTime == 0 ||([self getCurrentTimeinSeconds] - netWorkStatusTime)>2)
        {
            NSLog(@"abhi Called");
        }
        else
        {
            NSLog(@"abhi Skipped");
            return;
        }
        
        netWorkStatusTime = [self getCurrentTimeinSeconds];
        
        if([[StateMachineHandler sharedManager] isNativeStarted])
        {
            [[StateMachineHandler sharedManager] sendSwitchStatus:YES];
            [[StateMachineHandler sharedManager] sendCancelPlayoutToNative:YES];
            [[StateMachineHandler sharedManager] killTheatroNative];
            [[StateMachineHandler sharedManager] sendSwitchStatus:NO];
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"resetscreenNotification" object:nil];
            isCommStopped = true;
        }
        
        //BOOL hasWiFiNetwork = NO;
        NSArray *interfaces = CFBridgingRelease(CNCopySupportedInterfaces());
        for (NSString *interface in interfaces) {
            NSDictionary *networkInfo = CFBridgingRelease(CNCopyCurrentNetworkInfo((__bridge CFStringRef)(interface)));
            
            
            
            if (networkInfo != NULL) {
                //  hasWiFiNetwork = YES;
                
                [self getIPAddress];
                [self broadCast];
                
                if( [[InstoreData sharedManager] isInStoreTGS])
                {
                    //If instore TGS has got detect , Start a TCP thread...
                    
                    //static dispatch_once_t pred;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NetworkController sharedInstance] connect];
                    });
                    [[StateMachineHandler sharedManager] sendNetworkStatus:WIFI_ON_TGS_ON];
                }
                else
                {
                    
                    [[InstoreData sharedManager]setInStoreTGS:false];
                    
                    if( [[NetworkController sharedInstance] isConnected])
                    {
                        [[NetworkController sharedInstance] stopTCPConnection];
                    }
                    
                    [[StateMachineHandler sharedManager] sendNetworkStatus:WIFI_ON_TGS_OFF];
                }
                
                break;
            }
            else
            {
                
                [[InstoreData sharedManager]setInStoreTGS:false];
                
                if( [[NetworkController sharedInstance] isConnected])
                {
                    [[NetworkController sharedInstance] stopTCPConnection];
                }
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"InStore" object:@"NO"];
                //Remove the green title bar...and update the url - change back to theatro.com
                
                NSString *originalUrl=[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpathGlobal"];
                
                [[NSUserDefaults standardUserDefaults] setObject:originalUrl forKey:@"serverpath"];
                
                
                [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
                
                if(self.dataNetwork)
                {
                    [[StateMachineHandler sharedManager] sendNetworkStatus:WIFI_OFF_CELLULAR_ON];
                }
                else
                {
                    [[StateMachineHandler sharedManager] sendNetworkStatus:WIFI_OFF_CELLULAR_OFF];
                }
            }
            
        }
        
        if(isCommStopped)
        {
            [[StateMachineHandler sharedManager] communicator:YES];
            [[StateMachineHandler sharedManager] sendCancelPlayoutToNative:NO];
        }
        
    }
    else // isUMAapp is enable(NO) HMA.
    {
        if ([[InstoreData sharedManager] isInStoreTGS])
        {
            [self StopInstore];
        }
        [[StateMachineHandler sharedManager] sendNetworkStatus:WIFI_OFF_CELLULAR_ON];
    }
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    NSDictionary *dictLang=[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    switch (internetStatus)
    {
        case NotReachable:
        {
            [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"AppDelegateVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AppDelegate"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
            break;
        }
            
        case ReachableViaWWAN:
        {
            
            break;
        }
        case ReachableViaWiFi:
        {
            break;
        }
    }
    
    
}


- (void)AppNetworkStatus
{
    // called from communicator before startTheatroNative.
    // Set up Reachability
    NSString *str=[[NSUserDefaults standardUserDefaults] objectForKey:@"UMA"];
    if ([str isEqualToString:@"YES"])
    {// isUMAapp is enable(YES) UMA.
      //if (isUMAapp)
      //{
        //BOOL hasWiFiNetwork = NO;
        NSArray *interfaces = CFBridgingRelease(CNCopySupportedInterfaces());
        for (NSString *interface in interfaces) {
            NSDictionary *networkInfo = CFBridgingRelease(CNCopyCurrentNetworkInfo((__bridge CFStringRef)(interface)));
            
            if (networkInfo != NULL) {
                // hasWiFiNetwork = YES;
                
                if( [[InstoreData sharedManager] isInStoreTGS])
                {
                    [[StateMachineHandler sharedManager] sendNetworkStatus:WIFI_ON_TGS_ON];
                }
                else {
                    [[StateMachineHandler sharedManager] sendNetworkStatus:WIFI_ON_TGS_OFF];
                    NSString *strPath =[[NSUserDefaults standardUserDefaults]objectForKey:@"serverpathGlobal"];
                    
                    strPath=[strPath stringByReplacingOccurrencesOfString:@"https://" withString:@""];
                    strPath=[strPath stringByReplacingOccurrencesOfString:@"/" withString:@""];
                    
                    const char *websocketPath =[strPath UTF8String];
                    
                    sendWebSocketPathToNative(websocketPath);
                }
                
                break;
            }
            else
            {
                if(self.dataNetwork)
                {
                    [[StateMachineHandler sharedManager] sendNetworkStatus:WIFI_OFF_CELLULAR_ON];
                }
                else
                {
                    [[StateMachineHandler sharedManager] sendNetworkStatus:WIFI_OFF_CELLULAR_OFF];
                }
            }
            
        }
    }
    else  // isUMAapp is enable(NO) HMA.
    {
        [[StateMachineHandler sharedManager] sendNetworkStatus:WIFI_OFF_CELLULAR_ON];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *startDateString=[formatter stringFromDate:[NSDate date]];
    [[NSUserDefaults standardUserDefaults]setObject:startDateString forKey:@"startdate"];
    [[StateMachineHandler sharedManager]cancelAllobjoperations];
    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication]cancelAllLocalNotifications];
    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:[[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"] intValue]];
    NSLog(@" Appdelegate applicationWillResignActive");
}

-(void)requestUndeliveredMsgsinbox
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
     NSDictionary *dictLang=[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    if (internetStatus == NotReachable)
    {
         [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"AppDelegateVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AppDelegate"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
        
    }
    else
    {
        //requesting for undelivered messages and announcements......
        int companyId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"companyId"]intValue];
        NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        NSOperationQueue *myQueuemessage = [[NSOperationQueue alloc] init];
        [myQueuemessage addOperationWithBlock:^{
            
            NSDictionary *twundeliveredMsgListdic= [HttpHandler earboxundeliveredlistV2:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId twreqFor:@"message" userScreen:@"AppDelegate"];
            NSDictionary *twundeliveredAnccListdic= [HttpHandler earboxundeliveredlistV2:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId twreqFor:@"announcement" userScreen:@"AppDelegate"];
            NSDictionary *twundeliveredGroupListdic= [HttpHandler earboxundeliveredlistV2:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId twreqFor:@"groupmessage" userScreen:@"AppDelegate"];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if (![[[twundeliveredMsgListdic objectForKey:@"twResponseHeader" ] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]] || ![[[twundeliveredMsgListdic objectForKey:@"twResponseHeader" ] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]] || ![[[twundeliveredMsgListdic objectForKey:@"twResponseHeader" ] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
                {
                    NSArray *messageinbox,*messagesent,*messagesaved,*announcementinbox,*announcementsent,*announcementsaved,
                    *groupinbox,*groupsent,*groupsaved;
                    if ([[twundeliveredMsgListdic objectForKey:@"earbox"] isKindOfClass:[NSNull class]])
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredfailedInbox" forKey:@"undeliveredfailedInbox"];
                    }
                    else
                    {
                        
                        NSDictionary *message=[[twundeliveredMsgListdic objectForKey:@"earbox"]objectForKey:@"message"];
                        
                        for (int i=0; i<message.count; i++)
                        {
                            messageinbox=[message objectForKey:@"inbox"];
                            
                            messagesent=[message  objectForKey:@"sent"];
                            
                            messagesaved=[message objectForKey:@"saved"];
                        }
                        
                        if (messageinbox.count>0)
                        {
                            [[Earboxinboxmodel sharedModel]messageinboxSave:messageinbox];
                            
                            NSInteger messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                            
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:messagecount+[messageinbox count]] forKey:@"messagetotalcount"];
                        }
                        if (messagesaved.count>0)
                        {
                            [[Earboxinboxmodel sharedModel] messagesavedSave:messagesaved];
                            
                            NSInteger messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                            
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:messagecount+[messagesaved count]] forKey:@"messagetotalcount"];
                        }
                        
                        if (messagesent.count>0)
                        {
                            [[Earboxinboxmodel sharedModel] messagesentSave:messagesent];
                        }
                    }
                    if ([[twundeliveredAnccListdic objectForKey:@"earbox"] isKindOfClass:[NSNull class]])
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredfailedInbox" forKey:@"undeliveredfailedInbox"];
                    }
                    else
                    {
                        NSDictionary *announcement=[[twundeliveredAnccListdic objectForKey:@"earbox"] objectForKey:@"announcement"];
                        
                        for (int i=0; i<announcement.count; i++)
                        {
                            announcementinbox=[announcement objectForKey:@"inbox"];
                            
                            announcementsent=[announcement  objectForKey:@"sent"];
                            
                            announcementsaved=[announcement  objectForKey:@"saved"];
                            
                        }
                        if (announcementinbox.count>0)
                        {
                            [[Earboxinboxmodel sharedModel] announcementinboxSave:announcementinbox];
                            int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementtotalcount"] intValue];
                            NSInteger an_Count=0;
                            
                            an_Count = [announcementinbox count];
                            
                            an_Count = an_Count+announcementcount;
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:an_Count] forKey:@"announcementtotalcount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        if (announcementsaved.count>0)
                        {
                            [[Earboxinboxmodel sharedModel] announcementsavedSave:announcementsaved];
                            int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementtotalcount"] intValue];
                            NSInteger an_Count=0;
                            
                            an_Count = [announcementsaved count];
                            
                            an_Count = an_Count+announcementcount;
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:an_Count] forKey:@"announcementtotalcount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        
                        if (announcementsent.count>0)
                        {
                            [[Earboxinboxmodel sharedModel] announcementsentSave:announcementsent];
                        }
                    }
                    if ([[twundeliveredGroupListdic objectForKey:@"earbox"] isKindOfClass:[NSNull class]])
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredfailedInbox" forKey:@"undeliveredfailedInbox"];
                    }
                    else
                    {
                        NSDictionary *group=[[twundeliveredGroupListdic objectForKey:@"earbox"] objectForKey:@"groupMessage"];
                        
                        for (int i=0; i<group.count; i++)
                        {
                            groupinbox=[group objectForKey:@"inbox"];
                            
                            groupsent=[group  objectForKey:@"sent"];
                            
                            groupsaved=[group  objectForKey:@"saved"];
                            
                        }
                        if (groupinbox.count>0)
                        {
                            [[Earboxinboxmodel sharedModel] messageinboxSave:groupinbox];
                            int groupcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                            NSInteger an_Count=0;
                            
                            an_Count = [groupinbox count];
                            
                            an_Count = an_Count+groupcount;
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:an_Count] forKey:@"messagetotalcount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        if (groupsaved.count>0)
                        {
                            [[Earboxinboxmodel sharedModel] messagesavedSave:groupsaved];
                            int groupcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                            NSInteger an_Count=0;
                            
                            an_Count = [groupsaved count];
                            
                            an_Count = an_Count+groupcount;
                            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:an_Count] forKey:@"messagetotalcount"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                        }
                        if (groupsent.count>0)
                        {
                            [[Earboxinboxmodel sharedModel] messagesentSave:groupsent];
                        }
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"pushnotificationcount" object:@"messagecount"];
                    }
                }
            }];
        }];
    }
    
}

-(void)requestUndeliveredMsgs
{
    //Resetting the badge count...
    
    
    //requesting for undelivered messages...
    int storeId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"]intValue];
    int companyId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"companyId"]intValue];
    //if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"storeNameString"] isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:@"multistoreNameString"]])
    if (storeId!=0&&companyId!=0)
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
         NSDictionary *dictLang=[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
        if (internetStatus == NotReachable)
        {
             [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"AppDelegateVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AppDelegate"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
            
        }
        else
        {
            //requesting for undelivered messages and announcements......
            NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
            NSOperationQueue *myQueuemessage = [[NSOperationQueue alloc] init];
            [myQueuemessage addOperationWithBlock:^{
                
                NSDictionary *twundeliveredMsgListdic= [HttpHandler earboxundeliveredlistVeb:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId storeId:storeId twreqFor:@"message" userScreen:@"AppDelegate"];
                NSDictionary *twundeliveredAnccListdic= [HttpHandler earboxundeliveredlistVeb:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId storeId:storeId twreqFor:@"announcement" userScreen:@"AppDelegate"];
                NSDictionary *twundeliveredGroupListdic= [HttpHandler earboxundeliveredlistVeb:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] companyId:companyId storeId:storeId twreqFor:@"groupmessage" userScreen:@"AppDelegate"];
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    if (![[[twundeliveredMsgListdic objectForKey:@"twResponseHeader" ] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]] || ![[[twundeliveredAnccListdic objectForKey:@"twResponseHeader" ] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
                    {
                        NSArray *messageinbox,*messagesent,*messagesaved,*announcementinbox,*announcementsent,*announcementsaved,*groupinbox,*groupsent,*groupsaved;
                        if ([[twundeliveredMsgListdic objectForKey:@"earbox"] isKindOfClass:[NSNull class]])
                        {
                            [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredfailedEarbox" forKey:@"undeliveredfailedEarbox"];
                        }
                        else
                        {
                            
                            NSDictionary *message=[[twundeliveredMsgListdic objectForKey:@"earbox"]objectForKey:@"message"];
                            
                            for (int i=0; i<message.count; i++)
                            {
                                messageinbox=[message objectForKey:@"inbox"];
                                
                                messagesent=[message  objectForKey:@"sent"];
                                
                                messagesaved=[message objectForKey:@"saved"];
                            }
                            
                            if (messageinbox.count>0)
                            {
                                [[Earboxinboxmodel sharedModel]messageinboxSave:messageinbox];
                                
                                NSInteger messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                                
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:messagecount+[messageinbox count]] forKey:@"messagetotalcount"];
                            }
                            if (messagesaved.count>0)
                            {
                                [[Earboxinboxmodel sharedModel] messagesavedSave:messagesaved];
                                
                                NSInteger messagecount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                                
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:messagecount+[messagesaved count]] forKey:@"messagetotalcount"];
                            }
                            
                            if (messagesent.count>0)
                            {
                                [[Earboxinboxmodel sharedModel] messagesentSave:messagesent];
                            }
                        }
                        if ([[twundeliveredAnccListdic objectForKey:@"earbox"] isKindOfClass:[NSNull class]])
                        {
                            [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredfailedEarbox" forKey:@"undeliveredfailedEarbox"];
                        }
                        else
                        {
                            NSDictionary *announcement=[[twundeliveredAnccListdic objectForKey:@"earbox"] objectForKey:@"announcement"];
                            
                            for (int i=0; i<announcement.count; i++)
                            {
                                announcementinbox=[announcement objectForKey:@"inbox"];
                                
                                announcementsent=[announcement  objectForKey:@"sent"];
                                
                                announcementsaved=[announcement  objectForKey:@"saved"];
                                
                            }
                            if (announcementinbox.count>0)
                            {
                                [[Earboxinboxmodel sharedModel] announcementinboxSave:announcementinbox];
                                int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementtotalcount"] intValue];
                                NSInteger an_Count=0;
                                
                                an_Count = [announcementinbox count];
                                
                                an_Count = an_Count+announcementcount;
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:an_Count] forKey:@"announcementtotalcount"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                            }
                            if (announcementsaved.count>0)
                            {
                                [[Earboxinboxmodel sharedModel] announcementsavedSave:announcementsaved];
                                int announcementcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"announcementtotalcount"] intValue];
                                NSInteger an_Count=0;
                                
                                an_Count = [announcementsaved count];
                                
                                an_Count = an_Count+announcementcount;
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:an_Count] forKey:@"announcementtotalcount"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                            }
                            
                            if (announcementsent.count>0)
                            {
                                [[Earboxinboxmodel sharedModel] announcementsentSave:announcementsent];
                            }
                        }
                        if ([[twundeliveredGroupListdic objectForKey:@"earbox"] isKindOfClass:[NSNull class]])
                        {
                            [[NSUserDefaults standardUserDefaults]setObject:@"undeliveredfailedEarbox" forKey:@"undeliveredfailedEarbox"];
                        }
                        else
                        {
                            NSDictionary *group=[[twundeliveredGroupListdic objectForKey:@"earbox"] objectForKey:@"groupMessage"];
                            
                            for (int i=0; i<group.count; i++)
                            {
                                groupinbox=[group objectForKey:@"inbox"];
                                
                                groupsent=[group  objectForKey:@"sent"];
                                
                                groupsaved=[group  objectForKey:@"saved"];
                                
                            }
                            if (groupinbox.count>0)
                            {
                                [[Earboxinboxmodel sharedModel] messageinboxSave:groupinbox];
                                int groupcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                                NSInteger an_Count=0;
                                
                                an_Count = [groupinbox count];
                                
                                an_Count = an_Count+groupcount;
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:an_Count] forKey:@"messagetotalcount"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                            }
                            if (groupsaved.count>0)
                            {
                                [[Earboxinboxmodel sharedModel] messagesavedSave:groupsaved];
                                int groupcount=[[[NSUserDefaults standardUserDefaults]objectForKey:@"messagetotalcount"] intValue];
                                NSInteger an_Count=0;
                                
                                an_Count = [groupsaved count];
                                
                                an_Count = an_Count+groupcount;
                                [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:an_Count] forKey:@"messagetotalcount"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                
                            }
                            if (groupsent.count>0)
                            {
                                [[Earboxinboxmodel sharedModel] messagesentSave:groupsent];
                            }
                            
                        }
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"pushnotificationcount" object:@"messagecount"];
                    }
                }];
            }];
        }
        
    }
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"Appdelegate applicationDidEnterBackground");
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *startDateString=[formatter stringFromDate:[NSDate date]];
    [[NSUserDefaults standardUserDefaults]setObject:startDateString forKey:@"startdate"];
    
    if ([[InstoreData sharedManager] isInStoreTGS])
    {
        if ([[NetworkController sharedInstance] isConnected])
        {
            [[NetworkController sharedInstance] stopTCPConnection];
        }
    }
    
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Language"]==nil)
    {
        NSLog(@"Absent");
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Localizable"
                                                         ofType:@"strings"
                                                    inDirectory:nil
                                                forLocalization:@"en"];
        
        // compiled .strings file becomes a "binary property list"
       NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
        
        [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"Language"];
        [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"ChoosedLang"];

    }
    else
    {
       
    }
}

-(void)logout
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
     NSDictionary *dictLang=[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    if (internetStatus == NotReachable)
    {
         [AlertView alert:[dictLang objectForKey:@"No internet connection"] message:[NSString stringWithFormat:@"%@ (%@%@%@%@i)",[dictLang objectForKey:@"Connect to the internet"],[[DiagnosticTool sharedManager] generateScreenCode:@"AppDelegateVC"],[[DiagnosticTool sharedManager] generateCommunicationcode],[[DiagnosticTool sharedManager]generateApiCode:@"AppDelegate"],[[DiagnosticTool sharedManager]generateErrorCode:@"Connect to the internet"]]];
    }
    else
    {
        NSString *str=[NSString stringWithFormat:@"%@",[[NSLogger sharedInstance] getdata]];
        NSData *file=[NSData dataWithContentsOfFile:str];
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            
            NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
            
            NSDictionary *twResponseHeader= [HttpHandler userlogout:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"] userScreen:@""];
            if (file.length>0)
            {
                [HttpHandler uploadlogfiles:file userScreen:@"AppDelegate"];
            }
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                if (![[[twResponseHeader objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
                {
                    [[NSUserDefaults standardUserDefaults]setObject:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] forKey:@"logoutname"];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"earbox"];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"firstTimeStore"];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"logedin"];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"earboxv2"];
                    //[[NSUserDefaults standardUserDefaults]removeObjectForKey:@"twmAppLogin"];
                    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"inboxpreviousTab"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"badgecount"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"notificationtype"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"privateMessageCountdef"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"storeAnnouncementCountdef"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"groupMessageCountdef"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"everyoneMessageCountdef"];
                    //Database delete
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"EarboxInbox"];
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"EarboxSaved"];
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"EarboxSent"];
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"AnnouncementInbox"];
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"AnnouncementSaved"];
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"AnnouncementSent"];
                    [[EarboxDataStore sharedStore]deleteAllEntities:@"Favorites"];
                    
                    // [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"InStore"];
                    
                    //Audio files deleted
                    [[AudioManager getsharedInstance]deleteallEarboxAudioPath];
                    
                    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
                    [[UIApplication sharedApplication]cancelAllLocalNotifications];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"messagecount"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"announcementcount"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"messagetotalcount"];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"announcementtotalcount"];
        // UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                    
                    [StoreListViewController getsharedInstance].deletetimer=nil;
                    
                    [[VPViewController getsharedInstance].deletetimer invalidate];
                    
                    [VPViewController getsharedInstance].deletetimer=nil;
                    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"loginstatus"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ForceLogOut" object:nil];
                    
                    bool state= [[StateMachineHandler sharedManager] isNativeStarted];
                    if (state)
                    {
                        [[StateMachineHandler sharedManager] killTheatroNative];
                    }
                    
                }
                else
                {
                    
                }
                
            }];
        }];
        
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Language"]==nil)
    {
        NSLog(@"Absent");
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Localizable"
                                                         ofType:@"strings"
                                                    inDirectory:nil
                                                forLocalization:@"en"];
        
        // compiled .strings file becomes a "binary property list"
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
        
        [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"Language"];
        [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"ChoosedLang"];
        
    }
    else
    {
        
    }
    
    
    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"EarboxViewController"])
        
    {
        
        [self requestUndeliveredMsgs];
        
    }
    
    else if([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"InboxViewController"])
        
    {
        
        [self requestUndeliveredMsgsinbox];
        
    }
    
    NSLog(@"Appdelegate applicationDidBecomeActive");
    
    
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus == NotReachable)
        
    {
        
    }
    
    else
        
    {
        
        NSDictionary *loginDetails =[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        if (loginDetails.count>0)
        {
            NSOperationQueue *myQueueSales = [[NSOperationQueue alloc] init];
            
            [myQueueSales addOperationWithBlock:^{
                
                NSDictionary *responsDic=[HttpHandler dynamicLogUpload:@"AppDelegateVC"];
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                    
                    if ([[responsDic allKeys] containsObject:@"message"])
                    {
                        
                        
                        if ([[responsDic objectForKey:@"message"] isEqualToString:@"Success"])
                            
                        {
                            // [[NSUserDefaults standardUserDefaults] setObject:[responsDic objectForKey:@"employeeProfile"] forKey:@"EmployeeTime"];
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"employeeNameUpdate" object:[responsDic objectForKey:@"employeeProfile"]];
                            
                            if ([[responsDic objectForKey:@"logUpload"]  isKindOfClass:[NSNull class]])
                                
                            {
                                
                                
                                
                            }
                            
                            else
                                
                            {
                                
                                if ([[responsDic objectForKey:@"logUpload"]intValue]==1)
                                    
                                {
                                    
                                    [self uploadlogfile];
                                    
                                }
                                if ([[responsDic objectForKey:@"forceLogOut"]intValue]==1)
                                {
                                    NSLog(@"Tcm Force LogOut enabled");
                                    [self logout];
                                    
                                }
                            }
                            
                        }
                        
                    }
                    
                }];
                
            }];
            
        }
    }
    sleep(2);
    [self checkNetworkStatus:nil];
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
}

-(void)uploadlogfile

{
    
    NSString *str=[NSString stringWithFormat:@"%@",[[NSLogger sharedInstance] getdata]];
    
    NSData *file=[NSData dataWithContentsOfFile:str];
    
    long cacheSize =([file length]/1000);
    
    //cacheSize = (cacheSize/1024);
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if(internetStatus == NotReachable)
        
    {
        
        //No internet
        
    }
    
    else if (internetStatus == ReachableViaWiFi)
        
    {
        
        //WiFi
        
    }
    
    else if (internetStatus == ReachableViaWWAN)
        
    {
        
        //3G
        
    }
    
    if (file.length!=0)
        
    {
        
        
        
        
        
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        
        [myQueue addOperationWithBlock:^{
            
            
            
            [HttpHandler uploadlogfiles:file userScreen:@"AppDelegate"];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                
                
                
                
            }];
            
        }];
        
        
        
        
        
    }
    
    else
        
    {
        
        if (cacheSize>9216)
            
        {
            
            NSLog(@"Log file reached more than 10MB");
            
            [[NSLogger sharedInstance]removefilePath];
            
            [[NSLogger sharedInstance]writeNSLogToFile];
            
        }
        
    }
    
    
    
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [UAAppIntegration application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [UAAppIntegration application:application didRegisterUserNotificationSettings:notificationSettings];
}



-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    //foreground notification
    
    NSLog(@"Appdelegate Received a notification while the app was already in the foreground: %@", userInfo);
    
    // Called when a push is received when the app is in the foreground
    // You can work with the notification object here
    
    // Be sure to call the completion handler with a UIBackgroundFetchResult
    notificationinfo=userInfo;
    [[NSUserDefaults standardUserDefaults]setObject:[notificationinfo objectForKey:@"callType"] forKey:@"notificationtype"];
    if (notificationinfo)
    {
        [[NSUserDefaults standardUserDefaults]setObject:[[notificationinfo objectForKey:@"aps"] objectForKey:@"badge"] forKey:@"badgecount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        NSLog(@"Appdelegate App badge count:%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]);
        if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"EarboxViewController"] || [NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"InboxViewController"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnotificationrecieved" object:nil];
        }
        else
        {
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            NSString *role=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"roleId"]];
            int roleid = 0;
            roleid = [role intValue];
            if (roleid>10)
            {
                [appDelegate requestUndeliveredMsgsinbox];
            }
            else
            {
                [appDelegate requestUndeliveredMsgs];
            }
            
            //[AGPushNoteView showWithNotificationMessage:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
            storeNameStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
            if([[[NSUserDefaults standardUserDefaults]objectForKey:@"NotificationFilter"] isEqualToString:@"YES"])
            {
                
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:nil
                                                               description:[[notificationinfo objectForKey:@"aps"] objectForKey:@"alert"]
                                                                      type:TWMessageBarMessageTypeError
                                                            statusBarStyle:UIStatusBarStyleLightContent
                                                                  callback:^{
                                                                      [self forgroundNotificationShow];
                                                                      
                                                                  }];
            }
            else
            {
                
                [self forgroundNotificationShow];
            }
            if ([[notificationinfo objectForKey:@"callType"] isEqualToString:@"4"] || [[notificationinfo objectForKey:@"callType"] isEqualToString:@"17"])
            {
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"pushnotificationstorecount" object:@"msgincrement"];
            }
            else
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"pushnotificationstorecount" object:@"anncincrement"];
            }
            NSDictionary *apsInfo = [notificationinfo objectForKey:@"aps"];
            alertMessage = [apsInfo objectForKey:@"alert"];
            if ([alertMessage containsString:@"/"])
            {
                storeName=[alertMessage componentsSeparatedByString:@"/"][1];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:storeName forKey:@"multistoreNameString"];
            
        }
        
        NSLog(@"Appdelegate App was already in the foreground");
        
    }
    
    [UAAppIntegration application:application didReceiveRemoteNotification:notificationinfo fetchCompletionHandler:completionHandler];
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wstrict-prototypes"
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())handler {
    //background notification
    
    NSLog(@"Appdelegate The application was launched or resumed from a notification: %@", userInfo);
    
    // Called when the app is launched/resumed by interacting with the notification
    // You can work with the notification object here
    
    // Be sure to call the completion handler with a UIBackgroundFetchResult
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"logedin"])
    {
        notificationinfo=userInfo;
        [[NSUserDefaults standardUserDefaults]setObject:[notificationinfo objectForKey:@"callType"] forKey:@"notificationtype"];
        if ( [[NSUserDefaults standardUserDefaults]objectForKey:@"loginstatus"])
        {
            if (notificationinfo) {
                [[NSUserDefaults standardUserDefaults]setObject:[[notificationinfo objectForKey:@"aps"] objectForKey:@"badge"] forKey:@"badgecount"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                storeNameStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
                NSDictionary *apsInfo = [notificationinfo objectForKey:@"aps"];
                
                alertMessage = [apsInfo objectForKey:@"alert"];
                if ([alertMessage containsString:@"/"])
                {
                    storeName=[alertMessage componentsSeparatedByString:@"/"][1];
                }
                
                NSString *role=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"roleId"]];
                int roleid = 0;
                roleid = [role intValue];
                if (roleid>10)
                {
                    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"InboxViewController"])
                    {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnotificationrecieved" object:nil];
                    }
                    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"pushForMultiStore"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
                    [myQueue addOperationWithBlock:^{
                        if( [[StateMachineHandler sharedManager]isNativeStarted])
                        {
                            [[StateMachineHandler sharedManager] killTheatroNative];
                        }
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        }];
                    }];
                }
                else
                {
                    if ([storeNameStr isEqualToString:storeName])
                    {
                        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                 bundle: nil];
                        [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
                        EarboxViewController *earboxVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"EarboxViewController"];
                        
                        NSString *chain=[alertMessage componentsSeparatedByString:@","][1];
                        NSString *chainName=[[chain componentsSeparatedByString:@"/"][0]substringFromIndex:1];
                        [[NSUserDefaults standardUserDefaults]setObject:storeName forKey:@"storeNameString"];
                        NSDictionary *twmAppLogindic=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
                        NSMutableDictionary *updatedic=[[NSMutableDictionary alloc]init];
                        [updatedic setObject:[twmAppLogindic objectForKey:@"companyId"] forKey:@"companyId"];
                        [updatedic setObject:[twmAppLogindic objectForKey:@"firstTimeLogin"] forKey:@"firstTimeLogin"];
                        [updatedic setObject:[twmAppLogindic objectForKey:@"tagOutName"] forKey:@"tagOutName"];
                        [updatedic setObject:[twmAppLogindic objectForKey:@"userId"] forKey:@"userId"];
                        [updatedic setObject:chainName forKey:@"chainName"];
                        [[NSUserDefaults standardUserDefaults]setObject:updatedic forKey:@"twmAppLogin"];
                        [NSThread detachNewThreadSelector:@selector(communicatorStart) toTarget:self withObject:nil];
                        
                        if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"EarboxViewController"])
                        {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnotificationrecieved" object:nil];
                        }
                        else
                        {
                            [[SlideNavigationController sharedInstance] pushViewController:earboxVC animated:YES];
                        }
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"pushForDiffStore"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:storeName forKey:@"StoreNameForPush"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
                        [myQueue addOperationWithBlock:^{
                            [[StateMachineHandler sharedManager] killTheatroNative];
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            }];
                        }];
                        
                        
                    }
                }
            }
        }
        [UAAppIntegration application:application handleActionWithIdentifier:identifier forRemoteNotification:notificationinfo completionHandler:handler];
    }
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wstrict-prototypes"
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo withResponseInfo:(NSDictionary *)responseInfo completionHandler:(void (^)())handler {
    [UAAppIntegration application:application handleActionWithIdentifier:identifier forRemoteNotification:userInfo withResponseInfo:responseInfo completionHandler:handler];
}


// UNUserNotificationCenterDelegate methods

- (void) userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    [UAAppIntegration userNotificationCenter:center willPresentNotification:notification withCompletionHandler:completionHandler];
}

- (void) userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    //response.notification.request.content.userInfo
    NSLog(@"Appdelegate The application was launched or resumed from a notification: %@",  response.notification.request.content.userInfo);
    
    // Called when the app is launched/resumed by interacting with the notification
    // You can work with the notification object here
    
    // Be sure to call the completion handler with a UIBackgroundFetchResult
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"logedin"])
    {
        notificationinfo= response.notification.request.content.userInfo;
        [[NSUserDefaults standardUserDefaults]setObject:[notificationinfo objectForKey:@"callType"] forKey:@"notificationtype"];
        if ( [[NSUserDefaults standardUserDefaults]objectForKey:@"loginstatus"])
        {
            if ( notificationinfo) {
                [[NSUserDefaults standardUserDefaults]setObject:[[ notificationinfo objectForKey:@"aps"] objectForKey:@"badge"] forKey:@"badgecount"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                storeNameStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
                NSDictionary *apsInfo = [ notificationinfo objectForKey:@"aps"];
                
                alertMessage = [apsInfo objectForKey:@"alert"];
                if ([alertMessage containsString:@"/"])
                {
                    storeName=[alertMessage componentsSeparatedByString:@"/"][1];
                }
                
                NSString *role=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"roleId"]];
                int roleid = 0;
                roleid = [role intValue];
                if (roleid>10)
                {
                    if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"InboxViewController"])
                    {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnotificationrecieved" object:nil];
                    }
                    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"pushForMultiStore"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"killcommunicator"])
                    {
                        
                        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
                        [myQueue addOperationWithBlock:^{
                            if( [[StateMachineHandler sharedManager]isNativeStarted])
                            {
                                [[StateMachineHandler sharedManager] killTheatroNative];
                            }
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            }];
                        }];
                    }
                }
                else
                {
                    if ([storeNameStr isEqualToString:storeName])
                    {
                        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                 bundle: nil];
                        [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
                        EarboxViewController *earboxVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"EarboxViewController"];
                        
                        NSString *chain=[alertMessage componentsSeparatedByString:@","][1];
                        NSString *chainName=[[chain componentsSeparatedByString:@"/"][0]substringFromIndex:1];
                        [[NSUserDefaults standardUserDefaults]setObject:storeName forKey:@"storeNameString"];
                        NSDictionary *twmAppLogindic=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
                        NSMutableDictionary *updatedic=[[NSMutableDictionary alloc]init];
                        [updatedic setObject:[twmAppLogindic objectForKey:@"companyId"] forKey:@"companyId"];
                        [updatedic setObject:[twmAppLogindic objectForKey:@"firstTimeLogin"] forKey:@"firstTimeLogin"];
                        [updatedic setObject:[twmAppLogindic objectForKey:@"tagOutName"] forKey:@"tagOutName"];
                        [updatedic setObject:[twmAppLogindic objectForKey:@"userId"] forKey:@"userId"];
                        [updatedic setObject:chainName forKey:@"chainName"];
                        [[NSUserDefaults standardUserDefaults]setObject:updatedic forKey:@"twmAppLogin"];
                        [NSThread detachNewThreadSelector:@selector(communicatorStart) toTarget:self withObject:nil];
                        
                        if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"EarboxViewController"])
                        {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnotificationrecieved" object:nil];
                        }
                        else
                        {
                            [[SlideNavigationController sharedInstance] pushViewController:earboxVC animated:YES];
                        }
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"pushForDiffStore"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:storeName forKey:@"StoreNameForPush"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
                        [myQueue addOperationWithBlock:^{
                            [[StateMachineHandler sharedManager] killTheatroNative];
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            }];
                        }];
                        
                        
                    }
                }
            }
        }
        
        [UAAppIntegration userNotificationCenter:center didReceiveNotificationResponse:response withCompletionHandler:completionHandler];
    }
}

-(void)communicatorStart
{
    // Communicator starts here
    [[StateMachineHandler sharedManager] communicator:NO];
}




@end
