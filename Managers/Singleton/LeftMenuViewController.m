//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "ViewController.h"
#import "Constant.h"
#import "HttpHandler.h"

@implementation LeftMenuViewController

#pragma mark - UIViewController Methods -


static  LeftMenuViewController *sharedMgr = nil;


+(LeftMenuViewController *)getsharedInstance {
    if (sharedMgr == nil)
    {
        sharedMgr = [[LeftMenuViewController alloc] init];
    }
    return sharedMgr;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
	self.slideOutAnimationEnabled = YES;
	
	return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lefttablereload) name:@"lefttablereload" object:nil];
    logoutbutton.backgroundColor=kNavBackgroundColor;
    self.tableView.scrollEnabled=NO;
}


//Camera code

-(void)profileimageTapped
{
    UIActionSheet *sheet=[[UIActionSheet alloc]initWithTitle:@"Choose image from" delegate:self cancelButtonTitle:@"Ok" destructiveButtonTitle:nil otherButtonTitles:@"Camera",
                          @"Library", nil];
    [sheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
                case 0:
                    [self useCamera];
                    break;
                case 1:
                    [self useCameraRoll];
                    break;
            
    }
}

- (void) useCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
        
        newMedia = YES;
    }
}


- (void) useCameraRoll
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.allowsEditing = NO;
         [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = NO;
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
        image = [info
                 objectForKey:UIImagePickerControllerOriginalImage];
    [profileimage setImage:image forState:UIControlStateNormal];
        if (newMedia)
            UIImageWriteToSavedPhotosAlbum(image,
                                           self,
                                           @selector(image:finishedSavingWithError:contextInfo:),
                                           nil);
    
}
-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"\
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(void)lefttablereload
{
    [self.tableView reloadData];
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
	return 3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 130)];
	view.backgroundColor = kNavBackgroundColor;
    profileimage=[UIButton buttonWithType:UIButtonTypeCustom];
    profileimage.frame=CGRectMake((self.tableView.frame.size.width-155)/2, 25,81, 80);
    profileimage.layer.cornerRadius = profileimage.frame.size.width / 2;
    profileimage.clipsToBounds = YES;
   // [profileimage addTarget:self action:@selector(profileimageTapped) forControlEvents:UIControlEventTouchUpInside];
    [profileimage setImage:[UIImage imageNamed:@"user.png"] forState:UIControlStateNormal];
    [view addSubview:profileimage];
    UILabel *profilename=[[UILabel alloc]initWithFrame:CGRectMake((self.tableView.frame.size.width-260)/2, 110,190, 20)];
    profilename.textAlignment=NSTextAlignmentCenter;
    profilename.text=[[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"]capitalizedString];
    profilename.textColor=[UIColor whiteColor];
    [view addSubview:profilename];
	return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 150;
}

 -(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"cell";
   
    UITableViewCell *cell;
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:cellIdentifier];
        logoutbutton=[UIButton buttonWithType:UIButtonTypeCustom];
        logoutbutton.frame=CGRectMake((self.tableView.frame.size.width-160)/2, 10,100, 40);
        logoutbutton.layer.cornerRadius=8;
        logoutbutton.backgroundColor=kNavBackgroundColor;
        [logoutbutton addTarget:self action:@selector(logOutbuttonAction) forControlEvents:UIControlEventTouchUpInside];
        [logoutbutton setTitle:@"Log Out" forState:UIControlStateNormal];
        
    }
    cell.backgroundColor=[UIColor lightGrayColor];
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
	switch (indexPath.row)
	{
		case 0:
			cell.textLabel.text = @"Home";
			break;
			
		case 1:
            
			cell.textLabel.text = [NSString stringWithFormat:@"Version %@",version];
			break;
            
        case 2:
            [cell.contentView addSubview:logoutbutton];
            break;
	}
	
	return cell;
}

-(void)logOutbuttonAction
{
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
       NSDictionary *loginDetails=[[NSUserDefaults standardUserDefaults]objectForKey:@"logincredentials"];
        
        NSDictionary *twResponseHeader= [HttpHandler userlogout:[loginDetails objectForKey:@"twUserName"] twPassword:[loginDetails objectForKey:@"twPassword"] twTxId:[loginDetails objectForKey:@"twTxId"] twMoment:[loginDetails objectForKey:@"twMoment"]];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            if (![[[twResponseHeader objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"] isKindOfClass:[NSNull class]])
            {
                NSLog(@"twsuccessMessage:%@",[[twResponseHeader objectForKey:@"twResponseHeader"] objectForKey:@"twsuccessMessage"]);
                [[NSUserDefaults standardUserDefaults]setObject:[[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"] objectForKey:@"tagOutName"] forKey:@"logoutname"];
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                         bundle: nil];
                ViewController *loginVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ViewController"];
                [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:loginVC
                                                                         withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                                 andCompletion:nil];
            }
            else
            {
                NSString *twerrorMessage=[twResponseHeader objectForKey:@"twerrorMessage"];
                NSLog(@"twsError:%@",twerrorMessage);
            }
            
        }];
    }];

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    logoutbutton.backgroundColor=kNavBackgroundColor;
	
	switch (indexPath.row)
	{
		case 0:
             [[NSNotificationCenter defaultCenter] postNotificationName:@"leftMenuSelectedNotification" object:@"closed"];
            [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
            
			break;
	}
	
}

@end
