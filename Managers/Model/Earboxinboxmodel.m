//
//  Earboxinboxmodel.m
//  Managers
//
//  Created by sandeepchalla on 6/13/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "Earboxinboxmodel.h"
#import "EarboxDataStore.h"
#import "AudioManager.h"

@implementation Earboxinboxmodel

+ (Earboxinboxmodel *)sharedModel
{
    static Earboxinboxmodel *store;
    if (store == nil)
    {
        store = [[Earboxinboxmodel alloc] init];
    }
    return store;
}

-(NSArray *)exist:(EarboxDataStore *)store entityname:(NSString *)entityname callid:(NSString *)callid callType:(NSString *)callType storeid:(int)storeid
{
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityname inManagedObjectContext:store.context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    //VEB
    if (storeid==0)
    {
        [request setPredicate:[NSPredicate predicateWithFormat:@"(callId == %@) AND (callType == %@)",callid,callType]];
    }
    else
    {
        [request setPredicate:[NSPredicate predicateWithFormat:@"(callId == %@) AND (callType == %@) AND (storeid == %d)",callid,callType,storeid]];
    }
    NSError *error = nil;
    NSArray *results = [store.context executeFetchRequest:request error:&error];
    
    return results;
}

-(void)messageinboxSave:(NSArray *)messageinbox
{
    EarboxDataStore *store = [EarboxDataStore sharedStore];
    
    for (int i=0; i<messageinbox.count; i++)
    {
        NSArray *count;
        if ([[messageinbox objectAtIndex:i] objectForKey:@"storeId"])
        {
            count=[self exist:store entityname:@"EarboxInbox" callid:[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"] callType:[[messageinbox objectAtIndex:i] objectForKey:@"callType"] storeid:[[[messageinbox objectAtIndex:i] objectForKey:@"storeId"] intValue]];
        }
        else
        {
            if ([[[messageinbox objectAtIndex:i]allKeys] containsObject:@"hq"])
            {
                count=[self exist:store entityname:@"EarboxInbox" callid:[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"] callType:[[messageinbox objectAtIndex:i] objectForKey:@"callType"] storeid:0];
            }
            else
            {
            count=[self exist:store entityname:@"EarboxInbox" callid:[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"] callType:[[messageinbox objectAtIndex:i] objectForKey:@"callType"] storeid:[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]];
            }
        }
        if (count.count== 1)
        {
            NSLog(@"Earboxinboxmodel-Message inbox duplicate callTXId is:%@",[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"]);
        }
        else
        {
            EarboxInbox *inbox = [store createEarboxInbox];
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"storeId"] intValue]!=0)
            {
                inbox.storeid=[[[messageinbox objectAtIndex:i] objectForKey:@"storeId"] intValue];
            }
            else
            {
                if ([[[messageinbox objectAtIndex:i]allKeys] containsObject:@"hq"])
                {
                    inbox.storeid=0;
                }
                else
                {
                inbox.storeid=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue];
                }
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"maCallType"] intValue]!=0)
            {
                inbox.maCallType=[[[messageinbox objectAtIndex:i] objectForKey:@"maCallType"] intValue];
            }
            
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"] isKindOfClass:[NSNull class]])
            {
                inbox.callId=0;
            }
            else
            {
                inbox.callId=[NSString stringWithFormat:@"%@",[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"]]; //[[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"]longValue];
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"callType"] isKindOfClass:[NSNull class]])
            {
                inbox.callType=0;
            }
            else
            {
                inbox.callType=[[[messageinbox objectAtIndex:i] objectForKey:@"callType"]intValue];
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"callStartTime"] isKindOfClass:[NSNull class]])
            {
                inbox.callStartTime=0;
            }
            else
            {
                inbox.callStartTime=[[[messageinbox objectAtIndex:i] objectForKey:@"callStartTime"]longValue];
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"details"] isKindOfClass:[NSNull class]])
            {
                inbox.details=@"";
            }
            else
            {
                inbox.details=[[messageinbox objectAtIndex:i] objectForKey:@"details"];
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"downloadURL"] isKindOfClass:[NSNull class]])
            {
                inbox.downloadURL=@"";
            }
            else
            {
                if ([[[messageinbox objectAtIndex:i] objectForKey:@"storeId"] intValue]!=0)
                {
                    [[AudioManager getsharedInstance]createEarboxAudioPath:[[messageinbox objectAtIndex:i] objectForKey:@"downloadURL"] callId:[NSString stringWithFormat:@"%@%d%d",[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"],[[[messageinbox objectAtIndex:i] objectForKey:@"callType"]intValue],[[[messageinbox objectAtIndex:i] objectForKey:@"storeId"] intValue]] earboxName:@"EarboxInbox" callTxid:[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"]];
                }
                else
                {
                    if ([[[messageinbox objectAtIndex:i]allKeys] containsObject:@"hq"])
                    {
                        [[AudioManager getsharedInstance]createEarboxAudioPath:[[messageinbox objectAtIndex:i] objectForKey:@"downloadURL"] callId:[NSString stringWithFormat:@"%@%d%d",[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"],[[[messageinbox objectAtIndex:i] objectForKey:@"callType"]intValue],0] earboxName:@"EarboxInbox" callTxid:[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"]];
                    }
                    else
                    {
                    [[AudioManager getsharedInstance]createEarboxAudioPath:[[messageinbox objectAtIndex:i] objectForKey:@"downloadURL"] callId:[NSString stringWithFormat:@"%@%d%d",[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"],[[[messageinbox objectAtIndex:i] objectForKey:@"callType"]intValue],[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]] earboxName:@"EarboxInbox" callTxid:[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"]];
                    }
                    
                }
                inbox.downloadURL=[[messageinbox objectAtIndex:i] objectForKey:@"downloadURL"];
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"duration"] isKindOfClass:[NSNull class]])
            {
                inbox.duration=0;
            }
            else
            {
                inbox.duration=[[[messageinbox objectAtIndex:i] objectForKey:@"duration"] floatValue];
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"from"] isKindOfClass:[NSNull class]])
            {
                inbox.from=@"noname";
            }
            else
            {
                inbox.from=[[messageinbox objectAtIndex:i] objectForKey:@"from"];
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"to"] isKindOfClass:[NSNull class]])
            {
                inbox.to=@"";
            }
            else
            {
                inbox.to=[[messageinbox objectAtIndex:i] objectForKey:@"to"];
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"info"] isKindOfClass:[NSNull class]])
            {
                inbox.info=@"";
            }
            else
            {
                inbox.info=[[messageinbox objectAtIndex:i] objectForKey:@"info"];
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"priority"] isKindOfClass:[NSNull class]])
            {
                inbox.priority=@"";
            }
            else
            {
                inbox.priority=[[messageinbox objectAtIndex:i] objectForKey:@"priority"];
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"status"] isKindOfClass:[NSNull class]])
            {
                inbox.status=@"";
            }
            else
            {
                inbox.status=[[messageinbox objectAtIndex:i] objectForKey:@"status"];
            }
            
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"replyStatus"] isKindOfClass:[NSNull class]])
            {
                inbox.reply=@"";
            }
            else
            {
                inbox.reply=[NSString stringWithFormat:@"%@",[[messageinbox objectAtIndex:i] objectForKey:@"replyStatus"]];
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"storeName"] isKindOfClass:[NSNull class]])
            {
                inbox.storeName=@"";
            }
            else
            {
                if ([[messageinbox objectAtIndex:i] objectForKey:@"storeId"])
                {
                    inbox.storeName=[NSString stringWithFormat:@"%@",[[messageinbox objectAtIndex:i] objectForKey:@"storeName"]];
                }
                else
                {
                    inbox.storeName= [[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
                }
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"groupName"] isKindOfClass:[NSNull class]])
            {
                inbox.groupName=@"";
            }
            else
            {
                inbox.groupName=[NSString stringWithFormat:@"%@",[[messageinbox objectAtIndex:i] objectForKey:@"groupName"]];
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"ttl"] isKindOfClass:[NSNull class]])
            {
                inbox.ttl=0;
            }
            else
            {
                NSTimeInterval ttl=[[[messageinbox objectAtIndex:i] objectForKey:@"ttl"]doubleValue];
                NSTimeInterval callStartTime=[[[messageinbox objectAtIndex:i] objectForKey:@"callStartTime"]doubleValue];
                double ttltime=callStartTime+ttl;
                inbox.ttl=ttltime;
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"pushTxId"] isKindOfClass:[NSNull class]]||[[[messageinbox objectAtIndex:i] objectForKey:@"pushTxnId"] isKindOfClass:[NSNull class]])
            {
                inbox.pushTxId=@"";
            }
            else
            {
                if ([[[messageinbox objectAtIndex:i] allKeys] containsObject:@"pushTxnId"])
                {
                    inbox.pushTxId=[[messageinbox objectAtIndex:i] objectForKey:@"pushTxnId"];
                }
                else
                {
                    inbox.pushTxId=[[messageinbox objectAtIndex:i] objectForKey:@"pushTxId"];
                }
                
            }
            
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"mode"] isKindOfClass:[NSNull class]])
            {
                inbox.mode=@"";
            }
            else
            {
                inbox.mode=[NSString stringWithFormat:@"%@",[[messageinbox objectAtIndex:i] objectForKey:@"mode"]];
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"audience"] isKindOfClass:[NSNull class]])
            {
                inbox.groupNames=@"";
                inbox.storeNames=@"";
                inbox.storeIds=@"";
                inbox.groupIds=@"";
                inbox.nodeId=@"";
                inbox.nodeName=@"";
                inbox.levelName=@"";
            }
            else
            {
                if ([[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] isKindOfClass:[NSNull class]] || [[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] count]==0)
                {
                    inbox.groupNames=@"";
                }
                else
                {
                    NSString *groupNames = [[[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] valueForKey:@"groupName"] componentsJoinedByString:@","];
                    inbox.groupNames=groupNames;
                    NSString *groupIds = [[[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] valueForKey:@"groupId"] componentsJoinedByString:@","];
                    inbox.groupIds=groupIds;
                }
                if ([[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] isKindOfClass:[NSNull class]] || [[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] count]==0)
                {
                    inbox.storeNames=@"";
                }
                else
                {
                    NSString *storeNames = [[[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] valueForKey:@"storeName"] componentsJoinedByString:@","];
                    inbox.storeNames=storeNames;
                    NSString *storeIds = [[[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] valueForKey:@"storeId"] componentsJoinedByString:@","];
                    inbox.storeIds=storeIds;
                }
                if ([[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] isKindOfClass:[NSNull class]] || [[[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"] count]==0)
                {
                    inbox.nodeName=@"";
                    inbox.levelName=@"";
                }
                else
                {
                    NSString *nodeName = [[[[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"]valueForKey:@"nodeName"] componentsJoinedByString:@","];
                    inbox.nodeName=nodeName;
                    NSString *nodeId = [[[[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"]valueForKey:@"nodeId"] componentsJoinedByString:@","];
                    inbox.nodeId=nodeId;
                    if ([[[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"levelName"]isKindOfClass:[NSNull class]])
                    {
                        inbox.levelName=@"";
                    }
                    else
                    {
                    inbox.levelName= [[[[messageinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"levelName"];
                    }
                }
            }
            if ([[[messageinbox objectAtIndex:i] objectForKey:@"hq"] isKindOfClass:[NSNull class]])
            {
                inbox.managerAppId=@"";
                inbox.tagOutName=@"";
            }
            else
            {
                inbox.managerAppId=[[[messageinbox objectAtIndex:i] objectForKey:@"hq"] objectForKey:@"managerAppId"];
                inbox.tagOutName=[[[messageinbox objectAtIndex:i] objectForKey:@"hq"] objectForKey:@"tagOutName"];
            }
        }
    }
    NSError *error;
    [store save:error];
    
    if (error != nil) {
        [NSException raise:@"Error saving data to context"
                    format:@"Error: %@", error.localizedDescription];
    }
}

-(void)messagesavedSave:(NSArray *)messagesaved
{
    EarboxDataStore *store = [EarboxDataStore sharedStore];
    
    for (int i=0; i<messagesaved.count; i++)
    {
        NSArray *count;
        if ([[messagesaved objectAtIndex:i] objectForKey:@"storeId"])
        {
            count=[self exist:store entityname:@"EarboxSaved" callid:[[messagesaved objectAtIndex:i] objectForKey:@"callTxId"] callType:[[messagesaved objectAtIndex:i] objectForKey:@"callType"] storeid:[[[messagesaved objectAtIndex:i] objectForKey:@"storeId"] intValue]];
        }
        else
        {
            if ([[[messagesaved objectAtIndex:i]allKeys] containsObject:@"hq"])
            {
                count=[self exist:store entityname:@"EarboxSaved" callid:[[messagesaved objectAtIndex:i] objectForKey:@"callTxId"] callType:[[messagesaved objectAtIndex:i] objectForKey:@"callType"] storeid:0];
            }
            else
            {
            count=[self exist:store entityname:@"EarboxSaved" callid:[[messagesaved objectAtIndex:i] objectForKey:@"callTxId"] callType:[[messagesaved objectAtIndex:i] objectForKey:@"callType"] storeid:[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]];
            }
        }
        if (count.count== 1)
        {
            NSLog(@"Earboxinboxmodel-Message saved duplicate callTXId is:%@",[[messagesaved objectAtIndex:i] objectForKey:@"callTxId"]);
        }
        else
        {
            EarboxSaved *inbox = [store createEarboxSaved];
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"storeId"] intValue]!=0)
            {
                inbox.storeid=[[[messagesaved objectAtIndex:i] objectForKey:@"storeId"] intValue];
            }
            else
            {
                if ([[[messagesaved objectAtIndex:i]allKeys] containsObject:@"hq"])
                {
                    inbox.storeid=0;
                }
                else
                {
                inbox.storeid=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue];
                }
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"maCallType"] intValue]!=0)
            {
                inbox.maCallType=[[[messagesaved objectAtIndex:i] objectForKey:@"maCallType"] intValue];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"callTxId"] isKindOfClass:[NSNull class]])
            {
                inbox.callId=0;
            }
            else
            {
                inbox.callId=[NSString stringWithFormat:@"%@",[[messagesaved objectAtIndex:i] objectForKey:@"callTxId"]];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"callType"] isKindOfClass:[NSNull class]])
            {
                inbox.callType=0;
            }
            else
            {
                inbox.callType=[[[messagesaved objectAtIndex:i] objectForKey:@"callType"]intValue];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"callStartTime"] isKindOfClass:[NSNull class]])
            {
                inbox.callStartTime=0;
            }
            else
            {
                inbox.callStartTime=[[[messagesaved objectAtIndex:i] objectForKey:@"callStartTime"]longValue];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"details"] isKindOfClass:[NSNull class]])
            {
                inbox.details=@"";
            }
            else
            {
                inbox.details=[[messagesaved objectAtIndex:i] objectForKey:@"details"];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"downloadURL"] isKindOfClass:[NSNull class]])
            {
                inbox.downloadURL=@"";
            }
            else
            {
                if ([[[messagesaved objectAtIndex:i] objectForKey:@"storeId"] intValue]!=0)
                {
                    [[AudioManager getsharedInstance]createEarboxAudioPath:[[messagesaved objectAtIndex:i] objectForKey:@"downloadURL"] callId:[NSString stringWithFormat:@"%@%d%d",[[messagesaved objectAtIndex:i] objectForKey:@"callTxId"],[[[messagesaved objectAtIndex:i] objectForKey:@"callType"]intValue],[[[messagesaved objectAtIndex:i] objectForKey:@"storeId"] intValue]] earboxName:@"EarboxSaved" callTxid:[[messagesaved objectAtIndex:i] objectForKey:@"callTxId"]];
                }
                else
                {
                    if ([[[messagesaved objectAtIndex:i]allKeys] containsObject:@"hq"])
                    {
                        [[AudioManager getsharedInstance]createEarboxAudioPath:[[messagesaved objectAtIndex:i] objectForKey:@"downloadURL"] callId:[NSString stringWithFormat:@"%@%d%d",[[messagesaved objectAtIndex:i] objectForKey:@"callTxId"],[[[messagesaved objectAtIndex:i] objectForKey:@"callType"]intValue],0] earboxName:@"EarboxSaved" callTxid:[[messagesaved objectAtIndex:i] objectForKey:@"callTxId"]];
                    }
                    else
                    {
                    [[AudioManager getsharedInstance]createEarboxAudioPath:[[messagesaved objectAtIndex:i] objectForKey:@"downloadURL"] callId:[NSString stringWithFormat:@"%@%d%d",[[messagesaved objectAtIndex:i] objectForKey:@"callTxId"],[[[messagesaved objectAtIndex:i] objectForKey:@"callType"]intValue],[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]] earboxName:@"EarboxSaved" callTxid:[[messagesaved objectAtIndex:i] objectForKey:@"callTxId"]];
                    }
                    
                }
                inbox.downloadURL=[[messagesaved objectAtIndex:i] objectForKey:@"downloadURL"];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"duration"] isKindOfClass:[NSNull class]])
            {
                inbox.duration=0;
            }
            else
            {
                inbox.duration=[[[messagesaved objectAtIndex:i] objectForKey:@"duration"] floatValue];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"from"] isKindOfClass:[NSNull class]])
            {
                inbox.from=@"noname";
            }
            else
            {
                inbox.from=[[messagesaved objectAtIndex:i] objectForKey:@"from"];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"to"] isKindOfClass:[NSNull class]])
            {
                inbox.to=@"";
            }
            else
            {
                inbox.to=[[messagesaved objectAtIndex:i] objectForKey:@"to"];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"info"] isKindOfClass:[NSNull class]])
            {
                inbox.info=@"";
            }
            else
            {
                inbox.info=[[messagesaved objectAtIndex:i] objectForKey:@"info"];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"priority"] isKindOfClass:[NSNull class]])
            {
                inbox.priority=@"";
            }
            else
            {
                inbox.priority=[[messagesaved objectAtIndex:i] objectForKey:@"priority"];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"status"] isKindOfClass:[NSNull class]])
            {
                inbox.status=@"";
            }
            else
            {
                inbox.status=[[messagesaved objectAtIndex:i] objectForKey:@"status"];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"replyStatus"] isKindOfClass:[NSNull class]])
            {
                inbox.reply=@"";
            }
            else
            {
                inbox.reply=[NSString stringWithFormat:@"%@",[[messagesaved objectAtIndex:i] objectForKey:@"replyStatus"]];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"storeName"] isKindOfClass:[NSNull class]])
            {
                inbox.storeName=@"";
            }
            else
            {
                if ([[messagesaved objectAtIndex:i] objectForKey:@"storeId"])
                {
                    inbox.storeName=[NSString stringWithFormat:@"%@",[[messagesaved objectAtIndex:i] objectForKey:@"storeName"]];
                }
                else
                {
                    inbox.storeName= [[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
                }
                
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"groupName"] isKindOfClass:[NSNull class]])
            {
                inbox.groupName=@"";
            }
            else
            {
                inbox.groupName=[NSString stringWithFormat:@"%@",[[messagesaved objectAtIndex:i] objectForKey:@"groupName"]];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"ttl"] isKindOfClass:[NSNull class]])
            {
                inbox.ttl=0;
            }
            else
            {
                NSTimeInterval ttl=[[[messagesaved objectAtIndex:i] objectForKey:@"ttl"]doubleValue];
                NSTimeInterval callStartTime=[[[messagesaved objectAtIndex:i] objectForKey:@"callStartTime"]doubleValue];
                double ttltime=callStartTime+ttl;
                inbox.ttl=ttltime;
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"pushTxId"] isKindOfClass:[NSNull class]]||[[[messagesaved objectAtIndex:i] objectForKey:@"pushTxnId"] isKindOfClass:[NSNull class]])
            {
                inbox.pushTxId=@"";
            }
            else
            {
                if ([[[messagesaved objectAtIndex:i] allKeys] containsObject:@"pushTxnId"])
                {
                    inbox.pushTxId=[[messagesaved objectAtIndex:i] objectForKey:@"pushTxnId"];
                }
                else
                {
                    inbox.pushTxId=[[messagesaved objectAtIndex:i] objectForKey:@"pushTxId"];
                }
                
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"mode"] isKindOfClass:[NSNull class]])
            {
                inbox.mode=@"";
            }
            else
            {
                inbox.mode=[NSString stringWithFormat:@"%@",[[messagesaved objectAtIndex:i] objectForKey:@"mode"]];
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"audience"] isKindOfClass:[NSNull class]])
            {
                inbox.groupNames=@"";
                inbox.storeNames=@"";
                inbox.storeIds=@"";
                inbox.groupIds=@"";
                inbox.nodeId=@"";
                inbox.nodeName=@"";
                inbox.levelName=@"";
            }
            else
            {
                if ([[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] isKindOfClass:[NSNull class]] || [[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] count]==0)
                {
                    inbox.groupNames=@"";
                }
                else
                {
                    NSString *groupNames = [[[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] valueForKey:@"groupName"] componentsJoinedByString:@","];
                    inbox.groupNames=groupNames;
                    NSString *groupIds = [[[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] valueForKey:@"groupId"] componentsJoinedByString:@","];
                    inbox.groupIds=groupIds;
                }
                if ([[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] isKindOfClass:[NSNull class]] || [[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] count]==0)
                {
                    inbox.storeNames=@"";
                }
                else
                {
                    NSString *storeNames = [[[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] valueForKey:@"storeName"] componentsJoinedByString:@","];
                    inbox.storeNames=storeNames;
                    NSString *storeIds = [[[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] valueForKey:@"storeId"] componentsJoinedByString:@","];
                    inbox.storeIds=storeIds;
                }
                if ([[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] isKindOfClass:[NSNull class]] || [[[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"] count]==0)
                {
                    inbox.nodeName=@"";
                    inbox.levelName=@"";
                }
                else
                {
                    NSString *nodeName = [[[[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"]valueForKey:@"nodeName"] componentsJoinedByString:@","];
                    inbox.nodeName=nodeName;
                    NSString *nodeId = [[[[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"]valueForKey:@"nodeId"] componentsJoinedByString:@","];
                    inbox.nodeId=nodeId;
                    if ([[[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"levelName"]isKindOfClass:[NSNull class]])
                    {
                        inbox.levelName=@"";
                    }
                    else
                    {
                    inbox.levelName= [[[[messagesaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"levelName"];
                    }
                }
            }
            if ([[[messagesaved objectAtIndex:i] objectForKey:@"hq"] isKindOfClass:[NSNull class]])
            {
                inbox.managerAppId=@"";
                inbox.tagOutName=@"";
            }
            else
            {
                inbox.managerAppId=[[[messagesaved objectAtIndex:i] objectForKey:@"hq"] objectForKey:@"managerAppId"];
                inbox.tagOutName=[[[messagesaved objectAtIndex:i] objectForKey:@"hq"] objectForKey:@"tagOutName"];
            }
        }
    }
    NSError *error;
    [store save:error];
    
    if (error != nil) {
        [NSException raise:@"Error saving data to context"
                    format:@"Error: %@", error.localizedDescription];
    }
}

-(void)messagesentSave:(NSArray *)messagesent
{
    EarboxDataStore *store = [EarboxDataStore sharedStore];
    
    for (int i=0; i<messagesent.count; i++)
    {
        NSArray *count;
        if ([[messagesent objectAtIndex:i] objectForKey:@"storeId"])
        {
            count=[self exist:store entityname:@"EarboxSent" callid:[[messagesent objectAtIndex:i] objectForKey:@"callTxId"] callType:[[messagesent objectAtIndex:i] objectForKey:@"callType"] storeid:[[[messagesent objectAtIndex:i] objectForKey:@"storeId"] intValue]];
        }
        else
        {
            if ([[[messagesent objectAtIndex:i]allKeys] containsObject:@"hq"])
            {
                count=[self exist:store entityname:@"EarboxSent" callid:[[messagesent objectAtIndex:i] objectForKey:@"callTxId"] callType:[[messagesent objectAtIndex:i] objectForKey:@"callType"] storeid:0];
            }
            else
            {
            count=[self exist:store entityname:@"EarboxSent" callid:[[messagesent objectAtIndex:i] objectForKey:@"callTxId"] callType:[[messagesent objectAtIndex:i] objectForKey:@"callType"] storeid:[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]];
            }
        }
        
        if (count.count>=1)
        {
            NSLog(@"Earboxinboxmodel-Message sent duplicate callTXId is:%@",[[messagesent objectAtIndex:i] objectForKey:@"callTxId"]);
        }
        else
        {
            EarboxSent *inbox = [store createEarboxSent];
            if ([[[messagesent objectAtIndex:i] objectForKey:@"storeId"] intValue]!=0)
            {
                inbox.storeid=[[[messagesent objectAtIndex:i] objectForKey:@"storeId"] intValue];
            }
            else
            {
                if ([[[messagesent objectAtIndex:i]allKeys] containsObject:@"hq"])
                {
                    inbox.storeid=0;
                }
                else
                {
                inbox.storeid=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue];
                }
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"maCallType"] intValue]!=0)
            {
                inbox.maCallType=[[[messagesent objectAtIndex:i] objectForKey:@"maCallType"] intValue];
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"callTxId"] isKindOfClass:[NSNull class]])
            {
                inbox.callId=0;
            }
            else
            {
                
                inbox.callId=[NSString stringWithFormat:@"%@",[[messagesent objectAtIndex:i] objectForKey:@"callTxId"]];
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"callType"] isKindOfClass:[NSNull class]])
            {
                inbox.callType=0;
            }
            else
            {
                inbox.callType=[[[messagesent objectAtIndex:i] objectForKey:@"callType"]intValue];
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"callStartTime"] isKindOfClass:[NSNull class]])
            {
                inbox.callStartTime=0;
            }
            else
            {
                inbox.callStartTime=[[[messagesent objectAtIndex:i] objectForKey:@"callStartTime"]longValue];
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"details"] isKindOfClass:[NSNull class]])
            {
                inbox.details=@"";
            }
            else
            {
                inbox.details=[[messagesent objectAtIndex:i] objectForKey:@"details"];
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"downloadURL"] isKindOfClass:[NSNull class]])
            {
                inbox.downloadURL=@"";
            }
            else
            {
                [[AudioManager getsharedInstance]createEarboxAudioPath:[[messagesent objectAtIndex:i] objectForKey:@"downloadURL"] callId:[[messagesent objectAtIndex:i] objectForKey:@"callTxId"] earboxName:@"EarboxSent" callTxid:[[messagesent objectAtIndex:i] objectForKey:@"callTxId"]];
                inbox.downloadURL=[[messagesent objectAtIndex:i] objectForKey:@"downloadURL"];
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"duration"] isKindOfClass:[NSNull class]])
            {
                inbox.duration=0;
            }
            else
            {
                inbox.duration=[[[messagesent objectAtIndex:i] objectForKey:@"duration"] floatValue];
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"from"] isKindOfClass:[NSNull class]])
            {
                inbox.from=@"noname";
            }
            else
            {
                inbox.from=[[messagesent objectAtIndex:i] objectForKey:@"from"];
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"info"] isKindOfClass:[NSNull class]])
            {
                inbox.info=@"";
            }
            else
            {
                inbox.info=[[messagesent objectAtIndex:i] objectForKey:@"info"];
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"priority"] isKindOfClass:[NSNull class]])
            {
                inbox.priority=@"";
            }
            else
            {
                inbox.priority=[[messagesent objectAtIndex:i] objectForKey:@"priority"];
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"status"] isKindOfClass:[NSNull class]])
            {
                inbox.status=@"";
            }
            else
            {
                inbox.status=[[messagesent objectAtIndex:i] objectForKey:@"status"];
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"replyStatus"] isKindOfClass:[NSNull class]])
            {
                inbox.reply=@"";
            }
            else
            {
                inbox.reply=[NSString stringWithFormat:@"%@",[[messagesent objectAtIndex:i] objectForKey:@"replyStatus"]];
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"storeName"] isKindOfClass:[NSNull class]])
            {
                inbox.storeName=@"";
            }
            else
            {
                if ([[messagesent objectAtIndex:i] objectForKey:@"storeId"])
                {
                    inbox.storeName=[NSString stringWithFormat:@"%@",[[messagesent objectAtIndex:i] objectForKey:@"storeName"]];
                }
                else
                {
                    inbox.storeName= [[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
                }
                
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"groupName"] isKindOfClass:[NSNull class]])
            {
                
                inbox.groupName=@"";
                
            }
            else
            {
                if ([[[messagesent objectAtIndex:i] objectForKey:@"groupName"] isKindOfClass:[NSArray class]])
                {
                    NSString * result = [[[messagesent objectAtIndex:i] objectForKey:@"groupName"] componentsJoinedByString:@""];
                   
                         inbox.groupName=result;
                }
                else
                {
                inbox.groupName=[NSString stringWithFormat:@"%@",[[messagesent objectAtIndex:i] objectForKey:@"groupName"]];
                }
                
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"to"] isKindOfClass:[NSNull class]])
            {
                inbox.to=@"";
            }
            else
            {
                int j= [[[messagesent objectAtIndex:i] objectForKey:@"callType"]intValue];
                
                if (j==17)
                {
                    inbox.to=@"All";
                    inbox.groupName=[[messagesent objectAtIndex:i] objectForKey:@"to"];
                }
                else
                {
                    inbox.to=[[messagesent objectAtIndex:i] objectForKey:@"to"];
                }
                
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"ttl"] isKindOfClass:[NSNull class]])
            {
                inbox.ttl=0;
            }
            else
            {
                NSTimeInterval ttl=[[[messagesent objectAtIndex:i] objectForKey:@"ttl"]doubleValue];
                NSTimeInterval callStartTime=[[[messagesent objectAtIndex:i] objectForKey:@"callStartTime"]doubleValue];
                double ttltime=callStartTime+ttl;
                inbox.ttl=ttltime;
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"pushTxId"] isKindOfClass:[NSNull class]]||[[[messagesent objectAtIndex:i] objectForKey:@"pushTxnId"] isKindOfClass:[NSNull class]])
            {
                inbox.pushTxId=@"";
            }
            else
            {
                if ([[[messagesent objectAtIndex:i] allKeys] containsObject:@"pushTxnId"])
                {
                   inbox.pushTxId=[[messagesent objectAtIndex:i] objectForKey:@"pushTxnId"];
                }
                else
                {
                  inbox.pushTxId=[[messagesent objectAtIndex:i] objectForKey:@"pushTxId"];
                }
                
            }
            
            if ([[[messagesent objectAtIndex:i] objectForKey:@"mode"] isKindOfClass:[NSNull class]])
            {
                inbox.mode=@"";
            }
            else
            {
                inbox.mode=[NSString stringWithFormat:@"%@",[[messagesent objectAtIndex:i] objectForKey:@"mode"]];
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"audience"] isKindOfClass:[NSNull class]])
            {
                int j= [[[messagesent objectAtIndex:i] objectForKey:@"callType"]intValue];
                int k =[[[messagesent objectAtIndex:i] objectForKey:@"maCallType"]intValue];
                
                if (j==17 && k==1)
                {
                    inbox.groupNames=[NSString stringWithFormat:@"%@",[[messagesent objectAtIndex:i] objectForKey:@"to"]];
                    inbox.storeNames=@"";
                    inbox.storeIds=@"";
                    inbox.groupIds=@"";
                    inbox.nodeId=@"";
                    inbox.nodeName=@"";
                }
                else
                {
                    inbox.groupNames=@"";
                    inbox.storeNames=@"";
                    inbox.storeIds=@"";
                    inbox.groupIds=@"";
                    inbox.nodeId=@"";
                    inbox.nodeName=@"";
                    inbox.levelName=@"";
                }
            }
            else
            {
                if ([[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] isKindOfClass:[NSNull class]] || [[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] count]==0)
                {
                    
                    inbox.groupNames=@"";
                    
                }
                else
                {
                    
                    NSString *groupNames = [[[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] valueForKey:@"groupName"] componentsJoinedByString:@","];
                    inbox.groupNames=groupNames;
                    NSString *groupIds = [[[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] valueForKey:@"groupId"] componentsJoinedByString:@","];
                    inbox.groupIds=groupIds;
                    
                }
                if ([[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] isKindOfClass:[NSNull class]] || [[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] count]==0)
                {
                    inbox.storeNames=@"";
                }
                else
                {
                    NSString *storeNames = [[[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] valueForKey:@"storeName"] componentsJoinedByString:@","];
                    inbox.storeNames=storeNames;
                    NSString *storeIds = [[[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] valueForKey:@"storeId"] componentsJoinedByString:@","];
                    inbox.storeIds=storeIds;
                }
                if ([[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] isKindOfClass:[NSNull class]] || [[[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"] count]==0)
                {
                    inbox.nodeName=@"";
                    inbox.levelName=@"";
                }
                else
                {
                    NSString *nodeName = [[[[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"]valueForKey:@"nodeName"] componentsJoinedByString:@","];
                    inbox.nodeName=nodeName;
                    NSString *nodeId = [[[[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"]valueForKey:@"nodeId"] componentsJoinedByString:@","];
                    inbox.nodeId=nodeId;
                    if ([[[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"levelName"]isKindOfClass:[NSNull class]])
                    {
                        inbox.levelName=@"";
                    }
                    else
                    {
                    inbox.levelName= [[[[messagesent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"levelName"];
                    }
                }
            }
            if ([[[messagesent objectAtIndex:i] objectForKey:@"hq"] isKindOfClass:[NSNull class]])
            {
                inbox.managerAppId=@"";
                inbox.tagOutName=@"";
            }
            else
            {
                inbox.managerAppId=[[[messagesent objectAtIndex:i] objectForKey:@"hq"] objectForKey:@"managerAppId"];
                inbox.tagOutName=[[[messagesent objectAtIndex:i] objectForKey:@"hq"] objectForKey:@"tagOutName"];
            }
        }
    }
    NSError *error;
    [store save:error];
    
    if (error != nil) {
        [NSException raise:@"Error saving data to context"
                    format:@"Error: %@", error.localizedDescription];
    }
}

-(void)announcementinboxSave:(NSArray *)announcementinbox
{
    EarboxDataStore *store = [EarboxDataStore sharedStore];
    
    for (int i=0; i<announcementinbox.count; i++)
    {
        NSArray *count;
        if ([[announcementinbox objectAtIndex:i] objectForKey:@"storeId"])
        {
            count=[self exist:store entityname:@"AnnouncementInbox" callid:[[announcementinbox objectAtIndex:i] objectForKey:@"callTxId"] callType:[[announcementinbox objectAtIndex:i] objectForKey:@"callType"] storeid:[[[announcementinbox objectAtIndex:i] objectForKey:@"storeId"] intValue]];
        }
        else
        {
            count=[self exist:store entityname:@"AnnouncementInbox" callid:[[announcementinbox objectAtIndex:i] objectForKey:@"callTxId"] callType:[[announcementinbox objectAtIndex:i] objectForKey:@"callType"] storeid:[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]];
        }
        if (count.count== 1)
        {
            NSLog(@"Earboxinboxmodel-Announcement inbox duplicate callTXId is:%@",[[announcementinbox objectAtIndex:i] objectForKey:@"callTxId"]);
        }
        else
        {
            AnnouncementInbox *inbox = [store createAnnouncementInbox];
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"storeId"] intValue]!=0)
            {
                inbox.storeid=[[[announcementinbox objectAtIndex:i] objectForKey:@"storeId"] intValue];
            }
            else
            {
                inbox.storeid=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"maCallType"] intValue]!=0)
            {
                inbox.maCallType=[[[announcementinbox objectAtIndex:i] objectForKey:@"maCallType"] intValue];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"callTxId"] isKindOfClass:[NSNull class]])
            {
                inbox.callId=0;
            }
            else
            {
                inbox.callId=[NSString stringWithFormat:@"%@",[[announcementinbox objectAtIndex:i] objectForKey:@"callTxId"]];;
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"callType"] isKindOfClass:[NSNull class]])
            {
                inbox.callType=0;
            }
            else
            {
                inbox.callType=[[[announcementinbox objectAtIndex:i] objectForKey:@"callType"]intValue];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"callStartTime"] isKindOfClass:[NSNull class]])
            {
                inbox.callStartTime=0;
            }
            else
            {
                inbox.callStartTime=[[[announcementinbox objectAtIndex:i] objectForKey:@"callStartTime"]longValue];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"details"] isKindOfClass:[NSNull class]])
            {
                inbox.details=@"";
            }
            else
            {
                inbox.details=[[announcementinbox objectAtIndex:i] objectForKey:@"details"];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"downloadURL"] isKindOfClass:[NSNull class]])
            {
                inbox.downloadURL=@"";
            }
            else
            {
                if ([[[announcementinbox objectAtIndex:i] objectForKey:@"storeId"] intValue]!=0)
                {
                    [[AudioManager getsharedInstance]createEarboxAudioPath:[[announcementinbox objectAtIndex:i] objectForKey:@"downloadURL"] callId:[NSString stringWithFormat:@"%@%d%d",[[announcementinbox objectAtIndex:i] objectForKey:@"callTxId"],[[[announcementinbox objectAtIndex:i] objectForKey:@"callType"]intValue],[[[announcementinbox objectAtIndex:i] objectForKey:@"storeId"] intValue]] earboxName:@"AnnouncementInbox" callTxid:[[announcementinbox objectAtIndex:i] objectForKey:@"callTxId"]];
                }
                else
                {
                    [[AudioManager getsharedInstance]createEarboxAudioPath:[[announcementinbox objectAtIndex:i] objectForKey:@"downloadURL"] callId:[NSString stringWithFormat:@"%@%d%d",[[announcementinbox objectAtIndex:i] objectForKey:@"callTxId"],[[[announcementinbox objectAtIndex:i] objectForKey:@"callType"]intValue],[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]] earboxName:@"AnnouncementInbox" callTxid:[[announcementinbox objectAtIndex:i] objectForKey:@"callTxId"]];
                }
                
                inbox.downloadURL=[[announcementinbox objectAtIndex:i] objectForKey:@"downloadURL"];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"duration"] isKindOfClass:[NSNull class]])
            {
                inbox.duration=0;
            }
            else
            {
                inbox.duration=[[[announcementinbox objectAtIndex:i] objectForKey:@"duration"] floatValue];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"from"] isKindOfClass:[NSNull class]])
            {
                inbox.from=@"noname";
            }
            else
            {
                inbox.from=[[announcementinbox objectAtIndex:i] objectForKey:@"from"];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"info"] isKindOfClass:[NSNull class]])
            {
                inbox.info=@"";
            }
            else
            {
                inbox.info=[[announcementinbox objectAtIndex:i] objectForKey:@"info"];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"priority"] isKindOfClass:[NSNull class]])
            {
                inbox.priority=@"";
            }
            else
            {
                inbox.priority=[[announcementinbox objectAtIndex:i] objectForKey:@"priority"];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"status"] isKindOfClass:[NSNull class]])
            {
                inbox.status=@"";
            }
            else
            {
                inbox.status=[[announcementinbox objectAtIndex:i] objectForKey:@"status"];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"replyStatus"] isKindOfClass:[NSNull class]])
            {
                inbox.reply=@"";
            }
            else
            {
                inbox.reply=[NSString stringWithFormat:@"%@",[[announcementinbox objectAtIndex:i] objectForKey:@"replyStatus"]];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"storeName"] isKindOfClass:[NSNull class]])
            {
                inbox.storeName=@"";
            }
            else
            {
                if ([[announcementinbox objectAtIndex:i] objectForKey:@"storeId"])
                {
                    inbox.storeName=[NSString stringWithFormat:@"%@",[[announcementinbox objectAtIndex:i] objectForKey:@"storeName"]];
                }
                else
                {
                    inbox.storeName= [[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
                }
                
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"groupName"] isKindOfClass:[NSNull class]])
            {
                inbox.groupName=@"";
            }
            else
            {
                inbox.groupName=[NSString stringWithFormat:@"%@",[[announcementinbox objectAtIndex:i] objectForKey:@"groupName"]];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"ttl"] isKindOfClass:[NSNull class]])
            {
                inbox.ttl=0;
            }
            else
            {
                NSTimeInterval ttl=[[[announcementinbox objectAtIndex:i] objectForKey:@"ttl"]doubleValue];
                NSTimeInterval callStartTime=[[[announcementinbox objectAtIndex:i] objectForKey:@"callStartTime"]doubleValue];
                double ttltime=callStartTime+ttl;
                inbox.ttl=ttltime;
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"pushTxId"] isKindOfClass:[NSNull class]]||[[[announcementinbox objectAtIndex:i] objectForKey:@"pushTxnId"] isKindOfClass:[NSNull class]])
            {
                inbox.pushTxId=@"";
            }
            else
            {
                if ([[[announcementinbox objectAtIndex:i] allKeys] containsObject:@"pushTxnId"])
                {
                    inbox.pushTxId=[[announcementinbox objectAtIndex:i] objectForKey:@"pushTxnId"];
                }
                else
                {
                    inbox.pushTxId=[[announcementinbox objectAtIndex:i] objectForKey:@"pushTxId"];
                }
                
            }
            
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"mode"] isKindOfClass:[NSNull class]])
            {
                inbox.mode=@"";
            }
            else
            {
                inbox.mode=[NSString stringWithFormat:@"%@",[[announcementinbox objectAtIndex:i] objectForKey:@"mode"]];
            }
            if ([[[announcementinbox objectAtIndex:i] objectForKey:@"audience"] isKindOfClass:[NSNull class]])
            {
                inbox.groupNames=@"";
                inbox.storeNames=@"";
                inbox.storeIds=@"";
                inbox.groupIds=@"";
                inbox.nodeName=@"";
                inbox.nodeId=@"";
                inbox.levelName=@"";
            }
            else
            {
                if ([[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] isKindOfClass:[NSNull class]] || [[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] count]==0)
                {
                    inbox.groupNames=@"";
                }
                else
                {
                    NSString *groupNames = [[[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] valueForKey:@"groupName"] componentsJoinedByString:@","];
                    inbox.groupNames=groupNames;
                    NSString *groupIds = [[[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] valueForKey:@"groupId"] componentsJoinedByString:@","];
                    inbox.groupIds=groupIds;
                }
                if ([[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] isKindOfClass:[NSNull class]] || [[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] count]==0)
                {
                    inbox.storeNames=@"";
                }
                else
                {
                    NSString *storeNames = [[[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] valueForKey:@"storeName"] componentsJoinedByString:@","];
                    inbox.storeNames=storeNames;
                    NSString *storeIds = [[[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] valueForKey:@"storeId"] componentsJoinedByString:@","];
                    inbox.storeIds=storeIds;
                }
                if ([[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] isKindOfClass:[NSNull class]] || [[[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"] count]==0)
                {
                    inbox.nodeName=@"";
                    inbox.levelName=@"";
                }
                else
                {
                    NSString *nodeName = [[[[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"]valueForKey:@"nodeName"] componentsJoinedByString:@","];
                    inbox.nodeName=nodeName;
                    NSString *nodeId = [[[[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"]valueForKey:@"nodeId"] componentsJoinedByString:@","];
                    inbox.nodeId=nodeId;
                    if ([[[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"levelName"]isKindOfClass:[NSNull class]])
                    {
                        inbox.levelName=@"";
                    }
                    else
                    {
                    inbox.levelName= [[[[announcementinbox objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"levelName"];
                    }
                }
            }
        }
    }
    NSError *error=nil;
    [store save:error];
    
    if (error != nil) {
        [NSException raise:@"Error saving data to context"
                    format:@"Error: %@", error.localizedDescription];
    }
    
}

-(void)announcementsavedSave:(NSArray *)announcementsaved
{
    EarboxDataStore *store = [EarboxDataStore sharedStore];
    
    for (int i=0; i<announcementsaved.count; i++)
    {
        NSArray *count;
        if ([[announcementsaved objectAtIndex:i] objectForKey:@"storeId"] )
        {
            count=[self exist:store entityname:@"AnnouncementSaved" callid:[[announcementsaved objectAtIndex:i] objectForKey:@"callTxId"] callType:[[announcementsaved objectAtIndex:i] objectForKey:@"callType"] storeid:[[[announcementsaved objectAtIndex:i] objectForKey:@"storeId"] intValue]];
        }
        else
        {
            count=[self exist:store entityname:@"AnnouncementSaved" callid:[[announcementsaved objectAtIndex:i] objectForKey:@"callTxId"] callType:[[announcementsaved objectAtIndex:i] objectForKey:@"callType"] storeid:[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]];
        }
        
        if (count.count== 1)
        {
            NSLog(@"Earboxinboxmodel-Announcement saved duplicate callTXId is:%@",[[announcementsaved objectAtIndex:i] objectForKey:@"callTxId"]);
        }
        else
        {
            AnnouncementSaved *inbox = [store createAnnouncementSaved];
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"storeId"] intValue]!=0)
            {
                inbox.storeid=[[[announcementsaved objectAtIndex:i] objectForKey:@"storeId"] intValue];
            }
            else
            {
                inbox.storeid=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"maCallType"] intValue]!=0)
            {
                inbox.maCallType=[[[announcementsaved objectAtIndex:i] objectForKey:@"maCallType"] intValue];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"callTxId"] isKindOfClass:[NSNull class]])
            {
                inbox.callId=0;
            }
            else
            {
                inbox.callId=[NSString stringWithFormat:@"%@",[[announcementsaved objectAtIndex:i] objectForKey:@"callTxId"]];;
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"callType"] isKindOfClass:[NSNull class]])
            {
                inbox.callType=0;
            }
            else
            {
                inbox.callType=[[[announcementsaved objectAtIndex:i] objectForKey:@"callType"]intValue];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"callStartTime"] isKindOfClass:[NSNull class]])
            {
                inbox.callStartTime=0;
            }
            else
            {
                inbox.callStartTime=[[[announcementsaved objectAtIndex:i] objectForKey:@"callStartTime"]longValue];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"details"] isKindOfClass:[NSNull class]])
            {
                inbox.details=@"";
            }
            else
            {
                inbox.details=[[announcementsaved objectAtIndex:i] objectForKey:@"details"];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"downloadURL"] isKindOfClass:[NSNull class]])
            {
                inbox.downloadURL=@"";
            }
            else
            {
                if ([[[announcementsaved objectAtIndex:i] objectForKey:@"storeId"] intValue]!=0)
                {
                    [[AudioManager getsharedInstance]createEarboxAudioPath:[[announcementsaved objectAtIndex:i] objectForKey:@"downloadURL"] callId:[NSString stringWithFormat:@"%@%d%d",[[announcementsaved objectAtIndex:i] objectForKey:@"callTxId"],[[[announcementsaved objectAtIndex:i] objectForKey:@"callType"]intValue],[[[announcementsaved objectAtIndex:i] objectForKey:@"storeId"] intValue]] earboxName:@"AnnouncementSaved" callTxid:[[announcementsaved objectAtIndex:i] objectForKey:@"callTxId"]];
                }
                else
                {
                    [[AudioManager getsharedInstance]createEarboxAudioPath:[[announcementsaved objectAtIndex:i] objectForKey:@"downloadURL"] callId:[NSString stringWithFormat:@"%@%d%d",[[announcementsaved objectAtIndex:i] objectForKey:@"callTxId"],[[[announcementsaved objectAtIndex:i] objectForKey:@"callType"]intValue],[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]] earboxName:@"AnnouncementSaved" callTxid:[[announcementsaved objectAtIndex:i] objectForKey:@"callTxId"]];
                }
                
                inbox.downloadURL=[[announcementsaved objectAtIndex:i] objectForKey:@"downloadURL"];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"duration"] isKindOfClass:[NSNull class]])
            {
                inbox.duration=0;
            }
            else
            {
                inbox.duration=[[[announcementsaved objectAtIndex:i] objectForKey:@"duration"] floatValue];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"from"] isKindOfClass:[NSNull class]])
            {
                inbox.from=@"noname";
            }
            else
            {
                inbox.from=[[announcementsaved objectAtIndex:i] objectForKey:@"from"];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"info"] isKindOfClass:[NSNull class]])
            {
                inbox.info=@"";
            }
            else
            {
                inbox.info=[[announcementsaved objectAtIndex:i] objectForKey:@"info"];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"priority"] isKindOfClass:[NSNull class]])
            {
                inbox.priority=@"";
            }
            else
            {
                inbox.priority=[[announcementsaved objectAtIndex:i] objectForKey:@"priority"];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"status"] isKindOfClass:[NSNull class]])
            {
                inbox.status=@"";
            }
            else
            {
                inbox.status=[[announcementsaved objectAtIndex:i] objectForKey:@"status"];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"replyStatus"] isKindOfClass:[NSNull class]])
            {
                inbox.reply=@"";
            }
            else
            {
                inbox.reply=[NSString stringWithFormat:@"%@",[[announcementsaved objectAtIndex:i] objectForKey:@"replyStatus"]];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"storeName"] isKindOfClass:[NSNull class]])
            {
                inbox.storeName=@"";
            }
            else
            {
                if ([[announcementsaved objectAtIndex:i] objectForKey:@"storeId"])
                {
                    inbox.storeName=[NSString stringWithFormat:@"%@",[[announcementsaved objectAtIndex:i] objectForKey:@"storeName"]];
                }
                else
                {
                    inbox.storeName= [[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
                }
                
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"groupName"] isKindOfClass:[NSNull class]])
            {
                inbox.groupName=@"";
            }
            else
            {
                inbox.groupName=[NSString stringWithFormat:@"%@",[[announcementsaved objectAtIndex:i] objectForKey:@"groupName"]];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"ttl"] isKindOfClass:[NSNull class]])
            {
                inbox.ttl=0;
            }
            else
            {
                NSTimeInterval ttl=[[[announcementsaved objectAtIndex:i] objectForKey:@"ttl"]doubleValue];
                NSTimeInterval callStartTime=[[[announcementsaved objectAtIndex:i] objectForKey:@"callStartTime"]doubleValue];
                double ttltime=callStartTime+ttl;
                inbox.ttl=ttltime;
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"pushTxId"] isKindOfClass:[NSNull class]]||[[[announcementsaved objectAtIndex:i] objectForKey:@"pushTxnId"] isKindOfClass:[NSNull class]])
            {
                inbox.pushTxId=@"";
            }
            else
            {
                if ([[[announcementsaved objectAtIndex:i] allKeys] containsObject:@"pushTxnId"])
                {
                    inbox.pushTxId=[[announcementsaved objectAtIndex:i] objectForKey:@"pushTxnId"];
                }
                else
                {
                    inbox.pushTxId=[[announcementsaved objectAtIndex:i] objectForKey:@"pushTxId"];
                }
                
            }
            
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"mode"] isKindOfClass:[NSNull class]])
            {
                inbox.mode=@"";
            }
            else
            {
                inbox.mode=[NSString stringWithFormat:@"%@",[[announcementsaved objectAtIndex:i] objectForKey:@"mode"]];
            }
            if ([[[announcementsaved objectAtIndex:i] objectForKey:@"audience"] isKindOfClass:[NSNull class]])
            {
                inbox.groupNames=@"";
                inbox.storeNames=@"";
                inbox.storeIds=@"";
                inbox.groupIds=@"";
                inbox.nodeId=@"";
                inbox.nodeName=@"";
                inbox.levelName=@"";
            }
            else
            {
                if ([[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] isKindOfClass:[NSNull class]] || [[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] count]==0)
                {
                    inbox.groupNames=@"";
                }
                else
                {
                    NSString *groupNames = [[[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] valueForKey:@"groupName"] componentsJoinedByString:@","];
                    inbox.groupNames=groupNames;
                    NSString *groupIds = [[[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] valueForKey:@"groupId"] componentsJoinedByString:@","];
                    inbox.groupIds=groupIds;
                }
                if ([[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] isKindOfClass:[NSNull class]] || [[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] count]==0)
                {
                    inbox.storeNames=@"";
                }
                else
                {
                    NSString *storeNames = [[[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] valueForKey:@"storeName"] componentsJoinedByString:@","];
                    inbox.storeNames=storeNames;
                    NSString *storeIds = [[[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] valueForKey:@"storeId"] componentsJoinedByString:@","];
                    inbox.storeIds=storeIds;
                }
                if ([[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] isKindOfClass:[NSNull class]] || [[[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"] count]==0)
                {
                    inbox.nodeName=@"";
                    inbox.levelName=@"";
                }
                else
                {
                    NSString *nodeName = [[[[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"]valueForKey:@"nodeName"] componentsJoinedByString:@","];
                    inbox.nodeName=nodeName;
                    NSString *nodeId = [[[[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"]valueForKey:@"nodeId"] componentsJoinedByString:@","];
                    inbox.nodeId=nodeId;
                    if ([[[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"levelName"]isKindOfClass:[NSNull class]])
                    {
                        inbox.levelName=@"";
                    }
                    else
                    {
                    inbox.levelName= [[[[announcementsaved objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"levelName"];
                    }
                }
            }
        }
    }
    NSError *error;
    [store save:error];
    
    if (error != nil) {
        [NSException raise:@"Error saving data to context"
                    format:@"Error: %@", error.localizedDescription];
    }
}

-(void)announcementsentSave:(NSArray *)announcementsent
{
    EarboxDataStore *store = [EarboxDataStore sharedStore];
    
    for (int i=0; i<announcementsent.count; i++)
    {
        NSArray *count;
        if ([[announcementsent objectAtIndex:i] objectForKey:@"storeId"])
        {
            count=[self exist:store entityname:@"AnnouncementSent" callid:[[announcementsent objectAtIndex:i] objectForKey:@"callTxId"] callType:[[announcementsent objectAtIndex:i] objectForKey:@"callType"] storeid:[[[announcementsent objectAtIndex:i] objectForKey:@"storeId"] intValue]];
        }
        else
        {
            count=[self exist:store entityname:@"AnnouncementSent" callid:[[announcementsent objectAtIndex:i] objectForKey:@"callTxId"] callType:[[announcementsent objectAtIndex:i] objectForKey:@"callType"] storeid:[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]];
        }
        
        if (count.count== 1)
        {
            NSLog(@"Earboxinboxmodel-Announcement sent duplicate callTXId is:%@",[[announcementsent objectAtIndex:i] objectForKey:@"callTxId"]);
        }
        else
        {
            AnnouncementSent *inbox = [store createAnnouncementSent];
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"storeId"] intValue]!=0)
            {
                inbox.storeid=[[[announcementsent objectAtIndex:i] objectForKey:@"storeId"] intValue];
            }
            else
            {
                inbox.storeid=[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"maCallType"] intValue]!=0)
            {
                inbox.maCallType=[[[announcementsent objectAtIndex:i] objectForKey:@"maCallType"] intValue];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"callTxId"] isKindOfClass:[NSNull class]])
            {
                inbox.callId=0;
            }
            else
            {
                inbox.callId=[NSString stringWithFormat:@"%@",[[announcementsent objectAtIndex:i] objectForKey:@"callTxId"]];;
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"callType"] isKindOfClass:[NSNull class]])
            {
                inbox.callType=0;
            }
            else
            {
                inbox.callType=[[[announcementsent objectAtIndex:i] objectForKey:@"callType"]intValue];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"callStartTime"] isKindOfClass:[NSNull class]])
            {
                inbox.callStartTime=0;
            }
            else
            {
                inbox.callStartTime=[[[announcementsent objectAtIndex:i] objectForKey:@"callStartTime"]longValue];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"details"] isKindOfClass:[NSNull class]])
            {
                inbox.details=@"";
            }
            else
            {
                inbox.details=[[announcementsent objectAtIndex:i] objectForKey:@"details"];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"downloadURL"] isKindOfClass:[NSNull class]])
            {
                inbox.downloadURL=@"";
            }
            else
            {
                [[AudioManager getsharedInstance]createEarboxAudioPath:[[announcementsent objectAtIndex:i] objectForKey:@"downloadURL"] callId:[[announcementsent objectAtIndex:i] objectForKey:@"callTxId"] earboxName:@"AnnouncementSent" callTxid:[[announcementsent objectAtIndex:i] objectForKey:@"callTxId"]];
                inbox.downloadURL=[[announcementsent objectAtIndex:i] objectForKey:@"downloadURL"];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"duration"] isKindOfClass:[NSNull class]])
            {
                inbox.duration=0;
            }
            else
            {
                inbox.duration=[[[announcementsent objectAtIndex:i] objectForKey:@"duration"] floatValue];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"from"] isKindOfClass:[NSNull class]])
            {
                inbox.from=@"noname";
            }
            else
            {
                inbox.from=[[announcementsent objectAtIndex:i] objectForKey:@"from"];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"info"] isKindOfClass:[NSNull class]])
            {
                inbox.info=@"";
            }
            else
            {
                inbox.info=[[announcementsent objectAtIndex:i] objectForKey:@"info"];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"priority"] isKindOfClass:[NSNull class]])
            {
                inbox.priority=@"";
            }
            else
            {
                inbox.priority=[[announcementsent objectAtIndex:i] objectForKey:@"priority"];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"status"] isKindOfClass:[NSNull class]])
            {
                inbox.status=@"";
            }
            else
            {
                inbox.status=[[announcementsent objectAtIndex:i] objectForKey:@"status"];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"replyStatus"] isKindOfClass:[NSNull class]])
            {
                inbox.reply=@"";
            }
            else
            {
                inbox.reply=[NSString stringWithFormat:@"%@",[[announcementsent objectAtIndex:i] objectForKey:@"replyStatus"]];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"storeName"] isKindOfClass:[NSNull class]])
            {
                inbox.storeName=@"";
            }
            else
            {
                if ([[announcementsent objectAtIndex:i] objectForKey:@"storeId"])
                {
                    inbox.storeName=[NSString stringWithFormat:@"%@",[[announcementsent objectAtIndex:i] objectForKey:@"storeName"]];
                }
                else
                {
                    inbox.storeName= [[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
                }
                
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"groupName"] isKindOfClass:[NSNull class]])
            {
                inbox.groupName=@"";
            }
            else
            {
                inbox.groupName=[NSString stringWithFormat:@"%@",[[announcementsent objectAtIndex:i] objectForKey:@"groupName"]];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"to"] isKindOfClass:[NSNull class]])
            {
                inbox.to=@"";
            }
            else
            {
                // int j= [[[announcementsent objectAtIndex:i] objectForKey:@"callType"]intValue];
                
                //                if (j==25)
                //                {
                //                    inbox.to=@"All";
                //                }
                //                else
                {
                    inbox.to=@"All";//[[announcementsent objectAtIndex:i] objectForKey:@"to"];
                }
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"ttl"] isKindOfClass:[NSNull class]])
            {
                inbox.ttl=0;
            }
            else
            {
                NSTimeInterval ttl=[[[announcementsent objectAtIndex:i] objectForKey:@"ttl"]doubleValue];
                NSTimeInterval callStartTime=[[[announcementsent objectAtIndex:i] objectForKey:@"callStartTime"]doubleValue];
                double ttltime=callStartTime+ttl;
                inbox.ttl=ttltime;
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"pushTxId"] isKindOfClass:[NSNull class]]||[[[announcementsent objectAtIndex:i] objectForKey:@"pushTxnId"] isKindOfClass:[NSNull class]])
            {
                inbox.pushTxId=@"";
            }
            else
            {
                if ([[[announcementsent objectAtIndex:i] allKeys] containsObject:@"pushTxnId"])
                {
                    inbox.pushTxId=[[announcementsent objectAtIndex:i] objectForKey:@"pushTxnId"];
                }
                else
                {
                    inbox.pushTxId=[[announcementsent objectAtIndex:i] objectForKey:@"pushTxId"];
                }
                
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"mode"] isKindOfClass:[NSNull class]])
            {
                inbox.mode=@"";
            }
            else
            {
                inbox.mode=[NSString stringWithFormat:@"%@",[[announcementsent objectAtIndex:i] objectForKey:@"mode"]];
            }
            if ([[[announcementsent objectAtIndex:i] objectForKey:@"audience"] isKindOfClass:[NSNull class]])
            {
                int j= [[[announcementsent objectAtIndex:i] objectForKey:@"callType"]intValue];
                int k =[[[announcementsent objectAtIndex:i] objectForKey:@"maCallType"]intValue];
                
                if (j==17 && k==1)
                {
                    inbox.groupNames=[NSString stringWithFormat:@"%@",[[announcementsent objectAtIndex:i] objectForKey:@"to"]];
                    inbox.storeNames=@"";
                    inbox.storeIds=@"";
                    inbox.groupIds=@"";
                }
                else
                {
                    inbox.groupNames=@"";
                    inbox.storeNames=@"";
                    inbox.storeIds=@"";
                    inbox.groupIds=@"";
                    inbox.levelName=@"";
                }
            }
            else
            {
                if ([[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] isKindOfClass:[NSNull class]] || [[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] count]==0)
                {
                    inbox.groupNames=@"";
                }
                else
                {
                    NSString *groupNames = [[[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] valueForKey:@"groupName"] componentsJoinedByString:@","];
                    inbox.groupNames=groupNames;
                    NSString *groupIds = [[[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"groups"] valueForKey:@"groupId"] componentsJoinedByString:@","];
                    inbox.groupIds=groupIds;
                }
                if ([[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] isKindOfClass:[NSNull class]] || [[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] count]==0)
                {
                    inbox.storeNames=@"";
                }
                else
                {
                    NSString *storeNames = [[[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] valueForKey:@"storeName"] componentsJoinedByString:@","];
                    inbox.storeNames=storeNames;
                    
                    NSString *storeIds = [[[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"stores"] valueForKey:@"storeId"] componentsJoinedByString:@","];
                    inbox.storeIds=storeIds;
                }
                if ([[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] isKindOfClass:[NSNull class]] || [[[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"] count]==0)
                {
                    inbox.nodeName=@"";
                    inbox.levelName=@"";
                }
                else
                {
                    NSString *nodeName = [[[[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"]valueForKey:@"nodeName"] componentsJoinedByString:@","];
                    inbox.nodeName=nodeName;
                    NSString *nodeId = [[[[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"nodes"]valueForKey:@"nodeId"] componentsJoinedByString:@","];
                    inbox.nodeId=nodeId;
                    if ([[[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"levelName"]isKindOfClass:[NSNull class]])
                    {
                        inbox.levelName=@"";
                    }
                    else
                    {
                    inbox.levelName= [[[[announcementsent objectAtIndex:i] objectForKey:@"audience"]objectForKey:@"node"] objectForKey:@"levelName"];
                    }
                }
            }
        }
    }
    NSError *error;
    [store save:error];
    
    if (error != nil) {
        [NSException raise:@"Error saving data to context"
                    format:@"Error: %@", error.localizedDescription];
    }
}

-(void)messageSave:(EarboxSaved *)save
{
    EarboxDataStore *store = [EarboxDataStore sharedStore];
    NSArray *count=[self exist:store entityname:@"EarboxInbox" callid:save.callId callType:save.callId storeid:save.storeid];
    if (count.count== 1)
    {
        NSLog(@"Earboxinboxmodel-Message saved tab duplicate callTXId is:%@",save.callId);
    }
    else
    {
        EarboxSaved *inbox = [store createEarboxSaved];
        inbox.callId=save.callId;
        inbox.maCallType=save.maCallType;
        inbox.callStartTime=save.callStartTime;
        inbox.callType=save.callType;
        inbox.details=save.details;
        inbox.downloadURL=save.downloadURL;
        inbox.duration=save.duration;
        inbox.from=save.from;
        inbox.to=save.to;
        inbox.info=save.info;
        inbox.priority=save.priority;
        inbox.status=save.status;
        inbox.ttl=save.ttl;
        inbox.reply=save.reply;
        inbox.storeid=save.storeid;
        inbox.pushTxId=save.pushTxId;
        inbox.mode=save.mode;
        inbox.storeName=save.storeName;
        inbox.groupName=save.groupName;
        inbox.groupNames=save.groupNames;
        inbox.storeNames=save.storeNames;
        inbox.storeIds=save.storeIds;
        inbox.groupIds=save.groupIds;
        inbox.nodeName=save.nodeName;
        inbox.nodeId=save.nodeId;
        inbox.managerAppId=save.managerAppId;
        inbox.tagOutName=save.tagOutName;
        inbox.levelName=save.levelName;
        NSError *error;
        [store save:error];
        
        if (error != nil) {
            [NSException raise:@"Error saving data to context"
                        format:@"Error: %@", error.localizedDescription];
        }
    }
}

-(void)announcementSave:(AnnouncementSaved *)save
{
    EarboxDataStore *store = [EarboxDataStore sharedStore];
    NSArray *count=[self exist:store entityname:@"AnnouncementInbox" callid:save.callId callType:save.callId storeid:save.storeid];
    if (count.count== 1)
    {
        NSLog(@"Earboxinboxmodel-Announcement saved tab duplicate callTXId is:%@",save.callId);
    }
    else
    {
        AnnouncementSaved *inbox = [store createAnnouncementSaved];
        inbox.callId=save.callId;
        inbox.maCallType=save.maCallType;
        inbox.callStartTime=save.callStartTime;
        inbox.callType=save.callType;
        inbox.details=save.details;
        inbox.downloadURL=save.downloadURL;
        inbox.duration=save.duration;
        inbox.from=save.from;
        inbox.info=save.info;
        inbox.priority=save.priority;
        inbox.status=save.status;
        inbox.ttl=save.ttl;
        inbox.reply=save.reply;
        inbox.storeid=save.storeid;
        inbox.pushTxId=save.pushTxId;
        inbox.mode=save.mode;
        inbox.storeName=save.storeName;
        inbox.groupName=save.groupName;
        inbox.groupNames=save.groupNames;
        inbox.storeNames=save.storeNames;
        inbox.storeIds=save.storeIds;
        inbox.groupIds=save.groupIds;
        inbox.nodeName=save.nodeName;
        inbox.nodeId=save.nodeId;
        inbox.levelName=save.levelName;
        NSError *error;
        [store save:error];
        
        if (error != nil) {
            [NSException raise:@"Error saving data to context"
                        format:@"Error: %@", error.localizedDescription];
        }
    }
}

-(void)favoritesInsert:(NSArray *)favoritesinbox
{
    EarboxDataStore *store = [EarboxDataStore sharedStore];
    for (int i=0; i<favoritesinbox.count; i++)
    {
        /*
         NSArray *count;
         if ([[messageinbox objectAtIndex:i] objectForKey:@"storeId"])
         {
         count=[self exist:store entityname:@"EarboxInbox" callid:[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"] callType:[[messageinbox objectAtIndex:i] objectForKey:@"callType"] storeid:[[[messageinbox objectAtIndex:i] objectForKey:@"storeId"] intValue]];
         }
         else
         {
         count=[self exist:store entityname:@"EarboxInbox" callid:[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"] callType:[[messageinbox objectAtIndex:i] objectForKey:@"callType"] storeid:[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]];
         }
         if (count.count== 1)
         {
         NSLog(@"Message inbox duplicate callTXId is:%@",[[messageinbox objectAtIndex:i] objectForKey:@"callTxId"]);
         }
         else
         {*/
        Favorites *inbox = [store createFavorites];
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"PlayAudioType"] isKindOfClass:[NSNull class]])
        {
            inbox.playAudioType=@"";
        }
        else
        {
            inbox.playAudioType=[[favoritesinbox objectAtIndex:i] objectForKey:@"PlayAudioType"];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"FinalView"] isKindOfClass:[NSNull class]])
        {
            inbox.finalView=@"";
        }
        else
        {
            inbox.finalView=[[favoritesinbox objectAtIndex:i] objectForKey:@"FinalView"];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"FavouriteName"] isKindOfClass:[NSNull class]])
        {
            inbox.favouriteName=@"";
        }
        else
        {
            inbox.favouriteName=[[favoritesinbox objectAtIndex:i] objectForKey:@"FavouriteName"];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"MessageType"] isKindOfClass:[NSNull class]])
        {
            inbox.messageType=@"";
        }         else
        {
            inbox.messageType=[[favoritesinbox objectAtIndex:i] objectForKey:@"MessageType"];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"FeatureType"] isKindOfClass:[NSNull class]])
        {
            inbox.featureType=@"";
        }
        else
        {
            inbox.featureType=[[favoritesinbox objectAtIndex:i] objectForKey:@"FeatureType"];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"FeatureType"] isKindOfClass:[NSNull class]])
        {
            inbox.featureType=@"";
        }
        else
        {
            inbox.featureType=[[favoritesinbox objectAtIndex:i] objectForKey:@"FeatureType"];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"storeselect"] isKindOfClass:[NSNull class]])
        {
            inbox.storeselect=@"";
        }
        else
        {
            inbox.storeselect=[[favoritesinbox objectAtIndex:i] objectForKey:@"storeselect"];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"title"] isKindOfClass:[NSNull class]])
        {
            inbox.title=@"";
        }
        else
        {
            inbox.title=[[favoritesinbox objectAtIndex:i] objectForKey:@"title"];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"PlatForm"] isKindOfClass:[NSNull class]])
        {
            inbox.platForm=@"";
        }
        else
        {
            inbox.platForm=[[favoritesinbox objectAtIndex:i] objectForKey:@"PlatForm"];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"FavouriteID"] isKindOfClass:[NSNull class]])
        {
            inbox.favouriteID=@"";
        }
        else
        {
            inbox.favouriteID=[NSString stringWithFormat:@"%@",[[favoritesinbox objectAtIndex:i] objectForKey:@"FavouriteID"]];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"FavouriteType"] isKindOfClass:[NSNull class]])
        {
            inbox.favouriteType=@"";
        }
        else
        {
            inbox.favouriteType=[[favoritesinbox objectAtIndex:i] objectForKey:@"FavouriteType"];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"NodeId"] isKindOfClass:[NSNull class]])
        {
            inbox.nodeId=@"";
        }
        else
        {
            inbox.nodeId=[NSString stringWithFormat:@"%@",[[favoritesinbox objectAtIndex:i] objectForKey:@"NodeId"]];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"ParentId"] isKindOfClass:[NSNull class]])
        {
            inbox.parentId=@"";
        }
        else
        {
            inbox.parentId=[NSString stringWithFormat:@"%@",[[favoritesinbox objectAtIndex:i] objectForKey:@"ParentId"]];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"LevelId"] isKindOfClass:[NSNull class]])
        {
            inbox.levelId=@"";
        }
        else
        {
            inbox.levelId=[NSString stringWithFormat:@"%@",[[favoritesinbox objectAtIndex:i] objectForKey:@"LevelId"]];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"StoreScreen"] isKindOfClass:[NSNull class]])
        {
            inbox.storeScreen=@"";
        }
        else
        {
            inbox.storeScreen=[[favoritesinbox objectAtIndex:i] objectForKey:@"StoreScreen"];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"Groups"] isKindOfClass:[NSNull class]])
        {
            NSData *data ;
            inbox.groups=data;
        }
        else
        {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[[favoritesinbox objectAtIndex:i] objectForKey:@"Groups"]];
            inbox.groups=data;
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"StoreNames"] isKindOfClass:[NSNull class]])
        {
            NSData *data ;
            inbox.storeNames=data;
        }
        else
        {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[[favoritesinbox objectAtIndex:i] objectForKey:@"StoreNames"]];
            inbox.storeNames=data;
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"Stores"] isKindOfClass:[NSNull class]])
        {
            NSData *data ;
            inbox.stores=data;
        }
        else
        {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[[favoritesinbox objectAtIndex:i] objectForKey:@"Stores"]];
            inbox.stores=data;
        }
        /*
         [postData setObject:@"" forKey:@"MultiStore"];
         [postData setObject:@"" forKey:@"HQTagOutName"];
         [postData setObject:@"" forKey:@"HQManagerAppID"];
         [postData setObject:@[] forKey:@"Nodes"];
         [postData setObject:@[] forKey:@"MultiStoreNodeId"];
         [postData setObject:@[] forKey:@"MultiStoreNodesNames"];
         */
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"MultiStoreNodeId"] isKindOfClass:[NSNull class]])
        {
            NSData *data ;
            inbox.multiStoreNodeId=data;
        }
        else
        {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[[favoritesinbox objectAtIndex:i] objectForKey:@"MultiStoreNodeId"]];
            inbox.multiStoreNodeId=data;
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"Nodes"] isKindOfClass:[NSNull class]])
        {
            NSData *data ;
            inbox.nodes=data;
        }
        else
        {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[[favoritesinbox objectAtIndex:i] objectForKey:@"Nodes"]];
            inbox.nodes=data;
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"MultiStoreNodesNames"] isKindOfClass:[NSNull class]])
        {
            NSData *data ;
            inbox.multiStoreNodesNames=data;
        }
        else
        {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[[favoritesinbox objectAtIndex:i] objectForKey:@"MultiStoreNodesNames"]];
            inbox.multiStoreNodesNames=data;
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"MultiStore"] isKindOfClass:[NSNull class]])
        {
            inbox.multiStore=@"";
        }
        else
        {
            inbox.multiStore=[NSString stringWithFormat:@"%@",[[favoritesinbox objectAtIndex:i] objectForKey:@"MultiStore"]];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"HQTagOutName"] isKindOfClass:[NSNull class]])
        {
            inbox.hQTagOutName=@"";
        }
        else
        {
            inbox.hQTagOutName=[NSString stringWithFormat:@"%@",[[favoritesinbox objectAtIndex:i] objectForKey:@"HQTagOutName"]];
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"HQManagerAppID"] isKindOfClass:[NSNull class]])
        {
            NSData *data ;
            inbox.hQManagerAppID=data;
        }
        else
        {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[[favoritesinbox objectAtIndex:i] objectForKey:@"HQManagerAppID"]];
            inbox.hQManagerAppID=data;
        }
        if ([[[favoritesinbox objectAtIndex:i] objectForKey:@"levelName"] isKindOfClass:[NSNull class]])
        {
            inbox.levelName=@"";
        }
        else
        {
            inbox.levelName=[NSString stringWithFormat:@"%@",[[favoritesinbox objectAtIndex:i] objectForKey:@"levelName"]];
        }
        
    }
    NSError *error;
    [store save:error];
    
    if (error != nil) {
        [NSException raise:@"Error saving data to context"
                    format:@"Error: %@", error.localizedDescription];
    }
}

@end
