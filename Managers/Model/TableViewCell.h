//
//  TableViewCell.h
//  Earbox
//
//  Created by sandeepchalla on 4/28/16.
//  Copyright © 2016 sandeepchalla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICircularProgressView.h"
#import "MarqueeLabel.h"

@interface TableViewCell : UITableViewCell
{
    BOOL cellselect;
}
@property (weak, nonatomic) IBOutlet UILabel *groupLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupnameLabel;
@property (weak, nonatomic) IBOutlet UILabel *storeLabel;
@property (weak, nonatomic) IBOutlet UILabel *storenameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *statusimage;
@property (weak, nonatomic) IBOutlet UILabel *expriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *lengthLabel;
@property (weak, nonatomic) IBOutlet UILabel *sentLabel;
@property (weak, nonatomic) IBOutlet UILabel *expriesTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *lengthTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *sentTextLabel;
@property (weak, nonatomic) IBOutlet MarqueeLabel *nameTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *nameTextButton;
@property (weak, nonatomic) IBOutlet UIButton *sendTextButton;
@property (weak, nonatomic) IBOutlet UIButton *replyTextButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteTextButton;
@property (unsafe_unretained, nonatomic) IBOutlet UICircularProgressView *circularProgressView;
@property (weak, nonatomic) IBOutlet UISlider *currentTimeSlider;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UILabel *timeElapsed;
@property (weak, nonatomic) IBOutlet UIImageView *replyimage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;
@end
