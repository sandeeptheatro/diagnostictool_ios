//
//  PlacesTableViewCell.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "PlacesTableViewCell.h"
#import "Constant.h"

@implementation PlacesTableViewCell
@synthesize placeButton;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawRect:(CGRect)rect
{
    self.countLbl.layer.cornerRadius = self.countLbl.frame.size.width / 2;
    self.countLbl.clipsToBounds=YES;
    [super drawRect:rect];
}

@end
