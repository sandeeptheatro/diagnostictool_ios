//
//  ContactsList.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 16/01/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactsList : NSObject
@property(nonatomic,strong)NSString *tagOutName;
@property(nonatomic,strong)NSString *covarageTagOutName;
@property(nonatomic,strong)NSString *status;
@property(nonatomic,strong)NSString *location;
@property(nonatomic,assign)int employeeId;
@property(nonatomic,strong)NSString *manageriCon;
@property(nonatomic,strong)NSString *hiphopTime;
@end
