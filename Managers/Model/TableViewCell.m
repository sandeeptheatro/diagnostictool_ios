//
//  TableViewCell.m
//  Earbox
//
//  Created by sandeepchalla on 4/28/16.
//  Copyright © 2016 sandeepchalla. All rights reserved.
//

#import "TableViewCell.h"
#import "Constant.h"
#import <QuartzCore/QuartzCore.h>

@implementation TableViewCell

- (void)drawRect:(CGRect)rect
{
    self.playButton.clipsToBounds = YES;
    self.nameTextButton.clipsToBounds = NO;
    self.nameTextButton.layer.cornerRadius = self.nameTextButton.frame.size.width/2.0f;
    [super drawRect:rect];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.sendTextButton.layer.cornerRadius=4;
    //self.sendTextButton.backgroundColor=kNavBackgroundColor;
    self.replyTextButton.layer.cornerRadius=4;
   // self.replyTextButton.backgroundColor=kNavBackgroundColor;
    self.deleteTextButton.layer.cornerRadius=4;
    self.deleteTextButton.backgroundColor=kNavBackgroundColor;
    //self.playButton.layer.cornerRadius=18;
    
    self.nameTextButton.layer.borderColor=kNavBackgroundColor.CGColor;
    self.nameTextButton.layer.borderWidth=2.0f;
    /* if (selected)
     {
     cellselect=YES;
     self.nameTextLabel.font = [UIFont systemFontOfSize: 17.0];
     self.sentLabel.font = [UIFont systemFontOfSize: 10.0];
     self.sentTextLabel.font = [UIFont systemFontOfSize: 10.0];
     self.expriesLabel.font = [UIFont systemFontOfSize: 10.0];
     self.expriesTextLabel.font = [UIFont systemFontOfSize: 10.0];
     self.lengthLabel.font = [UIFont systemFontOfSize: 10.0];
     self.lengthTextLabel.font = [UIFont systemFontOfSize: 10.0];
     self.nameTextButton.titleLabel.font=[UIFont systemFontOfSize: 20.0];
     }
     else
     {
     if (cellselect==YES)
     {
     self.nameTextLabel.font = [UIFont systemFontOfSize: 17.0];
     self.sentLabel.font = [UIFont systemFontOfSize: 10.0];
     self.sentTextLabel.font = [UIFont systemFontOfSize: 10.0];
     self.expriesLabel.font = [UIFont systemFontOfSize: 10.0];
     self.expriesTextLabel.font = [UIFont systemFontOfSize: 10.0];
     self.lengthLabel.font = [UIFont systemFontOfSize: 10.0];
     self.lengthTextLabel.font = [UIFont systemFontOfSize: 10.0];
     self.nameTextButton.titleLabel.font=[UIFont systemFontOfSize: 20.0];
     }
     else
     {
     self.nameTextLabel.font = [UIFont boldSystemFontOfSize: 17.0];
     self.sentLabel.font = [UIFont boldSystemFontOfSize: 10.0];
     self.sentTextLabel.font = [UIFont boldSystemFontOfSize: 10.0];
     self.expriesLabel.font = [UIFont boldSystemFontOfSize: 10.0];
     self.expriesTextLabel.font = [UIFont boldSystemFontOfSize: 10.0];
     self.lengthLabel.font = [UIFont boldSystemFontOfSize: 10.0];
     self.lengthTextLabel.font = [UIFont boldSystemFontOfSize: 10.0];
     self.nameTextButton.titleLabel.font=[UIFont boldSystemFontOfSize: 20.0];
     }
     }*/
    // Configure the view for the selected state
}

@end
