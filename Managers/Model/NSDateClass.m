//
//  NSDateClass.m
//  Managers
//
//  Created by sandeepchalla on 6/23/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "NSDateClass.h"

@implementation NSDateClass

+(NSString *)ttlformate:(long)totalSec
{
    
    NSString *langStr=[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"];
    NSDictionary *dictLang =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    NSInteger mm=0,hh=0,dd=0;
    NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval seconds = [[[NSString stringWithFormat:@"%f",timeInMiliseconds] componentsSeparatedByString:@"."][0]  doubleValue];
    
    int totalNumSec;
    totalNumSec = totalSec - seconds;
    
    NSString *ttltime=[[NSString alloc]init];
    
    if(totalNumSec < 0)
    {
        return @"0";
    }
    
    if(totalNumSec <=59 && totalNumSec >=0)
    {
        ttltime=[dictLang objectForKey:@"few sec"];
        return ttltime;
    }
    
    dd = totalNumSec / (60 * 60 * 24);
    totalNumSec -= dd * (60 * 60 * 24);
    hh = totalNumSec / (60 * 60);
    totalNumSec -= hh * (60 * 60);
    mm = totalNumSec / 60;
   // totalNumSec -= (mm * 60);
    // ss = totalNumSec;
    
   /* if(totalNumSec >= 60)
    {
        mm = totalNumSec % 60;
        totalNumSec = totalNumSec/60;
    }
    else
    {
        mm = totalNumSec;
        totalNumSec = 0;
        
    }
    if(totalNumSec >= 24)
    {
        hh = totalNumSec % 24;
        dd = totalNumSec/24;
    }
    else
    {
        hh = totalNumSec;
        //totalNumSec = 0;
    }*/
    
    
    if(dd)
    {
        if ([langStr isEqualToString:@"en"])
        {
            ttltime=[NSString stringWithFormat:@"%ldd ",(long)dd];
        }
        else{
            ttltime=[NSString stringWithFormat:@"%ldj ",(long)dd];
        }
        
    }
    if(hh)
    {
        ttltime=[ttltime stringByAppendingFormat:@"%ldh ",(long)hh];
    }
    if (mm)
    {
        if (dd && hh)
        {
            
        }
        else
        {
        ttltime=[ttltime stringByAppendingFormat:@"%ldm ",(long)mm];
        }
    }
    /*if (dd&&hh&&mm)
     {
     ttltime=[NSString stringWithFormat:@"%ld h %ld m",(long)hh,(long)mm];
     }
     else  if (dd&&mm)
     {
     ttltime=[NSString stringWithFormat:@"%ld d %ld m",(long)dd,(long)mm];
     }
     else  if (dd&&hh)
     {
     ttltime=[NSString stringWithFormat:@"%ld d %ld h",(long)dd,(long)hh];
     }*/
    return ttltime;
    
}

+(NSString *)dateformate:(long )seconds
{
    
    // Convert NSString to NSTimeInterval
    // NSTimeInterval seconds = [[epochTime componentsSeparatedByString:@"."][0]  doubleValue];
    
    // (Step 1) Create NSDate object
    NSDate *epochNSDate = [[NSDate alloc] initWithTimeIntervalSince1970:seconds];
    //NSLog (@"Epoch time %@ equates to UTC %@", epochTime, epochNSDate);
    
    
    // (Step 2) Use NSDateFormatter to display epochNSDate in local time zone
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    NSString *local = [dateFormatter stringFromDate:epochNSDate];
    NSString *today=[dateFormatter stringFromDate:[NSDate date]];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *weekdayComponents =
    [gregorian components:(NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:[dateFormatter dateFromString:local]];
    
    NSDateComponents *todayweekdayComponents =
    [gregorian components:(NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:[dateFormatter dateFromString:today]];
    
    
    
    
    NSInteger day = [weekdayComponents day];
    NSUInteger month=[weekdayComponents month];
    NSUInteger year=[weekdayComponents year];
    NSInteger hour = [weekdayComponents hour];
    NSInteger minute = [weekdayComponents minute];
    NSDateFormatter* theDateFormatter = [[NSDateFormatter alloc] init];
    [theDateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [theDateFormatter setDateFormat:@"EEEE"];
    
    NSInteger todaydate = [todayweekdayComponents day];
    
    NSInteger diffDays = todaydate - day;
    if(diffDays == 0)
    {
        if (minute< 10)
        {
            return [NSString stringWithFormat:@"%ld:0%ld",(long)hour,(long)minute];
        }
        else
        {
            return [NSString stringWithFormat:@"%ld:%ld",(long)hour,(long)minute];
        }
    }
    if(diffDays == 1)
    {
            NSString *langStr=[[NSUserDefaults standardUserDefaults] objectForKey:@"ChoosedLang"];
        NSString *strD;
        if ([langStr isEqualToString:@"en"])
        {
            strD =@"Yesterday";
        }
        else
        {
            strD=@"Hier";
        }
        return strD;
    }
    
    return [NSString stringWithFormat:@"%ld/%ld/%ld",(long)day,(long)month,(long)year];
    
}


+(NSString *)dateformatesort:(long )seconds
{
    
    // Convert NSString to NSTimeInterval
    // NSTimeInterval seconds = [[epochTime componentsSeparatedByString:@"."][0]  doubleValue];
    
    // (Step 1) Create NSDate object
    NSDate *epochNSDate = [[NSDate alloc] initWithTimeIntervalSince1970:seconds];
    //NSLog (@"Epoch time %@ equates to UTC %@", epochTime, epochNSDate);
    
    
    // (Step 2) Use NSDateFormatter to display epochNSDate in local time zone
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    NSString *local = [dateFormatter stringFromDate:epochNSDate];
    NSString *today=[dateFormatter stringFromDate:[NSDate date]];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *weekdayComponents =
    [gregorian components:(NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:[dateFormatter dateFromString:local]];
    
    NSDateComponents *todayweekdayComponents =
    [gregorian components:(NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:[dateFormatter dateFromString:today]];
    
    
    
    
    NSInteger day = [weekdayComponents day];
    NSInteger hour = [weekdayComponents hour];
    NSDateFormatter* theDateFormatter = [[NSDateFormatter alloc] init];
    [theDateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [theDateFormatter setDateFormat:@"EEEE"];
    
    NSInteger todayhour = [todayweekdayComponents hour];
    NSInteger todaydate = [todayweekdayComponents day];
    
    NSInteger diffDays = todaydate - day;
    if(diffDays == 0)
    {
    if (todayhour-hour<=1)
    {
        return @"1";
    }
    else if (todayhour-hour<=24)
    {
        return @"24";
    }
    }
    if(diffDays >0 && diffDays < 7)
    {
        if (todayhour-hour<=24 && todayhour-hour<=168)
        {
            return @"168";
        }
    }
    
   /* NSInteger todaydate = [todayweekdayComponents day];
    
    NSInteger diffDays = todaydate - day;
    if(diffDays == 0)
    {
        if (minute < 61 && hour>1)
        {
            return @"1";
        }
        else if (hour>1)
        {
            return @"24";
        }
    }
    if(diffDays >0 && diffDays < 7)
    {
        return @"168";
    }*/
    
    return @"200";
    
}


+(NSString *)lengthformate:(float)totalNumSec
{
    int ss=0,mm=0;
    NSString *ttltime=[[NSString alloc]init];
   
    if(totalNumSec < 1)
    {
        if(totalNumSec > 0.5)
        {
            ttltime=@"00:01";
        }
        else
        {
            ttltime=@"00:00";
        }
        return ttltime;
    }
    
    
    if(totalNumSec >= 60)
    {
        ss = (int) totalNumSec % (60);
        totalNumSec = totalNumSec/60.0;
    }
    else{
        ss = totalNumSec;
        totalNumSec = 0;
        
    }
    if(totalNumSec >= 60)
    {
        mm = (int)totalNumSec % 60;
        //totalNumSec = totalNumSec/60;
    }
    else
    {
        mm = totalNumSec;
        //totalNumSec = 0;
        
    }
    
    if(mm)
    {
        if (mm>9)
        {
            if (ss>9)
            {
                ttltime=[NSString stringWithFormat:@"%ld:%ld",(long)mm,(long)ss];
            }
            else
            {
                ttltime=[NSString stringWithFormat:@"%ld:0%ld",(long)mm,(long)ss];
            }
            
        }
        else
        {
            if (ss>9)
            {
                ttltime=[NSString stringWithFormat:@"0%ld:%ld",(long)mm,(long)ss];
            }
            else
            {
                ttltime=[NSString stringWithFormat:@"0%ld:0%ld",(long)mm,(long)ss];
            }
        }
    }
    else if (ss)
    {
        
        if (ss>9)
        {
            ttltime=[NSString stringWithFormat:@"00:%ld",(long)ss];
        }
        else
        {
            ttltime=[NSString stringWithFormat:@"00:0%ld",(long)ss];
        }
        
    }
    return ttltime;
    
}

@end
