//
//  GroupList.h
//  Managers
//
//  Created by Ravi Shankar on 18/01/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupList : NSObject
@property(nonatomic,strong)NSString *name,*firstName,*lastName;
@property(nonatomic,assign)int groupId,groupType;

@end
