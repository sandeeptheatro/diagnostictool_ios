//
//  NSDateClass.h
//  Managers
//
//  Created by sandeepchalla on 6/23/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateClass : NSObject
+(NSString *)ttlformate:(long)totalSec;
+(NSString *)dateformate:(long )seconds;
+(NSString *)lengthformate:(float)totalNumSec;
+(NSString *)dateformatesort:(long )seconds;

@end
