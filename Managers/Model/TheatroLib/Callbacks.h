//
//  Callbacks.h
//  TheatroComLib
//
//  Created by Ravi Shankar on 07/02/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#ifndef __TheatroComLib__Callbacks__
#define __TheatroComLib__Callbacks__

#include <stdio.h>
typedef void (*updateStatus) (id,SEL,char* coverageString);
#endif /* defined(__TheatroComLib__Callbacks__) */
