//
//  TheatroComLib.h
//  TheatroComLib
//
//  Created by Ravi Shankar on 04/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <stdio.h>
#include <sys/socket.h>

#include "TheatroDefs.h"
#include "TheatroCommunicator.h"
#include "cJSON.h"
#include "Trace.h"
#include "Callbacks.h"

extern char TGSIPAddress[NAMESIZE];
typedef enum {
    WIFI_OFF_CELLULAR_OFF = 0,
    WIFI_OFF_CELLULAR_ON = 1,
    WIFI_ON_TGS_ON = 2,
    WIFI_ON_TGS_OFF = 3,
} NETWORK_STATE;
@interface TheatroComLib : NSObject

void startTheatroNative(void);
void killTheatroNativeLib(void);
void doMenuButtonAction(bool isDown);
void doQueryButtonAction(bool isDown);
void sendIpToNative(const char* Ip);
void sendProxyAddressToNative(const char* ipAddress);
void sendLoginNameToNative(const char *name);
void sendLogoffToNative(void);
void sendMacAddress(const char *comId);
void sendChainNameAndStoreNameToNative(const char *chainName);
void initCallbacks (updateStatus status , SEL sel, id idf);
void sendRefreshEvent(void);
void EnableBroadcastScreen(bool value);
void sendPlayMsgToNative(void);
void sendPlayAnncToNative(void);
void sendCancelPlayOutToNative(bool isStopped);
void sendCallEndToNative(void);
void sendPlaybufferenable(bool value);
void doMediabuttonAction(const char *typeStr, bool down);
void sendHuddleInfoToNative(const char *time , int type);
void sendVersionToNative(const char *ver);
void sendMsgActionToNative(int type,const char *txnId);

void sendDataToNative(void *buffer, unsigned int size);
unsigned int readDataFromNative(void *buffer,unsigned int size);
unsigned int getAvailablePlaybackDataFromNative(void);
bool isAudioCaptureRingBufferFull(void);
bool isAudioPlaybackRingBufferEmpty(void);
void reOpenClosedSockets(void);
void clearOraIdFromNative(void); //Added for slow internet connections...

void sendNetworkStatusToNative(NETWORK_STATE value);
void sendWebSocketPathToNative(const char *path);
void sendSwitchStatusToNative(bool value);
void sendChainNameToNative(const char *chainName);


//Websocket changes.


@end
