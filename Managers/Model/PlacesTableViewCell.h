//
//  PlacesTableViewCell.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 13/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlacesTableViewCell : UITableViewCell

@property(nonatomic,weak)IBOutlet UIButton *placeButton;
@property(nonatomic,weak)IBOutlet UILabel *countLbl;


@end
