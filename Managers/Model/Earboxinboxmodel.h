//
//  Earboxinboxmodel.h
//  Managers
//
//  Created by sandeepchalla on 6/13/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EarboxInbox+CoreDataProperties.h"
#import "EarboxSaved+CoreDataProperties.h"
#import "EarboxSent+CoreDataProperties.h"
#import "AnnouncementInbox+CoreDataProperties.h"
#import "AnnouncementSent+CoreDataProperties.h"
#import "AnnouncementSaved+CoreDataProperties.h"
#import "Favorites+CoreDataProperties.h"

@interface Earboxinboxmodel : NSObject

+ (Earboxinboxmodel *)sharedModel;
-(void)messageinboxSave:(NSArray *)messageinbox;
-(void)messagesavedSave:(NSArray *)messagesaved;
-(void)messagesentSave:(NSArray *)messagesent;
-(void)announcementinboxSave:(NSArray *)announcementinbox;
-(void)announcementsavedSave:(NSArray *)announcementsaved;
-(void)announcementsentSave:(NSArray *)announcementsent;
-(void)messageSave:(EarboxSaved *)save;
-(void)announcementSave:(AnnouncementSaved *)save;
-(void)favoritesInsert:(NSArray *)favoritesinbox;
@end
