//
//  StoreList.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 16/01/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreList : NSObject
@property(nonatomic,strong)NSString *name;
@property(nonatomic,assign)int storeId,announcementCount,messageCount,totalCount;

@end
