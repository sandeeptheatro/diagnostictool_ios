//
//  UIViewController.h
//  Managers
//
//  Created by Ravi Shankar on 20/05/15.
//  Copyright (c) 2015 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Utils)

+(UIViewController*) currentViewController;

@end
