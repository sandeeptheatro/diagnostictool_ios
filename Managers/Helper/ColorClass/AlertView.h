//
//  AlertView.h
//  Managers
//
//  Created by Ravi Shankar on 23/01/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertView : UIAlertView
+(UIAlertView *)alert:(NSString *)title message:(NSString *)message;

@end
