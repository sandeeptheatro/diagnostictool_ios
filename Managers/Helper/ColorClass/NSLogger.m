//
//  NSLogger.m
//  Managers
//
//  Created by sandeepchalla on 7/28/15.
//  Copyright (c) 2015 theatro. All rights reserved.
//

#import "NSLogger.h"

#import <unistd.h>

@implementation NSLogger

@synthesize stdErrRedirected;

static int savedStdErr = 0;
static NSLogger *sharedInstance;

+ (NSLogger *) sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(id)init {
    return self;
}


- (void) writeNSLogToFile
{
    //[self restoreStdErr];
    
    
    stdErrRedirected = YES;
    savedStdErr = dup(STDERR_FILENO);
    
    NSString *cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *logPath = [cachesDirectory stringByAppendingPathComponent:@"nslog.log"];
    freopen([logPath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stderr);
    
}

-(NSString *)getdata
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"nslog.log"];
    return filePath;
}

-(BOOL)removefilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"nslog.log"];
    return [[NSFileManager defaultManager] removeItemAtPath:filePath error:NULL];
}

- (void)restoreStdErr
{
    if (stdErrRedirected)
    {
        stdErrRedirected = NO;
        fflush(stderr);
        
        dup2(savedStdErr, STDERR_FILENO);
        close(savedStdErr);
        savedStdErr = 0;
    }
}

@end

