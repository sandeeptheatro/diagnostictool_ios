//
//  ActivityIndicatorView.m
//  ManagersMockup
//
//  Created by Ravi Shankar on 16/01/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#import "ActivityIndicatorView.h"

@implementation ActivityIndicatorView

+(UIView *)activityIndicatorView:(UIView *)view
{
    UIView *backendView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    backendView.alpha=0.5;
    backendView.backgroundColor=[UIColor darkGrayColor];
    UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake((view.frame.size.width-30)/2, view.frame.size.height/2, 60, 60)];
    activityView.backgroundColor=[UIColor darkGrayColor];
    activityView.activityIndicatorViewStyle=UIActivityIndicatorViewStyleWhiteLarge;
    [activityView startAnimating];
    [backendView addSubview:activityView];
    return backendView;
}

+(UIView *)alphaView:(UIView *)view
{
    UIView *backendView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    backendView.alpha=0.5;
    backendView.backgroundColor=[UIColor darkGrayColor];
    return backendView;
}

+(UIView *)alphaView1:(UIView *)view
{
    UIView *backendView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    backendView.alpha=0.75;
    backendView.backgroundColor=[UIColor darkGrayColor];
    return backendView;
}

+(void)activityIndicatorViewRemove:(UIView *)view
{
    for (UIView *subview in view.subviews)
    {
        if ([subview isKindOfClass:[UIView class]])
        {
            if (subview.alpha==0.5)
            {
                [subview removeFromSuperview];
            }
            
        }
    }
}

+(void)activityIndicatorViewRemove1:(UIView *)view
{
    for (UIView *subview in view.subviews)
    {
        if ([subview isKindOfClass:[UIView class]])
        {
            if (subview.alpha==0.75)
            {
                [subview removeFromSuperview];
            }
            
        }
    }
}

@end
