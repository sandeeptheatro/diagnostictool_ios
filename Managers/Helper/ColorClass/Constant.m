

#import "Constant.h"

@implementation Constant

NSString *const kFontName = @"Helvetica-Bold";
CGFloat kFontSize = 17.0f;
//NSString *const kGlobleURL = @"http://192.168.3.6:2608/restapi/mapp/";
//NSDictionary *dict=[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
NSString *const kGlobleURL = @"https://itg.theatrocentral.com/restapi/mapp/";
NSString *const kInternetConnectiontitle=@"No Internet connection";
NSString *const kInternetConnectionmessage=@"Connect to the internet";

@end
