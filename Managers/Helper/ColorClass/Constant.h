

#import <Foundation/Foundation.h>
#import "NSString+Color.h"
#define kLineBackgroundColor [@"#ff33b5e5" toColor]
#define kNavBackgroundColor [@"#306190" toColor]
#define kTabbarColor [@"#003366" toColor]
#define kGreyColor [@"#808080" toColor];
#define kGPlusColor [@"#3179FF" toColor];
#define kSettingsColor [@"#C9DCE4" toColor];
#define kGreencolor [@"#228b22" toColor]
#define kLightcolor [@"#008080" toColor]
#define kSkybluecolor [@"#C72F0F" toColor]
#define kmessageselectedtabcolor [@"#859CAA" toColor]
#define kmessageunselectedtabcolor [@"#8097B2" toColor]
#define kmessageselectedtabbordercolor [@"#2E4271" toColor]
#define kinboxselectedtabcolor [@"#D5E2EB" toColor]
#define kinboxunselectedtabcolor [@"#D6DDE5" toColor]
#define kinboxselectedtabbordercolor [@"#828CA5" toColor]

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif

// ALog always displays output regardless of the DEBUG setting
#define ALog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)



@interface Constant : NSObject
extern NSString *globalString;

extern NSString *const kFontName;
extern CGFloat kFontSize;
extern NSString *const kGlobleURL;
extern NSString *const kInternetConnectiontitle;
extern NSString *const kInternetConnectionmessage;

@end
