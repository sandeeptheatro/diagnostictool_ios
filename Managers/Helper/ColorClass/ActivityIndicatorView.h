//
//  ActivityIndicatorView.h
//  ManagersMockup
//
//  Created by Ravi Shankar on 16/01/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityIndicatorView : UIView

+(UIView *)activityIndicatorView:(UIView *)view;
+(UIView *)alphaView:(UIView *)view;
+(void)activityIndicatorViewRemove:(UIView *)view;
+(UIView *)alphaView1:(UIView *)view;
+(void)activityIndicatorViewRemove1:(UIView *)view;

@end
