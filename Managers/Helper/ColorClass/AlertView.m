//
//  AlertView.m
//  Managers
//
//  Created by Ravi Shankar on 23/01/1937 SAKA.
//  Copyright (c) 1937 SAKA theatro. All rights reserved.
//

#import "AlertView.h"

@implementation AlertView

+(UIAlertView *)alert:(NSString *)title message:(NSString *)message
{
  NSDictionary  *dict =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:[dict objectForKey:@"OK"] otherButtonTitles:nil];
    [alert show];
    return alert;
}

@end
