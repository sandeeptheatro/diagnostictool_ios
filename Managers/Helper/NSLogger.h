//
//  NSLogger.h
//  Managers
//
//  Created by sandeepchalla on 7/28/15.
//  Copyright (c) 2015 theatro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSLogger : NSObject
{
    BOOL stdErrRedirected;
}
@property (nonatomic, assign) BOOL stdErrRedirected;

-(NSString *)getdata;
-(void) writeNSLogToFile;

+ (NSLogger *) sharedInstance;
@end
