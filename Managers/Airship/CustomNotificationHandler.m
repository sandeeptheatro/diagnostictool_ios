//
//  CustomNotificationHandler.m
//  Managers
//
//  Created by sandeepchalla on 6/10/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "CustomNotificationHandler.h"
#import "EarboxViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "SlideNavigationController.h"
#import "UIViewController.h"
#import "AGPushNoteView.h"
#import "StateMachineHandler.h"
#import "AppDelegate.h"
#import "StoreListViewController.h"

@implementation CustomNotificationHandler


-(void) receivedForegroundNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    NSLog(@"Received a notification while the app was already in the foreground: %@", userInfo);
    
    // Called when a push is received when the app is in the foreground
    // You can work with the notification object here
    
    // Be sure to call the completion handler with a UIBackgroundFetchResult
    if (userInfo)
    {
        [[NSUserDefaults standardUserDefaults]setObject:[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] forKey:@"badgecount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
       
        
        NSLog(@"delegate badge count:%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]);
                if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"EarboxViewController"])
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnotificationrecieved" object:nil];
                }
                else
                {
                    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    
                    [appDelegate requestUndeliveredMsgs];
                    [AGPushNoteView showWithNotificationMessage:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
                    if ([[userInfo objectForKey:@"callType"] isEqualToString:@"4"] || [[userInfo objectForKey:@"callType"] isEqualToString:@"17"])
                    {
                      
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"pushnotificationstorecount" object:@"msgincrement"];
                    }
                    else
                    {
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"pushnotificationstorecount" object:@"anncincrement"];
                    }
                     NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
                    NSString *alertMessage = [apsInfo objectForKey:@"alert"];
                    NSString *storeName=[alertMessage componentsSeparatedByString:@"/"][1];
                     [[NSUserDefaults standardUserDefaults] setObject:storeName forKey:@"multistoreNameString"];
                    
                }
                
                NSLog(@"Notification received by running app");
                
    }
    completionHandler(UIBackgroundFetchResultNoData);
    
}

-(void) launchedFromNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    NSLog(@"The application was launched or resumed from a notification: %@", userInfo);
    
    // Called when the app is launched/resumed by interacting with the notification
    // You can work with the notification object here
    
    // Be sure to call the completion handler with a UIBackgroundFetchResult
    
    
    if ( [[NSUserDefaults standardUserDefaults]objectForKey:@"loginstatus"])
    {
        if (userInfo) {
             [[NSUserDefaults standardUserDefaults]setObject:[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] forKey:@"badgecount"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSString *storeNameStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"storeNameString"];
            NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
            
            NSString *alertMessage = [apsInfo objectForKey:@"alert"];
            NSString *storeName=[alertMessage componentsSeparatedByString:@"/"][1];
            
            if ([storeNameStr isEqualToString:storeName])
            {
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                         bundle: nil];
                [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
                EarboxViewController *earboxVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"EarboxViewController"];
                
                NSString *chain=[alertMessage componentsSeparatedByString:@","][1];
                NSString *chainName=[[chain componentsSeparatedByString:@"/"][0]substringFromIndex:1];
                [[NSUserDefaults standardUserDefaults]setObject:storeName forKey:@"storeNameString"];
                NSDictionary *twmAppLogindic=[[NSUserDefaults standardUserDefaults]objectForKey:@"twmAppLogin"];
                NSMutableDictionary *updatedic=[[NSMutableDictionary alloc]init];
                [updatedic setObject:[twmAppLogindic objectForKey:@"companyId"] forKey:@"companyId"];
                [updatedic setObject:[twmAppLogindic objectForKey:@"firstTimeLogin"] forKey:@"firstTimeLogin"];
                [updatedic setObject:[twmAppLogindic objectForKey:@"tagOutName"] forKey:@"tagOutName"];
                [updatedic setObject:[twmAppLogindic objectForKey:@"userId"] forKey:@"userId"];
                [updatedic setObject:chainName forKey:@"chainName"];
                [[NSUserDefaults standardUserDefaults]setObject:updatedic forKey:@"twmAppLogin"];
                [NSThread detachNewThreadSelector:@selector(communicatorStart) toTarget:self withObject:nil];
                
                if ([NSStringFromClass([[UIViewController currentViewController]class]) isEqualToString:@"EarboxViewController"])
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnotificationrecieved" object:nil];
                }
                else
                {
                    [[SlideNavigationController sharedInstance] pushViewController:earboxVC animated:YES];
                }
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"pushForDiffStore"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] setObject:storeName forKey:@"StoreNameForPush"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
                [myQueue addOperationWithBlock:^{
                    [[StateMachineHandler sharedManager] killTheatroNative];
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    }];
                }];
                
                
            }
            
        }
        completionHandler(UIBackgroundFetchResultNoData);
    }
}

- (void)receivedBackgroundNotification:(NSDictionary *)userInfo actionIdentifier:(NSString *)identifier completionHandler:(void (^)())completionHandler {
    UA_LDEBUG(@"The application was started in the background from a user notification button");
   

    // Called when the app is launched/resumed by interacting with a notification button
    // You can work with the notification object here
    
    completionHandler();
}

-(void) receivedBackgroundNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    NSLog(@"A CONTENT_AVAILABLE notification was received with the app in the background: %@", userInfo);
    
}

-(void)communicatorStart
{
    // Communicator starts here
    [[StateMachineHandler sharedManager] communicator];
}

@end
