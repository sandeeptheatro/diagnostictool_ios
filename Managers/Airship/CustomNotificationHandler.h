//
//  CustomNotificationHandler.h
//  Managers
//
//  Created by sandeepchalla on 6/10/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UAPush.h"
#import "SlideNavigationController.h"


@interface CustomNotificationHandler : NSOperation<UAPushNotificationDelegate>
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;



@end
