//
//  Favorites+CoreDataProperties.h
//  Managers
//
//  Created by Ravi on 14/03/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import "Favorites+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Favorites (CoreDataProperties)

+ (NSFetchRequest<Favorites *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *favouriteID;
@property (nullable, nonatomic, copy) NSString *favouriteName;
@property (nullable, nonatomic, copy) NSString *favouriteType;
@property (nullable, nonatomic, copy) NSString *featureType;
@property (nullable, nonatomic, copy) NSString *finalView;
@property (nullable, nonatomic, retain) NSData *groups;
@property (nullable, nonatomic, copy) NSString *levelId;
@property (nullable, nonatomic, copy) NSString *messageType;
@property (nullable, nonatomic, copy) NSString *nodeId;
@property (nullable, nonatomic, copy) NSString *originator;
@property (nullable, nonatomic, copy) NSString *parentId;
@property (nullable, nonatomic, copy) NSString *platForm;
@property (nullable, nonatomic, copy) NSString *playAudioType;
@property (nullable, nonatomic, retain) NSData *stores;
@property (nullable, nonatomic, retain) NSData *storeNames;
@property (nullable, nonatomic, copy) NSString *storeScreen;
@property (nullable, nonatomic, copy) NSString *hQTagOutName;
@property (nullable, nonatomic, retain) NSData *hQManagerAppID;
@property (nullable, nonatomic, copy) NSString *multiStore;
@property (nullable, nonatomic, retain) NSData *multiStoreNodesNames;
@property (nullable, nonatomic, retain) NSData *multiStoreNodeId;
@property (nullable, nonatomic, retain) NSData *nodes;
@property (nullable, nonatomic, copy) NSString *levelName;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *storeselect;

@end

NS_ASSUME_NONNULL_END
