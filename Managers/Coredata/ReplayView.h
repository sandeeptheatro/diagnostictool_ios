//
//  ReplayView.h
//  Managers
//
//  Created by sandeepchalla on 3/15/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"

@interface ReplayView : UIView
{
    IBOutlet MarqueeLabel *messageLabel;
    IBOutlet UIButton *cancelbutton;
    BOOL audioSendButton;
    NSDictionary *dict;
}
@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
-(IBAction)cancelView;
- (IBAction)sendFileTapped:(id)sender;
- (IBAction)deleteFileTapped:(id)sender;
- (IBAction)playTapped:(id)sender;
-(void)load;
-(void)alphaView:(UIView *)view;

@end
