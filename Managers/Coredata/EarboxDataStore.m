//
//  EarboxDataStore.m
//  Managers
//
//  Created by sandeepchalla on 6/13/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "EarboxDataStore.h"
#import "AudioManager.h"

@implementation EarboxDataStore

// 5
- (id)init {
    if (self = [super init]) {
        [self initManagedObjectModel];
        [self initManagedObjectContext];
    }
    return self;
}

// 2
#pragma mark - static methods
+ (EarboxDataStore *)sharedStore {
    static EarboxDataStore *store;
    if (store == nil) {
        store = [[EarboxDataStore alloc] init];
    }
    return store;
}

#pragma mark - instance methods for adding/deleting managed objects

- (Favorites *)createFavorites
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"Favorites" inManagedObjectContext:self.context];
}

- (void)deleteFavorites:(Favorites *)favArtist
{
     NSLog(@"EarboxDataStore-favArtist deleted %@",favArtist);                 // Deleted.
    [self.context deleteObject:favArtist];
    NSError *error;
    [self save:error];
    if ([self.context respondsToSelector:@selector(existingObjectWithID:error:)])
    {
        NSManagedObjectID   *objectID           = [favArtist objectID];
        NSManagedObject     *managedObjectClone = [self.context existingObjectWithID:objectID error:NULL];
        
        if (managedObjectClone)
           
            NSLog(@"EarboxDataStore-favArtist not deleted %@",favArtist);                 // Not deleted.
    }
}

- (NSMutableArray *)favoritesArtists
{
    
    //[artist objectID]
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Favorites"];
    [fetchRequest setReturnsObjectsAsFaults:NO];
    NSError *error;
    
    NSArray *artists = [self.context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *array=[[NSMutableArray alloc]init];
    for(NSManagedObject *object in artists)
    {
        if (object.fault)
        {
            
        }
        else
        {
            [array addObject:object];
        }
    }
    
    if (error != nil) {
        [NSException raise:@"Exception on retrieving artists" format:@"Error: %@", error.localizedDescription];
    }
    return array;
}

//Message-groupmessage-announcement table creations.
- (EarboxInbox *)createEarboxInbox
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"EarboxInbox" inManagedObjectContext:self.context];
}

- (EarboxSaved *)createEarboxSaved
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"EarboxSaved" inManagedObjectContext:self.context];
}

- (EarboxSent *)createEarboxSent
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"EarboxSent" inManagedObjectContext:self.context];
}

- (AnnouncementInbox *)createAnnouncementInbox
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"AnnouncementInbox" inManagedObjectContext:self.context];
}

- (AnnouncementSaved *)createAnnouncementSaved
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"AnnouncementSaved" inManagedObjectContext:self.context];
}

- (AnnouncementSent *)createAnnouncementSent
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"AnnouncementSent" inManagedObjectContext:self.context];
}

//Update tables.
- (NSArray *)updateArtist:(NSString *)entityname callId:(NSString *)callId{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:entityname inManagedObjectContext:self.context]];
    
    NSError *error = nil;
    
    //check if results is empty r not before process code
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"callId == %@",callId];
    [request setPredicate:predicate];
    NSArray *results = [self.context executeFetchRequest:request error:&error];
    return results;
}

// Delete message/groupmessage inbox data.
- (void)deleteEarboxInbox:(EarboxInbox *)artist {
    NSLog(@"EarboxDataStore-Mssg inbox deleted %@",artist);                 // Deleted.
    [self.context deleteObject:artist];
    NSError *error;
    [self save:error];
    if ([self.context respondsToSelector:@selector(existingObjectWithID:error:)])
    {
        NSManagedObjectID   *objectID           = [artist objectID];
        NSManagedObject     *managedObjectClone = [self.context existingObjectWithID:objectID error:NULL];
        
        if (managedObjectClone)
            
            NSLog(@"EarboxDataStore-Mssg inbox not deleted DB operation failed %@",artist);                 // Not deleted.
    }
}

// Delete message/groupmessage saved data.
- (void)deleteEarboxSaved:(EarboxSaved *)artist {
     NSLog(@"EarboxDataStore-Mssg saved deleted %@",artist);                 // Deleted.
    [self.context deleteObject:artist];
    NSError *error;
    [self save:error];
    if ([self.context respondsToSelector:@selector(existingObjectWithID:error:)])
    {
        NSManagedObjectID   *objectID           = [artist objectID];
        NSManagedObject     *managedObjectClone = [self.context existingObjectWithID:objectID error:NULL];
        
        if (managedObjectClone)
           
            NSLog(@"EarboxDataStore-Mssg saved not deleted DB operation failed %@",artist);                 // Not deleted.
    }
}

// Delete message/groupmessage sent data.
- (void)deleteEarboxSent:(EarboxSent *)artist {
    NSLog(@"EarboxDataStore-Mssg sent deleted is %@",artist);                 // Deleted.
    [self.context deleteObject:artist];
    NSError *error;
    [self save:error];
    if ([self.context respondsToSelector:@selector(existingObjectWithID:error:)])
    {
        NSManagedObjectID   *objectID           = [artist objectID];
        NSManagedObject     *managedObjectClone = [self.context existingObjectWithID:objectID error:NULL];
        
        if (managedObjectClone)
            
            NSLog(@"EarboxDataStore-Mssg sent not deleted DB operation failed %@",artist);                 // Not deleted.
    }
}

// Delete announcement inbox data.
- (void)deleteAnnouncementInbox:(AnnouncementInbox *)artist {
    NSLog(@"EarboxDataStore-Ann inbox deleted %@",artist);                 // Deleted.
    [self.context deleteObject:artist];
    NSError *error;
    [self save:error];
    if ([self.context respondsToSelector:@selector(existingObjectWithID:error:)])
    {
        NSManagedObjectID   *objectID           = [artist objectID];
        NSManagedObject     *managedObjectClone = [self.context existingObjectWithID:objectID error:NULL];
        
        if (managedObjectClone)
            NSLog(@"EarboxDataStore-Ann inbox not deleted DB operation failed %@",artist);                 // Not deleted.
    }
}

// Delete announcement saved data.
- (void)deleteAnnouncementSaved:(AnnouncementSaved *)artist {
     NSLog(@"EarboxDataStore-Ann saved deleted %@",artist);                 // Deleted.
    [self.context deleteObject:artist];
    NSError *error;
    [self save:error];
    if ([self.context respondsToSelector:@selector(existingObjectWithID:error:)])
    {
        NSManagedObjectID   *objectID           = [artist objectID];
        NSManagedObject     *managedObjectClone = [self.context existingObjectWithID:objectID error:NULL];
        
        if (managedObjectClone)
           
            NSLog(@"EarboxDataStore-Ann saved not deleted DB operation failed %@",artist);                 // Not deleted.
    }
}

// Delete announcement sent data.
- (void)deleteAnnouncementSent:(AnnouncementSent *)artist {
    NSLog(@"EarboxDataStore-Ann sent deleted %@",artist);                 // Deleted.
    [self.context deleteObject:artist];
    NSError *error;
    [self save:error];
    if ([self.context respondsToSelector:@selector(existingObjectWithID:error:)])
    {
        NSManagedObjectID   *objectID           = [artist objectID];
        NSManagedObject     *managedObjectClone = [self.context existingObjectWithID:objectID error:NULL];
        
        if (managedObjectClone)
            
            NSLog(@"EarboxDataStore-Ann sent not deleted DB operation failed %@",artist);                 // Not deleted.
    }
}

//Delete all data from message/groupmessage/announcement
-(void)deleteAllEntities:(NSString *)nameEntity
{
    NSError *error;
    
    NSPersistentStoreCoordinator *psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    // NSPersistentStore * store = [[psc persistentStores] lastObject];
   /* NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
                                                                                              URL:[self storeArchivePath]
                                                                                            error:&error];
    NSManagedObjectModel *destinationModel = [psc managedObjectModel];
    BOOL pscCompatibile = [destinationModel isConfiguration:nil compatibleWithStoreMetadata:sourceMetadata];
    NSLog(@"Migration needed? %d", !pscCompatibile);*/
    //[[NSUserDefaults standardUserDefaults]setBool:pscCompatibile forKey:@"dbmigration"];
    NSPersistentStore *store = [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:[self storeArchivePath]options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
    //NSError *error;
    [psc removePersistentStore:store error:&error];
    [[NSFileManager defaultManager] removeItemAtURL:[store URL] error:&error];
    self.context = nil;
    psc = nil;
    NSLog(@"EarboxDataStore-Mangers %@ table deleted",nameEntity);
   // [self initManagedObjectModel];
    [self initManagedObjectContext];//Rebuild The CoreData Stack
}

//Fetch earboxveb data from tables.
- (NSMutableArray *)artists:(NSString *)earboxname storeid:(int)storeid calltype:(int)calltype {
    
    //[artist objectID]
    NSError *error;
    NSMutableArray *array=[[NSMutableArray alloc]init];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:earboxname];
    [fetchRequest setReturnsObjectsAsFaults:NO];
    if (storeid==0)
    {
        NSSortDescriptor * firstNameDescriptor;
        firstNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"status"
                                                          ascending:NO
                                                           selector:@selector(localizedCaseInsensitiveCompare:)];
        NSSortDescriptor * lastNameDescriptor;
        lastNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"callStartTime"
                                                         ascending:NO
                                                          selector:@selector(localizedCaseInsensitiveCompare:)];
        [fetchRequest setSortDescriptors:@[firstNameDescriptor, lastNameDescriptor]];
        NSError *error;
        
        NSArray *artists = [self.context executeFetchRequest:fetchRequest error:&error];
       
        for(NSManagedObject *object in artists)
        {
            if (object.fault)
            {
                
            }
            else
            {
                //[self audioDownloadRetry:earboxname object:object];
                [array addObject:object];
            }
        }
    }
    else
    {
        if (calltype==0)
        {
            [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"((storeid == %d) AND (maCallType == 1))",storeid]]];
            NSSortDescriptor * firstNameDescriptor;
            firstNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"status"
                                                              ascending:NO
                                                               selector:@selector(localizedCaseInsensitiveCompare:)];
            NSSortDescriptor * lastNameDescriptor;
            lastNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"callStartTime"
                                                             ascending:NO
                                                              selector:@selector(localizedCaseInsensitiveCompare:)];
            [fetchRequest setSortDescriptors:@[firstNameDescriptor, lastNameDescriptor]];
            [fetchRequest setReturnsObjectsAsFaults:NO];
            
            NSArray *artists = [self.context executeFetchRequest:fetchRequest error:&error];
            
            for(NSManagedObject *object in artists)
            {
                if (object.fault)
                {
                    
                }
                else
                {
                   // [self audioDownloadRetry:earboxname object:object];
                    [array addObject:object];
                }
            }
        }
        else
        {
            if (calltype==4)
            {
                 [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(storeid == %d) AND (callType == %d) AND (maCallType == 1) ",storeid,calltype]]];
                NSSortDescriptor * firstNameDescriptor;
                firstNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"status"
                                                                  ascending:NO
                                                                   selector:@selector(localizedCaseInsensitiveCompare:)];
                NSSortDescriptor * lastNameDescriptor;
                lastNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"callStartTime"
                                                                 ascending:NO
                                                                  selector:@selector(localizedCaseInsensitiveCompare:)];
                [fetchRequest setSortDescriptors:@[firstNameDescriptor, lastNameDescriptor]];
                [fetchRequest setReturnsObjectsAsFaults:NO];
                
                NSArray *artists = [self.context executeFetchRequest:fetchRequest error:&error];
                
                for(NSManagedObject *object in artists)
                {
                    if (object.fault)
                    {
                        
                    }
                    else
                    {
                        //[self audioDownloadRetry:earboxname object:object];
                        [array addObject:object];
                    }
                }
                [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(storeid == %d) AND (callType == %d) AND (maCallType == 1) ",storeid,33]]];
                NSSortDescriptor * firstNameDescriptor1;
                firstNameDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"status"
                                                                  ascending:NO
                                                                   selector:@selector(localizedCaseInsensitiveCompare:)];
                NSSortDescriptor * lastNameDescriptor1;
                lastNameDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"callStartTime"
                                                                 ascending:NO
                                                                  selector:@selector(localizedCaseInsensitiveCompare:)];
                [fetchRequest setSortDescriptors:@[firstNameDescriptor1, lastNameDescriptor1]];
                
                [fetchRequest setReturnsObjectsAsFaults:NO];
                NSArray *artists1 = [self.context executeFetchRequest:fetchRequest error:&error];
                
                for(NSManagedObject *object in artists1)
                {
                    if (object.fault)
                    {
                        
                    }
                    else
                    {
                       // [self audioDownloadRetry:earboxname object:object];
                        [array addObject:object];
                    }
                }
            }
            else
            {
            [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(storeid == %d) AND (callType == %d) AND (maCallType == 1) ",storeid,calltype]]];
                NSSortDescriptor * firstNameDescriptor;
                firstNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"status"
                                                                  ascending:NO
                                                                   selector:@selector(localizedCaseInsensitiveCompare:)];
                NSSortDescriptor * lastNameDescriptor;
                lastNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"callStartTime"
                                                                 ascending:NO
                                                                  selector:@selector(localizedCaseInsensitiveCompare:)];
                [fetchRequest setSortDescriptors:@[firstNameDescriptor, lastNameDescriptor]];
                [fetchRequest setReturnsObjectsAsFaults:NO];
                
                NSArray *artists = [self.context executeFetchRequest:fetchRequest error:&error];
                
                for(NSManagedObject *object in artists)
                {
                    if (object.fault)
                    {
                        
                    }
                    else
                    {
                        //[self audioDownloadRetry:earboxname object:object];
                        [array addObject:object];
                    }
                }
            }
        }
    }
    
    if (error != nil) {
        [NSException raise:@"Exception on retrieving artists" format:@"Error: %@", error.localizedDescription];
    }
    return array;
}

//Fetch inbox data from tables.
- (NSMutableArray *)inboxartists:(NSString *)earboxname calltype:(int)calltype
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:earboxname];
    [fetchRequest setReturnsObjectsAsFaults:NO];
    if (calltype==17 || calltype==108)
    {
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(callType == %d) OR (callType == %@)",calltype,[NSNumber numberWithInt:108]]]];
    }
    else if (calltype==4 || calltype==102 || calltype==104 || calltype==110)
    {
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(callType == %d) OR (callType == %@) OR (callType == %@) OR (callType == %@) OR (callType == %@)",calltype,[NSNumber numberWithInt:104],[NSNumber numberWithInt:102],[NSNumber numberWithInt:110],[NSNumber numberWithInt:4]]]];
    }
    else if (calltype==33)
    {
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(callType == %d)",calltype]]];
    }
    else
    {
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"callType == %d",calltype]]];
    }
    
    NSSortDescriptor * firstNameDescriptor;
    firstNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"status"
                                                      ascending:NO
                                                       selector:@selector(localizedCaseInsensitiveCompare:)];
    NSSortDescriptor * lastNameDescriptor;
    lastNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"callStartTime"
                                                     ascending:NO
                                                      selector:@selector(localizedCaseInsensitiveCompare:)];
    [fetchRequest setSortDescriptors:@[firstNameDescriptor, lastNameDescriptor]];
    NSError *error;
    
    NSArray *artists = [self.context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *array=[[NSMutableArray alloc]init];
    for(NSManagedObject *object in artists)
    {
        if (object.fault)
        {
            
        }
        else
        {
            //[self audioDownloadRetry:earboxname object:object];
            [array addObject:object];
        }
    }
    
    if (error != nil) {
        [NSException raise:@"Exception on retrieving artists" format:@"Error: %@", error.localizedDescription];
    }
    return array;
}

-(void)audioDownloadRetryCelltapped:(NSString *)earboxname object:(NSManagedObject*)object
{
    NSArray *arrayobject=[NSArray arrayWithObject:object];
    if ([earboxname isEqualToString:@"EarboxInbox"])
    {
        EarboxInbox *messageinboxdata=[arrayobject objectAtIndex:0];
        if (![messageinboxdata.downloadURL isKindOfClass:[NSNull class]])
        {
            
            if (messageinboxdata.storeid !=0)
            {
                [[AudioManager getsharedInstance]createEarboxAudioPathCellTapped:messageinboxdata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid] earboxName:@"EarboxInbox" callTxid:messageinboxdata.callId];
            }
            else
            {
                if (![messageinboxdata.managerAppId isKindOfClass:[NSNull class]] && ![messageinboxdata.tagOutName isKindOfClass:[NSNull class]])
                {
                    [[AudioManager getsharedInstance]createEarboxAudioPathCellTapped:messageinboxdata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",messageinboxdata.callId,messageinboxdata.callType,0] earboxName:@"EarboxInbox" callTxid:messageinboxdata.callId];
                }
                else
                {
                    [[AudioManager getsharedInstance]createEarboxAudioPathCellTapped:messageinboxdata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",messageinboxdata.callId,messageinboxdata.callType,[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]] earboxName:@"EarboxInbox" callTxid:messageinboxdata.callId];
                }
                
            }
        }
    }
    if ([earboxname isEqualToString:@"EarboxSaved"])
    {
        EarboxSaved *messagesaveddata=[arrayobject objectAtIndex:0];
        if (![messagesaveddata.downloadURL isKindOfClass:[NSNull class]])
        {
            
            if (messagesaveddata.storeid !=0)
            {
                [[AudioManager getsharedInstance]createEarboxAudioPathCellTapped:messagesaveddata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",messagesaveddata.callId,messagesaveddata.callType,messagesaveddata.storeid] earboxName:@"EarboxSaved" callTxid:messagesaveddata.callId];
            }
            else
            {
                if (![messagesaveddata.managerAppId isKindOfClass:[NSNull class]] && ![messagesaveddata.tagOutName isKindOfClass:[NSNull class]])
                {
                    [[AudioManager getsharedInstance]createEarboxAudioPathCellTapped:messagesaveddata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",messagesaveddata.callId,messagesaveddata.callType,0] earboxName:@"EarboxSaved" callTxid:messagesaveddata.callId];
                }
                else
                {
                    [[AudioManager getsharedInstance]createEarboxAudioPathCellTapped:messagesaveddata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",messagesaveddata.callId,messagesaveddata.callType,[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]] earboxName:@"EarboxSaved" callTxid:messagesaveddata.callId];
                }
                
            }
        }
    }
    if ([earboxname isEqualToString:@"EarboxSent"])
    {
        EarboxSent *messagesentdata=[arrayobject objectAtIndex:0];
        if (![messagesentdata.downloadURL isKindOfClass:[NSNull class]])
        {
            [[AudioManager getsharedInstance]createEarboxAudioPathCellTapped:messagesentdata.downloadURL callId:messagesentdata.callId earboxName:@"EarboxSent" callTxid:messagesentdata.callId];
            
        }
    }
    
    if ([earboxname isEqualToString:@"AnnouncementInbox"])
    {
        AnnouncementInbox *announcementinboxdata=[arrayobject objectAtIndex:0];
        if (![announcementinboxdata.downloadURL isKindOfClass:[NSNull class]])
        {
            if (announcementinboxdata.storeid!=0)
            {
                [[AudioManager getsharedInstance]createEarboxAudioPathCellTapped:announcementinboxdata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",announcementinboxdata.callId,announcementinboxdata.callType,announcementinboxdata.storeid] earboxName:@"AnnouncementInbox" callTxid:announcementinboxdata.callId];
            }
            else
            {
                [[AudioManager getsharedInstance]createEarboxAudioPathCellTapped:announcementinboxdata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",announcementinboxdata.callId,announcementinboxdata.callType,[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]] earboxName:@"AnnouncementInbox" callTxid:announcementinboxdata.callId];
            }
        }
    }
    
    if ([earboxname isEqualToString:@"AnnouncementSaved"])
    {
        AnnouncementSaved *announcementsaveddata=[arrayobject objectAtIndex:0];
        if (![announcementsaveddata.downloadURL isKindOfClass:[NSNull class]])
        {
            if (announcementsaveddata.storeid!=0)
            {
                [[AudioManager getsharedInstance]createEarboxAudioPathCellTapped:announcementsaveddata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",announcementsaveddata.callId,announcementsaveddata.callType,announcementsaveddata.storeid] earboxName:@"AnnouncementSaved" callTxid:announcementsaveddata.callId];
            }
            else
            {
                [[AudioManager getsharedInstance]createEarboxAudioPathCellTapped:announcementsaveddata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",announcementsaveddata.callId,announcementsaveddata.callType,[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]] earboxName:@"AnnouncementSaved" callTxid:announcementsaveddata.callId];
            }
        }
    }
    
    if ([earboxname isEqualToString:@"AnnouncementSent"])
    {
        AnnouncementSent *announcementsentdata=[arrayobject objectAtIndex:0];
        if (![announcementsentdata.downloadURL isKindOfClass:[NSNull class]])
        {
            [[AudioManager getsharedInstance]createEarboxAudioPathCellTapped:announcementsentdata.downloadURL callId:announcementsentdata.callId earboxName:@"AnnouncementSent" callTxid:announcementsentdata.callId];
        }
    }
}


-(void)audioDownloadRetry:(NSString *)earboxname object:(NSManagedObject*)object
{
        NSArray *arrayobject=[NSArray arrayWithObject:object];
        if ([earboxname isEqualToString:@"EarboxInbox"])
        {
            EarboxInbox *messageinboxdata=[arrayobject objectAtIndex:0];
            if (![messageinboxdata.downloadURL isKindOfClass:[NSNull class]])
            {
                
                if (messageinboxdata.storeid !=0)
                {
                    [[AudioManager getsharedInstance]createEarboxAudioPath:messageinboxdata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",messageinboxdata.callId,messageinboxdata.callType,messageinboxdata.storeid] earboxName:@"EarboxInbox" callTxid:messageinboxdata.callId];
                }
                else
                {
                    if (![messageinboxdata.managerAppId isKindOfClass:[NSNull class]] && ![messageinboxdata.tagOutName isKindOfClass:[NSNull class]])
                    {
                        [[AudioManager getsharedInstance]createEarboxAudioPath:messageinboxdata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",messageinboxdata.callId,messageinboxdata.callType,0] earboxName:@"EarboxInbox" callTxid:messageinboxdata.callId];
                    }
                    else
                    {
                        [[AudioManager getsharedInstance]createEarboxAudioPath:messageinboxdata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",messageinboxdata.callId,messageinboxdata.callType,[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]] earboxName:@"EarboxInbox" callTxid:messageinboxdata.callId];
                    }
                    
                }
            }
        }
        if ([earboxname isEqualToString:@"EarboxSaved"])
        {
            EarboxSaved *messagesaveddata=[arrayobject objectAtIndex:0];
            if (![messagesaveddata.downloadURL isKindOfClass:[NSNull class]])
            {
                
                if (messagesaveddata.storeid !=0)
                {
                    [[AudioManager getsharedInstance]createEarboxAudioPath:messagesaveddata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",messagesaveddata.callId,messagesaveddata.callType,messagesaveddata.storeid] earboxName:@"EarboxSaved" callTxid:messagesaveddata.callId];
                }
                else
                {
                    if (![messagesaveddata.managerAppId isKindOfClass:[NSNull class]] && ![messagesaveddata.tagOutName isKindOfClass:[NSNull class]])
                    {
                        [[AudioManager getsharedInstance]createEarboxAudioPath:messagesaveddata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",messagesaveddata.callId,messagesaveddata.callType,0] earboxName:@"EarboxSaved" callTxid:messagesaveddata.callId];
                    }
                    else
                    {
                        [[AudioManager getsharedInstance]createEarboxAudioPath:messagesaveddata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",messagesaveddata.callId,messagesaveddata.callType,[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]] earboxName:@"EarboxSaved" callTxid:messagesaveddata.callId];
                    }
                    
                }
            }
        }
        if ([earboxname isEqualToString:@"EarboxSent"])
        {
            EarboxSent *messagesentdata=[arrayobject objectAtIndex:0];
            if (![messagesentdata.downloadURL isKindOfClass:[NSNull class]])
            {
                [[AudioManager getsharedInstance]createEarboxAudioPath:messagesentdata.downloadURL callId:messagesentdata.callId earboxName:@"EarboxSent" callTxid:messagesentdata.callId];
                
            }
        }
    
    if ([earboxname isEqualToString:@"AnnouncementInbox"])
    {
        AnnouncementInbox *announcementinboxdata=[arrayobject objectAtIndex:0];
        if (![announcementinboxdata.downloadURL isKindOfClass:[NSNull class]])
        {
            if (announcementinboxdata.storeid!=0)
            {
                [[AudioManager getsharedInstance]createEarboxAudioPath:announcementinboxdata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",announcementinboxdata.callId,announcementinboxdata.callType,announcementinboxdata.storeid] earboxName:@"AnnouncementInbox" callTxid:announcementinboxdata.callId];
            }
            else
            {
                [[AudioManager getsharedInstance]createEarboxAudioPath:announcementinboxdata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",announcementinboxdata.callId,announcementinboxdata.callType,[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]] earboxName:@"AnnouncementInbox" callTxid:announcementinboxdata.callId];
            }
        }
    }
    
    if ([earboxname isEqualToString:@"AnnouncementSaved"])
    {
        AnnouncementSaved *announcementsaveddata=[arrayobject objectAtIndex:0];
        if (![announcementsaveddata.downloadURL isKindOfClass:[NSNull class]])
        {
            if (announcementsaveddata.storeid!=0)
            {
                [[AudioManager getsharedInstance]createEarboxAudioPath:announcementsaveddata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",announcementsaveddata.callId,announcementsaveddata.callType,announcementsaveddata.storeid] earboxName:@"AnnouncementSaved" callTxid:announcementsaveddata.callId];
            }
            else
            {
                [[AudioManager getsharedInstance]createEarboxAudioPath:announcementsaveddata.downloadURL callId:[NSString stringWithFormat:@"%@%d%d",announcementsaveddata.callId,announcementsaveddata.callType,[[[NSUserDefaults standardUserDefaults]objectForKey:@"storeid"] intValue]] earboxName:@"AnnouncementSaved" callTxid:announcementsaveddata.callId];
            }
        }
    }
    
    if ([earboxname isEqualToString:@"AnnouncementSent"])
    {
        AnnouncementSent *announcementsentdata=[arrayobject objectAtIndex:0];
        if (![announcementsentdata.downloadURL isKindOfClass:[NSNull class]])
        {
            [[AudioManager getsharedInstance]createEarboxAudioPath:announcementsentdata.downloadURL callId:announcementsentdata.callId earboxName:@"AnnouncementSent" callTxid:announcementsentdata.callId];
        }
    }
}

// 9
- (void)save:(NSError *)error {
    
    if(![self.context save:&error]) {
        NSLog(@"EarboxDataStore-Failed to save to data store: %@", [error localizedDescription]);
        NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
        if(detailedErrors != nil && [detailedErrors count] > 0) {
            for(NSError* detailedError in detailedErrors) {
                NSLog(@"EarboxDataStore-Coredata DB DetailedError: %@", [detailedError userInfo]);
            }
        }
        else {
            NSLog(@"EarboxDataStore-Coredata DB %@", [error userInfo]);
        }
    }
    if ([self.context save:&error] == NO) {
        NSAssert(NO, @"Coredata DB Save should not fail\n%@", [error localizedDescription]);
        abort();
    }
}

#pragma mark - private methods
// 3
- (void)initManagedObjectModel {
    NSURL *momdURL = [[NSBundle mainBundle] URLForResource:@"Earbox" withExtension:@"momd"];
    self.model = [[NSManagedObjectModel alloc] initWithContentsOfURL:momdURL];
}
// 4
- (void)initManagedObjectContext {
    NSPersistentStoreCoordinator *psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    
    NSError *error;
    
    //*****  option nil is replaces with this code @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
    
   /* // Determine if a migration is needed
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
                                                                                              URL:[self storeArchivePath]
                                                                                            error:&error];
    NSManagedObjectModel *destinationModel = [psc managedObjectModel];
    BOOL pscCompatibile = [destinationModel isConfiguration:nil compatibleWithStoreMetadata:sourceMetadata];
    NSLog(@"Migration needed? %d", !pscCompatibile);
    [[NSUserDefaults standardUserDefaults]setBool:pscCompatibile forKey:@"dbmigration"];*/
    NSPersistentStore *store = [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:[self storeArchivePath]options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
    if (store == nil || error != nil) {
        [NSException raise:@"Exception Occurred while adding persistence store" format:@"Error: %@", error.localizedDescription];
    }
    self.context = [[NSManagedObjectContext alloc] init];
    [self.context setPersistentStoreCoordinator:psc];
}
- (NSURL *)storeArchivePath {
    // Determine if a migration is needed
    NSArray *urls = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    return [[urls firstObject] URLByAppendingPathComponent:@"Earbox.sqlite"];
   
}


@end
