//
//  Favorites+CoreDataClass.h
//  Managers
//
//  Created by Ravi on 14/03/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Favorites : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Favorites+CoreDataProperties.h"
