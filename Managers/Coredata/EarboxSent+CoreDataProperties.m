//
//  EarboxSent+CoreDataProperties.m
//  Managers
//
//  Created by sandeepchalla on 6/22/16.
//  Copyright © 2016 theatro. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EarboxSent+CoreDataProperties.h"

@implementation EarboxSent (CoreDataProperties)

@dynamic callId;
@dynamic callStartTime;
@dynamic callType;
@dynamic details;
@dynamic downloadURL;
@dynamic duration;
@dynamic from;
@dynamic info;
@dynamic priority;
@dynamic status;
@dynamic ttl;
@dynamic liveStatus;
@dynamic to;
@dynamic reply;
@dynamic storeid;
@dynamic pushTxId;
@dynamic storeName;
@dynamic groupName;
@dynamic mode;
@dynamic groupId;
@dynamic storeNames;
@dynamic groupNames;
@dynamic maCallType;
@dynamic storeIds;
@dynamic groupIds;
@dynamic levelName;
@dynamic nodeName;
@dynamic nodeId;
@dynamic tagOutName;
@dynamic managerAppId;
@end
