//
//  GroupSaved+CoreDataClass.h
//  Managers
//
//  Created by sandeepchalla on 11/28/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupSaved : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "GroupSaved+CoreDataProperties.h"
