//
//  ReplayView.m
//  Managers
//
//  Created by sandeepchalla on 3/15/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import "ReplayView.h"
#import "Constant.h"
#import "AudioManager.h"
#import "ActivityIndicatorView.h"

@implementation ReplayView
@synthesize deleteButton, playButton, recordPauseButton,sendButton;

-(void)load
{
    dict =[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    
    NSLog(@"");
    
    messageLabel.marqueeType = MLContinuous;
    messageLabel.scrollDuration = 8.0;
    messageLabel.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    messageLabel.fadeLength = 0.0f;
    messageLabel.leadingBuffer = 0.0f;
    messageLabel.trailingBuffer = 10.0f;
    
    [deleteButton setTitle:[dict objectForKey:@"Delete"] forState:UIControlStateNormal];
    [playButton setTitle:[dict objectForKey:@"Replay"] forState:UIControlStateNormal];
    [sendButton setTitle:[dict objectForKey:@"Send"] forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(audiolengthshort:)
                                                 name:@"audiolengthshort"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playAnnouncement:)
                                                 name:@"playAnnouncementsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(replayNotification:)
                                                 name:@"replayNotification"
                                               object:nil];
    cancelbutton.layer.cornerRadius=8;
     //cancelbutton.backgroundColor=[UIColor redColor];
    cancelbutton.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor blackColor]);
    playButton.layer.cornerRadius=8;
    deleteButton.layer.cornerRadius=8;
    sendButton.layer.cornerRadius=8;
    playButton.backgroundColor=kGreyColor;
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    [self buttonsDisable];
    [recordPauseButton addTarget:self action:@selector(buttondDownPressed:) forControlEvents: UIControlEventTouchDown];
    [recordPauseButton addTarget:self action:@selector(buttonUpPressed:) forControlEvents: UIControlEventTouchUpInside];
    [recordPauseButton addTarget:self action:@selector(buttonDragOutside:) forControlEvents: UIControlEventTouchDragOutside];
    [[AudioManager getsharedInstance] path];
}

- (void)buttondDownPressed:(UIButton *)sender
{
    [self buttonsDisable];
    deleteButton.backgroundColor=kGreyColor;
    sendButton.backgroundColor=kGreyColor;
    playButton.backgroundColor=kGreyColor;
    messageLabel.text=[dict objectForKey:@"Recording..."];
    [[AudioManager getsharedInstance] recordPauseTapped];
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue_white"] forState:UIControlStateNormal];
}



- (void)buttonUpPressed:(UIButton *)sender {
    [playButton setTitle:[dict objectForKey:@"Replay"] forState:UIControlStateNormal];
    messageLabel.text=[dict objectForKey:@"Ready to send"];
    audioSendButton=NO;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    [playButton setEnabled:YES];
    [deleteButton setEnabled:YES];
    [sendButton setEnabled:YES];
    deleteButton.backgroundColor=kNavBackgroundColor;
    sendButton.backgroundColor=kNavBackgroundColor;
    playButton.backgroundColor=kNavBackgroundColor;
    [[AudioManager getsharedInstance] stopTapped];
}

-(void)buttonDragOutside:(UIButton *)sender
{
    messageLabel.text=[dict objectForKey:@"Ready to send"];
    audioSendButton=NO;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    [playButton setEnabled:YES];
    [deleteButton setEnabled:YES];
    [sendButton setEnabled:YES];
    deleteButton.backgroundColor=kNavBackgroundColor;
    sendButton.backgroundColor=kNavBackgroundColor;
    playButton.backgroundColor=kNavBackgroundColor;
    [[AudioManager getsharedInstance] stopTapped];
}

- (IBAction)playTapped:(UIButton *)sender {
    if ([playButton.titleLabel.text isEqualToString:@"Stop"] || [playButton.titleLabel.text isEqualToString:@"Arrêter"])//Arrêter
    {
            if (audioSendButton==YES)
            {
                [deleteButton setEnabled:YES];
                [sendButton setEnabled:NO];
                deleteButton.backgroundColor=kNavBackgroundColor;
                sendButton.backgroundColor=kGreyColor;
                messageLabel.text=@"";
                [[AudioManager getsharedInstance]audioplayerstop];
                [playButton setTitle:[dict objectForKey:@"Replay"] forState:UIControlStateNormal];
            }
            else
            {
                [deleteButton setEnabled:YES];
                [sendButton setEnabled:YES];
                deleteButton.backgroundColor=kNavBackgroundColor;
                sendButton.backgroundColor=kNavBackgroundColor;
                messageLabel.text=@"";
                [[AudioManager getsharedInstance]audioplayerstop];
                [playButton setTitle:[dict objectForKey:@"Replay"] forState:UIControlStateNormal];
            }
    }
    else
    {
        [playButton setTitle:[dict objectForKey:@"Stop"] forState:UIControlStateNormal];
        [deleteButton setEnabled:NO];
        [sendButton setEnabled:NO];
        deleteButton.backgroundColor=kGreyColor;
        sendButton.backgroundColor=kGreyColor;
        messageLabel.text=[dict objectForKey:@"Replaying..."];
        [[AudioManager getsharedInstance] playTapped];
    }
}


- (IBAction)sendFileTapped:(id)sender
{
    
    [deleteButton setEnabled:NO];
    [sendButton setEnabled:NO];
    [playButton setEnabled:NO];
    playButton.backgroundColor=[UIColor grayColor];
    deleteButton.backgroundColor=[UIColor grayColor];
    sendButton.backgroundColor=[UIColor grayColor];
    recordPauseButton.userInteractionEnabled=NO;
    messageLabel.text=[dict objectForKey:@"Sending..."];
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_gray"] forState:UIControlStateNormal];
    [[AudioManager getsharedInstance] sendTapped];
    
}

- (IBAction)deleteFileTapped:(id)sender
{
    messageLabel.text=[dict objectForKey:@"Message deleted"];
    deleteButton.backgroundColor=[UIColor grayColor];
    sendButton.backgroundColor=[UIColor grayColor];
    playButton.backgroundColor=[UIColor grayColor];
    [sendButton setEnabled:NO];
    [playButton setEnabled:NO];
    [deleteButton setEnabled:NO];
    [[AudioManager getsharedInstance] deleteTapped];
}

-(void)playAnnouncement:(NSNotification *)sender
{
    [playButton setTitle:[dict objectForKey:@"Replay"] forState:UIControlStateNormal];
    messageLabel.text=@"";
    deleteButton.backgroundColor=kNavBackgroundColor;
    [deleteButton setEnabled:YES];
    
    if (audioSendButton==YES)
    {
        sendButton.backgroundColor=kGreyColor;
        [sendButton setEnabled:NO];
        
    }
    else
    {
        messageLabel.text=[dict objectForKey:@"Ready to send"];
        sendButton.backgroundColor=kNavBackgroundColor;
        [sendButton setEnabled:YES];
        
    }
    
}

-(void)buttonsDisable
{
    if (playButton.isEnabled)
    {
        [playButton setEnabled:NO];
    }
    if (deleteButton.isEnabled)
    {
        [deleteButton setEnabled:NO];
    }
    if (sendButton.isEnabled)
    {
        [sendButton setEnabled:NO];
    }
}

-(void)replayNotification:(NSNotification *)sender
{
    recordPauseButton.userInteractionEnabled=YES;
    [playButton setEnabled:YES];
    [deleteButton setEnabled:YES];
    playButton.backgroundColor=kNavBackgroundColor;
    deleteButton.backgroundColor=kNavBackgroundColor;
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
    
    if ([[sender.object objectAtIndex:0] isEqualToString:@"error"])
    {
        messageLabel.text=[dict objectForKey:@"Message not sent"];
        sendButton.backgroundColor=kNavBackgroundColor;
        [sendButton setEnabled:YES];
        
    }
    else if ([[sender.object objectAtIndex:0] isEqualToString:@"message"])
    {
        audioSendButton=YES;
        messageLabel.text=[dict objectForKey:@"Message Sent"];
        sendButton.backgroundColor=kGreyColor;
        
        [sendButton setEnabled:NO];
        
    }
}

-(void)audiolengthshort:(NSNotification *)sender
{
    [deleteButton setEnabled:NO];
    [sendButton setEnabled:NO];
    [playButton setEnabled:NO];
    playButton.backgroundColor=[UIColor grayColor];
    deleteButton.backgroundColor=[UIColor grayColor];
    sendButton.backgroundColor=[UIColor grayColor];
    recordPauseButton.userInteractionEnabled=YES;
    messageLabel.text=@"";
    [recordPauseButton setImage:[UIImage imageNamed:@"mike_blue"] forState:UIControlStateNormal];
}

-(void)alphaView:(UIView *)view
{
    [view addSubview: [ActivityIndicatorView alphaView1:view]];

}

-(IBAction)cancelView
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"audiolengthshort" object:nil];
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAnnouncementsNotification" object:nil];
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"replayNotification" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"replayCancelNotification" object:nil];
    [[AudioManager getsharedInstance]audioplayerstop];
}

@end
