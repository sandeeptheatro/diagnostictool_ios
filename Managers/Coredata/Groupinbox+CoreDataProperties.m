//
//  Groupinbox+CoreDataProperties.m
//  Managers
//
//  Created by sandeepchalla on 11/28/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "Groupinbox+CoreDataProperties.h"

@implementation Groupinbox (CoreDataProperties)

+ (NSFetchRequest<Groupinbox *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Groupinbox"];
}

@dynamic callId;
@dynamic callStartTime;
@dynamic callType;
@dynamic details;
@dynamic downloadURL;
@dynamic duration;
@dynamic from;
@dynamic info;
@dynamic priority;
@dynamic liveStatus;
@dynamic pushTxId;
@dynamic reply;
@dynamic status;
@dynamic storeid;
@dynamic ttl;
@dynamic storeName;
@dynamic groupName;
@dynamic mode;
@end
