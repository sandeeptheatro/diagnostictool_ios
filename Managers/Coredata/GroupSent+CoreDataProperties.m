//
//  GroupSent+CoreDataProperties.m
//  Managers
//
//  Created by sandeepchalla on 11/28/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "GroupSent+CoreDataProperties.h"

@implementation GroupSent (CoreDataProperties)

+ (NSFetchRequest<GroupSent *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"GroupSent"];
}

@dynamic callId;
@dynamic callStartTime;
@dynamic callType;
@dynamic details;
@dynamic downloadURL;
@dynamic from;
@dynamic duration;
@dynamic info;
@dynamic liveStatus;
@dynamic priority;
@dynamic pushTxId;
@dynamic reply;
@dynamic status;
@dynamic storeid;
@dynamic ttl;
@dynamic to;
@dynamic storeName;
@dynamic groupName;
@dynamic mode;

@end
