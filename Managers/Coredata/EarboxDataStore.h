//
//  EarboxDataStore.h
//  Managers
//
//  Created by sandeepchalla on 6/13/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "EarboxInbox+CoreDataProperties.h"
#import "EarboxSaved+CoreDataProperties.h"
#import "EarboxSent+CoreDataProperties.h"
#import "AnnouncementInbox+CoreDataProperties.h"
#import "AnnouncementSent+CoreDataProperties.h"
#import "AnnouncementSaved+CoreDataProperties.h"
#import "Favorites+CoreDataProperties.h"

@interface EarboxDataStore : NSObject


- (Favorites *)createFavorites;
- (void)deleteFavorites:(Favorites *)favArtist;
- (NSMutableArray *)favoritesArtists;

+ (EarboxDataStore *)sharedStore;
- (EarboxInbox *)createEarboxInbox;
- (EarboxSaved *)createEarboxSaved;
- (EarboxSent *)createEarboxSent;
- (AnnouncementInbox *)createAnnouncementInbox;
- (AnnouncementSaved *)createAnnouncementSaved;
- (AnnouncementSent *)createAnnouncementSent;
- (NSArray *)updateArtist:(NSString *)entityname callId:(NSString *)callId;
- (void)deleteEarboxInbox:(EarboxInbox *)artist;
- (void)deleteEarboxSaved:(EarboxSaved *)artist;
- (void)deleteEarboxSent:(EarboxSent *)artist;
- (void)deleteAnnouncementInbox:(AnnouncementInbox *)artist;
- (void)deleteAnnouncementSaved:(AnnouncementSaved *)artist;
- (void)deleteAnnouncementSent:(AnnouncementSent *)artist;
-(void)deleteAllEntities:(NSString *)nameEntity;
- (NSMutableArray *)artists:(NSString *)earboxname storeid:(int)storeid calltype:(int)calltype;
- (NSMutableArray *)inboxartists:(NSString *)earboxname calltype:(int)calltype;
- (void)save:(NSError *)error;
-(void)audioDownloadRetry:(NSString *)earboxname object:(NSManagedObject*)object;
-(void)audioDownloadRetryCelltapped:(NSString *)earboxname object:(NSManagedObject*)object;
@property (nonatomic, strong) NSManagedObjectModel *model;
@property (nonatomic, strong) NSManagedObjectContext *context;

@end
