//
//  AnnouncementInbox+CoreDataProperties.h
//  Managers
//
//  Created by sandeepchalla on 6/22/16.
//  Copyright © 2016 theatro. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AnnouncementInbox.h"

NS_ASSUME_NONNULL_BEGIN

@interface AnnouncementInbox (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *callId;
@property (nonatomic, assign) double callStartTime;
@property (nonatomic, assign) int callType;
@property (nullable, nonatomic, retain) NSString *from;
@property (nullable, nonatomic, retain) NSString *info;
@property (nonatomic, assign) double ttl;
@property (nullable, nonatomic, retain) NSString *downloadURL;
@property (nullable, nonatomic, retain) NSString *status;
@property (nullable, nonatomic, retain) NSString *priority;
@property (nullable, nonatomic, retain) NSString *details;
@property (nonatomic, assign) double duration;
@property (nullable, nonatomic, retain) NSString *liveStatus;
@property (nullable, nonatomic, retain) NSString *reply;
@property ( nonatomic, assign) int storeid;
@property (nullable, nonatomic, retain) NSString *pushTxId;
@property (nullable, nonatomic, retain) NSString *storeName;
@property (nullable, nonatomic, retain) NSString *groupName;
@property (nullable, nonatomic, retain) NSString *mode;
@property (nullable, nonatomic, retain) NSString *storeNames;
@property (nullable, nonatomic, retain) NSString *groupNames;
@property (nonatomic, assign) int maCallType;
@property (nullable, nonatomic, retain) NSString *storeIds;
@property (nullable, nonatomic, retain) NSString *groupIds;
@property (nullable, nonatomic, retain) NSString *levelName;
@property (nullable, nonatomic, retain) NSString *nodeId;
@property (nullable, nonatomic, retain) NSString *nodeName;
@end

NS_ASSUME_NONNULL_END
