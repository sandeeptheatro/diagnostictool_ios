//
//  EarboxInbox+CoreDataProperties.m
//  Managers
//
//  Created by sandeepchalla on 6/13/16.
//  Copyright © 2016 theatro. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EarboxInbox+CoreDataProperties.h"

@implementation EarboxInbox (CoreDataProperties)

@dynamic callId;
@dynamic callStartTime;
@dynamic callType;
@dynamic from;
@dynamic info;
@dynamic ttl;
@dynamic downloadURL;
@dynamic status;
@dynamic priority;
@dynamic details;
@dynamic duration;
@dynamic liveStatus;
@dynamic reply;
@dynamic storeid;
@dynamic pushTxId;
@dynamic storeName;
@dynamic groupName;
@dynamic mode;
@dynamic storeNames;
@dynamic groupNames;
@dynamic maCallType;
@dynamic storeIds;
@dynamic groupIds;
@dynamic levelName;
@dynamic nodeName;
@dynamic nodeId;
@dynamic tagOutName;
@dynamic managerAppId;
@dynamic to;
@end
