//
//  Groupinbox+CoreDataProperties.h
//  Managers
//
//  Created by sandeepchalla on 11/28/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "Groupinbox+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Groupinbox (CoreDataProperties)

+ (NSFetchRequest<Groupinbox *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *callId;
@property ( nonatomic, assign) double callStartTime;
@property ( nonatomic, assign) int callType;
@property (nullable, nonatomic, copy) NSString *details;
@property (nullable, nonatomic, copy) NSString *downloadURL;
@property ( nonatomic, assign) double duration;
@property (nullable, nonatomic, copy) NSString *from;
@property (nullable, nonatomic, copy) NSString *info;
@property (nullable, nonatomic, copy) NSString *priority;
@property (nullable, nonatomic, copy) NSString *liveStatus;
@property (nullable, nonatomic, copy) NSString *pushTxId;
@property (nullable, nonatomic, copy) NSString *reply;
@property (nullable, nonatomic, copy) NSString *status;
@property ( nonatomic, assign) int storeid;
@property ( nonatomic, assign) double ttl;
@property (nullable, nonatomic, retain) NSString *storeName;
@property (nullable, nonatomic, retain) NSString *groupName;
@property (nullable, nonatomic, retain) NSString *mode;

@end

NS_ASSUME_NONNULL_END
