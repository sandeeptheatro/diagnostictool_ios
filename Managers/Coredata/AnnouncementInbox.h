//
//  AnnouncementInbox.h
//  Managers
//
//  Created by sandeepchalla on 6/22/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface AnnouncementInbox : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "AnnouncementInbox+CoreDataProperties.h"
