//
//  EarboxInbox.h
//  Managers
//
//  Created by sandeepchalla on 6/13/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface EarboxInbox : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "EarboxInbox+CoreDataProperties.h"
