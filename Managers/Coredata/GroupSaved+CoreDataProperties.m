//
//  GroupSaved+CoreDataProperties.m
//  Managers
//
//  Created by sandeepchalla on 11/28/16.
//  Copyright © 2016 theatro. All rights reserved.
//

#import "GroupSaved+CoreDataProperties.h"

@implementation GroupSaved (CoreDataProperties)

+ (NSFetchRequest<GroupSaved *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"GroupSaved"];
}

@dynamic callId;
@dynamic callStartTime;
@dynamic callType;
@dynamic details;
@dynamic downloadURL;
@dynamic from;
@dynamic duration;
@dynamic info;
@dynamic liveStatus;
@dynamic priority;
@dynamic pushTxId;
@dynamic reply;
@dynamic status;
@dynamic storeid;
@dynamic ttl;
@dynamic storeName;
@dynamic groupName;
@dynamic mode;
@end
