//
//  Favorites+CoreDataProperties.m
//  Managers
//
//  Created by Ravi on 14/03/17.
//  Copyright © 2017 theatro. All rights reserved.
//

#import "Favorites+CoreDataProperties.h"

@implementation Favorites (CoreDataProperties)

+ (NSFetchRequest<Favorites *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Favorites"];
}

@dynamic favouriteID;
@dynamic favouriteName;
@dynamic favouriteType;
@dynamic featureType;
@dynamic finalView;
@dynamic groups;
@dynamic levelId;
@dynamic messageType;
@dynamic nodeId;
@dynamic originator;
@dynamic parentId;
@dynamic platForm;
@dynamic playAudioType;
@dynamic stores;
@dynamic storeScreen;
@dynamic storeNames;
@dynamic levelName;
@dynamic nodes;
@dynamic hQTagOutName;
@dynamic hQManagerAppID;
@dynamic multiStoreNodeId;
@dynamic multiStore;
@dynamic multiStoreNodesNames;
@dynamic storeselect;
@dynamic title;

@end
