/*
 * TheatroCommunicator.h
 *
 *  Created on: May 6, 2013
 *      Author: vincente
 *  Abhinandan Chougala : Ported to android.
 *
 */


#ifndef THEATROCOMMUNICATOR_H_
#define THEATROCOMMUNICATOR_H_

#include "TheatroDefs.h"
#include <pthread.h>
#include "cJSON.h"
#include<stdbool.h>
#include "Callbacks.h"
#include <sys/time.h>
#include <unistd.h>

#define MAX_CAPTURE_BUFFER_SIZE   262136
#define MAX_PLAYBACK_BUFFER_SIZE 2000000



#ifdef __cplusplus
extern "C" {
#endif

#define TICKWAIT	  25
#define	QUADTICK	 100
#define	HALFSECOND	 500
#define	ONESECOND	1000

#define NELEM(a)  (sizeof(a) / sizeof(a)[0])

#define QUOTEME(x) #x

#define	initDefaultString(STR, SIZE) \
   memset(STR, 0, SIZE); \
if (DefaultsObject) strncpy(STR, cJSON_GetObjectItem(DefaultsObject, QUOTEME(STR)) -> valuestring, SIZE); \
if (DefaultsFileObject) if (cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(STR))) strncpy(STR, cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(STR)) -> valuestring, SIZE);

#define	initDefaultInt(INT) \
   if (DefaultsObject) INT = cJSON_GetObjectItem(DefaultsObject, QUOTEME(INT)) -> valueint; \
if (DefaultsFileObject) if (cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(INT))) INT = cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(INT)) -> valueint;

#define	initDefaultNumber(NUM) \
   if (DefaultsObject) NUM = cJSON_GetObjectItem(DefaultsObject, QUOTEME(NUM)) -> valuedouble; \
if (DefaultsFileObject) if (cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(NUM))) NUM = cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(NUM)) -> valuedouble;

#define	initDefaultBool(BOOL) \
   if (DefaultsObject) BOOL = (bool) cJSON_GetObjectItem(DefaultsObject, QUOTEME(BOOL)) -> valueint; \
if (DefaultsFileObject) if (cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(BOOL))) BOOL = (bool) cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(BOOL)) -> valueint;

#define	getJSONInt(JSONOBJECT, INT) \
   if (JSONOBJECT) if (cJSON_GetObjectItem(JSONOBJECT, QUOTEME(INT))) INT = cJSON_GetObjectItem(JSONOBJECT, QUOTEME(INT)) -> valueint;

#define	getJSONString(JSONOBJECT, STR, SIZE) \
   memset(STR, 0, SIZE); \
if (JSONOBJECT) if (cJSON_GetObjectItem(JSONOBJECT, QUOTEME(STR))) strncpy(STR, cJSON_GetObjectItem(JSONOBJECT, QUOTEME(STR)) -> valuestring, SIZE);

#define	getJSONBool(JSONOBJECT, BOOL) \
   if (JSONOBJECT) if (cJSON_GetObjectItem(JSONOBJECT, QUOTEME(BOOL))) BOOL = (bool) cJSON_GetObjectItem(JSONOBJECT, QUOTEME(BOOL)) -> valueint;


extern pthread_mutex_t mutex;	 // Mutex for updating globals shared between C threads and Java.

extern char TGSIPAddress[NAMESIZE];
extern char TgsIp[NAMESIZE];
    extern char Dest[NAMESIZE];
extern bool isCancelledFromJava;
extern bool isNetworkSwitchFromApp;
extern int connection_flag;
extern int destroy_flag;
    

extern bool isCommunicatorAlive;
//extern bool isIpSetFromJava;
void performBroadcastButtonAction(bool isDown);
void performBroadcastScreenAction(bool isBroadcast);
void performPrimaryButtonAction(bool isDown);
void performVolumeUpButtonAction(bool isDown);
void performVolumeDownButtonAction(bool isDown);
void performChargingAction(bool isDown);
void performChargedAction(bool isDown);

void performCommAction(char * buttonTxt , bool isDown);
int buildJSONButtonPacket(char *buf, char *button_text, bool button_state);
void performLoginButtonAction(char *LoginName);
void performRefreshButtonAction();
void performButtonAction(char *type);
void startCommunicator();
void exitCommunicator();
void stopPlayBackFromJava(bool isStopped);
void performPlayBufferAction(bool value);
void performHuddleAction(char* time, int type);
void performMsgButtonAction(char *type, char* txnId);
    
void stausCallback(char *);
unsigned int performRingBufferRead(void *buffer, unsigned int size);
void performRingBufferWrite(void *buffer, unsigned int size);

void startRecording();
void stopRecording();
void startPlayback();
void stopPlayback();
    
int getRecordBuffer(signed short *buf, int frames_to_deliver);
    
int playBuffer(short *buf, int length);
    
int getAvailableRecordSpace();
int getAvailablePlaybackData();
    
bool isAudioPlaybackBufferEmpty();
bool isAudioCaptureBufferFull();
    
void emptyAudioPlaybackBuffer();
void emptyAudioRecordingBuffer();

void reOpenSockets();
void clearOraId(); //Added for slow connections...
void sendPlaycredits();
    
    extern bool isIpSetFromApp;
//Websocket API's
extern bool isWebSocketDataAvailable;
extern int countOra;
extern char inputBuf[MAX_NUM_PACKETS][MAX_PACKET_SIZE];
extern char outputBuf[MAX_NUM_PACKETS][MAX_PACKET_SIZE_OUTPUT];
extern int putIndex ;
extern int getIndex;
    
extern int putIndexOutput;
extern int getIndexOutput;
    
extern char buf[THEATROPAYLOADBUFFERSIZE];
    

    
int webSocket_init();
void webSocket_loopService();
int webSocket_sendData(char *buffer, int len,int type);
int webSocket_deinit();
int webSocket_wait();
pthread_t WebSocketThreadHandle;
 
void Send_PlayCredit_Msg(char *oruid ,unsigned long int dropped_packets_per_oration  ,
		unsigned long int total_packets_per_oration, bool isEofOration ,
		double mosValue , double ppl , double avgJitter, double avgLatency,
		double orationPktRateInSec );
double TheatroMOS_Calculation(double avg_Latency, double avgJitter, double percentage_packet_loss);   

    
    
    


#ifdef __cplusplus
}
#endif

#endif /* THEATROCOMMUNICATOR_H_ */
