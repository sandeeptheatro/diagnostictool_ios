/*
 * AndroidSound.cpp
 *
 *  Created on: Aug 14, 2013
 *      Author: Abhinandan
 */

#include <assert.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <semaphore.h>

#include "AndroidSound.h"

#define DEBUG
#include "Trace.h"

#include "RingBuffer.h"

#include "TheatroCommunicator.h"
#include "TheatroUtilities.h"


bool isAudioAlive;
bool isAudioRecording;
bool isAudioPlaying;
bool isCaptureOpen;
bool isPlaybackOpen;
bool isAudioStreaming;

bool isAudioDebugging;
bool isAudioPacketDebugging;


RingBuffer AudioCaptureRingBuffer(MAX_CAPTURE_BUFFER_SIZE);
RingBuffer AudioPlaybackRingBuffer(MAX_PLAYBACK_BUFFER_SIZE);


// 5 seconds of recorded audio at 16 kHz mono, 16-bit signed little endian
#define RECORDER_FRAMES (16000 * 5)
static short recorderBuffer[RECORDER_FRAMES];
static unsigned recorderSize = 0;
static SLmilliHertz recorderSR;


//engine interfaces
SLEngineItf engineEngine;
SLObjectItf engineObject;

//recorder interfaces
SLObjectItf recorderObject;
SLRecordItf recorderRecord;
SLAndroidSimpleBufferQueueItf recorderBufferQueue;

// buffer queue player interfaces
static SLObjectItf bqPlayerObject = NULL;
static SLPlayItf bqPlayerPlay;
static SLAndroidSimpleBufferQueueItf bqPlayerBufferQueue;
static SLEffectSendItf bqPlayerEffectSend;
static SLMuteSoloItf bqPlayerMuteSolo;
static SLVolumeItf bqPlayerVolume;

//output mix interfaces
SLObjectItf outputMixObject;
SLEnvironmentalReverbItf outputMixEnvironmentalReverb;

// aux effect on the output mix, used by the buffer queue player
static const SLEnvironmentalReverbSettings reverbSettings =
SL_I3DL2_ENVIRONMENT_PRESET_STONECORRIDOR;


sem_t playbackSemaphore;
sem_t recordingSemaphore;

pthread_t AudioThreadHandle;

//The recorder frame buffer initialisations..
const int frames_to_read = 200;
const int audio_record_length = frames_to_read * sizeof(signed short);
char audio_buffer_record[2][audio_record_length];

bool isRecordingCallbackCome;

//The playback frame buffer initialisations..
const int frames_to_deliver = 200;
const int audio_playback_length = frames_to_deliver * sizeof(signed short);
signed short audio_buffer_playback[2][audio_playback_length];

// create the engine and output mix objects
void createEngine()
{
   Begin;
   SLresult result;

   // create engine
   result = slCreateEngine(&engineObject, 0, NULL, 0, NULL, NULL);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   // realize the engine
   result = (*engineObject)->Realize(engineObject, SL_BOOLEAN_FALSE);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   // get the engine interface, which is needed in order to create other objects
   result = (*engineObject)->GetInterface(engineObject, SL_IID_ENGINE, &engineEngine);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   // create output mix, with environmental reverb specified as a non-required interface
   const SLInterfaceID ids[1] = { SL_IID_ENVIRONMENTALREVERB };
   const SLboolean req[1] = { SL_BOOLEAN_FALSE };
   result = (*engineEngine)->CreateOutputMix(engineEngine, &outputMixObject, 1, ids, req);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   // realize the output mix
   result = (*outputMixObject)->Realize(outputMixObject, SL_BOOLEAN_FALSE);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   // get the environmental reverb interface
   // this could fail if the environmental reverb effect is not available,
   // either because the feature is not present, excessive CPU load, or
   // the required MODIFY_AUDIO_SETTINGS permission was not requested and granted
   result = (*outputMixObject)->GetInterface(outputMixObject, SL_IID_ENVIRONMENTALREVERB,
         &outputMixEnvironmentalReverb);
   if (SL_RESULT_SUCCESS == result) {
      result = (*outputMixEnvironmentalReverb)->SetEnvironmentalReverbProperties(
            outputMixEnvironmentalReverb, &reverbSettings);
      (void) result;
   }
   // ignore unsuccessful result codes for environmental reverb, as it is optional for this example
   End;
}

// shut down the native audio system
void shutdownAudio()
{
   Begin;
   Printf("Shutdown Audio...Happening...\n");
   // destroy buffer queue audio player object, and invalidate all associated interfaces
   if (bqPlayerObject != NULL) {
      (*bqPlayerObject)->Destroy(bqPlayerObject);
      bqPlayerObject = NULL;
      bqPlayerPlay = NULL;
      bqPlayerBufferQueue = NULL;
      bqPlayerEffectSend = NULL;
      bqPlayerVolume = NULL;
   }

   // destroy audio recorder object, and invalidate all associated interfaces
   if (recorderObject != NULL) {
      (*recorderObject)->Destroy(recorderObject);
      recorderObject = NULL;
      recorderRecord = NULL;
      recorderBufferQueue = NULL;
   }

   // destroy output mix object, and invalidate all associated interfaces
   if (outputMixObject != NULL) {
      (*outputMixObject)->Destroy(outputMixObject);
      outputMixObject = NULL;
      outputMixEnvironmentalReverb = NULL;
   }

   // destroy engine object, and invalidate all associated interfaces
   if (engineObject != NULL) {
      (*engineObject)->Destroy(engineObject);
      engineObject = NULL;
      engineEngine = NULL;
   }
   End;
}

// this callback handler is called every time a buffer finishes playing..
void bqPlayerCallback(SLAndroidSimpleBufferQueueItf bq, void *context)
{
   //Begin;
   //assert(bq == bqPlayerBufferQueue);
   //assert(NULL == context);
   sem_post(&playbackSemaphore); //post the semaphore to unlock the wait for next enqueue in audio thread..
   //End;
}

// this callback handler is called every time a buffer finishes recording
void bqRecorderCallback(SLAndroidSimpleBufferQueueItf bq, void *context)
{
   Begin;
   //assert(bq == bqRecorderBufferQueue);
   //assert(NULL == context);
   sem_post(&recordingSemaphore);//post the semaphore to unlock the wait for next enqueue in audio thread..
   isRecordingCallbackCome = true;
   End;
}

bool createAudioRecorder()
{
   Begin;

   SLresult result;

   // configure audio source
   SLDataLocator_IODevice loc_dev = { SL_DATALOCATOR_IODEVICE, SL_IODEVICE_AUDIOINPUT,
      SL_DEFAULTDEVICEID_AUDIOINPUT, NULL };
   SLDataSource audioSrc = { &loc_dev, NULL };

   // configure audio sink
   SLDataLocator_AndroidSimpleBufferQueue loc_bq = { SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2 };
   SLDataFormat_PCM format_pcm = { SL_DATAFORMAT_PCM, 1,SL_SAMPLINGRATE_8,
      SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
      SL_SPEAKER_FRONT_CENTER, SL_BYTEORDER_LITTLEENDIAN };
   SLDataSink audioSnk = { &loc_bq, &format_pcm };

   // create audio recorder
   // (requires the RECORD_AUDIO permission)
   const SLInterfaceID id[1] = { SL_IID_ANDROIDSIMPLEBUFFERQUEUE };
   const SLboolean req[1] = { SL_BOOLEAN_TRUE };
   result = (*engineEngine)->CreateAudioRecorder(engineEngine, &recorderObject, &audioSrc,
         &audioSnk, 1, id, req);
   if (SL_RESULT_SUCCESS != result) {
      Return(false);
   }

   // realize the audio recorder
   result = (*recorderObject)->Realize(recorderObject, SL_BOOLEAN_FALSE);
   if (SL_RESULT_SUCCESS != result) {
      Return(false);
   }

   // get the record interface
   result = (*recorderObject)->GetInterface(recorderObject, SL_IID_RECORD, &recorderRecord);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   // get the buffer queue interface
   result = (*recorderObject)->GetInterface(recorderObject, SL_IID_ANDROIDSIMPLEBUFFERQUEUE,
         &recorderBufferQueue);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   // register callback on the buffer queue
   result = (*recorderBufferQueue)->RegisterCallback(recorderBufferQueue, bqRecorderCallback,
         NULL);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   Return(true);
}

bool startAudioRecording_Android()
{
   Begin;
   SLresult result;

   AudioCaptureRingBuffer.empty();
#if 0	
   // in case already recording, stop recording and clear buffer queue
   result = (*recorderRecord)->SetRecordState(recorderRecord, SL_RECORDSTATE_STOPPED);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;
   result = (*recorderBufferQueue)->Clear(recorderBufferQueue);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;
#endif
   // start recording
   result = (*recorderRecord)->SetRecordState(recorderRecord, SL_RECORDSTATE_RECORDING);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   sem_init(&recordingSemaphore,0,1);

   End;
   return true;
}

void stopAudioRecording_Android()
{
   Printf("Driver :Stopping the Android recording...\n");
   SLresult result;
#if 1
   result = (*recorderRecord)->SetRecordState(recorderRecord, SL_RECORDSTATE_STOPPED);
#endif	
   result = (*recorderBufferQueue)->Clear(recorderBufferQueue);
   assert(SL_RESULT_SUCCESS == result);

   isRecordingCallbackCome = false;
}

void createBufferQueueAudioPlayer()
{
   Begin;
   SLresult result;

   // configure audio source
   SLDataLocator_AndroidSimpleBufferQueue loc_bufq = { SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2 };
   SLDataFormat_PCM format_pcm = { SL_DATAFORMAT_PCM, 1, SL_SAMPLINGRATE_8,
      SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
      SL_SPEAKER_FRONT_CENTER, SL_BYTEORDER_LITTLEENDIAN };
   SLDataSource audioSrc = { &loc_bufq, &format_pcm };

   // configure audio sink
   SLDataLocator_OutputMix loc_outmix = { SL_DATALOCATOR_OUTPUTMIX, outputMixObject };
   SLDataSink audioSnk = { &loc_outmix, NULL };

   // create audio player
   const SLInterfaceID ids[3] = { SL_IID_BUFFERQUEUE, SL_IID_EFFECTSEND,
      /*SL_IID_MUTESOLO,*/SL_IID_VOLUME };
   const SLboolean req[3] = { SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE,
      /*SL_BOOLEAN_TRUE,*/SL_BOOLEAN_TRUE };
   result = (*engineEngine)->CreateAudioPlayer(engineEngine, &bqPlayerObject, &audioSrc, &audioSnk,
         3, ids, req);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   // realize the player
   result = (*bqPlayerObject)->Realize(bqPlayerObject, SL_BOOLEAN_FALSE);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   // get the play interface
   result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_PLAY, &bqPlayerPlay);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   // get the buffer queue interface
   result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_BUFFERQUEUE,
         &bqPlayerBufferQueue);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   // register callback on the buffer queue
   result = (*bqPlayerBufferQueue)->RegisterCallback(bqPlayerBufferQueue, bqPlayerCallback, NULL);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   // get the effect send interface
   result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_EFFECTSEND, &bqPlayerEffectSend);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

#if 0   // mute/solo is not supported for sources that are known to be mono, as this is
   // get the mute/solo interface
   result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_MUTESOLO, &bqPlayerMuteSolo);
   assert(SL_RESULT_SUCCESS == result);
   (void)result;
#endif

   // get the volume interface
   result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_VOLUME, &bqPlayerVolume);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   // set the player's state to playing
   result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PLAYING);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;

   End;
}

bool startAudioPlayback_Android()
{

   //createBufferQueueAudioPlayer();
   SLresult result;
   // set the player's state to playing
   result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PLAYING);
   assert(SL_RESULT_SUCCESS == result);
   (void) result;
   sem_init(&playbackSemaphore,0,1);
   return true;
}

void stopAudioPlayback_Android()
{
   isAudioPlaying = false;

   (*bqPlayerPlay)->SetPlayState(bqPlayerPlay,SL_PLAYSTATE_STOPPED);

   (*bqPlayerBufferQueue)->Clear(bqPlayerBufferQueue);

   // destroy buffer queue audio player object, and invalidate all associated interfaces
#if 0
   if (bqPlayerObject != NULL) {
      (*bqPlayerObject)->Destroy(bqPlayerObject);
      bqPlayerObject = NULL;
      bqPlayerPlay = NULL;
      bqPlayerBufferQueue = NULL;
      bqPlayerEffectSend = NULL;
      bqPlayerVolume = NULL;
   }
#endif

}

int playBuffer(short *buff, int length)
{
   int put_bytes = 0;
   do
   {
      if (AudioPlaybackRingBuffer.getWriteAvail() >= length)
      {
         put_bytes = AudioPlaybackRingBuffer.write((char *) buff, length);
      }
      else 
      {
         //When PlaybackRingbuffer is full..we are reading out the ringbuffer to make space for new data coming in..***Fix for blue Led issue **
         //millisleep(400);  // Sleep 400 ms.
         signed short data[length];
         int bytes_read = AudioPlaybackRingBuffer.readWithTimeout((char *) data,length,QUADTICK);
         put_bytes = AudioPlaybackRingBuffer.write((char *) buff, length);
         //printf("The playback overflowed.. Read out %d bytes and made space..\n",bytes_read);
      }
   }
   while (put_bytes == 0);

   if (isAudioDebugging) Printf("Enqueued %d bytes.  Requested %d bytes.  %d free in play buffer.", put_bytes, length, AudioPlaybackRingBuffer.getWriteAvail());

   return(put_bytes == length);
}

static FILE *recordF;
int noMoreData = 0;

bool playFile(const char *fn)
{
   SLresult result;

   //open the file to read data
   recordF = fopen(fn, "rb");

   if (NULL == recordF) {
      Printf("cannot open %s pcm file to read \n",fn);
      Return(false);
   }
   noMoreData = 0;

   //fill the two buffers for playing first
   int numOfRecords = fread(recorderBuffer, sizeof(short), RECORDER_FRAMES, recordF);

   if (RECORDER_FRAMES != numOfRecords)
   {
      if (numOfRecords <= 0)
      {
         Printf("no data for playing");
         Return(true);
      }
      noMoreData = 1;
      Printf("no more data");
   }
   else {
      Printf("read %d", numOfRecords);
   }

   result = (*bqPlayerBufferQueue)->Enqueue(bqPlayerBufferQueue, recorderBuffer, RECORDER_FRAMES * sizeof(short));

   if (SL_RESULT_SUCCESS != result) {
      Return(false);
   }

   // set the player's state to playing
   result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PLAYING);
   assert(SL_RESULT_SUCCESS == result);

   Printf("player started playing");
   Return(true);
}

void stopRecording()
{
   //only global variable is set to false... Android stopRecording is done in different function as well as different context...
   isAudioRecording = false;
   emptyAudioRecordingBuffer();

}




int getAvailablePlaybackSpace()
{
   return (((float) AudioPlaybackRingBuffer.getWriteAvail() / (float) MAX_PLAYBACK_BUFFER_SIZE) * 1000);
}



bool isAudioPlaybackBufferEmpty()
{
   return AudioPlaybackRingBuffer.isEmpty();
}



void emptyAudioPlaybackBuffer()
{
   isAudioPlaying = false;

   //	if (playback_handle)
   //	{
   //		snd_pcm_drop(playback_handle);
   //	}
   AudioPlaybackRingBuffer.empty();
}

void emptyAudioRecordingBuffer()
{
   AudioCaptureRingBuffer.empty(); //Added abhi.. not present...
}


int getRecordBuffer(signed short *buf, int frames_to_read)
{
   int get_bytes = 0;

   int length_bytes = frames_to_read * sizeof(signed short);

   if (AudioCaptureRingBuffer.getReadAvail() > 0)
   {
      get_bytes = AudioCaptureRingBuffer.read((char *) buf, length_bytes);
      //Printf("RingBuffer : Read %d bytes\n",get_bytes);
   }

   return(get_bytes);
}





bool injectRecordBuffer(char *buff, int length)
{
   int put_bytes = 0;

   do
   {
      if (AudioCaptureRingBuffer.getWriteAvail() >= length)
      {
         put_bytes = AudioCaptureRingBuffer.write((char *) buff, length);
      }
      //		else {
      //			millisleep(400);  // Sleep 400 ms.
      //		}
   }
   while (put_bytes == 0);

   if (isAudioDebugging) Printf("Injected %d bytes.  Requested %d bytes.  %d free in play buffer.", put_bytes, length, AudioCaptureRingBuffer.getWriteAvail());

   return(put_bytes == length);
}


bool injectFile(const char *file_name)
{
   // WavHeader_with_fact *header = NULL;
   char *header = NULL;

   FILE *fd;

   if (file_name)
   {
      fd = fopen(file_name,"rb");
      if (fd > 0)
      {
         const int header_size = 54;

         header = (char*) malloc(header_size);

         memset(header, 0, header_size);

         uint32_t length_in_bytes = 0;

         struct stat info;

         stat(file_name, &info);

         BreadCrumb;

         char *buffer = malloc(info.st_size);
         memset(buffer, 0, info.st_size);

         if (isAudioDebugging) Printf("stat size = %d", info.st_size);

         if (buffer)
         {
            int length = 0;

            BreadCrumb;

            length = fread(buffer, 1, info.st_size, fd);

            const char data[4] = {'d', 'a', 't', 'a'};

            char * p = 0;
            for ( int i = 0; i < length - 4; ++ i ) {
               if ( buffer[i] == 'd' )
                  if ( buffer[i + 1] == 'a' )
                     if ( buffer[i + 2] == 't' )
                        if ( buffer[i + 3] == 'a' )
                        {
                           p = &buffer[i];
                           break;
                        }
            }

            if (p) {
               p = &buffer[i + 8];


               int wave_length = buffer[i+4] | ( (int)buffer[i+5] << 8 ) | ( (int)buffer[i+6] << 16 ) | ( (int)buffer[i+7] << 24 );

               if (injectRecordBuffer(p, wave_length)) if (isAudioDebugging) Printf("Injected %d bytes.", wave_length);

               // No ulaw2PCM
               //							AudioCaptureRingBuffer.write((char *) p, wave_length);

            }
            else
               Printf("Data not found");
         }

         fclose(fd);
      }
      else {
         Printf("Error opening file %s", file_name);
      }
   }
   else {
      Printf("Filename not specified");
   }
}

void *AudioThread(void *ptr)
{
   Begin;

   isAudioAlive = true;
   isCaptureOpen = false;
   isPlaybackOpen = false;

   int index_record= 0;
   int index_playback = 0;
   int bytes_read = 0;
   //sem_init(&playbackSemaphore,0,1);
   //sem_init(&recordingSemaphore,0,1);

   SLresult result;

   Printf("Setup Audo REcorder \n");

   while (isAudioAlive)
   {
      if (isAudioRecording)   // We need to record.
      {

         if (isPlaybackOpen)
         {
            stopAudioPlayback_Android();
            isPlaybackOpen = false;
         }
         else {

            const int frames_to_deliver;

            int frames_read = 0;

            if (!AudioCaptureRingBuffer.isFull())
            {

               if (!isCaptureOpen)
               {
                  if (startAudioRecording_Android())
                  {
                     isCaptureOpen = true;
                     TracePoint("RECORD: OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
                  }
                  else {
                     stopAudioRecording_Android();
                     isCaptureOpen = false;
                  }

               }

               if (isCaptureOpen)
               {

                  Printf("RECORD: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\n");

                  while (isAudioRecording)
                  {

                     sem_wait(&recordingSemaphore);//Wait for notification from the callback function that the recorder buffer is full...
                     result = (*recorderBufferQueue)->Enqueue(recorderBufferQueue,audio_buffer_record[index_record],frames_to_read * sizeof(short));
                     index_record = index_record ? 0:1;	//Always swap the buffers in Enqueue...
                     if (SL_RESULT_SUCCESS == result && isRecordingCallbackCome)
                     {
                        int bytes_enqueued = AudioCaptureRingBuffer.write((char *) audio_buffer_record[index_record],frames_to_read * sizeof(signed short) );
                     }
                  }

                  Printf("RECORD: CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
                  sem_wait(&recordingSemaphore);//Wait for notification from the callback function that the recorder buffer is full...
                  stopAudioRecording_Android();
                  index_record = 0;
                  isCaptureOpen = false;
               }
            }
         }
      }

      else // We need to play.
      {
         if (isCaptureOpen)
         {
            TracePoint("RECORD: CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
            stopAudioRecording_Android();
            isCaptureOpen = false;
         }
         else {
            while (!AudioPlaybackRingBuffer.isEmpty())
            {
               const int  frames_to_deliver = 200;

               int err = 0;

               if (startAudioPlayback_Android())
               {
                  //Read from AudioPlayback ring buffer for once...
                  bytes_read = AudioPlaybackRingBuffer.readWithTimeout((char *)audio_buffer_playback[index_playback], frames_to_deliver * sizeof(signed short), QUADTICK);

                  TracePoint("PLAY: OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
                  isPlaybackOpen = true;
               }
               else {
                  stopAudioPlayback_Android();
               }

               if(isPlaybackOpen)
               {
                  Printf("PLAY: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");

                  do
                  {
                     sem_wait(&playbackSemaphore); //wait for the semaphore post from plaback callback.. then only enque the next buffer...

                     TracePoint("frames_to_deliver = %d\tbytes_read = %d\tBB.getReadAvail = %d", frames_to_deliver, bytes_read, AudioPlaybackRingBuffer.getReadAvail());
                     if (bytes_read > 0)
                     {
                        SLresult result;
                        // enqueue another buffer
                        result = (*bqPlayerBufferQueue)->Enqueue(bqPlayerBufferQueue,audio_buffer_playback[index_playback],bytes_read);

                        assert(SL_RESULT_SUCCESS == result);

                        (void) result;
                     }
                     else
                     {
                        Printf("=======No data======%d\n",bytes_read);
                        break;
                     }

                     index_playback = index_playback ? 0:1;

                     //next buffer read...
                     bytes_read = AudioPlaybackRingBuffer.readWithTimeout((char *)audio_buffer_playback[index_playback], frames_to_deliver * sizeof(signed short), QUADTICK);

                  }
                  while (!isAudioRecording);  // Continue as long as isAudioRecording is true...

                  if(bytes_read > 0)
                     sem_wait(&playbackSemaphore); //wait for the semaphore post from plaback callback.. then only enque the next buffer...
                  stopAudioPlayback_Android();
                  Printf("PLAY: CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
                  isPlaybackOpen = false;
                  if (isAudioDebugging) Printf("Player Ended..");
               }
            }
            millisleep(TICKWAIT);  // This keep the CPU utilization from going crazy.
         }
      }
   }
   millisleep(TICKWAIT);  // You will not starve the PCM device here, they are closed.
   shutdownAudio();
}


void initSound_Android()
{
   isAudioRecording = false;
   isAudioPlaying = false;

   createEngine();//lets create here and see...
   createBufferQueueAudioPlayer();// Initialise the audio playback..
   createAudioRecorder();// Initialise audio recorder..

   int rc = pthread_create(&AudioThreadHandle, NULL, AudioThread, NULL);
   Printf("Created AudioThread %d", rc);
}

void deInitSound_Andorid()
{
   Printf("Android Sound Deinit\n");
   isAudioAlive = false;
}
