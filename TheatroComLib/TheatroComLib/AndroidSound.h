/*
 * AndroidSound.h
 *
 *  Created on: Aug 14, 2013
 *      Author: Abhinandan
 */

#ifndef ANDROIDSOUND_H_
#define ANDROIDSOUND_H_

#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#include <assert.h>
#include <stdio.h>


#define MAX_CAPTURE_BUFFER_SIZE   262136
#define MAX_PLAYBACK_BUFFER_SIZE 2000000


extern bool isAudioAlive;
extern bool isAudioRecording;
extern bool isAudioPlaying;
extern bool isAudioDebugging;
extern bool isAudioPacketDebugging;
extern bool isAudioStreaming;


#ifdef __cplusplus
extern "C"
{
#endif

   void buildSawtoothBuffer();
   void createEngine();
   bool createAudioRecorder();
   void startRecording_Android();
   void createBufferQueueAudioPlayer();
   bool startPlaying();
   void shutdownAudio();
   int playBuffer(short *buf, int length);
   int enqueueBuffer(short *buf);
   bool playFile(const char *fn);
   void initSound_Android();
   void deInitSound_Andorid();

   void startRecording();
   void stopRecording();


   int getAvailableRecordSpace();
   int getAvailablePlaybackSpace();

   bool isAudioPlaybackBufferEmpty();
   void emptyAudioPlaybackBuffer();
   void emptyAudioRecordingBuffer();

   int getRecordBuffer(signed short *buf, int frames_to_deliver);
   bool injectRecordBuffer(char *buff, int length);
   bool injectFile(const char *fn);

   void replayAudioInDevice();

#ifdef __cplusplus
};
#endif

#endif /* ANDROIDSOUND_H_ */
