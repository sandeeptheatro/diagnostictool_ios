    /*
     * TheatroCommunicator.cpp
     *
     *  Created on: Mar 13, 2013
     *      Author: vincente
     *  Abhinandan Chougala : ported on Android
     *
     */

    #include <stdio.h>
    #include <stdlib.h>
    #include <fcntl.h>
    #include <stdint.h>
    #include <sys/mman.h>
    #include <sys/ioctl.h>
    //#include <sys/prctl.h>
    //#include <linux/ioctl.h>
    #include <strings.h>
    #include <string.h>
    #include <ctype.h>
    #include <unistd.h>
    #include <errno.h>
    #include <sys/types.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <netinet/in.h>
    #include <sys/select.h>
    #include <pthread.h>
    #include <signal.h>
    #include <sys/stat.h>

    #include "TheatroDefs.h"
    #include "TheatroUtilities.h"
    //#include "iwLib.h"
    //#include "iwList.h"
    //#include "wpaCLI.h"
    #include "TheatroCommunicator.h"
    #include "RingBuffer.h"
    #include "TheatroComLib.h"

    //#define DEBUG
    #include "Trace.h"

    //Compilation declarations...
    bool isAudioPacketDebugging;
    bool isAudioRecording;
    bool isAudioStreaming;

    #include <netdb.h>



    bool isBroadcastScreenOn;



    #import "TheatroComLib.h"



    char CommunicatorWiFiInterfaceName[NAMESIZE];
    char CommunicatorMACAddress[NAMESIZE];
    char TGSIPAddress[NAMESIZE];
    bool isTGSConnected;

    char TgsIp[NAMESIZE];
    char Dest[NAMESIZE];
    char appVersion[NAMESIZE];

    int RecordPort;
    int ErrorWait;
    int ReassociateWait;
    int TickWait;
    int MaxTicks;

    const int max_sockets = 2;
    int s[max_sockets];			// Our sockets

    int PlayCreditInterval;
    int WiFiScanInterval;
    int LEDFlashInterval;
    int InitialSilence;

    int NoReceptionInterval;
    int NoReceptionMessageCount;

    int AuthenticationInterval;
    int AuthenticationNoReceptionInterval;
    int AuthenticationInitialInterval;
    bool AuthenticationBroadcast;

    int NewOrationPacketsToSkip;
    int NewRecordingPacketsToSkip;
    double RecordAmplificationFactor;

    int CluePort;
    int AuthenticationPort;
    int AuthenticationRequestPort;
    int PlayPort;
    int PlayCreditPort;

    pthread_mutex_t mutex;// Mutex for updating globals shared between C threads and Java.

    unsigned long int PlayCreditSeqno;
    unsigned long int ClueSeqno;

    int ChargedThreshold;// Returned from TGS in network message to specify the fully charged level.
    bool isCommunicatorAlive;
    bool isSimulating;
    bool isNoReception;
    bool isReassociatedOnce;
    bool isCharging;
    bool isCharged;
    bool isFullyCharged;
    bool isRegistrationNeeded;
    bool isCancelledFromJava;
    bool isCommAppStopped;
    bool isNetworkSwitchFromApp;

    bool isIpSetFromApp;

    //Websocket constructs...
    bool isWebSocketDataAvailable;
    int countOra;
    char inputBuf[MAX_NUM_PACKETS][MAX_PACKET_SIZE];
    char outputBuf[MAX_NUM_PACKETS][MAX_PACKET_SIZE_OUTPUT];
    NETWORK_STATE currentNetworkStatus;

    int putIndex = 0;
    int getIndex = 0;
    int putIndexOutput = 0;
    int getIndexOutput = 0;
    int connection_flag = 0;
    int destroy_flag = 0;
    int  numTries = 0;


    bool isMosEnabled = true;
    bool MOS_Reported = false;
    unsigned long long ullAuthSendTime;
    unsigned long long ullAuthRecvTime;
    unsigned long long NetworkRecvTime;
    double  TrasmissionTime;
    long long offsetTime;



    typedef enum {
       None, PrimaryButton, BroadcastButton, Simulator
    } ButtonType;

    ButtonType currentButton;	// Which button is being pressed?

    // Individual Button states.
    bool isPrimaryButtonPushed;
    bool isBroadcastButtonPushed;
    bool isVolumeUpButtonPushed;
    bool isVolumeDownButtonPushed;

    const char *PrimaryButtonTxt = "menubutton"; //tag describing menu button for "Engage" or "Big Button"
    const char *BroadcastButtonTxt = "querybutton"; //tag describing query button for Broadcast/WalkieTalkie
    const char *mediaButtonTxt = "mediabutton";
    const char *audioTestTxt = "audiotest";

    pthread_t PollingThreadHandle;
    pthread_t RecorderThreadHandle;
    pthread_t SimulatorThreadHandle;
    pthread_t WiFiScanThreadHandle;


    #define AUTHENTICATIONSOCKET		s[0]
    #define PLAYSOCKET					s[1]

    int SimulatorSocket;
    int RecordSocket;
    int WiFiScanSocket = 0;
    int PlayCreditSocket;

    unsigned int gNumOfClues = 12;
    bool isWiFiScanEnabled = false;

    char *coverageStr = NULL;
    char *grouplistStr = NULL;

    unsigned int gCoverageVersion = 1;


    void *RecorderThread(void *button_text); // Forward Declaration, try to fix this.

    char last_oruid[LINESIZE];
    // set up select parameters
    fd_set set;
    bool isReopened;

    sockaddr_in PlayPortAddress;
    sockaddr_in AuthenticationPortAddress;


    int sendAuths()
    {
        char buffer[FOUR_K];
        struct sockaddr_in TGSAddress;
        cJSON *AuthenticationObject = cJSON_CreateObject();
        cJSON_AddStringToObject(AuthenticationObject, "cmd", "auth");
        cJSON_AddNumberToObject(AuthenticationObject, "ver", 2);
        cJSON_AddStringToObject(AuthenticationObject, "mac", CommunicatorMACAddress);
        //cJSON_AddStringToObject(AuthenticationObject, "svnver", SVNREVISION);
        cJSON_AddStringToObject(AuthenticationObject, "svnver",appVersion);
        
        // Convert getCurrentTimeMillis() to a string because cJSON renders this as a negative number.
        char timeMillis[NAMESIZE];
        memset(timeMillis, 0, NAMESIZE);
        snprintf(timeMillis, NAMESIZE, "%llu", getCurrentTimeMillis());
        cJSON_AddStringToObject(AuthenticationObject, "time", timeMillis);
        
        cJSON_AddNumberToObject(AuthenticationObject, "volume", 0);
        cJSON_AddNumberToObject(AuthenticationObject, "battery", 5555);
        cJSON_AddNumberToObject(AuthenticationObject, "chargestat", (isCharged * 2) + isCharging);
        cJSON_AddNumberToObject(AuthenticationObject, "batterytemp", 0);
        
        if(!isIpSetFromApp)
        {
        //Added for proxy server changes...
        cJSON_AddStringToObject(AuthenticationObject, "TGSIPAddress",TgsIp);
        if(strncmp(Dest,"StoreName",9))
        {
            cJSON_AddStringToObject(AuthenticationObject, "Dest",Dest);
        }
        }
        
    #if 0
        char version[NAMESIZE]= {0};
        snprintf(version, NAMESIZE, "%u",gCoverageVersion);
        cJSON_AddStringToObject(AuthenticationObject,"cvgver",version);
        
    #endif
        
        // Format the Authentication message in the buffer.
        
        memset(buffer, 0, THEATROPAYLOADBUFFERSIZE);
        
        char *cRet = cJSON_PrintUnformatted(AuthenticationObject);// Workaround for cJSON leak.
        if (cRet)
        {
            strncpy(buffer, cRet, THEATROPAYLOADBUFFERSIZE);
            free(cRet);										// Workaround for cJSON leak.
        }
        
        if( currentNetworkStatus < WIFI_ON_TGS_OFF )
        {
            TGSAddress.sin_family = AF_INET;
            TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
            TGSAddress.sin_port = htons(AuthenticationRequestPort);
        
        
            if (sendto(AUTHENTICATIONSOCKET, buffer, strlen(buffer), 0, (struct sockaddr *) &TGSAddress, sizeof TGSAddress) > 0)
            {
                // Message was sent successfully.
                NSLog(@"Native: UDP Sent Authentication \"%s\" to %s:%d\n", buffer, inet_ntoa(TGSAddress.sin_addr), ntohs(TGSAddress.sin_port));
                //  status();
                cJSON_Delete(AuthenticationObject);
                return 1;
           }
            else {
                NSLog(@"Authenticating sendto error xxxxxxxxxxxxxxxxxxxxx");
                close(AUTHENTICATIONSOCKET);
                
                // Kick Start the Authentication socket.
                AUTHENTICATIONSOCKET = socket(AF_INET, SOCK_DGRAM, 0);
                
                if (AUTHENTICATIONSOCKET != 0)
                {
                    memset(&AuthenticationPortAddress, 0, sizeof AuthenticationPortAddress);
                    AuthenticationPortAddress.sin_family = AF_INET;
                    AuthenticationPortAddress.sin_addr.s_addr = htons(INADDR_ANY);
                    AuthenticationPortAddress.sin_port = htons(AuthenticationPort);
                    
                    memset(&PlayPortAddress, 0, sizeof PlayPortAddress);
                    PlayPortAddress.sin_family = AF_INET;
                    PlayPortAddress.sin_addr.s_addr = htons(INADDR_ANY);
                    PlayPortAddress.sin_port = htons(PlayPort);
                    
                    fcntl(AUTHENTICATIONSOCKET, F_SETFL, O_NONBLOCK);
                    bind(AUTHENTICATIONSOCKET, (struct sockaddr*) &AuthenticationPortAddress, sizeof(AuthenticationPortAddress));
                    
                    // Allow Authentication messages to be broadcast to the subnet.
                    int BroadcastEnableOnSocket = 1;
                    setsockopt(AUTHENTICATIONSOCKET, SOL_SOCKET, SO_BROADCAST, &BroadcastEnableOnSocket, sizeof(BroadcastEnableOnSocket));
                }
            }
        }
        else if( currentNetworkStatus == WIFI_ON_TGS_OFF )
        {
            if(webSocket_sendData(buffer,(int)strlen(buffer),1))
            {
                NSLog(@"Native: Sent WSS Auth %s\n",buffer);
                cJSON_Delete(AuthenticationObject);
                return 1;
            }
            else
            {
                cJSON_Delete(AuthenticationObject);
                return 0;
            }
        }
        
        cJSON_Delete(AuthenticationObject);
        return 1;
    }

    void sendPlaycredits()
    {
        // Build and send PlayCredit message every tick.
        char buffer[FOUR_K];
        struct sockaddr_in TGSAddress;
        
        cJSON *PlayCreditObject = cJSON_CreateObject();
        cJSON_AddStringToObject(PlayCreditObject, "cmd", "playcredit");
        cJSON_AddNumberToObject(PlayCreditObject, "ver", 2);
        cJSON_AddStringToObject(PlayCreditObject, "mac", CommunicatorMACAddress);
        cJSON_AddNumberToObject(PlayCreditObject, "seqno", PlayCreditSeqno);
        //cJSON_AddNumberToObject(PlayCreditObject, "available", getAvailablePlaybackSpace() );
        cJSON_AddNumberToObject(PlayCreditObject, "available",0);
        
        
        // Format the PlayCredit message in the buffer.
        
        memset(buffer, 0, THEATROPAYLOADBUFFERSIZE);
        char *cRet = cJSON_PrintUnformatted(PlayCreditObject);// Workaround for cJSON leak.
        if (cRet)
        {
            strncpy(buffer, cRet, THEATROPAYLOADBUFFERSIZE);
            free(cRet);										// Workaround for cJSON leak.
        }
        
        TGSAddress.sin_family = AF_INET;
        TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
        TGSAddress.sin_port = htons(PlayCreditPort);
        
        if( currentNetworkStatus < WIFI_ON_TGS_OFF  )
        {

        if (sendto(PLAYSOCKET, buffer, strlen(buffer), 0, (struct sockaddr *) &TGSAddress, sizeof TGSAddress) > 0)
        {
            NSLog(@"Sent PlayCredits %s\n",buffer);
        // Reset the dropped packets per playcredit count.
            // Ravi- Aug 27th,2013, Its already incemented outside the loop
            //PlayCreditSeqno++;                  // Increment the sequence number.
        }
        else {
            NSLog(@"Error sending PlayCredits to %s:%d <== %s", inet_ntoa(TGSAddress.sin_addr), ntohs(TGSAddress.sin_port), buffer);
            
            // Kick Start the PlayCreditSocket socket.
            close(PlayCreditSocket);
            PlayCreditSocket = socket(AF_INET, SOCK_DGRAM, 0);
            NSLog(@"PlayCreditSocket = %d", PlayCreditSocket);
        }
        }
        else if( currentNetworkStatus == WIFI_ON_TGS_OFF)
        {
            webSocket_sendData(buffer, strlen(buffer),3);
        }
        cJSON_Delete(PlayCreditObject);
    }

    void *PollingThread(void *ptr) {
        Begin;

        char buffer[FOUR_K];  // Our message buffer;

        ChargedThreshold = 2000;
        
        emptyAudioPlaybackBuffer();
        emptyAudioRecordingBuffer();

        if ( currentNetworkStatus  == WIFI_ON_TGS_OFF )
        {
            webSocket_init();
            
             NSLog(@"Native Corporate: PollingThread : Waiting for Websocket connection...\n");
            numTries = 0;
            while(!connection_flag && !isNetworkSwitchFromApp )
            {
                if(currentNetworkStatus <= WIFI_OFF_CELLULAR_ON)
                {
                    NSLog(@"Native: Breaking bad.... b/w connection...\n");
                    webSocket_wait();
                    break;
                }
                
                usleep(500000);

                if(destroy_flag)
                {
                    if(isNetworkSwitchFromApp)
                    {
                        break;
                    }
                    
                    numTries++;
                    NSLog(@"Native: Corporate: PollingThread : Waiting for Websocket connection num of tries %d AND isNETWORKSWITHC =%d\n",numTries,isNetworkSwitchFromApp);
                    
                    webSocket_deinit();
                    webSocket_wait();
                    usleep(5000000);
                    
                    webSocket_init();
                }
                
                if(numTries == 1)
                {
                    char temp[20] = "websocketred";
                    stausCallback(temp);
                    //usleep(100000);
                    //break;
                }
            }
            
            
            if(numTries < 1)
                NSLog(@"Native: Corporate: PollingThread : After Websocket successful connection...\n");
        }
        
         if ( currentNetworkStatus  == WIFI_ON_TGS_OFF && connection_flag && isBroadcastScreenOn)
         {
             performBroadcastScreenAction(true);
         }
        
        if(isNetworkSwitchFromApp)
        {
            NSLog(@"Native: Exiting: NetworkSwitch Waiting for Websocket close/error\n");
            webSocket_deinit();
            webSocket_wait();
            NSLog(@"Native: Exiting: NetworkSwitch Waiting for Websocket Done....\n");
        }

        
       unsigned short ticks = 0;
       unsigned short tickms = 0;

        
       unsigned long authSeqNum = 0;

       unsigned long int previous_sequence_number = 0;
       unsigned long int dropped_packet_count = 0;
       unsigned long int dropped_packets_per_oration = 0;
       unsigned long int total_packets_per_oration = 0;
       
    /* START: MOS Calculation variables   */

       unsigned long long recvTimeMilliseconds = 0;
       unsigned long long sentTimeMilliseconds = 0;
       long long int trans_Latency = 0;
       long long int sum_Latency = 0;
       double avg_Latency = 0.0;
       struct timeval receivedTimeStamp;
       long long int prev_transLatency = -1000000;
       double interJitter = 0.0;
       double prev_interJitter = 0.0;
       double sumJitter = 0.0;
       double avgJitter = 0.0;
       long int changeInDelay = 0;
       double percentage_packet_loss = 0.0;
       double MOS_Score=0.0;

       unsigned long long orationStartTime = 0.0;
       unsigned long long orationEndTime = 0.0;
       double orationDuration = 0.0;
       double orationPacketsRateInSec = 0.0;
       bool orationStarted = false;


       /* END: MOS Calculation variables   */

       // Read configuration data from DefaultsObject.
       TickWait = 25;
       NSLog(@"TickWait = %d", TickWait);

       MaxTicks = 1000;
       NSLog(@"MaxTicks = %d", MaxTicks);

       AuthenticationInterval = 5000;
       NSLog(@"AuthenticationInterval = %d", AuthenticationInterval);

       AuthenticationNoReceptionInterval = 2000;
       NSLog(@"AuthenticationNoReceptionInterval = %d",
             AuthenticationNoReceptionInterval);
        
       AuthenticationInitialInterval = 250;
       NSLog(@"AuthenticationInitialInterval = %d",
               AuthenticationInitialInterval);

       WiFiScanInterval = 3000;
       NSLog(@"WiFiScanInterval = %d", WiFiScanInterval);

       PlayCreditInterval = 200;
       NSLog(@"PlayCreditInterval = %d", PlayCreditInterval);

       LEDFlashInterval = 500;
       NSLog(@"LEDFlashInterval = %d", LEDFlashInterval);

       NoReceptionInterval = 5000 ;
       NSLog(@"NoReceptionInterval = %d", NoReceptionInterval);

       InitialSilence = 500;
       NSLog(@"Initial Silence = %d", InitialSilence);

       PlayPort = 50008;
       NSLog(@"PlayPort = %d", PlayPort);

       AuthenticationPort = 41103;
       NSLog(@"AuthenticationPort = %d", AuthenticationPort);

       AuthenticationRequestPort = 41102;
       NSLog(@"AuthenticationRequestPort = %d", AuthenticationRequestPort);

       CluePort = 50011;
       NSLog(@"CluePort = %d", CluePort);

       PlayCreditPort = 50007;
       NSLog(@"PlayCreditPort = %d", PlayCreditPort);

       NewOrationPacketsToSkip = 0;
       NSLog(@"NewOrationPacketsToSkip = %d", NewOrationPacketsToSkip);

       NewRecordingPacketsToSkip = 4;
       NSLog(@"NewRecordingPacketsToSkip = %d", NewRecordingPacketsToSkip);

       RecordAmplificationFactor = 1;
       NSLog(@"RecordAmplificationFactor = %f", RecordAmplificationFactor);

       NoReceptionMessageCount = 40;
       NSLog(@"NoReceptionMessageCount = %d", NoReceptionMessageCount);

        struct sockaddr_in TGSAddress;
        memset(&TGSAddress, 0, sizeof TGSAddress);
        TGSAddress.sin_family = AF_INET;
        TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
        TGSAddress.sin_port = htons(AuthenticationRequestPort);
        
        
        int authentication_counter = 0;
        //int led_counter = 0;
        // int wifi_scan_counter = 0;
        int authentication_network_message_difference = 0;
        
        isCommunicatorAlive = true;
        isRegistrationNeeded = true;
        isNoReception = false;
        isReassociatedOnce = false;
        isAudioStreaming = false;
        
        memset(last_oruid, 0, LINESIZE);
        
        signed short last_linear_sample = 0x00;
        
        signed short midscale_ms_of_silence[8];
        memset(midscale_ms_of_silence, last_linear_sample, sizeof(midscale_ms_of_silence));

        
        
        if( currentNetworkStatus <  WIFI_ON_TGS_OFF  )
        {
            //Udp init...
            // Check if the default TGS server address is configured.
            // Create UDP sockets.

            PlayCreditSocket = socket(AF_INET, SOCK_DGRAM, 0);

            NSLog(@"PlayCreditSocket = %d", PlayCreditSocket);

            //const int max_sockets = 2;
            //int s[max_sockets];			// Our sockets

            AUTHENTICATIONSOCKET= socket(AF_INET, SOCK_DGRAM, 0);
            PLAYSOCKET= socket(AF_INET, SOCK_DGRAM, 0);

            if ( (AUTHENTICATIONSOCKET!= -1) && (PLAYSOCKET != -1) )
            {
                // Set the sockets to non-blocking;
                fcntl(AUTHENTICATIONSOCKET, F_SETFL, O_NONBLOCK);
                fcntl(PLAYSOCKET, F_SETFL, O_NONBLOCK);
            }
            else
            {
                NSLog(@"The socket creation Error");
                return 0;
            }

            // Set the playback socket receive buffer to 1M;
            size_t receive_buffer_size = 1000000;

            if (setsockopt(PLAYSOCKET, SOL_SOCKET, SO_RCVBUF, &receive_buffer_size, sizeof(receive_buffer_size)) < 0) {
                NSLog(@"setsockopt error while increasing the receive buffer size.");
            }

            int yes = 1;

            if ( setsockopt(AUTHENTICATIONSOCKET, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1 )
            {
                NSLog(@"setsockopt");
            }

            if ( setsockopt(PLAYSOCKET, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1 )
            {
                NSLog(@"setsockopt");
            }

            // Allow Authentication messages to be broadcast to the subnet.
            // int BroadcastEnableOnSocket = 1;
            // int ret = setsockopt(AUTHENTICATIONSOCKET, SOL_SOCKET, SO_BROADCAST, &BroadcastEnableOnSocket,sizeof(BroadcastEnableOnSocket));

            // Configure the Communicators IP address, Authentication Port, etc. and bind it to the new socket.

           
            memset(&AuthenticationPortAddress, 0, sizeof AuthenticationPortAddress);
            AuthenticationPortAddress.sin_family = AF_INET;
            AuthenticationPortAddress.sin_addr.s_addr = htons(INADDR_ANY);
            AuthenticationPortAddress.sin_port = htons(AuthenticationPort);

            memset(&PlayPortAddress, 0, sizeof PlayPortAddress);
            PlayPortAddress.sin_family = AF_INET;
            PlayPortAddress.sin_addr.s_addr = htons(INADDR_ANY);
            PlayPortAddress.sin_port = htons(PlayPort);
            

            if ((bind(AUTHENTICATIONSOCKET, (struct sockaddr*) &AuthenticationPortAddress, sizeof(AuthenticationPortAddress)) >= 0) &&
                (bind(PLAYSOCKET, (struct sockaddr*) &PlayPortAddress, sizeof(PlayPortAddress)) >= 0))
            {
                NSLog(@"SuccessFul binding to sockets %d or %d", AUTHENTICATIONSOCKET, PLAYSOCKET);
            }

            else {
                NSLog(@"Error binding to sockets %d or %d", AUTHENTICATIONSOCKET, PLAYSOCKET);
                return 0;
            //millisleep(ErrorWait);  // Sleep for a moment to recover.
            }
            
        }
             do  // while isCommunicatorAlive...
             {
                 if(isCommAppStopped)
                 {
                     break;
                 }
                 
                tickms = ticks * TickWait;

                if ((tickms % 1000) == 0) TracePoint("Tick = %d", ticks);

                int something_received = 0;
                
                 //loop_webSocketService();
                // webSocket_loopService();
                 
                
                 
                do {
                    
                    if(isCommAppStopped)
                    {
                        break;  //Stopping the inner do while immediately when app is stopped...
                    }

                    timeval no_time = {0, 0};  // Don't wait.
                    
                    //reopen logic
                    if( isReopened )
                    {
                        
                        if(currentNetworkStatus <  WIFI_ON_TGS_OFF  )
                        {
                            //This code has been added only in case of IOS..to solve onlock socket close problems...
                            emptyAudioPlaybackBuffer();
                            
                            isReopened = false;
                            
                            close(PLAYSOCKET);
                            close(AUTHENTICATIONSOCKET);
                            close(RecordSocket);
                            PLAYSOCKET= socket(AF_INET, SOCK_DGRAM, 0);
                            AUTHENTICATIONSOCKET= socket(AF_INET, SOCK_DGRAM, 0);
                            RecordSocket=socket(AF_INET, SOCK_DGRAM, 0);
                            fcntl(PLAYSOCKET, F_SETFL, O_NONBLOCK);
                            fcntl(AUTHENTICATIONSOCKET, F_SETFL, O_NONBLOCK);
                            
                            // Set the playback socket receive buffer to 1M;
                            size_t receive_buffer_size = 1000000;
                            if (setsockopt(PLAYSOCKET, SOL_SOCKET, SO_RCVBUF, &receive_buffer_size, sizeof(receive_buffer_size)) < 0) {
                                NSLog(@"setsockopt error while increasing the receive buffer size.");
                            }
                            
                            sockaddr_in PlayPortAddress;
                            memset(&PlayPortAddress, 0, sizeof PlayPortAddress);
                            PlayPortAddress.sin_family = AF_INET;
                            PlayPortAddress.sin_addr.s_addr = htons(INADDR_ANY);
                            PlayPortAddress.sin_port = htons(PlayPort);
                            
                            sockaddr_in AuthenticationPortAddress;
                            memset(&AuthenticationPortAddress, 0, sizeof AuthenticationPortAddress);
                            AuthenticationPortAddress.sin_family = AF_INET;
                            AuthenticationPortAddress.sin_addr.s_addr = htons(INADDR_ANY);
                            AuthenticationPortAddress.sin_port = htons(AuthenticationPort);
                            
                            if ((bind(AUTHENTICATIONSOCKET, (struct sockaddr*) &AuthenticationPortAddress, sizeof(AuthenticationPortAddress)) >= 0) &&
                                (bind(PLAYSOCKET, (struct sockaddr*) &PlayPortAddress, sizeof(PlayPortAddress)) >= 0))
                            {
                                NSLog(@"play socket bind sucessfull......");
                                
                                FD_ZERO(&set);
                                
                                for (int i = 0; i < max_sockets; i++)
                                {
                                    FD_SET(s[i], &set);
                                }
                                
                            }
                            else
                            {
                                NSLog(@"play socket bind failed......");
                            }
                        }
                        else if(currentNetworkStatus == WIFI_ON_TGS_OFF)
                        {
                            //This code has been added only in case of IOS..to solve onlock socket close problems...
                            emptyAudioPlaybackBuffer();
                            
                            isReopened = false;
                            
                            NSLog(@"Native Corporate: PollingThread : Waiting for Websocket disconnection in reopen...\n");
                            
                            webSocket_deinit();
                            
                            while(connection_flag)
                            {
                                usleep(10000);
                            }
                            
                            NSLog(@"Native Corporate: PollingThread : Websocket destroy success in reopen...\n");
                            
                            webSocket_init();
                            
                            NSLog(@"Native Corporate: PollingThread : Waiting for Websocket connection in reopen...\n");
                            while(!connection_flag)
                            {
                                usleep(10000);
                            }
                            NSLog(@"Native Corporate: PollingThread : After Websocket successful connection in reopen...\n");
                        }
                        
                        sendPlaycredits();
                        sendPlaycredits();
                        sendPlaycredits();
                        
                    }
                    
                    //
                    
                    
                    if( currentNetworkStatus < WIFI_ON_TGS_OFF  )
                    {
                        FD_ZERO(&set);

                        for (int i = 0; i < max_sockets; i++)
                        {
                            FD_SET(s[i], &set);
                        }
                        
                        something_received = select(FD_SETSIZE, &set, NULL, NULL, &no_time);
        
                
                    
                   if ( something_received )
                   {
                      // Poll all of the sockets that have received packets.

                      for (int i = 0; i < max_sockets; i++)
                      {
                         if (FD_ISSET(s[i], &set))
                         {
                            sockaddr_in from;
                            socklen_t slen = sizeof(from);
                            int byte_count = 0;

                            byte_count = recvfrom(s[i], (void *) buffer, sizeof(buffer), 0, (sockaddr *) &from, &slen);

                            // Handle incoming packets here.

                            if (byte_count > 0 )
                            {
                               isNoReception = false;
                               isReassociatedOnce = false;

                               ntohl(from.sin_addr.s_addr);
                               unsigned int from_port = ntohs(from.sin_port);

                               //Printf("Received %d bytes on Socket[%d] from %s:%d\n", byte_count, s[i], inet_ntoa(from.sin_addr), from_port);

                               cJSON *ResponseObject = NULL;
                               //ResponseObject = cJSON_Parse(cJSON_SingleQuoteToDouble(buffer));
                                
                               ResponseObject = cJSON_Parse(buffer);

                               if (ResponseObject)
                               {
                                  authentication_network_message_difference = 0;

                                  //
                                  // Decode "oruid" JSON message with voice packet suffix.
                                  //

                                  char oruid[LINESIZE];
                                  getJSONString(ResponseObject, oruid, LINESIZE);

                                  if (oruid[0])
                                  {

                                     // All bytes after the final '}' are the binary sound packet...
                                     //
                                     // {"oruid": "_4def0_25_TheatroDevelopment_HOdev1_O", "num": "5", "ver": 1, "length": 200, "time":
                                     // "1368470532049"}O[q\ZNYe???l`c??k[g_d?oh`^g[QMZi??gXPdY[......
                                     //

                                  //MOS changes...
                      if(isMosEnabled){

                          orationEndTime = recvTimeMilliseconds;

                          /* START: One-way Latency Calculation */
                          gettimeofday(&receivedTimeStamp, NULL); // get current time
                          recvTimeMilliseconds = 0;
                          sentTimeMilliseconds = 0;
                          trans_Latency = 0;
                          char time[NAMESIZE];
                          /*convert time into milliseconds*/
                          recvTimeMilliseconds = ( receivedTimeStamp.tv_sec*1000LL ) + ( receivedTimeStamp.tv_usec/1000LL );

                          if(false == orationStarted)
                          {
                              orationStartTime = recvTimeMilliseconds;
                              orationEndTime = recvTimeMilliseconds;
                              orationStarted = true;
                          }

                          getJSONString(ResponseObject, time, NAMESIZE); // extract packet sent time from Json response

                          // typecast char array to unsigned long long and copy to sentTimeMilliseconds
                          sscanf(time, "%llu", &sentTimeMilliseconds);

                          sentTimeMilliseconds = sentTimeMilliseconds + offsetTime;  //This is to synch with TGS timestamp...

                          trans_Latency = ((unsigned long long )recvTimeMilliseconds - (unsigned long long)sentTimeMilliseconds);
                          sum_Latency = sum_Latency + trans_Latency;

                          /* END: one-way latency Calculation */

                          /* START: Jiiter Calculation        */
                          /*For First Packet of Oration ,Jitter Calculation is not required hence reset jitter and  */
                          if(prev_transLatency == -1000000){
                              prev_transLatency = trans_Latency;
                              prev_interJitter = 0;
                              interJitter = 0;
                          }
                          else{
                              unsigned int leftOverBuffer = 0;
                              changeInDelay = abs(trans_Latency-prev_transLatency);
                              prev_transLatency = trans_Latency;
                              interJitter = changeInDelay;
                              
                              //interJitter = prev_interJitter + ((changeInDelay - prev_interJitter)/16.0);

                              //leftOverBuffer = getAvailablePlaybackSpace();

                              //leftOverBuffer = 2000000 - leftOverBuffer; // 2MB size of buffer - avail.space


                              //if ( leftOverBuffer > 400)

                              //interJitterBSK = interJitter;

                              //prev_interJitter = interJitter;

                              //if ( leftOverBuffer > 0)
                            //      interJitter = 0.0;

                              sumJitter= sumJitter+interJitter;
                          }

                      }
                      /* END: Jitter Calculation */

                                     // Find the byte following the last closing brace "}" in the JSON header.
                                     int index = 0;

                                     while (index < MAX_PACKET_SIZE)
                                     {
                                        //if (isprint(buffer[index]))
                                        //{
                                           if (buffer[index] != '}')
                                              index++;
                                           else {
                                              index++;
                                              break; // I hate doing breaks in the middle, but this one makes sense.
                                           }
                                        //}
                                        //else
                                        //   break;
                                     }
                                     int start = index; // This is the start of the sound packet in the buffer.
                                     char num[NAMESIZE];
                                     getJSONString(ResponseObject, num, NAMESIZE);

                                     unsigned long int oration_packet_num = 0;

                                     if (num[0])
                                     {
                                        sscanf(num, "%lu", &oration_packet_num);
                                     }
                                      
                                      //printf("mosValue: Received %lu\n",oration_packet_num);
                                      //Printf("Received oration  %d, isCancelled=%d\n",oration_packet_num,isCancelledFromJava);
                                      
                                      //printf("The oruid = %s The oration number in UDP =%lu \n",oruid,oration_packet_num);

                                     if (strncmp(oruid, last_oruid, LINESIZE) != 0)
                                     {

                                          if(isMosEnabled && total_packets_per_oration > 20) //min total pkt > 20 is to avoid small pkt mos
                          {
                              if(MOS_Reported == false)
                              {

                                  avg_Latency = sum_Latency / total_packets_per_oration;       //

                                  avgJitter = (sumJitter/ (total_packets_per_oration-1));

                                  percentage_packet_loss= (100 * dropped_packets_per_oration)/ total_packets_per_oration;

                                  orationDuration = ( orationEndTime - orationStartTime ) / 1000.0;

                                  if(orationDuration)
                                      orationPacketsRateInSec = total_packets_per_oration / orationDuration;

                                  MOS_Score = TheatroMOS_Calculation(avg_Latency,avgJitter, percentage_packet_loss);

                                  bool isEofOration = true; //making End of Oration s true for previous oration
                                  Send_PlayCredit_Msg(last_oruid, dropped_packets_per_oration ,total_packets_per_oration,isEofOration,MOS_Score, percentage_packet_loss, avgJitter, avg_Latency, orationPacketsRateInSec  );

                                  avg_Latency = 0.0;
                                  avgJitter = 0.0;
                                  percentage_packet_loss = 0.0;
                                  sum_Latency = 0;
                                  sumJitter = 0;
                                  dropped_packets_per_oration = 0;
                                  total_packets_per_oration = 0 ;
                                  orationStarted = false;
                                  orationStartTime = 0.0;
                                  orationEndTime = 0.0;


                              }
                          }
                                        // This is a NEW oration.
                                        dropped_packets_per_oration = 0;
                                        total_packets_per_oration = 0 ;
                                        strncpy(last_oruid, oruid, LINESIZE);

                                        if (isDebugging)
                                        {
                                           char *cRet = cJSON_PrintUnformatted(ResponseObject);// Workaround for cJSON leak.
                                           if (cRet)
                                           {
                                              TracePoint("Received %s on socket %d from %s:%d", cRet, s[i], inet_ntoa(from.sin_addr), from_port);
                                              free(cRet);
                                           }// Workaround for cJSON leak.
                                        }

                                        // Inject millisilence ms of silence before the new oration.
                                         
                                         if(oration_packet_num < 3)
                                         {
                                             isCancelledFromJava = false;

                                         }

                     

                                        //	const int millisilence = 500; //made as configurable parameter..


                                        if(!isCancelledFromJava)
                                        {
                                            TracePoint("!!! New oration, adding %d ms of mid-scale silence !!!", InitialSilence);
                                            //Initial silence...
                                            for (int i = 0; i < InitialSilence; i++)
                                            {
                                                if(isBroadcastScreenOn)
                                                    playBuffer((short int *) midscale_ms_of_silence,sizeof(midscale_ms_of_silence));
                                            }
                                            startPlayback();
                                        }
                                        else
                                        {
                                            memset(last_oruid, 0, LINESIZE);
                                        }
                                        MOS_Reported = false;
                                         
                                     }
                      dropped_packet_count = 0;
                      total_packets_per_oration ++;

                      if (oration_packet_num > 0)
                      {
                          if (oration_packet_num > previous_sequence_number + 1)
                          {
                              dropped_packet_count = oration_packet_num - previous_sequence_number - 1;

                              if (dropped_packet_count > 0)
                              {
                                  NSLog(@"!!! Network dropped %d packets !!! oration_packet_num = %d, previous_sequence_number = %d",
                                          dropped_packet_count,
                                          oration_packet_num,
                                          previous_sequence_number);
                                  dropped_packets_per_oration += dropped_packet_count;
                              }
                          }
                          previous_sequence_number = oration_packet_num;
                      }

                                     int ver = 0;
                                     getJSONInt(ResponseObject, ver);

                                     int length = 0;
                                     getJSONInt(ResponseObject, length);

                                     if (oration_packet_num >= NewOrationPacketsToSkip)
                                     {
                                        //Printf("Received Packet %d,isBroadcast=%d, isCancelled=%d,CurrentTime=%ld", oration_packet_num,isBroadcastScreenOn,isCancelledFromJava,getCurrentTimeMillis());
                                         
                                        if (length != 0 && !isCancelledFromJava)
                                        {
                                           int end = start + length; // This is the end of the sound packet in the buffer.

                                           TracePoint("Received Packet %d, start = %d, end = %d", oration_packet_num, start, end);

                                           int audio_length_bytes = length * sizeof(signed short);

                                           signed short audiobuffer[audio_length_bytes];

                                           if (true)
                                           {
                                              //Expect uLaw
                                              int loop;
                                              for (loop = 0; loop < length; loop++)
                                              {
                                                 audiobuffer[loop] = ulaw2linear(buffer[start + loop]);// New code 7/16/13
                                              }
                                           }
                                            //Printf("isBroadcastScreenOn=%d",isBroadcastScreenOn);
                                            if(isBroadcastScreenOn)
                                            {
                                                //Printf("isBroadcastScreenOn after=%d",isBroadcastScreenOn);
                                              playBuffer((short int *) audiobuffer,audio_length_bytes);
                                            }
                                           millisleep(1);
                                        }
                                     }
                                     else {
                                        TracePoint("NewOrationPacketsToSkip: oration_packet_num %d < %d", oration_packet_num, NewOrationPacketsToSkip);
                                     }

                                     if (length == 0)
                                     {
                                        // This is the last packet of an oration.
                                         NSLog(@"Received 0 length packet.......\n");
                     // This is the last packet of an oration and send  the MOS Score to TGS now .
                          if(isMosEnabled && total_packets_per_oration > 20)
                          {

                              avg_Latency= sum_Latency/ total_packets_per_oration;

                              avgJitter = (sumJitter/ (total_packets_per_oration-1));

                              percentage_packet_loss= (100 * dropped_packets_per_oration)/ total_packets_per_oration;

                              orationEndTime = recvTimeMilliseconds;
                              orationDuration = ( orationEndTime - orationStartTime ) / 1000.0;

                              if(orationDuration)
                                  orationPacketsRateInSec = total_packets_per_oration / orationDuration;

                              MOS_Score = TheatroMOS_Calculation(avg_Latency,avgJitter, percentage_packet_loss);
                              bool isEofOration = true;
                              Send_PlayCredit_Msg(last_oruid, dropped_packets_per_oration ,total_packets_per_oration,isEofOration,MOS_Score, percentage_packet_loss, avgJitter,  avg_Latency,orationPacketsRateInSec );

                              /*Reset the mos variable to zero*/
                              avg_Latency = 0.0;
                              avgJitter = 0.0;
                              percentage_packet_loss = 0.0;
                              sum_Latency = 0;
                              sumJitter = 0;
                              // reset the counters
                              dropped_packets_per_oration = 0;
                              total_packets_per_oration = 0 ;

                              MOS_Reported = true;
                              orationStartTime = 0.0;
                              orationEndTime = 0.0;
                              orationStarted = false;

                          }

                                       //  isAudioStreaming = false;
                                     }
                                  }  // oruid message

                                  //
                                  // Decode "networks" JSON message with valid WiFi APs, RSSI thresholds, etc.
                                  //
                                  cJSON *networks = cJSON_GetObjectItem(ResponseObject, "networks");
                                  if (networks)
                                  {

                                     if(isTGSConnected == false)
                                     {
                                         isRegistrationNeeded = false;
                                         isTGSConnected = true;
                                         NoReceptionMessageCount = 4;
                                         sendPlaycredits();
                                         sendPlaycredits();
                                         sendPlaycredits();
                                         
                                         char connected[20] = "connected";
                                         stausCallback(connected);
                                         NSLog(@"=========The received Networks=======\n");
                                     }
                                     // We received a JSON formatted Networks message.

                                     if (isDebugging)
                                     {
                                        char *cRet = cJSON_PrintUnformatted(ResponseObject);// Workaround for cJSON leak.
                                        if (cRet)
                                        {
                                           TracePoint("Received %s on socket %d from %s:%d\n", cRet, s[i], inet_ntoa(from.sin_addr), from_port);
                                           free(cRet);
                                        }										// Workaround for cJSON leak.
                                     }

                                     // Update the TgsIp according to the serverip returned by TGS in the Networks response message.
                                     char serverip[NAMESIZE];
                                     getJSONString(ResponseObject, serverip, NAMESIZE);
                                     // Ravi.S - TCS Aug 4, 2013, updated the code
                                     // to read the ChargeTheshold
                                     getJSONInt(ResponseObject, ChargedThreshold);
                                     TracePoint("ChargedThreshold value %d", ChargedThreshold);
                                     getJSONInt(ResponseObject, PlayCreditInterval);
                                     TracePoint("PlayCreditInterval value %d", PlayCreditInterval);
                      
                                   
                                      if(ullAuthRecvTime && ullAuthSendTime)
                                      {
                                         char AuthRecvTime[NAMESIZE];
                                         getJSONString(ResponseObject,AuthRecvTime , NAMESIZE);
                                         sscanf(AuthRecvTime, "%llu", &ullAuthRecvTime);


                                         char AuthSendTime[NAMESIZE];
                                         getJSONString(ResponseObject,AuthSendTime , NAMESIZE);
                                         sscanf(AuthSendTime, "%llu", &ullAuthSendTime);


                                      //Printf("mosValue SendTimeStamp value %llu\n", ullAuthSendTime);
                                      //Printf("mosValue TGS recvTimeStamp value %llu\n", ullAuthRecvTime);
                                      NetworkRecvTime = getCurrentTimeMillis();
                                      //Printf("mosValue Netwokrks RecvTimestamp value %llu\n", NetworkRecvTime);
                                      TrasmissionTime = (NetworkRecvTime - ullAuthSendTime)/2.0;
                                      //Printf("mosValue Trasmission time %lf\n", TrasmissionTime);
                                      offsetTime = (ullAuthSendTime + TrasmissionTime) - ullAuthRecvTime;
                                     
                                          /*
                                      if(offsetTime >= 0)
                                      {
                                          printf("mosValue Manager app is ahead... offset value %lld\n",offsetTime );
                                      }
                                      else
                                      {
                                          printf("mosValue TGS is ahead... offset value %lld\n",offsetTime );
                                      }
                                      */
                                          
                                      }


                                     // We received a "networks" message.  It should look something like this...
                                     //
                                     //     {"networks":[{"ssid":"827RF1","bssid":"00:12:44:b8:24:91"},
                                     //                  {"ssid":"827RF1","bssid":"00:1e:4a:31:cc:c1"},
                                     //                  {"ssid":"827RF1","bssid":"00:1e:4a:31:d3:21"}],
                                     //      'serverip':'10.0.0.12', 'DeltaRSSI':100, 'Hysteresis':5, 'PlayCreditInterval':100,
                                     //      'minRSSI':-60, 'ShowScan':'827RF1.*', 'uLawPCM':True}

                                     if (authentication_network_message_difference-- < 0) authentication_network_message_difference = 0;

                                  }  // networks message
                                   
                                  cJSON *coverage = cJSON_GetObjectItem(ResponseObject, "coverage");
                                   
                                   
                                  if(coverage)
                                  {
                                      coverageStr = cJSON_PrintUnformatted(ResponseObject);
                                      
                                      
                                      char cvgver[NAMESIZE] = {0};
                                      getJSONString(ResponseObject,cvgver, NAMESIZE);
                                      
                                      
                                      bool IFC = false;
                                      getJSONBool(ResponseObject,IFC);
                                      
                                      
                                      //Printf("=========The received coverage=%s, The cvgVer=%u, the IFC=%d",coverageStr,atoi(cvgver),IFC);
                                      
    #if 0
                                      if(cvgver[0] != 0)
                                      {
                                          if( gCoverageVersion + 1 == atoi(cvgver) || gCoverageVersion == atoi(cvgver))
                                          {
                                              gCoverageVersion = atoi(cvgver);
                                              stausCallback(coverageStr);
                                          }
                                          else
                                          {
                                              if(IFC)
                                              {
                                                  stausCallback(coverageStr);
                                                  gCoverageVersion = atoi(cvgver);
                                              }
                                          }
                                      }
                                      else
                                      {
                                          stausCallback(coverageStr);
                                      }
    #endif
                                      stausCallback(coverageStr);
                                     
                                  }
                                   
                                  //
                                  // Decode "soundout" JSON message for playing pre-recorded messages and silencing.
                                  //
                                  char soundout[NAMESIZE];
                                  getJSONString(ResponseObject, soundout, NAMESIZE);

                                  if (soundout[0])
                                  {
                                     if (strncmp(soundout, "silence", NAMESIZE) == 0)
                                     {
                                        emptyAudioPlaybackBuffer();
                                     }

                                     if (strncmp(soundout, "commandmodesound", NAMESIZE) == 0)
                                     {
                                        char resourcename[NAMESIZE];
                                        getJSONString(ResponseObject, resourcename, NAMESIZE);

                                        if (resourcename[0])
                                        {
                                           if (strncmp(resourcename, "ker-ching", NAMESIZE) == 0)
                                           {
                                               //playBuffer(kerChingBuff , sizeof(kerChingBuff));
                                           }
                                        }  // resource message
                                     }

                                     if (isDebugging)
                                     {
                                        char *cRet = cJSON_PrintUnformatted(ResponseObject);// Workaround for cJSON leak.
                                        if (cRet)
                                        {
                                           TracePoint("Received %s on socket %d from %s:%d", cRet, s[i], inet_ntoa(from.sin_addr), from_port);
                                           free(cRet);
                                        }
                                     }											// Workaround for cJSON leak.
                                  } // soundout message

                               }
                               else {
                                  //Printf("ResponseObject Parse Error: %s", cJSON_GetErrorPtr());
                               }
                               //isWebSocketDataAvailable = false;
                               cJSON_Delete(ResponseObject);
                                
                            }  // byte_count > 0
                             
                             
                         }
                      }  // Loop over sockets
                   }// Select on sockets
                   }
                   else if( currentNetworkStatus == WIFI_ON_TGS_OFF)
                   {
                       isNoReception = false;
                       isReassociatedOnce = false;
                       
                       cJSON *ResponseObject = NULL;
                       //ResponseObject = cJSON_Parse(cJSON_SingleQuoteToDouble(buffer));
                       
                       if( isWebSocketDataAvailable)
                       {
                           if(getIndex != putIndex)
                           {
                               ResponseObject = cJSON_Parse(inputBuf[getIndex]);
                           }
                           else
                           {
                               isWebSocketDataAvailable = 0;
                               continue;
                           }
                       }
                       
                       if (ResponseObject)
                       {
                           authentication_network_message_difference = 0;
                           
                           char oruid[LINESIZE];
                           getJSONString(ResponseObject, oruid, LINESIZE);
                           
                           if (oruid[0])
                           {
                               //printf("The count is %d\n",countOra);
       //MOS changes...
                 if(isMosEnabled){

                     orationEndTime = recvTimeMilliseconds;

                     /* START: One-way Latency Calculation */
                     gettimeofday(&receivedTimeStamp, NULL); // get current time
                     recvTimeMilliseconds = 0;
                     sentTimeMilliseconds = 0;
                     trans_Latency = 0;
                     char time[NAMESIZE];
                     /*convert time into milliseconds*/
                     recvTimeMilliseconds = ( receivedTimeStamp.tv_sec*1000LL ) + ( receivedTimeStamp.tv_usec/1000LL );

                     if(false == orationStarted)
                     {
                         orationStartTime = recvTimeMilliseconds;
                         orationEndTime = recvTimeMilliseconds;
                         orationStarted = true;
                     }

                     getJSONString(ResponseObject, time, NAMESIZE); // extract packet sent time from Json response

                     // typecast char array to unsigned long long and copy to sentTimeMilliseconds
                     sscanf(time, "%llu", &sentTimeMilliseconds);

                     sentTimeMilliseconds = sentTimeMilliseconds + offsetTime;  //This is to synch with TGS timestamp...

                     trans_Latency = ((unsigned long long )recvTimeMilliseconds - (unsigned long long)sentTimeMilliseconds);
                     sum_Latency = sum_Latency + trans_Latency;

                     /* END: one-way latency Calculation */

                     /* START: Jiiter Calculation        */
                     /*For First Packet of Oration ,Jitter Calculation is not required hence reset jitter and  */
                     if(prev_transLatency == -1000000){
                         prev_transLatency = trans_Latency;
                         prev_interJitter = 0;
                         interJitter = 0;
                     }
                     else{
                         unsigned int leftOverBuffer = 0;
                         changeInDelay = abs(trans_Latency-prev_transLatency);
                         prev_transLatency = trans_Latency;
                         interJitter = changeInDelay;

                         //interJitter = prev_interJitter + ((changeInDelay - prev_interJitter)/16.0);

                         //leftOverBuffer = getAvailablePlaybackSpace();

                         //leftOverBuffer = 2000000 - leftOverBuffer; // 2MB size of buffer - avail.space

                         //if ( leftOverBuffer > 400)

                         //interJitterBSK = interJitter;

                         //prev_interJitter = interJitter;

                         //if ( leftOverBuffer > 0)
                         //      interJitter = 0.0;

                         sumJitter= sumJitter+interJitter;
                     }

                 }
                 /* END: Jitter Calculation */

                               countOra = 0;
                               int index = 0;
                               
                               while (index < MAX_PACKET_SIZE)
                               {
                                   if (inputBuf[getIndex][index])
                                   {
                                       if (inputBuf[getIndex][index] != '}')
                                           index++;
                                       else {
                                           index++;
                                           break; // I hate doing breaks in the middle, but this one makes sense.
                                       }
                                   }
                                   else
                                       break;
                               }
                               int start = index; // This is the start of the sound packet in the buffer.
                               char num[NAMESIZE];
                               getJSONString(ResponseObject, num, NAMESIZE);
                               
                               unsigned long int oration_packet_num = 0;
                               
                               if (num[0])
                               {
                                   sscanf(num, "%lu", &oration_packet_num);
                               }
                               
                               //printf("The oruid = %s The oration number in WS =%lu\n",oruid,oration_packet_num);
                               
                               if (strncmp(oruid, last_oruid, LINESIZE) != 0)
                               {
         if(isMosEnabled && total_packets_per_oration > 20) //min total pkt > 20 is to avoid small pkt mos
                     {
                         if(MOS_Reported == false)
                         {

                             avg_Latency = sum_Latency / total_packets_per_oration;       //

                             avgJitter = (sumJitter/ (total_packets_per_oration-1));

                             percentage_packet_loss= (100 * dropped_packets_per_oration)/ total_packets_per_oration;

                             orationDuration = ( orationEndTime - orationStartTime ) / 1000.0;

                             if(orationDuration)
                                 orationPacketsRateInSec = total_packets_per_oration / orationDuration;

                             MOS_Score = TheatroMOS_Calculation(avg_Latency,avgJitter, percentage_packet_loss);

                             bool isEofOration = true; //making End of Oration s true for previous oration
                             Send_PlayCredit_Msg(last_oruid, dropped_packets_per_oration ,total_packets_per_oration,isEofOration,MOS_Score, percentage_packet_loss, avgJitter, avg_Latency, orationPacketsRateInSec  );

                             avg_Latency = 0.0;
                             avgJitter = 0.0;
                             percentage_packet_loss = 0.0;
                             sum_Latency = 0;
                             sumJitter = 0;
                             dropped_packets_per_oration = 0;
                             total_packets_per_oration = 0 ;
                             orationStarted = false;
                             orationStartTime = 0.0;
                             orationEndTime = 0.0;


                         }
                     }
                                   // This is a NEW oration.
                                   strncpy(last_oruid, oruid, LINESIZE);
                                   
                       dropped_packets_per_oration = 0;
                       total_packets_per_oration = 0 ; 
                                   if (isDebugging)
                                   {
                                       char *cRet = cJSON_PrintUnformatted(ResponseObject);// Workaround for cJSON leak.
                                       if (cRet)
                                       {
                                           free(cRet);
                                       }// Workaround for cJSON leak.
                                   }
                                   
                                   // Inject millisilence ms of silence before the new oration.
                                   
                                   if(oration_packet_num < 3)
                                   {
                                       isCancelledFromJava = false;
                                       
                                   }
                                   
                                   
                                   if(!isCancelledFromJava)
                                   {
                                       TracePoint("!!! New oration, adding %d ms of mid-scale silence !!!", InitialSilence);
                                       //Initial silence...
                                       for (int i = 0; i < InitialSilence; i++)
                                       {
                                           if(isBroadcastScreenOn)
                                               playBuffer((short int *) midscale_ms_of_silence,sizeof(midscale_ms_of_silence));
                                       }
                                       startPlayback();
                                   }
                                   else
                                   {
                                       memset(last_oruid, 0, LINESIZE);
                                   }
                                   MOS_Reported = false;
                                   
                               }

                     dropped_packet_count = 0;
                 total_packets_per_oration ++;

                 if (oration_packet_num > 0)
                 {
                     if (oration_packet_num > previous_sequence_number + 1)
                     {
                         dropped_packet_count = oration_packet_num - previous_sequence_number - 1;

                         if (dropped_packet_count > 0)
                         {
                             NSLog(@"!!! Network dropped %d packets !!! oration_packet_num = %d, previous_sequence_number = %d",
                                     dropped_packet_count,
                                     oration_packet_num,
                                     previous_sequence_number);
                             dropped_packets_per_oration += dropped_packet_count;
                         }
                     }
                     previous_sequence_number = oration_packet_num;
                 }
                               
                               int ver = 0;
                               getJSONInt(ResponseObject, ver);
                               
                               int length = 0;
                               getJSONInt(ResponseObject, length);
                               
                               if (oration_packet_num >= NewOrationPacketsToSkip)
                               {
                                   //Printf("Received Packet %d,isBroadcast=%d, isCancelled=%d,CurrentTime=%ld", oration_packet_num,isBroadcastScreenOn,isCancelledFromJava,getCurrentTimeMillis());
                                   
                                   if (length != 0 && !isCancelledFromJava)
                                   {
                                       int end = start + length; // This is the end of the sound packet in the buffer.
                                       
                                       TracePoint("Received Packet %d, start = %d, end = %d", oration_packet_num, start, end);
                                       
                                       int audio_length_bytes = length * sizeof(signed short);
                                       
                                       signed short audiobuffer[audio_length_bytes];
                                       
                                       if (true)
                                       {
                                           //Expect uLaw
                                           int loop;
                                           for (loop = 0; loop < length; loop++)
                                           {
                                               audiobuffer[loop] = ulaw2linear(inputBuf[getIndex][start + loop]);// New code 7/16/13
                                               
                                           }
                                       }
                                       //Printf("isBroadcastScreenOn=%d",isBroadcastScreenOn);
                                       if(isBroadcastScreenOn)
                                       {
                                           //Printf("isBroadcastScreenOn after=%d",isBroadcastScreenOn);
                                           playBuffer((short int *) audiobuffer,audio_length_bytes);
                                       }
                                       millisleep(1);
                                   }
                               }
                               else {
                                   TracePoint("NewOrationPacketsToSkip: oration_packet_num %d < %d", oration_packet_num, NewOrationPacketsToSkip);
                               }
                               
                               if (length == 0)
                               {
                                   // This is the last packet of an oration.
                                   NSLog(@"Received 0 length packet.......\n");
     // This is the last packet of an oration and send  the MOS Score to TGS now .
                     if(isMosEnabled && total_packets_per_oration > 20)
                     {

                         avg_Latency= sum_Latency/ total_packets_per_oration;

                         avgJitter = (sumJitter/ (total_packets_per_oration-1));

                         percentage_packet_loss= (100 * dropped_packets_per_oration)/ total_packets_per_oration;

                         orationEndTime = recvTimeMilliseconds;
                         orationDuration = ( orationEndTime - orationStartTime ) / 1000.0;

                         if(orationDuration)
                             orationPacketsRateInSec = total_packets_per_oration / orationDuration;

                         MOS_Score = TheatroMOS_Calculation(avg_Latency,avgJitter, percentage_packet_loss);
                         bool isEofOration = true;
                         Send_PlayCredit_Msg(last_oruid, dropped_packets_per_oration ,total_packets_per_oration,isEofOration,MOS_Score, percentage_packet_loss, avgJitter,  avg_Latency,orationPacketsRateInSec );

                         /*Reset the mos variable to zero*/
                         avg_Latency = 0.0;
                         avgJitter = 0.0;
                         percentage_packet_loss = 0.0;
                         sum_Latency = 0;
                         sumJitter = 0;
                         // reset the counters
                         dropped_packets_per_oration = 0;
                         total_packets_per_oration = 0 ;

                         MOS_Reported = true;
                         orationStartTime = 0.0;
                         orationEndTime = 0.0;
                         orationStarted = false;

                     }
                                   
                                   //  isAudioStreaming = false;
                               }
                           }  // oruid message
                           
                           //
                           // Decode "networks" JSON message with valid WiFi APs, RSSI thresholds, etc.
                           //
                           cJSON *networks = cJSON_GetObjectItem(ResponseObject, "networks");
                           if (networks)
                           {
                               
                               if(isTGSConnected == false)
                               {
                                   isRegistrationNeeded = false;
                                   isTGSConnected = true;
                                   NoReceptionMessageCount = 4;
                                   sendPlaycredits();
                                   sendPlaycredits();
                                   sendPlaycredits();
                                   
                                   char connected[20] = "connected";
                                   stausCallback(connected);
                                   NSLog(@"=========The received Networks=======\n");
                               }
                               // We received a JSON formatted Networks message.
                               
                               if (isDebugging)
                               {
                                   char *cRet = cJSON_PrintUnformatted(ResponseObject);// Workaround for cJSON leak.
                                   if (cRet)
                                   {
                                       free(cRet);
                                   }										// Workaround for cJSON leak.
                               }
                               
                               // Update the TgsIp according to the serverip returned by TGS in the Networks response message.
                               char serverip[NAMESIZE];
                               getJSONString(ResponseObject, serverip, NAMESIZE);
                               // Ravi.S - TCS Aug 4, 2013, updated the code
                               // to read the ChargeTheshold
                               getJSONInt(ResponseObject, ChargedThreshold);
                               TracePoint("ChargedThreshold value %d", ChargedThreshold);
                               getJSONInt(ResponseObject, PlayCreditInterval);
                               TracePoint("PlayCreditInterval value %d", PlayCreditInterval);
                      
                                         char AuthRecvTime[NAMESIZE];
                                 getJSONString(ResponseObject,AuthRecvTime , NAMESIZE);
                                 sscanf(AuthRecvTime, "%llu", &ullAuthRecvTime);


                                 char AuthSendTime[NAMESIZE];
                                 getJSONString(ResponseObject,AuthSendTime , NAMESIZE);
                                 sscanf(AuthSendTime, "%llu", &ullAuthSendTime);


                                 if(ullAuthRecvTime && ullAuthSendTime)
                                 {
                                 //Printf("mosValue SendTimeStamp value %llu\n", ullAuthSendTime);
                                 //Printf("mosValue TGS recvTimeStamp value %llu\n", ullAuthRecvTime);
                                 NetworkRecvTime = getCurrentTimeMillis();
                                 //Printf("mosValue Netwokrks RecvTimestamp value %llu\n", NetworkRecvTime);
                                 TrasmissionTime = (NetworkRecvTime - ullAuthSendTime)/2.0;
                                 //Printf("mosValue Trasmission time %lf\n", TrasmissionTime);
                                 offsetTime = (ullAuthSendTime + TrasmissionTime) - ullAuthRecvTime;
                                 
                                     /*
                                  if(offsetTime >= 0)
                                 {
                                     printf("mosValue Manager app is ahead... offset value %lld\n",offsetTime );
                                 }
                                 else
                                 {
                                     printf("mosValue TGS is ahead... offset value %lld\n",offsetTime );
                                 }
                                  */
                                 }

                               if (authentication_network_message_difference-- < 0) authentication_network_message_difference = 0;
                               
                           }  // networks message
                           
                           cJSON *coverage = cJSON_GetObjectItem(ResponseObject, "coverage");
                           



                           
                           if(coverage)
                           {
                               coverageStr = cJSON_PrintUnformatted(ResponseObject);
                               
                               
                               char cvgver[NAMESIZE] = {0};
                               getJSONString(ResponseObject,cvgver, NAMESIZE);
                               
                               
                               bool IFC = false;
                               getJSONBool(ResponseObject,IFC);
                               
                               
                               //Printf("=========The received coverage=%s, The cvgVer=%u, the IFC=%d",coverageStr,atoi(cvgver),IFC);
                               
    #if 0
                               if(cvgver[0] != 0)
                               {
                                   if( gCoverageVersion + 1 == atoi(cvgver) || gCoverageVersion == atoi(cvgver))
                                   {
                                       gCoverageVersion = atoi(cvgver);
                                       stausCallback(coverageStr);
                                   }
                                   else
                                   {
                                       if(IFC)
                                       {
                                           stausCallback(coverageStr);
                                           gCoverageVersion = atoi(cvgver);
                                       }
                                   }
                               }
                               else
                               {
                                   stausCallback(coverageStr);
                               }
                               
    #endif
                           stausCallback(coverageStr);
                           }
                           
                           //
                           // Decode "soundout" JSON message for playing pre-recorded messages and silencing.
                           //
                           char soundout[NAMESIZE];
                           getJSONString(ResponseObject, soundout, NAMESIZE);
                           
                           if (soundout[0])
                           {
                               if (strncmp(soundout, "silence", NAMESIZE) == 0)
                               {
                                   emptyAudioPlaybackBuffer();
                               }
                               
                               if (strncmp(soundout, "commandmodesound", NAMESIZE) == 0)
                               {
                                   char resourcename[NAMESIZE];
                                   getJSONString(ResponseObject, resourcename, NAMESIZE);
                                   
                                   if (resourcename[0])
                                   {
                                       if (strncmp(resourcename, "ker-ching", NAMESIZE) == 0)
                                       {
                                           //playBuffer(kerChingBuff , sizeof(kerChingBuff));
                                       }
                                   }  // resource message
                               }
                               
                               if (isDebugging)
                               {
                                   char *cRet = cJSON_PrintUnformatted(ResponseObject);// Workaround for cJSON leak.
                                   if (cRet)
                                   {
                                       free(cRet);
                                   }
                               }											// Workaround for cJSON leak.
                           } // soundout message
                           
                           //increment the getIndex at the end of receive...
                           
                           if(getIndex == MAX_NUM_PACKETS-1 )
                               getIndex = 0;
                           else
                               getIndex = getIndex + 1;
                       }
                       else {
                           //Printf("ResponseObject Parse Error: %s", cJSON_GetErrorPtr());
                       }
                       //isWebSocketDataAvailable = false;
                       cJSON_Delete(ResponseObject);
                   }
                }while (something_received != 0 || isWebSocketDataAvailable != 0);

                // All input has been processed at this point.  Begin performing timed actions.
                //
                // PlayCredits
                //
                // Be sure to multiply PlayCreditInterval by 10 because TGS uses tens of milliseconds.
                //
                if ((tickms % (PlayCreditInterval * 10)) == 0)
                {
                   //
                   // PlayCredits messages
                   //

                   if (!isRegistrationNeeded)// If we are registered...
                   {
                       sendPlaycredits();
                   }
                   // Even if the PlayCredit message could not be sent, increment the sequence numbers and clear the dropped packets
                   // so TGS can accurately understand how long the communicator was off the air.
                   PlayCreditSeqno++;// Increment the sequence number.
                }

                //
                // Authentication / Registration Message
                //
                int authentication_interval = AuthenticationInterval;

                if (!isTGSConnected)
                {
                    authentication_interval = AuthenticationInitialInterval;
                }
                 
                if(isNoReception)
                {
                    authentication_interval = AuthenticationNoReceptionInterval;
                }
                 
                if ((authentication_counter++ * TickWait) > authentication_interval || (authSeqNum == 0))
                {
                   // Build and send a JSON-formatted Authentication message

                    if(sendAuths())
                    {
                        if (authentication_network_message_difference++ >= NoReceptionMessageCount)
                        {
                            if(isNoReception == false)
                            {
                                isNoReception = true;
                                isTGSConnected = false;

                                char connected[20] = "disconnected";
                                stausCallback(connected);
                                emptyAudioPlaybackBuffer();
                                if (isAudioStreaming)
                                {
                                    isAudioStreaming = false;
                                }
                                isRegistrationNeeded = true;
                            }
                            
                            NSLog(@"111RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR NoReception RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR (No Reception)");

                        }
                    }
                    else
                    {
                        if(isNoReception == false)
                        {
                            isNoReception = true;
                            isTGSConnected = false;
                            
                            char connected[20] = "disconnected";
                            stausCallback(connected);
                            emptyAudioPlaybackBuffer();
                            if (isAudioStreaming)
                            {
                                isAudioStreaming = false;
                            }
                            isRegistrationNeeded = true;
                        }
                        
                        exitCommunicator();
                        
                        NSLog(@"222 RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR NoReception RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR (No Reception)");
                        
                    }
                   authentication_counter = 0;
                   authSeqNum ++;
                }

                //
                // All activities are done, sleep for TickWait.
                //

                millisleep(TickWait);

                if (ticks++ > MaxTicks) ticks = 0;// Click the ticks.

                //
                // Start over again.
                //
             }
             while (isCommunicatorAlive);

       //clearJavaEnv();

       NSLog(@"PollingThread Exiting...\n");
       emptyAudioPlaybackBuffer();
       emptyAudioRecordingBuffer();
       pthread_exit(NULL);
    }

    void reOpenSockets()
    {
        numTries = 0;
        isReopened = true;
    }

    void exitCommunicator() {
       Begin;

       NSLog(@
             "****************************** theatroExit! ******************************");


       isCommunicatorAlive = false; //this variable is set to gracefully exit the polling thread...
       isNoReception = true; //this variable is set to gracefully exit the recorder thread...
       isSimulating = false; //this variable is set to gracefully exit the simulator thread...
       isCommAppStopped = true;//this variable is set to gracefully exit the Polling thread...

       //only in case of simsock, shutdown is used to rewive the thread and exit gracefully...
        
        if(currentNetworkStatus < WIFI_ON_TGS_OFF)
        {
            //close the sockets...
            close(AUTHENTICATIONSOCKET);
            close(PLAYSOCKET);
            close(RecordSocket);
        }
        else if( currentNetworkStatus ==  WIFI_ON_TGS_OFF )
        {
           webSocket_deinit();
            NSLog(@"Native Corporate :  exitCommunicator before while \n");
            while(connection_flag)
            {
                usleep(10000);
            }
            NSLog(@"Native Corporate :  exitCommunicator after while \n");
           pthread_join(WebSocketThreadHandle, NULL);
        }


       //wait for the created threads to exit.. for graceful Exit...
       pthread_join(RecorderThreadHandle, NULL);
       pthread_join(PollingThreadHandle, NULL);

        NSLog(@
               "****************************** theatroExit after Join ******************************\n");
    }

    void performCommunicatorExit(int sig) {
       NSLog(@"Communicator received a %d signal.\n", sig);

       exitCommunicator();

       pthread_exit(0);
    }

    void startCommunicator() {
       Begin;

       currentButton = None;
       isPrimaryButtonPushed = false;
       isBroadcastButtonPushed = false;
       isVolumeUpButtonPushed = false;
       isVolumeDownButtonPushed = false;

       isRegistrationNeeded = true;
       isCommAppStopped = false;

       BreadCrumb;

       RecordSocket = 0;
       PlayCreditSocket = 0;

       isCharging = false;
       isCharged = false;

       struct hostent *h;

       isTGSConnected = false;


       struct sockaddr_in TGSAddress;
       memset(&TGSAddress, 0, sizeof TGSAddress);
       TGSAddress.sin_family = AF_INET;

        if ((h = gethostbyname(TGSIPAddress)) == NULL) {
            strncpy(TgsIp, "0.0.0.0", NAMESIZE);
        } else {
            strncpy(TgsIp, inet_ntoa(*((struct in_addr *) h->h_addr)),NAMESIZE);
        }
        
        if (inet_addr(TGSIPAddress) == INADDR_ANY ) {
            
            strncpy(TgsIp, "0.0.0.0", NAMESIZE);
            TGSAddress.sin_addr.s_addr = getNetworkMask(
                                                        CommunicatorWiFiInterfaceName);
            NSLog(@"TgsIp = %s (Broadcasting to local subnet.)", TgsIp);
            AuthenticationBroadcast = true;
        } else {
            TGSAddress.sin_addr.s_addr = inet_addr(TgsIp); //(*(h->h_addr));
        }
        
         if(currentNetworkStatus == WIFI_OFF_CELLULAR_ON)
        {
            isIpSetFromApp = false;
            NSLog(@"Native: dataNetwork : WIFI_OFF_CELLULAR_ON \n");
            
        }else if ( currentNetworkStatus == WIFI_ON_TGS_OFF )
        {
            isIpSetFromApp = false;
            NSLog(@"Native: Corporate: WIFI_ON_TGS_OFF \n");
        }
        else if (currentNetworkStatus == WIFI_ON_TGS_ON )
        {
            NSLog(@"Native: instore : WIFI_ON_TGS_ON\n");
        }
        else if (currentNetworkStatus == WIFI_OFF_CELLULAR_OFF )
        {
            NSLog(@"Native: instore : WIFI_OFF_CELLULAR_OFF\n");
            char connected[20] = "disconnected";
            stausCallback(connected);
            return;
        }
        
        
        NSLog(@"TGSIPAddress is %s AND tgsIp=%s\n", TGSIPAddress,TgsIp);

        if(currentNetworkStatus < WIFI_ON_TGS_OFF)
        {
           RecordPort = 50009;
           NSLog(@"RecordPort = %d", RecordPort);

           TGSAddress.sin_port = htons(RecordPort);

           // Create a UDP socket for recording.

           RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);

           NSLog(@"RecordSocket = %d", RecordSocket);

           if (RecordSocket != -1) {
              // Configure the Communicators IP address, etc. and bind it to the new socket.
              struct sockaddr_in CommunicatorAddress;
              memset(&CommunicatorAddress, 0, sizeof CommunicatorAddress);
              CommunicatorAddress.sin_family = AF_INET;
              CommunicatorAddress.sin_addr.s_addr = htons(INADDR_ANY);

              if (bind(RecordSocket, (struct sockaddr*) &CommunicatorAddress,
                       sizeof(CommunicatorAddress)) >= 0) {
              }
           }
        }

       PlayCreditSeqno = 0;  // Reset the sequence numbers to zero.
       ClueSeqno = 0;  // Reset the sequence numbers to zero.

       // Create an independent thread for the main packet loop.

       struct sigaction actions;

       memset(&actions, 0, sizeof(actions));
       sigemptyset(&actions.sa_mask);
       actions.sa_flags = 0;
       actions.sa_handler = performCommunicatorExit;

       int rc = sigaction(SIGUSR1, &actions, NULL);

       rc = pthread_create(&PollingThreadHandle, NULL, PollingThread,
             NULL);
       NSLog(@"Native: Created PollingThread %d", rc);

       End;

       // Android versions of the software return immediately because they are controlled
       // by the Java application.  They should call theatroExit() to clean up.
    }

    int buildJSONButtonPacket(char *buf, char *button_text, bool button_state) {

       cJSON *ButtonActionObject = cJSON_CreateObject();
       cJSON_AddNumberToObject(ButtonActionObject, "ver", 2);
       cJSON_AddStringToObject(ButtonActionObject, "mac", CommunicatorMACAddress);

       // Convert getCurrentTimeMillis() to a string because cJSON renders this as a negative number.
       char timeMillis[NAMESIZE];
       memset(timeMillis, 0, NAMESIZE);
       snprintf(timeMillis, NAMESIZE, "%ld", getCurrentTimeMillis());
       cJSON_AddStringToObject(ButtonActionObject, "time", timeMillis);

       cJSON_AddStringToObject(ButtonActionObject, "button", button_text);
       cJSON_AddStringToObject(ButtonActionObject, "num",button_state ? "down" : "up");

       cJSON_AddNumberToObject(ButtonActionObject, "length", currentNetworkStatus);

       memset(buf, 0, THEATROPAYLOADBUFFERSIZE);

       char *cRet = cJSON_PrintUnformatted(ButtonActionObject);// Workaround for cJSON leak.
       if (cRet) {
          strncpy(buf, cRet, THEATROPAYLOADBUFFERSIZE);
          free(cRet);								// Workaround for cJSON leak.
       }

       //StrLower(buf);

       int packet_header_end = strlen(buf);

       buf[packet_header_end] = '\0';

       cJSON_Delete(ButtonActionObject);

       // Return total buffer length.
       return packet_header_end;
    }




    int buildJSONSpecificButtonPacket(char *buf, char *button_text, char *value) {
        
        cJSON *ButtonActionObject = cJSON_CreateObject();
        cJSON_AddNumberToObject(ButtonActionObject, "ver", 2);
        cJSON_AddStringToObject(ButtonActionObject, "mac", CommunicatorMACAddress);
        
        // Convert getCurrentTimeMillis() to a string because cJSON renders this as a negative number.
        char timeMillis[NAMESIZE];
        memset(timeMillis, 0, NAMESIZE);
        snprintf(timeMillis, NAMESIZE, "%ld", getCurrentTimeMillis());
        cJSON_AddStringToObject(ButtonActionObject, "time", timeMillis);
        
        cJSON_AddStringToObject(ButtonActionObject, "button", button_text);
       
        cJSON_AddStringToObject(ButtonActionObject, "num", value);
      
        cJSON_AddNumberToObject(ButtonActionObject, "length", currentNetworkStatus);
        
        memset(buf, 0, THEATROPAYLOADBUFFERSIZE);
        
        char *cRet = cJSON_PrintUnformatted(ButtonActionObject);// Workaround for cJSON leak.
        if (cRet) {
            strncpy(buf, cRet, THEATROPAYLOADBUFFERSIZE);
            free(cRet);                                // Workaround for cJSON leak.
        }
        
        //StrLower(buf);
        
        int packet_header_end = strlen(buf);
        
        buf[packet_header_end] = '\0';
        
        cJSON_Delete(ButtonActionObject);
        
        // Return total buffer length.
        return packet_header_end;
    }


    //

    //
    // Build a "compressed" JSON Audio header (without unused parameters).  TBD...
    //

    int buildJSONAudioPacket(char *buf, signed short *audio_buffer,
          int audio_length_bytes, char *button_text, int seqno) {
       cJSON *ButtonAudioObject = cJSON_CreateObject();

       cJSON_AddStringToObject(ButtonAudioObject, "mac", CommunicatorMACAddress);

       cJSON_AddStringToObject(ButtonAudioObject, "button", button_text);
       cJSON_AddNumberToObject(ButtonAudioObject, "num", seqno);

       // Convert getCurrentTimeMillis() to a string because cJSON renders this as a negative number.
       char timeMillis[NAMESIZE];
       memset(timeMillis, 0, NAMESIZE);
       snprintf(timeMillis, NAMESIZE, "%llu", getCurrentTimeMillis() - offsetTime);
       cJSON_AddStringToObject(ButtonAudioObject, "time", timeMillis);

       int audio_length_frames = audio_length_bytes / sizeof(signed short);

       cJSON_AddNumberToObject(ButtonAudioObject, "length", audio_length_frames);

       memset(buf, 0, THEATROPAYLOADBUFFERSIZE);
       char *cRet = cJSON_PrintUnformatted(ButtonAudioObject);	// Workaround for cJSON leak.
       if (cRet) {
          strncpy(buf, cRet, THEATROPAYLOADBUFFERSIZE);
          free(cRet);
       }											// Workaround for cJSON leak.
       StrLower(buf);

       int packet_header_end = strlen(buf);

       buf[packet_header_end] = '\0';

       //#define LOOPBACK
    #if defined(LOOPBACK)
       // Remember this point for audio loopback testing.
       if (audio_length_bytes > 0) playBuffer((char *) audio_buffer, audio_length_bytes);
    #endif

       int loop = 0;

       //	if (currentButton != Simulator)
       //	{
       //Generate uLaw

       for (loop = 0; loop < audio_length_frames; loop++) {
          signed short sample = double(
                audio_buffer[loop] * RecordAmplificationFactor);

          //		buf[packet_header_end + loop] = Linear2uLaw(sample);	// Original code
          buf[packet_header_end + loop] = linear2ulaw(sample);// New code 7/16/13
       }

       if (isAudioPacketDebugging) {
          char hexstring[audio_length_frames * 3];
          NSLog(@"711: %s",
                toHexString((char * ) &buf[packet_header_end],
                   audio_length_frames, hexstring));
       }

       //	}

       cJSON_Delete(ButtonAudioObject);

       // Return total buffer length.
       return packet_header_end + (audio_length_frames);
    }

    char buf[THEATROPAYLOADBUFFERSIZE];
    int buf_length = 0;

    void *RecorderThread(void *buttonText) {


       char button_text[NAMESIZE] = { 0 };
       strcpy(button_text, (char *)buttonText);

       startRecording();
        
       if (isNoReception)  // Don't start recording if we have "No Reception"
       {
          stopRecording();
       } else {
          // Capture the current button push for the duration of this thread's life.

          ButtonType thisButton;
          if(currentButton == Simulator)
             thisButton = Simulator;
          else {
             if(strcmp(button_text,BroadcastButtonTxt)==0){
                thisButton = BroadcastButton;
             }
             else
             {
                 thisButton = PrimaryButton;
             }

          }
          if (button_text) {

             //char buf[THEATROPAYLOADBUFFERSIZE];
             //int buf_length = 0;

             const int frames_to_deliver = 200;

             signed short audio_buffer[frames_to_deliver];

             int audio_length_bytes = 0;

             int seqno = 0;

             int skip_count = 0;

             // Send out the rest of the recorded voice packets.
             do {
                audio_length_bytes = getRecordBuffer(audio_buffer,
                      frames_to_deliver); // Try to get voice bytes out of the capture buffer.
                
                 if (thisButton == Simulator) {
                   if (audio_length_bytes == 0)
                      currentButton = None;
                }

                if (audio_length_bytes > 0) {
                   if (skip_count < NewRecordingPacketsToSkip) {
                      // Skip over the first few packets according to NewRecordingPacketsToSkip.
                      // This is a configurable default to code around hardware problems.

                      TracePoint(
                            "NewRecordingPacketsToSkip: skip_count %d < %d.  Skipped %d bytes.",
                            skip_count, NewRecordingPacketsToSkip,
                            audio_length_bytes);

                      char audiohexstring[audio_length_bytes * 3];
                      TracePoint("SKIPPING %d < %d: %s", skip_count,
                            NewRecordingPacketsToSkip,
                            toHexString((char * ) audio_buffer,
                               audio_length_bytes, audiohexstring));

                      skip_count++;
                   } else {
                      if (isAudioPacketDebugging)
                         NSLog(@
                               "Dequeued %d bytes from the record buffer.  sizeof(audio_buffer) = %d, sizeof(signed short) = %d ",
                               audio_length_bytes, sizeof(audio_buffer),
                               sizeof(signed short));

                    buf_length = buildJSONAudioPacket(buf, audio_buffer,
                            audio_length_bytes, (char *) button_text,
                            seqno);  // Button down, with voice in message.
                if(currentNetworkStatus < WIFI_ON_TGS_OFF)
                {
                      sockaddr_in TGSAddress;
                      memset(&TGSAddress, 0, sizeof TGSAddress);

                      TGSAddress.sin_family = AF_INET;
                      TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                      TGSAddress.sin_port = htons(RecordPort);

                      if (sendto(RecordSocket, buf, buf_length, 0,
                               (struct sockaddr *) &TGSAddress,
                               sizeof TGSAddress) > 0) {
                          /*
                          Printf("The buffer send is =%s\n",buf);
                          Printf(
                                 "Sent voice ButtonAction \"%s\", seqno %d, length %d to %s:%d",
                                 button_text, seqno, buf_length,
                                 inet_ntoa(TGSAddress.sin_addr),
                                 ntohs(TGSAddress.sin_port));
                           */
                         // Menu Button message was sent successfully.
                         if (isAudioPacketDebugging)
                           /* Printf(
                                  "Sent voice ButtonAction \"%s\", seqno %d, length %d to %s:%d",
                                  button_text, seqno, buf_length,
                                  inet_ntoa(TGSAddress.sin_addr),
                                  ntohs(TGSAddress.sin_port));
                            */
                          
                         if (currentButton == Simulator) {
                            TracePoint("Simulator");
                            millisleep(20);
                         }
                      } else {
                         NSLog(@
                               "Error sending voice ButtonAction message sendto(2)");
                         millisleep(ErrorWait); // Sleep for a moment to recover.

                         close(RecordSocket);

                         // Re-Create a UDP socket for recording.

                         struct sockaddr_in TGSAddress;
                         memset(&TGSAddress, 0, sizeof TGSAddress);
                         TGSAddress.sin_family = AF_INET;
                         TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                         TGSAddress.sin_port = htons(RecordPort);

                         RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
                      
                     }
                }else if(currentNetworkStatus == WIFI_ON_TGS_OFF)
                {
                       webSocket_sendData(buf, buf_length,4);
                }
                seqno++;

                   }
                }
                 
                millisleep(2); // This should not be necessary, because we cannot send any faster than we sample.
             } while ((thisButton == currentButton) && (!isNoReception)); // Quit the loop if we loose reception.

             if (isNoReception) {
                //stopRecording();
             }
          }
       }
       TracePoint("Recorder Ending..");
       pthread_exit(NULL);
    }

    void performButtonAction(char* type) {
        
       char buf[THEATROPAYLOADBUFFERSIZE];
       int buf_length = buildJSONButtonPacket(buf, type, true); // Button down, no voice in message.

       close(RecordSocket);
       sockaddr_in TGSAddress;
       memset(&TGSAddress, 0, sizeof TGSAddress);

       TGSAddress.sin_family = AF_INET;
       TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
       TGSAddress.sin_port = htons(RecordPort);
        
       RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);

        if(currentNetworkStatus < WIFI_ON_TGS_OFF)
        {
        if (sendto(RecordSocket, buf, buf_length, 0,
                (struct sockaddr *) &TGSAddress, sizeof TGSAddress) > 0) {
          // Menu Button message was sent successfully.
          NSLog(@"Sent first ButtonAction \"%s\" to %s:%d\n", buf,
                inet_ntoa(TGSAddress.sin_addr), ntohs(TGSAddress.sin_port));
        } else {
          NSLog(@"Error sending first ButtonAction \"%s\" message sendto(2)",
                buf);
          close(RecordSocket);
          // Re-Create a UDP socket for recording.
          struct sockaddr_in TGSAddress;
          memset(&TGSAddress, 0, sizeof TGSAddress);
          TGSAddress.sin_family = AF_INET;
          TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
          TGSAddress.sin_port = htons(RecordPort);
          RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
        }
        }
        else if(currentNetworkStatus == WIFI_ON_TGS_OFF)
        {
           webSocket_sendData(buf, buf_length,2);
        }
        
    }

    void performLoginButtonAction(char* LoginName) {
        
       char buf[THEATROPAYLOADBUFFERSIZE];
       int buf_length = buildJSONSpecificButtonPacket(buf, "logon",LoginName); // Button down, no voice in message.

       close(RecordSocket);
       sockaddr_in TGSAddress;
       memset(&TGSAddress, 0, sizeof TGSAddress);

       TGSAddress.sin_family = AF_INET;
       TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
       TGSAddress.sin_port = htons(RecordPort);
        
       RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);

        if(currentNetworkStatus < WIFI_ON_TGS_OFF)
        {
          if (sendto(RecordSocket, buf, buf_length, 0,
                (struct sockaddr *) &TGSAddress, sizeof TGSAddress) > 0) {
          // Menu Button message was sent successfully.
          NSLog(@"Sent first ButtonAction \"%s\" to %s:%d\n", buf,
                inet_ntoa(TGSAddress.sin_addr), ntohs(TGSAddress.sin_port));
        } else {
          NSLog(@"Error sending first ButtonAction \"%s\" message sendto(2)",
                buf);
          close(RecordSocket);
          // Re-Create a UDP socket for recording.
          struct sockaddr_in TGSAddress;
          memset(&TGSAddress, 0, sizeof TGSAddress);
          TGSAddress.sin_family = AF_INET;
          TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
          TGSAddress.sin_port = htons(RecordPort);
          RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
        }
        }else if(currentNetworkStatus == WIFI_ON_TGS_OFF)
        {
           webSocket_sendData(buf, buf_length,2);
        }
    }

    void performMsgButtonAction(char* type, char* txnId) {
        
        char buf[THEATROPAYLOADBUFFERSIZE];
        int buf_length = buildJSONSpecificButtonPacket(buf,type,txnId); // Button down, no voice in message.
        
        close(RecordSocket);
        sockaddr_in TGSAddress;
        memset(&TGSAddress, 0, sizeof TGSAddress);
        
        TGSAddress.sin_family = AF_INET;
        TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
        TGSAddress.sin_port = htons(RecordPort);
        
        RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
        if(currentNetworkStatus < WIFI_ON_TGS_OFF)
        {

        if (sendto(RecordSocket, buf, buf_length, 0,
                   (struct sockaddr *) &TGSAddress, sizeof TGSAddress) > 0) {
            // Menu Button message was sent successfully.
            NSLog(@"Sent first ButtonAction \"%s\" to %s:%d\n", buf,
                   inet_ntoa(TGSAddress.sin_addr), ntohs(TGSAddress.sin_port));
        } else {
            NSLog(@"Error sending first ButtonAction \"%s\" message sendto(2)",
                   buf);
            close(RecordSocket);
            // Re-Create a UDP socket for recording.
            struct sockaddr_in TGSAddress;
            memset(&TGSAddress, 0, sizeof TGSAddress);
            TGSAddress.sin_family = AF_INET;
            TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
            TGSAddress.sin_port = htons(RecordPort);
            RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
        }
        }else if(currentNetworkStatus == WIFI_ON_TGS_OFF)
        {
            webSocket_sendData(buf, buf_length,2);
        }

    }


    void performRefreshButtonAction() {
        
       char buf[THEATROPAYLOADBUFFERSIZE];
       int buf_length = buildJSONButtonPacket(buf, "coverage", true); // Button down, no voice in message.

       sockaddr_in TGSAddress;
       memset(&TGSAddress, 0, sizeof TGSAddress);

       close(RecordSocket);
        
       TGSAddress.sin_family = AF_INET;
       TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
       TGSAddress.sin_port = htons(RecordPort);
        
       RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
        
       if(currentNetworkStatus < WIFI_ON_TGS_OFF)
       {
       if (sendto(RecordSocket, buf, buf_length, 0,
                (struct sockaddr *) &TGSAddress, sizeof TGSAddress) > 0) {
          // Menu Button message was sent successfully.
          NSLog(@"Sent Refresh ButtonAction \"%s\" to %s:%d\n", buf,
                inet_ntoa(TGSAddress.sin_addr), ntohs(TGSAddress.sin_port));
       } else {
          // Printf("Error numb %d",errno);
           //Printf("address %d",RecordSocket );
          NSLog(@"Error sending Refresh ButtonAction \"%s\" message sendto(2)",
                buf);
          close(RecordSocket);
          // Re-Create a UDP socket for recording.
          struct sockaddr_in TGSAddress;
          memset(&TGSAddress, 0, sizeof TGSAddress);
          TGSAddress.sin_family = AF_INET;
          TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
          TGSAddress.sin_port = htons(RecordPort);
          RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
           NSLog(@"end of refresh button");
       }
       }else if(currentNetworkStatus == WIFI_ON_TGS_OFF)
        {
            webSocket_sendData(buf, buf_length,2);
        }
    }

    void performPlayBufferAction(bool isValue)
    {
       isBroadcastScreenOn = isValue;
    }

    void performHuddleAction(char* time, int type) {
        
        char buf[THEATROPAYLOADBUFFERSIZE];
        int buf_length = 0;
        if(type == 1)
            buf_length= buildJSONSpecificButtonPacket(buf, "HuddleTime",time); // Button down, no voice in message.
        else
            buf_length = buildJSONButtonPacket(buf, "HuddleDelete", true);
        
        sockaddr_in TGSAddress;
        memset(&TGSAddress, 0, sizeof TGSAddress);
        
        close(RecordSocket);
        TGSAddress.sin_family = AF_INET;
        TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
        TGSAddress.sin_port = htons(RecordPort);
        
        RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
        if(currentNetworkStatus < WIFI_ON_TGS_OFF)
        {
        if (sendto(RecordSocket, buf, buf_length, 0,
                   (struct sockaddr *) &TGSAddress, sizeof TGSAddress) > 0) {
            // Menu Button message was sent successfully.
            NSLog(@"Sent Refresh ButtonAction \"%s\" to %s:%d\n", buf,
                   inet_ntoa(TGSAddress.sin_addr), ntohs(TGSAddress.sin_port));
        } else {
            NSLog(@"Error sending Refresh ButtonAction \"%s\" message sendto(2)",
                   buf);
            close(RecordSocket);
            // Re-Create a UDP socket for recording.
            struct sockaddr_in TGSAddress;
            memset(&TGSAddress, 0, sizeof TGSAddress);
            TGSAddress.sin_family = AF_INET;
            TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
            TGSAddress.sin_port = htons(RecordPort);
            RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
        }
        }else if(currentNetworkStatus == WIFI_ON_TGS_OFF)
        {
            webSocket_sendData(buf, buf_length,2);
        }
    }

    char g_button_txt[NAMESIZE] = { 0 };

    void performCommAction(char *buttonText, bool isDown) {

      strcpy(g_button_txt, buttonText);
        
        //strcpy(g_button_txt, "one2onea$%\\nbhi0s");

       if (isDown)
       {
          if (!isPrimaryButtonPushed) {
             
             TracePoint(
                   "VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV Primary Pushed! VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV  isAudioRecording = %d",
                   (int ) isAudioRecording);

             isPrimaryButtonPushed = true;
             currentButton = PrimaryButton;
             // stopPlayback();

             emptyAudioPlaybackBuffer();
             emptyAudioRecordingBuffer();

             if (!isNoReception) {

                char buf[THEATROPAYLOADBUFFERSIZE];
                int buf_length = buildJSONButtonPacket(buf, (char *) g_button_txt,
                      true);  // Button down, no voice in message.

             if(currentNetworkStatus < WIFI_ON_TGS_OFF)
             {
                close(RecordSocket);
                sockaddr_in TGSAddress;
                memset(&TGSAddress, 0, sizeof TGSAddress);

                TGSAddress.sin_family = AF_INET;
                TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                TGSAddress.sin_port = htons(RecordPort);
                 
                RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);

                if (sendto(RecordSocket, buf, buf_length, 0,
                         (struct sockaddr *) &TGSAddress, sizeof TGSAddress)
                      > 0) {
                   // Menu Button message was sent successfully.
                   TracePoint("Sent first ButtonAction \"%s\" to %s:%d", buf,
                         inet_ntoa(TGSAddress.sin_addr),
                         ntohs(TGSAddress.sin_port));
                } else {
                   NSLog(@
                         "Error sending first ButtonAction \"%s\" message sendto(2)",
                         buf);

                   close(RecordSocket);

                   /// Re-Create a UDP socket for recording.

                   struct sockaddr_in TGSAddress;
                   memset(&TGSAddress, 0, sizeof TGSAddress);
                   TGSAddress.sin_family = AF_INET;
                   TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                   TGSAddress.sin_port = htons(RecordPort);

                   RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
                }
             }else if(currentNetworkStatus == WIFI_ON_TGS_OFF)
             {
                 webSocket_sendData(buf, buf_length,2);
             }

                if (!isAudioRecording) {
                   // startRecording();
                    
                   int rc = pthread_create(&RecorderThreadHandle, NULL,
                         RecorderThread, (void *) g_button_txt);
                    TracePoint("Created RecorderThread %d", rc);
                }
             }
          }
       } else {
          if (isPrimaryButtonPushed) {
             isPrimaryButtonPushed = false;
             currentButton = None;

             stopRecording();
             pthread_join(RecorderThreadHandle, NULL);

             TracePoint(
                   "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Primary Released. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  isAudioRecording = %d",
                   (int ) isAudioRecording);

             if (!isNoReception) {
                char buf[THEATROPAYLOADBUFFERSIZE];

                // Send final button up packet.

                int buf_length = buildJSONButtonPacket(buf, (char *) g_button_txt,
                      false);  // Button up, no voice in message.
                 
             if(currentNetworkStatus < WIFI_ON_TGS_OFF)
             {
                sockaddr_in TGSAddress;
                memset(&TGSAddress, 0, sizeof TGSAddress);

                TGSAddress.sin_family = AF_INET;
                TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                TGSAddress.sin_port = htons(RecordPort);

                if (sendto(RecordSocket, buf, buf_length, 0,
                         (struct sockaddr *) &TGSAddress, sizeof TGSAddress)
                      > 0) {
                   // Menu Button message was sent successfully.
                   TracePoint("Sent last ButtonAction \"%s\" to %s:%d", buf,
                         inet_ntoa(TGSAddress.sin_addr),
                         ntohs(TGSAddress.sin_port));
                } else {
                   NSLog(@
                         "Error sending last ButtonAction \"%s\" message sendto(2)",
                         buf);

                   close(RecordSocket);

                   // Re-Create a UDP socket for recording.

                   struct sockaddr_in TGSAddress;
                   memset(&TGSAddress, 0, sizeof TGSAddress);
                   TGSAddress.sin_family = AF_INET;
                   TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                   TGSAddress.sin_port = htons(RecordPort);

                   RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
                }
             }else if(currentNetworkStatus == WIFI_ON_TGS_OFF)
             {
                 webSocket_sendData(buf, buf_length,2);
             }

             }

          }
       }
    }

    void stopPlayBackFromJava(bool isStopped) {

       if(isStopped)
       {
       NSLog(@
             "Its going to clear the audio buffer *****************************************************");
       emptyAudioPlaybackBuffer();
       isCancelledFromJava = true;
       memset(last_oruid, 0, LINESIZE);
       stopPlayback();
           
       }
       else
       {
          isCancelledFromJava = false;
       }
    }

    void performPrimaryButtonAction(bool isDown) {
       if (isDown)
       {
          if (!isPrimaryButtonPushed) {
             
             TracePoint(
                   "VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV Primary Pushed! VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV  isAudioRecording = %d",
                   (int ) isAudioRecording);

             isPrimaryButtonPushed = true;
             currentButton = PrimaryButton;
             stopPlayback();

             emptyAudioPlaybackBuffer();
             emptyAudioRecordingBuffer();

             if (!isNoReception) {

                char buf[THEATROPAYLOADBUFFERSIZE];

                int buf_length = buildJSONButtonPacket(buf,
                      (char *) PrimaryButtonTxt, true); // Button down, no voice in message.

                sockaddr_in TGSAddress;
                memset(&TGSAddress, 0, sizeof TGSAddress);

                TGSAddress.sin_family = AF_INET;
                TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                TGSAddress.sin_port = htons(RecordPort);

                if (sendto(RecordSocket, buf, buf_length, 0,
                         (struct sockaddr *) &TGSAddress, sizeof TGSAddress)
                      > 0) {
                   // Menu Button message was sent successfully.
                   TracePoint("Sent first ButtonAction \"%s\" to %s:%d", buf,
                         inet_ntoa(TGSAddress.sin_addr),
                         ntohs(TGSAddress.sin_port));
                } else {
                   NSLog(@
                         "Error sending first ButtonAction \"%s\" message sendto(2)",
                         buf);

                   close(RecordSocket);

                   // Re-Create a UDP socket for recording.

                   struct sockaddr_in TGSAddress;
                   memset(&TGSAddress, 0, sizeof TGSAddress);
                   TGSAddress.sin_family = AF_INET;
                   TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                   TGSAddress.sin_port = htons(RecordPort);

                   RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
                }

                if (!isAudioRecording) {
                   startRecording();
                   int rc = pthread_create(&RecorderThreadHandle, NULL,
                         RecorderThread, (void *) PrimaryButtonTxt);
                   TracePoint("Created RecorderThread %d", rc);
                }
             }
          }
       } else {
          if (isPrimaryButtonPushed) {
             isPrimaryButtonPushed = false;
             currentButton = None;

             stopRecording();
             pthread_join(RecorderThreadHandle, NULL);

             TracePoint(
                   "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Primary Released. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  isAudioRecording = %d",
                   (int ) isAudioRecording);

             if (!isNoReception) {
                char buf[THEATROPAYLOADBUFFERSIZE];

                // Send final button up packet.

                int buf_length = buildJSONButtonPacket(buf,
                      (char *) PrimaryButtonTxt, false); // Button up, no voice in message.

                sockaddr_in TGSAddress;
                memset(&TGSAddress, 0, sizeof TGSAddress);

                TGSAddress.sin_family = AF_INET;
                TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                TGSAddress.sin_port = htons(RecordPort);

                if (sendto(RecordSocket, buf, buf_length, 0,
                         (struct sockaddr *) &TGSAddress, sizeof TGSAddress)
                      > 0) {
                   // Menu Button message was sent successfully.
                   TracePoint("Sent last ButtonAction \"%s\" to %s:%d", buf,
                         inet_ntoa(TGSAddress.sin_addr),
                         ntohs(TGSAddress.sin_port));
                } else {
                   NSLog(@
                         "Error sending last ButtonAction \"%s\" message sendto(2)",
                         buf);

                   close(RecordSocket);

                   // Re-Create a UDP socket for recording.

                   struct sockaddr_in TGSAddress;
                   memset(&TGSAddress, 0, sizeof TGSAddress);
                   TGSAddress.sin_family = AF_INET;
                   TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                   TGSAddress.sin_port = htons(RecordPort);

                   RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
                }

             }

          }
       }
    }

    void performBroadcastScreenAction(bool isBroadcast) {
        

       if (isBroadcast) {
          char buf[THEATROPAYLOADBUFFERSIZE];
          int buf_length = buildJSONSpecificButtonPacket(buf, "broadcastscreen","down");  // Button down, no voice in message.

          close(RecordSocket);
           
          sockaddr_in TGSAddress;
          memset(&TGSAddress, 0, sizeof TGSAddress);

          TGSAddress.sin_family = AF_INET;
          TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
          TGSAddress.sin_port = htons(RecordPort);
           
          RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);

          if(currentNetworkStatus < WIFI_ON_TGS_OFF)
          {

          if (sendto(RecordSocket, buf, buf_length, 0,
                   (struct sockaddr *) &TGSAddress, sizeof TGSAddress) > 0) {
             // Menu Button message was sent successfully.
             NSLog(@"Sent first ButtonAction \"%s\" to %s:%d\n", buf,
                   inet_ntoa(TGSAddress.sin_addr), ntohs(TGSAddress.sin_port));
          } else {
             NSLog(@"Error sending first ButtonAction \"%s\" message sendto(2)",
                   buf);
             close(RecordSocket);
             // Re-Create a UDP socket for recording.
             struct sockaddr_in TGSAddress;
             memset(&TGSAddress, 0, sizeof TGSAddress);
             TGSAddress.sin_family = AF_INET;
             TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
             TGSAddress.sin_port = htons(RecordPort);
             RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
              int set = 1;
               setsockopt(RecordSocket, SOL_SOCKET, SO_NOSIGPIPE, (void *)&set, sizeof(int));
          }
       }else if(currentNetworkStatus == WIFI_ON_TGS_OFF)
       {
           webSocket_sendData(buf, buf_length,2);
       }
        
       } else {
          emptyAudioPlaybackBuffer();
          char buf[THEATROPAYLOADBUFFERSIZE];
          int buf_length = buildJSONSpecificButtonPacket(buf, "broadcastscreen", "up");  // Button down, no voice in message.

          close(RecordSocket);
           
          sockaddr_in TGSAddress;
          memset(&TGSAddress, 0, sizeof TGSAddress);

          TGSAddress.sin_family = AF_INET;
          TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
          TGSAddress.sin_port = htons(RecordPort);
           
           RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
           
          if(currentNetworkStatus < WIFI_ON_TGS_OFF)
          {
          if (sendto(RecordSocket, buf, buf_length, 0,
                   (struct sockaddr *) &TGSAddress, sizeof TGSAddress) > 0) {
             // Menu Button message was sent successfully.
            // Printf("Sent first ButtonAction \"%s\" to %s:%d\n", buf,
                  // inet_ntoa(TGSAddress.sin_addr), ntohs(TGSAddress.sin_port));
          } else {
             NSLog(@"Error sending first ButtonAction \"%s\" message sendto(2)",
                   buf);
             close(RecordSocket);
             // Re-Create a UDP socket for recording.
             struct sockaddr_in TGSAddress;
             memset(&TGSAddress, 0, sizeof TGSAddress);
             TGSAddress.sin_family = AF_INET;
             TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
             TGSAddress.sin_port = htons(RecordPort);
             RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
          }
          }
          else if(currentNetworkStatus == WIFI_ON_TGS_OFF)
          {
              webSocket_sendData(buf, buf_length,2);
          }

       }
        
    }

    void performBroadcastButtonAction(bool isDown) {
       if (isDown) {
          if (!isBroadcastButtonPushed) {
              NSLog(@"button action sandeep...\n");
             NSLog(@
                   "VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV Broadcast Pushed! VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV  isAudioRecording = %d",
                   (int ) isAudioRecording);

             isBroadcastButtonPushed = true;
             currentButton = BroadcastButton;
             //stopPlayback();

             emptyAudioPlaybackBuffer();
             emptyAudioRecordingBuffer();

             if (isNoReception) {
                //stopRecording();
             } else {

                char buf[THEATROPAYLOADBUFFERSIZE];

                int buf_length = buildJSONButtonPacket(buf,
                      (char *) BroadcastButtonTxt, true); // Button down, no voice in message.
             if(currentNetworkStatus < WIFI_ON_TGS_OFF)
             {
                close(RecordSocket);

                sockaddr_in TGSAddress;
                memset(&TGSAddress, 0, sizeof TGSAddress);

                TGSAddress.sin_family = AF_INET;
                // int set = 1;
                 //setsockopt(sd, SOL_SOCKET, SO_NOSIGPIPE, (void *)&set, sizeof(int));
                TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                TGSAddress.sin_port = htons(RecordPort);
                 
                RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);

                if (sendto(RecordSocket, buf, buf_length, 0,
                         (struct sockaddr *) &TGSAddress, sizeof TGSAddress)
                      > 0) {
                   // Menu Button message was sent successfully.
                   TracePoint("Sent first ButtonAction \"%s\" to %s:%d", buf,
                         inet_ntoa(TGSAddress.sin_addr),
                         ntohs(TGSAddress.sin_port));
                } else {
                   NSLog(@
                         "Error sending first ButtonAction \"%s\" message sendto(2)",
                         buf);

                   close(RecordSocket);

                   // Re-Create a UDP socket for recording.

                   struct sockaddr_in TGSAddress;
                   memset(&TGSAddress, 0, sizeof TGSAddress);
                   TGSAddress.sin_family = AF_INET;
                   TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                   TGSAddress.sin_port = htons(RecordPort);

                   RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
                  // int set = 1;
                 //  setsockopt(RecordSocket, SOL_SOCKET, SO_NOSIGPIPE, (void *)&set, sizeof(int));
                }
                }
                else if(currentNetworkStatus == WIFI_ON_TGS_OFF)
                {
                    webSocket_sendData(buf, buf_length,2);
                }

                if (!isAudioRecording) {
                  // startRecording();
                    
                   int rc = pthread_create(&RecorderThreadHandle, NULL,
                        RecorderThread, (void *) BroadcastButtonTxt);
                   TracePoint("Created RecorderThread %d", rc);
                }
             }
          }
       } else {
          if (isBroadcastButtonPushed) {
             isBroadcastButtonPushed = false;
             currentButton = None;
              NSLog(@"Stop Recording.....\n");
             stopRecording();
             pthread_join(RecorderThreadHandle, NULL);

             TracePoint(
                   "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Broadcast Released. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  isAudioRecording = %d",
                   (int ) isAudioRecording);
              



             if (!isNoReception) {

                // Send final button up packet.

                char buf[THEATROPAYLOADBUFFERSIZE];

                int buf_length = buildJSONButtonPacket(buf,
                      (char *) BroadcastButtonTxt, false); // Button up, no voice in message.
             if(currentNetworkStatus < WIFI_ON_TGS_OFF)
             {
                sockaddr_in TGSAddress;
                memset(&TGSAddress, 0, sizeof TGSAddress);

                TGSAddress.sin_family = AF_INET;
                TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                TGSAddress.sin_port = htons(RecordPort);


                if (sendto(RecordSocket, buf, buf_length, 0,
                         (struct sockaddr *) &TGSAddress, sizeof TGSAddress)
                      > 0) {
                   // Menu Button message was sent successfully.
                   TracePoint("Sent last ButtonAction \"%s\" to %s:%d", buf,
                         inet_ntoa(TGSAddress.sin_addr),
                         ntohs(TGSAddress.sin_port));
                } else {
                   NSLog(@
                         "Error sending last ButtonAction \"%s\" message sendto(2)",
                         buf);

                   close(RecordSocket);

                   // Re-Create a UDP socket for recording.

                   struct sockaddr_in TGSAddress;
                   memset(&TGSAddress, 0, sizeof TGSAddress);
                   TGSAddress.sin_family = AF_INET;
                   TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
                   TGSAddress.sin_port = htons(RecordPort);

                   RecordSocket = socket(AF_INET, SOCK_DGRAM, 0);
                }
             }
             else if(currentNetworkStatus == WIFI_ON_TGS_OFF)
             {
                 webSocket_sendData(buf, buf_length,2);
             }

             }
          }
       }
    }


    //Audio thread variable moved here....

    void startRecording()
    {
        isAudioRecording = true;
        char str[20] = "startRcrd"; //start record indication
        stausCallback(str);
    }

    void stopRecording()
    {
        isAudioRecording = false;
        char str[20] = "stopRcrd"; //stop record indication
        stausCallback(str);
    }

    void startPlayback()
    {
        isAudioStreaming= true;
        char str[20] = "startPbk"; //start playback indication
        stausCallback(str);
    }

    void stopPlayback()
    {
        char str[20] = "stopPbk"; //stop playback indication
        stausCallback(str);
        isAudioStreaming = false;
    }

    RingBuffer AudioCaptureRingBuffer(MAX_CAPTURE_BUFFER_SIZE);
    RingBuffer AudioPlaybackRingBuffer(MAX_PLAYBACK_BUFFER_SIZE);

    int getAvailablePlaybackData()
    {
        return AudioPlaybackRingBuffer.getReadAvail();
    }


bool isAudioPlaybackBufferEmpty()
{
    return AudioPlaybackRingBuffer.isEmpty();
}

bool isAudioCaptureBufferFull()
{
    return AudioCaptureRingBuffer.isFull();
}

    void emptyAudioPlaybackBuffer()
    {
        //isAudioPlaying = false;
        AudioPlaybackRingBuffer.empty();
    }

    void emptyAudioRecordingBuffer()
    {
        AudioCaptureRingBuffer.empty(); //Added abhi.. not present...
    }

    void clearOraId()
    {
        memset(last_oruid, 0, LINESIZE);
        NSLog(@"LastOruid Clearrrrrrrrrrrreeeeeddddddddd\n");
    }


    int playBuffer(short *buff, int length)
    {
        int put_bytes = 0;
        do
        {
            if (AudioPlaybackRingBuffer.getWriteAvail() >= length)
            {
                put_bytes = AudioPlaybackRingBuffer.write((char *) buff, length);
            }
            else
            {
                //When PlaybackRingbuffer is full..we are reading out the ringbuffer to make space for new data coming in..***Fix for blue Led issue **
                //millisleep(400);  // Sleep 400 ms.
                signed short data[length];
                int bytes_read = AudioPlaybackRingBuffer.readWithTimeout((char *) data,length,QUADTICK);
                put_bytes = AudioPlaybackRingBuffer.write((char *) buff, bytes_read);
                //Printf("The playback overflowed.. Read out %d bytes and made space..\n",bytes_read);
            }
        }
        while (put_bytes == 0);
        
        // if (isAudioDebugging) Printf("Enqueued %d bytes.  Requested %d bytes.  %d free in play buffer.", put_bytes, length, AudioPlaybackRingBuffer.getWriteAvail());
        
        return(put_bytes == length);
        
    }


    int getRecordBuffer(signed short *buf, int frames_to_read)
    {
        int get_bytes = 0;
        
        int length_bytes = frames_to_read * sizeof(signed short);
        
        if (AudioCaptureRingBuffer.getReadAvail() > length_bytes)
        {
            get_bytes = AudioCaptureRingBuffer.read((char *) buf, length_bytes);
            //Printf("RingBuffer : Read %d bytes\n",get_bytes);
        }
        
        return(get_bytes);
    }

    unsigned int performRingBufferRead(void *buffer, unsigned int size)
    {
        unsigned int bytes_read = AudioPlaybackRingBuffer.readWithTimeout((char *)buffer,size,QUADTICK);
        return bytes_read;
    }

    void performRingBufferWrite(void *buffer, unsigned int size)
    {
        AudioCaptureRingBuffer.write((char *)buffer, size);
    }



    /********************************************************************/
    /* Func : Calculate the MOS Score  for the Oration                  */
    /* Variables : avg_Latency ,avgJitter, percentage_packet_loss       */
    /*                                                                  */
    /********************************************************************/

    double TheatroMOS_Calculation(double avg_Latency, double avgJitter, double percentage_packet_loss)
    {

       double R_value = 0.0;
       double MOS_val = 0.0;

       // Take the average latency, add jitter, but double the impact to latency
       // then add 10 for protocol latancies
       double effective_latency = ( avg_Latency + (avgJitter * 2) + 10 );

       // Implement a basic curve - deduct 4 for the r_value at 160ms of latency
       // (round trip). Anything over that gets a much more agressive deduction

       if (effective_latency < 160) {
          R_value = 93.2 - (effective_latency / 40);
       }
       else {
          R_value = 93.2 - (effective_latency - 120) / 10;
       }

       // Now, let's deduct 2.5 r_value per percentage of packet_loss
       R_value = R_value - (percentage_packet_loss * 2.5);

       if(R_value <= 1)
          return 1.0;

       if(R_value >= 100)
          return 4.4;

       // Convert the r_value into an MOS value. (this is a known formula)
       MOS_val = 1 + (0.035)*R_value + (0.000007) * R_value * (R_value - 60) * (100 - R_value);

       return MOS_val;

    }

    /******************************************************/
    /* Func : Packetize Json Playcredit Formats           */
    /* Variables : oruid ,Polling Thread Variable         */
    /*             PlayCreditSeqno :Global Variable       */
    /******************************************************/
    void Send_PlayCredit_Msg(char *oruid,unsigned long int dropped_packets_per_oration,
          unsigned long int total_packets_per_oration, bool isEofOration ,
          double mosValue, double ppl, double avgJitter , double avgLatency,
          double orationPktRateInSec)
    {

       //Printf("mosValue Send playcredits...\n");
       char buffer[THEATROPAYLOADBUFFERSIZE];
       struct sockaddr_in TGSAddress;
       cJSON *PlayCreditObject = cJSON_CreateObject();
       cJSON_AddStringToObject(PlayCreditObject, "cmd", "playcredit");
       cJSON_AddNumberToObject(PlayCreditObject, "ver", 3);
       cJSON_AddStringToObject(PlayCreditObject, "mac", CommunicatorMACAddress);
       cJSON_AddNumberToObject(PlayCreditObject, "seqno", PlayCreditSeqno);
       cJSON_AddNumberToObject(PlayCreditObject, "available", getAvailablePlaybackData() );

       if(isEofOration && isMosEnabled)
       {

          cJSON_AddStringToObject(PlayCreditObject, "oruid", oruid);
          cJSON_AddNumberToObject(PlayCreditObject, "totalnumpkts", total_packets_per_oration);//Record it in playcredet
          cJSON_AddNumberToObject(PlayCreditObject, "mos",mosValue);
          cJSON_AddNumberToObject(PlayCreditObject, "ppl",ppl );
          cJSON_AddNumberToObject(PlayCreditObject, "avgJitter",avgJitter );
          cJSON_AddNumberToObject(PlayCreditObject, "avgLatency",avgLatency );
          cJSON_AddNumberToObject(PlayCreditObject, "orapktrate",orationPktRateInSec );
          cJSON_AddNumberToObject(PlayCreditObject, "protocol",currentNetworkStatus);
       }

       memset(buffer, 0, THEATROPAYLOADBUFFERSIZE);
       char *cRet = cJSON_PrintUnformatted(PlayCreditObject);        // Workaround for cJSON leak.
       if (cRet)
       {
          strncpy(buffer, cRet, THEATROPAYLOADBUFFERSIZE);
          free(cRet);                                               // Workaround for cJSON leak.
       }

        TGSAddress.sin_family = AF_INET;
        TGSAddress.sin_addr.s_addr = inet_addr(TgsIp);
        TGSAddress.sin_port = htons(PlayCreditPort);

        if( currentNetworkStatus < WIFI_ON_TGS_OFF  )
        {

            if (sendto(PLAYSOCKET, buffer, strlen(buffer), 0, (struct sockaddr *) &TGSAddress, sizeof TGSAddress) > 0)
            {
                NSLog(@"Native: UDP Sent PlayCredits %s\n",buffer);
                // Reset the dropped packets per playcredit count.
                // Ravi- Aug 27th,2013, Its already incemented outside the loop
                //PlayCreditSeqno++;                  // Increment the sequence number.
            }
            else {
                NSLog(@"Error sending PlayCredits to %s:%d <== %s", inet_ntoa(TGSAddress.sin_addr), ntohs(TGSAddress.sin_port), buffer);

                // Kick Start the PlayCreditSocket socket.
                close(PlayCreditSocket);
                PlayCreditSocket = socket(AF_INET, SOCK_DGRAM, 0);
                NSLog(@"PlayCreditSocket = %d", PlayCreditSocket);
            }
        }
        else if( currentNetworkStatus == WIFI_ON_TGS_OFF)
        {
            webSocket_sendData(buffer, strlen(buffer),3);
        }
        cJSON_Delete(PlayCreditObject);
    }

