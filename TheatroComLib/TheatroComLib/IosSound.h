/*
 * IOSSound.h
 *
 *  Created on: 12 May, 2015
 *      Author: Abhinandan
 */

#ifndef IOSSOUND_H_
#define IOSSOUND_H_

#include <stdio.h>
#include "RingBuffer.h"
#include<stdbool.h>
#import <AudioUnit/AudioUnit.h>
#import <AudioToolbox/AudioToolbox.h>


#define MAX_CAPTURE_BUFFER_SIZE   262136
#define MAX_PLAYBACK_BUFFER_SIZE 2000000

extern bool isAudioAlive;
extern bool isAudioRecording;
extern bool isAudioPlaying;
extern bool isAudioDebugging;
extern bool isAudioPacketDebugging;
extern bool isAudioStreaming;

RingBuffer AudioCaptureRingBuffer(MAX_CAPTURE_BUFFER_SIZE);
RingBuffer AudioPlaybackRingBuffer(MAX_PLAYBACK_BUFFER_SIZE);

   void buildSawtoothBuffer();
   void createEngine();
   bool createAudioRecorder();
   void startRecording_IOS();
   void createBufferQueueAudioPlayer();
   bool startPlaying();
   void shutdownAudio();
   int playBuffer(short *buf, int length);
   int enqueueBuffer(short *buf);
   bool playFile(const char *fn);
   void initSound_IOS();
   void deInitSound_IOS();

   void startRecording();
   void stopRecording();


   int getAvailableRecordSpace();
   int getAvailablePlaybackSpace();

   bool isAudioPlaybackBufferEmpty();
   void emptyAudioPlaybackBuffer();
   void emptyAudioRecordingBuffer();

   int getRecordBuffer(signed short *buf, int frames_to_deliver);
   bool injectRecordBuffer(char *buff, int length);
   bool injectFile(const char *fn);

   void replayAudioInDevice();


#endif /* IOSSOUND_H_ */
