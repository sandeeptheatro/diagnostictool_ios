/*
 * TheatroDefs.h
 *
 *  Created on: May 15, 2013
 *      Author: vincente
 */

#ifndef THEATRODEFS_H_
#define THEATRODEFS_H_

#define ONE_K	1024
#define FOUR_K	4096
#define EIGHT_K	8192

#define BIGBUFERSIZE 65535
#define IPV4_REASONABLE_BUFFER_SIZE	576
#define THEATROPAYLOADBUFFERSIZE	1400
#define NAMESIZE	64
#define LINESIZE	350
#define DEVICE_ID_LENGTH	6
#define STATIC_FILE_DATA 9000
#define MAX_NUM_PACKETS 500
#define MAX_PACKET_SIZE 1400
#define MAX_PACKET_SIZE_OUTPUT 500
//#define EVAL_BOARD

#define DRIVER_VER_FILE_PATH "/proc/wlan0/version"
#endif /* THEATRODEFS_H_ */
