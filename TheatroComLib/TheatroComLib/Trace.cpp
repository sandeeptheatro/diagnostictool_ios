/*
 * Trace.cpp
 *
 *  Created on: Apr 15, 2013
 *      Author: vincente
 */

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>
#include <syslog.h>


#ifdef __ANDROID_API__
#include <android/log.h>
#endif

//#define DEBUG
#include "Trace.h"

#define BUFFERSIZE	255
#define SMALLBUFFERSIZE	20

#define BIGBUFERSIZE 65535
#define ONE_K 1024

bool isDebugging;

bool useSyslog;


void initTrace(bool toSyslog)
{
	useSyslog = toSyslog;

	if (useSyslog)
	{
#ifndef __ANDROID_API__
	openlog("Communicator", LOG_CONS || LOG_PERROR, LOG_USER);
#endif
	}
}


void closeTrace()
{
	if (useSyslog)
	{
#ifndef __ANDROID_API__
		closelog();
#endif
	}
}


void tracePrintf(const char *SourceFilename, const char *SourceFunction, int SourceLineno, const char *CFormatString, ...)
{
	char buffer[ONE_K];

	char SourceFunctionBuffer[SMALLBUFFERSIZE];

	strncpy(SourceFunctionBuffer, SourceFunction, sizeof(SourceFunctionBuffer));
	SourceFunctionBuffer[SMALLBUFFERSIZE - 1] = '\0';

	if (useSyslog)
	{
		sprintf(buffer, "%20.20s|%4d| %s", SourceFunctionBuffer, SourceLineno, CFormatString);
	}
	else {
		//
		//  add the date and time stamp
		//
		int length = 0;

		time_t timer;
		char timebuffer[BUFFERSIZE];

		time_t now;
		time(&now);
		if (now > 0)
		{
			struct tm *tmPtr;
			struct timespec ts;
			tmPtr = localtime(&now);

			//clock_gettime(CLOCK_REALTIME, &ts);

			length += sprintf(&timebuffer[length], "%02d/%02d|%02d:%02d:%02d.%03ld ",
					tmPtr->tm_mon + 1, tmPtr->tm_mday,
					tmPtr->tm_hour, tmPtr->tm_min, tmPtr->tm_sec,
					// (ts.tv_nsec/10000000));
					// (ts.tv_nsec/100000));
					(ts.tv_nsec / 1000000));
			timebuffer[length] = '\0';
		}
		else
		{
			length += sprintf(&timebuffer[length], "%02d/%02d|%02d:%02d:%02d.%03ld ",
					0, 0, 0, 0, 0, 0L);
			timebuffer[length] = '\0';
		}

		char SourceFilenameBuffer[SMALLBUFFERSIZE];

		strncpy(SourceFilenameBuffer, SourceFilename, sizeof(SourceFilenameBuffer));
		SourceFilenameBuffer[SMALLBUFFERSIZE - 1] = '\0';

		sprintf(buffer, "%s|%16.16s|%20.20s|%4d| %s", timebuffer, SourceFilenameBuffer, SourceFunctionBuffer, SourceLineno, CFormatString);
	}

	va_list arglist;
	va_start(arglist, CFormatString);

	if (useSyslog)
	{
		vsyslog(LOG_DEBUG, buffer, arglist);
	}
	else {
#ifdef __ANDROID_API__
	__android_log_vprint(ANDROID_LOG_VERBOSE, "trace", buffer, arglist);
#else
	vfprintf(stderr, buffer, arglist);
	fprintf(stderr, "\n");
#endif
	}

	va_end(arglist);
}



int tracePrintBigString(char *str)
{
	Begin;

	char big_buffer[BIGBUFERSIZE];
	int length = 0;

	if (str != NULL) {
		strncpy(big_buffer, str, BIGBUFERSIZE);

		length = strlen(big_buffer);

		tracePrintf(__FILE__, __FUNCTION__, __LINE__, "\n%s", big_buffer);

	}
	Return(length);		// Return the length of the string that was printed.
}


