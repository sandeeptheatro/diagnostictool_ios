/*
 * RingBuffer.cpp
 *
 *  Created on: May 20, 2013
 *      Author: vincente
 */

#include <string.h>
#include <pthread.h>
#include <sys/time.h>

#include "RingBuffer.h"


#define DEBUG
#include "Trace.h"


#define ZEROVALUE 0x00


RingBuffer::RingBuffer(int sizeBytes)
{
	_data = new char[sizeBytes];
	memset(_data, ZEROVALUE, sizeBytes);
	_size = sizeBytes;

	_readPtr = 0;
	_writePtr = 0;
	_writeBytesAvail = sizeBytes;

	pthread_mutex_init(&my_mutex, NULL);
	pthread_cond_init(&empty_condition, NULL);
	pthread_cond_init(&full_condition, NULL);
}


RingBuffer::~RingBuffer()
{
	delete[] _data;
}



// Set all data to ZEROVALUE and flag buffer as empty.
bool RingBuffer::empty(void)
{
	pthread_mutex_lock(&my_mutex);  // lock mutex

	memset(_data, ZEROVALUE, _size);
	_readPtr = 0;
	_writePtr = 0;
	_writeBytesAvail = _size;

	pthread_cond_signal(&empty_condition);

	pthread_mutex_unlock(&my_mutex);  // unlock mutex

	return true;
}


int RingBuffer::read(char *dataPtr, int numBytes)
{
	pthread_mutex_lock(&my_mutex);  // lock mutex

	// If there's nothing to read or no data available, then we can't read anything.
	if (dataPtr == 0 || numBytes <= 0)
	{
		pthread_mutex_unlock(&my_mutex);  // unlock mutex
		return 0;
	}

	if (_writeBytesAvail == _size) pthread_cond_wait(&empty_condition, &my_mutex);

	int readBytesAvail = _size - _writeBytesAvail;

	// Cap our read at the number of bytes available to be read.
	if (numBytes > readBytesAvail)
	{
		numBytes = readBytesAvail;
	}

	// Simultaneously keep track of how many bytes we've read and our position in the outgoing buffer
	if (numBytes > _size - _readPtr)
	{
		int len = _size - _readPtr;
		memcpy(dataPtr, _data + _readPtr, len);
		memcpy(dataPtr + len, _data, numBytes - len);
	}
	else
	{
		memcpy(dataPtr, _data + _readPtr, numBytes);
	}

	_readPtr = (_readPtr + numBytes) % _size;
	_writeBytesAvail += numBytes;

	pthread_cond_signal(&full_condition);

	pthread_mutex_unlock(&my_mutex);  // unlock mutex

	return numBytes;
}


int RingBuffer::readWithTimeout(char *dataPtr, int numBytes, int ms)
{
	pthread_mutex_lock(&my_mutex);  // lock mutex



	// If there's nothing to read or no data available, then we can't read anything.
	if (dataPtr == 0 || numBytes <= 0)
	{
		pthread_mutex_unlock(&my_mutex);  // unlock mutex
		return 0;
	}

	if (_writeBytesAvail == _size)
	{
		  struct timespec timeToWait;
		  struct timeval now;

		  gettimeofday(&now, NULL);

		  timeToWait.tv_sec = now.tv_sec+5;
		  timeToWait.tv_nsec = (now.tv_usec+1000UL*ms)*1000UL;

		  pthread_cond_timedwait(&empty_condition, &my_mutex, &timeToWait);
	}
	int readBytesAvail = _size - _writeBytesAvail;

	// Cap our read at the number of bytes available to be read.
	if (numBytes > readBytesAvail)
	{
		numBytes = readBytesAvail;
	}

	// Simultaneously keep track of how many bytes we've read and our position in the outgoing buffer
	if (numBytes > _size - _readPtr)
	{
		int len = _size - _readPtr;
		memcpy(dataPtr, _data + _readPtr, len);
		memcpy(dataPtr + len, _data, numBytes - len);
	}
	else
	{
		memcpy(dataPtr, _data + _readPtr, numBytes);
	}

	_readPtr = (_readPtr + numBytes) % _size;
	_writeBytesAvail += numBytes;

	pthread_cond_signal(&full_condition);

	pthread_mutex_unlock(&my_mutex);  // unlock mutex

	return numBytes;
}




// Write to the ring buffer. Do not overwrite data that has not yet
// been read.
int RingBuffer::write(char *dataPtr, int numBytes)
{
	// If there's nothing to write or no room available, we can't write anything.
	if (dataPtr == 0 || numBytes <= 0)
	{
		return 0;
	}

	pthread_mutex_lock(&my_mutex);  // lock mutex

	if (_writeBytesAvail == 0) pthread_cond_wait(&full_condition, &my_mutex);

	// Cap our write at the number of bytes available to be written.
	if (numBytes > _writeBytesAvail)
	{
		numBytes = _writeBytesAvail;
	}

	// Simultaneously keep track of how many bytes we've written and our position in the incoming buffer
	if (numBytes > _size - _writePtr)
	{
		int len = _size - _writePtr;
		memcpy(_data + _writePtr, dataPtr, len);
		memcpy(_data, dataPtr + len, numBytes - len);
	}
	else
	{
		memcpy(_data + _writePtr, dataPtr, numBytes);
	}

	_writePtr = (_writePtr + numBytes) % _size;
	_writeBytesAvail -= numBytes;

	pthread_cond_signal(&empty_condition);

	pthread_mutex_unlock(&my_mutex);  // unlock mutex

	return numBytes;
}


int RingBuffer::getSize(void)
{
	return _size;
}


int RingBuffer::getWriteAvail(void)
{
    int retVal = 0;
    pthread_mutex_lock(&my_mutex);
    retVal = _writeBytesAvail;
    pthread_mutex_unlock(&my_mutex);  // unlock mutex
    return retVal;
}


int RingBuffer::getReadAvail(void)
{
    int retVal = 0;
    pthread_mutex_lock(&my_mutex);
    retVal = _size - _writeBytesAvail;
    pthread_mutex_unlock(&my_mutex);  // unlock mutex
    return retVal;
}
