/*
 * TheatroUtilities.h
 *
 *  Created on: May 15, 2013
 *      Author: vincente
 */

#ifndef THEATROUTILITIES_H_
#define THEATROUTILITIES_H_

#include <time.h>

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

unsigned long long getCurrentTimeMillis();


int getIPAddress(char *interface);
int getBroadcastAddress(char *interface);
int getNetworkMask(char *interface);
int getNetwork(int addr, int mask);


void StrLower(char str[]);

void ms2tv(struct timeval *result, unsigned long interval_ms);

int millisleep(unsigned ms);

void removeSeparatorsFromMACAddress(char *mac);
void addSeparatorToMACAddress(char *mac, char separator);


int ulaw2linear(unsigned char ulawbyte);
unsigned char linear2ulaw(int sample);


//signed short uLaw2Linear(unsigned char uval);
//signed short search(signed short sample);
//unsigned char Linear2uLaw(signed short sample);

ssize_t readLine(int sockd, void *vptr, size_t maxlen);
ssize_t writeLine(int sockd, const void *vptr, size_t n);

unsigned char getHighByte(signed short n);
unsigned char getLowByte(signed short n);

char *toHexString(char *buf, int length, char *hexstring);

#endif /* THEATROUTILITIES_H_ */
