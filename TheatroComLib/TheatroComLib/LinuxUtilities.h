/*
 * LinuxUtilities.h
 *
 *  Created on: May 21, 2013
 *      Author: vincente
 */

#ifndef LINUXUTILITIES_H_
#define LINUXUTILITIES_H_

#define CUTOFF_THRESHOLD 1.4

void toggle_led();

void set_led_brightness(char *color, unsigned char level);

void stop_led_heartbeat(char *color);
void start_led_heartbeat(char *color);

void red_led_on();
void red_led_off();

void blue_led_on();
void blue_led_off();

void green_led_on();
void green_led_off();

void yellow_led_on();
void yellow_led_off();

void get_mac_address();

float getBatteryLevel();
double getBatteryTemperature(const char *BatteryTemperatureDevicePath);

void powerOff();

#endif /* LINUXUTILITIES_H_ */
