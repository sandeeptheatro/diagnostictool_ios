/*
 * LinuxUtilities.cpp
 *
 *  Created on: May 21, 2013
 *      Author: vincente
 */

#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <spawn.h>
extern char **environ;
#define DEBUG
#include "Trace.h"

float battery_voltage = 1.80;

char mac_address[20] = "00:00:00:00:00:00";


void toggle_led()
{
    pid_t pid;
    char *argv[] = {
        "VALUE=`cat /sys/class/leds/d2/brightness`; if [[ $VALUE -gt 0 ]] ; then echo 0 > /sys/class/leds/d2/brightness; else echo 255 > /sys/class/leds/d2/brightness; fi",
        NULL
    };
    
    posix_spawn(&pid, argv[0], NULL, NULL, argv, environ);
    waitpid(pid, NULL, 0);
	//posix_spawn("VALUE=`cat /sys/class/leds/d2/brightness`; if [[ $VALUE -gt 0 ]] ; then echo 0 > /sys/class/leds/d2/brightness; else echo 255 > /sys/class/leds/d2/brightness; fi");
}


void stop_led_heartbeat(char *color)
{
	char led_device[_POSIX_PATH_MAX];

	snprintf(led_device, _POSIX_PATH_MAX, "/sys/class/leds/%s/brightness", color);

	int fd = open(led_device, O_WRONLY);

	if (fd > 0)
	{

		char level_string[_POSIX_NAME_MAX];

		snprintf(level_string, _POSIX_NAME_MAX, "none");

		write(fd, level_string, strlen(level_string));
		close(fd);
	}
}


void start_led_heartbeat(char *color)
{
	char led_device[_POSIX_PATH_MAX];

	snprintf(led_device, _POSIX_PATH_MAX, "/sys/class/leds/%s/brightness", color);

	int fd = open(led_device, O_WRONLY);

	if (fd > 0)
	{

		char level_string[_POSIX_NAME_MAX];

		snprintf(level_string, _POSIX_NAME_MAX, "heartbeat");

		write(fd, level_string, strlen(level_string));
		close(fd);
	}
}


void set_led_brightness(char *color, unsigned char level)
{
	char led_device[_POSIX_PATH_MAX];

	snprintf(led_device, _POSIX_PATH_MAX, "/sys/class/leds/%s/brightness", color);

	int fd = open(led_device, O_WRONLY);

	if (fd > 0)
	{
		char level_string[_POSIX_NAME_MAX];

		snprintf(level_string, _POSIX_NAME_MAX, "%d", level);

		write(fd, level_string, strlen(level_string));
		close(fd);
	}
}


void red_led_on()
{
	set_led_brightness("red", 50);
}


void red_led_off()
{
	set_led_brightness("red", 0);
}


void blue_led_on()
{
	// system("echo 255 > /sys/class/leds/blue/brightness");

	set_led_brightness("blue", 50);
}


void blue_led_off()
{
	set_led_brightness("blue", 0);
}


void green_led_on()
{
	set_led_brightness("green", 50);
}

void green_led_off()
{
	set_led_brightness("green", 0);
}


void yellow_led_on()
{
	set_led_brightness("red", 50);
	set_led_brightness("green", 50);
}


void yellow_led_off()
{
	set_led_brightness("red", 0);
	set_led_brightness("green", 0);
}


void get_mac_address()
{
	int fd = open("/sys/class/net/wlan0/address", O_RDONLY);
	if (fd > 0)
	{
		read(fd, mac_address, 19);
		close(fd);
	}
}

#define CUTOFF_THRESHOLD 1.67

float getBatteryLevel()
{
	float battery_voltage = 2.0;  // Returning zero could cause a unexpected shutdown.

	char str[100];
	int fd = open("/sys/devices/platform/at91_adc/volt9", O_RDONLY);
	if (fd > 0) {
		memset(str, 0, 100);
		if (read(fd, str, 90) > 0) {
			battery_voltage = atof(str);
		}
		close(fd);
	}
	return battery_voltage;
}


double getBatteryTemperature(const char *BatteryTemperatureDevicePath)
{
	double battery_temperature = 0;  // Returning zero could cause a unexpected shutdown.

	char str[100];
	int fd = open(BatteryTemperatureDevicePath, O_RDONLY);
	if (fd > 0) {
		memset(str, 0, 100);
		if (read(fd, str, 90) > 0) {
			battery_temperature = atof(str);
		}
		close(fd);
	}
	return battery_temperature;
}


void powerOff()
{
    pid_t pid;
    char *argv[] = {
       "/sbin/poweroff",
        NULL
    };
    
    posix_spawn(&pid, argv[0], NULL, NULL, argv, environ);
    waitpid(pid, NULL, 0);
	//nftw("/sbin/poweroff");
}
