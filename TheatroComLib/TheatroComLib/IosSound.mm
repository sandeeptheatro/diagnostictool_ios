/*
 * IOSSound.cpp
 *
 *  Created on: May 12, 2015
 *      Author: Abhinandan
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <semaphore.h>

#include "IosSound.h"

//#include "TheatroCommunicator.h"
#include "Trace.h"

#define TICKWAIT 25
#define QUADTICK 100

#include "TheatroUtilities.h"


bool isAudioAlive;
bool isAudioRecording;
bool isAudioPlaying;
bool isCaptureOpen;
bool isPlaybackOpen;
bool isAudioStreaming;

bool isAudioDebugging;
bool isAudioPacketDebugging;


pthread_t AudioThreadHandle;


// 5 seconds of recorded audio at 16 kHz mono, 16-bit signed little endian
//#define RECORDER_FRAMES (16000 * 5)
//static short recorderBuffer[RECORDER_FRAMES];
//static unsigned recorderSize = 0;

#define outputBus 0
#define inputBus 1

AudioComponentInstance ioUnit;

void printError(int result){
    if (result) {
        Printf("Status not 0! %d\n", result);
        //		exit(1);
    }
}


static OSStatus recordingCallback(void *inRefCon,
                                  AudioUnitRenderActionFlags *ioActionFlags,
                                  const AudioTimeStamp *inTimeStamp,
                                  UInt32 inBusNumber,
                                  UInt32 inNumberFrames,
                                  AudioBufferList *ioData) {
    
    AudioBuffer buffer;
    
    buffer.mNumberChannels = 1;
    buffer.mDataByteSize = inNumberFrames * 2;
    buffer.mData = malloc( inNumberFrames * 2 );
    
    // Put buffer in a AudioBufferList
    AudioBufferList bufferList;
    bufferList.mNumberBuffers = 1;
    bufferList.mBuffers[0] = buffer;
    
    // Then:
    // Obtain recorded samples
    
    OSStatus result;
    
    result = AudioUnitRender(ioUnit,
                             ioActionFlags,
                             inTimeStamp,
                             inBusNumber,
                             inNumberFrames,
                             &bufferList);
    printError(result);
    
    // Now, we have the samples we just read sitting in buffers in bufferList
    // Process the new data
    //   [iosAudio processAudio:&bufferList];
    
    AudioCaptureRingBuffer.write((char *)buffer.mData, inNumberFrames * sizeof(signed short) );
    
    //Printf("The inNumber = %d of frames recorded..hurray", inNumberFrames);
    
    // release the malloc'ed data in the buffer we created earlier
    free(bufferList.mBuffers[0].mData);
    
    return noErr;
}

static OSStatus playbackCallback(void *inRefCon,
                                 AudioUnitRenderActionFlags *ioActionFlags,
                                 const AudioTimeStamp *inTimeStamp,
                                 UInt32 inBusNumber,
                                 UInt32 inNumberFrames,
                                 AudioBufferList *ioData) {
    
    for (int i=0; i < ioData->mNumberBuffers; i++) { // in practice we will only ever have 1 buffer, since audio format is mono
        AudioBuffer buffer = ioData->mBuffers[i];
        
         Printf("  Buffer %d has %d channels and wants %d bytes of data. InFrames=%d\n", i, buffer.mNumberChannels, buffer.mDataByteSize,inNumberFrames);
        
         int bytes_read = AudioPlaybackRingBuffer.readWithTimeout((char *)buffer.mData, buffer.mDataByteSize , QUADTICK);
        
         buffer.mDataByteSize = bytes_read;
        
         if(AudioPlaybackRingBuffer.getReadAvail() < 400)
         {
             Printf("  Buffer is Null----\n");
             isAudioStreaming = false;
             
             *ioActionFlags |= kAudioUnitRenderAction_OutputIsSilence;
             memset(buffer.mData,0xff,buffer.mDataByteSize);
         }
        
    }
    
    return noErr;
}


// create the engine and output mix objects
void createEngine()
{
    OSStatus result;
    
    // Describe audio component
    AudioComponentDescription desc;
    desc.componentType = kAudioUnitType_Output;
    desc.componentSubType = kAudioUnitSubType_VoiceProcessingIO;
    desc.componentFlags = 0;
    desc.componentFlagsMask = 0;
    desc.componentManufacturer = kAudioUnitManufacturer_Apple;
    
    // Get component
    AudioComponent inputComponent = AudioComponentFindNext(NULL, &desc);
    
    // Get audio units
    result = AudioComponentInstanceNew(inputComponent, &ioUnit);
    printError(result);
    
    // Enable IO for recording
    UInt32 flag = 1;
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Input,
                                  inputBus,
                                  &flag,
                                  sizeof(flag));
    printError(result);
    
     // Enable IO for playback
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Output,
                                  outputBus,
                                  &flag,
                                  sizeof(flag));
    
    printError(result);
    
    
    // Describe format
    AudioStreamBasicDescription audioFormat;
    audioFormat.mSampleRate			= 8000.00;
    audioFormat.mFormatID			= kAudioFormatLinearPCM;
    audioFormat.mFormatFlags		= kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
    audioFormat.mFramesPerPacket	= 1;
    audioFormat.mChannelsPerFrame	= 1;
    audioFormat.mBitsPerChannel		= 16;
    audioFormat.mBytesPerPacket		= 2;
    audioFormat.mBytesPerFrame		= 2;
    
    // Apply format
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Output,
                                  inputBus,
                                  &audioFormat,
                                  sizeof(audioFormat));
    printError(result);
    

     result = AudioUnitSetProperty(ioUnit,
                                   kAudioUnitProperty_StreamFormat,
                                   kAudioUnitScope_Input,
                                   outputBus,
                                   &audioFormat,
                                   sizeof(audioFormat));
    
     printError(result);
    
    // Set input callback
    AURenderCallbackStruct callbackStruct;
    callbackStruct.inputProc = recordingCallback;
    //callbackStruct.inputProcRefCon = (__bridge void *)self;
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioOutputUnitProperty_SetInputCallback,
                                  kAudioUnitScope_Global,
                                  inputBus,
                                  &callbackStruct,
                                  sizeof(callbackStruct));
    printError(result);
    
    // Set output callback
    callbackStruct.inputProc = playbackCallback;
    //callbackStruct.inputProcRefCon = self;
    
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioUnitProperty_SetRenderCallback,
                                  kAudioUnitScope_Global,
                                  outputBus,
                                  &callbackStruct,
                                  sizeof(callbackStruct));
     printError(result);
    
    // Disable buffer allocation for the recorder (optional - do this if we want to pass in our own)
    flag = 0;
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioUnitProperty_ShouldAllocateBuffer,
                                  kAudioUnitScope_Output,
                                  inputBus,
                                  &flag,
                                  sizeof(flag));
    
    
     //Initialise
     //result = AudioUnitInitialize(ioUnit);
     //printError(result);
    
    Printf("Created engine successfully");
    
}

// shut down the native audio system
void shutdownAudio()
{
    AudioUnitUninitialize(ioUnit);
}

bool startAudioRecording_IOS()
{
    Printf("StartAudio Recording called.........\n");
    OSStatus result;
    
    // Enable IO for recording
    UInt32 flag = 1;
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Input,
                                  inputBus,
                                  &flag,
                                  sizeof(flag));
    printError(result);
    
    // Disable IO for playback
    flag = 0;
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Output,
                                  outputBus,
                                  &flag,
                                  sizeof(flag));
    
    printError(result);
 
    // Initialise
    result = AudioUnitInitialize(ioUnit);
    
    
    printError(result);
    
    result = AudioOutputUnitStart(ioUnit);
     printError(result);
    
    return true;
}

void stopAudioRecording_IOS()
{
    Printf("stopAudio Recording called.........\n");
     AudioOutputUnitStop(ioUnit);
     AudioUnitUninitialize(ioUnit);
    Printf("stopAudio Recording over.........\n");
}

bool startAudioPlayback_IOS()
{
    Printf("StartAudio PlaybackCalled called.........\n");
    OSStatus result;
   
    // Disable IO for recording
    UInt32 flag = 0;
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Input,
                                  inputBus,
                                  &flag,
                                  sizeof(flag));
    printError(result);
    
    // Enabel IO for playback
    flag = 1;
    result = AudioUnitSetProperty(ioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Output,
                                  outputBus,
                                  &flag,
                                  sizeof(flag));
    
    printError(result);
    
    // Initialise
    result = AudioUnitInitialize(ioUnit);
    printError(result);
    
    result = AudioOutputUnitStart(ioUnit);
    printError(result);
    
    Printf("StartAudio PlaybackCalled over.........\n");
   return true;
}

void stopAudioPlayback_IOS()
{
   AudioOutputUnitStop(ioUnit);
   AudioUnitUninitialize(ioUnit);
   isAudioPlaying = false;
}

int playBuffer(short *buff, int length)
{
   int put_bytes = 0;
   do
   {
      if (AudioPlaybackRingBuffer.getWriteAvail() >= length)
      {
         put_bytes = AudioPlaybackRingBuffer.write((char *) buff, length);
      }
      else 
      {
         //When PlaybackRingbuffer is full..we are reading out the ringbuffer to make space for new data coming in..***Fix for blue Led issue **
         //millisleep(400);  // Sleep 400 ms.
         signed short data[length];
         int bytes_read = AudioPlaybackRingBuffer.readWithTimeout((char *) data,length,QUADTICK);
         put_bytes = AudioPlaybackRingBuffer.write((char *) buff, length);
         //Printf("The playback overflowed.. Read out %d bytes and made space..\n",bytes_read);
      }
   }
   while (put_bytes == 0);

  // if (isAudioDebugging) Printf("Enqueued %d bytes.  Requested %d bytes.  %d free in play buffer.", put_bytes, length, AudioPlaybackRingBuffer.getWriteAvail());

   return(put_bytes == length);
    
}

bool playFile(const char *fn)
{
    return true;
}

void stopRecording()
{
   //only global variable is set to false... IOS stopRecording is done in different function as well as different context...
   isAudioRecording = false;
   emptyAudioRecordingBuffer();

}

int getAvailablePlaybackSpace()
{
   return (((float) AudioPlaybackRingBuffer.getWriteAvail() / (float) MAX_PLAYBACK_BUFFER_SIZE) * 1000);
}



bool isAudioPlaybackBufferEmpty()
{
   return AudioPlaybackRingBuffer.isEmpty();
}



void emptyAudioPlaybackBuffer()
{
   isAudioPlaying = false;

   AudioPlaybackRingBuffer.empty();
}

void emptyAudioRecordingBuffer()
{
   AudioCaptureRingBuffer.empty(); //Added abhi.. not present...
}


int getRecordBuffer(signed short *buf, int frames_to_read)
{
   int get_bytes = 0;

   int length_bytes = frames_to_read * sizeof(signed short);

   if (AudioCaptureRingBuffer.getReadAvail() > length_bytes)
   {
       get_bytes = AudioCaptureRingBuffer.read((char *) buf, length_bytes);
      //Printf("RingBuffer : Read %d bytes\n",get_bytes);
   }

   return(get_bytes);
}


void *AudioThread(void *ptr)
{
   createEngine();
    
   isAudioAlive = true;
   isCaptureOpen = false;
   isPlaybackOpen = false;

  // Printf("Setup Audo REcorder \n");

   while (isAudioAlive)
   {
      if (isAudioRecording)   // We need to record.
      {
         if (isPlaybackOpen)
         {
            stopAudioPlayback_IOS();
            isPlaybackOpen = false;
         }
         else {

            if (!AudioCaptureRingBuffer.isFull())
            {
               if (!isCaptureOpen)
               {
                  if (startAudioRecording_IOS())
                  {
                     isCaptureOpen = true;
             //Printf("RECORD: OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
                  }
                  else {
                     stopAudioRecording_IOS();
                     isCaptureOpen = false;
                  }
               }
            }
        }
      }
      else if(isAudioStreaming)// We need to play.
      {
         if (isCaptureOpen)
         {
            //TracePoint("RECORD: CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
            stopAudioRecording_IOS();
            isCaptureOpen = false;
         }
         else {
            if (!AudioPlaybackRingBuffer.isEmpty())
            {
                if(!isPlaybackOpen)
                {
                   if (startAudioPlayback_IOS())
                   {
                      isPlaybackOpen = true;
                      //         TracePoint("RECORD: OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
                   }
                   else {
                       stopAudioPlayback_IOS();
                       isPlaybackOpen = false;
                   }
                }
                else
                {
                    /*
                    if(!isAudioStreaming)
                    {
                        stopAudioPlayback_IOS();
                        isPlaybackOpen = false;
                    }
                     */
                }
            }
           
         }
      }
      else
      {
          if(isPlaybackOpen)
          {
              stopAudioPlayback_IOS();
              isPlaybackOpen = false;
          }
         
          if(isCaptureOpen)
          {
              stopAudioRecording_IOS();
              isCaptureOpen = false;
          }
      }
       
      millisleep(TICKWAIT);  // This keep the CPU utilization from going crazy.
   }
   
    millisleep(TICKWAIT);  // You will not starve the PCM device here, they are closed.
   shutdownAudio();
    return 0;
}


void initSound_IOS()
{
   isAudioRecording = false;
   isAudioPlaying = false;
    
   int rc = pthread_create(&AudioThreadHandle, NULL, AudioThread, NULL);
   Printf("Created AudioThread %d", rc);
}

void deInitSound_IOS()
{
   //Printf("IOS Sound Deinit\n");
   isAudioAlive = false;
}
