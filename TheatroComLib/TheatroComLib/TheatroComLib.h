//
//  TheatroComLib.h
//  TheatroComLib
//
//  Created by Ravi Shankar on 04/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#include "TheatroCommunicator.h"
#import <Foundation/Foundation.h>

#include <stdio.h>
#include <sys/socket.h>
#include <semaphore.h>

#include "TheatroDefs.h"
#include "cJSON.h"
#include "Trace.h"
#include "Callbacks.h"

extern char TGSIPAddress[NAMESIZE];
extern char appVersion[NAMESIZE];


typedef enum {
    WIFI_OFF_CELLULAR_OFF = 0,
    WIFI_OFF_CELLULAR_ON = 1,
    WIFI_ON_TGS_ON = 2,
    WIFI_ON_TGS_OFF = 3,
} NETWORK_STATE;


@interface TheatroComLib : NSObject

void startTheatroNative();
void killTheatroNativeLib();
void doMenuButtonAction(bool isDown);
void doQueryButtonAction(bool isDown);
void sendIpToNative(char* Ip);
void sendProxyAddressToNative(char* ipAddress);
void sendLoginNameToNative(char *name);
void sendLogoffToNative();
void sendMacAddress(char *comId);
void sendChainNameAndStoreNameToNative(char *chainName);
void initCallbacks (updateStatus status , SEL sel, id idf);
void sendRefreshEvent();
void EnableBroadcastScreen(bool value);
void sendPlayMsgToNative();
void sendPlayAnncToNative();
void sendCancelPlayOutToNative(bool isStopped);
void sendCallEndToNative();
void sendPlaybufferenable(bool value);
void doMediabuttonAction(char *typeStr, bool down);
void sendHuddleInfoToNative(char *time , int type);
void sendVersionToNative(char *ver);
void sendMsgActionToNative(int type,char *txnId);

void sendDataToNative(void *buffer, unsigned int size);
unsigned int readDataFromNative(void *buffer,unsigned int size);
unsigned int getAvailablePlaybackDataFromNative();
bool isAudioCaptureRingBufferFull();
bool isAudioPlaybackRingBufferEmpty();
void reOpenClosedSockets();
void clearOraIdFromNative(); //Added for slow internet connections...

void sendNetworkStatusToNative(NETWORK_STATE value);
void sendWebSocketPathToNative(char *path);
void sendSwitchStatusToNative(bool value);
void sendChainNameToNative(char *chainName);

//Websocket changes.


@end

extern NETWORK_STATE currentNetworkStatus;

