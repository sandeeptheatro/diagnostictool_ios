//
//  TheatroComLib.m
//  TheatroComLib
//
//  Created by Ravi Shankar on 04/12/1936 SAKA.
//  Copyright (c) 1936 SAKA theatro. All rights reserved.
//

#import "TheatroComLib.h"
#include <time.h>
#include <semaphore.h>

#ifdef ObjC
export "C"
{
#endif
    
#include "libwebsockets.h"
    
#ifdef ObjC
}
#endif

//static int destroy_flag = 0;
static int writeable_flag = 0;
static int webSocketIsWritable = 0;
sem_t writeDataSemphore;

char websocketDomain[400];
char chainNameNative[NAMESIZE];


struct session_data {
    int fd;
};

struct lws_context *context = NULL;
struct lws *wsi = NULL;
struct lws_context_creation_info info;
struct lws_protocols protocol;


pthread_t MainThreadHandle;

updateStatus globalStatus;
SEL selector;
id identifier;

extern char CommunicatorMACAddress[];

@implementation TheatroComLib

void startTheatroNative()
{
    startCommunicator();
    NSLog(@"Start communicator called...\n");
    //pthread_create(&MainThreadHandle, NULL, startCommunicator, NULL);
}

void initCallbacks (updateStatus status , SEL sel, id idf)
{
    selector = sel;
    identifier = idf;
    globalStatus = status;
}
/*!
 *  @author Sandeep Challa
 *
 *  nvdbnv,m,nsm,bnam,m,sanm,,msam,nd,
 */
void killTheatroNativeLib()
{
    //Deinit the soud also...
    exitCommunicator();
}

void doMenuButtonAction(bool isDown)
{
    performPrimaryButtonAction((bool) isDown);
    //status();
}

void doQueryButtonAction(bool isDown)
{
    performBroadcastButtonAction((bool) isDown);
}

void sendIpToNative(char* Ip)
{
    strcpy(TGSIPAddress,Ip);
    isIpSetFromApp = true;
    NSLog(@"The TGSIPADDRESS in Native: = %s\n",TGSIPAddress);
}

void stausCallback(char * coverageString)
{
   globalStatus(identifier,selector,coverageString);
}

void sendProxyAddressToNative(char* ipAddress)
{
    strcpy(TGSIPAddress,ipAddress);
    //Printf("+++++++++++++THe new Proxy address is ++++++++++++++++++ = %s\n",TGSIPAddress);
    //isIpSetFromJava = false;
}

void sendChainNameAndStoreNameToNative(char *chainName)
{
    strcpy(Dest,chainName);
}

void sendMacAddress(char *comId)
{
    strcpy(CommunicatorMACAddress,comId);
}

void sendLoginNameToNative(char *name)
{
    performLoginButtonAction(name);
}

void sendLogoffToNative()
{
    performButtonAction("logoff");
}

void sendRefreshEvent()
{
    performRefreshButtonAction();
}

void EnableBroadcastScreen(bool value)
{
  performBroadcastScreenAction(value);
}

void sendPlayMsgToNative()
{
    performButtonAction("playmsg");
    isCancelledFromJava= false;
}
void sendPlayAnncToNative()
{
    performButtonAction("playAnnc");
    isCancelledFromJava= false;
}

void sendCancelPlayOutToNative(bool isStopped)
{
    stopPlayBackFromJava(isStopped);
}

void sendCallEndToNative()
{
    performButtonAction("callend");
}

void sendPlaybufferenable(bool value)
{
    performPlayBufferAction(value);
}

void doMediabuttonAction(char *typeStr, bool down)
{
    performCommAction(typeStr,down);
}

void sendDataToNative(void *buffer, unsigned int size)
{
    performRingBufferWrite(buffer,size);
}

unsigned int readDataFromNative(void *buffer,unsigned int size)
{
    return performRingBufferRead(buffer,size);
}

unsigned int getAvailablePlaybackDataFromNative()
{
    return getAvailablePlaybackData();
}

bool isAudioCaptureRingBufferFull()
{
    return isAudioCaptureBufferFull();
}

bool isAudioPlaybackRingBufferEmpty()
{
    return isAudioPlaybackBufferEmpty();
}

void reOpenClosedSockets()
{
    reOpenSockets();
}

void sendHuddleInfoToNative(char *time , int type)
{
    performHuddleAction(time,type);
}

void clearOraIdFromNative() 
{
    clearOraId();
}

void sendVersionToNative(char *ver)
{
    strcpy(appVersion,ver);
    
}

void sendMsgActionToNative(int type, char *txnId)
{
    switch (type) {
            
        case 1:
            performMsgButtonAction("msgheard", txnId);
            break;
            
        case 2:
            performMsgButtonAction("msgdeleted",txnId);
            break;
            
        case 3:
            performMsgButtonAction("msgsaved",txnId);
            break;
            
        default:
            break;
    }
}

void sendNetworkStatusToNative(NETWORK_STATE value)
{
#if 0
    if(currentNetworkStatus != value)
    {
        printf("Native: sendNetworkStatusToNative  calledd Prev =%d , New=%d\n",currentNetworkStatus,value);
        if(currentNetworkStatus != WIFI_OFF_CELLULAR_OFF)
        {
           exitCommunicator();
           currentNetworkStatus = value;
           startCommunicator();
        }
        currentNetworkStatus = value;
    }
#endif
    
    currentNetworkStatus = value;
    NSLog(@"Native:  currentNetwork status=%d \n",currentNetworkStatus);
    
}

void sendWebSocketPathToNative(char *path)
{
    strcpy(websocketDomain,path);
}

void sendSwitchStatusToNative(bool value)
{
    isNetworkSwitchFromApp = value;
}

void sendChainNameToNative(char *chainName)
{
    strcpy(chainNameNative ,chainName);
}





//Websocket implementation...

static int websocket_write_back(struct lws *wsi_in, char *str, int str_size_in)
{
    //lws_callback_on_writable(wsi);
    
    //sem_wait(&writeDataSemphore);


    if (str == NULL || wsi_in == NULL)
    {
        if(str == NULL)
        {
            printf ("String null\n");
        }
        if(wsi_in == NULL)
        {
            printf ("wsi_in null\n");
        }
        printf("Not writable...%s\n", str);
        return -1;
    }
    
    
    int n;
    int len;
    char *out = NULL;
    
    //if (str_size_in < 1)
      //  len = strlen(str);
    //else
    len = str_size_in;
    
    out = (char *)malloc(sizeof(char)*(LWS_SEND_BUFFER_PRE_PADDING + len + LWS_SEND_BUFFER_POST_PADDING));
    
    //printf("Websocket sending %s \n", str);
    
    char temp[3] = {0};
    strncpy(temp,str,2);
    temp[2] = '\0';
    
    
    if((strcmp(temp,"AB"))==0)
    {
        
        str[1] = 'O';
        memcpy (out + LWS_SEND_BUFFER_PRE_PADDING , str, len );
        n = lws_write(wsi_in, out + LWS_SEND_BUFFER_PRE_PADDING, len , LWS_WRITE_BINARY);
        
    }
    else
    {
    
     memcpy (out + LWS_SEND_BUFFER_PRE_PADDING , str, len );
     n = lws_write(wsi_in, out + LWS_SEND_BUFFER_PRE_PADDING, len , LWS_WRITE_TEXT);
    }
    //* free the buffer*/
    free(out);

    //lws_callback_on_writable(wsi);
    
    return n;
}


static int ws_service_callback(
                               struct lws *wsi,
                               enum lws_callback_reasons reason, void *user,
                               void *in, size_t len)
{
    char msgType[2];
    
    switch (reason) {
            
        case LWS_CALLBACK_CLIENT_ESTABLISHED:
            // __android_log_print(ANDROID_LOG_VERBOSE,"websocket", "Connect with server success.", 1);
            printf("Native: Websocket Connection established\n");
            lws_callback_on_writable(wsi);
            
            sem_post(&writeDataSemphore);

            
            connection_flag = 1;
            break;
            
        case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
            printf("Native: Websocket Connection error\n");
            // __android_log_print(ANDROID_LOG_VERBOSE,"websocket", "Connect with server error", 1);
            
            destroy_flag = 1;
            connection_flag = 0;
            webSocketIsWritable = 0;
            return -1;
            
            break;
            
        case LWS_CALLBACK_CLOSED:
            // __android_log_print(ANDROID_LOG_VERBOSE,"websocket", "LWS_CALLBACK_CLOSED", 1);
            printf("Native: Websocket Connection callback closed returning -1\n");
            
            webSocketIsWritable = 0;
            destroy_flag = 1;
            connection_flag = 0;
            return -1;
            
            break;
            
        case LWS_CALLBACK_CLIENT_RECEIVE:
            
            if(destroy_flag)
            {
                printf("Native: In Receive Returning -1 for closing the connection...\n");
                return -1;
            }
            
            // __android_log_print(ANDROID_LOG_VERBOSE,"websocket", "Client recvived", 1);
            //printf("data received");
            //printf("crash time=%d [Main Service] Client recvived:%s\n", (int)time(NULL),(char *)in);
            
            memcpy(inputBuf[putIndex],(char *)in+2,len - 2);
            
            if(putIndex == MAX_NUM_PACKETS-1 )
                putIndex = 0;
            else
                putIndex = putIndex + 1;
            
            countOra ++;
            //printf("checkAI putIndex =%d",putIndex);
            //printf("checkAI Timestamp: %d and putIndex is=%d\n",(int)time(NULL),putIndex);
            isWebSocketDataAvailable = true;
            
            break;
            
            //if (writeable_flag)
              //  destroy_flag = 1;
        case LWS_CALLBACK_CLIENT_WRITEABLE :
            
            if(destroy_flag)
            {
                printf("Native: In Writable : Retuning -1 for closing the connection...\n");
                return -1;
            }
            
            if(putIndexOutput != getIndexOutput)
            {
                //printf("The getIndex  data is %s and strlen=%d \n",outputBuf[getIndexOutput], strlen(outputBuf[getIndexOutput]));
                
                websocket_write_back(wsi, outputBuf[getIndexOutput], strlen(outputBuf[getIndexOutput]));
                
                //struct timeval t;
                //gettimeofday(&t, NULL);
                
              //  printf("mosValue: write interval=%llu\n",(unsigned long) t.tv_sec * 1000ULL + t.tv_usec / 1000);
                
                lws_callback_on_writable( wsi );
                
                if(getIndexOutput == MAX_NUM_PACKETS-1 )
                    getIndexOutput = 0;
                else
                    getIndexOutput = getIndexOutput + 1;
            }
            
           // printf("WebSocket writable callback ");
            
            break;
            
        default:
            break;
    }
    
    return 0;
}


void webSocketThread()
{
    memset(&info, 0, sizeof info);
    info.port = CONTEXT_PORT_NO_LISTEN;
    info.iface = NULL;
    info.protocols = &protocol;
    info.ssl_cert_filepath = NULL;
    info.ssl_private_key_filepath = NULL;
    info.extensions = lws_get_internal_extensions();
    info.gid = -1;
    info.uid = -1;
    //info.options = 0;
    info.options |= LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;
    
    
    protocol.name  = "my-echo-protocol";
    protocol.callback = &ws_service_callback;
    protocol.per_session_data_size = sizeof(struct session_data);
    protocol.rx_buffer_size = 0;
    //protocol.tx_packet_size = 0;
    protocol.id = 0;
    protocol.user = NULL;
    
    context = lws_create_context(&info);
    printf("[Main] context created.\n");
    
    if (context == NULL) {
        //printf(KRED"[Main] context is NULL.\n"RESET);
        //  return -1;
    }
    
    //__android_log_print(ANDROID_LOG_VERBOSE,"websocket", "before connect  calledi connecting prashant", 1);
    
    
    
    char tempNamewithPort[400];
    char tempUrl[NAMESIZE];
    
    memset(tempNamewithPort,sizeof(tempNamewithPort),0);
    memset(tempUrl,sizeof(tempUrl),0);
    
    strcpy(tempNamewithPort,websocketDomain);
    strcat(tempNamewithPort,":443");
    
    strcpy(tempUrl,"/managerapp/");
    strcat(tempUrl,chainNameNative);
    
    printf("Native: lws_client_connect to %s and chain = %s\n",websocketDomain,chainNameNative);
    
    wsi = lws_client_connect(context, websocketDomain, 443, 2,
                             tempUrl, tempNamewithPort, NULL,
                             protocol.name, -1);
   
    if (wsi == NULL) {
        printf("[Main] wsi create error.\n");
        //      return -1;
    }
    
   printf("Native: lws_client_connect after\n");
    
    while (!connection_flag)
    {
        lws_service(context, 50);
        if(destroy_flag)
        {
            break;
        }
    }
    
    if(connection_flag)
    {
       printf("Native:+++++++++++++++++ lws_client_connect successful\n");
    }
    //return 1;
    
    while (!destroy_flag)
    {
        //usleep(25000);
        usleep(5000);
        lws_callback_on_writable( wsi );
        lws_service(context, 50);
    }
    
    //lws_callback_on_writable( wsi );
    
    
    lws_context_destroy(context);
    printf("Native:-------------------- lws_context_destroyed successfully\n");
    
}

int webSocket_init()
{
    destroy_flag = 0;
    connection_flag = 0;
    
    int rc = pthread_create(&WebSocketThreadHandle, NULL, webSocketThread,
                        NULL);
    return 0;
}

void webSocket_loopService()
{
    lws_service(context, 10);
}

int webSocket_sendData(char *buffer, int len, int type)
{
    if(connection_flag)
    {
      //websocket_write_back(wsi,buffer,len,type);
        memset(outputBuf[putIndexOutput],0,MAX_PACKET_SIZE_OUTPUT);

        if(type == 1)
        {
            memcpy (outputBuf[putIndexOutput] , "AU", 2 );
            memcpy (outputBuf[putIndexOutput] + 2 , buffer , len );
            
        }
        else if(type == 2 || type == 4)
        {
            
            
            if(type == 4)
            {
                memcpy (outputBuf[putIndexOutput] , "AB", 2 );
                memcpy (outputBuf[putIndexOutput] + 2 , buf , len );
            }
            else
            {
                 memcpy (outputBuf[putIndexOutput] , "AO", 2 );
                 memcpy (outputBuf[putIndexOutput] + 2 , buffer , len );
            }
        }
        else if(type == 3)
        {
            memcpy (outputBuf[putIndexOutput] , "PC", 2 );
            memcpy (outputBuf[putIndexOutput] + 2 , buffer , len );
        }
        
        if(putIndexOutput == MAX_NUM_PACKETS-1 )
            putIndexOutput = 0;
        else{
            putIndexOutput = putIndexOutput + 1;
        }
        
       // lws_callback_on_writable(wsi);
    }
    else
    {
        printf("Sending %s failed\n", buffer);
        return 0;
    }
    
    return 1;
}

int webSocket_deinit()
{
    printf("Native: Websocket Deinit waiting for write complete... \n");
    
    while (putIndexOutput != getIndexOutput) {
        lws_callback_on_writable(wsi);
        usleep(10000);
    }
    
     printf("Native: Websocket Deinit Write Completed...\n");
    destroy_flag = 1;
    return 0;
}

int webSocket_wait()
{
    printf("Native: Websocket websocket wait ****************************** pending...%d\n",destroy_flag);
    pthread_join(WebSocketThreadHandle, NULL);
      printf("Native: Websocket websocket wait ****************************** success...%d\n",destroy_flag);
    return 0;
}
@end





