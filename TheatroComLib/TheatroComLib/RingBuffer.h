/*
 * RingBuffer.h
 *
 *  Created on: May 20, 2013
 *      Author: vincente
 */

#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

#include <pthread.h>

class RingBuffer
{
public:
	RingBuffer(int sizeBytes);
	~RingBuffer();
	int read(char* dataPtr, int numBytes);
	int readWithTimeout(char *dataPtr, int numBytes, int ms);

	int write(char *dataPtr, int numBytes);
	bool empty(void);
	int getSize();
	int getWriteAvail();
	int getReadAvail();
	inline bool isEmpty() { return (_writeBytesAvail == _size); }
	inline bool isFull() { return (_writeBytesAvail == 0); }

private:
	char * _data;
	int _size;
	int _readPtr;
	int _writePtr;
	int _writeBytesAvail;

	pthread_mutex_t my_mutex;
	pthread_cond_t empty_condition;
	pthread_cond_t full_condition;
};

#endif /* RINGBUFFER_H_ */
