/*
 * Trace.h
 *
 *  Created on: Apr 15, 2013
 *      Author: vincente
 */

#ifndef TRACE_H_
#define TRACE_H_

extern bool isDebugging;

void initTrace(bool toSyslog);
void closeTrace();

void tracePrintf(const char *SourceFilename, const char *SourceFunction, int SourceLineno, const char *CFormatString, ...);

int tracePrintBigString(char *str);

#ifdef DEBUG
#define Printf(fmt,args...)  tracePrintf(__FILE__, __FUNCTION__, __LINE__, fmt, ##args)
#define TracePoint(fmt,args...)  if (isDebugging) tracePrintf(__FILE__, __FUNCTION__, __LINE__, fmt, ##args)
#define BreadCrumb  if (isDebugging) tracePrintf(__FILE__, __FUNCTION__, __LINE__, "")
#define Begin if (isDebugging) tracePrintf(__FILE__, __FUNCTION__, __LINE__, "Begin {")
#define End	if (isDebugging) tracePrintf(__FILE__, __FUNCTION__, __LINE__, "} End")
#define Return(R) 				\
	return(isDebugging ? (tracePrintf(__FILE__, __FUNCTION__, __LINE__, "} Return (0x%1x)", (long)(R)), (R)) : R )
#define CheckReturnVal(rc) if(rc<0)tracePrintf(__FILE__, __FUNCTION__, __LINE__, "ERROR. Function returned negative value")
#define CheckNullPtr(ptr) if(ptr==NULL)tracePrintf(__FILE__, __FUNCTION__, __LINE__, "ERROR. NULL Pointer")

#else
#define TracePoint
#define BreadCrumb
#define	Printf	printf
#define Begin
#define End
#define Return(R)  return R
#define CheckReturnVal(rc)
#endif

#endif /* TRACE_H_ */

