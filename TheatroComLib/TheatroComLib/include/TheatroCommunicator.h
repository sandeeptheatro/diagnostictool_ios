/*
 * TheatroCommunicator.h
 *
 *  Created on: May 6, 2013
 *      Author: vincente
 *  Abhinandan Chougala : Ported to android.
 *
 */


#ifndef THEATROCOMMUNICATOR_H_
#define THEATROCOMMUNICATOR_H_

#include "TheatroDefs.h"
#include <pthread.h>
#include "cJSON.h"
#include<stdbool.h>

#define TICKWAIT	  25
#define	QUADTICK	 100
#define	HALFSECOND	 500
#define	ONESECOND	1000

#define NELEM(a)  (sizeof(a) / sizeof(a)[0])

#define QUOTEME(x) #x

#define	initDefaultString(STR, SIZE) \
   memset(STR, 0, SIZE); \
if (DefaultsObject) strncpy(STR, cJSON_GetObjectItem(DefaultsObject, QUOTEME(STR)) -> valuestring, SIZE); \
if (DefaultsFileObject) if (cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(STR))) strncpy(STR, cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(STR)) -> valuestring, SIZE);

#define	initDefaultInt(INT) \
   if (DefaultsObject) INT = cJSON_GetObjectItem(DefaultsObject, QUOTEME(INT)) -> valueint; \
if (DefaultsFileObject) if (cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(INT))) INT = cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(INT)) -> valueint;

#define	initDefaultNumber(NUM) \
   if (DefaultsObject) NUM = cJSON_GetObjectItem(DefaultsObject, QUOTEME(NUM)) -> valuedouble; \
if (DefaultsFileObject) if (cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(NUM))) NUM = cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(NUM)) -> valuedouble;

#define	initDefaultBool(BOOL) \
   if (DefaultsObject) BOOL = (bool) cJSON_GetObjectItem(DefaultsObject, QUOTEME(BOOL)) -> valueint; \
if (DefaultsFileObject) if (cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(BOOL))) BOOL = (bool) cJSON_GetObjectItem(DefaultsFileObject, QUOTEME(BOOL)) -> valueint;

#define	getJSONInt(JSONOBJECT, INT) \
   if (JSONOBJECT) if (cJSON_GetObjectItem(JSONOBJECT, QUOTEME(INT))) INT = cJSON_GetObjectItem(JSONOBJECT, QUOTEME(INT)) -> valueint;

#define	getJSONString(JSONOBJECT, STR, SIZE) \
   memset(STR, 0, SIZE); \
if (JSONOBJECT) if (cJSON_GetObjectItem(JSONOBJECT, QUOTEME(STR))) strncpy(STR, cJSON_GetObjectItem(JSONOBJECT, QUOTEME(STR)) -> valuestring, SIZE);

#define	getJSONBool(JSONOBJECT, BOOL) \
   if (JSONOBJECT) if (cJSON_GetObjectItem(JSONOBJECT, QUOTEME(BOOL))) BOOL = (bool) cJSON_GetObjectItem(JSONOBJECT, QUOTEME(BOOL)) -> valueint;


extern pthread_mutex_t mutex;	 // Mutex for updating globals shared between C threads and Java.

extern char TGSIPAddress[NAMESIZE];
extern char TgsIp[NAMESIZE];

extern bool isCommunicatorAlive;
extern bool isIpSetFromJava;
void performBroadcastButtonAction(bool isDown);
void performBroadcastScreenAction(bool isBroadcast);
void performPrimaryButtonAction(bool isDown);
void performVolumeUpButtonAction(bool isDown);
void performVolumeDownButtonAction(bool isDown);
void performChargingAction(bool isDown);
void performChargedAction(bool isDown);

//void performPrimaryButtonAction(char * buttonTxt , bool isDown);
int buildJSONButtonPacket(char *buf, char *button_text, bool button_state);
void performLoginButtonAction(char *LoginName);
void performRefreshButtonAction();
void performButtonAction(char *type);
void startCommunicator(char *filename);
void exitCommunicator();
void stopPlayBackFromJava();
void performPlayBufferAction(bool value);

#endif /* THEATROCOMMUNICATOR_H_ */
