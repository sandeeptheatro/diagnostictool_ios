/*
 * TheatroUtilities.cpp
 *
 *  Created on: May 15, 2013
 *      Author: vincente
 */

#include <sys/time.h>
#include <unistd.h>
#include <sys/socket.h>
#include <errno.h>
#include <ctype.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#include <stdio.h>
#include <string.h>


//#define DEBUG
#include "Trace.h"


/* convert ms(milliseconds) to timeval struct */
void ms2tv(struct timeval *result, unsigned long interval_ms)
{
	result->tv_sec = (interval_ms / 1000);
	result->tv_usec = ((interval_ms % 1000) * 1000);
}


int millisleep(unsigned ms)
{
	return usleep(1000 * ms);
}


unsigned long long getCurrentTimeMillis()
{
	struct timeval t;
	gettimeofday(&t, NULL);

	return (unsigned long) t.tv_sec * 1000ULL + t.tv_usec / 1000;
}



#define INT_TO_ADDR(_addr) \
(_addr & 0xFF), \
(_addr >> 8 & 0xFF), \
(_addr >> 16 & 0xFF), \
(_addr >> 24 & 0xFF)



int getIPAddress(char *interface)
{
	struct ifconf ifc;
	struct ifreq ifr[10];
	int sd = 0;
	int ifc_num = 0;
	int addr = 0;

	/* Create a socket so we can use ioctl on the file
	 * descriptor to retrieve the interface info.
	 */
	sd = socket(PF_INET, SOCK_DGRAM, 0);
	if (sd > 0)
	{
		ifc.ifc_len = sizeof(ifr);
		ifc.ifc_ifcu.ifcu_buf = (caddr_t)ifr;

		if (ioctl(sd, SIOCGIFCONF, &ifc) == 0)
		{
			ifc_num = ifc.ifc_len / sizeof(struct ifreq);

			for (int i = 0; i < ifc_num; ++i)
			{
				if (ifr[i].ifr_addr.sa_family != AF_INET)
				{
					continue;
				}

				/* Find the interface name */

				if (strcmp(interface, ifr[i].ifr_name) == 0)
				{

					/* Retrieve the IP address, broadcast address, and subnet mask. */
					if (ioctl(sd, SIOCGIFADDR, &ifr[i]) == 0)
					{
						addr = ((struct sockaddr_in *)(&ifr[i].ifr_addr))->sin_addr.s_addr;
					}
				}
			}
		}
		close(sd);
	}
	return addr;
}



int getBroadcastAddress(char *interface)
{
	struct ifconf ifc;
	struct ifreq ifr[10];
	int sd = 0;
	int ifc_num = 0;
	int bcast = 0;

	/* Create a socket so we can use ioctl on the file
	 * descriptor to retrieve the interface info.
	 */
	sd = socket(PF_INET, SOCK_DGRAM, 0);
	if (sd > 0)
	{
		ifc.ifc_len = sizeof(ifr);
		ifc.ifc_ifcu.ifcu_buf = (caddr_t)ifr;

		if (ioctl(sd, SIOCGIFCONF, &ifc) == 0)
		{
			ifc_num = ifc.ifc_len / sizeof(struct ifreq);

			for (int i = 0; i < ifc_num; ++i)
			{
				if (ifr[i].ifr_addr.sa_family != AF_INET)
				{
					continue;
				}

				/* Find the interface name */

				if (strcmp(interface, ifr[i].ifr_name) == 0)
				{
					if (ioctl(sd, SIOCGIFBRDADDR, &ifr[i]) == 0)
					{
						bcast = ((struct sockaddr_in *)(&ifr[i].ifr_broadaddr))->sin_addr.s_addr;
					}
				}
			}
		}
		close(sd);
	}
	return bcast;
}



int getNetworkMask(char *interface)
{
	struct ifconf ifc;
	struct ifreq ifr[10];
	int sd = 0;
	int ifc_num = 0;
	int mask = 0;

	/* Create a socket so we can use ioctl on the file
	 * descriptor to retrieve the interface info.
	 */
	sd = socket(PF_INET, SOCK_DGRAM, 0);
	if (sd > 0)
	{
		ifc.ifc_len = sizeof(ifr);
		ifc.ifc_ifcu.ifcu_buf = (caddr_t)ifr;

		if (ioctl(sd, SIOCGIFCONF, &ifc) == 0)
		{
			ifc_num = ifc.ifc_len / sizeof(struct ifreq);

			for (int i = 0; i < ifc_num; ++i)
			{
				if (ifr[i].ifr_addr.sa_family != AF_INET)
				{
					continue;
				}

				/* Find the interface name */

				if (strcmp(interface, ifr[i].ifr_name) == 0)
				{
					if (ioctl(sd, SIOCGIFBRDADDR, &ifr[i]) == 0)
					{
						mask = ((struct sockaddr_in *)(&ifr[i].ifr_broadaddr))->sin_addr.s_addr;
					}
				}
			}
		}
		close(sd);
	}
	return mask;
}



int getNetwork(int addr, int mask)
{
	return(addr & mask);
}


//
//
//int main()
//{
//    struct ifconf ifc;
//    struct ifreq ifr[10];
//    int sd, ifc_num, addr, bcast, mask, network, i;
//
//    /* Create a socket so we can use ioctl on the file
//     * descriptor to retrieve the interface info.
//     */
//
//    sd = socket(PF_INET, SOCK_DGRAM, 0);
//    if (sd > 0)
//    {
//        ifc.ifc_len = sizeof(ifr);
//        ifc.ifc_ifcu.ifcu_buf = (caddr_t)ifr;
//
//        if (ioctl(sd, SIOCGIFCONF, &ifc) == 0)
//        {
//            ifc_num = ifc.ifc_len / sizeof(struct ifreq);
//            printf("%d interfaces found\n", ifc_num);
//
//            for (i = 0; i < ifc_num; ++i)
//            {
//                if (ifr[i].ifr_addr.sa_family != AF_INET)
//                {
//                    continue;
//                }
//
//                /* display the interface name */
//                printf("%d) interface: %s\n", i+1, ifr[i].ifr_name);
//
//                /* Retrieve the IP address, broadcast address, and subnet mask. */
//                if (ioctl(sd, SIOCGIFADDR, &ifr[i]) == 0)
//                {
//                    addr = ((struct sockaddr_in *)(&ifr[i].ifr_addr))->sin_addr.s_addr;
//                    printf("%d) address: %d.%d.%d.%d\n", i+1, INT_TO_ADDR(addr));
//                }
//                if (ioctl(sd, SIOCGIFBRDADDR, &ifr[i]) == 0)
//                {
//                    bcast = ((struct sockaddr_in *)(&ifr[i].ifr_broadaddr))->sin_addr.s_addr;
//                    printf("%d) broadcast: %d.%d.%d.%d\n", i+1, INT_TO_ADDR(bcast));
//                }
//                if (ioctl(sd, SIOCGIFNETMASK, &ifr[i]) == 0)
//                {
//                    mask = ((struct sockaddr_in *)(&ifr[i].ifr_netmask))->sin_addr.s_addr;
//                    printf("%d) netmask: %d.%d.%d.%d\n", i+1, INT_TO_ADDR(mask));
//                }
//
//                /* Compute the current network value from the address and netmask. */
//                network = addr & mask;
//                printf("%d) network: %d.%d.%d.%d\n", i+1, INT_TO_ADDR(network));
//            }
//        }
//
//        close(sd);
//    }
//
//    return 0;
//}
//







//            1111111
//  01234567890123456
// "00:1e:4a:31:cc:c1"
//     //_//
//    ////    11
//  012345678901
// "001e4a31ccc1"

void removeColonsFromMACAddress(char *mac)
{
	if (mac[2] == ':')
	{
		mac[2] = mac[3];
		mac[3] = mac[4];

		if (mac[5] == ':')
		{
			mac[4] = mac[6];
			mac[5] = mac[7];

			if (mac[8] == ':')
			{
				mac[6] = mac[9];
				mac[7] = mac[10];

				if (mac[11] == ':')
				{
					mac[8] = mac[12];
					mac[9] = mac[13];

					if (mac[14] == ':')
					{
						mac[10] = mac[15];
						mac[11] = mac[16];
						mac[12] = '\0';
					}
				}
			}
		}
	}
}



void removeSeparatorsFromMACAddress(char *mac)
{
	if (!isxdigit(mac[2]))
	{
		mac[2] = mac[3];
		mac[3] = mac[4];

		if (!isxdigit(mac[5]))
		{
			mac[4] = mac[6];
			mac[5] = mac[7];

			if (!isxdigit(mac[8]))
			{
				mac[6] = mac[9];
				mac[7] = mac[10];

				if (!isxdigit(mac[11]))
				{
					mac[8] = mac[12];
					mac[9] = mac[13];

					if (!isxdigit(mac[14]))
					{
						mac[10] = mac[15];
						mac[11] = mac[16];
						mac[12] = '\0';
					}
				}
			}
		}
	}
}




//            1111111
//  01234567890123456
// "00:1e:4a:31:cc:c1"
//     //_//
//    ////    11
//  012345678901
// "001e4a31ccc1"

void addSeparatorToMACAddress(char *mac, char separator)
{
	if (sizeof(mac) >= 17)
	{
		if (isxdigit(mac[11] && isxdigit(mac[10])))
		{
			mac[17] = '\0';
			mac[16] = mac[11];
			mac[15] = mac[10];
			mac[14] = separator;

			if (isxdigit(mac[9] && isxdigit(mac[8])))
			{
				mac[13] = mac[9];
				mac[12] = mac[8];
				mac[11] = separator;

				if (isxdigit(mac[7] && isxdigit(mac[6])))
				{
					mac[10] = mac[7];
					mac[9] = mac[6];
					mac[8] = separator;

					if (isxdigit(mac[5] && isxdigit(mac[4])))
					{
						mac[7] = mac[5];
						mac[6] = mac[4];
						mac[5] = separator;

						if (isxdigit(mac[3] && isxdigit(mac[2])))
						{
							mac[4] = mac[3];
							mac[3] = mac[2];
							mac[2] = separator;
						}
					}
				}
			}
		}
	}
}



/*
 ** This routine converts from linear to ulaw.
 **
 ** Craig Reese: IDA/Supercomputing Research Center
 ** Joe Campbell: Department of Defense
 ** 29 September 1989
 **
 ** References:
 ** 1) CCITT Recommendation G.711  (very difficult to follow)
 ** 2) "A New Digital Technique for Implementation of Any
 **     Continuous PCM Companding Law," Villeret, Michel,
 **     et al. 1973 IEEE Int. Conf. on Communications, Vol 1,
 **     1973, pg. 11.12-11.17
 ** 3) MIL-STD-188-113,"Interoperability and Performance Standards
 **     for Analog-to_Digital Conversion Techniques,"
 **     17 February 1987
 **
 ** Input: Signed 16 bit linear sample
 ** Output: 8 bit ulaw sample
 */

//#define ZEROTRAP    /* turn on the trap as per the MIL-STD */
//#undef ZEROTRAP
#define BIAS 0x84   /* define the add-in bias for 16 bit samples */
#define CLIP 32635

unsigned char linear2ulaw(int sample)
{
	static int exp_lut[256] = {0,0,1,1,2,2,2,2,3,3,3,3,3,3,3,3,
			4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
			5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
			5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
			6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
			6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
			6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
			6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
			7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
			7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
			7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
			7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
			7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
			7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
			7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
			7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7};
	int sign, exponent, mantissa;
	unsigned char ulawbyte;

	/* Get the sample into sign-magnitude. */
	sign = (sample >> 8) & 0x80;        /* set aside the sign */
	if(sign != 0) sample = -sample;             /* get magnitude */
	if(sample > CLIP) sample = CLIP;            /* clip the magnitude */

	/* Convert from 16 bit linear to ulaw. */
	sample = sample + BIAS;
	exponent = exp_lut[( sample >> 7 ) & 0xFF];
	mantissa = (sample >> (exponent + 3)) & 0x0F;
	ulawbyte = ~(sign | (exponent << 4) | mantissa);
//#ifdef ZEROTRAP
//	if (ulawbyte == 0) ulawbyte = 0x02; /* optional CCITT trap */
//#endif

	return(ulawbyte);
}

/*
 ** This routine converts from ulaw to 16 bit linear.
 **
 ** Craig Reese: IDA/Supercomputing Research Center
 ** 29 September 1989
 **
 ** References:
 ** 1) CCITT Recommendation G.711  (very difficult to follow)
 ** 2) MIL-STD-188-113,"Interoperability and Performance Standards
 **     for Analog-to_Digital Conversion Techniques,"
 **     17 February 1987
 **
 ** Input: 8 bit ulaw sample
 ** Output: signed 16 bit linear sample
 */

int ulaw2linear(unsigned char ulawbyte)
{
	static int exp_lut[8] = { 0, 132, 396, 924, 1980, 4092, 8316, 16764 };
	int sign, exponent, mantissa, sample;

	ulawbyte = ~ulawbyte;
	sign = (ulawbyte & 0x80);
	exponent = (ulawbyte >> 4) & 0x07;
	mantissa = ulawbyte & 0x0F;
	sample = exp_lut[exponent] + (mantissa << (exponent + 3));
	if(sign != 0) sample = -sample;

	return(sample);
}






//signed short uLaw2Linear(unsigned char uval)
//{
//	signed short t;
//	if (false)
//		return (signed short) (uval * 256);
//	uval = (unsigned char) (uval ^ 0xFF);
//	t = (signed short) (((uval & 0xF) << 3) + 0x84);
//	t <<= (uval & 0x70) >> 4;
//
//	return ((signed short) (((uval & 0x80) == 0x80) ? (0x84 - t) : (t - 0x84)));
//}
//
//
//signed short search(signed short sample)
//{
//	signed short i;
//	signed short ulawend[] = { 0x3F, 0x7F, 0xFF, 0x1FF,
//			0x3FF, 0x7FF, 0xFFF, 0x1FFF };
//
//	for (i = 0; i < 8; i++) {
//		if (sample <= ulawend[i]) return i;
//	}
//	return i;
//}
//
//
//
//unsigned char Linear2uLaw(signed short sample)
//{
//	signed short ret = 0;
//	signed short seg;
//	signed short mask;
//	signed short uval;
//
//	// make it 14 bits
//	sample >>= 2;
//
//	if (sample < 0) {
//		sample = (signed short) (-sample);
//		mask = 0x7F;
//	}
//	else {
//		mask = 0xFF;
//	}
//	if (sample > 8159) sample = 8159;
//	sample += (0x84 >> 2);
//
//	seg = search(sample);
//	if (seg >= 8) {
//		return (unsigned char) (0x7F ^ mask);
//	}
//	else {
//		uval = (signed short) ((seg << 4) | ((sample >> (seg + 1)) & 0xF));
//		return (unsigned char) (uval ^ mask);
//	}
//}

#if 0
/*  Read a line from a socket  */
ssize_t readLine(int sockd, void *vptr, size_t maxlen)
{
	ssize_t n, rc;
	char c, *buffer;

	buffer = vptr;

	for (n = 1; n < maxlen; n++) {

		if ((rc = read(sockd, &c, 1)) == 1) {
			*buffer++ = c;
			if (c == '\n')
				break;
		}
		else if (rc == 0) {
			if (n == 1)
				return 0;
			else
				break;
		}
		else {
			if (errno == EINTR)
				continue;
			return -1;
		}
	}

	*buffer = 0;
	return n;
}


/*  Write a line to a socket  */
ssize_t writeLine(int sockd, const void *vptr, size_t n)
{
	size_t nleft;
	ssize_t nwritten;
	const char *buffer;

	buffer = vptr;
	nleft = n;

	while (nleft > 0) {
		if ((nwritten = write(sockd, buffer, nleft)) <= 0) {
			if (errno == EINTR)
				nwritten = 0;
			else
				return -1;
		}
		nleft -= nwritten;
		buffer += nwritten;
	}

	return n;
}
#endif

unsigned char getHighByte(signed short n)
{
	return ((n >> 8) & 0x00FF);
}


unsigned char getLowByte(signed short n)
{
	return (n & 0x00FF);
}


void StrLower(char str[])
{
	int i;
	for (i = 0; str[i] != '\0'; i++)
		str[i] = (char) tolower(str[i]);
}


char *toHexString(char *buf, int length, char *hexstring)
{
	//    unsigned char buf[] = {0, 1, 10, 11};
	/* target buffer should be large enough */
	//    char hexstring[12];
	char * pin = buf;
	const char * hex = "0123456789ABCDEF";
	char * pout = hexstring;
	int i = 0;
//	for (; i < sizeof(buf) - 1; ++i)
	for (; i < length - 1; ++i)
	{
		*pout++ = hex[(*pin >> 4) & 0xF];
		*pout++ = hex[(*pin++) & 0xF];
		*pout++ = ' ';
	}
	*pout++ = hex[(*pin >> 4) & 0xF];
	*pout++ = hex[(*pin) & 0xF];
	*pout = 0;

	//    printf("%s\n", hexstring);
	return hexstring;
}

